/*
 * Created on Aug 6, 2009
 *
 */
package com.moneygram.geoip.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.moneygram.ree.lib.Config;

/**
 * 
 * Resource Config Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/11/11 19:29:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ResourceConfigFactory {

    /**
     * Creates new instance of the Config.
     * @return
     * @throws Exception
     */
    public Config createResourceConfiguration(String jndi) {
        Config config;
        try {
            Context ctx = new InitialContext();
            config = (Config) ctx.lookup(jndi);
        } catch (NamingException e) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi, e);
        }
        if (config == null) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi +". Configuration can not be null.");
        }
        return config;
    }
}
