/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.geoip.util;

/**
 * 
 * GEOIP Resource Config.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GEOIPProxyWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/11/11 19:29:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GEOIPResourceConfig extends ResourceConfig {

    public static final String GEOIP_ENDPOINT = "GEOIPEndpoint";
    public static final String GEOIP_TIMEOUT = "GEOIPTimeout";
    private static int GEOIP_DEFAULT_TIMEOUT = 15000;
    public static final String GEOIP_LICENSE = "GEOIPLicense";

    public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/GEOIPResourceReference";

    /**
     * singleton instance.
     */
    private static GEOIPResourceConfig instance = null;

    /**
     * Creates new instance of ResourceConfig
     * 
     */
    private GEOIPResourceConfig() {
        super();
    }

    /**
     * 
     * @return
     */
    public static GEOIPResourceConfig getInstance() {
        if (instance == null) {
            synchronized (GEOIPResourceConfig.class) {
                if (instance == null) {
                    instance = new GEOIPResourceConfig();
                    instance.initResourceConfig();
                }
            }
        }
        return instance;
    }

    public String getGEOIPEndpoint() {
        return getAttributeValue(GEOIP_ENDPOINT);
    }

    public int getGEOIPTimeout() {
        return getIntegerAttributeValue(GEOIP_TIMEOUT, GEOIP_DEFAULT_TIMEOUT);
    }

    public String getGEOIPLicense() {
        return getAttributeValue(GEOIP_LICENSE);
    }

    @Override
    protected String getResourceConfigurationJndiName() {
        return RESOURCE_REFERENCE_JNDI;
    }

}
