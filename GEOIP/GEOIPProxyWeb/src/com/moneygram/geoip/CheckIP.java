/*
 * Created on Nov 11, 2009
 *
 */
package com.moneygram.geoip;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.geoip.util.GEOIPResourceConfig;
import com.moneygram.geoip.util.HttpRequestUtility;

/**
 * 
 * Check IP servlet.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GEOIPProxyWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/11/12 18:28:54 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class CheckIP extends HttpServlet {
    private static final long serialVersionUID = 8637727531003658697L;

    private static final Logger logger = LogFactory.getInstance().getLogger(CheckIP.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        String url = GEOIPResourceConfig.getInstance().getGEOIPEndpoint();
        int timeout = GEOIPResourceConfig.getInstance().getGEOIPTimeout();

        if (url == null) {
            throw new ServletException("GEOIP url is not configured correctly.");
        }
        
        String ip = request.getParameter("ip");
        if (logger.isInfoEnabled()) {
            logger.info("process: ip=" + ip);
        }

        if (StringUtils.isEmpty(ip)) {
            throw new ServletException("Parameter ip must be specified");
        }

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("i", ip);
        parameters.put("l", GEOIPResourceConfig.getInstance().getGEOIPLicense());

        String geoipResponse;
        try {
            geoipResponse = HttpRequestUtility.sendRequest(url, timeout, parameters, HttpRequestUtility.POST_METHOD);
        } catch (Exception e) {
            throw new ServletException("Failed to process GEOIP request for ip=" + ip, e);
        }

        response.setContentLength(geoipResponse.length());
        Writer writer = response.getWriter();
        writer.write(geoipResponse);
        response.flushBuffer();

        stopWatch.stop();

        if (logger.isInfoEnabled()) {
            logger.info("process: ip=" + ip + " geoip response=" + geoipResponse + " time elapsed="+ stopWatch.toString());
        }
        
    }

}
