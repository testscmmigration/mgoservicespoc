package com.moneygram.geoip.util;


import java.util.Map;

import com.moneygram.ree.lib.Config;

/**
 * 
 * Test Resource Config Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GEOIPProxyWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/11/11 19:26:20 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TestResourceConfigFactory extends ResourceConfigFactory {
    private Map attributes = null;

    public TestResourceConfigFactory(Map attributes) {
        this.attributes = attributes;
    }

    @Override
    public Config createResourceConfiguration(String jndi) {
        return new TestConfig(attributes);
    }
    
}
