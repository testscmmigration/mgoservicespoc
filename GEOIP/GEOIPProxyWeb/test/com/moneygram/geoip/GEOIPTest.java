package com.moneygram.geoip;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.moneygram.geoip.util.GEOIPResourceConfig;
import com.moneygram.geoip.util.HttpRequestUtility;
import com.moneygram.geoip.util.ResourceConfig;
import com.moneygram.geoip.util.TestResourceConfigFactory;

/**
 * 
 * GEOIP Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GEOIPProxyWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/11/12 23:17:16 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GEOIPTest extends TestCase {

    private static final Map attributes = new HashMap();

    static {
        attributes.put(GEOIPResourceConfig.GEOIP_ENDPOINT, "http://geoip3.maxmind.com/a");
        attributes.put(GEOIPResourceConfig.GEOIP_TIMEOUT, "12000");
        attributes.put(GEOIPResourceConfig.GEOIP_LICENSE, "BSO8WuCOLg7S");
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ResourceConfig.setResourceConfigFactory(new TestResourceConfigFactory(attributes));
    }

    public void testGEOIPcall() throws Exception {

        String response = callGEOIP("63.91.129.63");
        assertNotNull("Expected not null response", response);
        assertEquals("Expected a particular country", "US", response);
        
        response = callGEOIP("123");
        assertNotNull("Expected not null response", response);
        assertEquals("Expected an error", "(null),IP_NOT_FOUND", response);

        attributes.put(GEOIPResourceConfig.GEOIP_LICENSE, "badlicense");
        response = callGEOIP("63.91.129.63");
        assertNotNull("Expected not null response", response);
        assertEquals("Expected an error", "(null),INVALID_LICENSE_KEY", response);
    }
    
    private String callGEOIP(String ip) throws Exception {
        String url = GEOIPResourceConfig.getInstance().getGEOIPEndpoint();
        int timeout = GEOIPResourceConfig.getInstance().getGEOIPTimeout();

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("i", ip);
        parameters.put("l", GEOIPResourceConfig.getInstance().getGEOIPLicense());
        
        String response = HttpRequestUtility.sendRequest(url, timeout, parameters, HttpRequestUtility.POST_METHOD); 
        //String response = HttpRequestUtility.sendRequest(url, timeout, parameters, HttpRequestUtility.GET_METHOD); 
        //String response = HttpRequestUtility.sendRequest("http://geoip3.maxmind.com/a?l=BSO8WuCOLg7S&i=63.91.129.63", timeout, null, HttpRequestUtility.GET_METHOD); 

        //String response = HttpRequestUtility.sendRequestViaHttpURLConnection(url, parameters); 

        return response;
    }
}
