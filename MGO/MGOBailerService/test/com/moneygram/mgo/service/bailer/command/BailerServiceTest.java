/*
 * Created on Jun 24, 2009
 *
 */
package com.moneygram.mgo.service.bailer.command;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.mgo.consumer_v1.common_v1.Header;
import com.moneygram.mgo.service.bailer.ProcessBailerEventsRequest;
import com.moneygram.mgo.service.bailer.ProcessBailerEventsResponse;
import com.moneygram.mgo.service.bailer.ServiceAction;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ConsumerServiceProxy;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ProfileEventType;
import com.moneygram.mgo.service.consumer_v1.client.Consumer;
import com.moneygram.mgo.service.consumer_v1.client.GetIncompleteProfilesRequest;
import com.moneygram.mgo.service.consumer_v1.client.GetIncompleteProfilesResponse;
import com.moneygram.mgo.service.consumer_v1.client.MGOConsumerServicePortType;
import com.moneygram.mgo.service.consumer_v1.client.MGOConsumerServicePortTypeProxy;
import com.moneygram.mgo.service.consumer_v1.client.ProfileEvent;

public class BailerServiceTest extends BaseBailerServiceTestCase {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			BailerServiceTest.class);

	private static final String BASE_URL_PROPERTY = "mgo.service.baseurl";
	private static final String BASE_URL = "http://devwsintsvcs.moneygram.com/";
	private static final String SERVICE_URL = "MGOService/services/MGOConsumerService_v1";
	private static final int timeout = 15000;
	
	private static MGOConsumerServicePortType consumerServiceClient = null;

	protected void setUp() throws Exception {
		if (consumerServiceClient != null)
			return;
		consumerServiceClient = new MGOConsumerServicePortTypeProxy(getServiceURL());
		super.setUp();
	}

	private String getServiceURL() {
		String url = System.getProperty(BASE_URL_PROPERTY);
		if (url == null || url.length() == 0) {
			url = BASE_URL;
			if (logger.isDebugEnabled()) {
				logger.debug("getServiceURL: property=" + BASE_URL_PROPERTY
						+ " is not defined. Use default base service url="
						+ url);
			}
		}
		url = url + SERVICE_URL;
		if (logger.isDebugEnabled()) {
			logger.debug("getServiceURL: service url=" + url);
		}
		return url;
	}

	public void testSortEventList() throws Exception {
		logger.info("\ntestSortEventList...");
		List<ProfileEvent> evList = new ArrayList<ProfileEvent>();
		ProfileEvent ev1 = new ProfileEvent();
		Calendar ev1Cal = Calendar.getInstance();
		ev1.setEvent(ProfileEventType.TXN_FirstTxn_Started);
		ev1.setEventDateTime(ev1Cal);
		ProfileEvent ev2 = new ProfileEvent();
		Calendar ev2Cal = Calendar.getInstance();
		ev2Cal.add(Calendar.SECOND, 1);
		ev2.setEvent(ProfileEventType.KBA_Bailer1_Sent);
		ev2.setEventDateTime(ev2Cal);
		ProfileEvent ev3 = new ProfileEvent();
		Calendar ev3Cal = Calendar.getInstance();
		ev3Cal.add(Calendar.SECOND, 2);
		ev3.setEvent(ProfileEventType.KBA_Bailer2_Sent);
		ev3.setEventDateTime(ev3Cal);
		ProfileEvent ev4 = new ProfileEvent();
		Calendar ev4Cal = Calendar.getInstance();
		ev4Cal.add(Calendar.SECOND, 3);
		ev4.setEvent(ProfileEventType.RSA_Bailer1_Sent);
		ev4.setEventDateTime(ev4Cal);

		evList.add(ev4);
		evList.add(ev2);
		evList.add(ev3);
		evList.add(ev1);

		// System.out.println("***BEFORE sort:");
		// printOutEventList(evList);

		ProcessBailerEventsCommand command = new ProcessBailerEventsCommand();
		command.sortEventListMostRecentFirst(evList);

		// System.out.println("***AFTER sort:");
		// printOutEventList(evList);

		// list should be sorted in reverse order

		assertEquals(ev4, evList.get(0));
		assertEquals(ev3, evList.get(1));
		assertEquals(ev2, evList.get(2));
		assertEquals(ev1, evList.get(3));
	}

	
//	public void testSortEventList() throws Exception {
//		logger.info("\ntestSortEventList...");
//		List<ProfileEvent> evList = new ArrayList<ProfileEvent>();
//		ProfileEvent ev1 = new ProfileEvent();
//		ev1.setEvent(ProfileEventType.TXN_FirstTxn_Started);
//		ev1.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev2 = new ProfileEvent();
//		ev2.setEvent(ProfileEventType.KBA_Bailer1_Sent);
//		ev2.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev3 = new ProfileEvent();
//		ev3.setEvent(ProfileEventType.KBA_Bailer2_Sent);
//		ev3.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev4 = new ProfileEvent();
//		ev4.setEvent(ProfileEventType.RSA_Bailer1_Sent);
//		ev4.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev5 = new ProfileEvent();
//		ev5.setEvent(ProfileEventType.RSA_Bailer2_Sent);
//		ev5.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev6 = new ProfileEvent();
//		ev6.setEvent(ProfileEventType.TXN_Review_Bailer1_Sent);
//		ev6.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev7 = new ProfileEvent();
//		ev7.setEvent(ProfileEventType.TXN_Review_Bailer2_Sent);
//		ev7.setEventDateTime(Calendar.getInstance());
//		ProfileEvent ev8 = new ProfileEvent();
//		ev8.setEvent(ProfileEventType.TXN_FirstTxn_Completed);
//		ev8.setEventDateTime(Calendar.getInstance());
//
//		ProfileEvent ev2a = new ProfileEvent();
//		Calendar cal1 = Calendar.getInstance();
//		ev2a.setEvent(ProfileEventType.KBA_Bailer1_Sent);
//		ev2a.setEventDateTime(cal1);
//		
//		ProfileEvent ev2b = new ProfileEvent();
//		Calendar cal2 = Calendar.getInstance();
//		cal2.add(Calendar.SECOND, 5);
//		ev2b.setEvent(ProfileEventType.KBA_Bailer1_Sent);
//		ev2b.setEventDateTime(cal2);
//		
//		evList.add(ev2a);
//		evList.add(ev2);
//		evList.add(ev8);
//		evList.add(ev3);
//		evList.add(ev7);
//		evList.add(ev4);
//		evList.add(ev6);
//		evList.add(ev5);
//		evList.add(ev2b);
//		evList.add(ev1);
//
////		System.out.println("***BEFORE sort:");
////		printOutEventList(evList);
//		
//		ProcessBailerEventsCommand command = new ProcessBailerEventsCommand();
//        command.sortEventListMostRecentFirst(evList);
//
// //		System.out.println("***AFTER sort:");
////		printOutEventList(evList);
//
//        //list should be sorted in reverse order
//		assertEquals(ev8, evList.get(0));
//		assertEquals(ev7, evList.get(1));
//		assertEquals(ev6, evList.get(2));
//		assertEquals(ev5, evList.get(3));
//		assertEquals(ev4, evList.get(4));
//		assertEquals(ev3, evList.get(5));
//		assertEquals(ev2b, evList.get(6));
//		assertEquals(ev2a, evList.get(7));
//		assertEquals(ev2, evList.get(8));
//		assertEquals(ev1, evList.get(9));				
//	}

	private void printOutEventList(List<ProfileEvent> evList) {
		int i=0;
		for (ProfileEvent pe : evList) {
		    SimpleDateFormat format = new SimpleDateFormat ("HH:mm:ss.S");
		    String itemString = "event:"+pe.getEvent() + "; datetime:"+ format.format(pe.getEventDateTime().getTime());		    
			System.out.println("item(" + i+")=" + itemString);
			i++;
		}		
	}

	public void testDurationExpired() throws Exception {
		logger.info("\ntestDurationExpired...");
		List<ProfileEvent> evList = new ArrayList<ProfileEvent>();
		ProfileEvent ev1 = new ProfileEvent();
		Calendar ev1Cal = Calendar.getInstance();
		ev1.setEvent(ProfileEventType.TXN_FirstTxn_Started);
		ev1.setEventDateTime(ev1Cal);
		ProfileEvent ev2 = new ProfileEvent();
		Calendar eventTime = Calendar.getInstance();
		eventTime.add(Calendar.MINUTE, -2);
		
		//Should always return false if 'minutes' value <= 0
		assertFalse(ProcessBailerEventsCommand.durationExpired(eventTime, 0));
		assertFalse(ProcessBailerEventsCommand.durationExpired(eventTime, -1));
		
		//event time is 2 minutes old,  minutes = 1 so duration is expired
		assertTrue(ProcessBailerEventsCommand.durationExpired(eventTime, 1));
		
		//event time is 2 minutes old,  minutes = 3 so duration is not expired		
		assertFalse(ProcessBailerEventsCommand.durationExpired(eventTime, 3));
				
	}
	public void xtestProcessBailerEvents() throws Exception {
		logger.info("\ntestProcessBailerEvents...");

		long[] consumerIds = getIncompleteProfiles();
		assertTrue(consumerIds.length > 0);
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.processBailerEvents.getValue());

		ProcessBailerEventsRequest request = new ProcessBailerEventsRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerId(new Long(consumerIds[0]));
		//request.setConsumerLoginId("mgobailer@mailinator.com");

		RequestRouter requestRouter = getRequestRouter();
		ProcessBailerEventsResponse response = (ProcessBailerEventsResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	private long[] getIncompleteProfiles() throws Exception {
		logger.info("\ngetIncompleteProfiles...");

		com.moneygram.mgo.consumer_v1.common_v1.ProcessingInstruction processingInstruction = new com.moneygram.mgo.consumer_v1.common_v1.ProcessingInstruction();
		processingInstruction.setAction(com.moneygram.mgo.service.consumer_v1.client.ServiceAction.getIncompleteProfiles.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);

		GetIncompleteProfilesRequest request = new GetIncompleteProfilesRequest();
		request.setHeader(header);
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.set(2009,0,1);
		request.setStartDateTime(start);
		request.setEndDateTime(end);

		GetIncompleteProfilesResponse response = consumerServiceClient.getIncompleteProfiles(request);
		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumer", response.getConsumerId());
		
		return response.getConsumerId();
	}

//	public void testConsumerProfile() throws Exception {
//	    ConsumerServiceProxy proxy = new ConsumerServiceProxy();
//	    Long consumerId = 1549631L;
//        Consumer profile = proxy.getConsumerProfile(consumerId , null);
//    }
}
