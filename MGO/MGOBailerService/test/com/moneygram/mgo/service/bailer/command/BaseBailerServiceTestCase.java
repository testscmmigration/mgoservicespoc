/*
 * Created on Jun 24, 2009
 *
 */
package com.moneygram.mgo.service.bailer.command;

import java.util.HashMap;
import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.util.ResourceConfig;
import com.moneygram.common.service.util.TestResourceConfigFactory;
import com.moneygram.mgo.service.bailer.dao.BailerDAO;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;
import com.moneygram.service.BaseServiceTest;
import java.security.Security;

public abstract class BaseBailerServiceTestCase extends BaseServiceTest {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			BaseBailerServiceTestCase.class);

	
	protected static final Map<String, String> attributes = new HashMap<String, String>();
	static {
		attributes.put(MGOBailerServiceResourceConfig.MGO_CONSUMER_SERVICE_URL,
				"http://devwsintsvcs.moneygram.com/MGOService/services/MGOConsumerService_v1");
		attributes.put(MGOBailerServiceResourceConfig.MGO_CONSUMER_SERVICE_TIMEOUT, "15000");
		attributes.put(MGOBailerServiceResourceConfig.BAILER_SEND_EMAIL_ADDRESS, "eMoneyTransfer@moneygram.com");
		attributes.put(MGOBailerServiceResourceConfig.NOTIFICATION_CONNECTION_FACTORY, "NOTIFICATION_CONNECTION_FACTORY");
		attributes.put(MGOBailerServiceResourceConfig.NOTIFICATION_QUEUE, "NOTIFICATION_QUEUE");
		attributes.put(MGOBailerServiceResourceConfig.KBA1_BAILER_DELAY_MINUTES, "1");
		attributes.put(MGOBailerServiceResourceConfig.KBA1_BAILER_DELAY_MINUTES, "2");
		attributes.put(MGOBailerServiceResourceConfig.RSA1_BAILER_DELAY_MINUTES, "1");
		attributes.put(MGOBailerServiceResourceConfig.RSA2_BAILER_DELAY_MINUTES, "2");
		attributes.put(MGOBailerServiceResourceConfig.TXN_REVIEW1_BAILER_DELAY_MINUTES, "1");
		attributes.put(MGOBailerServiceResourceConfig.TXN_REVIEW2_BAILER_DELAY_MINUTES, "2");		
	}

	private static BailerDAO bailerDAO = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Security.setProperty("ssl.SocketFactory.provider",
				"com.ibm.jsse2.SSLSocketFactoryImpl");
		Security.setProperty("ssl.ServerSocketFactory.provider",
				"com.ibm.jsse2.SSLServerSocketFactoryImpl");
		ResourceConfig.setResourceConfigFactory(new TestResourceConfigFactory(
				attributes));
	}


   @Override
   protected String[] getContextResources() {
       return new String[] { "BailerTestContext.xml" };
   }

}
