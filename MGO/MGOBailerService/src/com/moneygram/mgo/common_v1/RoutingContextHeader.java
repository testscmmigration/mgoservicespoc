/**
 * RoutingContextHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.common_v1;

public class RoutingContextHeader  implements java.io.Serializable {
    private com.moneygram.mgo.common_v1.ServiceClient clientSystem;

    private java.lang.String agentInfo;

    private java.lang.String product;

    public RoutingContextHeader() {
    }

    public RoutingContextHeader(
           com.moneygram.mgo.common_v1.ServiceClient clientSystem,
           java.lang.String agentInfo,
           java.lang.String product) {
           this.clientSystem = clientSystem;
           this.agentInfo = agentInfo;
           this.product = product;
    }


    /**
     * Gets the clientSystem value for this RoutingContextHeader.
     * 
     * @return clientSystem
     */
    public com.moneygram.mgo.common_v1.ServiceClient getClientSystem() {
        return clientSystem;
    }


    /**
     * Sets the clientSystem value for this RoutingContextHeader.
     * 
     * @param clientSystem
     */
    public void setClientSystem(com.moneygram.mgo.common_v1.ServiceClient clientSystem) {
        this.clientSystem = clientSystem;
    }


    /**
     * Gets the agentInfo value for this RoutingContextHeader.
     * 
     * @return agentInfo
     */
    public java.lang.String getAgentInfo() {
        return agentInfo;
    }


    /**
     * Sets the agentInfo value for this RoutingContextHeader.
     * 
     * @param agentInfo
     */
    public void setAgentInfo(java.lang.String agentInfo) {
        this.agentInfo = agentInfo;
    }


    /**
     * Gets the product value for this RoutingContextHeader.
     * 
     * @return product
     */
    public java.lang.String getProduct() {
        return product;
    }


    /**
     * Sets the product value for this RoutingContextHeader.
     * 
     * @param product
     */
    public void setProduct(java.lang.String product) {
        this.product = product;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RoutingContextHeader)) return false;
        RoutingContextHeader other = (RoutingContextHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientSystem==null && other.getClientSystem()==null) || 
             (this.clientSystem!=null &&
              this.clientSystem.equals(other.getClientSystem()))) &&
            ((this.agentInfo==null && other.getAgentInfo()==null) || 
             (this.agentInfo!=null &&
              this.agentInfo.equals(other.getAgentInfo()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientSystem() != null) {
            _hashCode += getClientSystem().hashCode();
        }
        if (getAgentInfo() != null) {
            _hashCode += getAgentInfo().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RoutingContextHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RoutingContextHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientSystem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "clientSystem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceClient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "agentInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "product"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
