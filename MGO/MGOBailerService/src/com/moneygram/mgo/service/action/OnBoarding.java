package com.moneygram.mgo.service.action;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.bailer.OnboardCustomer;
import com.moneygram.mgo.service.bailer.dao.BailerDAO;
import com.moneygram.mgo.service.bailer.notification.NotificationServiceProxy;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;

public class OnBoarding implements MGOJobSeviceAction {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			OnBoarding.class);
	private static final int SITE_PARAM = 1;

	@SuppressWarnings("static-access")
	/**
	 * This method process onboardCustomerList and send onboard mails to onboardCustomers
	 * @param bailerDAO
	 * @return String
	 */
	public String process(BailerDAO bailerDAO) {
		//changes by Ankit Bhatt for 8873
		//String returnValue = "SUCCESS";
		String returnValue = "FAILURE";
		//changes end for 8873
		logger.info("*******Processing Onboarding Emails has begun...");
		int nbrThankYouEmailsSent = 0;
		int nbrAccountOverviewEmailsSent = 0;
		int nbrSendMoneyAgainEmailsSent = 0;

		try {
			/* Process Onboarding Thank you emails */
			List<OnboardCustomer> onboardThankYouCustomerList = bailerDAO
					.getOnboardCustomers(SITE_PARAM,
							Integer
							.parseInt(MGOBailerServiceResourceConfig
									.getInstance()
									.getMgojobOnboardingNbrdaysThankyou()));
			NotificationServiceProxy notificationServiceProxy = new NotificationServiceProxy();
			if (onboardThankYouCustomerList != null) {
				for (OnboardCustomer onboardCustomer : onboardThankYouCustomerList) {
					notificationServiceProxy
							.sendMessageToNotificationService(
									notificationServiceProxy.MGO_EMAIL_ONBOARDING_THKU_US_EN,
									onboardCustomer,
									onboardCustomer.getCreateDateTime());
					nbrThankYouEmailsSent++;
				}
			}
			/* Process Onboarding Account Overview emails */
			List<OnboardCustomer> onboardAccountOverviewCustomerList = bailerDAO
					.getOnboardCustomers(SITE_PARAM,
			 Integer.parseInt(MGOBailerServiceResourceConfig
			 .getInstance()
			 .getMgojobOnboardingNbrdaysAccountoverview()));
			for (OnboardCustomer onboardCustomer : onboardAccountOverviewCustomerList) {
				notificationServiceProxy.sendMessageToNotificationService(
						notificationServiceProxy.MGO_EMAIL_ONBOARDING_AO_US_EN,
						onboardCustomer, onboardCustomer.getCreateDateTime());
				nbrAccountOverviewEmailsSent++;
			}
			/* Process Onboarding Send Money Again emails */
			List<OnboardCustomer> onboardSendMoneyAgainCustomerList = bailerDAO
					.getOnboardCustomers(SITE_PARAM,
			 Integer.parseInt(MGOBailerServiceResourceConfig
			 .getInstance()
			 .getMgojobOnboardingNbrdaysSendmoneyagain()));
			for (OnboardCustomer onboardCustomer : onboardSendMoneyAgainCustomerList) {
				notificationServiceProxy
						.sendMessageToNotificationService(
								notificationServiceProxy.MGO_EMAIL_ONBOARDING_SMA_US_EN,
								onboardCustomer,
								onboardCustomer.getCreateDateTime());
				nbrSendMoneyAgainEmailsSent++;
			}
			returnValue = "SUCCESS#Emails sent:  Thankyou="
					+ nbrThankYouEmailsSent + "; AcctOverview="
					+ nbrAccountOverviewEmailsSent + ";SendMoneyAgain="
					+ nbrSendMoneyAgainEmailsSent;
			logger.debug(returnValue);
			logger.info("*******Processing Onboarding Emails has completed...");
		} catch (Exception exception) {
			logger.error("Exception in processing onboarding mails: ",
					exception);
			exception.printStackTrace();
			logger.error("*******Processing onboarding Emails has FAILED...");
		}
		return returnValue;
	}

}