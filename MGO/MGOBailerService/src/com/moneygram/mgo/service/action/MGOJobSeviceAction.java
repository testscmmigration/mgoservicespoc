package com.moneygram.mgo.service.action;

import com.moneygram.mgo.service.bailer.dao.BailerDAO;

public interface MGOJobSeviceAction {
	public String process(BailerDAO bailerDAO);

}
