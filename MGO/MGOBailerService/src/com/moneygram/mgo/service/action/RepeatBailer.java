package com.moneygram.mgo.service.action;

import java.util.Calendar;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.bailer.JobCustomer;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ConsumerServiceProxy;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ProfileEventType;
import com.moneygram.mgo.service.bailer.dao.BailerDAO;
import com.moneygram.mgo.service.bailer.notification.NotificationServiceProxy;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;

/**
 * Class is used for Sending mails to Repeat Bailer customers.
 * MGO-8873
 * @author wx54
 *
 */
public class RepeatBailer implements MGOJobSeviceAction {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			RepeatBailer.class);

	@SuppressWarnings("static-access")
	/**
	 * This method process repeatBailerCustFirstList and repeatBailerCustSecondList
	 * to send repeat bailer mails to Repeat Bailer Customers.
	 * @param bailerDAO
	 * @return String
	 */
	public String process(BailerDAO bailerDAO) {		
		String returnValue = "FAILURE";
		System.out.println("Inside RepeatBailer Servlet for Repeat Bailer Emails");
		logger.info("*******Processing Repeat Bailer Emails has begun...");
		
		NotificationServiceProxy notificationServiceProxy = new NotificationServiceProxy();
		ConsumerServiceProxy csProxy = new ConsumerServiceProxy();
		int firstBailerMailCounter = 0;
		int secondBailerMailCounter = 0;

		try {
			/* Process Repeat Bailer Mails for the first time */
			
			//One mail will be send to Customer after 1 hr of INC transaction time
			List<JobCustomer> repeatBailerCustFirstList = bailerDAO
					.getRepeatBailerCustomers(MGOBailerServiceResourceConfig
							 .getInstance()
							 .getNbrHoursBailerMail1(), "Y");
						
			if (repeatBailerCustFirstList != null) {
				for (JobCustomer jobCustomer : repeatBailerCustFirstList) {
					notificationServiceProxy
							.sendMessageToNotificationService(
									notificationServiceProxy.MSG_KEY_REPEAT_BAILER_MAIL_1,
									jobCustomer);
					csProxy.addProfileEvent(jobCustomer.getCustId(), ProfileEventType.REPEAT_BAILER_MAIL1_SENT, Calendar.getInstance());
					firstBailerMailCounter++;
				}
			}
			/* Second mail to Customer will be send after 24 hrs after 1st bailer Mail of INC transaction IF no action 
			 * has been taken by Customer for INC transaction. */
			List<JobCustomer> repeatBailerCustSecondList = bailerDAO
			.getRepeatBailerCustomers(MGOBailerServiceResourceConfig
					 .getInstance()
					 .getNbrHoursBailerMail2(), "N");
			
			if (repeatBailerCustSecondList != null) {
				for (JobCustomer jobCustomer : repeatBailerCustSecondList) {
					notificationServiceProxy
							.sendMessageToNotificationService(
									notificationServiceProxy.MSG_KEY_REPEAT_BAILER_MAIL_2,
									jobCustomer);
					csProxy.addProfileEvent(jobCustomer.getCustId(), ProfileEventType.REPEAT_BAILER_MAIL2_SENT, Calendar.getInstance());
					secondBailerMailCounter++;
				}
			}
			
			returnValue = "SUCCESS#Emails sent:  Bailer1="
					+ firstBailerMailCounter + "; Bailer2="
					+ secondBailerMailCounter;
			logger.debug(returnValue);
			logger.info("*******Processing Repeat Bailer Emails has completed...");
			System.out.println("*******Processing Repeat Bailer Emails has completed with return value " + returnValue);
		} catch (Exception exception) {
			logger.error("Exception in processing Repeat Bailer mails: ",
					exception);
			exception.printStackTrace();
			logger.error("*******Processing Repeat Bailer Emails has FAILED...");
		}
		return returnValue;
	}

}