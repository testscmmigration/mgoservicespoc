package com.moneygram.mgo.service.bailer_v1;

public class MGOBailerServicePortTypeProxy implements com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType {
  private String _endpoint = null;
  private com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType mGOBailerServicePortType = null;
  
  public MGOBailerServicePortTypeProxy() {
    _initMGOBailerServicePortTypeProxy();
  }
  
  public MGOBailerServicePortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initMGOBailerServicePortTypeProxy();
  }
  
  private void _initMGOBailerServicePortTypeProxy() {
    try {
      mGOBailerServicePortType = (new com.moneygram.mgo.service.bailer_v1.MGOBailerServiceLocator()).getMGOBailerService_v1();
      if (mGOBailerServicePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)mGOBailerServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)mGOBailerServicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (mGOBailerServicePortType != null)
      ((javax.xml.rpc.Stub)mGOBailerServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType getMGOBailerServicePortType() {
    if (mGOBailerServicePortType == null)
      _initMGOBailerServicePortTypeProxy();
    return mGOBailerServicePortType;
  }
  
  public com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse processBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest processBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException{
    if (mGOBailerServicePortType == null)
      _initMGOBailerServicePortTypeProxy();
    return mGOBailerServicePortType.processBailerEvents(processBailerEventsRequest);
  }
  
  public com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse processFirstTxBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest processFirstTxBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException{
    if (mGOBailerServicePortType == null)
      _initMGOBailerServicePortTypeProxy();
    return mGOBailerServicePortType.processFirstTxBailerEvents(processFirstTxBailerEventsRequest);
  }
  
  public com.moneygram.mgo.service.bailer_v1.ProcessActionResponse processAction(com.moneygram.mgo.service.bailer_v1.ProcessActionRequest processActionRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException{
    if (mGOBailerServicePortType == null)
      _initMGOBailerServicePortTypeProxy();
    return mGOBailerServicePortType.processAction(processActionRequest);
  }
  
  
}