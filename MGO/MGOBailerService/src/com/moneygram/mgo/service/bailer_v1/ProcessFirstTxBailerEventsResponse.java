/**
 * ProcessFirstTxBailerEventsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public class ProcessFirstTxBailerEventsResponse  extends com.moneygram.mgo.common_v1.BaseResponse  implements java.io.Serializable {
    private boolean bailerEmailSent;

    public ProcessFirstTxBailerEventsResponse() {
    }

    public ProcessFirstTxBailerEventsResponse(
           com.moneygram.mgo.common_v1.Header header,
           boolean bailerEmailSent) {
        super(
            header);
        this.bailerEmailSent = bailerEmailSent;
    }


    /**
     * Gets the bailerEmailSent value for this ProcessFirstTxBailerEventsResponse.
     * 
     * @return bailerEmailSent
     */
    public boolean isBailerEmailSent() {
        return bailerEmailSent;
    }


    /**
     * Sets the bailerEmailSent value for this ProcessFirstTxBailerEventsResponse.
     * 
     * @param bailerEmailSent
     */
    public void setBailerEmailSent(boolean bailerEmailSent) {
        this.bailerEmailSent = bailerEmailSent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessFirstTxBailerEventsResponse)) return false;
        ProcessFirstTxBailerEventsResponse other = (ProcessFirstTxBailerEventsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.bailerEmailSent == other.isBailerEmailSent();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += (isBailerEmailSent() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcessFirstTxBailerEventsResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessFirstTxBailerEventsResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bailerEmailSent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "bailerEmailSent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
