/**
 * MGOBailerServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public interface MGOBailerServicePortType extends java.rmi.Remote {
    public com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse processBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest processBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException;
    public com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse processFirstTxBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest processFirstTxBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException;
    public com.moneygram.mgo.service.bailer_v1.ProcessActionResponse processAction(com.moneygram.mgo.service.bailer_v1.ProcessActionRequest processActionRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException;
}
