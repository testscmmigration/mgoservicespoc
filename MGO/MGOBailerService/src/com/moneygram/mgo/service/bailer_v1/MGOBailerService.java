/**
 * MGOBailerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public interface MGOBailerService extends javax.xml.rpc.Service {
    public java.lang.String getMGOBailerService_v1Address();

    public com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType getMGOBailerService_v1() throws javax.xml.rpc.ServiceException;

    public com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType getMGOBailerService_v1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
