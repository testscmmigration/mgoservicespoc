/**
 * MGOBailerService_v1SoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public class MGOBailerService_v1SoapBindingStub extends org.apache.axis.client.Stub implements com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("processBailerEvents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processBailerEventsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessBailerEventsRequest"), com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessBailerEventsResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processBailerEventsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "bailerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("processFirstTxBailerEvents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processFirstTxBailerEventsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessFirstTxBailerEventsRequest"), com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessFirstTxBailerEventsResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processFirstTxBailerEventsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "bailerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("processAction");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processActionRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessActionRequest"), com.moneygram.mgo.service.bailer_v1.ProcessActionRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessActionResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.bailer_v1.ProcessActionResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "processActionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "bailerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[2] = oper;

    }

    public MGOBailerService_v1SoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MGOBailerService_v1SoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MGOBailerService_v1SoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.BaseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ClientHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ClientHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorCategoryCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ErrorCategoryCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorHandlingCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ErrorHandlingCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Errors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ServiceException[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Header");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.Header.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "InvocationMethodCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.InvocationMethodCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ProcessingInstruction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ProcessingInstruction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.RelatedError.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedErrors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.RelatedError[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RoutingContextHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.RoutingContextHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "SecurityHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.SecurityHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceClient");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ServiceClient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v1.ServiceException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ConsumerId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "LoginId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessActionRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessActionRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessActionResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessActionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessBailerEventsRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessBailerEventsResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessFirstTxBailerEventsRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessFirstTxBailerEventsResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessParamType");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ProcessParamType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ServiceAction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.bailer_v1.ServiceAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "AddressLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "BuildingName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "City");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v1.ConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v1.ConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Country");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "CountryName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "County");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "DeliveryInstructionLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "FirstName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LoyaltyMemberId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleInitial");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v1.ReceiverConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v1.ReceiverConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "State");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "StateCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "UserId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Zip");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse processBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest processBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "processBailerEvents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {processBailerEventsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v1.ServiceException) {
              throw (com.moneygram.mgo.common_v1.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse processFirstTxBailerEvents(com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest processFirstTxBailerEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "processFirstTxBailerEvents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {processFirstTxBailerEventsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v1.ServiceException) {
              throw (com.moneygram.mgo.common_v1.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.bailer_v1.ProcessActionResponse processAction(com.moneygram.mgo.service.bailer_v1.ProcessActionRequest processActionRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v1.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "processAction"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {processActionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.bailer_v1.ProcessActionResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.bailer_v1.ProcessActionResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.bailer_v1.ProcessActionResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v1.ServiceException) {
              throw (com.moneygram.mgo.common_v1.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
