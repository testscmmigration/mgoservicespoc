/**
 * MGOBailerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public class MGOBailerServiceLocator extends org.apache.axis.client.Service implements com.moneygram.mgo.service.bailer_v1.MGOBailerService {

    public MGOBailerServiceLocator() {
    }


    public MGOBailerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MGOBailerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MGOBailerService_v1
    private java.lang.String MGOBailerService_v1_address = "http://base.url/MGOService/services/MGOBailerService_v1";

    public java.lang.String getMGOBailerService_v1Address() {
        return MGOBailerService_v1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MGOBailerService_v1WSDDServiceName = "MGOBailerService_v1";

    public java.lang.String getMGOBailerService_v1WSDDServiceName() {
        return MGOBailerService_v1WSDDServiceName;
    }

    public void setMGOBailerService_v1WSDDServiceName(java.lang.String name) {
        MGOBailerService_v1WSDDServiceName = name;
    }

    public com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType getMGOBailerService_v1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MGOBailerService_v1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMGOBailerService_v1(endpoint);
    }

    public com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType getMGOBailerService_v1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.moneygram.mgo.service.bailer_v1.MGOBailerService_v1SoapBindingStub _stub = new com.moneygram.mgo.service.bailer_v1.MGOBailerService_v1SoapBindingStub(portAddress, this);
            _stub.setPortName(getMGOBailerService_v1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMGOBailerService_v1EndpointAddress(java.lang.String address) {
        MGOBailerService_v1_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.moneygram.mgo.service.bailer_v1.MGOBailerService_v1SoapBindingStub _stub = new com.moneygram.mgo.service.bailer_v1.MGOBailerService_v1SoapBindingStub(new java.net.URL(MGOBailerService_v1_address), this);
                _stub.setPortName(getMGOBailerService_v1WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MGOBailerService_v1".equals(inputPortName)) {
            return getMGOBailerService_v1();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "MGOBailerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "MGOBailerService_v1"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MGOBailerService_v1".equals(portName)) {
            setMGOBailerService_v1EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
