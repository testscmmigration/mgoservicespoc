/**
 * ProcessBailerEventsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

public class ProcessBailerEventsRequest  extends com.moneygram.mgo.common_v1.BaseRequest  implements java.io.Serializable {
    private java.lang.Long consumerId;

    private java.lang.String consumerLoginId;

    public ProcessBailerEventsRequest() {
    }

    public ProcessBailerEventsRequest(
           com.moneygram.mgo.common_v1.Header header,
           java.lang.Long consumerId,
           java.lang.String consumerLoginId) {
        super(
            header);
        this.consumerId = consumerId;
        this.consumerLoginId = consumerLoginId;
    }


    /**
     * Gets the consumerId value for this ProcessBailerEventsRequest.
     * 
     * @return consumerId
     */
    public java.lang.Long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this ProcessBailerEventsRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(java.lang.Long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the consumerLoginId value for this ProcessBailerEventsRequest.
     * 
     * @return consumerLoginId
     */
    public java.lang.String getConsumerLoginId() {
        return consumerLoginId;
    }


    /**
     * Sets the consumerLoginId value for this ProcessBailerEventsRequest.
     * 
     * @param consumerLoginId
     */
    public void setConsumerLoginId(java.lang.String consumerLoginId) {
        this.consumerLoginId = consumerLoginId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessBailerEventsRequest)) return false;
        ProcessBailerEventsRequest other = (ProcessBailerEventsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.consumerLoginId==null && other.getConsumerLoginId()==null) || 
             (this.consumerLoginId!=null &&
              this.consumerLoginId.equals(other.getConsumerLoginId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getConsumerLoginId() != null) {
            _hashCode += getConsumerLoginId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcessBailerEventsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "ProcessBailerEventsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerLoginId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/bailer_v1", "consumerLoginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
