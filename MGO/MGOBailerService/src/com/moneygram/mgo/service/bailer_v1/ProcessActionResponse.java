/**
 * ProcessActionResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

import com.moneygram.common.service.BaseOperationResponse;

public class ProcessActionResponse extends BaseOperationResponse implements
		java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3395393473604120396L;
	private java.lang.String processingText;

	public ProcessActionResponse() {
	}

	/**
	 * Gets the processingText value for this ProcessActionResponse.
	 * 
	 * @return processingText
	 */
	public java.lang.String getProcessingText() {
		return processingText;
	}

	/**
	 * Sets the processingText value for this ProcessActionResponse.
	 * 
	 * @param processingText
	 */
	public void setProcessingText(java.lang.String processingText) {
		this.processingText = processingText;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ProcessActionResponse))
			return false;
		ProcessActionResponse other = (ProcessActionResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.processingText == null && other.getProcessingText() == null) || (this.processingText != null && this.processingText
						.equals(other.getProcessingText())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getProcessingText() != null) {
			_hashCode += getProcessingText().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			ProcessActionResponse.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1",
				"ProcessActionResponse"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("processingText");
		elemField
				.setXmlName(new javax.xml.namespace.QName(
						"http://moneygram.com/mgo/service/bailer_v1",
						"processingText"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType,
				_xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType,
				_xmlType, typeDesc);
	}

}
