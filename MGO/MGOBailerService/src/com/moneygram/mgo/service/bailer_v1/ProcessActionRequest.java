/**
 * ProcessActionRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.bailer_v1;

import com.moneygram.common.service.BaseOperationRequest;

public class ProcessActionRequest extends BaseOperationRequest implements
		java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8752104315976818331L;

	private java.lang.String action;

	private com.moneygram.mgo.service.bailer_v1.ProcessParamType[] processParams;

	private java.lang.String[] sites;

	public ProcessActionRequest() {
	}

	/**
	 * Gets the action value for this ProcessActionRequest.
	 * 
	 * @return action
	 */
	public java.lang.String getAction() {
		return action;
	}

	/**
	 * Sets the action value for this ProcessActionRequest.
	 * 
	 * @param action
	 */
	public void setAction(java.lang.String action) {
		this.action = action;
	}

	/**
	 * Gets the processParams value for this ProcessActionRequest.
	 * 
	 * @return processParams
	 */
	public com.moneygram.mgo.service.bailer_v1.ProcessParamType[] getProcessParams() {
		return processParams;
	}

	/**
	 * Sets the processParams value for this ProcessActionRequest.
	 * 
	 * @param processParams
	 */
	public void setProcessParams(
			com.moneygram.mgo.service.bailer_v1.ProcessParamType[] processParams) {
		this.processParams = processParams;
	}

	public com.moneygram.mgo.service.bailer_v1.ProcessParamType getProcessParams(
			int i) {
		return this.processParams[i];
	}

	public void setProcessParams(int i,
			com.moneygram.mgo.service.bailer_v1.ProcessParamType _value) {
		this.processParams[i] = _value;
	}

	/**
	 * Gets the sites value for this ProcessActionRequest.
	 * 
	 * @return sites
	 */
	public java.lang.String[] getSites() {
		return sites;
	}

	/**
	 * Sets the sites value for this ProcessActionRequest.
	 * 
	 * @param sites
	 */
	public void setSites(java.lang.String[] sites) {
		this.sites = sites;
	}

	public java.lang.String getSites(int i) {
		return this.sites[i];
	}

	public void setSites(int i, java.lang.String _value) {
		this.sites[i] = _value;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ProcessActionRequest))
			return false;
		ProcessActionRequest other = (ProcessActionRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.action == null && other.getAction() == null) || (this.action != null && this.action
						.equals(other.getAction())))
				&& ((this.processParams == null && other.getProcessParams() == null) || (this.processParams != null && java.util.Arrays
						.equals(this.processParams, other.getProcessParams())))
				&& ((this.sites == null && other.getSites() == null) || (this.sites != null && java.util.Arrays
						.equals(this.sites, other.getSites())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getAction() != null) {
			_hashCode += getAction().hashCode();
		}
		if (getProcessParams() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getProcessParams()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getProcessParams(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getSites() != null) {
			for (int i = 0; i < java.lang.reflect.Array.getLength(getSites()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(getSites(),
						i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	// Type metadata
	private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
			ProcessActionRequest.class, true);

	static {
		typeDesc.setXmlType(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1",
				"ProcessActionRequest"));
		org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("action");
		elemField.setXmlName(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1", "action"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("processParams");
		elemField.setXmlName(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1", "processParams"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1",
				"ProcessParamType"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		elemField.setMaxOccursUnbounded(true);
		typeDesc.addFieldDesc(elemField);
		elemField = new org.apache.axis.description.ElementDesc();
		elemField.setFieldName("sites");
		elemField.setXmlName(new javax.xml.namespace.QName(
				"http://moneygram.com/mgo/service/bailer_v1", "sites"));
		elemField.setXmlType(new javax.xml.namespace.QName(
				"http://www.w3.org/2001/XMLSchema", "string"));
		elemField.setMinOccurs(0);
		elemField.setNillable(false);
		elemField.setMaxOccursUnbounded(true);
		typeDesc.addFieldDesc(elemField);
	}

	/**
	 * Return type metadata object
	 */
	public static org.apache.axis.description.TypeDesc getTypeDesc() {
		return typeDesc;
	}

	/**
	 * Get Custom Serializer
	 */
	public static org.apache.axis.encoding.Serializer getSerializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanSerializer(_javaType,
				_xmlType, typeDesc);
	}

	/**
	 * Get Custom Deserializer
	 */
	public static org.apache.axis.encoding.Deserializer getDeserializer(
			java.lang.String mechType, java.lang.Class _javaType,
			javax.xml.namespace.QName _xmlType) {
		return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType,
				_xmlType, typeDesc);
	}

}
