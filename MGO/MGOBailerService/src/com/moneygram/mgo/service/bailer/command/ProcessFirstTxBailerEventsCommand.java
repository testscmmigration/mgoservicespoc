package com.moneygram.mgo.service.bailer.command;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.bailer.ProcessFirstTxBailerEventsRequest;
import com.moneygram.mgo.service.bailer.ProcessFirstTxBailerEventsResponse;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ConsumerServicev2Proxy;
import com.moneygram.mgo.service.bailer.consumerservice.proxy.ProfileEventType;
import com.moneygram.mgo.service.bailer.notification.NotificationServiceProxy;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;
import com.moneygram.mgo.service.consumer_v2.client.Consumer;
import com.moneygram.mgo.service.consumer_v2.client.ProfileEvent;

public class ProcessFirstTxBailerEventsCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			ProcessFirstTxBailerEventsCommand.class);
 
	public static final int KBA1_BAILER_DELAY_MINUTES = 0;
	public static final int KBA2_BAILER_DELAY_MINUTES = 0;
	public static final int RSA1_BAILER_DELAY_MINUTES = 0;
	public static final int RSA2_BAILER_DELAY_MINUTES = 0;
	public static final int TXN_REVIEW1_BAILER_DELAY_MINUTES = 0;
	public static final int TXN_REVIEW2_BAILER_DELAY_MINUTES = 0;

	@SuppressWarnings("rawtypes")
	private Map eventNotifyMap = null;	
	
	@SuppressWarnings("rawtypes")
	public Map getEventNotifyMap() {
        return eventNotifyMap;
    }

    @SuppressWarnings("rawtypes")
	public void setEventNotifyMap(Map eventNotifyMap) {
        this.eventNotifyMap = eventNotifyMap;
    }

    @Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof ProcessFirstTxBailerEventsRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		ProcessFirstTxBailerEventsRequest pbRequest = (ProcessFirstTxBailerEventsRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: ProcessFirstTxBailerEventsRequest: " + pbRequest.toString());
		}
		
		try {
			ConsumerServicev2Proxy csProxy = new ConsumerServicev2Proxy();
			Consumer consumer = csProxy.getConsumerProfile(pbRequest.getConsumerId(), pbRequest.getConsumerLoginId());
			if (consumer == null) {
				throw new CommandException("No consumer found to process");
			}
			
			String profileEventType = getProfileEventToSend(consumer);

			ProcessFirstTxBailerEventsResponse response = new ProcessFirstTxBailerEventsResponse();
			if (profileEventType != null) {
				Calendar currTime = Calendar.getInstance();
				
				//Add event to profile				
				csProxy.addProfileEvent(consumer.getConsumerId(), profileEventType, currTime);
				
				//Send email request to notification service
				NotificationServiceProxy nsProxy = new NotificationServiceProxy();				
				nsProxy.sendMessageToNotificationService((String)eventNotifyMap.get(profileEventType), consumer, currTime);
				response.setBailerEmailSent(true);
			} else {
				response.setBailerEmailSent(false);
			}
            return response;
			
		} catch (CommandException ce) {
			throw ce;
		}	catch (Exception e) {		
			logger.warn("process: Failure in ProcessFirstTxBailerEventsCommand", e);
			throw new CommandException("Failure in ProcessFirstTxBailerEventsCommand", e);
		}
	}

	private String getProfileEventToSend(Consumer consumer) {

		//Don't do bailer emails for blocked consumer
		if (consumer.getInternal().getConsumerBlocked()) {
			return null;
		}
		List<ProfileEvent> eventList = Arrays.asList(consumer.getProfileEvents());

		// If customer ever completed a txn or for some reason, they a txn start
		// event was never logged - don't process customer
		if (eventList == null || eventList.isEmpty()
			|| isEventTypeFoundInList(eventList, ProfileEventType.TXN_FirstTxn_Completed)
			|| !isEventTypeFoundInList(eventList, ProfileEventType.TXN_FirstTxn_Started) ) {
			return null;
		}
		
		sortEventListMostRecentFirst(eventList);

		ProfileEvent latestPE = eventList.get(0);
	
		if (consumer.getInternal().getConsumerStatus().equals("ACT") &&
			consumer.getInternal().getConsumerSubStatus().equals("L12")) {						
			
			if ( latestPE.getEvent().equals(ProfileEventType.TXN_FirstTxn_Started ) && 
				durationExpired(latestPE.getEventDateTime(), 
						        Integer.parseInt(MGOBailerServiceResourceConfig.getInstance().getSendMoneyBailer1DelayMinutes()))) {
				return ProfileEventType.SendMoney_Bailer1_Sent;
			}			
			if (latestPE.getEvent().equals(ProfileEventType.SendMoney_Bailer1_Sent) &&
				durationExpired(latestPE.getEventDateTime(), 
							    Integer.parseInt(MGOBailerServiceResourceConfig.getInstance().getSendMoneyBailer2DelayMinutes()))) {	
				return ProfileEventType.SendMoney_Bailer2_Sent;
			}	
			if (latestPE.getEvent().equals(ProfileEventType.Receiver_Information_Stored) &&
					durationExpired(latestPE.getEventDateTime(), 
								    Integer.parseInt(MGOBailerServiceResourceConfig.getInstance().getTxnReview1DelayMinutes()))) {	
				return ProfileEventType.TXN_Review_Bailer1_Sent;
			}
			if (latestPE.getEvent().equals(ProfileEventType.TXN_Review_Bailer1_Sent) &&
					durationExpired(latestPE.getEventDateTime(), 
								    Integer.parseInt(MGOBailerServiceResourceConfig.getInstance().getTxnReview2DelayMinutes()))) {	
				return ProfileEventType.TXN_Review_Bailer2_Sent;
			}
		}
		return null;
	}

	/**
	 * Returns true if eventDateTime + minutes > current datetime.  
	 * Note: If 'minutes' has value <= 0, false is returned
	 */
	protected static boolean durationExpired(Calendar eventDateTime, int minutes) {	
		if (minutes <= 0) {
			return false;
		}
		return Calendar.getInstance().getTimeInMillis() > eventDateTime.getTimeInMillis() + (1000L * 60L * minutes);
	}
	
	private boolean isEventTypeFoundInList(List<ProfileEvent> eventList,
			String searchPet) {
		for (ProfileEvent pe : eventList) {
			if (pe.getEvent() == null) {
				//boilerplating - consumer service is returning null events if it doesn't know the type of events
				return false;
			}
			if (pe.getEvent().equals(searchPet)) {
				return true;
			}
		}
		return false;
	}


	protected void sortEventListMostRecentFirst(List<ProfileEvent> eventList) {
		Collections.sort(eventList, new Comparator<ProfileEvent>() {
		    public int compare(ProfileEvent e1, ProfileEvent e2) {
		    	return e1.getEventDateTime().compareTo(e2.getEventDateTime()) * -1; //descending
		    }});
	}

}
