package com.moneygram.mgo.service.bailer.notification;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.moneygram.common.jms.JMSWriterCache;
import com.moneygram.common.jms.MessageWriter;
import com.moneygram.common.jms.MessagingException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.StringUtility;
import com.moneygram.mgo.service.bailer.JobCustomer;
import com.moneygram.mgo.service.bailer.OnboardCustomer;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;

public class NotificationServiceProxy 
{
    private static Logger log = LogFactory.getInstance().getLogger(NotificationServiceProxy.class);
    
//    public static final String NOTIFICATIONCONNECTIONFACTORY = "CONSUMER.NOTIFICATION.MSGING.REQ.QCF";
//    public static final String NOTIFICATIONQUEUE = "CONSUMER.NOTIFICATION.MSGING.REQ";
    
    /*
	 * TODO: These vars are hard-coded,but should be dynamic depending on the
	 * affiliate, the mechanism to make them dynamic is yet to be determined.
	 * Could be in the profiles.xml document, a database table, or possibly WAS
	 * resource environment variables.
	 */
    private static final String AFF_SENDER_EMAIL_ADDRESS = "no-reply.walmartmgo@moneygram.com";
    
	private static final String SENDER_EMAIL_ADDRESS = "no-reply.moneygramonline@moneygram.com"; // Today this one is a Context Resource
	private static final String CORPORATE_SITE_URL = "http://www.moneygram.com";
	private static final String CONSUMER_SITE_URL = "https://www.moneygram.com/moneygramonline";
	private static final String HELP_LINE_NUMBER = "1-800-922-7146";
	private static final String EMAILHEADER_LOGO_LINK = "http://www.moneygram.com/html/emailHeader.gif";
	private static final String AFFILIATE_NAME = "MoneyGram";
	private static final String AFFILIATE_SITE_NAME = "MoneyGramOnline";
	
	//SITES INDENTIFIERS
	public static final String SITEIDENTIFIER = "MGO";
    public static final String SITEIDENTIFIER_INLANE = "INLANE";
    public static final String SITEIDENTIFIER_WAP = "WAP"; // TODO is WAP correct?
    public static final String SITEIDENTIFIER_MGOUK = "MGOUK";
    public static final String SITEIDENTIFIER_MGODE = "MGODE";
    
    //MESSAGE TYPES
    public static final String KBAABANDONED = "KBA";
    public static final String KBAABANDONEDREMINDER = "KBARD";
    public static final String RSANOTENROLLED = "RSA";
    public static final String RSANOTENROLLEDREMINDER = "RSARD";
    public static final String REVIEWTRANSACTION = "RVTX";
    public static final String REVIEWTRANSACTIONREMINDER = "RVTXRD";
    public static final String AFF_KBAABANDONED = "AKBA";
    public static final String AFF_KBAABANDONEDREMINDER = "AKBARD";
    public static final String AFF_RSANOTENROLLED = "ARSA";
    public static final String AFF_RSANOTENROLLEDREMINDER = "ARSARD";
    public static final String AFF_REVIEWTRANSACTION = "ARVTX";
    public static final String AFF_REVIEWTRANSACTIONREMINDER = "ARVTXR";
    
    // ONBOARD EMAILS MESSAGE TYPES
    public static final String MGO_EMAIL_ONBOARDING_THKU_US_EN  = "OBTKU";
    public static final String MGO_EMAIL_ONBOARDING_AO_US_EN  = "OBAO";
    public static final String MGO_EMAIL_ONBOARDING_SMA_US_EN  = "OBSMA";
    
    //MESSAGE KEYS
    private static final String FIRSTNAME = "firstName";
    private static final String LASTNAME = "lastName";
    private static final String DATETIMESENT = "dateTimeSent";
    
    // ONBOARD EMAILS KEYS
    private static final String BILLPAYTRANTYPE = "EPSEND";
    private static final String DESTINATION = "destination";
    private static final String CREATEDATETIME = "createDateTime";
    private static final String RECEIVERFIRSTNAME = "receiverFirstName";
    private static final String RECEIVERLASTNAME = "receiverLastName";
    private static final String SENDAMOUNT = "sendAmount";
    private static final String SENDCURRENCYCODE = "sendCurrencyCode";
    
    // MESSAGE KEYS FOR AFFILIATE PROGRAM
    private static final String MSG_KEY_CONSUMER_SITE_URL = "consumerSiteURL";
    private static final String MSG_KEY_CONTACT_NUMBER = "contactNumber";
	private static final String MSG_KEY_AFFILIATE_LOGO = "affiliateLogo";
	private static final String MSG_KEY_AFFILIATE_NAME = "affiliateName";
	private static final String MSG_KEY_AFFILIATE_SITE_URL = "affiliateSiteUrl";
	private static final String MSG_KEY_AFFILIATE_SITE_NAME = "affiliateSiteName";
	
	//Added by Ankit Bhatt for 8873 - Repeat Bailer Mails
	public static final String MSG_KEY_REPEAT_BAILER_MAIL_1 = "RVTXR1";
	public static final String MSG_KEY_REPEAT_BAILER_MAIL_2 = "RVTXR2";
	//ended
    
    private static MessageWriter writer = null;
    private static String senderEmailAddress = null;
    
    private MessageWriter getMessageWriter() throws MessagingException {
    	if (writer == null) {
    		String notificationConnectionFactory = MGOBailerServiceResourceConfig.getInstance().getNotificationConnectionFactory();
    		String notificationQueue = MGOBailerServiceResourceConfig.getInstance().getNotificationQueue();
    		        	   
    		writer = JMSWriterCache.getInstance().getWriter(notificationConnectionFactory, notificationQueue, false);
    	}    	
    	return writer;    	
   }
   
    private String getAffiliateParameters()
    {
    	StringBuffer parms = new StringBuffer();
    	parms.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, CONSUMER_SITE_URL));
    	parms.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_URL, CORPORATE_SITE_URL));
    	parms.append(getContentParameter(MSG_KEY_CONTACT_NUMBER, HELP_LINE_NUMBER));
    	parms.append(getContentParameter(MSG_KEY_AFFILIATE_LOGO, EMAILHEADER_LOGO_LINK));
    	parms.append(getContentParameter(MSG_KEY_AFFILIATE_NAME, AFFILIATE_NAME));
    	parms.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_NAME, AFFILIATE_SITE_NAME));
    	return parms.toString();
    }

    public void sendMessageToNotificationService(String messageType,
    		com.moneygram.mgo.service.consumer_v1.client.Consumer consumer, Calendar dateTime) throws Exception {
    	if (isAffiliateSite(consumer.getSourceSite()))
    	{
    		senderEmailAddress = AFF_SENDER_EMAIL_ADDRESS;
    	}
    	else {
    		senderEmailAddress = MGOBailerServiceResourceConfig.getInstance().getSendEmailAddress();
    		log.debug("senderEmailAddress="+ senderEmailAddress);
    	}
    	if (senderEmailAddress == null) {
    		senderEmailAddress = SENDER_EMAIL_ADDRESS;
    	}
    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append("<MessageInfo>");
		messageBuffer.append(getMessageType(messageType, consumer.getSourceSite()));
		messageBuffer.append(getServices(consumer.getLoginId()));
		messageBuffer.append("<contentParameters>");
		messageBuffer.append(getContentParameter(FIRSTNAME, consumer.getPersonal().getFirstName()));
		messageBuffer.append(getContentParameter(LASTNAME, consumer.getPersonal().getLastName()));
		messageBuffer.append(getContentParameter(DATETIMESENT, format.format(dateTime.getTime())));
		messageBuffer.append(getAffiliateParameters());
		messageBuffer.append("</contentParameters>");
		messageBuffer.append("</MessageInfo>");
		String message = messageBuffer.toString();
		writeMessageToQueue(message);
	}
    
    private String getContentParameter(String key, String value) 
    {
        StringBuffer contentParameter = new StringBuffer();
        contentParameter.append("<contentParameter>");
            contentParameter.append("<name>");
            contentParameter.append(key);
            contentParameter.append("</name>");
            contentParameter.append("<value>");
            contentParameter.append(value);
            contentParameter.append("</value>");
        contentParameter.append("</contentParameter>");
        return contentParameter.toString();
    }
    
    private boolean isAffiliateSite(String siteId)
    {
    	if (siteId != null && !siteId.equals(SITEIDENTIFIER) && !siteId.equals(SITEIDENTIFIER_INLANE) 
    			&& !siteId.equals(SITEIDENTIFIER_MGOUK) && !SITEIDENTIFIER_MGODE.equals(siteId)){
    		return true;
    	}
    	return false;
    }
    
    private String getMessageType(String messageTypeCode, String siteId){
        return getMessageType(messageTypeCode, siteId, null);
    }
    
    private String getLanguageCodeForSite(String siteId, String defaultLanguageCode) {
		String languageCode = "";
    	if(SITEIDENTIFIER_MGOUK.equals(siteId)){
    		languageCode = "en-GB";
    	}
    	else if(SITEIDENTIFIER_MGODE.equals(siteId)){
    		languageCode = !StringUtility.isNullOrEmpty(defaultLanguageCode) ? defaultLanguageCode : "de-DE";
    	}
    	else{
    		languageCode = "en-US";
    	}
		return languageCode;
	}

	private String getCountryCodeForSite(String siteId) {
    	String countryCode = "";
    	if(SITEIDENTIFIER_MGOUK.equals(siteId)){
    		countryCode = "GBR";
    	}
    	else if(SITEIDENTIFIER_MGODE.equals(siteId)){
    		countryCode = "DEU";
    	}
    	else{
    		countryCode = "USA";
    	}
		return countryCode;
	}

	private String getAffiliateMessageType(String msgType) {
    	if (msgType.equals(KBAABANDONED)) {
    		return AFF_KBAABANDONED;
    	}
    	if (msgType.equals(KBAABANDONEDREMINDER)) {
    		return AFF_KBAABANDONEDREMINDER;
    	}
    	if (msgType.equals(RSANOTENROLLED)) {
    		return AFF_RSANOTENROLLED;
    	}
    	if (msgType.equals(RSANOTENROLLEDREMINDER)) {
    		return AFF_RSANOTENROLLEDREMINDER;
    	}
    	if (msgType.equals(REVIEWTRANSACTION)) {
    		return AFF_REVIEWTRANSACTION;
    	}
    	if (msgType.equals(REVIEWTRANSACTIONREMINDER)) {
    		return AFF_REVIEWTRANSACTIONREMINDER;
    	}
    	return msgType;
    }
    
    private String getServices(String emailAddress) 
    {
        StringBuffer services = new StringBuffer();
        services.append("<services>");
            services.append("<service type=\"E-MAIL\">");
                services.append("<sender>");
                    services.append(senderEmailAddress);
                services.append("</sender>");
                services.append("<receiver>");
                    services.append(emailAddress);
                services.append("</receiver>");
            services.append("</service>");
        services.append("</services>");
        return services.toString();
    }
    
    private void writeMessageToQueue(String message) throws Exception
    {       
        getMessageWriter().writeTextMessage(message);
        log.debug("Message sent successfully to Notification Queue:" + message);
    }
    
    
    /**************************************************************************
     * Overloaded method to use com.moneygram.mgo.service.consumer_v2.Consumer
     **************************************************************************/
    
    public void sendMessageToNotificationService(String messageType,
    		com.moneygram.mgo.service.consumer_v2.client.Consumer consumer, Calendar dateTime) throws Exception {
    	
    	String siteId = consumer.getSourceSite();
    	String defaultLanguageCode = null;
    	if(consumer.getContact() != null && consumer.getContact().getPrefCommLanguage() != null){
    		defaultLanguageCode = consumer.getContact().getPrefCommLanguage().getLanguageTagText();
    	}
    	
		if (isAffiliateSite(siteId)){
    		senderEmailAddress = AFF_SENDER_EMAIL_ADDRESS;
    	}
		else {
    		senderEmailAddress = MGOBailerServiceResourceConfig.getInstance().getSendEmailAddress();
    		log.debug("senderEmailAddress="+ senderEmailAddress);
    	}
    	if (senderEmailAddress == null) {
    		senderEmailAddress = SENDER_EMAIL_ADDRESS;
    	}
    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append("<MessageInfo>");
		messageBuffer.append(getMessageType(messageType, siteId, defaultLanguageCode));
		messageBuffer.append(getServices(consumer.getLoginId()));
		messageBuffer.append("<contentParameters>");
		messageBuffer.append(getContentParameter(FIRSTNAME, consumer.getPersonal().getFirstName()));
		messageBuffer.append(getContentParameter(LASTNAME, consumer.getPersonal().getLastName()));
		messageBuffer.append(getContentParameter(DATETIMESENT, format.format(dateTime.getTime())));
		messageBuffer.append(getAffiliateParameters());
		messageBuffer.append("</contentParameters>");
		messageBuffer.append("</MessageInfo>");
		String message = messageBuffer.toString();
		writeMessageToQueue(message);
	}
    
    /**************************************************************************
     * Overloaded method for Onboard emails
     **************************************************************************/
    
    public void sendMessageToNotificationService(String messageType,
    		OnboardCustomer onboardCustomer, Calendar dateTime) throws Exception {
    	String receiverFirstName;
    	String receiverLastName;
    	if (senderEmailAddress == null) {
    		senderEmailAddress = SENDER_EMAIL_ADDRESS;
    	}
    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append("<MessageInfo>");
		messageBuffer.append(getMessageType(messageType, "MGO"));
		messageBuffer.append(getServices(onboardCustomer.getEmailID()));
		messageBuffer.append("<contentParameters>");
		messageBuffer.append(getContentParameter(FIRSTNAME, onboardCustomer.getCustomerFirstName()));
		messageBuffer.append(getContentParameter(LASTNAME, onboardCustomer.getCustomerLastName()));
		messageBuffer.append(getContentParameter(CREATEDATETIME, format.format(dateTime.getTime())));
		messageBuffer.append(getContentParameter(DESTINATION, onboardCustomer.getReceiverCountry()));
		if(onboardCustomer.getTranType().equals(BILLPAYTRANTYPE)) { 
			receiverFirstName = onboardCustomer.getRcv_agent_name();
			receiverLastName = "";
		} else {
			receiverFirstName = onboardCustomer.getReceiverFirstName();
			receiverLastName = onboardCustomer.getReceiverLastName();
		}
		messageBuffer.append(getContentParameter(RECEIVERFIRSTNAME, receiverFirstName));
		messageBuffer.append(getContentParameter(RECEIVERLASTNAME, receiverLastName));
		messageBuffer.append(getContentParameter(SENDAMOUNT, onboardCustomer.getSendFaceAmount().toString()));
		messageBuffer.append(getContentParameter(SENDCURRENCYCODE, onboardCustomer.getSendCurrency()));
		messageBuffer.append("</contentParameters>");
		messageBuffer.append("</MessageInfo>");
		String message = messageBuffer.toString();
		writeMessageToQueue(message);
	}
    
    private String getMessageType(String messageTypeCode, String siteId, String defaultLanguageCode) 
    {
    	if (isAffiliateSite(siteId))
    	{
    		String oldMsgType = messageTypeCode;
    		messageTypeCode = getAffiliateMessageType(messageTypeCode);
    		log.debug("Affiliate Program - Old MessageType: " + oldMsgType + ", New MessageType: " + messageTypeCode);
    	}
        StringBuffer messageType = new StringBuffer();
        messageType.append("<messageType>");
            messageType.append("<type>");
            messageType.append(messageTypeCode);
            messageType.append("</type>");
            messageType.append("<subtype>1</subtype>");
            messageType.append("<sourceSystem>MGO</sourceSystem>");
            messageType.append("<countryCode>");
            //messageType.append("USA");
            messageType.append(getCountryCodeForSite(siteId));
            messageType.append("</countryCode>");
            messageType.append("<languageCode>");
           // messageType.append("en-US");
            messageType.append(getLanguageCodeForSite(siteId, defaultLanguageCode));
            messageType.append("</languageCode>");
        messageType.append("</messageType>");
        return messageType.toString();
    }
    
    //added by Ankit Bhatt for 8873 - Repeat Bailer Transaction Flow
    public void sendMessageToNotificationService(String messageType,
    		JobCustomer jobCustomer) throws Exception {    	
    	if (senderEmailAddress == null) {
    		senderEmailAddress = SENDER_EMAIL_ADDRESS;
    	}
    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append("<MessageInfo>");
		messageBuffer.append(getMessageType(messageType, "MGO"));
		messageBuffer.append(getServices(jobCustomer.getCustLogonId()));
		messageBuffer.append("<contentParameters>");
		//as per template MGO_EMAIL_RVTXR1_US_EN and MGO_EMAIL_RVTXR2_US_EN only First Name is required.
		messageBuffer.append(getContentParameter(FIRSTNAME, jobCustomer.getCustomerFirstName()));
		//messageBuffer.append(getContentParameter(LASTNAME, jobCustomer.getCustomerLastName()));						
		messageBuffer.append("</contentParameters>");
		messageBuffer.append("</MessageInfo>");
		String message = messageBuffer.toString();
		writeMessageToQueue(message);
	}
    
    //ended by Ankit Bhatt for 8873
    
}