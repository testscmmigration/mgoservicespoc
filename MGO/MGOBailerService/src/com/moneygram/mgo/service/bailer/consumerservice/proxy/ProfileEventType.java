/**
 * ProfileEventType.java
 *
 */

package com.moneygram.mgo.service.bailer.consumerservice.proxy;

/**
 * 
 * Profile Event Type.
 * Defines a set of event types used by the bailer process.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOBailerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/07/23 14:04:27 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class ProfileEventType {
    public static final java.lang.String KBA_Bailer1_Sent = "KBA_BAILER1_SENT";
    public static final java.lang.String KBA_Bailer2_Sent = "KBA_BAILER2_SENT";
    public static final java.lang.String KBA_Passed = "KBA_PASSED";
    public static final java.lang.String RSA_Bailer1_Sent = "RSA_BAILER1_SENT";
    public static final java.lang.String RSA_Bailer2_Sent = "RSA_BAILER2_SENT";
    public static final java.lang.String TXN_Review_Bailer1_Sent = "TXN_REVIEW_BAILER1_SENT";
    public static final java.lang.String TXN_Review_Bailer2_Sent = "TXN_REVIEW_BAILER2_SENT";
    public static final java.lang.String TXN_FirstTxn_Started = "TXN_FIRSTTXN_STARTED";
    public static final java.lang.String TXN_FirstTxn_Completed = "TXN_FIRSTTXN_COMPLETED";
    public static final java.lang.String Password_Changed = "PASSWORD_CHANGED";
    public static final java.lang.String Profile_Unlocked = "PROFILE_UNLOCKED";
    public static final java.lang.String User_Profile_Created = "USER_PROFILE_CREATED";
    public static final java.lang.String SendMoney_Bailer1_Sent = "SENDMONEY_BAILER1_SENT";
    public static final java.lang.String SendMoney_Bailer2_Sent = "SENDMONEY_BAILER2_SENT";
    public static final java.lang.String Receiver_Information_Stored = "RECEIVER_INFORMATION_STORED";
    
    //added by Ankit Bhatt for 8873 - Repeat Bailer Flow
    public static final String REPEAT_BAILER_MAIL1_SENT = "REPEAT_TXN_BAILER1_SENT";
    public static final String REPEAT_BAILER_MAIL2_SENT = "REPEAT_TXN_BAILER2_SENT";
    //ended
    
}
