package com.moneygram.mgo.service.bailer.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.bailer.JobCustomer;
import com.moneygram.mgo.service.bailer.OnboardCustomer;

public class BailerDAO extends BaseDAO {
    private static final Logger logger = LogFactory.getInstance().getLogger(
            BailerDAO.class);

    private static final String DB_CALL_TYPE_CODE = "WEB";
    private static final String DB_CALLER_ID = "MGO";
    private static final String GET_ONBOARD_CUST_PROC = "pkg_mgo_cust_profile.prc_get_onboard_cust_cv";
    
    //added by Ankit Bhatt for 8873 - Repeat Bailer Transaction
    private static final String GET_REPEAT_BAILER_CUST = " {call pkg_mgo_cust_profile.prc_get_rept_tran_bailer_candt (?,?,?,?,?,?)}";   
    //ended for 8873

	/**
	 * Returns procedure log id.
	 * 
	 * @param result
	 * @return procedure log id
	 */
//	private Number getProcedureLogId(Map<?, ?> result) {
//		return (Number) result.get("ov_prcs_log_id");
//	}
//
//	/**
//	 * Returns list of consumers.
//	 * 
//	 * @param consumerId
//	 * @param consumerLoginId
//	 * @return list of consumers
//	 */
//	@SuppressWarnings("unchecked")
//	public Consumer getConsumer(Long consumerId, String consumerLoginId)
//			throws DAOException {
//		if (consumerId == null && consumerLoginId == null)
//			throw new DAOException(
//					"Invalid consumerId/consumerLoginId provided");
//
//		SimpleJdbcCall getConsumers = new SimpleJdbcCall(getJdbcTemplate())
//				.withProcedureName(GET_CUSTOMER_PROC)
//				.withoutProcedureColumnMetaDataAccess()
//				.declareParameters(
//						new SqlParameter("iv_user_id", Types.VARCHAR),
//						new SqlParameter("iv_call_type_code", Types.VARCHAR),
//						new SqlParameter("iv_cust_id", Types.INTEGER),
//						new SqlParameter("iv_cust_logon_id", Types.VARCHAR),
//						new SqlParameter("iv_cust_stat_code", Types.VARCHAR),
//						new SqlParameter("iv_cust_sub_stat_code", Types.VARCHAR),
//						new SqlParameter("iv_cust_gu_id", Types.VARCHAR),
//						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
//						new SqlOutParameter("ov_customer_cv",
//								OracleTypes.CURSOR, new ConsumerRowMapper()));
//
//		MapSqlParameterSource parameters = new MapSqlParameterSource();
//		parameters.addValue("iv_user_id", DB_CALLER_ID);
//		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
//		parameters.addValue("iv_cust_id", consumerId);
//		parameters.addValue("iv_cust_logon_id", consumerLoginId);
//		parameters.addValue("iv_cust_stat_code", null);
//		parameters.addValue("iv_cust_sub_stat_code", null);
//		parameters.addValue("iv_cust_gu_id", null);
//
//		Map<?, ?> result = getConsumers.execute(parameters);
//		List<Consumer> consumers = (List<Consumer>) result
//				.get("ov_customer_cv");
//
//		if (logger.isDebugEnabled()) {
//			if (consumers == null)
//				logger.debug("getConsumer: returned list is null");
//			else
//				logger.debug("getConsumer: returned list size is "
//						+ consumers.size());
//			logger.debug("getConsumer: procedure log id="
//					+ getProcedureLogId(result));
//		}
//
//		if (consumers == null || consumers.size() != 1)
//			return null;
//		return consumers.get(0);
//	}
	/**
	 * getOnboardCustomers returns onboardCustomers List
	 * @param call_type
	 * @param userID
	 * @param siteParam
	 * @param nbrValue
	 * @return onboardCustomerList
	 * @throws DAOException
	 */
	public List<OnboardCustomer> getOnboardCustomers(int siteParam, int nbrDaysBack) throws DAOException {
		
		if (siteParam == 0)
			throw new DAOException("Invalid consumerId/loginId provided");
		
		SimpleJdbcCall getOnboardCustomers = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_ONBOARD_CUST_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_src_web_site_code", Types.INTEGER),
						new SqlParameter("iv_send_nbr_days_back", Types.NUMERIC),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_onboard_cust_cv",
								OracleTypes.CURSOR,
								new OnboardCustomerRowMapper()));
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_src_web_site_code", siteParam);
		parameters.addValue("iv_send_nbr_days_back", nbrDaysBack);
	    	Map<String, Object> result = getOnboardCustomers.execute(parameters);
			List<OnboardCustomer> onboardCustomerList = (List<OnboardCustomer>) result.get("ov_onboard_cust_cv");

		return onboardCustomerList;
	}
	
	//Method added by Ankit Bhatt for 8873 - Repeat Bailer
	public List<JobCustomer> getRepeatBailerCustomers(int nbrMinutes, String firstTime)
					throws DAOException,SQLException{
		
		List<JobCustomer> jobCustList = new ArrayList<JobCustomer>();
		//Callable statement to execute Procedure
		CallableStatement callableSt = getJdbcTemplate().getDataSource().getConnection().prepareCall(GET_REPEAT_BAILER_CUST);
		callableSt.setString("iv_user_id", DB_CALLER_ID);
		callableSt.setString("iv_call_type_code", DB_CALL_TYPE_CODE);
		callableSt.setFloat("iv_nbr_minutes_old", nbrMinutes);
		callableSt.setString("iv_first_run",firstTime);
		callableSt.registerOutParameter("ov_prcs_log_id", Types.INTEGER);
		callableSt.registerOutParameter("ov_rept_tran_bailer_cand_cv", OracleTypes.CURSOR);

		//Call Stored Procedure
		callableSt.execute();
		ResultSet rs = (ResultSet)callableSt.getObject("ov_rept_tran_bailer_cand_cv");

		//Metadata display for ResultSet
		/*while(rs.next()){
			System.out.println("1 " + rs.getString("CUST_ID") + " " + rs.getMetaData().getColumnName(1));
			System.out.println("1 " + rs.getString("CUST_LOGON_ID") + " " + rs.getMetaData().getColumnName(2));
			System.out.println("1 " + rs.getString("CUST_LAST_NAME") +  " " + rs.getMetaData().getColumnName(3));
			System.out.println("1 " + rs.getString("CUST_FRST_NAME") + " " + rs.getMetaData().getColumnName(4));
		}*/
		
		while(rs.next()){
			long custId = rs.getString("CUST_ID")!=null ? Long.valueOf(rs.getString("CUST_ID")) : 0;
			String custLogonId = rs.getString("CUST_LOGON_ID")!=null ? rs.getString("CUST_LOGON_ID") : null;
			String custLastName = rs.getString("CUST_LAST_NAME")!=null ? rs.getString("CUST_LAST_NAME") : null;
			String custFirstName = rs.getString("CUST_FRST_NAME")!=null ? rs.getString("CUST_FRST_NAME") : null;
			
			if(custId!=0 && custLogonId!=null) {
				JobCustomer jobCustomer = new JobCustomer();
				jobCustomer.setCustId(custId);
				jobCustomer.setCustLogonId(custLogonId);
				jobCustomer.setCustomerLastName(custLastName);
				jobCustomer.setCustomerFirstName(custFirstName);
				jobCustList.add(jobCustomer);
			}			
		}		
		return jobCustList;
		
	}
	
}