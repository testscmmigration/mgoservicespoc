package com.moneygram.mgo.service.bailer;

import java.math.BigDecimal;
import java.util.Calendar;

public class OnboardCustomer {

	private String emailID;
	private Long customerID;
	private String customerFirstName;
	private String customerLastName;
	private String CustomerLangPref;
	private String tranType;
	private Calendar createDateTime;
	private BigDecimal sendFaceAmount;
	private String sendCurrency;
	private String receiverFirstName;
	private String receiverLastName;
	private String receiverCountry;
	private String rcv_agent_name;

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public Long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Long customerID) {
		this.customerID = customerID;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerLangPref() {
		return CustomerLangPref;
	}

	public void setCustomerLangPref(String customerLangPref) {
		CustomerLangPref = customerLangPref;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public Calendar getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Calendar createDateTime) {
		this.createDateTime = createDateTime;
	}

	public BigDecimal getSendFaceAmount() {
		return sendFaceAmount;
	}

	public void setSendFaceAmount(BigDecimal sendFaceAmount) {
		this.sendFaceAmount = sendFaceAmount;
	}

	public String getSendCurrency() {
		return sendCurrency;
	}

	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public String getRcv_agent_name() {
		return rcv_agent_name;
	}

	public void setRcv_agent_name(String rcv_agent_name) {
		this.rcv_agent_name = rcv_agent_name;
	}

}