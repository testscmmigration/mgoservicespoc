package com.moneygram.mgo.service.bailer.command;

import java.util.HashMap;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.action.MGOJobSeviceAction;
import com.moneygram.mgo.service.action.OnBoarding;
import com.moneygram.mgo.service.bailer.dao.BailerDAO;
import com.moneygram.mgo.service.bailer_v1.ProcessActionRequest;
import com.moneygram.mgo.service.bailer_v1.ProcessActionResponse;
import com.moneygram.mgo.service.bailer_v1.ProcessParamType;

public class ProcessActionCommand extends ReadCommand {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ProcessActionCommand.class);
	String processingText;

	/**
	 * 
	 * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
	 */
	protected OperationResponse process(OperationRequest request)
			throws CommandException {

		logger.info("Inside Process Action - process method");
		ProcessActionRequest pARequest = (ProcessActionRequest) request;
		if (logger.isDebugEnabled()) {
			logger.debug("process: request=" + request);
		}
		String processingText = "";
		ProcessActionResponse paResponse = new ProcessActionResponse();
		try {
			Object instanceObject =null;
			Class instanceClass = Class.forName("com.moneygram.mgo.service.action."+pARequest.getAction());
			instanceObject = instanceClass.newInstance();
			if (logger.isDebugEnabled()) {
				logger.debug("process: instanceClass=" + instanceClass.getName());
			}
		
			//changes made by Ankit bhatt for 8873
			//removing if condition for making it generic for other classes also.
			//if (instanceObject instanceof OnBoarding) {
				BailerDAO bailerDAO = (BailerDAO)getDataAccessObject();
				processingText = ((MGOJobSeviceAction) instanceObject).process(bailerDAO);
			//}
			paResponse.setProcessingText(processingText);
			return paResponse;
		} catch (Exception exp) {
			processingText = "FAILURE";
			logger.error("process: Failure in ProcessActionCommand", exp);
			throw new CommandException("Failure in ProcessActionCommand",
					exp);
		}

	}

	private HashMap getProcessParamType(ProcessParamType[] processParamType)
			throws CommandException {

		if (processParamType == null || processParamType.length == 0) {
			throw new DataFormatException(
					"At least one ProcessParamType is required");
		}
		HashMap hM = new HashMap();
		for (int i = 0; i < processParamType.length; i++) {
			if (i != 0) {
				hM.put(processParamType[i].getParamName(),
						processParamType[i].getParamValue());
			}

		}
		return hM;
	}

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof ProcessActionRequest;
	}

}
