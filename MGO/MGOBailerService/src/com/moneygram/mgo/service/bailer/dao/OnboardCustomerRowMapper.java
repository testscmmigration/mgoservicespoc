package com.moneygram.mgo.service.bailer.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.mgo.service.bailer.OnboardCustomer;

/**
 * @author ws03
 * OnboardCustomerRowMapper Class to map proc response to OnboardCustomer class
 */
public class OnboardCustomerRowMapper extends BaseRowMapper {

	/**
	 * mapRow This method is used to map proc response to OnboardCustomer class
	 * @param ResultSet
	 * @param int
	 * @return onboardCustomer
	 */
	public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		OnboardCustomer onboardCustomer = new OnboardCustomer();
		onboardCustomer.setEmailID(resultSet.getString("CUST_LOGON_ID"));
		onboardCustomer.setCustomerID(new Long(resultSet.getLong("CUST_ID")));
		onboardCustomer.setCustomerFirstName(resultSet.getString("CUST_FRST_NAME"));
		onboardCustomer.setCustomerLastName(resultSet.getString("CUST_LAST_NAME"));
		onboardCustomer.setCustomerLangPref(resultSet.getString("CUST_LANG_PREF"));
		onboardCustomer.setTranType(resultSet.getString("EMG_TRAN_TYPE_CODE"));
		Calendar calendar;
		Date date = resultSet.getDate("TRAN_CREATE_DATE");
		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);

			onboardCustomer.setCreateDateTime(calendar);
		}
		onboardCustomer.setSendFaceAmount(resultSet.getBigDecimal("SND_FACE_AMT"));
		onboardCustomer.setSendCurrency(resultSet.getString("SND_ISO_CRNCY_ID"));
		onboardCustomer.setReceiverFirstName(resultSet.getString("RCV_CUST_FRST_NAME"));
		onboardCustomer.setReceiverLastName(resultSet.getString("RCV_CUST_LAST_NAME"));
		onboardCustomer.setReceiverCountry(resultSet.getString("RCV_CNTRY_NAME"));
		onboardCustomer.setRcv_agent_name(resultSet.getString("RCV_AGENT_NAME"));

		return onboardCustomer;
	}
}