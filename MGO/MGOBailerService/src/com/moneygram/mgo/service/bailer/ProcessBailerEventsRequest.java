package com.moneygram.mgo.service.bailer;

import com.moneygram.common.service.BaseOperationRequest;

public class ProcessBailerEventsRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private java.lang.Long consumerId;

    private java.lang.String consumerLoginId;

    public ProcessBailerEventsRequest() {
    }

    /**
     * Gets the consumerId value for this ProcessBailerEventsRequest.
     * 
     * @return consumerId
     */
    public java.lang.Long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this ProcessBailerEventsRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(java.lang.Long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the consumerLoginId value for this ProcessBailerEventsRequest.
     * 
     * @return consumerLoginId
     */
    public java.lang.String getConsumerLoginId() {
        return consumerLoginId;
    }


    /**
     * Sets the consumerLoginId value for this ProcessBailerEventsRequest.
     * 
     * @param consumerLoginId
     */
    public void setConsumerLoginId(java.lang.String consumerLoginId) {
        this.consumerLoginId = consumerLoginId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessBailerEventsRequest)) return false;
        ProcessBailerEventsRequest other = (ProcessBailerEventsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.consumerLoginId==null && other.getConsumerLoginId()==null) || 
             (this.consumerLoginId!=null &&
              this.consumerLoginId.equals(other.getConsumerLoginId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getConsumerLoginId() != null) {
            _hashCode += getConsumerLoginId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[consumerId=").append(getConsumerId());
		buffer.append(" consumerLogonId=").append(getConsumerLoginId());
		buffer.append(" ]");
		return buffer.toString();
	}

}
