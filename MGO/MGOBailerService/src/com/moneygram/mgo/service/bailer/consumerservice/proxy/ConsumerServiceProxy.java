package com.moneygram.mgo.service.bailer.consumerservice.proxy;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.mgo.consumer_v1.common_v1.Header;
import com.moneygram.mgo.consumer_v1.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.bailer.util.MGOBailerServiceResourceConfig;
import com.moneygram.mgo.service.consumer_v1.client.Consumer;
import com.moneygram.mgo.service.consumer_v1.client.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v1.client.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v1.client.MGOConsumerServicePortType;
import com.moneygram.mgo.service.consumer_v1.client.MGOConsumerServicePortTypeProxy;
import com.moneygram.mgo.service.consumer_v1.client.ProfileEvent;
import com.moneygram.mgo.service.consumer_v1.client.ProfilePart;
import com.moneygram.mgo.service.consumer_v1.client.SaveProfileEventRequest;
import com.moneygram.mgo.service.consumer_v1.client.SaveProfileEventResponse;
import com.moneygram.mgo.service.consumer_v1.client.ServiceAction;

public class ConsumerServiceProxy
{
        private static final Logger log = LogFactory.getInstance().getLogger(ConsumerServiceProxy.class);
        private MGOConsumerServicePortType consumerServiceClient;
        
        private MGOConsumerServicePortType getConsumerServiceClient() throws Exception 
        {
            if(consumerServiceClient != null)
            {
                return consumerServiceClient;
            }
            else
            {
                try 
                {
                    String url = MGOBailerServiceResourceConfig.getInstance().getConsumerServiceURL();
                    consumerServiceClient = new MGOConsumerServicePortTypeProxy(url);
                    return consumerServiceClient;
                } 
                catch (Exception e) 
                {
                    throw new CommandException("Failed to create ConsumerServiceClient:" + e.getMessage(),e);
                }
            }
        }
   
	public Consumer getConsumerProfile(Long consumerId, String userId)
			throws CommandException {
		
		Consumer consumer = null;
		GetConsumerProfileRequest request = new GetConsumerProfileRequest();

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.getConsumerProfile.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);
		if (consumerId != null) {
			request.setConsumerId(consumerId);
		} else if (userId != null) {
			request.setConsumerLoginId(userId);
		} else {
			throw new CommandException(
					"Can't retrieve Consumer profile - no consumerId or userId values");
		}

		ProfilePart[] profilePart = new ProfilePart[3];
		profilePart[0] = ProfilePart.PersonalInfo;
		profilePart[1] = ProfilePart.Internal;
		profilePart[2] = ProfilePart.ProfileEvents;
		
		request.setResponseFilter(profilePart);
		try {
			GetConsumerProfileResponse response = getConsumerServiceClient().get(request);
			if (response != null) {
				consumer = response.getConsumer();
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new CommandException("Call to getConsumerProfile failed", e);
		}
		return consumer;
	}

	public void addProfileEvent(Long consumerId, String eventType, Calendar eventDateTime)
			throws CommandException {
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.saveProfileEvent.getValue());

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		SaveProfileEventRequest request = new SaveProfileEventRequest();
		request.setHeader(header);
		request.setConsumerId(consumerId.longValue());
		
		ProfileEvent event = new ProfileEvent();
		event.setEvent(eventType);
		if (eventDateTime != null) {
			event.setEventDateTime(eventDateTime);
		} else {
			event.setEventDateTime(Calendar.getInstance());
		}
		request.setEvent(event);

		try {
			SaveProfileEventResponse response = getConsumerServiceClient().saveProfileEvent(request);
			if (response == null) {
				throw new CommandException ("Call to saveProfileEvent resulted in null response");
			}
		} catch (Exception e) {
			throw new CommandException("Call to saveProfileEvent failed", e);
		}

	}
}
