/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.mgo.service.bailer.util;

import com.moneygram.common.service.util.ResourceConfig;

/**
 * 
 * MGO Bailer Service Resource Config. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOBailerService</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.2 $ $Date: 2009/12/23 18:56:16 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: w162 $ </td>
 *        </table>
 *        </div>
 */
public class MGOBailerServiceResourceConfig extends ResourceConfig {

	public static final String MGO_CONSUMER_SERVICE_URL = "MGO_CONSUMER_SERVICE_URL";
	public static final String MGO_CONSUMER_SERVICE_V2_URL = "MGO_CONSUMER_SERVICE_V2_URL";	//New
	public static final String MGO_CONSUMER_SERVICE_TIMEOUT = "MGO_CONSUMER_SERVICE_TIMEOUT";
	public static final String BAILER_SEND_EMAIL_ADDRESS = "BAILER_SEND_EMAIL_ADDRESS";
	public static final String NOTIFICATION_QUEUE="NOTIFICATION_QUEUE";
	public static final String NOTIFICATION_CONNECTION_FACTORY="NOTIFICATION_CONNECTION_FACTORY";
	public static final String KBA1_BAILER_DELAY_MINUTES="KBA1_BAILER_DELAY_MINUTES";
	public static final String KBA2_BAILER_DELAY_MINUTES="KBA2_BAILER_DELAY_MINUTES";
	public static final String RSA1_BAILER_DELAY_MINUTES="RSA1_BAILER_DELAY_MINUTES";
	public static final String RSA2_BAILER_DELAY_MINUTES="RSA2_BAILER_DELAY_MINUTES";
	public static final String TXN_REVIEW1_BAILER_DELAY_MINUTES="TXN_REVIEW1_BAILER_DELAY_MINUTES";
	public static final String TXN_REVIEW2_BAILER_DELAY_MINUTES="TXN_REVIEW2_BAILER_DELAY_MINUTES";
	
	public static final String SENDMONEY_BAILER1_DELAY_MINUTES = "SENDMONEY_BAILER1_DELAY_MINUTES"; //New
	public static final String SENDMONEY_BAILER2_DELAY_MINUTES = "SENDMONEY_BAILER2_DELAY_MINUTES"; //New
	
	//Onboard emails
	public static final String MGOJOB_ONBOARDING_NBRDAYS_THANKYOU = "MGOJOB_ONBOARDING_NBRDAYS_THANKYOU";
	public static final String MGOJOB_ONBOARDING_NBRDAYS_ACCOUNTOVERVIEW = "MGOJOB_ONBOARDING_NBRDAYS_ACCOUNTOVERVIEW";
	public static final String MGOJOB_ONBOARDING_NBRDAYS_SENDMONEYAGAIN = "MGOJOB_ONBOARDING_NBRDAYS_SENDMONEYAGAIN";
	
	public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOConsumerServiceResourceReference";
    public static final String NBR_HOURS_BACK_TO_PROCESS = "nbrHoursBackToProcess";
    
    
    //added by Ankit Bhatt for 8873
    public static final String NBR_HOURS_BAILER_MAIL_1 = "MGOJOB_REPEATBAILER_BAILER1_NBRMINUTES";
    public static final String NBR_HOURS_BAILER_MAIL_2 = "MGOJOB_REPEATBAILER_BAILER2_NBRMINUTES";
    //ended for 8873
	/**
	 * singleton instance.
	 */
	private static MGOBailerServiceResourceConfig instance = null;

	/**
	 * Creates new instance of ResourceConfig
	 * 
	 */
	private MGOBailerServiceResourceConfig() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static MGOBailerServiceResourceConfig getInstance() {
		if (instance == null) {
			synchronized (MGOBailerServiceResourceConfig.class) {
				if (instance == null) {
					instance = new MGOBailerServiceResourceConfig();
					instance.initResourceConfig();
				}
			}
		}
		return instance;
	}
	
	public String getConsumerServiceURL() {
		return getAttributeValue(MGO_CONSUMER_SERVICE_URL);
	}
	
	public String getConsumerServicev2URL() {
		return getAttributeValue(MGO_CONSUMER_SERVICE_V2_URL);
	}

	public int getConsumerServiceTimeOut() {
		return getIntegerAttributeValue(MGO_CONSUMER_SERVICE_TIMEOUT, 15000);
	}

	public String getSendEmailAddress() {
		return getAttributeValue(BAILER_SEND_EMAIL_ADDRESS);
	}
	
	public String getNotificationConnectionFactory() {
		return getAttributeValue(NOTIFICATION_CONNECTION_FACTORY);
	}
	
	public String getNotificationQueue() {
		return getAttributeValue(NOTIFICATION_QUEUE);
	}

	public String getKBA1DelayMinutes() {
		return getAttributeValue(KBA1_BAILER_DELAY_MINUTES);
	}
	
	public String getKBA2DelayMinutes() {
		return getAttributeValue(KBA2_BAILER_DELAY_MINUTES);
	}

	public String getRSA1DelayMinutes() {
		return getAttributeValue(RSA1_BAILER_DELAY_MINUTES);
	}

	public String getRSA2DelayMinutes() {
		return getAttributeValue(RSA2_BAILER_DELAY_MINUTES);
	}
	
	public String getTxnReview1DelayMinutes() {
		return getAttributeValue(TXN_REVIEW1_BAILER_DELAY_MINUTES);
	}	
	
	public String getTxnReview2DelayMinutes() {
		return getAttributeValue(TXN_REVIEW2_BAILER_DELAY_MINUTES);
	}		
	
	public String getSendMoneyBailer1DelayMinutes(){
		return getAttributeValue(SENDMONEY_BAILER1_DELAY_MINUTES);
	}
	
	public String getSendMoneyBailer2DelayMinutes(){
		return getAttributeValue(SENDMONEY_BAILER2_DELAY_MINUTES);
	}
	
	@Override
	protected String getResourceConfigurationJndiName() {
		return RESOURCE_REFERENCE_JNDI;
	}
	
	public int getJobServiceTimeoutMinutes() {
		// TODO Auto-generated method stub
		return getIntegerAttributeValue(NBR_HOURS_BACK_TO_PROCESS, "72");
	}
	
	public String getMgojobOnboardingNbrdaysThankyou() {
		return getAttributeValue(MGOJOB_ONBOARDING_NBRDAYS_THANKYOU);
	}

	public String getMgojobOnboardingNbrdaysAccountoverview() {
		return getAttributeValue(MGOJOB_ONBOARDING_NBRDAYS_ACCOUNTOVERVIEW);
	}

	public String getMgojobOnboardingNbrdaysSendmoneyagain() {
		return getAttributeValue(MGOJOB_ONBOARDING_NBRDAYS_SENDMONEYAGAIN);
	}

	//added by Ankit Bhatt 8873
	/**
	 * @return the nbrHoursBailerMail1
	 */
	public int getNbrHoursBailerMail1() {
		return getIntegerAttributeValue(NBR_HOURS_BAILER_MAIL_1,"60");
	}

	/**
	 * @return the nbrHoursBailerMail2
	 */
	public int getNbrHoursBailerMail2() {
		return getIntegerAttributeValue(NBR_HOURS_BAILER_MAIL_2,"1440");
	}
	//ended
}
