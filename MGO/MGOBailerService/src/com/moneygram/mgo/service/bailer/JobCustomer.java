package com.moneygram.mgo.service.bailer;

import java.math.BigDecimal;
import java.util.Calendar;

public class JobCustomer {

	private String custLogonId;
	private Long custId;
	private String customerFirstName;
	private String customerLastName;
	private Calendar createDateTime;
	/**
	 * @return the custLogonId
	 */
	public String getCustLogonId() {
		return custLogonId;
	}
	/**
	 * @param custLogonId the custLogonId to set
	 */
	public void setCustLogonId(String custLogonId) {
		this.custLogonId = custLogonId;
	}
	/**
	 * @return the custId
	 */
	public Long getCustId() {
		return custId;
	}
	/**
	 * @param custId the custId to set
	 */
	public void setCustId(Long custId) {
		this.custId = custId;
	}
	/**
	 * @return the customerFirstName
	 */
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	/**
	 * @param customerFirstName the customerFirstName to set
	 */
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	/**
	 * @return the customerLastName
	 */
	public String getCustomerLastName() {
		return customerLastName;
	}
	/**
	 * @param customerLastName the customerLastName to set
	 */
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	/**
	 * @return the createDateTime
	 */
	public Calendar getCreateDateTime() {
		return createDateTime;
	}
	/**
	 * @param createDateTime the createDateTime to set
	 */
	public void setCreateDateTime(Calendar createDateTime) {
		this.createDateTime = createDateTime;
	}

}