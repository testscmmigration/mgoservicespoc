package com.moneygram.mgo.service.bailer;

import com.moneygram.common.service.BaseOperationResponse;

public class ProcessBailerEventsResponse  extends BaseOperationResponse  implements java.io.Serializable {
    private boolean bailerEmailSent;

    public ProcessBailerEventsResponse() {
    }


    /**
     * Gets the bailerEmailSent value for this ProcessBailerEventsResponse.
     * 
     * @return bailerEmailSent
     */
    public boolean isBailerEmailSent() {
        return bailerEmailSent;
    }


    /**
     * Sets the bailerEmailSent value for this ProcessBailerEventsResponse.
     * 
     * @param bailerEmailSent
     */
    public void setBailerEmailSent(boolean bailerEmailSent) {
        this.bailerEmailSent = bailerEmailSent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessBailerEventsResponse)) return false;
        ProcessBailerEventsResponse other = (ProcessBailerEventsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.bailerEmailSent == other.isBailerEmailSent();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += (isBailerEmailSent() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[bailerEmailSent=").append(isBailerEmailSent());
		buffer.append(" ]");
		return buffer.toString();
	}
}
