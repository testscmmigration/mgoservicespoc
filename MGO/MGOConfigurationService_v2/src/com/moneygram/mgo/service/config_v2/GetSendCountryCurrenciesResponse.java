/**
 * GetSendCountryCurrenciesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

import com.moneygram.common.service.BaseOperationResponse;

public class GetSendCountryCurrenciesResponse  extends BaseOperationResponse  implements java.io.Serializable {
    private com.moneygram.mgo.service.config_v2.SendCountryCurrencies sendCountryCurrencies;

    public GetSendCountryCurrenciesResponse() {
    }


    /**
     * Gets the sendCountryCurrencies value for this GetSendCountryCurrenciesResponse.
     * 
     * @return sendCountryCurrencies
     */
    public com.moneygram.mgo.service.config_v2.SendCountryCurrencies getSendCountryCurrencies() {
        return sendCountryCurrencies;
    }


    /**
     * Sets the sendCountryCurrencies value for this GetSendCountryCurrenciesResponse.
     * 
     * @param sendCountryCurrencies
     */
    public void setSendCountryCurrencies(com.moneygram.mgo.service.config_v2.SendCountryCurrencies sendCountryCurrencies) {
        this.sendCountryCurrencies = sendCountryCurrencies;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSendCountryCurrenciesResponse)) return false;
        GetSendCountryCurrenciesResponse other = (GetSendCountryCurrenciesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.sendCountryCurrencies==null && other.getSendCountryCurrencies()==null) || 
             (this.sendCountryCurrencies!=null &&
              this.sendCountryCurrencies.equals(other.getSendCountryCurrencies())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSendCountryCurrencies() != null) {
            _hashCode += getSendCountryCurrencies().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSendCountryCurrenciesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSendCountryCurrenciesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendCountryCurrencies");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "sendCountryCurrencies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "SendCountryCurrencies"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
