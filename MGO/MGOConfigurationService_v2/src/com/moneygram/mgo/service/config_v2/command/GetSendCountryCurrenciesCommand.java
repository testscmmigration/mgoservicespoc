package com.moneygram.mgo.service.config_v2.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config_v2.CountryCurrenciesRow;
import com.moneygram.mgo.service.config_v2.Currency;
import com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest;
import com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse;
import com.moneygram.mgo.service.config_v2.SendCountryCurrencies;
import com.moneygram.mgo.service.config_v2.dao.MGTD2DAO;

public class GetSendCountryCurrenciesCommand extends ReadCommand{
	
	@Override
	protected boolean isRequestSupported(OperationRequest operationRequest)
			throws CommandException {
		
		return operationRequest instanceof GetSendCountryCurrenciesRequest;
		
	}

	@Override
	protected OperationResponse process(OperationRequest operationRequest)
			throws CommandException {

		GetSendCountryCurrenciesRequest request = (GetSendCountryCurrenciesRequest) operationRequest;
		
		MGTD2DAO dao = (MGTD2DAO) getDataAccessObject();
		List<CountryCurrenciesRow> rowList = dao.getSendCountryCurrencies();
		
		Map<String, List<Currency>> map = new HashMap<String, List<Currency>>();
		
		for(CountryCurrenciesRow row : rowList){
			String countryCode = row.getIsoCountryCode();
			
			if(!map.containsKey(countryCode)) {
				map.put(countryCode, new ArrayList<Currency>());
			}
				
			Currency currency = new Currency();
			currency.setCode(row.getCurrency());
			currency.setName(row.getCurrencyname());
			
			map.get(countryCode).add(currency);
		}
		
		SendCountryCurrencies sendCountryCurrencies = new SendCountryCurrencies();
		List<Currency> currencies = map.get(request.getSendCountry());
		
		sendCountryCurrencies.setCountryCode(request.getSendCountry());
		sendCountryCurrencies.setCurrencies((Currency[]) currencies.toArray(new Currency[currencies.size()]));
		
		GetSendCountryCurrenciesResponse response = new GetSendCountryCurrenciesResponse();
		response.setSendCountryCurrencies(sendCountryCurrencies);
		
		return (OperationResponse) response;	
	}

}
