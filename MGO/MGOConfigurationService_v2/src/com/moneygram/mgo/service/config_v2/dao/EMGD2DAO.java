/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.config_v2.dao;

import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config_v2.ReceiveAgent;
import com.moneygram.mgo.service.config_v2.ReceiveCountry;

/**
 *
 * MGTD2 DAO.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService MGTD2 DAO</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2012/03/09 04:40:47 $ </td><tr><td>
 * @author   </td><td>$Author: vf69 $ </td>
 *</table>
 *</div>
 */
public class EMGD2DAO extends BaseDAO {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			EMGD2DAO.class);

	private static final String GET_RECEIVE_COUNTRIES_PROC = "pkg_em_biller_and_country.PRC_GET_COUNTRY_EXCEPTION_CV";
	private static final String GET_TRANSACTION_PROC = "pkg_mgo_transactions.PRC_GET_EMG_TRAN_DETAILS_CV";
	private static final String GET_TRANSACTIONS_BY_CONSUMER_PROC = "pkg_mgo_transactions.PRC_GET_EMG_TRAN_LIST_CV";

	private static final String DB_CALL_TYPE_CODE = "WEB";
	private static final String DB_CALLER_ID = "MGO";

	/**
	 * Returns procedure log id.
	 * @param result
	 * @return procedure log id.
	 */
	private Number getProcedureLogId(Map<?, ?> result) {
		return (Number) result.get("ov_prcs_log_id");
	}

	/**
	 *
	 * @param sendCountry
	 * @return
	 * @throws DAOException
	 */
	@SuppressWarnings("unchecked")
	public List<ReceiveCountry> getReceiveCountries(String sendCountry)
			throws DAOException {
		SimpleJdbcCall getReceiveCountries = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_RECEIVE_COUNTRIES_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_rcv_iso_cntry_code", Types.VARCHAR),
						new SqlParameter("iv_iso_cntry_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_emg_country_exception_cv",
								OracleTypes.CURSOR,
								new ReceiveCountryRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_rcv_iso_cntry_code", null);
		parameters.addValue("iv_iso_cntry_code", sendCountry);

		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveCountries: calling procedure="
					+ GET_RECEIVE_COUNTRIES_PROC + " with "
					+ parameters.getValues().size()
					+ " input parameters. sendCountry=" + sendCountry);
		}

		Map<?, ?> result = getReceiveCountries.execute(parameters);
		List<ReceiveCountry> countries = (List<ReceiveCountry>) result
				.get("ov_emg_country_exception_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveCountries: procedure log id="
					+ getProcedureLogId(result) + " countries="
					+ (countries == null ? null : countries.size()));
		}

		return countries;
	}

	/**
	 * Returns receive agent.
	 *
	 * @param agentId
	 * @return receive agent
	 */
	@SuppressWarnings("unchecked")
	public ReceiveAgent getReceiveAgent(Long agentId) throws DAOException {
		if (agentId == null)
			throw new DAOException("Invalid agentId provided");

		SimpleJdbcCall getBiller = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(
						"pkg_em_biller_and_country.prc_get_biller_cv")
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_agent_id", Types.INTEGER),
						new SqlParameter("iv_rcv_agcy_code", Types.VARCHAR),
						new SqlParameter("iv_agent_stat_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_emg_expay_agent_cv",
								OracleTypes.CURSOR, new RecvAgentRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_agent_id", agentId);
		parameters.addValue("iv_rcv_agcy_code", null);
		parameters.addValue("iv_agent_stat_code", null);

		Map<?, ?> result = getBiller.execute(parameters);
		List<ReceiveAgent> agents = (List<ReceiveAgent>) result
				.get("ov_emg_expay_agent_cv");

		if (logger.isDebugEnabled()) {
			if (agents == null)
				logger.debug("getBiller: returned list is null");
			else
				logger.debug("getBiller: returned list size is "
						+ agents.size());
			logger.debug("getBiller: procedure log id="
					+ getProcedureLogId(result));
		}

		if (agents == null || agents.size() != 1)
			return null;
		return agents.get(0);
	}

	/**
	 * Returns receive agents.
	 *
	 * @return receive agents
	 */
	@SuppressWarnings("unchecked")
	public List<ReceiveAgent> getActiveReceiveAgents() throws DAOException {
		SimpleJdbcCall getBillers = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(
						"pkg_em_biller_and_country.prc_get_mgo_billers")
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlOutParameter("ov_agent_id_cv",
								OracleTypes.CURSOR, new RecvAgentsRowMapper()),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);

		Map<?, ?> result = getBillers.execute(parameters);
		List<ReceiveAgent> agents = (List<ReceiveAgent>) result
				.get("ov_agent_id_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getBillers: procedure log id="
					+ getProcedureLogId(result));
		}

		return agents;
	}

	protected static Date getPendingStartDate(int pendTxnExpHours) {
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, pendTxnExpHours * -1);
        return cal.getTime();
	}

	private void addCallTypeParameter(MapSqlParameterSource parameters) {
        parameters.addValue("iv_call_type_code", "MGO");
    }

}
