package com.moneygram.mgo.service.config_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.mgo.service.config_v2.CountryCurrenciesRow;

public class CountryCurrenciesRowMapper extends BaseRowMapper {

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		CountryCurrenciesRow o = new CountryCurrenciesRow();
		o.setServiceTypeCode(rs.getString("CNTRY_SERVICE_TYPE_CODE"));
		o.setCountryCode(rs.getString("CNTRY_CNTRY_CODE"));
		o.setDeliveryOption(rs.getString("CNTRY_DLVRY_OPT"));
		o.setCurrency(rs.getString("CNTRY_CURRENCY"));
		o.setBaseInd(getBooleanFromString(rs.getString("CNTRY_BASE_IND")));
		o.setDecPrecesion(rs.getString("CNTRY_DEC_PRECESION"));
		o.setMaxTransactionAmount(rs.getString("CNTRY_MAX_TX_AMT"));
		o.setLegalIdAmount(rs.getString("CNTRY_LEGAL_ID_AMT"));
		o.setPhotoIdAmount(rs.getString("CNTRY_PHOTO_ID_AMT"));
		o.setQuestionAmount(rs.getString("CNTRY_QUESTION_AMT"));
		o.setAddressAmount(rs.getString("CNTRY_ADDRESS_AMT"));
		o.setWarningAmount(rs.getString("CNTRY_WARNING_AMT"));
		o.setIsoCountryAbbreviation(rs.getString("CNTRY_ISO_CNTRY_ABBR"));
		o.setCountryName(rs.getString("CNTRY_CNTRY_NAME"));
		o.setCurrencyname(rs.getString("CNTRY_CURRENCY_NAME"));
		o.setIndctvCurrency(rs.getString("CNTRY_INDCTV_CURR"));
		o.setIndctvRate(rs.getString("CNTRY_INDCTV_RATE"));
		o.setIndctvDecPrecesion(rs.getString("CNTRY_INDCTV_DEC_PREC"));
		o.setIndctvCurrencyName(rs.getString("CNTRY_INDCTV_CURR_NAME"));
		o.setIsoCountryCode(rs.getString("CNTRY_ISO_CNTRY_CODE"));
		//
		o.setDispIndctvSw(getBooleanFromString(rs.getString("CNTRY_DISP_INDCTV_SW")));
		o.setBlockAnyRdrct(getBooleanFromString(rs.getString("CNTRY_BLK_ANY_RDRCT")));
		o.setBlockRdrctDest(getBooleanFromString(rs.getString("CNTRY_BLK_RDRCT_DEST")));
		o.setAllowSndAddrDtl(getBooleanFromString(rs.getString("CNTRY_ALLOW_SND_ADDR_DTL_FLAG")));
	
		

		return o;
	}
	
	private Boolean getBooleanFromString(String s){
		return (s==null)?(false):(s.equals("Y")); 
	}
}
