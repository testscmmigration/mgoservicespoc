package com.moneygram.mgo.service.config_v2.command;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest;
import com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse;
import com.moneygram.mgo.service.config_v2.profile.Profile;
import com.moneygram.mgo.service.config_v2.profilesconfiguration.ProfilesConfiguration;
import com.moneygram.mgo.service.config_v2.util.ConfigurationReader;
/**
 * @author vf69
 */

public class GetSiteProfilesCommand extends ReadCommand {
	
	@Override
	protected boolean isRequestSupported(OperationRequest request) throws CommandException {
		return request instanceof GetSiteProfilesRequest;
	}

	protected OperationResponse process(OperationRequest request) throws CommandException {
		
		try {
			GetSiteProfilesRequest configRequest = (GetSiteProfilesRequest) request;
			ConfigurationReader configReader = new ConfigurationReader();			
			ProfilesConfiguration profilesConfig = configReader.parseConfigurationXML();
			
			List<Profile> profileList = new ArrayList<Profile>();
			
			for(Profile profile: profilesConfig.getProfile()) {
				if(profile.getMgoAgentIdentifier().toString().equals(configRequest.getProfileCodePattern())) {
					profileList.add(profile);
				}
			}
			
			if(profileList.size() == 0) {
				throw new Exception("Profile Code Pattern does not match any profile");
			}
			
			GetSiteProfilesResponse response = new GetSiteProfilesResponse();
			response.setProfilesConfiguration(profileList.toArray(new Profile[profileList.size()]));
			
			return (OperationResponse) response;
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new CommandException("Failed to get Site Profiles", e);
		}
	}
}

