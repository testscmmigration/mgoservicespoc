package com.moneygram.mgo.service.config_v2.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.moneygram.mgo.service.config_v2.profilesconfiguration.ProfilesConfiguration;

public class ConfigurationReader {

	MGOConfigServiceResourceConfig mgoResourceConfig = MGOConfigServiceResourceConfig.getInstance();
	
	public ProfilesConfiguration parseConfigurationXML() throws JAXBException, Exception {
		try {
			String filename = mgoResourceConfig.getProfilesFilename();
			
			InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
			 
			JAXBContext jc = JAXBContext.newInstance("com.moneygram.mgo.service.config_v2.profilesconfiguration");
			Unmarshaller u = jc.createUnmarshaller();
			JAXBElement<ProfilesConfiguration> o;
			o = (JAXBElement<ProfilesConfiguration>) u.unmarshal(is);
			return o.getValue();
		}
		catch(Exception e) {
			throw new Exception("Could not load profiles file",e);
		}
	}
	
}
