/**
 * SendCountryCurrencies.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class SendCountryCurrencies  implements java.io.Serializable {
    private java.lang.String countryCode;

    private com.moneygram.mgo.service.config_v2.Currency[] currencies;

    public SendCountryCurrencies() {
    }

    public SendCountryCurrencies(
           java.lang.String countryCode,
           com.moneygram.mgo.service.config_v2.Currency[] currencies) {
           this.countryCode = countryCode;
           this.currencies = currencies;
    }


    /**
     * Gets the countryCode value for this SendCountryCurrencies.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this SendCountryCurrencies.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the currencies value for this SendCountryCurrencies.
     * 
     * @return currencies
     */
    public com.moneygram.mgo.service.config_v2.Currency[] getCurrencies() {
        return currencies;
    }


    /**
     * Sets the currencies value for this SendCountryCurrencies.
     * 
     * @param currencies
     */
    public void setCurrencies(com.moneygram.mgo.service.config_v2.Currency[] currencies) {
        this.currencies = currencies;
    }

    public com.moneygram.mgo.service.config_v2.Currency getCurrencies(int i) {
        return this.currencies[i];
    }

    public void setCurrencies(int i, com.moneygram.mgo.service.config_v2.Currency _value) {
        this.currencies[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SendCountryCurrencies)) return false;
        SendCountryCurrencies other = (SendCountryCurrencies) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.currencies==null && other.getCurrencies()==null) || 
             (this.currencies!=null &&
              java.util.Arrays.equals(this.currencies, other.getCurrencies())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getCurrencies() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCurrencies());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCurrencies(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SendCountryCurrencies.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "SendCountryCurrencies"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "countryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencies");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "currencies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "Currency"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
