package com.moneygram.mgo.service.config_v2.command;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config_v2.GetProfileRequest;
import com.moneygram.mgo.service.config_v2.GetProfileResponse;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse;
import com.moneygram.mgo.service.config_v2.dao.MGTD2DAO;
import com.moneygram.mgo.service.config_v2.profile.Profile;
import com.moneygram.mgo.service.config_v2.profilesconfiguration.ProfilesConfiguration;
import com.moneygram.mgo.service.config_v2.util.ConfigurationReader;

/**
 * @author vf69
 *
 */
public class GetProfileCommand extends ReadCommand {

	@Override
	protected boolean isRequestSupported(OperationRequest request) throws CommandException {
		return request instanceof GetProfileRequest;
	}

	protected OperationResponse process(OperationRequest request) throws CommandException {
		try {
			GetProfileRequest configRequest = (GetProfileRequest) request;
			ConfigurationReader configReader = new ConfigurationReader();
			ProfilesConfiguration profilesConfig = configReader.parseConfigurationXML();
			
			GetProfileResponse response = new GetProfileResponse();
			for(Profile profile: profilesConfig.getProfile()) {
				if(profile.getMgoAgentIdentifier().toString().equals(configRequest.getProfileCode()) 
						&& profile.getProductName().equals(configRequest.getMgoProduct())) {
					response.setProfile(profile);
				}
			}

			return (OperationResponse) response;
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new CommandException("Failed to get valid profile", e);
		}
	}
}
