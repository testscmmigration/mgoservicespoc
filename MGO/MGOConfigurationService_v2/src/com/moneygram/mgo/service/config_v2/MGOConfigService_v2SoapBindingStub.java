/**
 * MGOConfigService_v2SoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class MGOConfigService_v2SoapBindingStub extends org.apache.axis.client.Stub implements com.moneygram.mgo.service.config_v2.MGOConfigServicePortType_v2 {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[6];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReceiveCountries");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveCountriesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveCountriesRequest"), com.moneygram.mgo.service.config_v2.GetReceiveCountriesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveCountriesResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveCountriesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReceiveAgent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveAgentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentRequest"), com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveAgentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReceiveAgents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveAgentsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentsRequest"), com.moneygram.mgo.service.config_v2.GetReceiveAgentsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentsResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getReceiveAgentsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSendCountryCurrencies");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getSendCountryCurrenciesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSendCountryCurrenciesRequest"), com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSendCountryCurrenciesResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getSendCountryCurrenciesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getProfile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getProfileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetProfileRequest"), com.moneygram.mgo.service.config_v2.GetProfileRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetProfileResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSiteProfiles");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getSiteProfilesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesRequest"), com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "getSiteProfilesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "configServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[5] = oper;

    }

    public MGOConfigService_v2SoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MGOConfigService_v2SoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MGOConfigService_v2SoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.BaseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ClientHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ClientHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorCategoryCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ErrorCategoryCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorHandlingCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ErrorHandlingCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Errors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceException[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Header");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.Header.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "InvocationMethodCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.InvocationMethodCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ProcessingInstruction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ProcessingInstruction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RelatedError.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedErrors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RelatedError[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RoutingContextHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RoutingContextHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "SecurityHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.SecurityHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceClient");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceClient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profilesConfiguration", "ProfilesConfiguration");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.profile.Profile[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Profile");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profilesConfiguration", "profile");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "AgentId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "AgentSequence");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "contentId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "CountryRef");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.IDRef.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Currency");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.profile.Currency.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "CurrencyCode");
            cachedSerQNames.add(qName);
            cls = org.apache.axis.types.IDRef.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Decimal14nonZero");
            cachedSerQNames.add(qName);
            cls = java.math.BigDecimal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "EmgMerchantId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "MGOAgentIdentifier");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "MGOConsumerId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PaymentOption");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.profile.PaymentOption.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PaymentOptionCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PaymentOptionName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "ProductName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Profile");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.profile.Profile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "ProfileCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "ProfileDescription");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "ProfileId");
            cachedSerQNames.add(qName);
            cls = int.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "ProfileToken");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Property");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.profile.Property.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PropertyDescription");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PropertyName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "TransactionAmount");
            cachedSerQNames.add(qName);
            cls = java.math.BigDecimal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "AgentId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ArrayOf_xs_anyType");
            cachedSerQNames.add(qName);
            cls = java.lang.Object[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "contentId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "Currency");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.Currency.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "Decimal14nonZero");
            cachedSerQNames.add(qName);
            cls = java.math.BigDecimal.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "EmgMerchantId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetProfileRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetProfileRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentsRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveAgentsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveAgentsResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveCountriesRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveCountriesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetReceiveCountriesResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSendCountryCurrenciesRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSendCountryCurrenciesResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "MGOConsumerId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ProfileCodePattern");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveAgent");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.ReceiveAgent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveAgents");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.ReceiveAgent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveAgent");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveCountries");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.ReceiveCountry[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveCountry");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveCountry");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.ReceiveCountry.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "SendCountryCurrencies");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.SendCountryCurrencies.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ServiceAction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.config_v2.ServiceAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "AddressLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "BuildingName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "City");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Country");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "CountryName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "County");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "DeliveryInstructionLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "FirstName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LoyaltyMemberId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleInitial");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ReceiverConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ReceiverConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "State");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "StateCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "UserId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Zip");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse getReceiveCountries(com.moneygram.mgo.service.config_v2.GetReceiveCountriesRequest getReceiveCountriesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getReceiveCountries"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getReceiveCountriesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse getReceiveAgent(com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest getReceiveAgentRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getReceiveAgent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getReceiveAgentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse getReceiveAgents(com.moneygram.mgo.service.config_v2.GetReceiveAgentsRequest getReceiveAgentsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getReceiveAgents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getReceiveAgentsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse getSendCountryCurrencies(com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest getSendCountryCurrenciesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getSendCountryCurrencies"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getSendCountryCurrenciesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.config_v2.GetProfileResponse getProfile(com.moneygram.mgo.service.config_v2.GetProfileRequest getProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getProfile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getProfileRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse getSiteProfiles(com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest getSiteProfilesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getSiteProfiles"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getSiteProfilesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
