/**
 * ReceiveCountry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class ReceiveCountry  implements java.io.Serializable {
    private java.lang.String countryCode;

    private java.lang.String countryName;

    private boolean sendAllowed;

    private java.math.BigDecimal sendAmount;

    public ReceiveCountry() {
    }

    public ReceiveCountry(
           java.lang.String countryCode,
           java.lang.String countryName,
           boolean sendAllowed,
           java.math.BigDecimal sendAmount) {
           this.countryCode = countryCode;
           this.countryName = countryName;
           this.sendAllowed = sendAllowed;
           this.sendAmount = sendAmount;
    }


    /**
     * Gets the countryCode value for this ReceiveCountry.
     * 
     * @return countryCode
     */
    public java.lang.String getCountryCode() {
        return countryCode;
    }


    /**
     * Sets the countryCode value for this ReceiveCountry.
     * 
     * @param countryCode
     */
    public void setCountryCode(java.lang.String countryCode) {
        this.countryCode = countryCode;
    }


    /**
     * Gets the countryName value for this ReceiveCountry.
     * 
     * @return countryName
     */
    public java.lang.String getCountryName() {
        return countryName;
    }


    /**
     * Sets the countryName value for this ReceiveCountry.
     * 
     * @param countryName
     */
    public void setCountryName(java.lang.String countryName) {
        this.countryName = countryName;
    }


    /**
     * Gets the sendAllowed value for this ReceiveCountry.
     * 
     * @return sendAllowed
     */
    public boolean isSendAllowed() {
        return sendAllowed;
    }


    /**
     * Sets the sendAllowed value for this ReceiveCountry.
     * 
     * @param sendAllowed
     */
    public void setSendAllowed(boolean sendAllowed) {
        this.sendAllowed = sendAllowed;
    }


    /**
     * Gets the sendAmount value for this ReceiveCountry.
     * 
     * @return sendAmount
     */
    public java.math.BigDecimal getSendAmount() {
        return sendAmount;
    }


    /**
     * Sets the sendAmount value for this ReceiveCountry.
     * 
     * @param sendAmount
     */
    public void setSendAmount(java.math.BigDecimal sendAmount) {
        this.sendAmount = sendAmount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReceiveCountry)) return false;
        ReceiveCountry other = (ReceiveCountry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.countryCode==null && other.getCountryCode()==null) || 
             (this.countryCode!=null &&
              this.countryCode.equals(other.getCountryCode()))) &&
            ((this.countryName==null && other.getCountryName()==null) || 
             (this.countryName!=null &&
              this.countryName.equals(other.getCountryName()))) &&
            this.sendAllowed == other.isSendAllowed() &&
            ((this.sendAmount==null && other.getSendAmount()==null) || 
             (this.sendAmount!=null &&
              this.sendAmount.equals(other.getSendAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCountryCode() != null) {
            _hashCode += getCountryCode().hashCode();
        }
        if (getCountryName() != null) {
            _hashCode += getCountryName().hashCode();
        }
        _hashCode += (isSendAllowed() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getSendAmount() != null) {
            _hashCode += getSendAmount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReceiveCountry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveCountry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "countryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "countryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendAllowed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "sendAllowed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "sendAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
