/**
 * GetSiteProfilesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

import com.moneygram.common.service.BaseOperationResponse;

public class GetSiteProfilesResponse  extends BaseOperationResponse  implements java.io.Serializable {
    private com.moneygram.mgo.service.config_v2.profile.Profile[] profilesConfiguration;

    public GetSiteProfilesResponse() {
    }

   


    /**
     * Gets the profilesConfiguration value for this GetSiteProfilesResponse.
     * 
     * @return profilesConfiguration
     */
    public com.moneygram.mgo.service.config_v2.profile.Profile[] getProfilesConfiguration() {
        return profilesConfiguration;
    }


    /**
     * Sets the profilesConfiguration value for this GetSiteProfilesResponse.
     * 
     * @param profilesConfiguration
     */
    public void setProfilesConfiguration(com.moneygram.mgo.service.config_v2.profile.Profile[] profilesConfiguration) {
        this.profilesConfiguration = profilesConfiguration;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSiteProfilesResponse)) return false;
        GetSiteProfilesResponse other = (GetSiteProfilesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.profilesConfiguration==null && other.getProfilesConfiguration()==null) || 
             (this.profilesConfiguration!=null &&
              java.util.Arrays.equals(this.profilesConfiguration, other.getProfilesConfiguration())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProfilesConfiguration() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProfilesConfiguration());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProfilesConfiguration(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSiteProfilesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profilesConfiguration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "profilesConfiguration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Profile"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profilesConfiguration", "profile"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
