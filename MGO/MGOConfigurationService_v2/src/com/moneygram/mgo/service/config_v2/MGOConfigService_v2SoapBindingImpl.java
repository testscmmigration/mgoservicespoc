/**
 * MGOConfigService_v2SoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class MGOConfigService_v2SoapBindingImpl implements com.moneygram.mgo.service.config_v2.MGOConfigServicePortType_v2{
    public com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse getReceiveCountries(com.moneygram.mgo.service.config_v2.GetReceiveCountriesRequest getReceiveCountriesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse getReceiveAgent(com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest getReceiveAgentRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse getReceiveAgents(com.moneygram.mgo.service.config_v2.GetReceiveAgentsRequest getReceiveAgentsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse getSendCountryCurrencies(com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest getSendCountryCurrenciesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.config_v2.GetProfileResponse getProfile(com.moneygram.mgo.service.config_v2.GetProfileRequest getProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse getSiteProfiles(com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest getSiteProfilesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

}
