package com.moneygram.mgo.service.config_v2;

public class CountryCurrenciesRow implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	
	private String serviceTypeCode = null;
	private String countryCode = null;
	private String deliveryOption = null;
	private String currency = null;
	private boolean baseInd = false;
	private String decPrecesion = null;
	private String maxTransactionAmount = null;
	private String legalIdAmount = null;
	private String photoIdAmount = null;
	private String questionAmount = null;
	private String addressAmount = null;
	private String warningAmount = null;
	private String isoCountryAbbreviation = null;
	private String countryName = null;
	private String currencyname = null;
	private String indctvCurrency = null;
	private String indctvRate = null;
	private String indctvDecPrecesion = null;
	private String indctvCurrencyName = null;
	private String isoCountryCode = null;
	private boolean dispIndctvSw = false;
	private boolean blockAnyRdrct = false;
	private boolean blockRdrctDest = false;
	private boolean allowSndAddrDtl = false;
	public String getServiceTypeCode() {
		return serviceTypeCode;
	}
	public void setServiceTypeCode(String serviceTypeCode) {
		this.serviceTypeCode = serviceTypeCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getDeliveryOption() {
		return deliveryOption;
	}
	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public boolean getBaseInd() {
		return baseInd;
	}
	public void setBaseInd(boolean baseInd) {
		this.baseInd = baseInd;
	}
	public String getDecPrecesion() {
		return decPrecesion;
	}
	public void setDecPrecesion(String decPrecesion) {
		this.decPrecesion = decPrecesion;
	}
	public String getMaxTransactionAmount() {
		return maxTransactionAmount;
	}
	public void setMaxTransactionAmount(String maxTransactionAmount) {
		this.maxTransactionAmount = maxTransactionAmount;
	}
	public String getLegalIdAmount() {
		return legalIdAmount;
	}
	public void setLegalIdAmount(String legalIdAmount) {
		this.legalIdAmount = legalIdAmount;
	}
	public String getPhotoIdAmount() {
		return photoIdAmount;
	}
	public void setPhotoIdAmount(String photoIdAmount) {
		this.photoIdAmount = photoIdAmount;
	}
	public String getQuestionAmount() {
		return questionAmount;
	}
	public void setQuestionAmount(String questionAmount) {
		this.questionAmount = questionAmount;
	}
	public String getAddressAmount() {
		return addressAmount;
	}
	public void setAddressAmount(String addressAmount) {
		this.addressAmount = addressAmount;
	}
	public String getWarningAmount() {
		return warningAmount;
	}
	public void setWarningAmount(String warningAmount) {
		this.warningAmount = warningAmount;
	}
	public String getIsoCountryAbbreviation() {
		return isoCountryAbbreviation;
	}
	public void setIsoCountryAbbreviation(String isoCountryAbbreviation) {
		this.isoCountryAbbreviation = isoCountryAbbreviation;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCurrencyname() {
		return currencyname;
	}
	public void setCurrencyname(String currencyname) {
		this.currencyname = currencyname;
	}
	public String getIndctvCurrency() {
		return indctvCurrency;
	}
	public void setIndctvCurrency(String indctvCurrency) {
		this.indctvCurrency = indctvCurrency;
	}
	public String getIndctvRate() {
		return indctvRate;
	}
	public void setIndctvRate(String indctvRate) {
		this.indctvRate = indctvRate;
	}
	public String getIndctvDecPrecesion() {
		return indctvDecPrecesion;
	}
	public void setIndctvDecPrecesion(String indctvDecPrecesion) {
		this.indctvDecPrecesion = indctvDecPrecesion;
	}
	public String getIndctvCurrencyName() {
		return indctvCurrencyName;
	}
	public void setIndctvCurrencyName(String indctvCurrencyName) {
		this.indctvCurrencyName = indctvCurrencyName;
	}
	public String getIsoCountryCode() {
		return isoCountryCode;
	}
	public void setIsoCountryCode(String isoCountryCode) {
		this.isoCountryCode = isoCountryCode;
	}
	public boolean isDispIndctvSw() {
		return dispIndctvSw;
	}
	public void setDispIndctvSw(boolean dispIndctvSw) {
		this.dispIndctvSw = dispIndctvSw;
	}
	public boolean isBlockAnyRdrct() {
		return blockAnyRdrct;
	}
	public void setBlockAnyRdrct(boolean blockAnyRdrct) {
		this.blockAnyRdrct = blockAnyRdrct;
	}
	public boolean isBlockRdrctDest() {
		return blockRdrctDest;
	}
	public void setBlockRdrctDest(boolean blockRdrctDest) {
		this.blockRdrctDest = blockRdrctDest;
	}
	public boolean isAllowSndAddrDtl() {
		return allowSndAddrDtl;
	}
	public void setAllowSndAddrDtl(boolean allowSndAddrDtl) {
		this.allowSndAddrDtl = allowSndAddrDtl;
	}

}
