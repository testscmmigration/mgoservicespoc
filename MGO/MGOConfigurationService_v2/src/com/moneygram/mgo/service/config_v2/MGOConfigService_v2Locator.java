/**
 * MGOConfigService_v2Locator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class MGOConfigService_v2Locator extends org.apache.axis.client.Service implements com.moneygram.mgo.service.config_v2.MGOConfigService_v2 {

    public MGOConfigService_v2Locator() {
    }


    public MGOConfigService_v2Locator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MGOConfigService_v2Locator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MGOConfigService_v2
    private java.lang.String MGOConfigService_v2_address = "http://base.url/MGOService/services/MGOConfigService_v2";

    public java.lang.String getMGOConfigService_v2Address() {
        return MGOConfigService_v2_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MGOConfigService_v2WSDDServiceName = "MGOConfigService_v2";

    public java.lang.String getMGOConfigService_v2WSDDServiceName() {
        return MGOConfigService_v2WSDDServiceName;
    }

    public void setMGOConfigService_v2WSDDServiceName(java.lang.String name) {
        MGOConfigService_v2WSDDServiceName = name;
    }

    public com.moneygram.mgo.service.config_v2.MGOConfigServicePortType_v2 getMGOConfigService_v2() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MGOConfigService_v2_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMGOConfigService_v2(endpoint);
    }

    public com.moneygram.mgo.service.config_v2.MGOConfigServicePortType_v2 getMGOConfigService_v2(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.moneygram.mgo.service.config_v2.MGOConfigService_v2SoapBindingStub _stub = new com.moneygram.mgo.service.config_v2.MGOConfigService_v2SoapBindingStub(portAddress, this);
            _stub.setPortName(getMGOConfigService_v2WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMGOConfigService_v2EndpointAddress(java.lang.String address) {
        MGOConfigService_v2_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.moneygram.mgo.service.config_v2.MGOConfigServicePortType_v2.class.isAssignableFrom(serviceEndpointInterface)) {
                com.moneygram.mgo.service.config_v2.MGOConfigService_v2SoapBindingStub _stub = new com.moneygram.mgo.service.config_v2.MGOConfigService_v2SoapBindingStub(new java.net.URL(MGOConfigService_v2_address), this);
                _stub.setPortName(getMGOConfigService_v2WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MGOConfigService_v2".equals(inputPortName)) {
            return getMGOConfigService_v2();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "MGOConfigService_v2");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "MGOConfigService_v2"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MGOConfigService_v2".equals(portName)) {
            setMGOConfigService_v2EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
