/**
 * GetSiteProfilesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

import com.moneygram.common.service.BaseOperationRequest;

public class GetSiteProfilesRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private java.lang.String profileCodePattern;

    public GetSiteProfilesRequest() {
    }

   


    /**
     * Gets the profileCodePattern value for this GetSiteProfilesRequest.
     * 
     * @return profileCodePattern
     */
    public java.lang.String getProfileCodePattern() {
        return profileCodePattern;
    }


    /**
     * Sets the profileCodePattern value for this GetSiteProfilesRequest.
     * 
     * @param profileCodePattern
     */
    public void setProfileCodePattern(java.lang.String profileCodePattern) {
        this.profileCodePattern = profileCodePattern;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSiteProfilesRequest)) return false;
        GetSiteProfilesRequest other = (GetSiteProfilesRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.profileCodePattern==null && other.getProfileCodePattern()==null) || 
             (this.profileCodePattern!=null &&
              this.profileCodePattern.equals(other.getProfileCodePattern())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getProfileCodePattern() != null) {
            _hashCode += getProfileCodePattern().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSiteProfilesRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "GetSiteProfilesRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileCodePattern");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "profileCodePattern"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
