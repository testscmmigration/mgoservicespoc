package com.moneygram.mgo.service.config_v2.util;

import com.moneygram.common.service.util.ResourceConfig;


public class MGOConfigServiceResourceConfig extends ResourceConfig {

	public static final String PROFILES_FILE_NAME = "PROFILES_FILE_NAME";

	public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOConsumerServiceResourceReference";

	
	private static MGOConfigServiceResourceConfig instance = null;

	
	private MGOConfigServiceResourceConfig() {
		super();
	}
	
	public static MGOConfigServiceResourceConfig getInstance() {
		if (instance == null) {
			synchronized (MGOConfigServiceResourceConfig.class) {
				if (instance == null) {
					instance = new MGOConfigServiceResourceConfig();
					instance.initResourceConfig();
				}
			}
		}
		return instance;
	}

	public String getProfilesFilename() {
		return getAttributeValue(PROFILES_FILE_NAME);
	}

	@Override
	protected String getResourceConfigurationJndiName() {
		return RESOURCE_REFERENCE_JNDI;
	}

}
