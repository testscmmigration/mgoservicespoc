/**
 * ReceiveAgent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2;

public class ReceiveAgent  implements java.io.Serializable {
    private long agentId;

    private java.math.BigDecimal limitAmount;

    private java.lang.String emgMerchantId;

    public ReceiveAgent() {
    }

    public ReceiveAgent(
           long agentId,
           java.math.BigDecimal limitAmount,
           java.lang.String emgMerchantId) {
           this.agentId = agentId;
           this.limitAmount = limitAmount;
           this.emgMerchantId = emgMerchantId;
    }


    /**
     * Gets the agentId value for this ReceiveAgent.
     * 
     * @return agentId
     */
    public long getAgentId() {
        return agentId;
    }


    /**
     * Sets the agentId value for this ReceiveAgent.
     * 
     * @param agentId
     */
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }


    /**
     * Gets the limitAmount value for this ReceiveAgent.
     * 
     * @return limitAmount
     */
    public java.math.BigDecimal getLimitAmount() {
        return limitAmount;
    }


    /**
     * Sets the limitAmount value for this ReceiveAgent.
     * 
     * @param limitAmount
     */
    public void setLimitAmount(java.math.BigDecimal limitAmount) {
        this.limitAmount = limitAmount;
    }


    /**
     * Gets the emgMerchantId value for this ReceiveAgent.
     * 
     * @return emgMerchantId
     */
    public java.lang.String getEmgMerchantId() {
        return emgMerchantId;
    }


    /**
     * Sets the emgMerchantId value for this ReceiveAgent.
     * 
     * @param emgMerchantId
     */
    public void setEmgMerchantId(java.lang.String emgMerchantId) {
        this.emgMerchantId = emgMerchantId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReceiveAgent)) return false;
        ReceiveAgent other = (ReceiveAgent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.agentId == other.getAgentId() &&
            ((this.limitAmount==null && other.getLimitAmount()==null) || 
             (this.limitAmount!=null &&
              this.limitAmount.equals(other.getLimitAmount()))) &&
            ((this.emgMerchantId==null && other.getEmgMerchantId()==null) || 
             (this.emgMerchantId!=null &&
              this.emgMerchantId.equals(other.getEmgMerchantId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getAgentId()).hashCode();
        if (getLimitAmount() != null) {
            _hashCode += getLimitAmount().hashCode();
        }
        if (getEmgMerchantId() != null) {
            _hashCode += getEmgMerchantId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReceiveAgent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "ReceiveAgent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "agentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limitAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "limitAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emgMerchantId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2", "emgMerchantId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
