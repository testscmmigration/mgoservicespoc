/**
 * Profile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config_v2.profile;

public class Profile  implements java.io.Serializable {
    private java.lang.String mgoAgentIdentifier;

    private java.lang.String agentSequence;

    private long agentId;

    private int profileId;

    private java.lang.String productName;

    private java.lang.String description;

    private java.lang.String token;

    private java.math.BigDecimal maxTransactionAmount;

    private org.apache.axis.types.IDRef country;

    private boolean showRewards;

    private boolean showPayBills;

    private org.apache.axis.types.IDRef[] currencyCodes;

    private com.moneygram.mgo.service.config_v2.profile.PaymentOption[] paymentOptions;

    private boolean enabled;

    private com.moneygram.mgo.service.config_v2.profile.Property[] properties;

    public Profile() {
    }

    public Profile(
           java.lang.String mgoAgentIdentifier,
           java.lang.String agentSequence,
           long agentId,
           int profileId,
           java.lang.String productName,
           java.lang.String description,
           java.lang.String token,
           java.math.BigDecimal maxTransactionAmount,
           org.apache.axis.types.IDRef country,
           boolean showRewards,
           boolean showPayBills,
           org.apache.axis.types.IDRef[] currencyCodes,
           com.moneygram.mgo.service.config_v2.profile.PaymentOption[] paymentOptions,
           boolean enabled,
           com.moneygram.mgo.service.config_v2.profile.Property[] properties) {
           this.mgoAgentIdentifier = mgoAgentIdentifier;
           this.agentSequence = agentSequence;
           this.agentId = agentId;
           this.profileId = profileId;
           this.productName = productName;
           this.description = description;
           this.token = token;
           this.maxTransactionAmount = maxTransactionAmount;
           this.country = country;
           this.showRewards = showRewards;
           this.showPayBills = showPayBills;
           this.currencyCodes = currencyCodes;
           this.paymentOptions = paymentOptions;
           this.enabled = enabled;
           this.properties = properties;
    }


    /**
     * Gets the mgoAgentIdentifier value for this Profile.
     * 
     * @return mgoAgentIdentifier
     */
    public java.lang.String getMgoAgentIdentifier() {
        return mgoAgentIdentifier;
    }


    /**
     * Sets the mgoAgentIdentifier value for this Profile.
     * 
     * @param mgoAgentIdentifier
     */
    public void setMgoAgentIdentifier(java.lang.String mgoAgentIdentifier) {
        this.mgoAgentIdentifier = mgoAgentIdentifier;
    }


    /**
     * Gets the agentSequence value for this Profile.
     * 
     * @return agentSequence
     */
    public java.lang.String getAgentSequence() {
        return agentSequence;
    }


    /**
     * Sets the agentSequence value for this Profile.
     * 
     * @param agentSequence
     */
    public void setAgentSequence(java.lang.String agentSequence) {
        this.agentSequence = agentSequence;
    }


    /**
     * Gets the agentId value for this Profile.
     * 
     * @return agentId
     */
    public long getAgentId() {
        return agentId;
    }


    /**
     * Sets the agentId value for this Profile.
     * 
     * @param agentId
     */
    public void setAgentId(long agentId) {
        this.agentId = agentId;
    }


    /**
     * Gets the profileId value for this Profile.
     * 
     * @return profileId
     */
    public int getProfileId() {
        return profileId;
    }


    /**
     * Sets the profileId value for this Profile.
     * 
     * @param profileId
     */
    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }


    /**
     * Gets the productName value for this Profile.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this Profile.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the description value for this Profile.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Profile.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the token value for this Profile.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this Profile.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the maxTransactionAmount value for this Profile.
     * 
     * @return maxTransactionAmount
     */
    public java.math.BigDecimal getMaxTransactionAmount() {
        return maxTransactionAmount;
    }


    /**
     * Sets the maxTransactionAmount value for this Profile.
     * 
     * @param maxTransactionAmount
     */
    public void setMaxTransactionAmount(java.math.BigDecimal maxTransactionAmount) {
        this.maxTransactionAmount = maxTransactionAmount;
    }


    /**
     * Gets the country value for this Profile.
     * 
     * @return country
     */
    public org.apache.axis.types.IDRef getCountry() {
        return country;
    }


    /**
     * Sets the country value for this Profile.
     * 
     * @param country
     */
    public void setCountry(org.apache.axis.types.IDRef country) {
        this.country = country;
    }


    /**
     * Gets the showRewards value for this Profile.
     * 
     * @return showRewards
     */
    public boolean isShowRewards() {
        return showRewards;
    }


    /**
     * Sets the showRewards value for this Profile.
     * 
     * @param showRewards
     */
    public void setShowRewards(boolean showRewards) {
        this.showRewards = showRewards;
    }


    /**
     * Gets the showPayBills value for this Profile.
     * 
     * @return showPayBills
     */
    public boolean isShowPayBills() {
        return showPayBills;
    }


    /**
     * Sets the showPayBills value for this Profile.
     * 
     * @param showPayBills
     */
    public void setShowPayBills(boolean showPayBills) {
        this.showPayBills = showPayBills;
    }


    /**
     * Gets the currencyCodes value for this Profile.
     * 
     * @return currencyCodes
     */
    public org.apache.axis.types.IDRef[] getCurrencyCodes() {
        return currencyCodes;
    }


    /**
     * Sets the currencyCodes value for this Profile.
     * 
     * @param currencyCodes
     */
    public void setCurrencyCodes(org.apache.axis.types.IDRef[] currencyCodes) {
        this.currencyCodes = currencyCodes;
    }

    public org.apache.axis.types.IDRef getCurrencyCodes(int i) {
        return this.currencyCodes[i];
    }

    public void setCurrencyCodes(int i, org.apache.axis.types.IDRef _value) {
        this.currencyCodes[i] = _value;
    }


    /**
     * Gets the paymentOptions value for this Profile.
     * 
     * @return paymentOptions
     */
    public com.moneygram.mgo.service.config_v2.profile.PaymentOption[] getPaymentOptions() {
        return paymentOptions;
    }


    /**
     * Sets the paymentOptions value for this Profile.
     * 
     * @param paymentOptions
     */
    public void setPaymentOptions(com.moneygram.mgo.service.config_v2.profile.PaymentOption[] paymentOptions) {
        this.paymentOptions = paymentOptions;
    }

    public com.moneygram.mgo.service.config_v2.profile.PaymentOption getPaymentOptions(int i) {
        return this.paymentOptions[i];
    }

    public void setPaymentOptions(int i, com.moneygram.mgo.service.config_v2.profile.PaymentOption _value) {
        this.paymentOptions[i] = _value;
    }


    /**
     * Gets the enabled value for this Profile.
     * 
     * @return enabled
     */
    public boolean isEnabled() {
        return enabled;
    }


    /**
     * Sets the enabled value for this Profile.
     * 
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    /**
     * Gets the properties value for this Profile.
     * 
     * @return properties
     */
    public com.moneygram.mgo.service.config_v2.profile.Property[] getProperties() {
        return properties;
    }


    /**
     * Sets the properties value for this Profile.
     * 
     * @param properties
     */
    public void setProperties(com.moneygram.mgo.service.config_v2.profile.Property[] properties) {
        this.properties = properties;
    }

    public com.moneygram.mgo.service.config_v2.profile.Property getProperties(int i) {
        return this.properties[i];
    }

    public void setProperties(int i, com.moneygram.mgo.service.config_v2.profile.Property _value) {
        this.properties[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Profile)) return false;
        Profile other = (Profile) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.mgoAgentIdentifier==null && other.getMgoAgentIdentifier()==null) || 
             (this.mgoAgentIdentifier!=null &&
              this.mgoAgentIdentifier.equals(other.getMgoAgentIdentifier()))) &&
            ((this.agentSequence==null && other.getAgentSequence()==null) || 
             (this.agentSequence!=null &&
              this.agentSequence.equals(other.getAgentSequence()))) &&
            this.agentId == other.getAgentId() &&
            this.profileId == other.getProfileId() &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.maxTransactionAmount==null && other.getMaxTransactionAmount()==null) || 
             (this.maxTransactionAmount!=null &&
              this.maxTransactionAmount.equals(other.getMaxTransactionAmount()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            this.showRewards == other.isShowRewards() &&
            this.showPayBills == other.isShowPayBills() &&
            ((this.currencyCodes==null && other.getCurrencyCodes()==null) || 
             (this.currencyCodes!=null &&
              java.util.Arrays.equals(this.currencyCodes, other.getCurrencyCodes()))) &&
            ((this.paymentOptions==null && other.getPaymentOptions()==null) || 
             (this.paymentOptions!=null &&
              java.util.Arrays.equals(this.paymentOptions, other.getPaymentOptions()))) &&
            this.enabled == other.isEnabled() &&
            ((this.properties==null && other.getProperties()==null) || 
             (this.properties!=null &&
              java.util.Arrays.equals(this.properties, other.getProperties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMgoAgentIdentifier() != null) {
            _hashCode += getMgoAgentIdentifier().hashCode();
        }
        if (getAgentSequence() != null) {
            _hashCode += getAgentSequence().hashCode();
        }
        _hashCode += new Long(getAgentId()).hashCode();
        _hashCode += getProfileId();
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getMaxTransactionAmount() != null) {
            _hashCode += getMaxTransactionAmount().hashCode();
        }
        _hashCode += (isShowRewards() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isShowPayBills() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCurrencyCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCurrencyCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCurrencyCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPaymentOptions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentOptions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentOptions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isEnabled() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Profile.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Profile"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mgoAgentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "mgoAgentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentSequence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "agentSequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "agentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "profileId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "productName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxTransactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "maxTransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "IDREF"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("showRewards");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "showRewards"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("showPayBills");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "showPayBills"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "currencyCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "CurrencyCode"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentOptions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "paymentOptions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "PaymentOption"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enabled");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "enabled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("properties");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/config_v2/profile", "Property"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
