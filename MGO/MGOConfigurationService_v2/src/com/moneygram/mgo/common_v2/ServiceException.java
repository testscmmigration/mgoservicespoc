/**
 * ServiceException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.common_v2;

public class ServiceException  extends org.apache.axis.AxisFault  implements java.io.Serializable {
    private java.lang.String errorCode;

    /* Service/Application Name */
    private java.lang.String errorSource;

    private java.lang.String errorMessage;

    private java.lang.String errorStackTrace;

    /* Step in processing that caused the error */
    private java.lang.String errorLocation;

    private com.moneygram.mgo.common_v2.ErrorHandlingCode errorHandlingCode;

    /* Indicates client or system error. */
    private com.moneygram.mgo.common_v2.ErrorCategoryCode errorCategoryCode;

    /* List of related errors. Example: required field validation. */
    private com.moneygram.mgo.common_v2.RelatedError[] relatedErrors;

    public ServiceException() {
    }

    public ServiceException(
           java.lang.String errorCode,
           java.lang.String errorSource,
           java.lang.String errorMessage,
           java.lang.String errorStackTrace,
           java.lang.String errorLocation,
           com.moneygram.mgo.common_v2.ErrorHandlingCode errorHandlingCode,
           com.moneygram.mgo.common_v2.ErrorCategoryCode errorCategoryCode,
           com.moneygram.mgo.common_v2.RelatedError[] relatedErrors) {
        this.errorCode = errorCode;
        this.errorSource = errorSource;
        this.errorMessage = errorMessage;
        this.errorStackTrace = errorStackTrace;
        this.errorLocation = errorLocation;
        this.errorHandlingCode = errorHandlingCode;
        this.errorCategoryCode = errorCategoryCode;
        this.relatedErrors = relatedErrors;
    }


    /**
     * Gets the errorCode value for this ServiceException.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this ServiceException.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorSource value for this ServiceException.
     * 
     * @return errorSource   * Service/Application Name
     */
    public java.lang.String getErrorSource() {
        return errorSource;
    }


    /**
     * Sets the errorSource value for this ServiceException.
     * 
     * @param errorSource   * Service/Application Name
     */
    public void setErrorSource(java.lang.String errorSource) {
        this.errorSource = errorSource;
    }


    /**
     * Gets the errorMessage value for this ServiceException.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this ServiceException.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the errorStackTrace value for this ServiceException.
     * 
     * @return errorStackTrace
     */
    public java.lang.String getErrorStackTrace() {
        return errorStackTrace;
    }


    /**
     * Sets the errorStackTrace value for this ServiceException.
     * 
     * @param errorStackTrace
     */
    public void setErrorStackTrace(java.lang.String errorStackTrace) {
        this.errorStackTrace = errorStackTrace;
    }


    /**
     * Gets the errorLocation value for this ServiceException.
     * 
     * @return errorLocation   * Step in processing that caused the error
     */
    public java.lang.String getErrorLocation() {
        return errorLocation;
    }


    /**
     * Sets the errorLocation value for this ServiceException.
     * 
     * @param errorLocation   * Step in processing that caused the error
     */
    public void setErrorLocation(java.lang.String errorLocation) {
        this.errorLocation = errorLocation;
    }


    /**
     * Gets the errorHandlingCode value for this ServiceException.
     * 
     * @return errorHandlingCode
     */
    public com.moneygram.mgo.common_v2.ErrorHandlingCode getErrorHandlingCode() {
        return errorHandlingCode;
    }


    /**
     * Sets the errorHandlingCode value for this ServiceException.
     * 
     * @param errorHandlingCode
     */
    public void setErrorHandlingCode(com.moneygram.mgo.common_v2.ErrorHandlingCode errorHandlingCode) {
        this.errorHandlingCode = errorHandlingCode;
    }


    /**
     * Gets the errorCategoryCode value for this ServiceException.
     * 
     * @return errorCategoryCode   * Indicates client or system error.
     */
    public com.moneygram.mgo.common_v2.ErrorCategoryCode getErrorCategoryCode() {
        return errorCategoryCode;
    }


    /**
     * Sets the errorCategoryCode value for this ServiceException.
     * 
     * @param errorCategoryCode   * Indicates client or system error.
     */
    public void setErrorCategoryCode(com.moneygram.mgo.common_v2.ErrorCategoryCode errorCategoryCode) {
        this.errorCategoryCode = errorCategoryCode;
    }


    /**
     * Gets the relatedErrors value for this ServiceException.
     * 
     * @return relatedErrors   * List of related errors. Example: required field validation.
     */
    public com.moneygram.mgo.common_v2.RelatedError[] getRelatedErrors() {
        return relatedErrors;
    }


    /**
     * Sets the relatedErrors value for this ServiceException.
     * 
     * @param relatedErrors   * List of related errors. Example: required field validation.
     */
    public void setRelatedErrors(com.moneygram.mgo.common_v2.RelatedError[] relatedErrors) {
        this.relatedErrors = relatedErrors;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceException)) return false;
        ServiceException other = (ServiceException) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorSource==null && other.getErrorSource()==null) || 
             (this.errorSource!=null &&
              this.errorSource.equals(other.getErrorSource()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.errorStackTrace==null && other.getErrorStackTrace()==null) || 
             (this.errorStackTrace!=null &&
              this.errorStackTrace.equals(other.getErrorStackTrace()))) &&
            ((this.errorLocation==null && other.getErrorLocation()==null) || 
             (this.errorLocation!=null &&
              this.errorLocation.equals(other.getErrorLocation()))) &&
            ((this.errorHandlingCode==null && other.getErrorHandlingCode()==null) || 
             (this.errorHandlingCode!=null &&
              this.errorHandlingCode.equals(other.getErrorHandlingCode()))) &&
            ((this.errorCategoryCode==null && other.getErrorCategoryCode()==null) || 
             (this.errorCategoryCode!=null &&
              this.errorCategoryCode.equals(other.getErrorCategoryCode()))) &&
            ((this.relatedErrors==null && other.getRelatedErrors()==null) || 
             (this.relatedErrors!=null &&
              java.util.Arrays.equals(this.relatedErrors, other.getRelatedErrors())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorSource() != null) {
            _hashCode += getErrorSource().hashCode();
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getErrorStackTrace() != null) {
            _hashCode += getErrorStackTrace().hashCode();
        }
        if (getErrorLocation() != null) {
            _hashCode += getErrorLocation().hashCode();
        }
        if (getErrorHandlingCode() != null) {
            _hashCode += getErrorHandlingCode().hashCode();
        }
        if (getErrorCategoryCode() != null) {
            _hashCode += getErrorCategoryCode().hashCode();
        }
        if (getRelatedErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRelatedErrors());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRelatedErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceException.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorSource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorStackTrace");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorStackTrace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorHandlingCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorHandlingCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorHandlingCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCategoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "errorCategoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorCategoryCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relatedErrors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "relatedErrors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
