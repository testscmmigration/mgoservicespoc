/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.config.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentResponse;
import com.moneygram.mgo.service.config.GetReceiveAgentsRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentsResponse;
import com.moneygram.mgo.service.config.GetReceiveCountriesRequest;
import com.moneygram.mgo.service.config.GetReceiveCountriesResponse;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesRequest;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesResponse;

/**
 * 
 * Configuration Service Soap Handler.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.8 $ $Date: 2011/08/01 17:10:28 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class ConfigurationServiceSoapHandler extends BaseSoapRequestHandler {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConfigurationServiceSoapHandler.class);

	/**
	 * 
	 * Creates new instance of ConfigurationServiceSoapHandler.
	 */
	public ConfigurationServiceSoapHandler() {
		super();
		setServiceFactory(new MGOServiceFactory());
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ServiceException
	 */
	public GetReceiveCountriesResponse getReceiveCountries(
			GetReceiveCountriesRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveCountries: request=" + request);
		}
		return (GetReceiveCountriesResponse) process(request);
	}

	public GetReceiveAgentResponse getReceiveAgent(
			GetReceiveAgentRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveAgent: request=" + request);
		}
		return (GetReceiveAgentResponse) process(request);
	}

	public GetReceiveAgentsResponse getReceiveAgents(
			GetReceiveAgentsRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveAgents: request=" + request);
		}
		return (GetReceiveAgentsResponse) process(request);
	}
	
	public GetSendCountryCurrenciesResponse getSendCountryCurrencies(GetSendCountryCurrenciesRequest request){
		//String message = "you said " + request.getMessage() + ", I say hallo worrrld!";
		
		try {
			return (GetSendCountryCurrenciesResponse) process(request);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
		
	}

	@Override
	protected String getServiceJndiName() {
		return "java:comp/env/ejb/MGOService";
	}

}
