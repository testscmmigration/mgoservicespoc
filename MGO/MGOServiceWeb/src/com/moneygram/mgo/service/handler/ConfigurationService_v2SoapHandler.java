/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.config_v2.GetProfileRequest;
import com.moneygram.mgo.service.config_v2.GetProfileResponse;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentResponse;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentsRequest;
import com.moneygram.mgo.service.config_v2.GetReceiveAgentsResponse;
import com.moneygram.mgo.service.config_v2.GetReceiveCountriesRequest;
import com.moneygram.mgo.service.config_v2.GetReceiveCountriesResponse;
import com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesRequest;
import com.moneygram.mgo.service.config_v2.GetSendCountryCurrenciesResponse;
import com.moneygram.mgo.service.config_v2.GetSiteProfilesRequest;
import com.moneygram.mgo.service.config_v2.GetSiteProfilesResponse;



/**
 * 
 * Configuration Service Soap Handler.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.8 $ $Date: 2011/08/01 17:10:28 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class ConfigurationService_v2SoapHandler extends BaseSoapRequestHandler {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConfigurationService_v2SoapHandler.class);

	/**
	 * 
	 * Creates new instance of ConfigurationServiceSoapHandler.
	 */
	public ConfigurationService_v2SoapHandler() {
		super();
		setServiceFactory(new MGOServiceFactory());
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ServiceException
	 */
	public GetReceiveCountriesResponse getReceiveCountries(
			GetReceiveCountriesRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveCountries: request=" + request);
		}
		return (GetReceiveCountriesResponse) process((OperationRequest) request);
	}

	public GetReceiveAgentResponse getReceiveAgent(
			GetReceiveAgentRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveAgent: request=" + request);
		}
		return (GetReceiveAgentResponse) process((OperationRequest) request);
	}

	public GetReceiveAgentsResponse getReceiveAgents(
			GetReceiveAgentsRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getReceiveAgents: request=" + request);
		}
		return (GetReceiveAgentsResponse) process((OperationRequest) request);
	}
	
	public GetSendCountryCurrenciesResponse getSendCountryCurrencies(GetSendCountryCurrenciesRequest request){		
		try {
			return (GetSendCountryCurrenciesResponse) process((OperationRequest) request);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} 
		
	}
	
	public GetProfileResponse getProfile(GetProfileRequest request){		
		try {
			return (GetProfileResponse) process((OperationRequest) request);
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		} 
	}
	
	public GetSiteProfilesResponse getSiteProfiles(GetSiteProfilesRequest request){		
		try {
			return (GetSiteProfilesResponse) process((OperationRequest) request);
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		} 
	}

	@Override
	protected String getServiceJndiName() {
		return "java:comp/env/ejb/MGOService";
	}

}
