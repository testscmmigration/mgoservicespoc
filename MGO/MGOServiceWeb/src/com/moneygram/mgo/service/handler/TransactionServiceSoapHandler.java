/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.transaction.CountConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.CountConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.GetCCAuthConfIdRequest;
import com.moneygram.mgo.service.transaction.GetCCAuthConfIdResponse;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsTotalRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsTotalResponse;
import com.moneygram.mgo.service.transaction.GetMaxStateRegulatoryVersionRequest;
import com.moneygram.mgo.service.transaction.GetMaxStateRegulatoryVersionResponse;
import com.moneygram.mgo.service.transaction.GetStateRegulatoryInfoRequest;
import com.moneygram.mgo.service.transaction.GetStateRegulatoryInfoResponse;
import com.moneygram.mgo.service.transaction.GetTranActionsRequest;
import com.moneygram.mgo.service.transaction.GetTranActionsResponse;
import com.moneygram.mgo.service.transaction.GetTransactionRequest;
import com.moneygram.mgo.service.transaction.GetTransactionResponse;
import com.moneygram.mgo.service.transaction.SaveStateRegulatoryInfoRequest;
import com.moneygram.mgo.service.transaction.SaveStateRegulatoryInfoResponse;
import com.moneygram.mgo.service.transaction.SaveTransactionCommentRequest;
import com.moneygram.mgo.service.transaction.SaveTransactionCommentResponse;
import com.moneygram.mgo.service.transaction.SaveTransactionRequest;
import com.moneygram.mgo.service.transaction.SaveTransactionResponse;
import com.moneygram.mgo.service.transaction.UpdateAccountRequest;
import com.moneygram.mgo.service.transaction.UpdateAccountResponse;
import com.moneygram.mgo.service.transaction.UpdateTransactionRequest;
import com.moneygram.mgo.service.transaction.UpdateTransactionResponse;
import com.moneygram.mgo.service.transaction.UpdateTransactionStatusRequest;
import com.moneygram.mgo.service.transaction.UpdateTransactionStatusResponse;

/**
 *
 * Transaction Service Soap Handler.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.11 $ $Date: 2011/04/25 04:56:09 $ </td><tr><td>
 * @author   </td><td>$Author: uo87 $ </td>
 *</table>
 *</div>
 */
public class TransactionServiceSoapHandler extends BaseSoapRequestHandler {
    private static final Logger logger = LogFactory.getInstance().getLogger(TransactionServiceSoapHandler.class);

    /**
     *
     * Creates new instance of TransactionServiceSoapHandler.
     */
    public TransactionServiceSoapHandler() {
        super();
        setServiceFactory(new MGOServiceFactory());
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public SaveTransactionResponse save(SaveTransactionRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("save: request=" + request);
        }
        return (SaveTransactionResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public UpdateTransactionResponse update(UpdateTransactionRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("update: request=" + request);
        }
        return (UpdateTransactionResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public GetTransactionResponse get(GetTransactionRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("get: request=" + request);
        }
        return (GetTransactionResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public GetTranActionsResponse getTranActions(GetTranActionsRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getTranActions: request=" + request);
        }
        return (GetTranActionsResponse) process(request);
    }

    /**
    *
    * @param request
    * @return
    * @throws ServiceException
    */
   public GetConsumerTransactionsResponse getConsumerTransactions(GetConsumerTransactionsRequest request) throws ServiceException {
       if (logger.isDebugEnabled()) {
           logger.debug("getConsumerTransactions: request=" + request);
       }
       return (GetConsumerTransactionsResponse) process(request);
   }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public CountConsumerTransactionsResponse countConsumerTransactions(CountConsumerTransactionsRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("countConsumerTransactions: request=" + request);
        }
        return (CountConsumerTransactionsResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public UpdateTransactionStatusResponse updateTransactionStatus(UpdateTransactionStatusRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("updateTransactionStatus: request=" + request);
        }
        return (UpdateTransactionStatusResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public UpdateAccountResponse updateAccount(UpdateAccountRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("updateAccount: request=" + request);
        }
        return (UpdateAccountResponse) process(request);
    }

    /**
     *
     * @param request
     * @return
     * @throws ServiceException
     */
    public GetConsumerTransactionsTotalResponse getConsumerTransactionsTotal(GetConsumerTransactionsTotalRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerTransactionsTotal: request=" + request);
        }
        return (GetConsumerTransactionsTotalResponse) process(request);
    }

    public SaveStateRegulatoryInfoResponse saveStateRegulatoryInfo(SaveStateRegulatoryInfoRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("saveStateRegulatoryInfo: request=" + request);
        }
        return (SaveStateRegulatoryInfoResponse) process(request);
    }

    public GetStateRegulatoryInfoResponse getStateRegulatoryInfo(GetStateRegulatoryInfoRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getStateRegulatoryInfo: request=" + request);
        }
        return (GetStateRegulatoryInfoResponse) process(request);
    }

    public GetMaxStateRegulatoryVersionResponse getMaxStateRegulatoryVersion(GetMaxStateRegulatoryVersionRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getMaxStateRegulatoryVersion: request=" + request);
        }
        return (GetMaxStateRegulatoryVersionResponse) process(request);
    }
    
    /**
    *
    * @param request
    * @return
    * @throws ServiceException
    */
   public SaveTransactionCommentResponse saveTransactionComment(SaveTransactionCommentRequest request) throws ServiceException {
       if (logger.isDebugEnabled()) {
           logger.debug("saveTransactionComment: request=" + request);
       }
       return (SaveTransactionCommentResponse) process(request);
   }

   public GetCCAuthConfIdResponse getCCAuthConfId(GetCCAuthConfIdRequest request) throws ServiceException {
       if (logger.isDebugEnabled()) {
           logger.debug("getCCAuthConfId: request=" + request);
       }
       return (GetCCAuthConfIdResponse) process(request);
   }

   @Override
    protected String getServiceJndiName() {
        return "java:comp/env/ejb/MGOService";
    }

}
