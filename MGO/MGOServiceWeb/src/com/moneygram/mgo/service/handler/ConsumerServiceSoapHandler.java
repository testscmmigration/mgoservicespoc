/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.consumer.AddAccountRequest;
import com.moneygram.mgo.service.consumer.AddAccountResponse;
import com.moneygram.mgo.service.consumer.CreateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer.CreateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer.FindConsumersRequest;
import com.moneygram.mgo.service.consumer.FindConsumersResponse;
import com.moneygram.mgo.service.consumer.GetConsumerBlockedStatusRequest;
import com.moneygram.mgo.service.consumer.GetConsumerBlockedStatusResponse;
import com.moneygram.mgo.service.consumer.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer.GetIncompleteProfilesRequest;
import com.moneygram.mgo.service.consumer.GetIncompleteProfilesResponse;
import com.moneygram.mgo.service.consumer.GetProfileEventsRequest;
import com.moneygram.mgo.service.consumer.GetProfileEventsResponse;
import com.moneygram.mgo.service.consumer.SaveLogonTryRequest;
import com.moneygram.mgo.service.consumer.SaveLogonTryResponse;
import com.moneygram.mgo.service.consumer.SaveProfileEventRequest;
import com.moneygram.mgo.service.consumer.SaveProfileEventResponse;
import com.moneygram.mgo.service.consumer.UpdateAccountRequest;
import com.moneygram.mgo.service.consumer.UpdateAccountResponse;
import com.moneygram.mgo.service.consumer.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer.UpdateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer.GetCCLowAuthSequencerValueRequest;
import com.moneygram.mgo.service.consumer.GetCCLowAuthSequencerValueResponse;

/**
 * 
 * Consumer Service Soap Handler. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOTransactionService</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.11 $ $Date: 2010/01/12 18:42:04 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: w162 $ </td>
 *         </table>
 *         </div>
 */
public class ConsumerServiceSoapHandler extends BaseSoapRequestHandler {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerServiceSoapHandler.class);

	public ConsumerServiceSoapHandler() {
		super();
		setServiceFactory(new MGOServiceFactory());
	}

	public GetConsumerProfileResponse get(GetConsumerProfileRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("get: request=" + request);
		}
		return (GetConsumerProfileResponse) process(request);
	}

	public UpdateConsumerProfileResponse update(
			UpdateConsumerProfileRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("update: request=" + request);
		}
		return (UpdateConsumerProfileResponse) process(request);
	}

	public CreateConsumerProfileResponse create(
			CreateConsumerProfileRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("create: request=" + request);
		}
		return (CreateConsumerProfileResponse) process(request);
	}

	public UpdateAccountResponse updateAccount(UpdateAccountRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("updateAccount: request=" + request);
		}
		return (UpdateAccountResponse) process(request);
	}

	public AddAccountResponse addAccount(AddAccountRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("addAccount: request=" + request);
		}
		return (AddAccountResponse) process(request);
	}

	public GetConsumerBlockedStatusResponse getBlockedStatus(
			GetConsumerBlockedStatusRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getBlockedStatus: request=" + request);
		}
		return (GetConsumerBlockedStatusResponse) process(request);
	}

	public FindConsumersResponse findConsumers(FindConsumersRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("findConsumers: request=" + request);
		}
		return (FindConsumersResponse) process(request);
	}

	public GetIncompleteProfilesResponse getIncompleteProfiles(
			GetIncompleteProfilesRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getIncompleteProfiles: request=" + request);
		}
		return (GetIncompleteProfilesResponse) process(request);
	}

	public GetProfileEventsResponse getProfileEvents(
			GetProfileEventsRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getProfileEvents: request=" + request);
		}
		return (GetProfileEventsResponse) process(request);
	}
	
	public SaveProfileEventResponse saveProfileEvent(
			SaveProfileEventRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("saveProfileEvent: request=" + request);
		}
		return (SaveProfileEventResponse) process(request);
	}

	public SaveLogonTryResponse saveLogonTry(
			SaveLogonTryRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("saveLogonTry: request=" + request);
		}
		return (SaveLogonTryResponse) process(request);
	}
	
	
	public GetCCLowAuthSequencerValueResponse getCCLowAuthSequencerValue(
			GetCCLowAuthSequencerValueRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getCCLowAuthSequencerValue: request=" + request);
		}
		return (GetCCLowAuthSequencerValueResponse) process(request);
	}
	
	
	@Override
	protected String getServiceJndiName() {
		return "java:comp/env/ejb/MGOService";
	}
}
