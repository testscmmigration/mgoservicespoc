/*
 * Created on Sep 24, 2012
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.consumer_v2.AddAccountRequest;
import com.moneygram.mgo.service.consumer_v2.AddAccountResponse;
import com.moneygram.mgo.service.consumer_v2.AddImageRequest;
import com.moneygram.mgo.service.consumer_v2.AddImageResponse;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.FindConsumersRequest;
import com.moneygram.mgo.service.consumer_v2.FindConsumersResponse;
import com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueRequest;
import com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdRequest;
import com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesRequest;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse;
import com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest;
import com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse;
import com.moneygram.mgo.service.consumer_v2.GetProfileEventsRequest;
import com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationRequest;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse;
import com.moneygram.mgo.service.consumer_v2.SaveLogonTryRequest;
import com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse;
import com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest;
import com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse;
import com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationRequest;
import com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse;
import com.moneygram.mgo.service.consumer_v2.UpdateAccountRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse;

/**
 * 
 * Consumer Service v2 Soap Handler. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOConsumerService_v2</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2012</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.0 $ $Date: 2012/09/124 11:20:00 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: vl58 $ </td>
 *         </table>
 *         </div>
 */
public class ConsumerService_v2SoapHandler extends BaseSoapRequestHandler {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerService_v2SoapHandler.class);

	public ConsumerService_v2SoapHandler() {
		super();
		setServiceFactory(new MGOServiceFactory());
	}

	public GetConsumerProfileResponse get(GetConsumerProfileRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("get: request=" + request);
		}
		return (GetConsumerProfileResponse) process(request);
	}

	public UpdateConsumerProfileResponse update(
			UpdateConsumerProfileRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("update: request=" + request);
		}
		return (UpdateConsumerProfileResponse) process(request);
	}

	public CreateConsumerProfileResponse create(
			CreateConsumerProfileRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("create: request=" + request);
		}
		return (CreateConsumerProfileResponse) process(request);
	}

	public UpdateAccountResponse updateAccount(UpdateAccountRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("updateAccount: request=" + request);
		}
		return (UpdateAccountResponse) process(request);
	}

	public AddAccountResponse addAccount(AddAccountRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("addAccount: request=" + request);
		}
		return (AddAccountResponse) process(request);
	}

	public GetConsumerBlockedStatusResponse getBlockedStatus(
			GetConsumerBlockedStatusRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getBlockedStatus: request=" + request);
		}
		return (GetConsumerBlockedStatusResponse) process(request);
	}

	public FindConsumersResponse findConsumers(FindConsumersRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("findConsumers: request=" + request);
		}
		return (FindConsumersResponse) process(request);
	}

	public GetIncompleteProfilesResponse getIncompleteProfiles(
			GetIncompleteProfilesRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getIncompleteProfiles: request=" + request);
		}
		return (GetIncompleteProfilesResponse) process(request);
	}

	public GetProfileEventsResponse getProfileEvents(
			GetProfileEventsRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getProfileEvents: request=" + request);
		}
		return (GetProfileEventsResponse) process(request);
	}
	
	public SaveProfileEventResponse saveProfileEvent(
			SaveProfileEventRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("saveProfileEvent: request=" + request);
		}
		return (SaveProfileEventResponse) process(request);
	}

	public SaveLogonTryResponse saveLogonTry(
			SaveLogonTryRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("saveLogonTry: request=" + request);
		}
		return (SaveLogonTryResponse) process(request);
	}
	
	public GetOccupationsListResponse getOccupationsList(
			GetOccupationsListRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getOccupationsList: request=" + request);
		}
		return (GetOccupationsListResponse) process(request);
	}
	
	
	public AddImageResponse addImage(
			AddImageRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("AddImage: request=" + request);
		}
		return (AddImageResponse) process(request);
	}
	public GetConsumerIDImageResponse getConsumerIDImage(
			GetConsumerIDImageRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("GetConsumerIDImage: request=" + request);
		}
		return (GetConsumerIDImageResponse) process(request);
	}
	public UpdateConsumerIDImageResponse updateConsumerIDImage(
			UpdateConsumerIDImageRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("UpdateConsumerIDImage: request=" + request);
		}
		return (UpdateConsumerIDImageResponse) process(request);
	}
	public GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatus(
			GetConsumerIDApprovalStatusRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("GetConsumerIDApprovalStatus: request=" + request);
		}
		return (GetConsumerIDApprovalStatusResponse) process(request);
	}
	public UpdateConsumerIDImageStatusResponse updateConsumerIDImageStatus(
			UpdateConsumerIDImageStatusRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("UpdateConsumerIDImageStatus: request=" + request);
		}
		return (UpdateConsumerIDImageStatusResponse) process(request);
	}
	
	public GetValidateRegistrationInformationResponse getValidateRegistrationInformation(
			GetValidateRegistrationInformationRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("GetValidateRegistrationInformation: request=" + request);
		}
		return (GetValidateRegistrationInformationResponse) process(request);
	}
	public SaveValidateRegistrationInformationResponse saveValidateRegistrationInformation(
			SaveValidateRegistrationInformationRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("SaveValidateRegistrationInformation: request=" + request);
		}
		return (SaveValidateRegistrationInformationResponse) process(request);
	}
	
	//Start - MGO-750: bailer job modifications
	public GetIncompleteFirstTxProfilesResponse getIncompleteFirstTxProfiles(
			GetIncompleteFirstTxProfilesRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getIncompleteFirstTxProfiles: request=" + request);
		}
		return (GetIncompleteFirstTxProfilesResponse) process(request);
	}
	//End - MGO-750: bailer job modifications
	
	
	public GetCCLowAuthSequencerValueResponse getCCLowAuthSequencerValue(
			GetCCLowAuthSequencerValueRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getCCLowAuthSequencerValue: request=" + request);
		}
		return (GetCCLowAuthSequencerValueResponse) process(request);
	}
	
	public GetGlobalConsumerProfileIdResponse getGlobalConsumerProfileId(
			GetGlobalConsumerProfileIdRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("getOccupationsList: request=" + request);
		}
		return (GetGlobalConsumerProfileIdResponse) process(request);
	}
	@Override
	protected String getServiceJndiName() {
		return "java:comp/env/ejb/MGOService";
	}
}
