/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.bailer.ProcessBailerEventsRequest;
import com.moneygram.mgo.service.bailer.ProcessBailerEventsResponse;
import com.moneygram.mgo.service.bailer.ProcessFirstTxBailerEventsRequest;
import com.moneygram.mgo.service.bailer.ProcessFirstTxBailerEventsResponse;
import com.moneygram.mgo.service.bailer_v1.ProcessActionRequest;
import com.moneygram.mgo.service.bailer_v1.ProcessActionResponse;

/**
 * 
 * Consumer Service Soap Handler. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOTransactionService</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.1 $ $Date: 2009/12/23 14:43:20 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: w162 $ </td>
 *         </table>
 *         </div>
 */
public class BailerServiceSoapHandler extends BaseSoapRequestHandler {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			BailerServiceSoapHandler.class);

	public BailerServiceSoapHandler() {
		super();
		setServiceFactory(new MGOServiceFactory());
	}

	public ProcessBailerEventsResponse processBailerEvents(ProcessBailerEventsRequest request)
			throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("processBailerEvents: request=" + request);
		}
		return (ProcessBailerEventsResponse) process(request);
	}
	
	public ProcessFirstTxBailerEventsResponse processFirstTxBailerEvents(
			ProcessFirstTxBailerEventsRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("processFirstTxBailerEvents: request=" + request);
		}
		return (ProcessFirstTxBailerEventsResponse) process(request);
	}
	//Start MGO 11539: MGOJob modifications
	public ProcessActionResponse processAction(
			ProcessActionRequest request) throws ServiceException {
		if (logger.isDebugEnabled()) {
			logger.debug("processAction: request=" + request);
		}
		return (ProcessActionResponse) process(request);
	}
	//End MGO 11539: MGOJob modifications
	
	@Override
	protected String getServiceJndiName() {
		return "java:comp/env/ejb/MGOService";
	}
}
