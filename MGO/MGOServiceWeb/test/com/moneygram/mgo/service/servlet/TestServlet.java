/*
 * Created on Jun 10, 2009
 *
 */
package com.moneygram.mgo.service.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * TestServlet
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/21 22:00:44 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TestServlet extends HttpServlet {
    private static final Logger logger = LogFactory.getInstance().getLogger(TestServlet.class);
    
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Processes the request by routing them. 
     * @param request request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("processRequest: request="+ request);
        }
        String configFile = "MGOServiceCache.xml";
        String entryFactoryConfigFile = "MGOServiceCacheEntryFactoryConfig";
        
        ResourceBundle resourceBundle = null;
        try {
            resourceBundle = ResourceBundle.getBundle(entryFactoryConfigFile);
            if (logger.isDebugEnabled()) {
                logger.debug("processRequest: keys="+ Collections.list(resourceBundle.getKeys()));
            }
        } catch (Exception e) {
            throw new ServletException("Failed to load cache entry factory configuration from file="+ entryFactoryConfigFile, e);
        }

        
//        CacheManager manager;
//        try {
//            manager = CacheManagerFactory.createCacheManager(configFile, entryFactoryConfigFile);
//        } catch (CacheException e) {
//            throw new ServletException("Failed to create cache manager", e);
//        }
//        String[] caches = manager.getCacheNames();
//        if (logger.isDebugEnabled()) {
//            logger.debug("processRequest: caches: "+ (caches == null ? null : Arrays.asList(caches)));
//        }
        
        String responseContent = Calendar.getInstance().getTime()+ ": "+ getClass().getName() + ".processRequest: request="+ request;
        response.setContentLength(responseContent.length());
        Writer writer = response.getWriter();
        writer.write(responseContent);
        response.flushBuffer();
    }
}
