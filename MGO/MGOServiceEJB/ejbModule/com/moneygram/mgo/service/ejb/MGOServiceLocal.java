package com.moneygram.mgo.service.ejb;

import com.moneygram.common.service.ServiceProcessor;


/**
 * Local interface for Enterprise Bean: MGOService
 */
public interface MGOServiceLocal extends javax.ejb.EJBLocalObject, ServiceProcessor {
}
