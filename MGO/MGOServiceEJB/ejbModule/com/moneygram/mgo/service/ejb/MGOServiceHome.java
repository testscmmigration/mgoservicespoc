package com.moneygram.mgo.service.ejb;

/**
 * Home interface for Enterprise Bean: MGOService
 */
public interface MGOServiceHome extends javax.ejb.EJBHome {

    /**
     * Creates a default instance of Session Bean: MGOService
     */
    public com.moneygram.mgo.service.ejb.MGOService create() throws javax.ejb.CreateException, java.rmi.RemoteException;
}
