package com.moneygram.mgo.service.ejb;

/**
 * Local Home interface for Enterprise Bean: MGOService
 */
public interface MGOServiceLocalHome extends javax.ejb.EJBLocalHome {

    /**
     * Creates a default instance of Session Bean: MGOService
     */
    public com.moneygram.mgo.service.ejb.MGOServiceLocal create() throws javax.ejb.CreateException;
}
