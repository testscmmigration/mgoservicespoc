/*
 * Created on Jun 19, 2009
 *
 */
package com.moneygram.mgo.service.handler;

import javax.ejb.EJBException;
import javax.ejb.EJBLocalHome;
import javax.ejb.EJBLocalObject;
import javax.ejb.RemoveException;

import com.moneygram.common.service.ServiceProcessor;
import com.moneygram.common.service.ServiceException;
import com.moneygram.mgo.service.ejb.MGOServiceBean;
import com.moneygram.mgo.service.ejb.MGOServiceLocal;

/**
 * 
 * MGO Service Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceEJB</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/08/05 21:00:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TestMGOServiceFactory extends MGOServiceFactory {

    /**
     * Returns an instance of MGOService.
     * @return instance of MGOService.
     * @throws ServiceException
     */
    public ServiceProcessor createService(String jndi) throws ServiceException {
        return new TestMGOService();
    }
    
    class TestMGOService extends MGOServiceBean implements MGOServiceLocal {
        private static final long serialVersionUID = 1L;

        public EJBLocalHome getEJBLocalHome() throws EJBException {
            return null;
        }

        public Object getPrimaryKey() throws EJBException {
            return null;
        }

        public boolean isIdentical(EJBLocalObject arg0) throws EJBException {
            return false;
        }

        public void remove() throws RemoveException, EJBException {
        }
        
    }

}
