/**
 * ReceiveAgent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

public class ReceiveAgent implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private long agentId;
	private java.math.BigDecimal limitAmount;
	private String emgMerchantId;

	public ReceiveAgent() {
	}

	/**
	 * Gets the agentId value for this ReceiveAgent.
	 *
	 * @return agentId
	 */
	public long getAgentId() {
		return agentId;
	}

	/**
	 * Sets the agentId value for this ReceiveAgent.
	 *
	 * @param agentId
	 */
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	/**
	 * Gets the limitAmount value for this ReceiveAgent.
	 *
	 * @return limitAmount
	 */
	public java.math.BigDecimal getLimitAmount() {
		return limitAmount;
	}

	/**
	 * Sets the limitAmount value for this ReceiveAgent.
	 *
	 * @param limitAmount
	 */
	public void setLimitAmount(java.math.BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}

	public String getEmgMerchantId() {
		return emgMerchantId;
	}

	public void setEmgMerchantId(String emgMerchantId) {
		this.emgMerchantId = emgMerchantId;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ReceiveAgent))
			return false;
		ReceiveAgent other = (ReceiveAgent) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& this.agentId == other.getAgentId()
				&& ((this.limitAmount == null && other.getLimitAmount() == null) || (this.limitAmount != null && this.limitAmount
						.equals(other.getLimitAmount())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		_hashCode += new Long(getAgentId()).hashCode();
		if (getLimitAmount() != null) {
			_hashCode += getLimitAmount().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" super=").append(super.toString());
		buffer.append(" AgentId=").append(getAgentId());
		buffer.append(" LimitAmount=").append(getLimitAmount());
		return buffer.toString();
	}
}
