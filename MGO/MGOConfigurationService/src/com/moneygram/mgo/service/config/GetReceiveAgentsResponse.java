/**
 * GetReceiveAgentsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationResponse;

public class GetReceiveAgentsResponse extends BaseOperationResponse {
	private static final long serialVersionUID = 1L;

	private ReceiveAgent[] receiveAgents;

	public GetReceiveAgentsResponse() {
	}

	/**
	 * Gets the receiveAgents value for this GetReceiveAgentsResponse.
	 * 
	 * @return receiveAgents
	 */
	public ReceiveAgent[] getReceiveAgents() {
		return receiveAgents;
	}

	/**
	 * Sets the receiveAgents value for this GetReceiveAgentsResponse.
	 * 
	 * @param receiveAgents
	 */
	public void setReceiveAgents(ReceiveAgent[] receiveAgents) {
		this.receiveAgents = receiveAgents;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetReceiveAgentsResponse))
			return false;
		GetReceiveAgentsResponse other = (GetReceiveAgentsResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.receiveAgents == null && other.getReceiveAgents() == null) || (this.receiveAgents != null && java.util.Arrays
						.equals(this.receiveAgents, other.getReceiveAgents())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getReceiveAgents() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getReceiveAgents()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getReceiveAgents(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ReceiveAgents=").append(getReceiveAgents());
		buffer.append(" ]");
		return buffer.toString();
	}
}
