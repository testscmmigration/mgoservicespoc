/**
 * GetReceiveAgentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationResponse;

public class GetReceiveAgentResponse  extends BaseOperationResponse {
	private static final long serialVersionUID = 1L;
	
	private ReceiveAgent receiveAgent;

    public GetReceiveAgentResponse() {
    }

    /**
     * Gets the receiveAgent value for this GetReceiveAgentResponse.
     * 
     * @return receiveAgent
     */
    public ReceiveAgent getReceiveAgent() {
        return receiveAgent;
    }


    /**
     * Sets the receiveAgent value for this GetReceiveAgentResponse.
     * 
     * @param receiveAgent
     */
    public void setReceiveAgent(ReceiveAgent receiveAgent) {
        this.receiveAgent = receiveAgent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReceiveAgentResponse)) return false;
        GetReceiveAgentResponse other = (GetReceiveAgentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.receiveAgent==null && other.getReceiveAgent()==null) || 
             (this.receiveAgent!=null &&
              this.receiveAgent.equals(other.getReceiveAgent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getReceiveAgent() != null) {
            _hashCode += getReceiveAgent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ReceiveAgent=").append(getReceiveAgent());
		buffer.append(" ]");
		return buffer.toString();
	}
}
