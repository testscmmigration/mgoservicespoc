/**
 * GetReceiveCountriesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Get Receive Countries Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 16:13:05 $ </td><tr><td>
 * @author   </td><td>$Author: a145 $ </td>
 *</table>
 *</div>
 */
public class GetReceiveCountriesRequest extends BaseOperationRequest {
	private static final long serialVersionUID = 1L;

	private java.lang.String sendCountry;

	public GetReceiveCountriesRequest() {
	}

	public GetReceiveCountriesRequest(RequestHeader header,
			java.lang.String sendCountry) {
		super(header);
		this.sendCountry = sendCountry;
	}

	/**
	 * Gets the sendCountry value for this GetReceiveCountriesRequest.
	 * 
	 * @return sendCountry
	 */
	public java.lang.String getSendCountry() {
		return sendCountry;
	}

	/**
	 * Sets the sendCountry value for this GetReceiveCountriesRequest.
	 * 
	 * @param sendCountry
	 */
	public void setSendCountry(java.lang.String sendCountry) {
		this.sendCountry = sendCountry;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetReceiveCountriesRequest))
			return false;
		GetReceiveCountriesRequest other = (GetReceiveCountriesRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.sendCountry == null && other.getSendCountry() == null) || (this.sendCountry != null && this.sendCountry
						.equals(other.getSendCountry())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getSendCountry() != null) {
			_hashCode += getSendCountry().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ SendCountry=").append(getSendCountry());
		buffer.append(" ]");
		return buffer.toString();
	}
}
