/**
 * GetReceiveCountriesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationResponse;

/**
 * 
 * Get Receive Countries Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 16:13:05 $ </td><tr><td>
 * @author   </td><td>$Author: a145 $ </td>
 *</table>
 *</div>
 */
public class GetReceiveCountriesResponse extends BaseOperationResponse {
	private static final long serialVersionUID = 1L;

	private ReceiveCountry[] countries;

	public GetReceiveCountriesResponse() {
	}

	/**
	 * Gets the countries value for this GetReceiveCountriesResponse.
	 * 
	 * @return countries
	 */
	public ReceiveCountry[] getCountries() {
		return countries;
	}

	/**
	 * Sets the countries value for this GetReceiveCountriesResponse.
	 * 
	 * @param countries
	 */
	public void setCountries(ReceiveCountry[] countries) {
		this.countries = countries;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetReceiveCountriesResponse))
			return false;
		GetReceiveCountriesResponse other = (GetReceiveCountriesResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.countries == null && other.getCountries() == null) || (this.countries != null && java.util.Arrays
						.equals(this.countries, other.getCountries())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getCountries() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getCountries()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getCountries(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ Countries=").append(getCountries());
		buffer.append(" ]");
		return buffer.toString();
	}
}
