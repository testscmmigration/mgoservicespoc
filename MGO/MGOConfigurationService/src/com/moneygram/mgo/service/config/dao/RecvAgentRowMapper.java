package com.moneygram.mgo.service.config.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config.ReceiveAgent;

public class RecvAgentRowMapper extends BaseRowMapper {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			RecvAgentRowMapper.class);

	/**
	 *
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReceiveAgent recvAgent = new ReceiveAgent();
		recvAgent.setAgentId(new Long(rs.getLong("AGENT_ID")));
		recvAgent.setLimitAmount(rs.getBigDecimal("AGENT_RCV_LIM_AMT"));
		recvAgent.setEmgMerchantId(rs.getString("MCC_PTNR_PRFL_ID"));
		return recvAgent;
	}
}
