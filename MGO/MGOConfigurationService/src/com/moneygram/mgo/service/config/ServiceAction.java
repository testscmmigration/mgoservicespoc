/**
 * ServiceAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;


/**
 * 
 * Service Action.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2011/08/03 18:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class ServiceAction implements java.io.Serializable {
	private java.lang.String _value_;
	private static java.util.HashMap _table_ = new java.util.HashMap();

	// Constructor
	protected ServiceAction(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	}

	public static final java.lang.String _getReceiveCountries = "getReceiveCountries";
	public static final java.lang.String _getReceiveAgent = "getReceiveAgent";
	public static final java.lang.String _getReceiveAgents = "getReceiveAgents";
	public static final java.lang.String _getSendCountryCurrencies = "getSendCountryCurrencies";
	public static final java.lang.String _getAd = "getAd";
	public static final ServiceAction getReceiveCountries = new ServiceAction(
			_getReceiveCountries);
	public static final ServiceAction getReceiveAgent = new ServiceAction(
			_getReceiveAgent);
	public static final ServiceAction getReceiveAgents = new ServiceAction(
			_getReceiveAgents);
	public static final ServiceAction getSendCountryCurrencies = new ServiceAction(
			_getSendCountryCurrencies);
	public static final ServiceAction getAd = new ServiceAction(_getAd);

	public java.lang.String getValue() {
		return _value_;
	}

	public static ServiceAction fromValue(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		ServiceAction enumeration = (ServiceAction) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static ServiceAction fromString(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

	public java.lang.Object readResolve() throws java.io.ObjectStreamException {
		return fromValue(_value_);
	}
}
