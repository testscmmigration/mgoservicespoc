package com.moneygram.mgo.service.config.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config.Currency;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesRequest;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesResponse;
import com.moneygram.mgo.service.config.SendCountryCurrencies;
import com.moneygram.mgo.service.config.CountryCurrenciesRow;
import com.moneygram.mgo.service.config.dao.MGTD2DAO;

public class GetSendCountryCurrenciesCommand extends ReadCommand{
	
	@Override
	protected boolean isRequestSupported(OperationRequest operationRequest)
			throws CommandException {
		
		return operationRequest instanceof GetSendCountryCurrenciesRequest;
		
	}

	@Override
	protected OperationResponse process(OperationRequest operationRequest)
			throws CommandException {

		GetSendCountryCurrenciesRequest request = (GetSendCountryCurrenciesRequest) operationRequest;
		
		
		// TODO build debug messages
		
		/*
		 
		if (logger.isDebugEnabled()) {
			logger.debug("process: get recv countries sendCountry="
					+ configRequest.getSendCountry());
		}

		if (configRequest.getSendCountry() == null) {
			logger.debug("process: invalid input send country");
			throw new DataFormatException("Invalid input send country");
		}

		 */
		
		
		
		MGTD2DAO dao = (MGTD2DAO) getDataAccessObject();
		List<CountryCurrenciesRow> rowList = dao.getSendCountryCurrencies(request.getMessage());
		
		Map<String, SendCountryCurrencies> map = new HashMap<String, SendCountryCurrencies>();
		
		
		for(CountryCurrenciesRow row : rowList){
			
			String countryCode = row.getIsoCountryCode();
			
			if(!map.containsKey(countryCode)){
				SendCountryCurrencies sendCountryCurrencies = new SendCountryCurrencies();
				sendCountryCurrencies.setCountryCode(countryCode);
				map.put(countryCode, sendCountryCurrencies);
			}
			
			Currency currency = new Currency();
			currency.setCode(row.getCurrency());
			currency.setName(row.getCurrencyname());

			map.get(countryCode).getCurrencies().add(currency);
			
		}
		
		
		
		String message = "Message is deprecated. Please remove this field from GetSendCountryCurrenciesResponse";
		GetSendCountryCurrenciesResponse response = new GetSendCountryCurrenciesResponse();
		response.setMessage(message);
		
		for(Entry<String, SendCountryCurrencies> entry : map.entrySet()){
			response.getSendCountryCurrencies().add(entry.getValue());
		}
		
		
		
		
		return response;
		

	}

}
