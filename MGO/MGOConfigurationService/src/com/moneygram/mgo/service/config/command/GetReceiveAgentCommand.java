/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.config.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentResponse;
import com.moneygram.mgo.service.config.ReceiveAgent;
import com.moneygram.mgo.service.config.dao.EMGD2DAO;
import com.moneygram.mgo.shared.ErrorCodes;

/**
 * 
 * Get Receive Agent Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/08/03 18:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class GetReceiveAgentCommand extends ReadCommand {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			GetReceiveAgentCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetReceiveAgentRequest;
	}

	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetReceiveAgentRequest configRequest = (GetReceiveAgentRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: get recv agent agentId="
					+ configRequest.getAgentId());
		}

		EMGD2DAO dao = (EMGD2DAO) getDataAccessObject();

		ReceiveAgent recvAgent = null;
		try {
			recvAgent = dao
					.getReceiveAgent(new Long(configRequest.getAgentId()));
		} catch (Exception e) {
			logger.warn("process: failed to get receive agent", e);
			throw new CommandException("Failed to get receive agent", e);
		}
		if (recvAgent == null)
			throw new BusinessRulesException(ErrorCodes.RECV_AGENT_NOT_FOUND,
					ErrorCodes.getMessage(ErrorCodes.RECV_AGENT_NOT_FOUND));

		GetReceiveAgentResponse response = new GetReceiveAgentResponse();
		response.setReceiveAgent(recvAgent);
		return response;
	}
}
