package com.moneygram.mgo.service.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SendCountryCurrencies implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String countryCode = null;
	private List<Currency> currencies = new ArrayList<Currency>();
	
	
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public List<Currency> getCurrencies() {
		return currencies;
	}
	public void setCurrencies(List<Currency> currencies) {
		this.currencies = currencies;
	}
}
