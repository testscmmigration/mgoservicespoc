/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.config.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config.GetReceiveCountriesRequest;
import com.moneygram.mgo.service.config.GetReceiveCountriesResponse;
import com.moneygram.mgo.service.config.ReceiveCountry;
import com.moneygram.mgo.service.config.dao.EMGD2DAO;

/**
 * 
 * Get Receive Countries Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2011/08/03 18:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class GetReceiveCountriesCommand extends ReadCommand {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			GetReceiveCountriesCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetReceiveCountriesRequest;
	}

	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetReceiveCountriesRequest configRequest = (GetReceiveCountriesRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: get recv countries sendCountry="
					+ configRequest.getSendCountry());
		}

		if (configRequest.getSendCountry() == null) {
			logger.debug("process: invalid input send country");
			throw new DataFormatException("Invalid input send country");
		}

		EMGD2DAO dao = (EMGD2DAO) getDataAccessObject();

		List<ReceiveCountry> countries = null;
		try {
			countries = dao.getReceiveCountries(configRequest.getSendCountry());
		} catch (Exception e) {
			logger.warn("process: failed to retrieve countries", e);
			throw new CommandException("Failed to retrieve countries", e);
		}

		GetReceiveCountriesResponse response = new GetReceiveCountriesResponse();
		ReceiveCountry[] array = null;
		if (countries != null && countries.size() >= 0) {
			array = (ReceiveCountry[]) countries
					.toArray(new ReceiveCountry[countries.size()]);
		}
		response.setCountries(array);
		return response;
	}
}
