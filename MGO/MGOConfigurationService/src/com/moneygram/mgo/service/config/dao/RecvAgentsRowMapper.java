package com.moneygram.mgo.service.config.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config.ReceiveAgent;

public class RecvAgentsRowMapper extends BaseRowMapper {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			RecvAgentsRowMapper.class);

	/**
	 *
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReceiveAgent recvAgent = new ReceiveAgent();
		recvAgent.setAgentId(new Long(rs.getLong("AGENT_ID")));
		recvAgent.setLimitAmount(rs.getBigDecimal("AGENT_RCV_LIM_AMT"));
		recvAgent.setEmgMerchantId("");
		return recvAgent;
	}
}
