/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.config.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.config.GetReceiveAgentsRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentsResponse;
import com.moneygram.mgo.service.config.GetReceiveCountriesResponse;
import com.moneygram.mgo.service.config.ReceiveAgent;
import com.moneygram.mgo.service.config.ReceiveCountry;
import com.moneygram.mgo.service.config.dao.EMGD2DAO;
import com.moneygram.mgo.shared.ErrorCodes;

/**
 *
 * Get Receive Agents Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2012/03/09 04:40:47 $ </td><tr><td>
 * @author   </td><td>$Author: vf69 $ </td>
 *</table>
 *</div>
 */
public class GetReceiveAgentsCommand extends ReadCommand {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			GetReceiveAgentsCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetReceiveAgentsRequest;
	}

	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetReceiveAgentsRequest configRequest = (GetReceiveAgentsRequest) request;

		if (logger.isDebugEnabled()) {
		}

		EMGD2DAO dao = (EMGD2DAO) getDataAccessObject();

		List<ReceiveAgent> agents = null;
		try {
			agents = dao.getActiveReceiveAgents();
		} catch (Exception e) {
			logger.warn("process: failed to retrieve receive agents", e);
			throw new CommandException("Failed to retrieve receive agents", e);
		}

		for(ReceiveAgent agent: agents) {
			try {
				ReceiveAgent dbAgent = dao.getReceiveAgent(agent.getAgentId());
				agent.setEmgMerchantId(dbAgent.getEmgMerchantId());
			} catch (Exception e) {
				logger.warn("process: failed to retrieve receive agent with agentid=" + agent.getAgentId(), e);
				throw new CommandException("Failed to retrieve receive agent with agentid=" + agent.getAgentId(), e);
			}
		}

		GetReceiveAgentsResponse response = new GetReceiveAgentsResponse();
		ReceiveAgent[] array = null;
		if (agents != null && agents.size() >= 0) {
			array = (ReceiveAgent[]) agents.toArray(new ReceiveAgent[agents
					.size()]);
		}

		response.setReceiveAgents(array);
		return response;
	}
}
