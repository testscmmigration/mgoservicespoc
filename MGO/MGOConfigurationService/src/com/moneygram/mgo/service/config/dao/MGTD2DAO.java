package com.moneygram.mgo.service.config.dao;

import java.sql.Types;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.mgo.service.config.CountryCurrenciesRow;

public class MGTD2DAO extends BaseDAO {

	private static final String DB_CALL_TYPE_CODE = "WEB";
	private static final String DB_CALLER_ID = "MGO";
	private static final String SERVICE_TYPE_SEND = "S";
	//private static final String SERVICE_TYPE_RECEIVE = "R";

	public List<CountryCurrenciesRow> getSendCountryCurrencies(String s) {
		
		List<CountryCurrenciesRow> countryCurrencyCV = getCountryCurrencyCV();
		
		List<CountryCurrenciesRow> sendCountryCurrencies = filterByServiceType(
				countryCurrencyCV, SERVICE_TYPE_SEND);
		
		return sendCountryCurrencies;

	}

	private List<CountryCurrenciesRow> filterByServiceType(
			List<CountryCurrenciesRow> sendCountryCurrencyRows,
			String serviceTypeCode) {

		List<CountryCurrenciesRow> sendCountryCurrencies = new LinkedList<CountryCurrenciesRow>();
		for (CountryCurrenciesRow row : sendCountryCurrencyRows) {
			if (row.getServiceTypeCode().equals(serviceTypeCode)) {
				sendCountryCurrencies.add(row);
			}
		}
		return sendCountryCurrencies;
	}

	@SuppressWarnings("unchecked")
	public List<CountryCurrenciesRow> getCountryCurrencyCV() {

		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(
						"pkg_mf_country_currency.prc_get_country_currency_cv")
				.withoutProcedureColumnMetaDataAccess() //
				.declareParameters(
						//
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_date", Types.DATE),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_mf_cntry_curr_type_cv",
								OracleTypes.CURSOR,
								new CountryCurrenciesRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();

		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("ov_prcs_log_id", 0);
		parameters.addValue("iv_date", new java.sql.Date(new Date().getTime()));

		Map<?, ?> result = jdbcCall.execute(parameters);
		List<CountryCurrenciesRow> sendCountryCurrencies = (List<CountryCurrenciesRow>) result
				.get("ov_mf_cntry_curr_type_cv");

		return sendCountryCurrencies;
	}
}
