/**
 * GetReceiveAgentRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class GetReceiveAgentRequest extends BaseOperationRequest {
	private static final long serialVersionUID = 1L;

	private long agentId;

	public GetReceiveAgentRequest() {
	}

	public GetReceiveAgentRequest(RequestHeader header, long agentId) {
		super(header);
		this.agentId = agentId;
	}

	/**
	 * Gets the agentId value for this GetReceiveAgentRequest.
	 * 
	 * @return agentId
	 */
	public long getAgentId() {
		return agentId;
	}

	/**
	 * Sets the agentId value for this GetReceiveAgentRequest.
	 * 
	 * @param agentId
	 */
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetReceiveAgentRequest))
			return false;
		GetReceiveAgentRequest other = (GetReceiveAgentRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj) && this.agentId == other.getAgentId();
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		_hashCode += new Long(getAgentId()).hashCode();
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ AgentId=").append(getAgentId());
		buffer.append(" ]");
		return buffer.toString();
	}
}
