/*
 * Created on Jul 22, 2009
 *
 */
package com.moneygram.mgo.service.config.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.config.ReceiveCountry;

/**
 * 
 * Receive Country Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 16:13:05 $ </td><tr><td>
 * @author   </td><td>$Author: a145 $ </td>
 *</table>
 *</div>
 */
public class ReceiveCountryRowMapper extends BaseRowMapper {

	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReceiveCountry country = new ReceiveCountry();
		country.setCountryCode(rs.getString("rcv_iso_cntry_code"));
		country.setSendAmount(rs.getBigDecimal("snd_max_amt"));
		country.setSendAllowed(DAOUtils.toBoolean(rs
				.getString("snd_allow_flag")));
		return country;
	}

}
