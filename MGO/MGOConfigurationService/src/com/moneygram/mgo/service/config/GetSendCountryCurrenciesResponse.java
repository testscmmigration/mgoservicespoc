package com.moneygram.mgo.service.config;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.common.service.BaseOperationResponse;

public class GetSendCountryCurrenciesResponse extends BaseOperationResponse {
	private static final long serialVersionUID = 1L;

	private String message;
	private List<SendCountryCurrencies> sendCountryCurrencies = new ArrayList<SendCountryCurrencies>();

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<SendCountryCurrencies> getSendCountryCurrencies() {
		return sendCountryCurrencies;
	}

	public void setSendCountryCurrencies(
			List<SendCountryCurrencies> sendCountryCurrencies) {
		this.sendCountryCurrencies = sendCountryCurrencies;
	}
}
