package com.moneygram.mgo.service.config;

import com.moneygram.common.service.BaseOperationRequest;

public class GetSendCountryCurrenciesRequest extends BaseOperationRequest{
	private static final long serialVersionUID = 1L;
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
