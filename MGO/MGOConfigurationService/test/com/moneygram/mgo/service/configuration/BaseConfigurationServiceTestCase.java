package com.moneygram.mgo.service.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config.dao.MGTD2DAO;
import com.moneygram.mgo.service.config.dao.EMGD2DAO;
import com.moneygram.service.BaseServiceTest;

public abstract class BaseConfigurationServiceTestCase extends BaseServiceTest {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			BaseConfigurationServiceTestCase.class);

	private static MGTD2DAO mgtd2DAO = null;
	private static EMGD2DAO emgd2DAO = null;

	protected Long generateAgentIdThatHasRecvLimit() {
		String sql = "select agent_id from emg_expay_agent where agent_rcv_lim_amt > 0 and rownum = 1";
		try {
			return (Long) getEMGD2DAO().getJdbcTemplate()
					.queryForObject(sql, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected MGTD2DAO getMGTD2DAO(){
		if (mgtd2DAO != null)
			return mgtd2DAO;
		ApplicationContext context = getContext();
		mgtd2DAO = (MGTD2DAO) context.getBean("mgtd2DAO");
		return mgtd2DAO;
	}
	
	protected EMGD2DAO getEMGD2DAO(){
		if (emgd2DAO != null)
			return emgd2DAO;
		ApplicationContext context = getContext();
		emgd2DAO = (EMGD2DAO) context.getBean("emgd2DAO");
		return emgd2DAO;
	}

	
	
    @Override
    protected String[] getContextResources() {
        return new String[] { "ConfigurationTestContext.xml" };
    }
	
	
}
