/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.mgo.service.configuration;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;
import com.moneygram.mgo.service.config.GetReceiveAgentRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentResponse;
import com.moneygram.mgo.service.config.GetReceiveAgentsRequest;
import com.moneygram.mgo.service.config.GetReceiveAgentsResponse;
import com.moneygram.mgo.service.config.GetReceiveCountriesRequest;
import com.moneygram.mgo.service.config.GetReceiveCountriesResponse;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesRequest;
import com.moneygram.mgo.service.config.GetSendCountryCurrenciesResponse;
import com.moneygram.mgo.service.config.SendCountryCurrencies;
import com.moneygram.mgo.service.config.ServiceAction;

/**
 * Configuration Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2011/08/03 18:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class ConfigurationServiceTest extends BaseConfigurationServiceTestCase {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConfigurationServiceTest.class);

	public void testGetReceiveCountries() throws ServiceException {
		logger.info("\ntestGetReceiveCountries...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getReceiveCountries.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		GetReceiveCountriesRequest request = new GetReceiveCountriesRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setSendCountry("USA");
		RequestRouter requestRouter = getRequestRouter();

		GetReceiveCountriesResponse response = (GetReceiveCountriesResponse) requestRouter
				.process(request);
		assertNotNull("Expected not null response", response);
	}

	public void testGetReceiveAgent() throws ServiceException {
		logger.info("\ntestGetReceiveAgent...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getReceiveAgent.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		GetReceiveAgentRequest request = new GetReceiveAgentRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		Long agentId = generateAgentIdThatHasRecvLimit();
		assertNotNull("Expected not null agentId", agentId);
		request.setAgentId(agentId.longValue());
		RequestRouter requestRouter = getRequestRouter();

		GetReceiveAgentResponse response = (GetReceiveAgentResponse) requestRouter
				.process(request);
		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null receive agent", response
				.getReceiveAgent());
		assertNotNull("Expected not null receive agent limit amount", response
				.getReceiveAgent().getLimitAmount());
		logger.info("receive agent limit amount="
				+ response.getReceiveAgent().getLimitAmount());
	}

	public void testGetReceiveAgents() throws ServiceException {
		logger.info("\ntestGetReceiveAgents...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getReceiveAgents.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		GetReceiveAgentsRequest request = new GetReceiveAgentsRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		RequestRouter requestRouter = getRequestRouter();

		GetReceiveAgentsResponse response = (GetReceiveAgentsResponse) requestRouter
				.process(request);
		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null receive agents", response
				.getReceiveAgents());
		logger.info("number of receive agents="
				+ response.getReceiveAgents().length);
	}
	
	public void testGetSendCountryCurrencies() throws ServiceException{
		logger.info("\ntestGetSendCountryCurrencies...");
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getSendCountryCurrencies.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);
		
		GetSendCountryCurrenciesRequest request = new GetSendCountryCurrenciesRequest();
		request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
		RequestRouter requestRouter = getRequestRouter();
		
		
		GetSendCountryCurrenciesResponse response = //
			(GetSendCountryCurrenciesResponse) requestRouter.process(request);
		
		
		assertNotNull("Expected not null response", response);
		
		List<SendCountryCurrencies> sendCountryCurrencies = response.getSendCountryCurrencies();
		assertFalse("Expected not empty list", sendCountryCurrencies.isEmpty());
		
	}
}
