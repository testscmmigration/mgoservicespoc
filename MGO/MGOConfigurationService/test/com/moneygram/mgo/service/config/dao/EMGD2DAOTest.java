/*
 * Created on Jul 16, 2009
 *
 */
package com.moneygram.mgo.service.config.dao;

import java.util.List;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config.ReceiveAgent;
import com.moneygram.mgo.service.config.ReceiveCountry;
import com.moneygram.mgo.service.configuration.BaseConfigurationServiceTestCase;

/**
 * 
 * Configuration DAO Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConfigurationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/08/03 18:49:43 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public class EMGD2DAOTest extends BaseConfigurationServiceTestCase {
	private static final Logger logger = LogFactory.getInstance().getLogger(
			EMGD2DAOTest.class);

	public void testGetReceiveCountries() throws DAOException {
		logger.info("\ntestGetReceiveCountries...");
		List<ReceiveCountry> countries = getEMGD2DAO()
				.getReceiveCountries("USA");
		assertNotNull("Expected not null countries list", countries);
		assertTrue("Expected not empty countries list", countries.size() > 0);
		Object object = countries.get(0);
		assertTrue(
				"Expected countries list to contain ReceiveCountry instances",
				object instanceof ReceiveCountry);
		ReceiveCountry country = (ReceiveCountry) object;
		assertNotNull("Expected not null country", country);
		assertNotNull("Expected not null country country", country
				.getCountryCode());
		assertNotNull("Expected not null country sendAmount", country
				.getSendAmount());
		if (logger.isDebugEnabled()) {
			for (ReceiveCountry recvCntry : countries) {
				logger.debug("country=" + recvCntry);
			}
		}
	}

	public void testGetReceiveAgent() throws DAOException {
		logger.info("\ntestGetReceiveAgent...");
		Long agentId = generateAgentIdThatHasRecvLimit();
		assertNotNull("Expected not null agentId", agentId);
		ReceiveAgent agent = getEMGD2DAO().getReceiveAgent(agentId);
		assertNotNull("Expected not null agent", agent);
		assertNotNull("Expected not null agent limit amount", agent
				.getLimitAmount());
		logger.info(agent.toString());
	}

	public void testGetActiveReceiveAgents() throws DAOException {
		logger.info("\ntestGetActiveReceiveAgents...");
		List<ReceiveAgent> agents = getEMGD2DAO()
				.getActiveReceiveAgents();
		assertNotNull("Expected not null agent", agents);
		for (ReceiveAgent agent : agents) {
			logger.info("agent=" + agent);
		}
	}
}
