package com.moneygram.mgo.service.config.dao;

import java.util.List;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.config.CountryCurrenciesRow;
import com.moneygram.mgo.service.configuration.BaseConfigurationServiceTestCase;

public class MGTD2DAOTest extends BaseConfigurationServiceTestCase {
	
	private static final Logger logger = LogFactory.getInstance().getLogger(
			MGTD2DAOTest.class);
	
	public void testGetSendCountryCurrencies() throws DAOException {
		logger.info("\ntestGetSendCountryCurrencies...");
		
		List<CountryCurrenciesRow> sendCountryCurrencies = getMGTD2DAO().getSendCountryCurrencies("");
		
		assertNotNull("List of SendCountryCurrenciesRow must not be null", sendCountryCurrencies);
		assertFalse("List of SendCountryCurrenciesRow must not be empty", sendCountryCurrencies.isEmpty());
		
		for (CountryCurrenciesRow sendCountryCurrenciesRow : sendCountryCurrencies) {
			assertTrue(
					"List of SendCountryCurrenciesRow must not contain any item with service type different from \"S\"",
					sendCountryCurrenciesRow.getServiceTypeCode().equals("S")
					);
			
		}
		
	}
	
	public void testGetCountryCurrencyCV(){
		logger.info("\ntestGetCountryCurrencyCV...");
		
		List<CountryCurrenciesRow> sendCountryCurrencies = getMGTD2DAO().getCountryCurrencyCV();
		
		assertNotNull("List of SendCountryCurrenciesRow must not be null", sendCountryCurrencies);
		assertFalse("List of SendCountryCurrenciesRow must not be empty", sendCountryCurrencies.isEmpty());
	
	}
}
