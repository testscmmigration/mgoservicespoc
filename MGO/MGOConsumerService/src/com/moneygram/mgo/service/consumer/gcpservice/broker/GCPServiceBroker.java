package com.moneygram.mgo.service.consumer.gcpservice.broker;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.gcpservice.exception.GCPServiceException;
import com.moneygram.mgo.service.consumer.gcpservice.exception.ProxyException;
import com.moneygram.mgo.service.consumer.gcpservice.proxy.GCPServiceProxy;
import com.moneygram.mgo.service.consumer.gcpservice.proxy.GCPServiceProxyImpl;
import com.moneygram.mgo.service.consumer.util.MGOConsumerServiceResourceConfig;

public class GCPServiceBroker {
	
	public static MGOConsumerServiceResourceConfig getResourceConfig() {
		return MGOConsumerServiceResourceConfig.getInstance();
	}
	private static final Logger log = LogFactory.getInstance().getLogger(GCPServiceBroker.class);
	private GCPServiceProxy gcpServiceProxy=new GCPServiceProxyImpl();
	
	private int gcpRetryTimes = getResourceConfig().getGCPServiceCallRetryTimes();
    private int gcpRetryWaitTime = getResourceConfig().getGCPServiceCallRetryWaitPeriod();
	
	
	
	public long getGCPID() throws Exception{
		// Call GCP service to retrieve GCP consumer ID, get number of retry times for connection in case the
        // service is down
        long gcpId = -1;
        boolean gcpServiceOK = false;
        try {
        	int counter=1;
            while ((counter <= gcpRetryTimes) && !gcpServiceOK) {
                try {
                    gcpId = gcpServiceProxy.getConsumerProfileID();
                    gcpServiceOK = true;
                } catch (ProxyException p) {
                    log.error("Error calling GCP service: " + p.getMessage(), p);
                    log.debug("Calling GCP Service, retry number: " + counter);
                    // Wait "gcpRetryWaitTime" seconds and retry again the calling
                    counter++;
                    if(gcpRetryTimes > 0){
                    	Thread.sleep(gcpRetryWaitTime *1000);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in getGCPID: " + e.getMessage(), e);
        }

        if (!gcpServiceOK) {
            // if GCP service didn�t return the id, throw an exception
        	log.error("GCP Service down, could not proceed with consumer profile creation");
        	throw new GCPServiceException("GCP Service down, could not proceed with consumer profile creation");
        } 
        return gcpId;
		
	}

}
