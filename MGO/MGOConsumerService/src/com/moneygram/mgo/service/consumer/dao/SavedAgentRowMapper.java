package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.mgo.service.consumer.SavedAgent;

public class SavedAgentRowMapper extends BaseRowMapper {

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		SavedAgent agent = new SavedAgent();
		Number id = (Number) rs.getObject(1);
		agent.setAgentId(id.longValue());
		return agent;
	}
}
