/**
 * FIAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.mgo.service.consumer.util.ConsumerUtil;

public class FIAccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.Long accountId;
	private FIAccountType accountType;
	private java.lang.String accountNumber;
	private java.lang.String accountStatus;
	private java.lang.String accountSubStatus;
	private Comment[] comments;
	private java.lang.Boolean blocked;
	private String encryptedAccountNumber;
	

	public FIAccount() {
	}

	/**
	 * Gets the accountId value for this FIAccount.
	 *
	 * @return accountId
	 */
	public java.lang.Long getAccountId() {
		return accountId;
	}

	/**
	 * Sets the accountId value for this FIAccount.
	 *
	 * @param accountId
	 */
	public void setAccountId(java.lang.Long accountId) {
		this.accountId = accountId;
	}

	/**
	 * Gets the accountType value for this FIAccount.
	 *
	 * @return accountType
	 */
	public FIAccountType getAccountType() {
		return accountType;
	}

	/**
	 * Sets the accountType value for this FIAccount.
	 *
	 * @param accountType
	 */
	public void setAccountType(FIAccountType accountType) {
		this.accountType = accountType;
	}

	/**
	 * Gets the accountNumber value for this FIAccount.
	 *
	 * @return accountNumber
	 */
	public java.lang.String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the accountNumber value for this FIAccount.
	 *
	 * @param accountNumber
	 */
	public void setAccountNumber(java.lang.String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the accountStatus value for this FIAccount.
	 *
	 * @return accountStatus
	 */
	public java.lang.String getAccountStatus() {
		return accountStatus;
	}

	/**
	 * Sets the accountStatus value for this FIAccount.
	 *
	 * @param accountStatus
	 */
	public void setAccountStatus(java.lang.String accountStatus) {
		this.accountStatus = accountStatus;
	}

	/**
	 * Gets the accountSubStatus value for this FIAccount.
	 *
	 * @return accountSubStatus
	 */
	public java.lang.String getAccountSubStatus() {
		return accountSubStatus;
	}

	/**
	 * Sets the accountSubStatus value for this FIAccount.
	 *
	 * @param accountSubStatus
	 */
	public void setAccountSubStatus(java.lang.String accountSubStatus) {
		this.accountSubStatus = accountSubStatus;
	}

	/**
	 * Gets the comments value for this FIAccount.
	 *
	 * @return comments
	 */
	public Comment[] getComments() {
		return comments;
	}

	/**
	 * Sets the comments value for this FIAccount.
	 *
	 * @param comments
	 */
	public void setComments(Comment[] comments) {
		this.comments = comments;
	}

	/**
	 * Gets the blocked value for this FIAccount.
	 *
	 * @return blocked
	 */
	public java.lang.Boolean getBlocked() {
		return blocked;
	}

	/**
	 * Sets the blocked value for this FIAccount.
	 *
	 * @param blocked
	 */
	public void setBlocked(java.lang.Boolean blocked) {
		this.blocked = blocked;
	}

	public String getEncryptedAccountNumber() {
		return encryptedAccountNumber;
	}

	public void setEncryptedAccountNumber(String encryptedAccountNumber) {
		this.encryptedAccountNumber = encryptedAccountNumber;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof FIAccount))
			return false;
		FIAccount other = (FIAccount) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.accountId == null && other.getAccountId() == null) || (this.accountId != null && this.accountId
						.equals(other.getAccountId())))
				&& ((this.accountType == null && other.getAccountType() == null) || (this.accountType != null && this.accountType
						.equals(other.getAccountType())))
				&& ((this.accountNumber == null && other.getAccountNumber() == null) || (this.accountNumber != null && this.accountNumber
						.equals(other.getAccountNumber())))
				&& ((this.encryptedAccountNumber == null && other.getEncryptedAccountNumber() == null) || (this.encryptedAccountNumber != null && this.encryptedAccountNumber
						.equals(other.getEncryptedAccountNumber())))
				&& ((this.accountStatus == null && other.getAccountStatus() == null) || (this.accountStatus != null && this.accountStatus
						.equals(other.getAccountStatus())))
				&& ((this.accountSubStatus == null && other
						.getAccountSubStatus() == null) || (this.accountSubStatus != null && this.accountSubStatus
						.equals(other.getAccountSubStatus())))
				&& ((this.comments == null && other.getComments() == null) || (this.comments != null && java.util.Arrays
						.equals(this.comments, other.getComments())))
				&& ((this.blocked == null && other.getBlocked() == null) || (this.blocked != null && this.blocked
						.equals(other.getBlocked())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getAccountId() != null) {
			_hashCode += getAccountId().hashCode();
		}
		if (getAccountType() != null) {
			_hashCode += getAccountType().hashCode();
		}
		if (getAccountNumber() != null) {
			_hashCode += getAccountNumber().hashCode();
		}
		if (getAccountStatus() != null) {
			_hashCode += getAccountStatus().hashCode();
		}
		if (getAccountSubStatus() != null) {
			_hashCode += getAccountSubStatus().hashCode();
		}
		if (getComments() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getComments()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getComments(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		if (getBlocked() != null) {
			_hashCode += getBlocked().hashCode();
		}
		if (getEncryptedAccountNumber() != null) {
			_hashCode += getEncryptedAccountNumber().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ AccountId=").append(getAccountId());
		buffer.append(" AccountType=").append(getAccountType());
		buffer.append(" AccountNumber=").append(ConsumerUtil.maskLast4(getAccountNumber()));
		buffer.append(" AccountStatus=").append(getAccountStatus());
		buffer.append(" AccountSubStatus=").append(getAccountSubStatus());
		buffer.append(" Comments=").append(getComments());
		buffer.append(" Blocked=").append(getBlocked());
		buffer.append(" EncryptedAccountNumber=").append(getEncryptedAccountNumber());
		buffer.append(" ]");
		return buffer.toString();
	}
	
}
