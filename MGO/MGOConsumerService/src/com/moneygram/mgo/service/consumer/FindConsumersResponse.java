/**
 * FindConsumersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationResponse;

public class FindConsumersResponse extends BaseOperationResponse {

	private static final long serialVersionUID = 1L;
	private Consumer[] consumers;

	public FindConsumersResponse() {
	}

	/**
	 * Gets the consumers value for this FindConsumersResponse.
	 * 
	 * @return consumers
	 */
	public Consumer[] getConsumers() {
		return consumers;
	}

	/**
	 * Sets the consumers value for this FindConsumersResponse.
	 * 
	 * @param consumers
	 */
	public void setConsumers(Consumer[] consumers) {
		this.consumers = consumers;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof FindConsumersResponse))
			return false;
		FindConsumersResponse other = (FindConsumersResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.consumers == null && other.getConsumers() == null) || (this.consumers != null && java.util.Arrays
						.equals(this.consumers, other.getConsumers())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getConsumers() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getConsumers()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getConsumers(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ Consumers=").append(getConsumers());
		buffer.append(" ]");
		return buffer.toString();
	}
}
