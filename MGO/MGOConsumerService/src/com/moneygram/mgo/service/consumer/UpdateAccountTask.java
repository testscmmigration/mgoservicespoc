/**
 * UpdateAccountTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class UpdateAccountTask implements java.io.Serializable {
	private java.lang.String _value_;
	private static java.util.HashMap _table_ = new java.util.HashMap();

	// Constructor
	protected UpdateAccountTask(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	}

	public static final java.lang.String _UpdateStatus = "UpdateStatus";
	public static final java.lang.String _AddComment = "AddComment";
	public static final java.lang.String _UpdateExpiration = "UpdateExpiration";
	public static final java.lang.String _UpdateBlocked = "UpdateBlocked";
	public static final java.lang.String _UpdateCardType = "UpdateCardType";
	
	public static final UpdateAccountTask UpdateStatus = new UpdateAccountTask(
			_UpdateStatus);
	public static final UpdateAccountTask AddComment = new UpdateAccountTask(
			_AddComment);
	public static final UpdateAccountTask UpdateExpiration = new UpdateAccountTask(
			_UpdateExpiration);
	public static final UpdateAccountTask UpdateBlocked = new UpdateAccountTask(
			_UpdateBlocked);
	public static final UpdateAccountTask UpdateCardType = new UpdateAccountTask(_UpdateCardType);

	public java.lang.String getValue() {
		return _value_;
	}

	public static UpdateAccountTask fromValue(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		UpdateAccountTask enumeration = (UpdateAccountTask) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static UpdateAccountTask fromString(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

	public java.lang.Object readResolve() throws java.io.ObjectStreamException {
		return fromValue(_value_);
	}
}
