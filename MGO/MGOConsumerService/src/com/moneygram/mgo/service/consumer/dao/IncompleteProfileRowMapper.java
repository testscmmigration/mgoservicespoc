package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;

public class IncompleteProfileRowMapper extends BaseRowMapper {

	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Long(rs.getLong("CUST_ID"));

	}
}
