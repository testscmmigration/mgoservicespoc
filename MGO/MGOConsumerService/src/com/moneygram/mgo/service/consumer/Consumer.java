/**
 * Consumer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.mgo.shared.ConsumerAddress;

/**
 *
 * MGO Consumer.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.9 $ $Date: 2012/01/22 18:12:32 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class Consumer implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    private java.lang.Long consumerId;

    private java.lang.String loginId;

    private PersonalInfo personal;

    private Contact contact;

    private LoyaltyInfo loyalty;

    private TransactionPreferences transactionPreferences;

    private ConsumerFIAccount[] accounts;

    private MGOAttributes internal;

    private Comment[] comments;

    private Access access;

    private ConsumerAddress address;

    private ProfileEvent[] profileEvents;

    private java.lang.String sourceSite;
    
    //vl58 -- Release 36, new field returned for consumer "cnsmr_prfl_id"
    private java.lang.Long cnsmrProfileId;

    public Consumer() {
    }

    public Consumer(
           java.lang.Long consumerId,
           java.lang.String loginId,
           PersonalInfo personal,
           Contact contact,
           LoyaltyInfo loyalty,
           TransactionPreferences transactionPreferences,
           ConsumerFIAccount[] accounts,
           MGOAttributes internal,
           Comment[] comments,
           Access access,
           ConsumerAddress address,
           ProfileEvent[] profileEvents,
           java.lang.String sourceSite,
           java.lang.Long cnsmrProfileId) {
           this.consumerId = consumerId;
           this.loginId = loginId;
           this.personal = personal;
           this.contact = contact;
           this.loyalty = loyalty;
           this.transactionPreferences = transactionPreferences;
           this.accounts = accounts;
           this.internal = internal;
           this.comments = comments;
           this.access = access;
           this.address = address;
           this.profileEvents = profileEvents;
           this.sourceSite = sourceSite;
           this.cnsmrProfileId = cnsmrProfileId;
    }


    /**
     * Gets the consumerId value for this Consumer.
     *
     * @return consumerId
     */
    public java.lang.Long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this Consumer.
     *
     * @param consumerId
     */
    public void setConsumerId(java.lang.Long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the loginId value for this Consumer.
     *
     * @return loginId
     */
    public java.lang.String getLoginId() {
        return loginId;
    }


    /**
     * Sets the loginId value for this Consumer.
     *
     * @param loginId
     */
    public void setLoginId(java.lang.String loginId) {
        this.loginId = loginId;
    }


    /**
     * Gets the personal value for this Consumer.
     *
     * @return personal
     */
    public PersonalInfo getPersonal() {
        return personal;
    }


    /**
     * Sets the personal value for this Consumer.
     *
     * @param personal
     */
    public void setPersonal(PersonalInfo personal) {
        this.personal = personal;
    }


    /**
     * Gets the contact value for this Consumer.
     *
     * @return contact
     */
    public Contact getContact() {
        return contact;
    }


    /**
     * Sets the contact value for this Consumer.
     *
     * @param contact
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }


    /**
     * Gets the loyalty value for this Consumer.
     *
     * @return loyalty
     */
    public LoyaltyInfo getLoyalty() {
        return loyalty;
    }


    /**
     * Sets the loyalty value for this Consumer.
     *
     * @param loyalty
     */
    public void setLoyalty(LoyaltyInfo loyalty) {
        this.loyalty = loyalty;
    }


    /**
     * Gets the transactionPreferences value for this Consumer.
     *
     * @return transactionPreferences
     */
    public TransactionPreferences getTransactionPreferences() {
        return transactionPreferences;
    }


    /**
     * Sets the transactionPreferences value for this Consumer.
     *
     * @param transactionPreferences
     */
    public void setTransactionPreferences(TransactionPreferences transactionPreferences) {
        this.transactionPreferences = transactionPreferences;
    }


    /**
     * Gets the accounts value for this Consumer.
     *
     * @return accounts
     */
    public ConsumerFIAccount[] getAccounts() {
        return accounts;
    }


    /**
     * Sets the accounts value for this Consumer.
     *
     * @param accounts
     */
    public void setAccounts(ConsumerFIAccount[] accounts) {
        this.accounts = accounts;
    }


    /**
     * Gets the internal value for this Consumer.
     *
     * @return internal
     */
    public MGOAttributes getInternal() {
        return internal;
    }


    /**
     * Sets the internal value for this Consumer.
     *
     * @param internal
     */
    public void setInternal(MGOAttributes internal) {
        this.internal = internal;
    }


    /**
     * Gets the comments value for this Consumer.
     *
     * @return comments
     */
    public Comment[] getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this Consumer.
     *
     * @param comments
     */
    public void setComments(Comment[] comments) {
        this.comments = comments;
    }


    /**
     * Gets the access value for this Consumer.
     *
     * @return access
     */
    public Access getAccess() {
        return access;
    }


    /**
     * Sets the access value for this Consumer.
     *
     * @param access
     */
    public void setAccess(Access access) {
        this.access = access;
    }


    /**
     * Gets the address value for this Consumer.
     *
     * @return address
     */
    public ConsumerAddress getAddress() {
        return address;
    }


    /**
     * Sets the address value for this Consumer.
     *
     * @param address
     */
    public void setAddress(ConsumerAddress address) {
        this.address = address;
    }


    /**
     * Gets the profileEvents value for this Consumer.
     *
     * @return profileEvents
     */
    public ProfileEvent[] getProfileEvents() {
        return profileEvents;
    }


    /**
     * Sets the profileEvents value for this Consumer.
     *
     * @param profileEvents
     */
    public void setProfileEvents(ProfileEvent[] profileEvents) {
        this.profileEvents = profileEvents;
    }

    public ProfileEvent getProfileEvents(int i) {
        return this.profileEvents[i];
    }

    public void setProfileEvents(int i, ProfileEvent _value) {
        this.profileEvents[i] = _value;
    }


    /**
     * Gets the sourceSite value for this Consumer.
     *
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this Consumer.
     *
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }
    
    /**
	 * @return the cnsmrProfileId
	 */
	public java.lang.Long getCnsmrProfileId() {
		return cnsmrProfileId;
	}

	/**
	 * @param cnsmrProfileId the cnsmrProfileId to set
	 */
	public void setCnsmrProfileId(java.lang.Long cnsmrProfileId) {
		this.cnsmrProfileId = cnsmrProfileId;
	}

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Consumer)) return false;
        Consumer other = (Consumer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.consumerId==null && other.getConsumerId()==null) ||
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.loginId==null && other.getLoginId()==null) ||
             (this.loginId!=null &&
              this.loginId.equals(other.getLoginId()))) &&
            ((this.personal==null && other.getPersonal()==null) ||
             (this.personal!=null &&
              this.personal.equals(other.getPersonal()))) &&
            ((this.contact==null && other.getContact()==null) ||
             (this.contact!=null &&
              this.contact.equals(other.getContact()))) &&
            ((this.loyalty==null && other.getLoyalty()==null) ||
             (this.loyalty!=null &&
              this.loyalty.equals(other.getLoyalty()))) &&
            ((this.transactionPreferences==null && other.getTransactionPreferences()==null) ||
             (this.transactionPreferences!=null &&
              this.transactionPreferences.equals(other.getTransactionPreferences()))) &&
            ((this.accounts==null && other.getAccounts()==null) ||
             (this.accounts!=null &&
              java.util.Arrays.equals(this.accounts, other.getAccounts()))) &&
            ((this.internal==null && other.getInternal()==null) ||
             (this.internal!=null &&
              this.internal.equals(other.getInternal()))) &&
            ((this.comments==null && other.getComments()==null) ||
             (this.comments!=null &&
              java.util.Arrays.equals(this.comments, other.getComments()))) &&
            ((this.access==null && other.getAccess()==null) ||
             (this.access!=null &&
              this.access.equals(other.getAccess()))) &&
            ((this.address==null && other.getAddress()==null) ||
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.profileEvents==null && other.getProfileEvents()==null) ||
             (this.profileEvents!=null &&
              java.util.Arrays.equals(this.profileEvents, other.getProfileEvents()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) ||
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite()))) &&
            ((this.cnsmrProfileId==null && other.getCnsmrProfileId()==null) || 
             (this.cnsmrProfileId!=null &&
              this.cnsmrProfileId.equals(other.getCnsmrProfileId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getLoginId() != null) {
            _hashCode += getLoginId().hashCode();
        }
        if (getPersonal() != null) {
            _hashCode += getPersonal().hashCode();
        }
        if (getContact() != null) {
            _hashCode += getContact().hashCode();
        }
        if (getLoyalty() != null) {
            _hashCode += getLoyalty().hashCode();
        }
        if (getTransactionPreferences() != null) {
            _hashCode += getTransactionPreferences().hashCode();
        }
        if (getAccounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInternal() != null) {
            _hashCode += getInternal().hashCode();
        }
        if (getComments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getComments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getComments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAccess() != null) {
            _hashCode += getAccess().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getProfileEvents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProfileEvents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProfileEvents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        if (getCnsmrProfileId() != null) {
            _hashCode += getCnsmrProfileId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
	    StringBuffer buffer = new StringBuffer();
	    buffer.append(" super=").append(super.toString());
	    buffer.append(" ConsumerId=").append(getConsumerId());
	    buffer.append(" LoginId=").append(getLoginId());
	    buffer.append(" Personal=").append(getPersonal());
	    buffer.append(" Contact=").append(getContact());
	    buffer.append(" Loyalty=").append(getLoyalty());
	    buffer.append(" TransactionPreferences=").append(getTransactionPreferences());
	    buffer.append(" Accounts=").append(getAccounts());
	    buffer.append(" Internal=").append(getInternal());
	    buffer.append(" Comments=").append(getComments());
	    buffer.append(" Access=").append(getAccess());
	    buffer.append(" Address=").append(getAddress());
	    buffer.append(" ProfileEvents=").append(getProfileEvents());
        buffer.append(" SourceSite=").append(getSourceSite());
        buffer.append(" CnsmrProfileId=").append(getCnsmrProfileId());
	    return buffer.toString();
	}
}
