/**
 * ServiceAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;


public class ServiceAction implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ServiceAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _getConsumerProfile = "getConsumerProfile";
    public static final java.lang.String _updateConsumerProfile = "updateConsumerProfile";
    public static final java.lang.String _createConsumerProfile = "createConsumerProfile";
    public static final java.lang.String _updateConsumerAccount = "updateConsumerAccount";
    public static final java.lang.String _addConsumerAccount = "addConsumerAccount";
    public static final java.lang.String _getConsumerBlockedStatus = "getConsumerBlockedStatus";
    public static final java.lang.String _findConsumers = "findConsumers";
    public static final java.lang.String _getIncompleteProfiles = "getIncompleteProfiles";
    public static final java.lang.String _getProfileEvents = "getProfileEvents";
    public static final java.lang.String _saveProfileEvent = "saveProfileEvent";
    public static final java.lang.String _saveLogonTry = "saveLogonTry";    
    public static final java.lang.String _getCCLowAuthSequencerValue = "getCCLowAuthSequencerValue";
    public static final ServiceAction getConsumerProfile = new ServiceAction(_getConsumerProfile);
    public static final ServiceAction updateConsumerProfile = new ServiceAction(_updateConsumerProfile);
    public static final ServiceAction createConsumerProfile = new ServiceAction(_createConsumerProfile);
    public static final ServiceAction updateConsumerAccount = new ServiceAction(_updateConsumerAccount);
    public static final ServiceAction addConsumerAccount = new ServiceAction(_addConsumerAccount);
    public static final ServiceAction getConsumerBlockedStatus = new ServiceAction(_getConsumerBlockedStatus);
    public static final ServiceAction findConsumers = new ServiceAction(_findConsumers);
    public static final ServiceAction getIncompleteProfiles = new ServiceAction(_getIncompleteProfiles);
    public static final ServiceAction getProfileEvents = new ServiceAction(_getProfileEvents);
    public static final ServiceAction saveProfileEvent = new ServiceAction(_saveProfileEvent);
    public static final ServiceAction saveLogonTry = new ServiceAction(_saveLogonTry);
    public static final ServiceAction getCCLowAuthSequencerValue = new ServiceAction(_getCCLowAuthSequencerValue);
    
    public java.lang.String getValue() { return _value_;}
    public static ServiceAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ServiceAction enumeration = (ServiceAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ServiceAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}

}
