package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.Consumer;

public class ConsumerSearchRowMapper extends BaseRowMapper {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerSearchRowMapper.class);

	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Consumer consumer = new Consumer();
		consumer.setConsumerId(new Long(rs.getLong("CUST_ID")));
		consumer.setLoginId(rs.getString("CUST_LOGON_ID"));
		return consumer;
	}
}
