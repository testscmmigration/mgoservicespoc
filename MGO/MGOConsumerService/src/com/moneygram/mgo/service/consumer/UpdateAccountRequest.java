/**
 * UpdateAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class UpdateAccountRequest extends BaseOperationRequest implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private ConsumerFIAccount consumerFIAccount;
	private Comment comment;
	private UpdateAccountTask[] updateTasks;

	public UpdateAccountRequest() {
	}

	/**
	 * Gets the consumerFIAccount value for this UpdateAccountRequest.
	 * 
	 * @return consumerFIAccount
	 */
	public ConsumerFIAccount getConsumerFIAccount() {
		return consumerFIAccount;
	}

	/**
	 * Sets the consumerFIAccount value for this UpdateAccountRequest.
	 * 
	 * @param consumerFIAccount
	 */
	public void setConsumerFIAccount(ConsumerFIAccount consumerFIAccount) {
		this.consumerFIAccount = consumerFIAccount;
	}

	/**
	 * Gets the comment value for this UpdateAccountRequest.
	 * 
	 * @return comment
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * Sets the comment value for this UpdateAccountRequest.
	 * 
	 * @param comment
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}

	/**
	 * Gets the updateTasks value for this UpdateAccountRequest.
	 * 
	 * @return updateTasks
	 */
	public UpdateAccountTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * Sets the updateTasks value for this UpdateAccountRequest.
	 * 
	 * @param updateTasks
	 */
	public void setUpdateTasks(UpdateAccountTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof UpdateAccountRequest))
			return false;
		UpdateAccountRequest other = (UpdateAccountRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.consumerFIAccount == null && other
						.getConsumerFIAccount() == null) || (this.consumerFIAccount != null && this.consumerFIAccount
						.equals(other.getConsumerFIAccount())))
				&& ((this.comment == null && other.getComment() == null) || (this.comment != null && this.comment
						.equals(other.getComment())))
				&& ((this.updateTasks == null && other.getUpdateTasks() == null) || (this.updateTasks != null && java.util.Arrays
						.equals(this.updateTasks, other.getUpdateTasks())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getConsumerFIAccount() != null) {
			_hashCode += getConsumerFIAccount().hashCode();
		}
		if (getComment() != null) {
			_hashCode += getComment().hashCode();
		}
		if (getUpdateTasks() != null) {
			for (int i = 0; i < java.lang.reflect.Array
					.getLength(getUpdateTasks()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(
						getUpdateTasks(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ConsumerFIAccount=").append(getConsumerFIAccount());
		buffer.append(" Comment=").append(getComment());
		buffer.append(" UpdateTasks=").append(getUpdateTasks());
		buffer.append(" ]");
		return buffer.toString();
	}
}
