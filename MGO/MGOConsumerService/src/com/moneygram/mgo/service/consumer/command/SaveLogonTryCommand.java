/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer.Access;
import com.moneygram.mgo.service.consumer.SaveLogonTryRequest;
import com.moneygram.mgo.service.consumer.SaveLogonTryResponse;
import com.moneygram.mgo.service.consumer.SaveProfileEventResponse;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;

public class SaveLogonTryCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			SaveLogonTryCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof SaveLogonTryRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		SaveLogonTryRequest saveRequest = (SaveLogonTryRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: save logon try - SaveLogonTryRequest="
					+ saveRequest);
		}

		if (saveRequest == null)
            throw new DataFormatException("SaveLogonTryRequest is null");
		if (saveRequest.getLoginId() == null)
            throw new DataFormatException("loginId is null");		
        if (saveRequest.getAccess() == null)
            throw new DataFormatException("Access is missing");

        Access access = saveRequest.getAccess();
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
    
	    if (access.getIpAddress() == null
				|| access.getWebServerName() == null
				|| access.getWebServerIpAddress() == null) {
			throw new DataFormatException(
					"Invalid access information");
		}
	    //****using the password hashed field to avoid schema changes for prod fix
	    boolean isMgo = false;
	    boolean isMmg = false;
	    if (access.getPasswordHash() != null) {
	    	if (access.getPasswordHash().equalsIgnoreCase("MGO")) {
	    		isMgo = true;
	    	} else if (access.getPasswordHash().equalsIgnoreCase("MMG")) {
	    		isMmg = true;
	    	}	    	
	    }
	    
	    //*******************************************************
		Long logonTryId;
		try {
			logonTryId = dao.addLoginTry(saveRequest.getLoginId(), 
					access.getIpAddress(), 
					access.getWebServerName(), 
					access.getWebServerIpAddress(),
					access.getLogonSuccessful(),
		           	new Boolean(isMgo),
		           	new Boolean(isMmg),
		           	saveRequest.getSourceSite());
		} catch (Exception e) {
			throw new CommandException("Failed to log access", e);
		}
		
		try {			    
			dao.addBlockedAttemptsNoConsumer (access.getIpAddressBlocked(), logonTryId);
		} catch (Exception e) {
			throw new CommandException("Failed to log logon_failures", e);
		}        	

		SaveLogonTryResponse response = new SaveLogonTryResponse();		
		return response;
	}

}
