/**
 * LoyaltyInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class LoyaltyInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String memberId;
	private java.lang.Boolean autoEnroll;

	public LoyaltyInfo() {
	}

	public LoyaltyInfo(java.lang.String memberId, java.lang.Boolean autoEnroll) {
		this.memberId = memberId;
		this.autoEnroll = autoEnroll;
	}

	/**
	 * Gets the memberId value for this LoyaltyInfo.
	 * 
	 * @return memberId
	 */
	public java.lang.String getMemberId() {
		return memberId;
	}

	/**
	 * Sets the memberId value for this LoyaltyInfo.
	 * 
	 * @param memberId
	 */
	public void setMemberId(java.lang.String memberId) {
		this.memberId = memberId;
	}

	/**
	 * Gets the autoEnroll value for this LoyaltyInfo.
	 * 
	 * @return autoEnroll
	 */
	public java.lang.Boolean getAutoEnroll() {
		return autoEnroll;
	}

	/**
	 * Sets the autoEnroll value for this LoyaltyInfo.
	 * 
	 * @param autoEnroll
	 */
	public void setAutoEnroll(java.lang.Boolean autoEnroll) {
		this.autoEnroll = autoEnroll;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof LoyaltyInfo))
			return false;
		LoyaltyInfo other = (LoyaltyInfo) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.memberId == null && other.getMemberId() == null) || (this.memberId != null && this.memberId
						.equals(other.getMemberId())))
				&& ((this.autoEnroll == null && other.getAutoEnroll() == null) || (this.autoEnroll != null && this.autoEnroll
						.equals(other.getAutoEnroll())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getMemberId() != null) {
			_hashCode += getMemberId().hashCode();
		}
		if (getAutoEnroll() != null) {
			_hashCode += getAutoEnroll().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ MemberId=").append(getMemberId());
		buffer.append(" AutoEnroll=").append(getAutoEnroll());
		buffer.append(" ]");
		return buffer.toString();
	}
}
