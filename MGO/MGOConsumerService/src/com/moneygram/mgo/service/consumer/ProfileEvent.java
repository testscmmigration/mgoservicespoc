/**
 * ProfileEvent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

/**
 * 
 * Profile Event.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/06/28 19:00:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ProfileEvent  implements java.io.Serializable {
    private java.lang.String event;

    private java.util.Calendar eventDateTime;

    public ProfileEvent() {
    }

    public ProfileEvent(
           java.lang.String event,
           java.util.Calendar eventDateTime) {
           this.event = event;
           this.eventDateTime = eventDateTime;
    }


    /**
     * Gets the event value for this ProfileEvent.
     * 
     * @return event
     */
    public java.lang.String getEvent() {
        return event;
    }


    /**
     * Sets the event value for this ProfileEvent.
     * 
     * @param event
     */
    public void setEvent(java.lang.String event) {
        this.event = event;
    }


    /**
     * Gets the eventDateTime value for this ProfileEvent.
     * 
     * @return eventDateTime
     */
    public java.util.Calendar getEventDateTime() {
        return eventDateTime;
    }


    /**
     * Sets the eventDateTime value for this ProfileEvent.
     * 
     * @param eventDateTime
     */
    public void setEventDateTime(java.util.Calendar eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProfileEvent)) return false;
        ProfileEvent other = (ProfileEvent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent()))) &&
            ((this.eventDateTime==null && other.getEventDateTime()==null) || 
             (this.eventDateTime!=null &&
              this.eventDateTime.equals(other.getEventDateTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        if (getEventDateTime() != null) {
            _hashCode += getEventDateTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ event=").append(getEvent());
		buffer.append(" eventDateTime=").append(getEventDateTime());
		buffer.append(" ]");
		return buffer.toString();
	}

}
