package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.extended.ExtendedContact;

public class ConsumerContactRowMapper extends BaseRowMapper {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerContactRowMapper.class);

	/**
	 *
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		String primaryPhoneFlag = null, emailTypeCode = null;
		for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
			if ("PRM_PHN_FLAG".equalsIgnoreCase(rsmd.getColumnName(i)))
				primaryPhoneFlag = rs.getString("PRM_PHN_FLAG");
			if ("ELEC_ADDR_TYPE_CODE".equalsIgnoreCase(rsmd.getColumnName(i)))
				emailTypeCode = rs.getString("ELEC_ADDR_TYPE_CODE");
		}
		ExtendedContact c = new ExtendedContact();
		if ("Y".equals(primaryPhoneFlag)) {
			c.setPrimaryPhone(rs.getString("CUST_PH_NBR"));
			c.setPrimaryPhoneBlocked(DAOUtils.toBoolean(rs.getString("PH_BLKD_CODE"),
					"B", null));
			c.setPrimaryPhoneType(rs.getString("BSNS_CODE"));
		} else if ("N".equals(primaryPhoneFlag)) {
			c.setAlternatePhone(rs.getString("CUST_PH_NBR"));
			c.setAlternatePhoneBlocked(DAOUtils.toBoolean(rs.getString("PH_BLKD_CODE"),
					"B", null));
			c.setAlternatePhoneType(rs.getString("BSNS_CODE"));
		} else if ("EMAIL".equals(emailTypeCode)) {
			c.setEmail(rs.getString("CUST_ELEC_ADDR_ID"));
			c.setEmailBlocked(DAOUtils.toBoolean(rs
					.getString("ELEC_ADDR_BLKD_CODE"), "B", null));
			c.setEmailDomainBlocked(DAOUtils.toBoolean(rs
					.getString("ELEC_ADDR_DOMN_BLKD_CODE"), "B", null));
		}
		return c;
	}
}
