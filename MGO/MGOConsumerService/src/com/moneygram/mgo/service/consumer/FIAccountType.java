/**
 * FIAccountType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class FIAccountType implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String _value_;
	private static java.util.HashMap<String, FIAccountType> _table_ = new java.util.HashMap<String, FIAccountType>();

	// Constructor
	protected FIAccountType(java.lang.String value) {
		_value_ = value;
		_table_.put(_value_, this);
	}

    public static final java.lang.String _value1 = "BANK-CHK";
    public static final java.lang.String _value2 = "BANK-SAV";
    public static final java.lang.String _value3 = "CC-VISA";
    public static final java.lang.String _value4 = "CC-MSTR";
    public static final java.lang.String _value5 = "CC-MAEST";
    public static final java.lang.String _value6 = "CC-DSCVR";
    public static final FIAccountType value1 = new FIAccountType(_value1);
    public static final FIAccountType value2 = new FIAccountType(_value2);
    public static final FIAccountType value3 = new FIAccountType(_value3);
    public static final FIAccountType value4 = new FIAccountType(_value4);
    public static final FIAccountType value5 = new FIAccountType(_value5);
    public static final FIAccountType value6 = new FIAccountType(_value6);

	public java.lang.String getValue() {
		return _value_;
	}

	public static FIAccountType fromValue(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		FIAccountType enumeration = (FIAccountType) _table_.get(value);
		if (enumeration == null)
			throw new java.lang.IllegalArgumentException();
		return enumeration;
	}

	public static FIAccountType fromString(java.lang.String value)
			throws java.lang.IllegalArgumentException {
		return fromValue(value);
	}

	public boolean equals(java.lang.Object obj) {
		return (obj == this);
	}

	public int hashCode() {
		return toString().hashCode();
	}

	public java.lang.String toString() {
		return _value_;
	}

	public java.lang.Object readResolve() throws java.io.ObjectStreamException {
		return fromValue(_value_);
	}
}
