/**
 * SaveProfileEventRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class SaveProfileEventRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private long consumerId;

    private com.moneygram.mgo.service.consumer.ProfileEvent event;

    public SaveProfileEventRequest() {
    }

    /**
     * Gets the consumerId value for this SaveProfileEventRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this SaveProfileEventRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the event value for this SaveProfileEventRequest.
     * 
     * @return event
     */
    public com.moneygram.mgo.service.consumer.ProfileEvent getEvent() {
        return event;
    }


    /**
     * Sets the event value for this SaveProfileEventRequest.
     * 
     * @param event
     */
    public void setEvent(com.moneygram.mgo.service.consumer.ProfileEvent event) {
        this.event = event;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveProfileEventRequest)) return false;
        SaveProfileEventRequest other = (SaveProfileEventRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId() &&
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ consumerId=").append(getConsumerId());
		buffer.append(" event=").append(getEvent());
		buffer.append(" ]");
		return buffer.toString();
	}
	
}
