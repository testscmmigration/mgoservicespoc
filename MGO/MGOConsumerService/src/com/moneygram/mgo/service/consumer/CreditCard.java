/**
 * CreditCard.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class CreditCard extends FIAccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.Integer expMonth;
	private java.lang.Integer expYear;
	private java.lang.Boolean binBlocked;
	private java.lang.String cardType;
	

	public CreditCard() {
	}

	/**
	 * Gets the expMonth value for this CreditCard.
	 * 
	 * @return expMonth
	 */
	public java.lang.Integer getExpMonth() {
		return expMonth;
	}

	/**
	 * Sets the expMonth value for this CreditCard.
	 * 
	 * @param expMonth
	 */
	public void setExpMonth(java.lang.Integer expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * Gets the expYear value for this CreditCard.
	 * 
	 * @return expYear
	 */
	public java.lang.Integer getExpYear() {
		return expYear;
	}

	/**
	 * Sets the expYear value for this CreditCard.
	 * 
	 * @param expYear
	 */
	public void setExpYear(java.lang.Integer expYear) {
		this.expYear = expYear;
	}

	/**
	 * Gets the binBlocked value for this CreditCard.
	 * 
	 * @return binBlocked
	 */
	public java.lang.Boolean getBinBlocked() {
		return binBlocked;
	}
	
    /**
     * Gets the cardType value for this CreditCard.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this CreditCard.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }

	/**
	 * Sets the binBlocked value for this CreditCard.
	 * 
	 * @param binBlocked
	 */
	public void setBinBlocked(java.lang.Boolean binBlocked) {
		this.binBlocked = binBlocked;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof CreditCard))
			return false;
		CreditCard other = (CreditCard) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.expMonth == null && other.getExpMonth() == null) || (this.expMonth != null && this.expMonth
						.equals(other.getExpMonth())))
				&& ((this.expYear == null && other.getExpYear() == null) || (this.expYear != null && this.expYear
						.equals(other.getExpYear())))
				&& ((this.binBlocked == null && other.getBinBlocked() == null) || (this.binBlocked != null && this.binBlocked
						.equals(other.getBinBlocked())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getExpMonth() != null) {
			_hashCode += getExpMonth().hashCode();
		}
		if (getExpYear() != null) {
			_hashCode += getExpYear().hashCode();
		}
		if (getBinBlocked() != null) {
			_hashCode += getBinBlocked().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ExpMonth=").append(getExpMonth());
		buffer.append(" ExpYear=").append(getExpYear());
		buffer.append(" BinBlocked=").append(getBinBlocked());
		buffer.append(" ]");
		return buffer.toString();
	}
}
