package com.moneygram.mgo.service.consumer.dao;


import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.Access;
import com.moneygram.mgo.service.consumer.BankAccount;
import com.moneygram.mgo.service.consumer.Comment;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.CreditCard;
import com.moneygram.mgo.service.consumer.CreditCardReponse;
import com.moneygram.mgo.service.consumer.MGOAttributes;
import com.moneygram.mgo.service.consumer.PersonalInfo;
import com.moneygram.mgo.service.consumer.ProfileEvent;
import com.moneygram.mgo.service.consumer.SavedAgent;
import com.moneygram.mgo.service.consumer.TransactionPreferences;
import com.moneygram.mgo.service.consumer.extended.ExtendedContact;
import com.moneygram.mgo.service.consumer.util.ConsumerUtil;
import com.moneygram.mgo.service.consumer.util.PartnerSiteIdentifier;
import com.moneygram.mgo.shared.ConsumerAddress;


public class ConsumerDAO extends BaseDAO {
    private static final Logger logger = LogFactory.getInstance().getLogger(
            ConsumerDAO.class);

    private static final Map<Integer, Class<? extends Exception>> errorsMap = new HashMap<Integer, Class<? extends Exception>>();
    static {
        errorsMap.put(new Integer(20010), AccountExistsDAOException.class);
    }

    private static final String DB_CALL_TYPE_CODE = "WEB";
    private static final String DB_CALLER_ID = "MGO";
    private static final String DEFAULT_PARTNER_SITE_ID = "MGO";
    private static final int LOGON_NONMGO_NONMMG = -1;
    private static final int LOGON_MGO_GOOD = 0;
    private static final int LOGON_MGO_BAD = 1;
    private static final int LOGON_MMG_GOOD = 2;
    private static final int LOGON_MMG_BAD = 3;

	private static final String INSERT_LOGON_TRY = "pkg_mgo_cust_profile.prc_insert_logon_try";
	private static final String INSERT_BLOCKED_LOGON = "pkg_mgo_cust_profile.prc_insert_logon_failure";
    private static final String GET_ACTIVE_CUST_PROC = "pkg_mgo_cust_profile.prc_get_active_cust_cv";
    private static final String GET_BLOCKED_IP_ADDR_PROC = "pkg_mgo_cust_profile.prc_get_blocked_ip_addr_cv";
    private static final String UPDATE_CUST_REWARDS_INFO_PROC = "pkg_mgo_cust_profile.prc_update_cust_rewards_info";
    private static final String IU_CUST_PASSWORD_PROC = "pkg_mgo_cust_profile.prc_iu_cust_password";
    private static final String GET_BLOCKED_BANK_ACCT_PROC = "pkg_mgo_cust_profile.prc_get_blocked_bank_acct";
    private static final String GET_BLKD_CC_ACCT_AND_BIN_PROC = "pkg_mgo_cust_profile.prc_get_blkd_cc_acct_and_bin";
    private static final String GET_BLOCKED_CUST_PROFILE_PROC = "pkg_mgo_cust_profile.prc_get_blocked_cust_profile";
    private static final String UPDATE_CUST_STAT_PROC = "pkg_mgo_cust_profile.prc_update_cust_stat";
    private static final String INSRT_CUST_COMMENT_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_comment";
    private static final String INSRT_CUST_ACCT_COMMENT_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_acct_comment";
    private static final String INSRT_CUST_ACCOUNT_BANK_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_account_bank";
    private static final String INSRT_CUST_ACCOUNT_CC_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_account_cc";
    private static final String INSRT_CUST_PROFILE_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_profile";
    private static final String GET_CUST_ELEC_ADDR_PROC = "pkg_mgo_cust_profile.prc_get_cust_elec_addr_cv";
    private static final String GET_CUST_PHONE_PROC = "pkg_mgo_cust_profile.prc_get_cust_phone_cv";
    private static final String GET_CUST_ADDRESS_PROC = "pkg_mgo_cust_profile.prc_get_cust_address_cv";
    private static final String GET_CUSTOMER_PROC = "pkg_mgo_cust_profile.prc_get_customer_cv";
    private static final String INSRT_CUST_SAVED_AGT_PROC = "pkg_mgo_cust_profile.prc_insrt_cust_saved_agt";
    private static final String GET_CUST_SAVED_AGT_PROC = "pkg_mgo_cust_profile.prc_get_cust_saved_agt";
    private static final String UPDATE_CONSUMER_SECURITY_QUESTIONS_COLLECTION_DATE_PROC = "PKG_MGO_CUST_PROFILE.prc_update_adptv_authn_info";
    private static final String UPDATE_ACCOUNT_STATUS_PROC = "PKG_MGO_CUST_PROFILE.prc_update_cust_account_status";
    private static final String GET_CUST_ACCOUNT_PROC = "PKG_MGO_CUST_PROFILE.prc_get_cust_account";
    private static final String UPDATE_CUST_PROMO_EMAIL_CD_PROC = "PKG_MGO_CUST_PROFILE.prc_update_cust_promo_email_cd";
    private static final String UPDATE_ADDRESS_PROC = "PKG_MGO_CUST_PROFILE.prc_iu_address";
    private static final String UPDATE_PHONE_PROC = "PKG_MGO_CUST_PROFILE.prc_iud_cust_phone";
    private static final String GET_INCOMPLETE_PROFILES_PROC = "PKG_MGO_CUST_PROFILE.prc_get_incomplete_profiles";
    private static final String GET_PROFILE_EVENTS_PROC = "PKG_MGO_CUST_PROFILE.prc_get_cust_acty_bsns_cd";
    private static final String SAVE_PROFILE_EVENT_PROC = "PKG_MGO_CUST_PROFILE.prc_insrt_cust_acty_bsns_cd";
    private static final String GET_BLOCKED_PHONE_CV = "PKG_EM_BLOCKED_LIST.prc_get_blocked_phone_cv";
    private static final String GET_CUSTOMER_EXTERNAL_IDENT = "pkg_em_customer_profile.prc_get_cust_extnl_ident_cv";
    
    private static final String UPDATE_CREDITCARD_TYPE = "PKG_MGO_CUST_PROFILE.prc_update_cust_acct_crcd_type";

    private static final String GET_CC_NEXT_SEQ_VAL = "PKG_MGO_CUST_PROFILE.PRC_GET_CC_NEXT_SEQ_VAL";
    private static final String UPDATE_CUST_NAME = "PKG_MGO_CUST_PROFILE.PRC_UPDATE_CUST_NAME";
    
     

    protected static final String CC_BIN_BLOCKED = "CCBINBLOCK";
    protected static final String CC_BLOCKED = "CCBLOCK";
    protected static final String BANK_ACCT_BLOCKED = "BABLOCK";
    protected static final String BANK_ABA_BLOCKED = "BABABLOCK";
    protected static final String CUST_BLOCKED = "B";
    protected static final String CUST_PRIMARY_PHONE_TYPE = "Y";
    protected static final String CUST_ALTERNATE_PHONE_TYPE = "N";

    protected static final String ACCT_NBR_WILDCARDED = "**********";

    /**
	 * Returns procedure log id.
	 *
	 * @param result
	 * @return procedure log id
	 */
	private Number getProcedureLogId(Map<?, ?> result) {
		return (Number) result.get("ov_prcs_log_id");
	}

	/**
	 * Returns list of consumer agents.
	 *
	 * @param consumerId
	 * @param loginId
	 * @return list of consumer agents
	 */
	@SuppressWarnings("unchecked")
	public List<SavedAgent> getAgents(Long consumerId, String loginId)
			throws DAOException {
		if (consumerId == null && loginId == null)
			throw new DAOException("Invalid consumerId/loginId provided");

		SimpleJdbcCall retriveAgents = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_CUST_SAVED_AGT_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_login_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_saved_agt_cv",
								OracleTypes.CURSOR, new SavedAgentRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_login_id", loginId);
		parameters.addValue("iv_call_type_code", "MGO");

		Map<?, ?> result = retriveAgents.execute(parameters);
		List<SavedAgent> agents = (List<SavedAgent>) result
				.get("ov_cust_saved_agt_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getAgents: procedure log id="
					+ getProcedureLogId(result));
		}

		return agents;
	}

	/**
	 * Saves consumer agents.
	 *
	 * @param consumer
	 */
	public void saveAgents(Consumer consumer) throws DAOException {
		if (consumer == null)
			throw new DAOException("Invalid consumer provided");
		if (consumer.getConsumerId() == null && consumer.getLoginId() == null)
			throw new DAOException(
					"Invalid consumerId/consumerLoginId provided");

		SimpleJdbcCall saveAgents = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(INSRT_CUST_SAVED_AGT_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_cust_id", Types.DECIMAL),
						new SqlParameter("iv_login_id", Types.VARCHAR),
						new SqlParameter("iv_agent_id1", Types.DECIMAL),
						new SqlParameter("iv_agent_id2", Types.DECIMAL),
						new SqlParameter("iv_agent_id3", Types.DECIMAL),
						new SqlParameter("iv_agent_id4", Types.DECIMAL),
						new SqlParameter("iv_agent_id5", Types.DECIMAL),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_cust_id", consumer.getConsumerId());
		parameters.addValue("iv_login_id", consumer.getLoginId());
		TransactionPreferences transactionPreferences = consumer
				.getTransactionPreferences();
		SavedAgent[] agents = (transactionPreferences == null ? null
				: transactionPreferences.getSavedAgents());

		if (agents == null) {
			throw new DAOException("List of agents to save cannot be null");
		}

		int index = 0;
		parameters.addValue("iv_agent_id1", index >= agents.length ? null
				: agents[index++].getAgentId());
		parameters.addValue("iv_agent_id2", index >= agents.length ? null
				: agents[index++].getAgentId());
		parameters.addValue("iv_agent_id3", index >= agents.length ? null
				: agents[index++].getAgentId());
		parameters.addValue("iv_agent_id4", index >= agents.length ? null
				: agents[index++].getAgentId());
		parameters.addValue("iv_agent_id5", index >= agents.length ? null
				: agents[index++].getAgentId());

		parameters.addValue("iv_call_type_code", "MGO");

		Map<?, ?> result = saveAgents.execute(parameters);

		if (logger.isDebugEnabled()) {
			logger.debug("saveAgents: procedure log id="
					+ getProcedureLogId(result));
		}
	}

	/**
	 * Please use <pre>
	 * getConsumer(Long consumerId, String consumerLoginId, String
	 * parterSiteId)
	 * </pre> instead of this method
	 *
	 * @param consumerId
	 * @param consumerLoginId
	 * @return
	 * @throws DAOException
	 */
	@Deprecated
	public Consumer getConsumer(Long consumerId, String consumerLoginId)
			throws DAOException {
		return getConsumer(consumerId, consumerLoginId, DEFAULT_PARTNER_SITE_ID);
	}

	private List<Consumer> getConsumerBasicInfo(Long consumerId, String consumerLoginId,
			String partnerSiteId) throws DAOException {
		if (consumerId == null && consumerLoginId == null)
			throw new DAOException(
					"Invalid consumerId/consumerLoginId provided");

		SimpleJdbcCall getConsumers = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_CUSTOMER_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_cust_logon_id", Types.VARCHAR),
						new SqlParameter("iv_cust_stat_code", Types.VARCHAR),
						new SqlParameter("iv_cust_sub_stat_code", Types.VARCHAR),
						new SqlParameter("iv_cust_gu_id", Types.VARCHAR),
						new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_customer_cv",
								OracleTypes.CURSOR, new ConsumerRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_cust_logon_id", consumerLoginId);
		parameters.addValue("iv_cust_stat_code", null);
		parameters.addValue("iv_cust_sub_stat_code", null);
		parameters.addValue("iv_cust_gu_id", null);
        parameters.addValue("iv_src_web_site_bsns_cd", partnerSiteId);

		Map<?, ?> result = getConsumers.execute(parameters);
		List<Consumer> consumers = (List<Consumer>) result.get("ov_customer_cv");

		if (logger.isDebugEnabled()) {
			if (consumers == null)
				logger.debug("getConsumer: returned list is null");
			else
				logger.debug("getConsumer: returned list size is "
						+ consumers.size());
			logger.debug("getConsumer: procedure log id="
					+ getProcedureLogId(result));
		}

		if (consumers == null || consumers.size() != 1)
			return null;
		return consumers;
	}


	/**
	 * Returns list of consumers.
	 *
	 * @param consumerId
	 * @param consumerLoginId
	 * @return list of consumers
	 */
	@SuppressWarnings("unchecked")
	public Consumer getConsumer(Long consumerId, String consumerLoginId, String partnerSiteId)
			throws DAOException {
		List<Consumer> consumers = getConsumerBasicInfo(consumerId, consumerLoginId, partnerSiteId);
		if (consumers == null) {
			return null;
		}
		Consumer consumer = consumers.get(0);
		if (consumer == null) {
			return null;
		}
		try {
			fetchConsumerExternalIdentificationInfo(consumer);
		} catch (SQLException e) {
			logger.debug("Problems while reading customer external identification", e);
		}
		return consumer;
	}

	private void fetchConsumerExternalIdentificationInfo(Consumer consumer) throws SQLException {
		SimpleJdbcCall callCostumers = new SimpleJdbcCall(getJdbcTemplate())
		.withProcedureName(GET_CUSTOMER_EXTERNAL_IDENT)
		.withoutProcedureColumnMetaDataAccess()
		.declareParameters(new SqlParameter("iv_user_id", Types.VARCHAR),
				new SqlParameter("iv_call_type_code", Types.VARCHAR),
				new SqlParameter("iv_cust_id", Types.VARCHAR),
				new SqlParameter("iv_encrypt_extnl_id", Types.VARCHAR),
				new SqlParameter("iv_mask_extnl_id", Types.VARCHAR),
				new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
				new SqlOutParameter("ov_cust_extnl_ident_cv", OracleTypes.CURSOR, 
						new PersonalInfoRowMapper(consumer.getPersonal())));
		
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumer.getConsumerId());
		parameters.addValue("iv_encrypt_extnl_id", null);
		parameters.addValue("iv_mask_extnl_id", null);
		
		Map<String, Object> result = callCostumers.execute(parameters);
		
		@SuppressWarnings("unchecked")
		List<PersonalInfo> poList = (List<PersonalInfo>) result.get("ov_cust_extnl_ident_cv");
		if (poList.size() > 0){
			consumer.setPersonal(poList.get(0));
		}
			
	}

    /**
     * Returns list of consumer addresses.
     *
     * @param consumerId
     * @return list of active consumer addresses
     */
    public List<ConsumerAddress> getConsumerAddresses(Long consumerId) throws DAOException {
        return getConsumerAddresses(consumerId, null);
    }

    /**
	 * Returns list of consumer addresses.
	 *
	 * @param consumerId
	 * @param addressId
	 * @return list of active consumer addresses or a specific address
	 */
	@SuppressWarnings("unchecked")
	public List<ConsumerAddress> getConsumerAddresses(Long consumerId, Long addressId)
			throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		SimpleJdbcCall getAddresses = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_CUST_ADDRESS_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_addr_id", Types.INTEGER),
						new SqlParameter("iv_addr_stat_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_address_cv",
								OracleTypes.CURSOR,
								new ConsumerAddressRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_addr_id", addressId);
		//if specific address is provided then do not specify the status
        //if no specific address is provided then specify the active status
		parameters.addValue("iv_addr_stat_code", (addressId == null ? "ACT" : null));

		Map<?, ?> result = getAddresses.execute(parameters);
		List<ConsumerAddress> addresses = (List<ConsumerAddress>) result
				.get("ov_cust_address_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getConsumerAddresses: procedure log id="
					+ getProcedureLogId(result));
		}

		return addresses;
	}

	/**
	 * Returns list of consumer phone contacts. Promo email used to hold blocked status.
	 *
	 * @param consumerId
	 * @return list of consumer phone contacts
	 */
	@SuppressWarnings("unchecked")
	public List<ExtendedContact> getConsumerPhoneContacts(Long consumerId)
			throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		SimpleJdbcCall getContacts = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_CUST_PHONE_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_prm_phn_flag", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_phone_cv",
								OracleTypes.CURSOR,
								new ConsumerContactRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_prm_phn_flag", null);

		Map<?, ?> result = getContacts.execute(parameters);
		List<ExtendedContact> contacts = (List<ExtendedContact>) result.get("ov_cust_phone_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getConsumerPhoneContacts: procedure log id="
					+ getProcedureLogId(result));
		}

		return contacts;
	}

	/**
	 * Returns list of consumer email contacts. Promo email used to hold blocked status.
	 *
	 * @param consumerId
	 * @return list of consumer email contacts
	 */
	@SuppressWarnings("unchecked")
	public List<ExtendedContact> getConsumerEmailContacts(Long consumerId)
			throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		SimpleJdbcCall getContacts = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_CUST_ELEC_ADDR_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_elec_addr_stat_code",
								Types.VARCHAR),
						new SqlParameter("iv_elec_addr_type_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_electronic_addr_cv",
								OracleTypes.CURSOR,
								new ConsumerContactRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_elec_addr_stat_code", null);
		parameters.addValue("iv_elec_addr_type_code", null);

		Map<?, ?> result = getContacts.execute(parameters);
		List<ExtendedContact> contacts = (List<ExtendedContact>) result
				.get("ov_cust_electronic_addr_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getConsumerEmailContacts: procedure log id="
					+ getProcedureLogId(result));
		}

		return contacts;
	}

	/**
	 * Adds consumer profile.
	 *
	 * @param consumer
	 * @param cons
	 * @return consumerId
	 */
	public Long addConsumerProfile(Consumer consumer, Long gcpId) throws DAOException {

		Map<?, ?> result = executeCreation(consumer, gcpId);

		Long consumerId = null;
		Number cId = (Number) result.get("ov_cust_id");
		if (cId != null)
			consumerId = new Long(cId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addConsumerProfile: procedure log id="
					+ getProcedureLogId(result) +" consumerId="+ consumerId);
		}

		return consumerId;
	}

	private Map<?, ?> executeCreation(Consumer consumer, Long gcpId) throws DAOException {
		SimpleJdbcCall addConsumerProfile = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(INSRT_CUST_PROFILE_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_edir_gu_id", Types.VARCHAR),
						new SqlParameter("iv_cust_last_name", Types.VARCHAR),
						new SqlParameter("iv_cust_matrnl_name", Types.VARCHAR),
						new SqlParameter("iv_cust_frst_name", Types.VARCHAR),
						new SqlParameter("iv_cust_mid_name", Types.VARCHAR),
						new SqlParameter("iv_cust_ssn_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_cust_brth_date", Types.DATE),
						new SqlParameter("iv_cust_stat_code", Types.VARCHAR),
						new SqlParameter("iv_cust_sub_stat_code", Types.VARCHAR),
						new SqlParameter("iv_cust_gu_id", Types.VARCHAR),
						new SqlParameter("iv_promo_email_code", Types.VARCHAR),
						new SqlParameter("iv_cust_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_addr_line1_text", Types.VARCHAR),
						new SqlParameter("iv_addr_line2_text", Types.VARCHAR),
						new SqlParameter("iv_addr_line3_text", Types.VARCHAR),
						new SqlParameter("iv_addr_city_name", Types.VARCHAR),
						new SqlParameter("iv_addr_state_name", Types.VARCHAR),
						new SqlParameter("iv_addr_postal_code", Types.VARCHAR),
						new SqlParameter("iv_addr_cntry_id", Types.VARCHAR),
//									new SqlParameter("iv_addr_bldg_name", Types.VARCHAR),	// S28
						new SqlParameter("iv_addr_cnty_name", Types.VARCHAR),	// S28
						new SqlParameter("iv_addr_bldg_name", Types.VARCHAR),	// vl58 - S11 (MGO new architecture)
						new SqlParameter("iv_primary_phone", Types.VARCHAR),
						new SqlParameter("iv_prm_phn_type_bsns_code", Types.CHAR, 3),	// S28
						new SqlParameter("iv_prm_ph_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_alternate_phone", Types.VARCHAR),
						new SqlParameter("iv_alt_phn_type_bsns_code", Types.CHAR, 3),	// S28
						new SqlParameter("iv_alt_ph_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_email_addr", Types.VARCHAR),
						new SqlParameter("iv_elec_addr_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_elec_addr_domn_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_cust_cmnt_reas_code", Types.VARCHAR),
						new SqlParameter("iv_cust_cmnt_text", Types.VARCHAR),
				        new SqlParameter("iv_create_ip_addr_id", Types.VARCHAR),
				        new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
				        new SqlParameter("iv_encrypt_extnl_id", Types.VARCHAR),	// S28
				        new SqlParameter("iv_mask_extnl_id", Types.VARCHAR),	// S28
				        new SqlParameter("iv_send_iso_cntry_code", Types.VARCHAR),	// S28
				        new SqlParameter("iv_ident_doc_bsns_code", Types.VARCHAR),	// S28
				        new SqlParameter("iv_issu_iso_cntry_code", Types.VARCHAR),	// S28
				        new SqlParameter("iv_issu_addr_state_name", Types.VARCHAR),	// S28
				        new SqlParameter("iv_issu_date", Types.DATE),	// S28
				        new SqlParameter("iv_exp_date", Types.DATE),	// S28
				        new SqlParameter("iv_gndr_code", Types.CHAR, 1),	// S28
				        new SqlParameter("iv_cnsmr_prfl_id", Types.NUMERIC, 1),	// vl58 - S8 (MGO new architecture), GCP profile id retrieved from GCP service
				        new SqlParameter("iv_ocupn_code", Types.NUMERIC, 1),	//  vl58 - S11 (MGO new architecture, must be null)
				        new SqlParameter("iv_cust_brth_iso_cntry_code", Types.VARCHAR),	//  vl58 - S11 (MGO new architecture, must be null)
				        new SqlParameter("iv_lang_tag_text", Types.VARCHAR),	//  vl58 - S11 (MGO new architecture, must be null)
				        new SqlParameter("iv_encrypt_psprt_mrz_ln2_text", Types.VARCHAR),	//  vl58 - S12 (MGO new architecture, must be null)
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_id", Types.NUMERIC));

		System.out.println(addConsumerProfile.getCallString());
        Map<?, ?> result = addConsumerProfile.execute(fillParameters(consumer,gcpId));
		return result;
	}

	private MapSqlParameterSource fillParameters(Consumer consumer, Long gcpId)
	throws DAOException {
		MapSqlParameterSource parameters = new MapSqlParameterSource();

		String commentCode = null, commentText = null;
		if (consumer.getComments() != null && consumer.getComments().length > 0
				&& consumer.getComments()[0] != null
				&& consumer.getComments()[0].getReasonCode() != null
				&& consumer.getComments()[0].getCommentText() != null) {
			commentCode = consumer.getComments()[0].getReasonCode();
			commentText = consumer.getComments()[0].getCommentText();
		}

		parameters.addValue("iv_user_id", consumer.getLoginId());
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_edir_gu_id", consumer.getInternal().getGlobalUniqueId());
		parameters.addValue("iv_cust_last_name", consumer.getPersonal().getLastName());
		parameters.addValue("iv_cust_matrnl_name", consumer.getPersonal().getSecondLastName());
		parameters.addValue("iv_cust_frst_name", consumer.getPersonal().getFirstName());
		parameters.addValue("iv_cust_mid_name", consumer.getPersonal().getMiddleName());
		parameters.addValue("iv_cust_ssn_mask_nbr", determineLastFourMask(consumer.getPersonal().getSsn()));
		parameters.addValue("iv_cust_brth_date", consumer.getPersonal().getDateOfBirth());
		parameters.addValue("iv_cust_stat_code", consumer.getInternal().getConsumerStatus());
		parameters.addValue("iv_cust_sub_stat_code", consumer.getInternal().getConsumerSubStatus());
		parameters.addValue("iv_cust_gu_id", consumer.getLoginId());
		parameters.addValue("iv_promo_email_code", DAOUtils.toString(consumer.getContact().getPromoEmail()));
		parameters.addValue("iv_cust_blkd_code", getBlockedCode(consumer.getInternal().getConsumerBlocked()));
		parameters.addValue("iv_addr_line1_text", consumer.getAddress().getLine1());
		parameters.addValue("iv_addr_line2_text", consumer.getAddress().getLine2());
		parameters.addValue("iv_addr_line3_text", consumer.getAddress().getLine3());
		parameters.addValue("iv_addr_city_name", consumer.getAddress().getCity());
		parameters.addValue("iv_addr_state_name", consumer.getAddress().getState());
		parameters.addValue("iv_addr_postal_code", consumer.getAddress().getZipCode());
		parameters.addValue("iv_addr_cntry_id", consumer.getAddress().getCountry());
		parameters.addValue("iv_addr_cnty_name", consumer.getAddress().getCounty());
		parameters.addValue("iv_addr_bldg_name", null);
		parameters.addValue("iv_primary_phone", consumer.getContact().getPrimaryPhone());
		parameters.addValue("iv_prm_phn_type_bsns_code", consumer.getContact().getPrimaryPhoneType());
		parameters.addValue("iv_prm_ph_blkd_code", getBlockedCode(consumer.getInternal().getPrimaryPhoneBlocked()));
		parameters.addValue("iv_alternate_phone", consumer.getContact().getAlternatePhone());
		parameters.addValue("iv_alt_phn_type_bsns_code", consumer.getContact().getAlternatePhoneType());
		parameters.addValue("iv_alt_ph_blkd_code", getBlockedCode(consumer.getInternal().getAlternatePhoneBlocked()));
		parameters.addValue("iv_email_addr", consumer.getContact().getEmail());
		parameters.addValue("iv_elec_addr_blkd_code", getBlockedCode(consumer.getInternal().getEmailBlocked()));
		parameters.addValue("iv_elec_addr_domn_blkd_code", getBlockedCode(consumer.getInternal().getEmailDomainBlocked()));
		parameters.addValue("iv_cust_cmnt_reas_code", commentCode);
		parameters.addValue("iv_cust_cmnt_text", commentText);
		parameters.addValue("iv_create_ip_addr_id", consumer.getAccess() == null ? null : consumer.getAccess().getIpAddress());
		parameters.addValue("iv_src_web_site_bsns_cd", consumer.getSourceSite());
		// Begin - vl58 - S8 (MGO new architecture), GCP profile id retrieved from GCP service
		if(gcpId != null){
			parameters.addValue("iv_cnsmr_prfl_id", gcpId);
			//Begin - vl58 - MGO-4360: Change to retrieve global consumer ID from customer table to UIs
			consumer.setCnsmrProfileId(gcpId);
			//Begin - vl58 - MGO-4360
		}
		// End - vl58 - S8 (MGO new architecture), GCP profile id retrieved from GCP service
		

		PersonalInfo personal = consumer.getPersonal();
		String externalId = personal.getExternalId();
		String sourceSite = consumer.getSourceSite();

		boolean isMGOUK = PartnerSiteIdentifier.MGOUK.equals(sourceSite);
		parameters.addValue("iv_encrypt_extnl_id", isMGOUK ? ConsumerUtil.encryptWithNoPadding(externalId) : null);
		parameters.addValue("iv_mask_extnl_id", isMGOUK ? determineLastFourMask(externalId) : null);
		parameters.addValue("iv_send_iso_cntry_code", PartnerSiteIdentifier.COUNTRIES_BY_PARTNER_SITE.get(consumer.getSourceSite()));
		parameters.addValue("iv_ident_doc_bsns_code", isMGOUK ? personal.getExternalIdType() : null);
		parameters.addValue("iv_issu_iso_cntry_code", isMGOUK ? personal.getExternalIdCountryOfIssuance() : null);
		parameters.addValue("iv_issu_addr_state_name", isMGOUK ? personal.getExternalIdStateOfIssuance() : null);
		parameters.addValue("iv_issu_date", isMGOUK ? personal.getExternalIdDateOfIssuance() : null);
		parameters.addValue("iv_exp_date", isMGOUK ? personal.getExternalIdDateOfExpiration() : null);
		parameters.addValue("iv_gndr_code", personal.getGender());
		
		//Begin - vl58 - this new parameters must be passed with null value, to make the procedure work
		parameters.addValue("iv_ocupn_code", null);
		parameters.addValue("iv_cust_brth_iso_cntry_code", null);
		parameters.addValue("iv_lang_tag_text", null);
		parameters.addValue("iv_encrypt_psprt_mrz_ln2_text", null);
		//End - vl58 - this new parameters must be passed with null value, to make the procedure work

		if(logger.isDebugEnabled()){
			logger.debug("Parameters: " + parameters.getValues());
		}

		return parameters;
	}

	private String getBlockedCode(Boolean blockedValue) {
		return DAOUtils.toString(blockedValue, "B", "N");
	}

	/**
	 * Adds credit card account.
	 *
	 * @param consumerId
	 * @param addressId
	 * @param account
	 * @return accountId
	 */
	public Long addCreditCardAccount(Long consumerId, Long addressId,
			CreditCard account, String acctHolderName, String blockedId) throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		/** vl58 - Release 36 > parameter iv_addr_id removed
		if (addressId == null)
			throw new DAOException("Invalid addressId provided");
		 **/

		if (account == null)
			throw new DAOException("Invalid credit card account provided");
		if (acctHolderName == null)
			throw new DAOException("Invalid acctHolderName provided");
		String binNumber = determineCreditCardBin(account.getAccountNumber());
		if (binNumber == null)
			throw new DAOException("Invalid credit card bin provided");

		SimpleJdbcCall addConsumerAccount = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(INSRT_CUST_ACCOUNT_CC_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),

						//vl58 - Release 36 > parameter iv_addr_id removed
						//new SqlParameter("iv_addr_id", Types.INTEGER),
						new SqlParameter("iv_acct_type_code", Types.VARCHAR),
						new SqlParameter("iv_acct_stat_code", Types.VARCHAR),
						new SqlParameter("iv_acct_sub_stat_code", Types.VARCHAR),
						new SqlParameter("iv_acct_encryp_nbr", Types.VARCHAR),
						new SqlParameter("iv_acct_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_acct_holdr_name", Types.VARCHAR),
						new SqlParameter("iv_crcd_bin_hash_text", Types.VARCHAR),
						new SqlParameter("iv_crcd_exp_mth_nbr", Types.INTEGER),
						new SqlParameter("iv_crcd_exp_yr_nbr", Types.INTEGER),
						new SqlParameter("iv_acct_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_crcd_bin_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_crcd_type_code", Types.VARCHAR),
						//new SqlParameter("iv_crcd_nbr_hash_text", Types.VARCHAR),
						new SqlParameter("iv_blkd_crcd_acct_id", Types.NUMERIC),
						new SqlParameter("iv_crcd_bin_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_cust_acct_cmnt_reas_code",
								Types.VARCHAR),
						new SqlParameter("iv_cmnt_text", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_acct_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);

		//vl58 - Release 36 > parameter iv_addr_id removed
		//parameters.addValue("iv_addr_id", addressId);

		parameters.addValue("iv_acct_type_code", account.getAccountType()
				.getValue());
		parameters.addValue("iv_acct_stat_code", account.getAccountStatus());
		parameters.addValue("iv_acct_sub_stat_code", account
				.getAccountSubStatus());
		parameters.addValue("iv_acct_encryp_nbr", null);
		parameters.addValue("iv_acct_mask_nbr", determineLastFourMask(account
				.getAccountNumber()));
		parameters.addValue("iv_acct_holdr_name", acctHolderName);
		parameters.addValue("iv_crcd_bin_hash_text", ConsumerUtil
				.hash(binNumber));
		parameters.addValue("iv_crcd_exp_mth_nbr", account.getExpMonth());
		parameters.addValue("iv_crcd_exp_yr_nbr", account.getExpYear());
		parameters.addValue("iv_acct_blkd_code", DAOUtils.toString(account
				.getBlocked(), "B", "N"));
		parameters.addValue("iv_crcd_bin_blkd_code", DAOUtils.toString(account
				.getBinBlocked(), "B", "N"));
		// U-Unknown, C-Credit, D-Debit
		parameters.addValue("iv_crcd_type_code", "U");
		//parameters.addValue("iv_crcd_nbr_hash_text", ConsumerUtil.hash(account.getAccountNumber()));
		parameters.addValue("iv_blkd_crcd_acct_id", blockedId != null ? new Long(blockedId) : null);
		parameters.addValue("iv_crcd_bin_mask_nbr", binNumber);

		if (account.getComments() != null && account.getComments().length > 0
				&& account.getComments()[0] != null
				&& account.getComments()[0].getReasonCode() != null
				&& account.getComments()[0].getCommentText() != null) {
			parameters.addValue("iv_cust_acct_cmnt_reas_code", account
					.getComments()[0].getReasonCode());
			parameters.addValue("iv_cmnt_text", account.getComments()[0]
					.getCommentText());
		} else {
			parameters.addValue("iv_cust_acct_cmnt_reas_code", null);
			parameters.addValue("iv_cmnt_text", null);
		}

		Map<String, Object> result = executeJdbcCall(addConsumerAccount, parameters, errorsMap);

		Long accountId = null;
		Number aId = (Number) result.get("ov_cust_acct_id");
		if (aId != null)
			accountId = new Long(aId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addCreditCardAccount: procedure log id="
					+ getProcedureLogId(result));
		}

		return accountId;
	}

	/**
	 * Adds bank account.
	 *
	 * @param consumerId
	 * @param addressId
	 * @param account
	 * @return accountId
	 */
	public Long addBankAccount(Long consumerId, Long addressId,
			BankAccount account, String acctHolderName) throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		/** vl58 - Release 36 > parameter iv_addr_id removed
		if (addressId == null)
			throw new DAOException("Invalid addressId provided");
		 **/


		if (account == null)
			throw new DAOException("Invalid bank account provided");

		SimpleJdbcCall addConsumerAccount = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(INSRT_CUST_ACCOUNT_BANK_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						//vl58 - Release 36 > parameter iv_addr_id removed
						//new SqlParameter("iv_addr_id", Types.INTEGER),
						new SqlParameter("iv_acct_type_code", Types.VARCHAR),
						new SqlParameter("iv_acct_stat_code", Types.VARCHAR),
						new SqlParameter("iv_acct_sub_stat_code", Types.VARCHAR),
						new SqlParameter("iv_acct_encryp_nbr", Types.VARCHAR),
						new SqlParameter("iv_acct_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_acct_holdr_name", Types.VARCHAR),
						new SqlParameter("iv_bank_aba_nbr", Types.VARCHAR),
						new SqlParameter("iv_fin_instn_name", Types.VARCHAR),
						new SqlParameter("iv_acct_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_bank_aba_blkd_code", Types.VARCHAR),
						new SqlParameter("iv_cust_acct_cmnt_reas_code",
								Types.VARCHAR),
						new SqlParameter("iv_cmnt_text", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_acct_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);

		//vl58 - Release 36 > parameter iv_addr_id removed
		//parameters.addValue("iv_addr_id", addressId);

		parameters.addValue("iv_acct_type_code", account.getAccountType()
				.getValue());
		parameters.addValue("iv_acct_stat_code", account.getAccountStatus());
		parameters.addValue("iv_acct_sub_stat_code", account
				.getAccountSubStatus());
		parameters.addValue("iv_acct_encryp_nbr", ConsumerUtil.encrypt(account
				.getAccountNumber()));
		parameters.addValue("iv_acct_mask_nbr", determineLastFourMask(account
				.getAccountNumber()));
		parameters.addValue("iv_acct_holdr_name", acctHolderName);
		parameters.addValue("iv_bank_aba_nbr", account.getAbaNumber());
		parameters.addValue("iv_fin_instn_name", account.getFiName());
		parameters.addValue("iv_acct_blkd_code", DAOUtils.toString(account
				.getBlocked(), "B", "N"));
		parameters.addValue("iv_bank_aba_blkd_code", DAOUtils.toString(account
				.getAbaBlocked(), "B", "N"));

		if (account.getComments() != null && account.getComments().length > 0
				&& account.getComments()[0] != null
				&& account.getComments()[0].getReasonCode() != null
				&& account.getComments()[0].getCommentText() != null) {
			parameters.addValue("iv_cust_acct_cmnt_reas_code", account
					.getComments()[0].getReasonCode());
			parameters.addValue("iv_cmnt_text", account.getComments()[0]
					.getCommentText());
		} else {
			parameters.addValue("iv_cust_acct_cmnt_reas_code", null);
			parameters.addValue("iv_cmnt_text", null);
		}

        Map<String, Object> result = executeJdbcCall(addConsumerAccount, parameters, errorsMap);
		Long accountId = null;
		Number aId = (Number) result.get("ov_cust_acct_id");
		if (aId != null)
			accountId = new Long(aId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addBankAccount: procedure log id="
					+ getProcedureLogId(result));
		}

		return accountId;
	}

	/**
	 * Returns list of accounts.
	 *
	 * @param consumerId
	 * @param accountId
	 * @param status optional account status
	 * @return list of accounts
	 */
	@SuppressWarnings("unchecked")
	public List<ConsumerFIAccount> getConsumerAccounts(Long consumerId,
			Long accountId, String status) throws DAOException {

        SimpleJdbcCall getTransactionsByConsumer =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_CUST_ACCOUNT_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlParameter("iv_cust_acct_id", Types.INTEGER),
                        new SqlParameter("iv_acct_stat_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_cust_account_info_cv", OracleTypes.CURSOR, new ConsumerAccountsRowMapper())
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("iv_call_type_code", "MGO");
        parameters.addValue("iv_cust_id", consumerId);
        parameters.addValue("iv_cust_acct_id", accountId);
        parameters.addValue("iv_acct_stat_code", status);

        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerAccounts: calling procedure="+ GET_CUST_ACCOUNT_PROC +" for consumerId="+ consumerId +" accountId="+ accountId+" status="+ status);
        }

        Map result = null;
        try {
            result = getTransactionsByConsumer.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve consumer accounts for consumerId="+ consumerId +" accountId="+ accountId +" status="+ status, e);
        }

        List accounts = (List)result.get("ov_cust_account_info_cv");

        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerAccounts: procedure log id="+ getProcedureLogId(result) +" accounts="+ (accounts == null ? null : accounts.size()));
        }

        return accounts;
	}

	/**
	 * Adds account comment.
	 *
	 * @param accountId
	 * @param comment
	 * @return accountCommentId
	 */
	public Long addAccountComment(Long accountId, Comment comment)
			throws DAOException {
		if (accountId == null)
			throw new DAOException("Invalid accountId provided");
		if (comment == null)
			throw new DAOException("Invalid comment provided");

		SimpleJdbcCall addAccountComment = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(INSRT_CUST_ACCT_COMMENT_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_acct_id", Types.INTEGER),
						new SqlParameter("iv_cust_acct_cmnt_reas_code",
								Types.VARCHAR),
						new SqlParameter("iv_cmnt_text", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_acct_cmnt_id",
								Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_acct_id", accountId);
		parameters.addValue("iv_cust_acct_cmnt_reas_code", comment
				.getReasonCode());
		parameters.addValue("iv_cmnt_text", comment.getCommentText());

		Map<?, ?> result = addAccountComment.execute(parameters);
		Long accountCommentId = null;
		Number acId = (Number) result.get("ov_cust_acct_cmnt_id");
		if (acId != null)
			accountCommentId = new Long(acId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addAccountComment: procedure log id="
					+ getProcedureLogId(result));
		}

		return accountCommentId;
	}

	/**
	 * Adds consumer comment.
	 *
	 * @param consumerId
	 * @param comment
	 * @return consumerCommentId
	 */
	public Long addConsumerComment(Long consumerId, Comment comment)
			throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");
		if (comment == null)
			throw new DAOException("Invalid comment provided");

		SimpleJdbcCall addConsumerComment = new SimpleJdbcCall(
				getJdbcTemplate()).withProcedureName(INSRT_CUST_COMMENT_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_cust_cmnt_reas_code",
								Types.VARCHAR),
						new SqlParameter("iv_cust_cmnt_text", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_cust_cmnt_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_cust_cmnt_reas_code", comment.getReasonCode());
		parameters.addValue("iv_cust_cmnt_text", comment.getCommentText());

		Map<?, ?> result = addConsumerComment.execute(parameters);
		Long consumerCommentId = null;
		Number ccId = (Number) result.get("ov_cust_cmnt_id");
		if (ccId != null)
			consumerCommentId = new Long(ccId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addConsumerComment: procedure log id="
					+ getProcedureLogId(result));
		}

		return consumerCommentId;
	}

	/**
	 * Updates consumer status.
	 *
	 * @param consumerId
	 * @param statusCode
	 * @param subStatusCode
	 */
	public void updateConsumerStatus(Long consumerId, String statusCode,
			String subStatusCode) throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");
		if (statusCode == null)
			throw new DAOException("Invalid statusCode provided");
		if (subStatusCode == null)
			throw new DAOException("Invalid subStatusCode provided");

		SimpleJdbcCall updateConsumerStatus = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(UPDATE_CUST_STAT_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_cust_stat_code", Types.VARCHAR),
						new SqlParameter("iv_cust_sub_stat_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_cust_stat_code", statusCode);
		parameters.addValue("iv_cust_sub_stat_code", subStatusCode);

		Map<?, ?> result = updateConsumerStatus.execute(parameters);

		if (logger.isDebugEnabled()) {
			logger.debug("updateConsumerStatus: procedure log id="
					+ getProcedureLogId(result));
		}
	}

	private String determineCreditCardBin(String account) {
		if (account == null || account.length() < 6)
			return null;
		return account.substring(0, 6);
	}

	private String determineLastFourMask(String account) {
		if (account == null || account.length() < 4)
			return null;
		int leng = account.length();
		return account.substring(leng - 4);
	}

	/**
	 * Gets consumer blocked codes.
	 *
	 * @param primaryPhone
	 * @param alternatePhone
	 * @param email
	 * @return MGOAttributes
	 */
	public MGOAttributes getConsumerBlockedCodes(String primaryPhone,
			String alternatePhone, String email) throws DAOException {
		if (primaryPhone == null && alternatePhone == null && email == null)
			throw new DAOException(
					"Either primaryPhone or alternatePhone or email must be provided");

		String domain = null;
		if (email != null) {
			domain = ConsumerUtil.extractDomainFromEmail(email);
		}

		SimpleJdbcCall getConsumerBlockedCodes = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_BLOCKED_CUST_PROFILE_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_blkd_prm_ph_nbr", Types.VARCHAR),
						new SqlParameter("iv_blkd_alt_ph_nbr", Types.VARCHAR),
						new SqlParameter("iv_blkd_elec_addr_id", Types.VARCHAR),
						new SqlParameter("iv_blkd_domain_name", Types.VARCHAR),
						new SqlOutParameter("ov_prm_ph_blkd_stat_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_alt_ph_blkd_stat_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_elec_addr_blkd_stat_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_domain_blkd_stat_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_blkd_prm_ph_nbr", primaryPhone);
		parameters.addValue("iv_blkd_alt_ph_nbr", alternatePhone);
		parameters.addValue("iv_blkd_elec_addr_id", email);
		parameters.addValue("iv_blkd_domain_name", domain);

		Map<?, ?> result = getConsumerBlockedCodes.execute(parameters);
		MGOAttributes internal = new MGOAttributes();
		boolean consumerBlocked = false;
		String code = StringUtils.trimToNull((String) result
				.get("ov_prm_ph_blkd_stat_code"));
		boolean blocked = code == null || "NBK".equals(code) ? false : true;
		if (blocked)
			consumerBlocked = true;
		internal.setPrimaryPhoneBlocked(new Boolean(blocked));

		code = StringUtils.trimToNull((String) result
				.get("ov_alt_ph_blkd_stat_code"));
		blocked = code == null || "NBK".equals(code) ? false : true;
		if (blocked)
			consumerBlocked = true;
		internal.setAlternatePhoneBlocked(new Boolean(blocked));


		code = StringUtils.trimToNull((String) result
				.get("ov_elec_addr_blkd_stat_code"));
		blocked = code == null || "NBK".equals(code) ? false : true;
		if (blocked)
			consumerBlocked = true;
		internal.setEmailBlocked(new Boolean(blocked));

		code = StringUtils.trimToNull((String) result
				.get("ov_domain_blkd_stat_code"));
		blocked = code == null || "NBK".equals(code) ? false : true;
		if (blocked)
			consumerBlocked = true;
		internal.setEmailDomainBlocked(new Boolean(blocked));

		internal.setConsumerBlocked(new Boolean(consumerBlocked));

		if (logger.isDebugEnabled()) {
			logger.debug("getConsumerBlockedCodes: procedure log id="
					+ getProcedureLogId(result));
		}

		return internal;
	}

	/**
	 * Gets credit card blocked codes.
	 *
	 * @param creditCardNumber
	 * @return CreditCard
	 */
	public CreditCardReponse getCreditCardBlockedCodes(String creditCardNumber, String blockedId)
			throws DAOException {
		if (creditCardNumber == null)
			throw new DAOException("Invalid creditCardNumber provided");

		SimpleJdbcCall getCreditCardBlockedCodes = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_BLKD_CC_ACCT_AND_BIN_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_blkd_crcd_acct_id", Types.NUMERIC),
						new SqlParameter("iv_blkd_crcd_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_blkd_crcd_bin_hash_text", Types.VARCHAR),
						new SqlOutParameter("ov_crcd_account_blkd_stat_code", Types.VARCHAR),
						new SqlOutParameter("ov_crcd_bin_blkd_stat_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_blkd_crcd_acct_id", blockedId != null ? new Long(blockedId) : null);
		parameters.addValue("iv_blkd_crcd_mask_nbr", determineLastFourMask(creditCardNumber));
		parameters.addValue("iv_blkd_crcd_bin_hash_text", ConsumerUtil.hash(determineCreditCardBin(creditCardNumber)));

		Map<?, ?> result = getCreditCardBlockedCodes.execute(parameters);
		CreditCard cc = new CreditCard();
		
		String code = StringUtils.trimToNull((String) result
				.get("ov_crcd_account_blkd_stat_code"));
		CreditCardReponse ccr = new CreditCardReponse();
		ccr.setBlockedStatCode(code);
		
		boolean blocked = code == null || "NBK".equals(code) ? false : true;
		cc.setBlocked(new Boolean(blocked));

		code = StringUtils.trimToNull((String) result
				.get("ov_crcd_bin_blkd_stat_code"));
		blocked = code == null || "NBK".equals(code) ? false : true;
		cc.setBinBlocked(new Boolean(blocked));

		if (logger.isDebugEnabled()) {
			logger.debug("getCreditCardBlockedCodes: procedure log id="
					+ getProcedureLogId(result));
		}

		ccr.setCreditCard(cc);
		return ccr;
	}

	/**
	 * Gets bank account blocked codes.
	 *
	 * @param abaNumber
	 * @param bankAccountNumber
	 * @return BankAccount
	 */
	public BankAccount getBankAccountBlockedCodes(String abaNumber,
			String bankAccountNumber) throws DAOException {
		if (abaNumber == null)
			throw new DAOException("Invalid abaNumber provided");
		if (bankAccountNumber == null)
			throw new DAOException("Invalid bankAccountNumber provided");

		BankAccount ba = new BankAccount();

		//call method twice, once to check against ABA blocks, once against acct# blocks
		ba.setAbaBlocked(new Boolean(isBankAccountBlocked (abaNumber, bankAccountNumber, true)));
		ba.setBlocked(new Boolean(isBankAccountBlocked (abaNumber, bankAccountNumber, false)));

		return ba;
	}

	protected boolean isBankAccountBlocked(String abaNumber,
			String bankAccountNumber, boolean isAbaBlockCheck) throws DAOException {

		String lastFour = determineLastFourMask(bankAccountNumber);
		if (isAbaBlockCheck) {
			bankAccountNumber = ACCT_NBR_WILDCARDED;
			lastFour = null;
		}

		SimpleJdbcCall getBankAccountBlockedCodes = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_BLOCKED_BANK_ACCT_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_bank_aba_nbr", Types.VARCHAR),
						new SqlParameter("iv_blkd_acct_hash_text",
								Types.VARCHAR),
						new SqlParameter("iv_blkd_acct_mask_nbr", Types.VARCHAR),
						new SqlOutParameter("ov_bank_account_blkd_stat_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_bank_aba_nbr", abaNumber);
		parameters.addValue("iv_blkd_acct_hash_text", ConsumerUtil
				.hash(bankAccountNumber));
		parameters.addValue("iv_blkd_acct_mask_nbr", lastFour);

		Map<?, ?> result = getBankAccountBlockedCodes.execute(parameters);

		String code = StringUtils.trimToNull((String) result
				.get("ov_bank_account_blkd_stat_code"));
		boolean blocked = code == null || "NBK".equals(code) ? false : true;

		if (logger.isDebugEnabled()) {
			logger.debug("getBankAccountBlockedCodes: procedure log id="
					+ getProcedureLogId(result));
		}

		return blocked;
	}

	/**
	 * Updates Consumer Security Questions Collection Date.
	 * @param consumerId
	 * @param securityQuestionsCollectionDate
	 * @throws DAOException
	 */
    public void updateConsumerSecurityQuestionsCollectionDate(Long consumerId, Date securityQuestionsCollectionDate)
            throws DAOException {

        SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_CONSUMER_SECURITY_QUESTIONS_COLLECTION_DATE_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlParameter("iv_adptv_authn_prfl_cmplt_date", Types.TIMESTAMP),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        parameters.addValue("iv_cust_id", consumerId);
        parameters.addValue("iv_adptv_authn_prfl_cmplt_date", securityQuestionsCollectionDate);
        parameters.addValue("iv_call_type_code", "MGO");

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerSecurityQuestionsCollectionDate: calling procedure="+ UPDATE_CONSUMER_SECURITY_QUESTIONS_COLLECTION_DATE_PROC +" with consumerId="+ consumerId +" securityQuestionsCollectionDate="+ securityQuestionsCollectionDate);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update securityQuestionsCollectionDate for consumerId="+ consumerId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerSecurityQuestionsCollectionDate: procedure log id="+ getProcedureLogId(result));
        }

    }

	/**
	 * Adds consumer password hash.
	 *
	 * @param consumerId
	 * @param passwordHash
	 */
	public void addConsumerPasswordHash(Long consumerId, String passwordHash)
			throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");
		if (passwordHash == null)
			throw new DAOException("Invalid passwordHash provided");

		SimpleJdbcCall addConsumerPasswordHash = new SimpleJdbcCall(
				getJdbcTemplate()).withProcedureName(IU_CUST_PASSWORD_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_pswd_text", Types.VARCHAR),
						new SqlOutParameter("ov_row_update_count",
								Types.NUMERIC),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_pswd_text", passwordHash);

		Map<?, ?> result = addConsumerPasswordHash.execute(parameters);
		Number rowCount = (Number) result.get("ov_row_update_count");
		if (rowCount == null || rowCount.intValue() == 0) {
			logger.warn("Consumer passwordHash not added");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("addConsumerPasswordHash: procedure log id="
					+ getProcedureLogId(result));
		}
	}

	/**
	 * Updates consumer loyalty info.
	 *
	 * @param consumerId
	 * @param autoEnroll
	 * @param memberId
	 */
	public void updateConsumerLoyalty(Long consumerId, Boolean autoEnroll,
			String memberId) throws DAOException {
		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		SimpleJdbcCall updateConsumerLoyalty = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(UPDATE_CUST_REWARDS_INFO_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_lylty_pgm_mbshp_id", Types.VARCHAR),
						new SqlParameter("iv_cust_auto_enrl_flag",
								Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_lylty_pgm_mbshp_id", memberId);
		parameters.addValue("iv_cust_auto_enrl_flag", DAOUtils
				.toString(autoEnroll));
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);

		Map<?, ?> result = updateConsumerLoyalty.execute(parameters);

		if (logger.isDebugEnabled()) {
			logger.debug("updateConsumerLoyalty: procedure log id="
					+ getProcedureLogId(result));
		}
	}

	/**
	 * Determines if IP address is blocked.
	 *
	 * @param ipAddress
	 * @param bankAccountNumber
	 * @return true if blocked and false if not blocked
	 */
	@SuppressWarnings("unchecked")
	public boolean isIpAddressBlocked(String ipAddress) throws DAOException {
		if (ipAddress == null)
			throw new DAOException("Invalid ipAddress provided");

		SimpleJdbcCall isIpAddressBlocked = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_BLOCKED_IP_ADDR_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_begin_ip_addr_range",
								Types.VARCHAR),
						new SqlParameter("iv_end_ip_addr_range", Types.VARCHAR),
						new SqlParameter("iv_blkd_reas_code", Types.VARCHAR),
						new SqlParameter("iv_blkd_stat_code", Types.VARCHAR),
						new SqlParameter("iv_fraud_risk_lvl_code",
								Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_blocked_ip_addr_cv",
								OracleTypes.CURSOR,
								new BlockedIPAddressRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_begin_ip_addr_range", ipAddress);
		parameters.addValue("iv_end_ip_addr_range", null);
		parameters.addValue("iv_blkd_reas_code", null);
		parameters.addValue("iv_blkd_stat_code", null);
		parameters.addValue("iv_fraud_risk_lvl_code", null);

		Map<?, ?> result = isIpAddressBlocked.execute(parameters);
		List<BlockedIPAddress> blockedIPAddressList = (List<BlockedIPAddress>) result
				.get("ov_blocked_ip_addr_cv");
		boolean blocked = false;
		if (blockedIPAddressList != null && !blockedIPAddressList.isEmpty()) {
			for (BlockedIPAddress bipa : blockedIPAddressList) {
				if ("BLK".equals(bipa.getStatusCode())) {
					blocked = true;
					break;
				}
			}
		}

		if (logger.isDebugEnabled()) {
			logger.debug("isIpAddressBlocked: procedure log id="
					+ getProcedureLogId(result));
		}

		return blocked;
	}

	/**
	 * Adds logon try.
	 *
	 * @param loginId
	 * @param ipAddress
	 * @param webServerName
	 * @param webServerIpAddress
	 * @return loginTryId
	 */
	public Long addLoginTry(String loginId, String ipAddress,
			String webServerName, String webServerIpAddress,
			Boolean logonSuccessful, Boolean isMgoUser, Boolean isMmgUser, String sourceSite) throws DAOException {
		if (loginId == null)
			throw new DAOException("Invalid loginId provided");
		if (ipAddress == null)
			throw new DAOException("Invalid ipAddress provided");
		if (webServerName == null)
			throw new DAOException("Invalid webServerName provided");
		if (webServerIpAddress == null)
			throw new DAOException("Invalid webServerIpAddress provided");
		if (logonSuccessful == null)
			throw new DAOException("Invalid logonSuccessful provided");

		SimpleJdbcCall addLoginTry = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(INSERT_LOGON_TRY)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_logon_id", Types.VARCHAR),
						new SqlParameter("iv_logon_ip_addr_id", Types.VARCHAR),
						new SqlParameter("iv_invl_login_try_cnt", Types.INTEGER),
						new SqlParameter("iv_web_srvr_name", Types.VARCHAR),
						new SqlParameter("iv_web_srvr_ip_addr_id", Types.VARCHAR),
                        new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
						new SqlOutParameter("ov_logon_try_seq_id", Types.NUMERIC),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_logon_id", loginId);
		parameters.addValue("iv_logon_ip_addr_id", ipAddress);
        parameters.addValue("iv_src_web_site_bsns_cd", sourceSite);

		int logonType = LOGON_NONMGO_NONMMG; //non MGO/MMG user
		if (logonSuccessful.booleanValue()) {
			if (isMgoUser.booleanValue()) {
				logonType = LOGON_MGO_GOOD; //good logon MGO user (could also have MMG access)
			} else if (isMmgUser.booleanValue()) {
				logonType = LOGON_MMG_GOOD; //good logon MMG user
			}
		} else {
			if (isMgoUser.booleanValue()) {
				logonType = LOGON_MGO_BAD; //failed logon MGO user (could also have MMG access)
		    }
			else if (isMmgUser.booleanValue()) {
				logonType = LOGON_MMG_BAD; //failed logon MMG user
			}
		}

		parameters.addValue("iv_invl_login_try_cnt", logonType);
		parameters.addValue("iv_web_srvr_name", webServerName);
		parameters.addValue("iv_web_srvr_ip_addr_id", webServerIpAddress);

		Map<?, ?> result = addLoginTry.execute(parameters);
		Long loginTryId = null;
		Number ltId = (Number) result.get("ov_logon_try_seq_id");
		if (ltId != null)
			loginTryId = new Long(ltId.longValue());

		if (logger.isDebugEnabled()) {
			logger.debug("addLoginTry: procedure log id="
					+ getProcedureLogId(result));
		}

		return loginTryId;
	}

	/**
	 * Adds logon_failure row entries for each block flag set to true.
	 *
	 * @param consumer
	 * @param logonTryId
	 * @return void
	 */
	public void addBlockedAttempts(Consumer consumer, Number logonTryId) throws DAOException {
		Access access = consumer.getAccess();
		MGOAttributes internal = consumer.getInternal();

		//acctBlockMap will be populated with any acct type blocks for use in calling stored proc
		HashMap<String, Boolean> acctBlockMap = new HashMap<String, Boolean>();
        setBankCardBlocking(consumer, acctBlockMap);

		SimpleJdbcCall insertBlockedLogon = new SimpleJdbcCall(getJdbcTemplate())
		.withProcedureName(INSERT_BLOCKED_LOGON).withoutProcedureColumnMetaDataAccess()
		.declareParameters(
				new SqlParameter("iv_user_id", Types.VARCHAR),
				new SqlParameter("iv_call_type_code", Types.VARCHAR),
				new SqlParameter("iv_logon_try_seq_id", Types.NUMERIC),
				new SqlParameter("iv_profile_disabled", Types.VARCHAR),
				new SqlParameter("iv_blocked_ip", Types.VARCHAR),
				new SqlParameter("iv_blocked_ssn", Types.VARCHAR),
				new SqlParameter("iv_blocked_bank", Types.VARCHAR),
				new SqlParameter("iv_blocked_cc", Types.VARCHAR),
				new SqlParameter("iv_blocked_aba", Types.VARCHAR),
				new SqlParameter("iv_blocked_cc_bin", Types.VARCHAR),
				new SqlParameter("iv_blocked_prm_phone", Types.VARCHAR),
				new SqlParameter("iv_blocked_alt_phone", Types.VARCHAR),
				new SqlParameter("iv_blocked_email", Types.VARCHAR),
				new SqlParameter("iv_blocked_email_domain", Types.VARCHAR),
				new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_logon_try_seq_id", logonTryId);
		parameters.addValue("iv_profile_disabled",
				 translateBlockedValue(internal.getConsumerBlocked()));
		parameters.addValue("iv_blocked_ip",
	             translateBlockedValue(access.getIpAddressBlocked()));
		//no longer do SSN blocking
		parameters.addValue("iv_blocked_ssn", "0");
		parameters.addValue("iv_blocked_bank",
	             translateBlockedValue(acctBlockMap.get(BANK_ACCT_BLOCKED)));
		parameters.addValue("iv_blocked_cc",
				 translateBlockedValue(acctBlockMap.get(CC_BLOCKED)));
		parameters.addValue("iv_blocked_aba",
				 translateBlockedValue(acctBlockMap.get(BANK_ABA_BLOCKED)));
		parameters.addValue("iv_blocked_cc_bin",
				 translateBlockedValue(acctBlockMap.get(CC_BIN_BLOCKED)));
		parameters.addValue("iv_blocked_prm_phone",
	             translateBlockedValue(internal.getPrimaryPhoneBlocked()));
		parameters.addValue("iv_blocked_alt_phone",
	             translateBlockedValue(internal.getAlternatePhoneBlocked()));
		parameters.addValue("iv_blocked_email",
	             translateBlockedValue(internal.getEmailBlocked()));
		parameters.addValue("iv_blocked_email_domain",
	             translateBlockedValue(internal.getEmailDomainBlocked()));

		Map<?, ?> result = insertBlockedLogon.execute(parameters);

		if (logger.isDebugEnabled()) {
			logger.debug("addBlockedAttempts: procedure log id="
					+ getProcedureLogId(result));
		}

	}

	/**
	 * Adds logon_failure row entries for each block flag set to true.
	 *
	 * @param ipAddressBlocked
	 * @param logonTryId
	 * @return void
	 */
	public void addBlockedAttemptsNoConsumer(Boolean ipAddressBlocked, Number logonTryId) throws DAOException {

		SimpleJdbcCall insertBlockedLogon = new SimpleJdbcCall(getJdbcTemplate())
		.withProcedureName(INSERT_BLOCKED_LOGON).withoutProcedureColumnMetaDataAccess()
		.declareParameters(
				new SqlParameter("iv_user_id", Types.VARCHAR),
				new SqlParameter("iv_call_type_code", Types.VARCHAR),
				new SqlParameter("iv_logon_try_seq_id", Types.NUMERIC),
				new SqlParameter("iv_profile_disabled", Types.VARCHAR),
				new SqlParameter("iv_blocked_ip", Types.VARCHAR),
				new SqlParameter("iv_blocked_ssn", Types.VARCHAR),
				new SqlParameter("iv_blocked_bank", Types.VARCHAR),
				new SqlParameter("iv_blocked_cc", Types.VARCHAR),
				new SqlParameter("iv_blocked_aba", Types.VARCHAR),
				new SqlParameter("iv_blocked_cc_bin", Types.VARCHAR),
				new SqlParameter("iv_blocked_prm_phone", Types.VARCHAR),
				new SqlParameter("iv_blocked_alt_phone", Types.VARCHAR),
				new SqlParameter("iv_blocked_email", Types.VARCHAR),
				new SqlParameter("iv_blocked_email_domain", Types.VARCHAR),
				new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_logon_try_seq_id", logonTryId);
		parameters.addValue("iv_profile_disabled", "0");
		parameters.addValue("iv_blocked_ip", translateBlockedValue(ipAddressBlocked));
		parameters.addValue("iv_blocked_ssn", "0");
		parameters.addValue("iv_blocked_bank", "0");
		parameters.addValue("iv_blocked_cc", "0");
		parameters.addValue("iv_blocked_aba",  "0");
		parameters.addValue("iv_blocked_cc_bin",  "0");
		parameters.addValue("iv_blocked_prm_phone",  "0");
		parameters.addValue("iv_blocked_alt_phone",  "0");
		parameters.addValue("iv_blocked_email",  "0");
		parameters.addValue("iv_blocked_email_domain",  "0");

		Map<?, ?> result = insertBlockedLogon.execute(parameters);

		if (logger.isDebugEnabled()) {
			logger.debug("addBlockedAttemptsNoConsumer: procedure log id="
					+ getProcedureLogId(result));
		}

	}

	private String translateBlockedValue(Boolean boolVal) {
		if (boolVal == null || !boolVal.booleanValue()) {
			return "0";
		} else {
			return "1";
		}
	}

	protected void setBankCardBlocking(Consumer consumer, HashMap<String,Boolean> map) {
		if (consumer == null) {
			return;
		}
		ConsumerFIAccount[] accounts = consumer.getAccounts();
		if (accounts == null || accounts.length < 1) {
			return;
		}

		for (ConsumerFIAccount account : accounts) {
			CreditCard cc = account.getCreditCard();
			BankAccount ba = account.getBankAccount();

			if (cc != null) {
				if (cc.getBinBlocked()) {
					map.put(CC_BIN_BLOCKED, new Boolean(true));
				}
				if (cc.getBlocked()) {
					map.put(CC_BLOCKED, new Boolean(true));
				}
			} else if (ba != null) {
				if (ba.getAbaBlocked()) {
					map.put(BANK_ABA_BLOCKED, new Boolean(true));
				}
				if (ba.getBlocked()) {
					map.put(BANK_ACCT_BLOCKED, new Boolean(true));
				}
			}
		}

	}

	/**
	 * Returns list of consumers. Only consumerId and loginId are populated for each consumer.
	 *
	 * @param birthDate
	 * @param ssnMask
	 * @param lastName
	 * @param addressLine1StartsWith
	 * @return list of matching consumers
	 */
	@SuppressWarnings("unchecked")
	public List<Consumer> getActiveConsumers(Date birthDate, String ssnMask,
			String lastName, String addressLine1StartsWith) throws DAOException {
		if (birthDate == null)
			throw new DAOException("Invalid birthDate provided");
		if (ssnMask == null)
			throw new DAOException("Invalid ssnMask provided");
		if (lastName == null)
			throw new DAOException("Invalid lastName provided");
		if (addressLine1StartsWith == null)
			throw new DAOException("Invalid addressLine1StartsWith provided");

		SimpleJdbcCall getActiveConsumers = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_ACTIVE_CUST_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_cust_brth_date", Types.DATE),
						new SqlParameter("iv_cust_ssn_mask_nbr", Types.VARCHAR),
						new SqlParameter("iv_cust_last_name", Types.VARCHAR),
						new SqlParameter("iv_addr_line1_text", Types.VARCHAR),
						new SqlParameter("iv_addr_postal_code", Types.VARCHAR),//vl58 - S14, new parameters for consumer duplicate verification (only Germany)
						new SqlParameter("iv_addr_bldg_name", Types.VARCHAR),//vl58 - S14, new parameters for consumer duplicate verification (only Germany)
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_customer_cv",
								OracleTypes.CURSOR,
								new ConsumerSearchRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_cust_brth_date", birthDate);
		parameters.addValue("iv_cust_ssn_mask_nbr", ssnMask);
		parameters.addValue("iv_cust_last_name", lastName);
		parameters.addValue("iv_addr_line1_text", addressLine1StartsWith);
		parameters.addValue("iv_addr_postal_code", null);//vl58 - S14, new parameters for consumer duplicate verification (only Germany)
		parameters.addValue("iv_addr_bldg_name", null);//vl58 - S14, new parameters for consumer duplicate verification (only Germany)

		Map<?, ?> result = getActiveConsumers.execute(parameters);
		List<Consumer> consumers = (List<Consumer>) result
				.get("ov_customer_cv");

		if (logger.isDebugEnabled()) {
			logger.debug("getActiveConsumers: procedure log id="
					+ getProcedureLogId(result));
		}

		return consumers;
	}

    public void updateAccountStatus(Long accountId, String status, String subStatus) throws DAOException {
        SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_ACCOUNT_STATUS_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_acct_id", Types.INTEGER),
                        new SqlParameter("iv_acct_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_acct_sub_stat_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_call_type_code", "MGO");
        parameters.addValue("iv_cust_acct_id", accountId);
        parameters.addValue("iv_acct_stat_code", status);
        parameters.addValue("iv_acct_sub_stat_code", subStatus);

        if (logger.isDebugEnabled()) {
            logger.debug("updateAccountStatus: calling procedure="+ UPDATE_ACCOUNT_STATUS_PROC +" with accountId="+ accountId +" status="+ status +" subStatus="+ subStatus);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update account status for accountId="+ accountId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateAccountStatus: procedure log id="+ getProcedureLogId(result));
        }

    }
    
    
    public void updateCreditCardType(Long accountId, String newCardType) throws DAOException {
        
    	SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_CREDITCARD_TYPE)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                    	new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_acct_id", Types.INTEGER),
                        new SqlParameter("iv_crcd_type_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        parameters.addValue("iv_call_type_code", "MGO");
        parameters.addValue("iv_cust_acct_id", accountId);
        parameters.addValue("iv_crcd_type_code", newCardType);

        if (logger.isDebugEnabled()) {
            logger.debug("updateCreditCardType: calling procedure="+ UPDATE_CREDITCARD_TYPE +" with accountId="+ accountId +" newCardType="+ newCardType);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update CreditCard Type for accountId="+ accountId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateCreditCardType: procedure log id="+ getProcedureLogId(result));
        }

    }

    /**
     * Please use <pre>
     * updateConsumerPromoEmailFlag(Long consumerId, boolean promoEmailFlag, String partnerSiteId)
     * </pre> instead.
     * @param consumerId
     * @param promoEmailFlag
     * @throws DAOException
     */
    @Deprecated
    public void updateConsumerPromoEmailFlag(Long consumerId, boolean promoEmailFlag) throws DAOException {
    	updateConsumerPromoEmailFlag(consumerId, promoEmailFlag, DEFAULT_PARTNER_SITE_ID);
    }

    public void updateConsumerPromoEmailFlag(Long consumerId, boolean promoEmailFlag, String partnerSiteId) throws DAOException {
        SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_CUST_PROMO_EMAIL_CD_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlParameter("iv_promo_email_code", Types.VARCHAR),
                        new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        parameters.addValue("iv_call_type_code", "MGO");
        parameters.addValue("iv_cust_id", consumerId);
        parameters.addValue("iv_promo_email_code", DAOUtils.toString(promoEmailFlag));
        parameters.addValue("iv_src_web_site_bsns_cd", partnerSiteId);
        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerPromoEmailFlag: calling procedure="+ UPDATE_CUST_PROMO_EMAIL_CD_PROC +" with consumerId="+ consumerId +" promoEmailFlag="+ promoEmailFlag);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update Promo Email Flag for consumerId="+ consumerId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerPromoEmailFlag: procedure log id="+ getProcedureLogId(result));
        }
    }

    public void updateConsumerAddress(Long consumerId, ConsumerAddress address) throws DAOException {
        SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_ADDRESS_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_addr_id", Types.INTEGER),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlParameter("iv_addr_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_addr_line1_text", Types.VARCHAR),
                        new SqlParameter("iv_addr_line2_text", Types.VARCHAR),
                        new SqlParameter("iv_addr_line3_text", Types.VARCHAR),
                        new SqlParameter("iv_addr_city_name", Types.VARCHAR),
                        new SqlParameter("iv_addr_state_name", Types.VARCHAR),
                        new SqlParameter("iv_addr_postal_code", Types.VARCHAR),
                        new SqlParameter("iv_addr_cntry_id", Types.VARCHAR),
                        new SqlParameter("iv_addr_cnty_id", Types.VARCHAR),
                        new SqlParameter("iv_addr_bldg_name", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_addr_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        parameters.addValue("iv_call_type_code", "MGO");

        parameters.addValue("iv_addr_id", address.getAddressId());
        parameters.addValue("iv_cust_id", consumerId);
        parameters.addValue("iv_addr_stat_code", "ACT");
        parameters.addValue("iv_addr_line1_text", address.getLine1());
        parameters.addValue("iv_addr_line2_text", address.getLine2());
        parameters.addValue("iv_addr_line3_text", address.getLine3());
        parameters.addValue("iv_addr_city_name", address.getCity());
        parameters.addValue("iv_addr_state_name", address.getState());
        parameters.addValue("iv_addr_postal_code", address.getZipCode());
        parameters.addValue("iv_addr_cntry_id", address.getCountry());
        parameters.addValue("iv_addr_cnty_id", address.getCounty());
        parameters.addValue("iv_addr_bldg_name", address.getBuildingName());

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerAddress: calling procedure="+ UPDATE_ADDRESS_PROC +" with consumerId="+ consumerId +" address="+ address);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update address for consumerId="+ consumerId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerAddress: procedure log id="+ getProcedureLogId(result) +" consumerId="+ consumerId +" new address id="+ result.get("ov_addr_id"));
        }
    }

    /**
     * Updates consumer phone.
     *
     * @param consumerId
     * @param phoneType
     * @param phoneNumber
     */
    protected boolean updateConsumerPhone(Long consumerId, String phoneNumber, String phoneType, String primaryPhoneFlag) throws DAOException {
        SimpleJdbcCall update =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_PHONE_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_ph_nbr", Types.VARCHAR),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlParameter("iv_prm_phn_flag", Types.VARCHAR),
                        new SqlParameter("iv_phn_type_bsns_code", Types.VARCHAR),
                        new SqlOutParameter("ov_cust_phone_update_status", Types.NUMERIC),
                        new SqlOutParameter("ov_cust_phone_affected_count", Types.NUMERIC),
                        new SqlOutParameter("ov_customer_update_count", Types.NUMERIC),
                        new SqlOutParameter("ov_blkd_stat_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("iv_user_id", DB_CALLER_ID);
        parameters.addValue("iv_call_type_code", "MGO");
        parameters.addValue("iv_cust_ph_nbr", phoneNumber);
        parameters.addValue("iv_cust_id", consumerId);
        parameters.addValue("iv_prm_phn_flag", primaryPhoneFlag);
        parameters.addValue("iv_phn_type_bsns_code", phoneType);

        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerPhone: calling procedure="+ UPDATE_PHONE_PROC +" with consumerId="+ consumerId);
        }

        Map<String, Object> result = null;
        try {
            result = update.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update phone for consumerId="+ consumerId, e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("updateConsumerPhone: procedure log id="+ getProcedureLogId(result));
        }
        
        String blockedValue =  (String) result.get("ov_customer_blocked");
        return blockedValue != null && blockedValue.startsWith(CUST_BLOCKED);
    }

    public boolean updatePrimaryPhone(Long consumerId, String phoneNumber, String phoneType) throws DAOException {
        return updateConsumerPhone(consumerId, phoneNumber, phoneType, CUST_PRIMARY_PHONE_TYPE);
    }

    public boolean updateAlternatePhone(Long consumerId, String phoneNumber, String phoneType) throws DAOException {
        return updateConsumerPhone(consumerId, phoneNumber, phoneType, CUST_ALTERNATE_PHONE_TYPE);
    }

	/**
	 * Returns list of consumerIds.
	 *
	 * @param startDate
	 * @param endDate
     * @param eventType (optional)
	 * @return list of consumerIds having incomplete profiles created in the date range.
	 */
	@SuppressWarnings("unchecked")
	public List<Long> getIncompleteProfiles(Calendar startDate, Calendar endDate, Number eventType, Number maxResults) throws DAOException {
		if (startDate == null)
			throw new DAOException("Invalid startDate provided");
		if (endDate == null)
			throw new DAOException("Invalid endDate provided");

		SimpleJdbcCall getIncompleteProfiles = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_INCOMPLETE_PROFILES_PROC)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_acty_log_code", Types.INTEGER),
						new SqlParameter("iv_beg_date", Types.TIMESTAMP),
						new SqlParameter("iv_end_date", Types.TIMESTAMP),
						new SqlParameter("iv_profile_count", Types.NUMERIC),
						new SqlOutParameter("ov_incmpl_prfl_cust_id_cv",
								OracleTypes.CURSOR,
								new IncompleteProfileRowMapper()),
					    new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_acty_log_code", eventType);
		parameters.addValue("iv_beg_date", startDate);
		parameters.addValue("iv_end_date", endDate);
		parameters.addValue("iv_profile_count", maxResults==null?null:maxResults.intValue());

		List<Long> consumerIds = null;
	    try {
			Map<?, ?> result = getIncompleteProfiles.execute(parameters);
			consumerIds = (List<Long>) result.get("ov_incmpl_prfl_cust_id_cv");

			if (logger.isDebugEnabled()) {
				logger.debug("getIncompleteProfiles: procedure log id="
						+ getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException("Failed to get Incomplete Profiles", e);
		}


		return consumerIds;
	}

	/**
	 * Gets profile events for the given consumerId.
	 *
	 * @param consumerId
	 * @param event (optional)
	 * @param startDate (optional)
	 * @param endDate (optional)
	 * @return List<ProfileEvent>
	 */
	public List<ProfileEvent> getProfileEvents(Number consumerId, Calendar startDate, Calendar endDate, String event)
			throws DAOException {

		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");

		SimpleJdbcCall getProfileEvents = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_PROFILE_EVENTS_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_acty_bsns_cd", Types.VARCHAR),
						new SqlParameter("iv_beg_date", Types.TIMESTAMP),
						new SqlParameter("iv_end_date", Types.TIMESTAMP),
						new SqlOutParameter("ov_cust_acty_cv",
								OracleTypes.CURSOR,
								new GetProfileEventsRowMapper()),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_cust_id", consumerId);

        parameters.addValue("iv_acty_bsns_cd", event);
		parameters.addValue("iv_beg_date", startDate==null?null:startDate.getTime());
		parameters.addValue("iv_end_date", endDate==null?null:endDate.getTime());

        List<ProfileEvent> events = null;
	    try {
			Map<String, Object> result = getProfileEvents.execute(parameters);

			events = (List<ProfileEvent>) result.get("ov_cust_acty_cv");
			if (logger.isDebugEnabled()) {
				logger.debug("getProfileEvents: procedure log id="
						+ getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException("Failed to get profile events for consumerId = "+ consumerId +" event="+ event, e);
		}

		return events;
	}

	/**
	 * Adds profile event.
	 *
	 * @param consumerId
	 * @param event
	 * @return void
	 */
	public void saveProfileEvent(Number consumerId, ProfileEvent event)
			throws DAOException {

		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");
		if (event == null)
			throw new DAOException("Invalid event provided");

		SimpleJdbcCall saveProfileEvent = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(SAVE_PROFILE_EVENT_PROC)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_acty_date", Types.TIMESTAMP),
						new SqlParameter("iv_acty_bsns_code", Types.VARCHAR),
						new SqlOutParameter("ov_cust_acty_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_acty_date", event.getEventDateTime().getTime());
        parameters.addValue("iv_acty_bsns_code", event.getEvent());

	    try {
			Map<?, ?> result = saveProfileEvent.execute(parameters);
			if (logger.isDebugEnabled()) {
				logger.debug("saveProfileEvent: consumerId = "+ consumerId +" event="+ event +" procedure log id="
						+ getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException("Failed to save Profile Event for consumerId = "+ consumerId +" event="+ event, e);
		}
	}

	
	/**
	 * Check if the phone number is included in the blocked phone number's list
	 * @param phoneNumber The phone number to test
	 * @return isBlocked Return true if the phone number is included in the list, false the otherwise 	
	 */
	@SuppressWarnings("unchecked")
	public boolean isPhoneNumberBlocked(String phoneNumber)throws DAOException {
		if (phoneNumber == null)
			throw new DAOException("Invalid phoneNumber provided");

		SimpleJdbcCall getBlockedPhone = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(GET_BLOCKED_PHONE_CV)
				.withoutProcedureColumnMetaDataAccess().declareParameters(
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_blkd_ph_nbr", Types.VARCHAR),
						new SqlParameter("iv_blkd_reas_code", Types.VARCHAR),
						new SqlParameter("iv_blkd_stat_code", Types.VARCHAR),
						new SqlParameter("iv_fraud_risk_lvl_code", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
						new SqlOutParameter("ov_blocked_phone_cv",
								OracleTypes.CURSOR, 
								new BlockedPhoneNumberRowMapper()));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_blkd_ph_nbr", phoneNumber);
		parameters.addValue("iv_blkd_reas_code", "");
		parameters.addValue("iv_blkd_stat_code", "");
		parameters.addValue("iv_fraud_risk_lvl_code", "");

        List<Map<String,String>> blockedPhonesList = null;
        boolean isBlocked = false;
	    try {
			Map<String, Object> result = getBlockedPhone.execute(parameters);

			blockedPhonesList = (List<Map<String,String>>) result.get("ov_blocked_phone_cv");
	
			if(blockedPhonesList != null && !blockedPhonesList.isEmpty()){
				for(Map<String,String> blockedPhonesMap : blockedPhonesList){
					if(blockedPhonesMap != null && !blockedPhonesMap.isEmpty()){
						Set<Entry<String,String>> blockedPhonesSet = blockedPhonesMap.entrySet();
						for(Entry<String,String> blkdPhone : blockedPhonesSet){
							if(phoneNumber.equals(blkdPhone.getKey()) && !"NBK".equalsIgnoreCase(blkdPhone.getValue().trim())){
								isBlocked = true;
								break;
							}
						}
					}
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug("isPhoneNumberBlocked: procedure log id=" + getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException("Failed to if the phoneNumber = "+ phoneNumber +" is blocked", e);
		}

		return isBlocked;
	}
	
	/**
	 * @author vx15
	 * @return Long
	 * @throws DAOException
	 */
	public BigDecimal getCCNextSeqValue() throws DAOException {
		
		SimpleJdbcCall nextSeqValueCall = new SimpleJdbcCall(
				getJdbcTemplate())
				.withProcedureName(GET_CC_NEXT_SEQ_VAL)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(new SqlOutParameter("ov_nbr", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();

		BigDecimal nextSeqValue = null;
	    try {
			Map<?, ?> result = nextSeqValueCall.execute(parameters);
			nextSeqValue = (BigDecimal) result.get("ov_nbr");

			if (logger.isDebugEnabled()) {
				logger.debug("getCCNextSeqValue: procedure log id=" + getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException("Failed to get CC Next SeqValue", e);
		}
		return nextSeqValue;
	}

	/**
	 * 
	 * @author vx15
	 * @param consumerId
	 * @param fullName
	 * @throws DAOException
	 */
	public void updateName(Number consumerId, PersonalInfo personalInfo) throws DAOException {

		if (consumerId == null)
			throw new DAOException("Invalid consumerId provided");
		if (personalInfo == null)
			throw new DAOException("Invalid personalInfo provided");

		SimpleJdbcCall updateNameCall = new SimpleJdbcCall(getJdbcTemplate())
				.withProcedureName(UPDATE_CUST_NAME)
				.withoutProcedureColumnMetaDataAccess()
				.declareParameters(
						new SqlParameter("iv_call_type_code", Types.VARCHAR),
						new SqlParameter("iv_user_id", Types.VARCHAR),
						new SqlParameter("iv_cust_id", Types.INTEGER),
						new SqlParameter("iv_cust_last_name", Types.VARCHAR),
						new SqlParameter("iv_cust_mtrnl_name", Types.VARCHAR),
						new SqlParameter("iv_cust_frst_name", Types.VARCHAR),
						new SqlParameter("iv_cust_mid_name", Types.VARCHAR),
						new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC));

		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("iv_call_type_code", DB_CALL_TYPE_CODE);
		parameters.addValue("iv_user_id", DB_CALLER_ID);
		parameters.addValue("iv_cust_id", consumerId);
		parameters.addValue("iv_cust_last_name", personalInfo.getLastName());
		parameters.addValue("iv_cust_mtrnl_name", personalInfo.getSecondLastName());
		parameters.addValue("iv_cust_frst_name", personalInfo.getFirstName());
		parameters.addValue("iv_cust_mid_name", personalInfo.getMiddleName());
		

		try {
			Map<?, ?> result = updateNameCall.execute(parameters);
			if (logger.isDebugEnabled()) {
				logger.debug("saveProfileEvent: consumerId = " + consumerId
						+ " personalInfo=" + personalInfo + " procedure log id="
						+ getProcedureLogId(result));
			}
		} catch (Exception e) {
			throw new DAOException(
					"Failed to save Profile Event for consumerId = "
							+ consumerId + " personalInfo=" + personalInfo, e);
		}
	}
	
}
