/*
 * Created on Aug 28, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer.BankAccount;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.CreditCard;
import com.moneygram.mgo.service.consumer.CreditCardReponse;
import com.moneygram.mgo.service.consumer.GetConsumerBlockedStatusRequest;
import com.moneygram.mgo.service.consumer.GetConsumerBlockedStatusResponse;
import com.moneygram.mgo.service.consumer.MGOAttributes;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer.util.ConsumerUtil;

public class GetConsumerBlockedStatusCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetConsumerBlockedStatusCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetConsumerBlockedStatusRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetConsumerBlockedStatusRequest profileRequest = (GetConsumerBlockedStatusRequest) request;
		Consumer consumer = profileRequest.getConsumer();
		boolean consumerBlocked = false;
		
		if (logger.isDebugEnabled()) {
			logger.debug("process: get consumer blocked status=" + consumer);
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		if (consumer == null) {
			throw new DataFormatException("Invalid input consumer");
		}

		boolean checkContactInfoBlocked = false;
		if (consumer.getContact() != null)
			checkContactInfoBlocked = true;

		boolean checkIpAddressBlocked = false;
		if (consumer.getAccess() != null
				&& consumer.getAccess().getIpAddress() != null
				&& consumer.getAccess().getIpAddress().length() > 0)
			checkIpAddressBlocked = true;

		if (!checkIpAddressBlocked && !checkContactInfoBlocked) {
			throw new DataFormatException("Invalid input consumer info");
		}

		if (checkContactInfoBlocked) {
			MGOAttributes internal = null;
			try {
				internal = dao.getConsumerBlockedCodes(consumer.getContact()
						.getPrimaryPhone(), consumer.getContact()
						.getAlternatePhone(), consumer.getContact().getEmail());
			} catch (Exception e) {
				logger.warn("process: failed to get consumer blocked codes", e);
				throw new CommandException(
						"Failed to get consumer blocked codes", e);
			}
			if (consumer.getInternal() == null) {
				consumer.setInternal(internal);
			} else {
				consumer.getInternal().setConsumerBlocked(
						internal.getConsumerBlocked());
				consumer.getInternal().setPrimaryPhoneBlocked(
						internal.getPrimaryPhoneBlocked());
				consumer.getInternal().setAlternatePhoneBlocked(
						internal.getAlternatePhoneBlocked());
				consumer.getInternal().setEmailBlocked(
						internal.getEmailBlocked());
				consumer.getInternal().setEmailDomainBlocked(
						internal.getEmailDomainBlocked());
				consumerBlocked = 
					consumerBlocked ||
					internal.getConsumerBlocked().booleanValue() ||
					internal.getPrimaryPhoneBlocked().booleanValue() ||
					internal.getAlternatePhoneBlocked().booleanValue() ||
					internal.getEmailBlocked().booleanValue() ||
					internal.getEmailDomainBlocked().booleanValue();
			}
		}

		if (checkIpAddressBlocked) {
			try {
				boolean ipAddressBlocked = dao.isIpAddressBlocked(consumer
						.getAccess().getIpAddress());
				consumer.getAccess().setIpAddressBlocked(
						new Boolean(ipAddressBlocked));
				consumerBlocked = consumerBlocked || ipAddressBlocked;
			} catch (Exception e) {
				logger.warn("process: failed to see if IP address blocked", e);
				throw new CommandException(
						"Failed to see if IP address blocked", e);
			}
		}

		if (consumer.getAccounts() != null && consumer.getAccounts().length > 0) {
			BankAccount ba;
			CreditCard cc;
			for (int i = 0; i < consumer.getAccounts().length; i++) {
				if (consumer.getAccounts()[i] == null)
					continue;
				try {
					if (consumer.getAccounts()[i].getBankAccount() != null &&
						consumer.getAccounts()[i].getBankAccount().getAccountId() == null) {
						//only do if new account - if accountId not null, we only have masked values
						ba = dao.getBankAccountBlockedCodes(consumer
								.getAccounts()[i].getBankAccount()
								.getAbaNumber(), consumer.getAccounts()[i]
								.getBankAccount().getAccountNumber());
						consumer.getAccounts()[i].getBankAccount().setBlocked(
								ba.getBlocked());
						consumer.getAccounts()[i].getBankAccount()
								.setAbaBlocked(ba.getAbaBlocked());
						consumerBlocked = 
							 consumerBlocked || 
						     ba.getAbaBlocked().booleanValue() ||
						     ba.getBlocked();
					} else if (consumer.getAccounts()[i].getCreditCard() != null &&
							   consumer.getAccounts()[i].getCreditCard().getAccountId() == null) {
						//only do if new account - if accountId not null, we only have masked values
						
						
						String accNumber = consumer.getAccounts()[i].getCreditCard().getAccountNumber();
						
						//call decrypt service
						String blockedId = ConsumerUtil.decryptUsingPCI(accNumber, com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.BLOCKED);
						CreditCardReponse ccr = dao.getCreditCardBlockedCodes(accNumber, blockedId);						
						cc = ccr.getCreditCard();
						cc.setBlocked(false);
						
						if(blockedId != null) {
							cc.setBlocked(true);
							String blockedCode = ccr.getBlockedStatCode();
							//If blockedId returned, but status is 'NBK' in EMG blocked card table, then don't block cust_account
							if("NBK".equalsIgnoreCase(blockedCode)){
								cc.setBlocked(false);
							}

						} // If no result, then card number isn�t blocked
						
						consumer.getAccounts()[i].getCreditCard().setBlocked(
								cc.getBlocked());
						consumer.getAccounts()[i].getCreditCard()
								.setBinBlocked(cc.getBinBlocked());
						consumerBlocked = 
							 consumerBlocked || 
						     cc.getBinBlocked().booleanValue() ||
						     cc.getBlocked();
					}
				} catch (Exception e) {
					logger.warn("process: failed to get account blocked codes",
							e);
					throw new CommandException(
							"Failed to get account blocked codes", e);
				}
			}
		}

		if (consumer.getInternal() != null) {
			consumer.getInternal().setConsumerBlocked(consumerBlocked);
		}
			
		GetConsumerBlockedStatusResponse response = new GetConsumerBlockedStatusResponse();
		response.setConsumer(consumer);
		return response;
	}
}
