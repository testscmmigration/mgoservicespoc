package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class UpdateConsumerProfileRequest extends BaseOperationRequest {

	private static final long serialVersionUID = 1L;
	private Consumer consumer;
	private Comment comment;
    private com.moneygram.mgo.service.consumer.ProfileEvent profileEvent;	
	private UpdateConsumerProfileTask[] updateTasks;

	public UpdateConsumerProfileRequest() {
	}

	/**
	 * Gets the consumer value for this UpdateConsumerProfileRequest.
	 * 
	 * @return consumer
	 */
	public Consumer getConsumer() {
		return consumer;
	}

	/**
	 * Sets the consumer value for this UpdateConsumerProfileRequest.
	 * 
	 * @param consumer
	 */
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	/**
	 * Gets the comment value for this UpdateConsumerProfileRequest.
	 * 
	 * @return comment
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * Sets the comment value for this UpdateConsumerProfileRequest.
	 * 
	 * @param comment
	 */
	public void setComment(Comment comment) {
		this.comment = comment;
	}

    /**
     * Gets the profileEvent value for this UpdateConsumerProfileRequest.
     * 
     * @return profileEvent
     */
    public com.moneygram.mgo.service.consumer.ProfileEvent getProfileEvent() {
        return profileEvent;
    }


    /**
     * Sets the profileEvent value for this UpdateConsumerProfileRequest.
     * 
     * @param profileEvent
     */
    public void setProfileEvent(com.moneygram.mgo.service.consumer.ProfileEvent profileEvent) {
        this.profileEvent = profileEvent;
    }
    
	/**
	 * Gets the updateTasks value for this UpdateConsumerProfileRequest.
	 * 
	 * @return updateTasks
	 */
	public UpdateConsumerProfileTask[] getUpdateTasks() {
		return updateTasks;
	}

	/**
	 * Sets the updateTasks value for this UpdateConsumerProfileRequest.
	 * 
	 * @param updateTasks
	 */
	public void setUpdateTasks(UpdateConsumerProfileTask[] updateTasks) {
		this.updateTasks = updateTasks;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateConsumerProfileRequest)) return false;
        UpdateConsumerProfileRequest other = (UpdateConsumerProfileRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumer==null && other.getConsumer()==null) || 
             (this.consumer!=null &&
              this.consumer.equals(other.getConsumer()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.profileEvent==null && other.getProfileEvent()==null) || 
             (this.profileEvent!=null &&
              this.profileEvent.equals(other.getProfileEvent()))) &&
            ((this.updateTasks==null && other.getUpdateTasks()==null) || 
             (this.updateTasks!=null &&
              java.util.Arrays.equals(this.updateTasks, other.getUpdateTasks())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumer() != null) {
            _hashCode += getConsumer().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getProfileEvent() != null) {
            _hashCode += getProfileEvent().hashCode();
        }
        if (getUpdateTasks() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpdateTasks());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpdateTasks(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ Consumer=").append(getConsumer());
		buffer.append("  Comment=").append(getComment());
		buffer.append("  ProfileEvent=").append(getProfileEvent());
		buffer.append("  UpdateTasks=").append(getUpdateTasks());
		buffer.append(" ]");
		return buffer.toString();
	}
}
