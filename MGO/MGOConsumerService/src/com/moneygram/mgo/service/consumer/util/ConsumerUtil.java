package com.moneygram.mgo.service.consumer.util;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.commons.lang.StringUtils;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.util.SecurityUtility;
import com.moneygram.mgo.service.consumer.Comment;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.CreditCard;
import com.moneygram.mgo.service.consumer.FIAccountType;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.shared.ConsumerAddress;
import com.moneygram.www.PCIDecryptService_v2.PCIDecryptService_PortType;
import com.moneygram.www.PCIDecryptService_v2.PCIDecryptService_ServiceLocator;
import com.moneygram.www.PCIDecryptService_v2.SourceIdByAccountRequest;
import com.moneygram.www.PCIDecryptService_v2.SourceIdRecord;
import com.moneygram.www.PCIEncryptService_v2.CardType;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountRequest;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountResponse;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountResponseCode;
import com.moneygram.www.PCIEncryptService_v2.PCIEncryptServiceClient;
import com.moneygram.www.PCIEncryptService_v2.SourceUsageCategoryCode;
import com.moneygram.www.PCIEncryptService_v2.SourceUsageTypeCode;

public class ConsumerUtil {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerUtil.class);

	private ConsumerUtil() {
	}

	public static CardType determinePCICardType(CreditCard creditCard) {
		if (creditCard == null || creditCard.getAccountType() == null)
			return null;
		if (creditCard.getAccountType().equals(
				FIAccountType.fromValue("CC-VISA")))
			return CardType.VI;
		else if (creditCard.getAccountType().equals(
				FIAccountType.fromValue("CC-MSTR")))
			return CardType.MC;
		else if (creditCard.getAccountType().equals(FIAccountType.fromValue("CC-DSCVR")))
		{
         return CardType.DS;
		}
		return null;
	}

	public static void addAccount(Long consumerId, Long addressId,
			ConsumerFIAccount account, ConsumerDAO dao, String consumerName)
			throws DAOException {
		if (account == null)
			throw new DAOException("ConsumerFIAccount is invalid");

		Long accountId = null;
		if (account.getBankAccount() != null) {
			accountId = dao.addBankAccount(consumerId, addressId, account
					.getBankAccount(), consumerName);
		} else if (account.getCreditCard() != null) {
			if (account.getCreditCard().getAccountNumber() == null)
				throw new DAOException("Credit card number invalid");
			CardType cardType = determinePCICardType(account.getCreditCard());
			if (cardType == null)
				throw new DAOException("Credit card type invalid");

			accountId = dao.addCreditCardAccount(consumerId, addressId, account
					.getCreditCard(), consumerName, null);

			encryptUsingPCI(account.getCreditCard().getAccountNumber(), String.valueOf(accountId), cardType);
            //TODO(AJP): if PCI service throws exception, new entry is still attached to profile
			//but they won't be able to use it...
		} else {
			throw new DAOException(
					"ConsumerFIAccount must have BankAccount or CreditCard");
		}
		if (account.getBankAccount() != null) {
			account.getBankAccount().setAccountId(accountId);
		} else if (account.getCreditCard() != null) {
			account.getCreditCard().setAccountId(accountId);
		}
	}

   

	public static Long getAddressId(Long consumerId, ConsumerDAO dao)
			throws DAOException {
		List<ConsumerAddress> addressList = dao
				.getConsumerAddresses(consumerId);
		if (addressList == null || addressList.size() != 1) {
			logger.warn("Failed to get one and only one address for consumer "
					+ consumerId);
			throw new DAOException(
					"Failed to get one and only one address for consumer "
							+ consumerId);
		}
		if (addressList.get(0) == null
				|| addressList.get(0).getAddressId() == null) {
			logger.warn("Failed to get address id for consumer " + consumerId);
			throw new DAOException("Failed to get address id for consumer "
					+ consumerId);
		}
		return addressList.get(0).getAddressId();
	}

	public static String getConsumerName(Long consumerId, ConsumerDAO dao)
			throws DAOException {
		Consumer consumer = dao.getConsumer(consumerId, null);
		if (consumer == null || consumer.getPersonal() == null
				|| consumer.getPersonal().getFirstName() == null
				|| consumer.getPersonal().getLastName() == null) {
			logger
					.warn("Failed to get consumer first and last name for consumer "
							+ consumerId);
			throw new DAOException(
					"Failed to get consumer first and last name for consumer "
							+ consumerId);
		}
		String middleName = consumer.getPersonal().getMiddleName() == null ? "" : consumer.getPersonal().getMiddleName();
		String secondLastName = consumer.getPersonal().getSecondLastName() == null ? "" : consumer.getPersonal().getSecondLastName();
		return consumer.getPersonal().getFirstName() + " "
				+ consumer.getPersonal().getLastName() + middleName + secondLastName;
	}

	public static Long extractAccountId(ConsumerFIAccount account) {
		if (account == null)
			return null;
		if (account.getBankAccount() != null
				&& account.getBankAccount().getAccountId() != null)
			return account.getBankAccount().getAccountId();
		if (account.getCreditCard() != null
				&& account.getCreditCard().getAccountId() != null)
			return account.getCreditCard().getAccountId();
		return null;
	}

	public static void setCommentsForAccount(Comment[] comments,
			ConsumerFIAccount account) {
		if (account == null)
			return;
		if (account.getBankAccount() != null
				&& account.getBankAccount().getAccountId() != null)
			account.getBankAccount().setComments(comments);
		if (account.getCreditCard() != null
				&& account.getCreditCard().getAccountId() != null)
			account.getCreditCard().setComments(comments);
	}

	public static String encrypt(String value) throws DAOException {
		try {
			if (value == null)
				throw new Exception("Data to encrypt invalid");
			return SecurityUtility.encrypt(value, getResourceConfig()
					.getPublicKeyFile());
		} catch (Exception e) {
			logger.error("Failed to encrypt data", e);
			throw new DAOException("Failed to encrypt data", e);
		}
	}

	public static String encryptWithNoPadding(String value) throws DAOException {
		try {
			if (value == null)
				throw new Exception("Data to encrypt invalid");
			return SecurityUtility.encryptWithNoPadding(value, getResourceConfig()
					.getPublicKeyFile());
		} catch (Exception e) {
			logger.error("Failed to encrypt data", e);
			throw new DAOException("Failed to encrypt data", e);
		}
	}
	
	public static void encryptUsingPCI(String account, String systemId, CardType cardType) throws DAOException {
        PCIEncryptServiceClient pciClient = null;
        try {
        	String pciUrl = getResourceConfig().getPCIEncryptUrl();
        	if(!pciUrl.contains("_v2")){
        		pciUrl = pciUrl + "_v2";
        	}
            pciClient = PCIEncryptServiceClient.getInstance(
                    pciUrl,
                    getResourceConfig().getPCIEncryptTimeout());
            System.out.println(getResourceConfig().getPCIEncryptUrl());
        } catch (Exception e) {
            logger.error("Exception in getting PCIEncryptServiceClient", e);
            throw new DAOException(
                    "Exception in getting PCIEncryptServiceClient", e);
        }
        EncryptAccountRequest request = new EncryptAccountRequest();
        request.setPrimaryAccountNumber(account);
        request.setSourceSystemID(systemId);
        request.setCardType(cardType);
        request.setSourceUsageCategory(SourceUsageCategoryCode.PROFILE);
        request.setSourceUsageType(SourceUsageTypeCode.EMONEY_TRANSFER);
        request.setLastUsedDate(Calendar.getInstance());

        EncryptAccountResponse response = null;
        try {
        	response = pciClient.encryptAccount(request);
        	if (response == null
        			|| response.getResult() == null
        			|| !response.getResult().equals(
        					EncryptAccountResponseCode.SUCCESS))
        		throw new Exception("Failed to get SUCCESS response");
        } catch (Exception e) {
        	logger
        			.error(
        					"Exception in storing account number using PCIEncryptServiceClient",
        					e);
        	throw new DAOException(
        			"Exception in storing account number using PCIEncryptServiceClient",
        			e);
        }
    }
	
    public static String decryptUsingPCI(String account, com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode succ) throws DAOException {
        
    	SourceIdRecord[] response = new SourceIdRecord[0];
		
		try {
			URL endpoint = new URL(getResourceConfig().getPCIDecryptUrl());
			String userName = getResourceConfig().getPCIDecryptUser();
			String password = getResourceConfig().getPCIDecryptPass();
			
			PCIDecryptService_PortType decryptService = new PCIDecryptService_ServiceLocator()
			.getPCIDecryptServiceSOAP_v2(endpoint);
			
			// provide the service account userID & password for authentication
			((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
			((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);
			
			SourceIdByAccountRequest request = new SourceIdByAccountRequest();
			request.setPrimaryAccountNumber(account);
			
			com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode catCode;
			com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode typeCode;
			
			catCode =  com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.BLOCKED;
			typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;
			
			request.setSourceUsageCategory (catCode);
			request.setSourceUsageType(typeCode);
			request.setPrimaryAccountNumber(account);
			request.setUserID("MGODE");
			
			response = decryptService.getSourceIdByAccount(request);

		} catch (Exception e) { //SourceSystemID not found for this account
			return null; //the card is not blocked
		}
		
		return response[0].getSourceSystemID();
    }

	public static String hash(String value) throws DAOException {
		try {
			if (value == null)
				throw new Exception("Data to hash invalid");
			return SecurityUtility.digest(value, getResourceConfig()
					.getSecureHashSeed());
		} catch (Exception e) {
			logger.error("Failed to hash data", e);
			throw new DAOException("Failed to hash data", e);
		}
	}

	public static MGOConsumerServiceResourceConfig getResourceConfig() {
		return MGOConsumerServiceResourceConfig.getInstance();
	}

	public static String extractDomainFromEmail(String email) {
		if (email == null)
			return null;

		String domain = null;
		if (email.length() > 3) {
			String splitResult[] = email.split("@",2);
			if (splitResult.length > 1) {
				//only pull domain if '@' delimiter is found
				domain = splitResult[splitResult.length - 1];
			}
		}
        return domain;
	}

	public static void checkNewAccountValidity(ConsumerFIAccount account)
			throws CommandException {
		if (account == null)
			throw new DataFormatException("Invalid account");
		if (account.getCreditCard() != null) {
			if (account.getCreditCard().getAccountStatus() == null)
				throw new DataFormatException("Invalid account status");
			if (account.getCreditCard().getAccountSubStatus() == null)
				throw new DataFormatException("Invalid account subStatus");
		} else if (account.getBankAccount() != null) {
			if (account.getBankAccount().getAccountStatus() == null)
				throw new DataFormatException("Invalid account status");
			if (account.getBankAccount().getAccountSubStatus() == null)
				throw new DataFormatException("Invalid account subStatus");
		} else
			throw new DataFormatException(
					"Invalid account; must be either bankAccount or creditCard");
	}

    /**
     * Masks the string with last N characters left.
     * @param value
     * @param n
     * @param mask
     * @return
     */
    public static String maskLastN(String value, int n, char mask) {
        StringBuffer accountNumber = null;
        if (value != null && StringUtils.isNotEmpty(value)) {
            accountNumber = new StringBuffer(value);
            for (int i = 0; i < accountNumber.length() - n; i++) {
                accountNumber.setCharAt(i, mask);
            }
        }

        return (accountNumber == null ? null : accountNumber.toString());
    }

    /**
     * Masks the string with last 4 characters left using '*'.
     * @param value
     * @return
     */
    public static String maskLast4(String value) {
        return maskLastN(value, 4, '*');
    }

}
