package com.moneygram.mgo.service.consumer.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.Contact;
import com.moneygram.mgo.service.consumer.LoyaltyInfo;
import com.moneygram.mgo.service.consumer.MGOAttributes;
import com.moneygram.mgo.service.consumer.PersonalInfo;

public class ConsumerRowMapper extends BaseRowMapper {

	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet,
	 *      int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Consumer consumer = new Consumer();
		consumer.setConsumerId(new Long(rs.getLong("CUST_ID")));
		consumer.setLoginId(rs.getString("CUST_LOGON_ID"));
		PersonalInfo pi = new PersonalInfo();
		pi.setFirstName(rs.getString("CUST_FRST_NAME"));
		pi.setMiddleName(rs.getString("CUST_MID_NAME"));
		pi.setLastName(rs.getString("CUST_LAST_NAME"));
		//vl58 -- Release 36, capture "CUST_MATRNL_NAME" for storing it in secondLastName attribute
		pi.setSecondLastName(rs.getString("CUST_MATRNL_NAME"));
		pi.setSsn(rs.getString("CUST_SSN_MASK_NBR"));
		Calendar cal;
		Date date = rs.getDate("CUST_BRTH_DATE");
		if (date != null) {
			cal = Calendar.getInstance();
			cal.setTime(date);
			pi.setDateOfBirth(cal);
		}
		consumer.setPersonal(pi);
		consumer.setContact(new Contact());
		consumer.getContact().setPromoEmail(
				DAOUtils.toBoolean(rs.getString("PROMO_EMAIL_CODE")));
		consumer.setInternal(new MGOAttributes());
		consumer.getInternal().setGlobalUniqueId(rs.getString("EDIR_GU_ID"));
		consumer.getInternal()
				.setConsumerStatus(rs.getString("CUST_STAT_CODE"));
		consumer.getInternal().setConsumerSubStatus(
				rs.getString("CUST_SUB_STAT_CODE"));
		consumer.getInternal().setPremierStatus(rs.getString("CUST_PRMR_CODE"));
		consumer.getInternal().setConsumerBlocked(
				DAOUtils.toBoolean(rs.getString("CUST_BLKD_CODE"), "B", null));
		consumer.getInternal().setSecurityQuestion(rs.getString("VRFY_QUEST_TEXT"));
		consumer.getInternal().setSecurityAnswer(rs.getString("VRFY_ANS_TEXT"));
		date = rs.getDate("ADPTV_AUTHN_PRFL_CMPLT_DATE");
		if (date != null) {
			cal = Calendar.getInstance();
			cal.setTime(date);
			consumer.getInternal().setSecurityQuestionsCollectionDate(cal);
		}
		date = rs.getDate("CREATE_DATE");
		if (date != null) {
			cal = Calendar.getInstance();
			cal.setTime(date);
			consumer.getInternal().setCreateDate(cal);
		}
		consumer.setLoyalty(new LoyaltyInfo());
		consumer.getLoyalty().setMemberId(rs.getString("LYLTY_PGM_MBSHP_ID"));
		consumer.getLoyalty().setAutoEnroll(
				DAOUtils.toBoolean(rs.getString("CUST_AUTO_ENRL_FLAG")));
		consumer.setSourceSite(rs.getString("src_web_site_bsns_cd"));
		
		//vl58 -- Release 36, new field returned for consumer profile id "cnsmr_prfl_id"
		Long cnsmrProfileId = rs.getLong("cnsmr_prfl_id");
		if(cnsmrProfileId != null){
			consumer.setCnsmrProfileId(cnsmrProfileId);
		}
		return consumer;
	}
}
