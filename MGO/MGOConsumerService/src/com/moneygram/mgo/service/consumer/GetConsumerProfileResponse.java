/**
 * GetConsumerProfileResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationResponse;

public class GetConsumerProfileResponse extends BaseOperationResponse {

	private static final long serialVersionUID = 1L;
	private Consumer consumer;

	public GetConsumerProfileResponse() {
	}

	/**
	 * Gets the consumer value for this GetConsumerProfileResponse.
	 * 
	 * @return consumer
	 */
	public Consumer getConsumer() {
		return consumer;
	}

	/**
	 * Sets the consumer value for this GetConsumerProfileResponse.
	 * 
	 * @param consumer
	 */
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetConsumerProfileResponse))
			return false;
		GetConsumerProfileResponse other = (GetConsumerProfileResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.consumer == null && other.getConsumer() == null) || (this.consumer != null && this.consumer
						.equals(other.getConsumer())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getConsumer() != null) {
			_hashCode += getConsumer().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ Consumer=").append(getConsumer());
		buffer.append(" ]");
		return buffer.toString();
	}
}
