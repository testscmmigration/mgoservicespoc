/**
 * BankAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class BankAccount extends FIAccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String abaNumber;
	private java.lang.String fiName;
	private java.lang.Boolean abaBlocked;

	public BankAccount() {
	}

	/**
	 * Gets the aBANumber value for this BankAccount.
	 * 
	 * @return aBANumber
	 */
	public java.lang.String getAbaNumber() {
		return abaNumber;
	}

	/**
	 * Sets the abaNumber value for this BankAccount.
	 * 
	 * @param abaNumber
	 */
	public void setAbaNumber(java.lang.String abaNumber) {
		this.abaNumber = abaNumber;
	}

	/**
	 * Gets the fiName value for this BankAccount.
	 * 
	 * @return fiName
	 */
	public java.lang.String getFiName() {
		return fiName;
	}

	/**
	 * Sets the fiName value for this BankAccount.
	 * 
	 * @param fiName
	 */
	public void setFiName(java.lang.String fiName) {
		this.fiName = fiName;
	}

	/**
	 * Gets the abaBlocked value for this BankAccount.
	 * 
	 * @return abaBlocked
	 */
	public java.lang.Boolean getAbaBlocked() {
		return abaBlocked;
	}

	/**
	 * Sets the abaBlocked value for this BankAccount.
	 * 
	 * @param abaBlocked
	 */
	public void setAbaBlocked(java.lang.Boolean abaBlocked) {
		this.abaBlocked = abaBlocked;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof BankAccount))
			return false;
		BankAccount other = (BankAccount) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.abaNumber == null && other.getAbaNumber() == null) || (this.abaNumber != null && this.abaNumber
						.equals(other.getAbaNumber())))
				&& ((this.fiName == null && other.getFiName() == null) || (this.fiName != null && this.fiName
						.equals(other.getFiName())))
				&& ((this.abaBlocked == null && other.getAbaBlocked() == null) || (this.abaBlocked != null && this.abaBlocked
						.equals(other.getAbaBlocked())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getAbaNumber() != null) {
			_hashCode += getAbaNumber().hashCode();
		}
		if (getFiName() != null) {
			_hashCode += getFiName().hashCode();
		}
		if (getAbaBlocked() != null) {
			_hashCode += getAbaBlocked().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ AbaNumber=").append(getAbaNumber());
		buffer.append(" FiName=").append(getFiName());
		buffer.append(" AbaBlocked=").append(getAbaBlocked());
		buffer.append(" ]");
		return buffer.toString();
	}
}
