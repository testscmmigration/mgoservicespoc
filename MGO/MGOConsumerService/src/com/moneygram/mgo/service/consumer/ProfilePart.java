/**
 * ProfilePart.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class ProfilePart implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ProfilePart(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _TransactionPreferences = "TransactionPreferences";
    public static final java.lang.String _PersonalInfo = "PersonalInfo";
    public static final java.lang.String _Contact = "Contact";
    public static final java.lang.String _Accounts = "Accounts";
    public static final java.lang.String _AccountComments = "AccountComments";
    public static final java.lang.String _LoyaltyInfo = "LoyaltyInfo";
    public static final java.lang.String _Internal = "Internal";
    public static final java.lang.String _ConsumerComments = "ConsumerComments";
    public static final java.lang.String _Address = "Address";
    public static final java.lang.String _ProfileEvents = "ProfileEvents";
    public static final ProfilePart TransactionPreferences = new ProfilePart(_TransactionPreferences);
    public static final ProfilePart PersonalInfo = new ProfilePart(_PersonalInfo);
    public static final ProfilePart Contact = new ProfilePart(_Contact);
    public static final ProfilePart Accounts = new ProfilePart(_Accounts);
    public static final ProfilePart AccountComments = new ProfilePart(_AccountComments);
    public static final ProfilePart LoyaltyInfo = new ProfilePart(_LoyaltyInfo);
    public static final ProfilePart Internal = new ProfilePart(_Internal);
    public static final ProfilePart ConsumerComments = new ProfilePart(_ConsumerComments);
    public static final ProfilePart Address = new ProfilePart(_Address);
    public static final ProfilePart ProfileEvents = new ProfilePart(_ProfileEvents);
    public java.lang.String getValue() { return _value_;}
    public static ProfilePart fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ProfilePart enumeration = (ProfilePart)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ProfilePart fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}

}
