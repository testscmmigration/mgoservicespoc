/**
 * PersonalInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;


import java.util.Date;


public class PersonalInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.lang.String middleName;
    
    private java.lang.String secondLastName;

    private java.lang.String ssn;

    private java.util.Calendar dateOfBirth;

	private String gender;
	private String externalId;
	private String externalIdCountryOfIssuance;
	private String externalIdType;
	private String externalIdStateOfIssuance;
	private Date externalIdDateOfIssuance;
	private Date externalIdDateOfExpiration;

    public PersonalInfo() {
    }

    public PersonalInfo(
	           java.lang.String firstName,
	           java.lang.String lastName,
	           java.lang.String middleName,
	           java.lang.String secondLastName,
	           java.lang.String ssn,
	           java.util.Calendar dateOfBirth,
	           String gender,
	           String externalId,
	           String externalIdCountryOfIssuance,
	           String externalIdType,
	           String externalIdStateOfIssuance,
	           Date externalIdDateOfIssuance,
	           Date externalIdDateOfExpiration) {
           this.firstName = firstName;
           this.lastName = lastName;
           this.middleName = middleName;
           this.secondLastName = secondLastName;
           this.ssn = ssn;
           this.dateOfBirth = dateOfBirth;
           this.gender = gender;
           this.externalId = externalId;
           this.externalIdCountryOfIssuance = externalIdCountryOfIssuance;
           this.externalIdType = externalIdType;
           this.externalIdStateOfIssuance = externalIdStateOfIssuance;
           this.externalIdDateOfIssuance = externalIdDateOfIssuance;
           this.externalIdDateOfExpiration = externalIdDateOfExpiration;
    }


    /**
     * Gets the firstName value for this PersonalInfo.
     *
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this PersonalInfo.
     *
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this PersonalInfo.
     *
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this PersonalInfo.
     *
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the middleName value for this PersonalInfo.
     *
     * @return middleName
     */
    public java.lang.String getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this PersonalInfo.
     *
     * @param middleName
     */
    public void setMiddleName(java.lang.String middleName) {
        this.middleName = middleName;
    }
    
    /**
     * Gets the secondLastName value for this PersonalInfo.
     * 
     * @return
     */
    public java.lang.String getSecondLastName() {
		return secondLastName;
	}

    /**
     * Sets the secondLastName value for this PersonalInfo.
     * 
     * @param secondLastName
     */
	public void setSecondLastName(java.lang.String secondLastName) {
		this.secondLastName = secondLastName;
	}

	/**
     * Gets the ssn value for this PersonalInfo.
     *
     * @return ssn
     */
    public java.lang.String getSsn() {
        return ssn;
    }


    /**
     * Sets the ssn value for this PersonalInfo.
     *
     * @param ssn
     */
    public void setSsn(java.lang.String ssn) {
        this.ssn = ssn;
    }


    /**
     * Gets the dateOfBirth value for this PersonalInfo.
     *
     * @return dateOfBirth
     */
    public java.util.Calendar getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this PersonalInfo.
     *
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.util.Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }



    public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

    public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getExternalIdCountryOfIssuance() {
		return externalIdCountryOfIssuance;
	}

	public void setExternalIdCountryOfIssuance(String externalIdCountryOfIssuance) {
		this.externalIdCountryOfIssuance = externalIdCountryOfIssuance;
	}

	public String getExternalIdType() {
		return externalIdType;
	}

	public void setExternalIdType(String externalIdType) {
		this.externalIdType = externalIdType;
	}

	public String getExternalIdStateOfIssuance() {
		return externalIdStateOfIssuance;
	}

	public void setExternalIdStateOfIssuance(String externalIdStateOfIssuance) {
		this.externalIdStateOfIssuance = externalIdStateOfIssuance;
	}

	public Date getExternalIdDateOfIssuance() {
		return externalIdDateOfIssuance;
	}

	public void setExternalIdDateOfIssuance(Date externalIdDateOfIssuance) {
		this.externalIdDateOfIssuance = externalIdDateOfIssuance;
	}

	public Date getExternalIdDateOfExpiration() {
		return externalIdDateOfExpiration;
	}

	public void setExternalIdDateOfExpiration(Date externalIdDateOfExpiration) {
		this.externalIdDateOfExpiration = externalIdDateOfExpiration;
	}



	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PersonalInfo)) return false;
        PersonalInfo other = (PersonalInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.firstName==null && other.getFirstName()==null) ||
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) ||
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.middleName==null && other.getMiddleName()==null) ||
             (this.middleName!=null &&
              this.middleName.equals(other.getMiddleName()))) &&
            ((this.secondLastName==null && other.getSecondLastName()==null) ||
             (this.secondLastName!=null &&
              this.secondLastName.equals(other.getSecondLastName()))) &&
            ((this.ssn==null && other.getSsn()==null) ||
             (this.ssn!=null &&
              this.ssn.equals(other.getSsn()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) ||
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth())) &&
            ((this.gender==null && other.getExternalId()==null) ||
             (this.gender!=null &&
              this.gender.equals(other.getExternalId()))) &&
            ((this.externalId==null && other.getExternalId()==null) ||
             (this.externalId!=null &&
              this.externalId.equals(other.getExternalId()))) &&
            ((this.externalIdCountryOfIssuance==null && other.getExternalIdCountryOfIssuance()==null) ||
             (this.externalIdCountryOfIssuance!=null &&
              this.externalIdCountryOfIssuance.equals(other.getExternalIdCountryOfIssuance()))) &&
            ((this.externalIdType==null && other.getExternalIdType()==null) ||
             (this.externalIdType!=null &&
              this.externalIdType.equals(other.getExternalIdType()))) &&
            ((this.externalIdStateOfIssuance==null && other.getExternalIdStateOfIssuance()==null) ||
             (this.externalIdStateOfIssuance!=null &&
              this.externalIdStateOfIssuance.equals(other.getExternalIdStateOfIssuance()))) &&
            ((this.externalIdDateOfIssuance==null && other.getExternalIdDateOfIssuance()==null) ||
             (this.externalIdDateOfIssuance!=null &&
              this.externalIdDateOfIssuance.equals(other.getExternalIdDateOfIssuance()))) &&
            ((this.externalIdDateOfExpiration==null && other.getExternalIdDateOfExpiration()==null) ||
             (this.externalIdDateOfExpiration!=null &&
              this.externalIdDateOfExpiration.equals(other.getExternalIdDateOfExpiration()))));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getMiddleName() != null) {
            _hashCode += getMiddleName().hashCode();
        }
        if (getSecondLastName() != null) {
            _hashCode += getSecondLastName().hashCode();
        }
        if (getSsn() != null) {
            _hashCode += getSsn().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getExternalId() != null) {
            _hashCode += getExternalId().hashCode();
        }
        if (getExternalIdCountryOfIssuance() != null) {
            _hashCode += getExternalIdCountryOfIssuance().hashCode();
        }
        if (getExternalIdType() != null) {
            _hashCode += getExternalIdType().hashCode();
        }
        if (getExternalIdStateOfIssuance() != null) {
            _hashCode += getExternalIdStateOfIssuance().hashCode();
        }
        if (getExternalIdDateOfIssuance() != null) {
            _hashCode += getExternalIdDateOfIssuance().hashCode();
        }
        if (getExternalIdDateOfExpiration() != null) {
            _hashCode += getExternalIdDateOfExpiration().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ FirstName=").append(getFirstName());
		buffer.append(" LastName=").append(getLastName());
		buffer.append(" MiddleName=").append(getMiddleName());
		buffer.append(" SecondLastName=").append(getSecondLastName());
		buffer.append(" Ssn=").append(getSsn());
		buffer.append(" DateOfBirth=").append(getDateOfBirth());
		buffer.append(" ExternalId=").append(getExternalId());
		buffer.append(" ExternalIdCountryOfIssuance=").append(getExternalIdCountryOfIssuance());
		buffer.append(" ExternalIdType=").append(getExternalIdType());
		buffer.append(" ExternalIdStateOfIssuance=").append(getExternalIdStateOfIssuance());
		buffer.append(" ExternalIdDateOfIssuance=").append(getExternalIdDateOfIssuance());
		buffer.append(" ExternalIdDateOfExpiration=").append(getExternalIdDateOfExpiration());
		buffer.append(" ]");
		return buffer.toString();
	}
}
