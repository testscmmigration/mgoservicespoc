/**
 * TransactionPreferences.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class TransactionPreferences implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private SavedAgent[] savedAgents;

	public TransactionPreferences() {
	}

	/**
	 * Gets the savedAgents value for this TransactionPreferences.
	 * 
	 * @return savedAgents
	 */
	public SavedAgent[] getSavedAgents() {
		return savedAgents;
	}

	/**
	 * Sets the savedAgents value for this TransactionPreferences.
	 * 
	 * @param savedAgents
	 */
	public void setSavedAgents(SavedAgent[] savedAgents) {
		this.savedAgents = savedAgents;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ SavedAgents=").append(getSavedAgents());
		buffer.append(" ]");
		return buffer.toString();
	}

}
