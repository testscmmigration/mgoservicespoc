/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.Contact;
import com.moneygram.mgo.service.consumer.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer.ProfileEvent;
import com.moneygram.mgo.service.consumer.ProfilePart;
import com.moneygram.mgo.service.consumer.SavedAgent;
import com.moneygram.mgo.service.consumer.TransactionPreferences;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer.extended.ExtendedContact;
import com.moneygram.mgo.shared.ConsumerAddress;
import com.moneygram.mgo.shared.ErrorCodes;

/**
 *
 * Get Consumer Profile Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.24 $ $Date: 2012/02/03 17:24:55 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class GetConsumerProfileCommand extends ReadCommand {

    public static final Logger logger = LogFactory.getInstance().getLogger(GetConsumerProfileCommand.class);
    public static final String DEFAULT_PARTNER_SITE_ID = "MGO";

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetConsumerProfileRequest;
    }

    @Override
    protected OperationResponse process(OperationRequest request) throws CommandException {
        GetConsumerProfileRequest profileRequest = (GetConsumerProfileRequest) request;

        if (logger.isDebugEnabled()) {
            logger.debug("process: get consumer profile consumerId=" + profileRequest.getConsumerId() + " loginId="
                    + profileRequest.getConsumerLoginId() + " responseFilter="
                    + printResponseFilter(profileRequest.getResponseFilter()));
        }

        validateRequest(profileRequest);

        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

        Consumer consumer = null;
        try {
            //The following data is retrieved and populated by getConsumer
            //ConsumerId
            //LoginId
            //PersonalInfo
            //Internal
            //LoyaltyInfo
        	Long consumerId = profileRequest.getConsumerId();
        	String consumerLoginId = profileRequest.getConsumerLoginId();
        	String partnerSiteId = profileRequest.getHeader().getProcessingInstruction().getPartnerSiteId();
        	if (partnerSiteId == null) {
        		partnerSiteId = DEFAULT_PARTNER_SITE_ID;
        		profileRequest.getHeader().getProcessingInstruction().setPartnerSiteId(partnerSiteId);
        	}

            consumer = dao.getConsumer(consumerId, consumerLoginId, partnerSiteId);
        } catch (Exception e) {
            throw new CommandException("Failed to get basic consumer info", e);
        }
        if (consumer == null)
            throw new BusinessRulesException(ErrorCodes.CONSUMER_NOT_FOUND, ErrorCodes
                    .getMessage(ErrorCodes.CONSUMER_NOT_FOUND));

        if (includedInResponseFilter(ProfilePart.Address, profileRequest.getResponseFilter())) {
            try {
                List<ConsumerAddress> list = dao.getConsumerAddresses(consumer.getConsumerId(), profileRequest
                        .getAddressId());
                if (list != null && !list.isEmpty()) {
                    consumer.setAddress(list.get(0));
                }
            } catch (Exception e) {
                throw new CommandException("Failed to get consumer address", e);
            }
        }

        if (includedInResponseFilter(ProfilePart.Contact, profileRequest.getResponseFilter())
                || includedInResponseFilter(ProfilePart.Internal, profileRequest.getResponseFilter())) {
            try {
                List<ExtendedContact> list = dao.getConsumerPhoneContacts(consumer.getConsumerId());
                if (list != null && !list.isEmpty()) {

                    if (consumer.getContact() == null)
                        consumer.setContact(new Contact());

                    for (ExtendedContact c : list) {
                        if (c.getPrimaryPhone() != null) {
                            consumer.getContact().setPrimaryPhone(c.getPrimaryPhone());
                            consumer.getInternal().setPrimaryPhoneBlocked(c.getPrimaryPhoneBlocked());
                            consumer.getContact().setPrimaryPhoneType(c.getPrimaryPhoneType());
                        }
                        if (c.getAlternatePhone() != null) {
                            consumer.getContact().setAlternatePhone(c.getAlternatePhone());
                            consumer.getInternal().setAlternatePhoneBlocked(c.getAlternatePhoneBlocked());
                            consumer.getContact().setAlternatePhoneType(c.getAlternatePhoneType());
                        }
                    }
                }

                list = dao.getConsumerEmailContacts(consumer.getConsumerId());
                if (list != null && !list.isEmpty()) {
                    if (consumer.getContact() == null)
                        consumer.setContact(new Contact());
                    for (ExtendedContact c : list) {
                        if (c.getEmail() != null) {
                            consumer.getContact().setEmail(c.getEmail());
                            consumer.getInternal().setEmailBlocked(c.getEmailBlocked());
                            consumer.getInternal().setEmailDomainBlocked(c.getEmailDomainBlocked());
                        }
                    }
                }
            } catch (Exception e) {
                throw new CommandException("Failed to get consumer contacts", e);
            }
        }

        if (includedInResponseFilter(ProfilePart.TransactionPreferences, profileRequest.getResponseFilter())) {
            try {
                List<SavedAgent> agents = dao.getAgents(profileRequest.getConsumerId(), profileRequest
                        .getConsumerLoginId());
                TransactionPreferences transactionPreferences = new TransactionPreferences();
                if (agents == null)
                    transactionPreferences.setSavedAgents(new SavedAgent[0]);
                else
                    transactionPreferences.setSavedAgents((SavedAgent[]) agents.toArray(new SavedAgent[agents.size()]));
                consumer.setTransactionPreferences(transactionPreferences);
            } catch (Exception e) {
                throw new CommandException("Failed to get consumer transaction preferences", e);
            }
        }

        if (includedInResponseFilter(ProfilePart.Accounts, profileRequest.getResponseFilter())) {
            try {
                List<ConsumerFIAccount> accounts = dao.getConsumerAccounts(consumer.getConsumerId(), null, null);
                if (accounts != null)
                    consumer
                            .setAccounts((ConsumerFIAccount[]) accounts.toArray(new ConsumerFIAccount[accounts.size()]));
            } catch (Exception e) {
                throw new CommandException("Failed to get consumer accounts", e);
            }
        }

        if (includedInResponseFilter(ProfilePart.ProfileEvents, profileRequest.getResponseFilter())) {
            try {
                List<ProfileEvent> events = dao.getProfileEvents(consumer.getConsumerId(), null, null, null);
                if (events != null)
                    consumer.setProfileEvents((ProfileEvent[]) events.toArray(new ProfileEvent[events.size()]));
            } catch (Exception e) {
                throw new CommandException("Failed to get consumer profile events", e);
            }
        }

        //Remove internal if not requested
        if (!includedInResponseFilter(ProfilePart.Internal, profileRequest.getResponseFilter())) {
            consumer.setInternal(null);
        }

        //Remove personalInfo if not requested
        if (!includedInResponseFilter(ProfilePart.PersonalInfo, profileRequest.getResponseFilter())) {
            consumer.setPersonal(null);
        }

        //Remove loyaltyInfo if not requested
        if (!includedInResponseFilter(ProfilePart.LoyaltyInfo, profileRequest.getResponseFilter())) {
            consumer.setLoyalty(null);
        }

        //Remove contact if not requested
        if (!includedInResponseFilter(ProfilePart.Contact, profileRequest.getResponseFilter())) {
            consumer.setContact(null);
        }

        GetConsumerProfileResponse response = new GetConsumerProfileResponse();
        response.setConsumer(consumer);
        return response;
    }

    private boolean includedInResponseFilter(ProfilePart profilePart, ProfilePart[] responseFilter) {
        if (responseFilter == null || responseFilter.length == 0 || profilePart == null)
            return false;
        for (int i = 0; i < responseFilter.length; i++) {
            if (responseFilter[i] == null || responseFilter[i].getValue() == null)
                continue;
            if (responseFilter[i].getValue().equals(profilePart.getValue()))
                return true;
        }
        return false;
    }

    private String printResponseFilter(ProfilePart[] responseFilter) {
        if (responseFilter == null || responseFilter.length == 0)
            return "";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < responseFilter.length; i++) {
            if (responseFilter[i] == null)
                continue;
            if (sb.length() > 0)
                sb.append(",");
            sb.append(responseFilter[i].getValue());
        }
        return sb.toString();
    }

    private void validateRequest(GetConsumerProfileRequest profileRequest) throws CommandException{
    	if (profileRequest.getConsumerId() == null && profileRequest.getConsumerLoginId() == null) {
            throw new DataFormatException("Invalid input consumer id/login id");
        }

        if (profileRequest.getResponseFilter() == null || profileRequest.getResponseFilter().length == 0) {
            throw new DataFormatException("Empty response filter");
        }
    }
}
