/**
 * AddAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class AddAccountRequest extends BaseOperationRequest implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private long consumerId;
	private ConsumerFIAccount consumerFIAccount;

	public AddAccountRequest() {
	}

	/**
	 * Gets the consumerId value for this AddAccountRequest.
	 * 
	 * @return consumerId
	 */
	public long getConsumerId() {
		return consumerId;
	}

	/**
	 * Sets the consumerId value for this AddAccountRequest.
	 * 
	 * @param consumerId
	 */
	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	/**
	 * Gets the consumerFIAccount value for this AddAccountRequest.
	 * 
	 * @return consumerFIAccount
	 */
	public ConsumerFIAccount getConsumerFIAccount() {
		return consumerFIAccount;
	}

	/**
	 * Sets the consumerFIAccount value for this AddAccountRequest.
	 * 
	 * @param consumerFIAccount
	 */
	public void setConsumerFIAccount(ConsumerFIAccount consumerFIAccount) {
		this.consumerFIAccount = consumerFIAccount;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof AddAccountRequest))
			return false;
		AddAccountRequest other = (AddAccountRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& this.consumerId == other.getConsumerId()
				&& ((this.consumerFIAccount == null && other
						.getConsumerFIAccount() == null) || (this.consumerFIAccount != null && this.consumerFIAccount
						.equals(other.getConsumerFIAccount())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		_hashCode += new Long(getConsumerId()).hashCode();
		if (getConsumerFIAccount() != null) {
			_hashCode += getConsumerFIAccount().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ConsumerId=").append(getConsumerId());
		buffer.append(" ConsumerFIAccount=").append(getConsumerFIAccount());
		buffer.append(" ]");
		return buffer.toString();
	}
}
