/*
 * Created on Aug 28, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer.GetProfileEventsRequest;
import com.moneygram.mgo.service.consumer.GetProfileEventsResponse;
import com.moneygram.mgo.service.consumer.ProfileEvent;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;

public class GetProfileEventsCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetProfileEventsCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetProfileEventsRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetProfileEventsRequest peRequest = (GetProfileEventsRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: GetProfileEventsRequest: " + peRequest.toString());
		}
		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		List<ProfileEvent> list = null;

		try {
			list = dao.getProfileEvents(peRequest.getConsumerId(), peRequest.getStartDateTime(), peRequest
					.getEndDateTime(), null);
		} catch (Exception e) {
			logger.warn("process: failed to get incomplete profiles", e);
			throw new CommandException("Failed to get incomplete profiles", e);
		}
		GetProfileEventsResponse response = new GetProfileEventsResponse();
		response.setEvents((ProfileEvent[]) list.toArray(new ProfileEvent[list.size()]));

		return response;
	}

}
