/**
 * Comment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class Comment implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String reasonCode;
	private java.lang.String commentText;
	private java.lang.String userId;
	private java.util.Calendar createDate;

	public Comment() {
	}

	/**
	 * Gets the reasonCode value for this Comment.
	 * 
	 * @return reasonCode
	 */
	public java.lang.String getReasonCode() {
		return reasonCode;
	}

	/**
	 * Sets the reasonCode value for this Comment.
	 * 
	 * @param reasonCode
	 */
	public void setReasonCode(java.lang.String reasonCode) {
		this.reasonCode = reasonCode;
	}

	/**
	 * Gets the commentText value for this Comment.
	 * 
	 * @return commentText
	 */
	public java.lang.String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the commentText value for this Comment.
	 * 
	 * @param commentText
	 */
	public void setCommentText(java.lang.String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the userId value for this Comment.
	 * 
	 * @return userId
	 */
	public java.lang.String getUserId() {
		return userId;
	}

	/**
	 * Sets the userId value for this Comment.
	 * 
	 * @param userId
	 */
	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the createDate value for this Comment.
	 * 
	 * @return createDate
	 */
	public java.util.Calendar getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the createDate value for this Comment.
	 * 
	 * @param createDate
	 */
	public void setCreateDate(java.util.Calendar createDate) {
		this.createDate = createDate;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Comment))
			return false;
		Comment other = (Comment) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.reasonCode == null && other.getReasonCode() == null) || (this.reasonCode != null && this.reasonCode
						.equals(other.getReasonCode())))
				&& ((this.commentText == null && other.getCommentText() == null) || (this.commentText != null && this.commentText
						.equals(other.getCommentText())))
				&& ((this.userId == null && other.getUserId() == null) || (this.userId != null && this.userId
						.equals(other.getUserId())))
				&& ((this.createDate == null && other.getCreateDate() == null) || (this.createDate != null && this.createDate
						.equals(other.getCreateDate())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getReasonCode() != null) {
			_hashCode += getReasonCode().hashCode();
		}
		if (getCommentText() != null) {
			_hashCode += getCommentText().hashCode();
		}
		if (getUserId() != null) {
			_hashCode += getUserId().hashCode();
		}
		if (getCreateDate() != null) {
			_hashCode += getCreateDate().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ReasonCode=").append(getReasonCode());
		buffer.append(" CommentText=").append(getCommentText());
		buffer.append(" UserId=").append(getUserId());
		buffer.append(" CreateDate=").append(getCreateDate());
		buffer.append(" ]");
		return buffer.toString();
	}
}
