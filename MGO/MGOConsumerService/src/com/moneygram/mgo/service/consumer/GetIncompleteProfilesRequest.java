/**
 * GetIncompleteProfilesRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class GetIncompleteProfilesRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private java.util.Calendar startDateTime;

    private java.util.Calendar endDateTime;

    private java.lang.Integer maxResults;

    public GetIncompleteProfilesRequest() {
    }

    public GetIncompleteProfilesRequest(
           RequestHeader header,
           java.util.Calendar startDateTime,
           java.util.Calendar endDateTime,
           java.lang.Integer maxResults) {
        super(
            header);
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.maxResults = maxResults;
    }


    /**
     * Gets the startDateTime value for this GetIncompleteProfilesRequest.
     * 
     * @return startDateTime
     */
    public java.util.Calendar getStartDateTime() {
        return startDateTime;
    }


    /**
     * Sets the startDateTime value for this GetIncompleteProfilesRequest.
     * 
     * @param startDateTime
     */
    public void setStartDateTime(java.util.Calendar startDateTime) {
        this.startDateTime = startDateTime;
    }


    /**
     * Gets the endDateTime value for this GetIncompleteProfilesRequest.
     * 
     * @return endDateTime
     */
    public java.util.Calendar getEndDateTime() {
        return endDateTime;
    }


    /**
     * Sets the endDateTime value for this GetIncompleteProfilesRequest.
     * 
     * @param endDateTime
     */
    public void setEndDateTime(java.util.Calendar endDateTime) {
        this.endDateTime = endDateTime;
    }


    /**
     * Gets the maxResults value for this GetIncompleteProfilesRequest.
     * 
     * @return maxResults
     */
    public java.lang.Integer getMaxResults() {
        return maxResults;
    }


    /**
     * Sets the maxResults value for this GetIncompleteProfilesRequest.
     * 
     * @param maxResults
     */
    public void setMaxResults(java.lang.Integer maxResults) {
        this.maxResults = maxResults;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIncompleteProfilesRequest)) return false;
        GetIncompleteProfilesRequest other = (GetIncompleteProfilesRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.startDateTime==null && other.getStartDateTime()==null) || 
             (this.startDateTime!=null &&
              this.startDateTime.equals(other.getStartDateTime()))) &&
            ((this.endDateTime==null && other.getEndDateTime()==null) || 
             (this.endDateTime!=null &&
              this.endDateTime.equals(other.getEndDateTime()))) &&
            ((this.maxResults==null && other.getMaxResults()==null) || 
             (this.maxResults!=null &&
              this.maxResults.equals(other.getMaxResults())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStartDateTime() != null) {
            _hashCode += getStartDateTime().hashCode();
        }
        if (getEndDateTime() != null) {
            _hashCode += getEndDateTime().hashCode();
        }
        if (getMaxResults() != null) {
            _hashCode += getMaxResults().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ startDateTime=").append(getStartDateTime());
		buffer.append(" endDateTime=").append(getEndDateTime());
		buffer.append(" maxResults=").append(getMaxResults());
		buffer.append(" ]");
		return buffer.toString();
	}
	
}
