package com.moneygram.mgo.service.consumer.gcpservice.proxy;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.gcp.service_v1.GCPProxy;
import com.moneygram.gcp.service_v1.GetConsumerProfileIDRequest;
import com.moneygram.gcp.service_v1.GetConsumerProfileIDResponse;
import com.moneygram.gcp.service_v1.ServiceAction;
import com.moneygram.mgo.service.consumer.gcpservice.exception.ProxyException;
import com.moneygram.mgo.service.consumer.util.MGOConsumerServiceResourceConfig;



public class GCPServiceProxyImpl
        implements GCPServiceProxy {

	private static final Logger log = LogFactory.getInstance().getLogger(GCPServiceProxyImpl.class);
	
	public static MGOConsumerServiceResourceConfig getResourceConfig() {
		return MGOConsumerServiceResourceConfig.getInstance();
	}
	
    private GCPProxy gcpServiceClient;
    
    public GCPServiceProxyImpl(){
    	try{  
    		String endpoint=getResourceConfig().getGCPServiceUrl();
    		gcpServiceClient=new GCPProxy(endpoint);
    		log.debug("GCP Service url: "+endpoint);
    	} catch (Exception ex) {
            log.error("Error in GCPServiceProxyImpl: " + ex.getMessage(), ex);
        }
    }
    
 
    
    
    public long getConsumerProfileID() throws ProxyException {
        long gcpId = -1;
        try {
            GetConsumerProfileIDRequest request = new GetConsumerProfileIDRequest();
            Header header = new Header();
            ProcessingInstruction processingInstr = new ProcessingInstruction();
            processingInstr.setAction("GetConsumerProfileID");
            header.setProcessingInstruction(processingInstr);
            request.setHeader(header);
            GetConsumerProfileIDResponse response = gcpServiceClient.getConsumerProfileID(request);
            gcpId = response.getProfileId();
            log.debug("GCP consumer profile id: " + gcpId);
        } catch (Exception ex) {
            log.error("Error in getConsumerProfileID: " + ex.getMessage(), ex);
            throw new ProxyException("Failed to obtain GCP consumer profile id", ex);
        }
        return gcpId;
    }
    
 
}
