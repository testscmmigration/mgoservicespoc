/*
 * Created on Oct 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.consumer.BankAccount;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.CreditCard;
import com.moneygram.mgo.service.consumer.FIAccount;
import com.moneygram.mgo.service.consumer.FIAccountType;

/**
 *
 * Consumer Accounts Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/10/22 02:46:33 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class ConsumerAccountsRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int index) throws SQLException {
        ConsumerFIAccount consumerAccount = new ConsumerFIAccount();

        FIAccountType type = FIAccountType.fromValue(rs.getString("ACCT_TYPE_CODE"));

        if (type.getValue().startsWith("CC")) {
            CreditCard acct = new CreditCard();
            setupFIAccount(acct, rs);

            acct.setExpMonth(new Integer(rs.getInt("CRCD_EXP_MTH_NBR")));
            acct.setExpYear(new Integer(rs.getInt("CRCD_EXP_YR_NBR")));
            acct.setBinBlocked(DAOUtils.toBoolean(rs.getString("CRCD_BIN_BLKD_CODE"), "B", null));
            acct.setCardType(rs.getString("CRCD_TYPE_CODE"));
            consumerAccount.setCreditCard(acct);

        } else if (type.getValue().startsWith("BANK")) {
            BankAccount acct = new BankAccount();
            setupFIAccount(acct, rs);

            acct.setAbaNumber(rs.getString("BANK_ABA_NBR"));
            acct.setFiName(rs.getString("FIN_INSTN_NAME"));
            acct.setAbaBlocked(DAOUtils.toBoolean(rs.getString("BANK_ABA_BLKD_CODE"), "B", null));

            consumerAccount.setBankAccount(acct);
        }

        return consumerAccount;
    }

    private void setupFIAccount(FIAccount acct, ResultSet rs) throws SQLException {
        acct.setAccountType(FIAccountType.fromValue(rs.getString("ACCT_TYPE_CODE")));
        acct.setAccountId(rs.getLong("CUST_ACCT_ID"));
        acct.setAccountNumber(rs.getString("ACCT_MASK_NBR"));
        acct.setEncryptedAccountNumber(rs.getString("ACCT_ENCRYP_NBR"));
        acct.setAccountStatus(rs.getString("ACCT_STAT_CODE"));
        acct.setAccountSubStatus(rs.getString("ACCT_SUB_STAT_CODE"));
        acct.setBlocked(DAOUtils.toBoolean(rs.getString("ACCT_BLKD_CODE"), "B", null));
    }

}
