/**
 * Access.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class Access implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String passwordHash;
	private java.lang.String ipAddress;
	private java.lang.String webServerName;
	private java.lang.String webServerIpAddress;
	private java.lang.Boolean ipAddressBlocked;
    private java.lang.Boolean logonSuccessful;

	public Access() {
	}

	/**
	 * Gets the passwordHash value for this Access.
	 * 
	 * @return passwordHash
	 */
	public java.lang.String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * Sets the passwordHash value for this Access.
	 * 
	 * @param passwordHash
	 */
	public void setPasswordHash(java.lang.String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * Gets the ipAddress value for this Access.
	 * 
	 * @return ipAddress
	 */
	public java.lang.String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the ipAddress value for this Access.
	 * 
	 * @param ipAddress
	 */
	public void setIpAddress(java.lang.String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * Gets the webServerName value for this Access.
	 * 
	 * @return webServerName
	 */
	public java.lang.String getWebServerName() {
		return webServerName;
	}

	/**
	 * Sets the webServerName value for this Access.
	 * 
	 * @param webServerName
	 */
	public void setWebServerName(java.lang.String webServerName) {
		this.webServerName = webServerName;
	}

	/**
	 * Gets the webServerIpAddress value for this Access.
	 * 
	 * @return webServerIpAddress
	 */
	public java.lang.String getWebServerIpAddress() {
		return webServerIpAddress;
	}

	/**
	 * Sets the webServerIpAddress value for this Access.
	 * 
	 * @param webServerIpAddress
	 */
	public void setWebServerIpAddress(java.lang.String webServerIpAddress) {
		this.webServerIpAddress = webServerIpAddress;
	}

	/**
	* Gets the ipAddressBlocked value for this Access.
	* 
	* @return ipAddressBlocked
	*/
	public java.lang.Boolean getIpAddressBlocked() {
		return ipAddressBlocked;
	}

	/**
	 * Sets the ipAddressBlocked value for this Access.
	 * 
	 * @param ipAddressBlocked
	 */
	public void setIpAddressBlocked(java.lang.Boolean ipAddressBlocked) {
		this.ipAddressBlocked = ipAddressBlocked;
	}


    /**
     * Gets the logonSuccessful value for this Access.
     * 
     * @return logonSuccessful
     */
    public java.lang.Boolean getLogonSuccessful() {
        return logonSuccessful;
    }


    /**
     * Sets the logonSuccessful value for this Access.
     * 
     * @param logonSuccessful
     */
    public void setLogonSuccessful(java.lang.Boolean logonSuccessful) {
        this.logonSuccessful = logonSuccessful;
    }


    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Access)) return false;
        Access other = (Access) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.passwordHash==null && other.getPasswordHash()==null) || 
             (this.passwordHash!=null &&
              this.passwordHash.equals(other.getPasswordHash()))) &&
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            ((this.webServerName==null && other.getWebServerName()==null) || 
             (this.webServerName!=null &&
              this.webServerName.equals(other.getWebServerName()))) &&
            ((this.webServerIpAddress==null && other.getWebServerIpAddress()==null) || 
             (this.webServerIpAddress!=null &&
              this.webServerIpAddress.equals(other.getWebServerIpAddress()))) &&
            ((this.ipAddressBlocked==null && other.getIpAddressBlocked()==null) || 
             (this.ipAddressBlocked!=null &&
              this.ipAddressBlocked.equals(other.getIpAddressBlocked()))) &&
            ((this.logonSuccessful==null && other.getLogonSuccessful()==null) || 
             (this.logonSuccessful!=null &&
              this.logonSuccessful.equals(other.getLogonSuccessful())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPasswordHash() != null) {
            _hashCode += getPasswordHash().hashCode();
        }
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        if (getWebServerName() != null) {
            _hashCode += getWebServerName().hashCode();
        }
        if (getWebServerIpAddress() != null) {
            _hashCode += getWebServerIpAddress().hashCode();
        }
        if (getIpAddressBlocked() != null) {
            _hashCode += getIpAddressBlocked().hashCode();
        }
        if (getLogonSuccessful() != null) {
            _hashCode += getLogonSuccessful().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ PasswordHash=").append(getPasswordHash());
		buffer.append(" IpAddress=").append(getIpAddress());
		buffer.append(" WebServerName=").append(getWebServerName());
		buffer.append(" WebServerIpAddress=").append(getWebServerIpAddress());
		buffer.append(" IpAddressBlocked=").append(getIpAddressBlocked());
		buffer.append(" LogonSuccessful=").append(getLogonSuccessful());
		buffer.append(" ]");
		return buffer.toString();
	}
}
