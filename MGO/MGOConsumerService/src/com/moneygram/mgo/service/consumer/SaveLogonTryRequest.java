/**
 * SaveLogonTryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class SaveLogonTryRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private static final long serialVersionUID = 6178006715930789329L;

    private java.lang.String loginId;

    private com.moneygram.mgo.service.consumer.Access access;

    private java.lang.String sourceSite;

    public SaveLogonTryRequest() {
    }

    public SaveLogonTryRequest(
           RequestHeader header,
           java.lang.String loginId,
           Access access,
           java.lang.String sourceSite) {
        super(
            header);
        this.loginId = loginId;
        this.access = access;
        this.sourceSite = sourceSite;
    }


    /**
     * Gets the loginId value for this SaveLogonTryRequest.
     * 
     * @return loginId
     */
    public java.lang.String getLoginId() {
        return loginId;
    }


    /**
     * Sets the loginId value for this SaveLogonTryRequest.
     * 
     * @param loginId
     */
    public void setLoginId(java.lang.String loginId) {
        this.loginId = loginId;
    }


    /**
     * Gets the access value for this SaveLogonTryRequest.
     * 
     * @return access
     */
    public Access getAccess() {
        return access;
    }


    /**
     * Sets the access value for this SaveLogonTryRequest.
     * 
     * @param access
     */
    public void setAccess(Access access) {
        this.access = access;
    }


    /**
     * Gets the sourceSite value for this SaveLogonTryRequest.
     * 
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this SaveLogonTryRequest.
     * 
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveLogonTryRequest)) return false;
        SaveLogonTryRequest other = (SaveLogonTryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.loginId==null && other.getLoginId()==null) || 
             (this.loginId!=null &&
              this.loginId.equals(other.getLoginId()))) &&
            ((this.access==null && other.getAccess()==null) || 
             (this.access!=null &&
              this.access.equals(other.getAccess()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) || 
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLoginId() != null) {
            _hashCode += getLoginId().hashCode();
        }
        if (getAccess() != null) {
            _hashCode += getAccess().hashCode();
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ loginId=").append(getLoginId());
		buffer.append(" access=").append(getAccess());
        buffer.append(" SourceSite=").append(getSourceSite());
		buffer.append(" ]");
		return buffer.toString();
	}

}
