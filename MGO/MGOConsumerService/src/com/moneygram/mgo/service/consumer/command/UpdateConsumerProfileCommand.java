/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer.Comment;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.PersonalInfo;
import com.moneygram.mgo.service.consumer.ProfileEvent;
import com.moneygram.mgo.service.consumer.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer.UpdateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer.UpdateConsumerProfileTask;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.shared.ErrorCodes;

public class UpdateConsumerProfileCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			UpdateConsumerProfileCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof UpdateConsumerProfileRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		UpdateConsumerProfileRequest profileRequest = (UpdateConsumerProfileRequest) request;
		Consumer consumer = profileRequest.getConsumer();
		Comment comment = profileRequest.getComment();

		
		if (logger.isDebugEnabled()) {
			logger.debug("process: update consumer profile consumer="
					+ consumer + " comment=" + comment + " updateTasks="
					+ printUpdateTasks(profileRequest.getUpdateTasks()));
		}

		if (consumer == null) {
			logger.debug("process: invalid input consumer");
			throw new DataFormatException("Invalid input consumer");
		}
		if (consumer.getConsumerId() == null && consumer.getLoginId() == null) {
			logger.debug("process: invalid input consumer id/login id");
			throw new DataFormatException("Invalid input consumer id/login id");
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
		UpdateConsumerProfileResponse response = new UpdateConsumerProfileResponse();
		boolean blocked = false;
		
		if (consumer.getInternal() != null && 
			consumer.getInternal().getConsumerBlocked() != null &&
			consumer.getInternal().getConsumerBlocked().booleanValue() == true) {
			blocked = true;
		}
		
		if (includedInUpdateTasks(UpdateConsumerProfileTask.LogAccess,
				profileRequest.getUpdateTasks())) {
			if (consumer.getLoginId() == null || consumer.getAccess() == null
					|| consumer.getAccess().getIpAddress() == null
					|| consumer.getAccess().getWebServerName() == null
					|| consumer.getAccess().getWebServerIpAddress() == null
				    || consumer.getAccess().getLogonSuccessful() == null) {
				throw new DataFormatException(
						"Invalid input consumer log access");
			}
			Long logonTryId;
			boolean isMMG = false;
			if ("MMG".equals(consumer.getAccess().getPasswordHash())) {
				isMMG = true;
			}
			try {
				logonTryId = dao.addLoginTry(consumer.getLoginId(), 
						consumer.getAccess().getIpAddress(), 
						consumer.getAccess().getWebServerName(), 
						consumer.getAccess().getWebServerIpAddress(),
						consumer.getAccess().getLogonSuccessful(),
						new Boolean(true), //valid MGO user since we have a profile
				        new Boolean(isMMG),
				        consumer.getSourceSite()); 
			} catch (Exception e) {
				throw new CommandException("Failed to log access", e);
			}
			
			try {			    
				dao.addBlockedAttempts (consumer, logonTryId);
			} catch (Exception e) {
				throw new CommandException("Failed to log logon_failures", e);
			}
			if (profileRequest.getUpdateTasks().length == 1)
				return response;
		}

		Consumer consumer2 = null;
		try {
			consumer2 = dao.getConsumer(consumer.getConsumerId(), consumer
					.getLoginId());
		} catch (Exception e) {
			logger.info("process: failed to find consumer", e);
			throw new CommandException("Failed to find consumer", e);
		}
		if (consumer2 == null)
			throw new BusinessRulesException(ErrorCodes.CONSUMER_NOT_FOUND,
					ErrorCodes.getMessage(ErrorCodes.CONSUMER_NOT_FOUND));

		if (consumer.getConsumerId() == null)
			consumer.setConsumerId(consumer2.getConsumerId());
		if (consumer.getLoginId() == null)
			consumer.setLoginId(consumer2.getLoginId());

		if (includedInUpdateTasks(UpdateConsumerProfileTask.SaveAgents,
				profileRequest.getUpdateTasks())) {
			try {
				dao.saveAgents(consumer);
			} catch (Exception e) {
				throw new CommandException("Failed to save agents", e);
			}
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateStatus,
				profileRequest.getUpdateTasks())) {
			if (consumer.getInternal() == null)
				throw new DataFormatException("Invalid input consumer internal");
			try {
				dao.updateConsumerStatus(
				        consumer.getConsumerId(), 
				        consumer.getInternal().getConsumerStatus(), 
				        consumer.getInternal().getConsumerSubStatus());
			} catch (Exception e) {
				throw new CommandException("Failed to update consumer status",
						e);
			}
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.AddProfileEvent,
				profileRequest.getUpdateTasks())) {
			ProfileEvent event = profileRequest.getProfileEvent();
			if (event == null)
				throw new DataFormatException("Invalid input event");
			try {
				dao.saveProfileEvent(consumer.getConsumerId(), event);
			} catch (Exception e) {
				throw new CommandException("Failed to add consumer profile event", e);
			}
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.AddComment,
				profileRequest.getUpdateTasks())) {
			if (comment == null)
				throw new DataFormatException("Invalid input comment");
			try {
				dao.addConsumerComment(consumer.getConsumerId(), comment);
			} catch (Exception e) {
				throw new CommandException("Failed to add consumer comment", e);
			}
		}
		
		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateContact,
				profileRequest.getUpdateTasks())) {
            if (consumer.getConsumerId() == null)
                throw new DataFormatException("Invalid consumerId provided");

		    if (consumer.getContact() == null)
				throw new DataFormatException("Invalid input consumer contact");
			
            if (consumer.getContact().getPrimaryPhone() != null) {
                //update primary phone
                try {
                	Long consumerId = consumer.getConsumerId();
                	String primaryPhone = consumer.getContact().getPrimaryPhone();
                	String primaryPhoneType = consumer.getContact().getPrimaryPhoneType();
                	if(!dao.isPhoneNumberBlocked(primaryPhone)){
                		blocked = dao.updatePrimaryPhone(consumerId, primaryPhone, primaryPhoneType);
                	}
                	else{
                		blocked = true;
                	}
                } catch (Exception e) {
                    throw new CommandException("Failed to update consumer primary phone", e);
                }
            }

            if (!"".equals(consumer.getContact().getAlternatePhone()) && consumer.getContact().getAlternatePhoneType() != null) {
            	//update alternate phone if the alternate phone is not null, otherwise remove the alternate phone
                try {
                	Long consumerId = consumer.getConsumerId();
                	String alternatePhone = consumer.getContact().getAlternatePhone();
                	String alternatePhoneType = consumer.getContact().getAlternatePhoneType();
                	boolean isBlockedPhone = alternatePhone==null ? false : dao.isPhoneNumberBlocked(alternatePhone);
                	if(!isBlockedPhone){
                		blocked = dao.updateAlternatePhone(consumerId, alternatePhone, alternatePhoneType);
                	}
                	else{
                		blocked = true;
                	}
                } catch (Exception e) {
                    throw new CommandException("Failed to update consumer alternate phone", e);
                }
            }
            
            if (consumer.getContact().getPromoEmail() != null) {
                //update promo email flag
                try {
                    dao.updateConsumerPromoEmailFlag(consumer.getConsumerId(), consumer.getContact().getPromoEmail().booleanValue(),consumer.getSourceSite());
                } catch (Exception e) {
                    throw new CommandException("Failed to update consumer promo email flag", e);
                }
            }
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateBlocked,
				profileRequest.getUpdateTasks())) {
	          throw new CommandException("UpdateBlocked Task not implemented");
		}
		
		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdatePersonal, profileRequest.getUpdateTasks())) {
            if (consumer.getPersonal() == null) {
                throw new DataFormatException("Personal information is missing");
            }

            if (consumer.getAddress() == null) {
                throw new DataFormatException("Address information is missing");
            }

            try {
                dao.updateConsumerAddress(consumer.getConsumerId(), consumer.getAddress());
            } catch (Exception e) {
                throw new CommandException("Failed to updated consumer address", e);
            }
        }

		if (includedInUpdateTasks(UpdateConsumerProfileTask.ChangePassword,
				profileRequest.getUpdateTasks())) {
			if (consumer.getAccess() == null
					|| consumer.getAccess().getPasswordHash() == null)
				throw new DataFormatException("Invalid input password hash");
			try {
				dao.addConsumerPasswordHash(consumer.getConsumerId(), consumer
						.getAccess().getPasswordHash());
			} catch (Exception e) {
				throw new CommandException("Failed to add password hash", e);
			}
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateSecurity,
				profileRequest.getUpdateTasks())) {
			if (consumer.getInternal() == null
					|| (consumer.getInternal().getSecurityQuestionsCollectionDate() == null))
				throw new DataFormatException(
						"Invalid input consumer security info");
			try {
                dao.updateConsumerSecurityQuestionsCollectionDate(consumer.getConsumerId(), consumer.getInternal().getSecurityQuestionsCollectionDate().getTime());
			} catch (Exception e) {
				throw new CommandException(
						"Failed to update consumer security info", e);
			}
		}

		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateLoyalty,
				profileRequest.getUpdateTasks())) {
			if (consumer.getLoyalty() == null
					|| (consumer.getLoyalty().getAutoEnroll() == null && consumer
							.getLoyalty().getMemberId() == null))
				throw new DataFormatException("Invalid input loyalty info");
			try {
				dao.updateConsumerLoyalty(consumer.getConsumerId(), consumer
						.getLoyalty().getAutoEnroll(), consumer.getLoyalty()
						.getMemberId());
			} catch (Exception e) {
				throw new CommandException("Failed to update loyalty info", e);
			}
		}
		
		//MGO-5169
		if (includedInUpdateTasks(UpdateConsumerProfileTask.UpdateName,
				profileRequest.getUpdateTasks())) {
			PersonalInfo personalInfo = consumer.getPersonal();
			
			if (personalInfo == null) {
                throw new DataFormatException("Personal information is missing");
            }
			
            //lengh validation
			String nameLength = "" + personalInfo.getFirstName() + personalInfo.getLastName() + personalInfo.getMiddleName() + personalInfo.getSecondLastName();
			int maxLength = nameLength.length(); //replace with the real lenght
            if (nameLength.length() > maxLength) {
                throw new DataFormatException("The max name length is " + maxLength);
            }
            
            if (consumer.getPersonal() == null) {
                throw new DataFormatException("Personal information is missing");
            }
			try {
				dao.updateName(consumer.getConsumerId(), consumer.getPersonal());
			} catch (Exception e) {
				throw new CommandException("Failed to update loyalty info", e);
			}
		}

        response.setConsumerBlocked(Boolean.valueOf(blocked));
		return response;
	}

	private String printUpdateTasks(UpdateConsumerProfileTask[] updateTasks) {
		if (updateTasks == null || updateTasks.length == 0)
			return "";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < updateTasks.length; i++) {
			if (updateTasks[i] == null)
				continue;
			if (sb.length() > 0)
				sb.append(",");
			sb.append(updateTasks[i].getValue());
		}
		return sb.toString();
	}

	private boolean includedInUpdateTasks(UpdateConsumerProfileTask updateTask,
			UpdateConsumerProfileTask[] updateTasks) {
		if (updateTasks == null || updateTasks.length == 0
				|| updateTask == null)
			return false;
		for (int i = 0; i < updateTasks.length; i++) {
			if (updateTasks[i] == null || updateTasks[i].getValue() == null)
				continue;
			if (updateTasks[i].getValue().equals(updateTask.getValue()))
				return true;
		}
		return false;
	}
}
