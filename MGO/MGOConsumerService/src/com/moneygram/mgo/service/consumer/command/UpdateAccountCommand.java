/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer.Comment;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.UpdateAccountRequest;
import com.moneygram.mgo.service.consumer.UpdateAccountResponse;
import com.moneygram.mgo.service.consumer.UpdateAccountTask;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer.util.ConsumerUtil;
import com.moneygram.mgo.shared.ErrorCodes;

public class UpdateAccountCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			UpdateAccountCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof UpdateAccountRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		UpdateAccountRequest profileRequest = (UpdateAccountRequest) request;
		ConsumerFIAccount account = profileRequest.getConsumerFIAccount();
		Comment comment = profileRequest.getComment();

		if (logger.isDebugEnabled()) {
			logger.debug("process: update account account=" + account
					+ " comment=" + comment + " updateTasks="
					+ printUpdateTasks(profileRequest.getUpdateTasks()));
		}

		if (account == null) {
			logger.debug("process: invalid input account");
			throw new DataFormatException("Invalid input account");
		}

		Long accountId = ConsumerUtil.extractAccountId(account);
		if (accountId == null) {
			logger.debug("process: invalid input account id");
			throw new DataFormatException("Invalid input account id");
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
		
		List<ConsumerFIAccount> accts = null;
		try {
			accts = dao.getConsumerAccounts(null, accountId, null);
		} catch (Exception e) {
			logger.info("process: failed to find account", e);
			throw new CommandException("Failed to find account", e);
		}
		if (accts == null || accts.size() != 1)
			throw new BusinessRulesException(ErrorCodes.ACCOUNT_NOT_FOUND,
					ErrorCodes.getMessage(ErrorCodes.ACCOUNT_NOT_FOUND));

		if (includedInUpdateTasks(UpdateAccountTask.AddComment, profileRequest
				.getUpdateTasks())) {
			if (comment == null)
				throw new DataFormatException("Invalid input comment");
			try {
				dao.addAccountComment(accountId, comment);
			} catch (Exception e) {
				logger.warn("process: failed to add account comment", e);
				throw new CommandException("Failed to add account comment", e);
			}
		} 
		
		if (includedInUpdateTasks(UpdateAccountTask.UpdateCardType, profileRequest.getUpdateTasks())) {
			String cardType = profileRequest.getConsumerFIAccount().getCreditCard().getCardType();
			if (cardType == null)
				throw new DataFormatException("Invalid input (cardType)");
			try {
				dao.updateCreditCardType(accountId, cardType);
			} catch (Exception e) {
				logger.warn("process: failed to updateCreditCardType", e);
				throw new CommandException("Failed to updateCreditCardType", e);
			}
		}

		int result;
		if (includedInUpdateTasks(UpdateAccountTask.UpdateStatus,
				profileRequest.getUpdateTasks())) {
			String status = null, subStatus = null;
			if (account.getBankAccount() != null
					&& account.getBankAccount().getAccountId() != null) {
				status = account.getBankAccount().getAccountStatus();
				subStatus = account.getBankAccount().getAccountSubStatus();
			} else if (account.getCreditCard() != null
					&& account.getCreditCard().getAccountId() != null) {
				status = account.getCreditCard().getAccountStatus();
				subStatus = account.getCreditCard().getAccountSubStatus();
			}
			try {
				dao.updateAccountStatus(accountId, status, subStatus);
			} catch (Exception e) {
				throw new CommandException("Failed to update account status", e);
			}
		}

		if (includedInUpdateTasks(UpdateAccountTask.UpdateBlocked,
				profileRequest.getUpdateTasks())) {
		    throw new DataFormatException("UpdateBlocked task is not implemented");
		}

		UpdateAccountResponse response = new UpdateAccountResponse();
		return response;
	}

	private String printUpdateTasks(UpdateAccountTask[] updateTasks) {
		if (updateTasks == null || updateTasks.length == 0)
			return "";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < updateTasks.length; i++) {
			if (updateTasks[i] == null)
				continue;
			if (sb.length() > 0)
				sb.append(",");
			sb.append(updateTasks[i].getValue());
		}
		return sb.toString();
	}

	private boolean includedInUpdateTasks(UpdateAccountTask updateTask,
			UpdateAccountTask[] updateTasks) {
		if (updateTasks == null || updateTasks.length == 0
				|| updateTask == null)
			return false;
		for (int i = 0; i < updateTasks.length; i++) {
			if (updateTasks[i] == null || updateTasks[i].getValue() == null)
				continue;
			if (updateTasks[i].getValue().equals(updateTask.getValue()))
				return true;
		}
		return false;
	}
}
