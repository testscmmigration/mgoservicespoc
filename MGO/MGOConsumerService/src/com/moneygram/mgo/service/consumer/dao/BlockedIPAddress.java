package com.moneygram.mgo.service.consumer.dao;

public class BlockedIPAddress implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String ipAddress;
	private String statusCode;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
