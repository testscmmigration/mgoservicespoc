package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.util.StringUtility;

public class BlockedPhoneNumberRowMapper extends BaseRowMapper {
	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Map<String,String> blockedPhoneMap = new HashMap<String,String>();
		String blkdPhoneNumber = rs.getString("BLKD_PH_NBR");
		String blkdPhoneStatus = rs.getString("BLKD_STAT_CODE");
		if(!StringUtility.isNullOrEmpty(blkdPhoneNumber) && !StringUtility.isNullOrEmpty(blkdPhoneStatus)){
			blockedPhoneMap.put(blkdPhoneNumber, blkdPhoneStatus);
		}
		return blockedPhoneMap;
	}
}
