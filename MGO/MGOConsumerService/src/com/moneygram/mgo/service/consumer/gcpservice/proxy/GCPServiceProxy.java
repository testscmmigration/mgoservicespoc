package com.moneygram.mgo.service.consumer.gcpservice.proxy;

import com.moneygram.mgo.service.consumer.gcpservice.exception.ProxyException;



public interface GCPServiceProxy {

    public long getConsumerProfileID() throws ProxyException;

}
