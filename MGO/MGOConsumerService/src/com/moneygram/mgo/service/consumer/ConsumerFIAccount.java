/**
 * ConsumerFIAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class ConsumerFIAccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private CreditCard creditCard;
	private BankAccount bankAccount;

	public ConsumerFIAccount() {
	}

	/**
	 * Gets the creditCard value for this ConsumerFIAccount.
	 * 
	 * @return creditCard
	 */
	public CreditCard getCreditCard() {
		return creditCard;
	}

	/**
	 * Sets the creditCard value for this ConsumerFIAccount.
	 * 
	 * @param creditCard
	 */
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	/**
	 * Gets the bankAccount value for this ConsumerFIAccount.
	 * 
	 * @return bankAccount
	 */
	public BankAccount getBankAccount() {
		return bankAccount;
	}

	/**
	 * Sets the bankAccount value for this ConsumerFIAccount.
	 * 
	 * @param bankAccount
	 */
	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ConsumerFIAccount))
			return false;
		ConsumerFIAccount other = (ConsumerFIAccount) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.creditCard == null && other.getCreditCard() == null) || (this.creditCard != null && this.creditCard
						.equals(other.getCreditCard())))
				&& ((this.bankAccount == null && other.getBankAccount() == null) || (this.bankAccount != null && this.bankAccount
						.equals(other.getBankAccount())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getCreditCard() != null) {
			_hashCode += getCreditCard().hashCode();
		}
		if (getBankAccount() != null) {
			_hashCode += getBankAccount().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ CreditCard=").append(getCreditCard());
		buffer.append(" BankAccount=").append(getBankAccount());
		buffer.append(" ]");
		return buffer.toString();
	}
}
