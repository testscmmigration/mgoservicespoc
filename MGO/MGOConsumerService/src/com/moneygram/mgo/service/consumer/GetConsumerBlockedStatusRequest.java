/**
 * GetConsumerBlockedStatusRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class GetConsumerBlockedStatusRequest extends BaseOperationRequest {

	private static final long serialVersionUID = 1L;
	private Consumer consumer;

	public GetConsumerBlockedStatusRequest() {
	}

	/**
	 * Gets the consumer value for this GetConsumerBlockedStatusRequest.
	 * 
	 * @return consumer
	 */
	public Consumer getConsumer() {
		return consumer;
	}

	/**
	 * Sets the consumer value for this GetConsumerBlockedStatusRequest.
	 * 
	 * @param consumer
	 */
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof GetConsumerBlockedStatusRequest))
			return false;
		GetConsumerBlockedStatusRequest other = (GetConsumerBlockedStatusRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.consumer == null && other.getConsumer() == null) || (this.consumer != null && this.consumer
						.equals(other.getConsumer())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getConsumer() != null) {
			_hashCode += getConsumer().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ Consumer=").append(getConsumer());
		buffer.append(" ]");
		return buffer.toString();
	}
}
