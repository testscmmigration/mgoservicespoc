/**
 * SavedAgent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class SavedAgent implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private long agentId;

	public SavedAgent() {
	}

	public SavedAgent(long agentId) {
		this.agentId = agentId;
	}

	/**
	 * Gets the agentId value for this SavedAgent.
	 * 
	 * @return agentId
	 */
	public long getAgentId() {
		return agentId;
	}

	/**
	 * Sets the agentId value for this SavedAgent.
	 * 
	 * @param agentId
	 */
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SavedAgent)) {
			return false;
		}
		SavedAgent other = (SavedAgent) o;

		return getAgentId() == other.getAgentId();
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ AgentId=").append(getAgentId());
		buffer.append(" ]");
		return buffer.toString();
	}
}
