/**
 * GetIncompleteProfilesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationResponse;

public class GetIncompleteProfilesResponse  extends BaseOperationResponse  implements java.io.Serializable {
    private long[] consumerId;

    public GetIncompleteProfilesResponse() {
    }

    /**
     * Gets the consumerId value for this GetIncompleteProfilesResponse.
     * 
     * @return consumerId
     */
    public long[] getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this GetIncompleteProfilesResponse.
     * 
     * @param consumerId
     */
    public void setConsumerId(long[] consumerId) {
        this.consumerId = consumerId;
    }

    public long getConsumerId(int i) {
        return this.consumerId[i];
    }

    public void setConsumerId(int i, long _value) {
        this.consumerId[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIncompleteProfilesResponse)) return false;
        GetIncompleteProfilesResponse other = (GetIncompleteProfilesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              java.util.Arrays.equals(this.consumerId, other.getConsumerId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsumerId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsumerId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[");
		for (int i = 0; i < consumerId.length; i++) {
			buffer.append(" consumerId(" + i + ")=").append(consumerId[i]);
		}
		buffer.append(" ]");
		return buffer.toString();
	}
	
}
