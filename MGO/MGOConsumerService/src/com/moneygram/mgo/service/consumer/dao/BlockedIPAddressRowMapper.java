package com.moneygram.mgo.service.consumer.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;

public class BlockedIPAddressRowMapper extends BaseRowMapper {
	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		BlockedIPAddress bipa = new BlockedIPAddress();
		bipa.setIpAddress(rs.getString("BLKD_IP_ADDR_TEXT"));
		bipa.setStatusCode(rs.getString("BLKD_STAT_CODE"));
		return bipa;
	}
}
