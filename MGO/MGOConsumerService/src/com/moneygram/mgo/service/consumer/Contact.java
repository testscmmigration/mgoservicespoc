/**
 * Contact.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

public class Contact implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private java.lang.String primaryPhone;
	private String primaryPhoneType;
	private java.lang.String alternatePhone;
	private String alternatePhoneType;
	private java.lang.String email;
	private java.lang.Boolean promoEmail;

	public Contact() {
	}

    public Contact(
           java.lang.String primaryPhone,
           java.lang.String primaryPhoneType,
           java.lang.String alternatePhone,
           java.lang.String alternatePhoneType,
           java.lang.String email,
           java.lang.Boolean promoEmail) {
           this.primaryPhone = primaryPhone;
           this.primaryPhoneType = primaryPhoneType;
           this.alternatePhone = alternatePhone;
           this.alternatePhoneType = alternatePhoneType;
           this.email = email;
           this.promoEmail = promoEmail;
    }


	/**
	 * Gets the primaryPhone value for this Contact.
	 *
	 * @return primaryPhone
	 */
	public java.lang.String getPrimaryPhone() {
		return primaryPhone;
	}

	/**
	 * Sets the primaryPhone value for this Contact.
	 *
	 * @param primaryPhone
	 */
	public void setPrimaryPhone(java.lang.String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	/**
	 * Gets the alternatePhone value for this Contact.
	 *
	 * @return alternatePhone
	 */
	public java.lang.String getAlternatePhone() {
		return alternatePhone;
	}

	/**
	 * Sets the alternatePhone value for this Contact.
	 *
	 * @param alternatePhone
	 */
	public void setAlternatePhone(java.lang.String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}

	public void setPrimaryPhoneType(String primaryPhoneType) {
		this.primaryPhoneType = primaryPhoneType;
	}

	public String getPrimaryPhoneType() {
		return primaryPhoneType;
	}

	public void setAlternatePhoneType(String alternatePhoneType) {
		this.alternatePhoneType = alternatePhoneType;
	}

	public String getAlternatePhoneType() {
		return alternatePhoneType;
	}

	/**
	 * Gets the email value for this Contact.
	 *
	 * @return email
	 */
	public java.lang.String getEmail() {
		return email;
	}

	/**
	 * Sets the email value for this Contact.
	 *
	 * @param email
	 */
	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	/**
	 * Gets the promoEmail value for this Contact.
	 *
	 * @return promoEmail
	 */
	public java.lang.Boolean getPromoEmail() {
		return promoEmail;
	}

	/**
	 * Sets the promoEmail value for this Contact.
	 *
	 * @param promoEmail
	 */
	public void setPromoEmail(java.lang.Boolean promoEmail) {
		this.promoEmail = promoEmail;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Contact))
			return false;
		Contact other = (Contact) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
        _equals = true && 
            ((this.primaryPhone==null && other.getPrimaryPhone()==null) || 
             (this.primaryPhone!=null &&
              this.primaryPhone.equals(other.getPrimaryPhone()))) &&
            ((this.primaryPhoneType==null && other.getPrimaryPhoneType()==null) || 
             (this.primaryPhoneType!=null &&
              this.primaryPhoneType.equals(other.getPrimaryPhoneType()))) &&
            ((this.alternatePhone==null && other.getAlternatePhone()==null) || 
             (this.alternatePhone!=null &&
              this.alternatePhone.equals(other.getAlternatePhone()))) &&
            ((this.alternatePhoneType==null && other.getAlternatePhoneType()==null) || 
             (this.alternatePhoneType!=null &&
              this.alternatePhoneType.equals(other.getAlternatePhoneType()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.promoEmail==null && other.getPromoEmail()==null) || 
             (this.promoEmail!=null &&
              this.promoEmail.equals(other.getPromoEmail())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getPrimaryPhone() != null) {
			_hashCode += getPrimaryPhone().hashCode();
		}
		if (getPrimaryPhoneType() != null) {
			_hashCode += getPrimaryPhoneType().hashCode();
		}
		if (getAlternatePhone() != null) {
			_hashCode += getAlternatePhone().hashCode();
		}
		if (getAlternatePhoneType() != null) {
			_hashCode += getAlternatePhoneType().hashCode();
		}
		if (getEmail() != null) {
			_hashCode += getEmail().hashCode();
		}
		if (getPromoEmail() != null) {
			_hashCode += getPromoEmail().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ PrimaryPhone=").append(getPrimaryPhone());
		buffer.append("[ PrimaryPhoneType=").append(getPrimaryPhoneType());
		buffer.append(" AlternatePhone=").append(getAlternatePhone());
		buffer.append("[ PrimaryPhoneType=").append(getPrimaryPhoneType());
		buffer.append(" Email=").append(getEmail());
		buffer.append(" PromoEmail=").append(getPromoEmail());
		buffer.append(" ]");
		return buffer.toString();
	}
}
