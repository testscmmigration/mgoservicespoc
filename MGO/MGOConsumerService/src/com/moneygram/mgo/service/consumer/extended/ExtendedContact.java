package com.moneygram.mgo.service.consumer.extended;

import com.moneygram.mgo.service.consumer.Contact;

public class ExtendedContact extends Contact{

	private static final long serialVersionUID = 1L;
	private Boolean primaryPhoneBlocked;
	private Boolean alternatePhoneBlocked;
	private Boolean emailBlocked;
	private Boolean emailDomainBlocked;

	
	public Boolean getPrimaryPhoneBlocked() {
		return primaryPhoneBlocked;
	}
	public void setPrimaryPhoneBlocked(Boolean primaryPhoneBlocked) {
		this.primaryPhoneBlocked = primaryPhoneBlocked;
	}
	public Boolean getAlternatePhoneBlocked() {
		return alternatePhoneBlocked;
	}
	public void setAlternatePhoneBlocked(Boolean alternatePhoneBlocked) {
		this.alternatePhoneBlocked = alternatePhoneBlocked;
	}
	public Boolean getEmailBlocked() {
		return emailBlocked;
	}
	public void setEmailBlocked(Boolean emailBlocked) {
		this.emailBlocked = emailBlocked;
	}
	public Boolean getEmailDomainBlocked() {
		return emailDomainBlocked;
	}
	public void setEmailDomainBlocked(Boolean emailDomainBlocked) {
		this.emailDomainBlocked = emailDomainBlocked;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}
