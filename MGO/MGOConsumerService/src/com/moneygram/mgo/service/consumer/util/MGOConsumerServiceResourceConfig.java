/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.mgo.service.consumer.util;

import com.moneygram.common.service.util.ResourceConfig;

/**
 * 
 * MGO Consumer Service Resource Config. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOConsumerService</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.3 $ $Date: 2012/03/30 20:48:20 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: vi79 $ </td>
 *        </table>
 *        </div>
 */
public class MGOConsumerServiceResourceConfig extends ResourceConfig {

	public static final String SECURE_HASH_SEED = "SecureHashSeed_password";
	public static final String PUBLIC_KEY_FILE = "PublicKeyFile";
	public static final String PCI_ENCRYPT_URL = "PCIEncryptUrl";
	public static final String PCI_ENCRYPT_TIMEOUT = "PCIEncryptTimeout";

	public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOConsumerServiceResourceReference";

	//Begin : vl58 - S8 (New Architecture) new properties for GCP Service Calling
	public static final String GCP_SERVICE_CALL_RETRY_TIMES = "GCP_SERVICE_CALL_RETRY_TIMES";
	public static final String GCP_SERVICE_CALL_RETRY_WAIT_TIME = "GCP_SERVICE_CALL_RETRY_WAIT_TIME"; // in seconds
	public static final String GCP_SERVICE_URL="GCP_SERVICE_URL";
	//End : vl58 - S8 (New Architecture) new properties for GCP Service Calling
	
	public static final String PCI_DECRYPT_URL= "PCI_DECRYPT_URL";
	public static final int PCIDECRYPT_SERVICE_TIMEOUT = 15000;
	public static final String PCIDECRYPT_APPLICATION_ID = "MGO";
	public static final String PCIDECRYPT_USER="PCIDECRYPT_USER";
	public static final String PCIDECRYPT_PASS="PCIDECRYPT_PASS";

	/**
	 * singleton instance.
	 */
	private static MGOConsumerServiceResourceConfig instance = null;

	/**
	 * Creates new instance of ResourceConfig
	 * 
	 */
	private MGOConsumerServiceResourceConfig() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static MGOConsumerServiceResourceConfig getInstance() {
		if (instance == null) {
			synchronized (MGOConsumerServiceResourceConfig.class) {
				if (instance == null) {
					instance = new MGOConsumerServiceResourceConfig();
					instance.initResourceConfig();
				}
			}
		}
		return instance;
	}

	public String getSecureHashSeed() {
		return getAttributeValue(SECURE_HASH_SEED);
	}

	public String getPublicKeyFile() {
		return getAttributeValue(PUBLIC_KEY_FILE);
	}

	public String getPCIEncryptUrl() {
		return getAttributeValue(PCI_ENCRYPT_URL);
	}

	public int getPCIEncryptTimeout() {
		return getIntegerAttributeValue(PCI_ENCRYPT_TIMEOUT, "5000");
	}
	
	public String getPCIDecryptUrl() {
		return getAttributeValue(PCI_DECRYPT_URL);

	}
	
	public String getPCIDecryptUser() {
		return getAttributeValue(PCIDECRYPT_USER);
	}
	
	public String getPCIDecryptPass() {
		return getAttributeValue(PCIDECRYPT_PASS);
	}

	@Override
	protected String getResourceConfigurationJndiName() {
		return RESOURCE_REFERENCE_JNDI;
	}

	public int getGCPServiceCallRetryTimes(){
		return getIntegerAttributeValue(GCP_SERVICE_CALL_RETRY_TIMES, 3);
	}
	
	public int getGCPServiceCallRetryWaitPeriod(){
		return getIntegerAttributeValue(GCP_SERVICE_CALL_RETRY_WAIT_TIME, 2);
	}
	
	public String getGCPServiceUrl(){
		return getAttributeValue(GCP_SERVICE_URL);
	}
}
