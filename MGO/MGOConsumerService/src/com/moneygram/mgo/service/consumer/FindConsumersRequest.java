/**
 * FindConsumersRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationRequest;

public class FindConsumersRequest extends BaseOperationRequest {

	private static final long serialVersionUID = 1L;
	private java.lang.String ssnMask;
	private java.util.Calendar dateOfBirth;
	private java.lang.String lastName;
	private java.lang.String addressLine1StartsWith;

	public FindConsumersRequest() {
	}

	/**
	 * Gets the ssnMask value for this FindConsumersRequest.
	 * 
	 * @return ssnMask
	 */
	public java.lang.String getSsnMask() {
		return ssnMask;
	}

	/**
	 * Sets the ssnMask value for this FindConsumersRequest.
	 * 
	 * @param ssnMask
	 */
	public void setSsnMask(java.lang.String ssnMask) {
		this.ssnMask = ssnMask;
	}

	/**
	 * Gets the dateOfBirth value for this FindConsumersRequest.
	 * 
	 * @return dateOfBirth
	 */
	public java.util.Calendar getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets the dateOfBirth value for this FindConsumersRequest.
	 * 
	 * @param dateOfBirth
	 */
	public void setDateOfBirth(java.util.Calendar dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Gets the lastName value for this FindConsumersRequest.
	 * 
	 * @return lastName
	 */
	public java.lang.String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName value for this FindConsumersRequest.
	 * 
	 * @param lastName
	 */
	public void setLastName(java.lang.String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the addressLine1StartsWith value for this FindConsumersRequest.
	 * 
	 * @return addressLine1StartsWith
	 */
	public java.lang.String getAddressLine1StartsWith() {
		return addressLine1StartsWith;
	}

	/**
	 * Sets the addressLine1StartsWith value for this FindConsumersRequest.
	 * 
	 * @param addressLine1StartsWith
	 */
	public void setAddressLine1StartsWith(
			java.lang.String addressLine1StartsWith) {
		this.addressLine1StartsWith = addressLine1StartsWith;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof FindConsumersRequest))
			return false;
		FindConsumersRequest other = (FindConsumersRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = super.equals(obj)
				&& ((this.ssnMask == null && other.getSsnMask() == null) || (this.ssnMask != null && this.ssnMask
						.equals(other.getSsnMask())))
				&& ((this.dateOfBirth == null && other.getDateOfBirth() == null) || (this.dateOfBirth != null && this.dateOfBirth
						.equals(other.getDateOfBirth())))
				&& ((this.lastName == null && other.getLastName() == null) || (this.lastName != null && this.lastName
						.equals(other.getLastName())))
				&& ((this.addressLine1StartsWith == null && other
						.getAddressLine1StartsWith() == null) || (this.addressLine1StartsWith != null && this.addressLine1StartsWith
						.equals(other.getAddressLine1StartsWith())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = super.hashCode();
		if (getSsnMask() != null) {
			_hashCode += getSsnMask().hashCode();
		}
		if (getDateOfBirth() != null) {
			_hashCode += getDateOfBirth().hashCode();
		}
		if (getLastName() != null) {
			_hashCode += getLastName().hashCode();
		}
		if (getAddressLine1StartsWith() != null) {
			_hashCode += getAddressLine1StartsWith().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ SsnMask=").append(getSsnMask());
		buffer.append(" DateOfBirth=").append(getDateOfBirth());
		buffer.append(" LastName=").append(getLastName());
		buffer.append(" AddressLine1StartsWith=").append(
				getAddressLine1StartsWith());
		buffer.append(" ]");
		return buffer.toString();
	}
}
