/**
 * GetProfileEventsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationResponse;

public class GetProfileEventsResponse  extends BaseOperationResponse  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumer.ProfileEvent[] events;

    public GetProfileEventsResponse() {
    }

    /**
     * Gets the events value for this GetProfileEventsResponse.
     * 
     * @return events
     */
    public com.moneygram.mgo.service.consumer.ProfileEvent[] getEvents() {
        return events;
    }


    /**
     * Sets the events value for this GetProfileEventsResponse.
     * 
     * @param events
     */
    public void setEvents(com.moneygram.mgo.service.consumer.ProfileEvent[] events) {
        this.events = events;
    }

    public com.moneygram.mgo.service.consumer.ProfileEvent getEvents(int i) {
        return this.events[i];
    }

    public void setEvents(int i, com.moneygram.mgo.service.consumer.ProfileEvent _value) {
        this.events[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetProfileEventsResponse)) return false;
        GetProfileEventsResponse other = (GetProfileEventsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.events==null && other.getEvents()==null) || 
             (this.events!=null &&
              java.util.Arrays.equals(this.events, other.getEvents())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEvents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEvents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEvents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		if (events == null){
			return buffer.append("[ event=null]").toString();
		}
		buffer.append("[");
		for (int i = 0; i < events.length; i++) {
			buffer.append(" events(" + i + ")=").append(events[i]);
		}
		buffer.append(" ]");
		return buffer.toString();
	}
	
}
