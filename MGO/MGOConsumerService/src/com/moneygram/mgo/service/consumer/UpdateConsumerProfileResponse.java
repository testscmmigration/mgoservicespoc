package com.moneygram.mgo.service.consumer;

import com.moneygram.common.service.BaseOperationResponse;

public class UpdateConsumerProfileResponse extends BaseOperationResponse {

	private static final long serialVersionUID = 1L;
	private java.lang.Boolean consumerBlocked;
	
	public UpdateConsumerProfileResponse() {
	}
    /**
     * Gets the consumerBlocked value for this UpdateConsumerProfileResponse.
     * 
     * @return consumerBlocked
     */
    public java.lang.Boolean getConsumerBlocked() {
        return consumerBlocked;
    }


    /**
     * Sets the consumerBlocked value for this UpdateConsumerProfileResponse.
     * 
     * @param consumerBlocked
     */
    public void setConsumerBlocked(java.lang.Boolean consumerBlocked) {
        this.consumerBlocked = consumerBlocked;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateConsumerProfileResponse)) return false;
        UpdateConsumerProfileResponse other = (UpdateConsumerProfileResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerBlocked==null && other.getConsumerBlocked()==null) || 
             (this.consumerBlocked!=null &&
              this.consumerBlocked.equals(other.getConsumerBlocked())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerBlocked() != null) {
            _hashCode += getConsumerBlocked().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
    
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ ConsumerBlocked=").append(getConsumerBlocked());
		buffer.append(" ]");
		return buffer.toString();
	}    
}
