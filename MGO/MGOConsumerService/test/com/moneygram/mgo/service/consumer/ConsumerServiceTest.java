/*
 * Created on Jun 24, 2009
 *
 */
package com.moneygram.mgo.service.consumer;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;
import com.moneygram.mgo.service.consumer.util.MGOConsumerServiceResourceConfig;

public class ConsumerServiceTest extends BaseConsumerServiceTestCase {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerServiceTest.class);

	public void testMGOConsumerServiceResourceConfig() {
		MGOConsumerServiceResourceConfig config = MGOConsumerServiceResourceConfig
				.getInstance();
		assertNotNull("Expected not null config", config);

		String value = null;
		value = config.getPublicKeyFile();
		if (logger.isDebugEnabled()) {
			logger.debug("testMGOConsumerServiceResourceConfig: PublicKeyFile="
					+ value);
		}
		assertNotNull("Expected not null PublicKeyFile", value);

		value = config.getSecureHashSeed();
		if (logger.isDebugEnabled()) {
			logger
					.debug("testMGOConsumerServiceResourceConfig: SecureHashSeed="
							+ value);
		}
		assertNotNull("Expected not null SecureHashSeed", value);
	}

	public void xtestGetAgentsForConsumer() throws ServiceException {
		logger.info("\ntestGetAgentsForConsumer...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getConsumerProfile.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Long consumerId = generateConsumerIdForGettingAgents();
		assertNotNull("Expected not null consumerId", consumerId);
		String loginId = getConsumerLoginId(consumerId);
		assertNotNull("Expected not null loginId", loginId);

		GetConsumerProfileRequest request = new GetConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerLoginId(loginId);
		request
				.setResponseFilter(new ProfilePart[] { ProfilePart.TransactionPreferences });
		RequestRouter requestRouter = getRequestRouter();

		GetConsumerProfileResponse response = (GetConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumer", response.getConsumer());
		assertNotNull("Expected not null login id", response.getConsumer()
				.getLoginId());
		assertNotNull("Expected not null consumer transaction preferences",
				response.getConsumer().getTransactionPreferences());
		assertNotNull("Expected not null saved agents", response.getConsumer()
				.getTransactionPreferences().getSavedAgents());
	}

	public void xtestSaveAgentsForConsumer() throws ServiceException {
		logger.info("\ntestSaveAgentsForConsumer...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerIdForConsumerWithNoAgents();
		assertNotNull("Expected not null consumerId", consumerId);
		String loginId = getConsumerLoginId(consumerId);
		assertNotNull("Expected not null loginId", loginId);

		SavedAgent[] agents = generateSavedAgents();
		assertNotNull("Expected not null agents", agents);

		Consumer consumer = new Consumer();
		consumer.setLoginId(loginId);
		consumer.setConsumerId(consumerId);
		TransactionPreferences tp = new TransactionPreferences();
		tp.setSavedAgents(agents);
		consumer.setTransactionPreferences(tp);

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.SaveAgents });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestAddConsumerProfileWithPersonalAndContact()
			throws ServiceException {
		logger.info("\ntestAddConsumerProfileWithPersonalAndContact...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.createConsumerProfile.getValue());

		Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
		assertNotNull("Expected not null consumer1", consumer);

		CreateConsumerProfileRequest request = new CreateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);

		RequestRouter requestRouter = getRequestRouter();
		CreateConsumerProfileResponse response = (CreateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumer", response.getConsumer());
		assertNotNull("Expected not null consumer id", response.getConsumer()
				.getConsumerId());
	}

	public void xtestAddConsumerComment() throws ServiceException {
		logger.info("\ntestAddConsumerComment...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerIdForConsumerWithNoComments();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);

		Comment comment = generateNewConsumerComment();
		assertNotNull("Expected not null comment", comment);

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request.setComment(comment);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.AddComment });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestAddConsumerProfileEvent() throws ServiceException {
		logger.info("\ntestAddConsumerProfileEvent...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = getConsumerIdWithIncompleteProfile();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);

		ProfileEvent event = generateNewConsumerProfileEvent(null);
		assertNotNull("Expected not null event", event);

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request.setProfileEvent(event);
		request.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.AddProfileEvent });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}
	
	public void xtestAddAccountComment() throws ServiceException {
		logger.info("\ntestAddAccountComment...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerAccount.getValue());

		Long accountId = generateAccountIdForAccountWithNoComments();
		assertNotNull("Expected not null accountId", accountId);
		ConsumerFIAccount consumerFIAccount = new ConsumerFIAccount();
		consumerFIAccount.setCreditCard(new CreditCard());
		consumerFIAccount.getCreditCard().setAccountId(accountId);

		Comment comment = generateNewAccountComment();
		assertNotNull("Expected not null comment", comment);

		UpdateAccountRequest request = new UpdateAccountRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerFIAccount(consumerFIAccount);
		request.setComment(comment);
		request
				.setUpdateTasks(new UpdateAccountTask[] { UpdateAccountTask.AddComment });

		RequestRouter requestRouter = getRequestRouter();
		UpdateAccountResponse response = (UpdateAccountResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestAddCreditCardAccount() throws ServiceException {
		logger.info("\ntestAddCreditCardAccount...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.addConsumerAccount.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);

		CreditCard creditCard = generateNewCreditCardAccount();
		assertNotNull("Expected not null creditCard", creditCard);
		ConsumerFIAccount consumerFIAccount = new ConsumerFIAccount();
		consumerFIAccount.setCreditCard(creditCard);

		Long addressId = getConsumerAddressId(consumerId);
		assertNotNull("Expected not null addressId", addressId);

		String consumerName = getConsumerName(consumerId);
		assertNotNull("Expected not null consumerName", consumerName);

		AddAccountRequest request = new AddAccountRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerId(consumerId);
		request.setConsumerFIAccount(consumerFIAccount);

		RequestRouter requestRouter = getRequestRouter();
		AddAccountResponse response = (AddAccountResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumerFIAccount", response
				.getConsumerFIAccount());
		assertNotNull("Expected not null consumerFIAccount creditCard",
				response.getConsumerFIAccount().getCreditCard());
		assertNotNull("Expected not null consumerFIAccount creditCard id",
				response.getConsumerFIAccount().getCreditCard().getAccountId());
	}

	public void xtestAddBankAccount() throws ServiceException {
		logger.info("\ntestAddBankAccount...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.addConsumerAccount.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);

		BankAccount bankAccount = generateNewBankAccount();
		assertNotNull("Expected not null bankAccount", bankAccount);
		ConsumerFIAccount consumerFIAccount = new ConsumerFIAccount();
		consumerFIAccount.setBankAccount(bankAccount);

		Long addressId = getConsumerAddressId(consumerId);
		assertNotNull("Expected not null addressId", addressId);

		String consumerName = getConsumerName(consumerId);
		assertNotNull("Expected not null consumerName", consumerName);

		AddAccountRequest request = new AddAccountRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerId(consumerId);
		request.setConsumerFIAccount(consumerFIAccount);

		RequestRouter requestRouter = getRequestRouter();
		AddAccountResponse response = (AddAccountResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumerFIAccount", response
				.getConsumerFIAccount());
		assertNotNull("Expected not null consumerFIAccount bankAccount",
				response.getConsumerFIAccount().getBankAccount());
		assertNotNull("Expected not null consumerFIAccount bankAccount id",
				response.getConsumerFIAccount().getBankAccount().getAccountId());
	}

	public void xtestUpdateConsumerStatus() throws ServiceException {
		logger.info("\ntestUpdateConsumerStatus...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);
		consumer.setInternal(new MGOAttributes());
		consumer.getInternal().setConsumerStatus("NAT");
		consumer.getInternal().setConsumerSubStatus("USR");

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.UpdateStatus });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestGetConsumerBlockedStatus() throws ServiceException {
		logger.info("\ntestGetConsumerBlockedStatus...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getConsumerBlockedStatus.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Consumer consumer = generateNewConsumerProfileforGettingBlockedStatus();
		assertNotNull("Expected not null consumer", consumer);
		assertNotNull("Expected not null consumer contact", consumer
				.getContact());

		GetConsumerBlockedStatusRequest request = new GetConsumerBlockedStatusRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		RequestRouter requestRouter = getRequestRouter();

		GetConsumerBlockedStatusResponse response = (GetConsumerBlockedStatusResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumer", response.getConsumer());
		assertNotNull("Expected not null consumer internal", response
				.getConsumer().getInternal());
	}

	public void xtestAddConsumerPasswordHash() throws ServiceException {
		logger.info("\ntestAddConsumerPasswordHash...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);
		consumer.setAccess(new Access());
		consumer.getAccess().setPasswordHash("HashHashHash");

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.ChangePassword });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestUpdateConsumerLoyalty() throws ServiceException {
		logger.info("\ntestUpdateConsumerLoyalty...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);
		consumer.setLoyalty(new LoyaltyInfo());
		consumer.getLoyalty().setAutoEnroll(Boolean.TRUE);
		consumer.getLoyalty().setMemberId("123456789");

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.UpdateLoyalty });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestLogAccess() throws ServiceException {
		logger.info("\ntestLogAccess...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.updateConsumerProfile.getValue());

		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);
		Consumer consumer = new Consumer();
		consumer.setConsumerId(consumerId);
		consumer.setLoginId("eric@msn.com");
		consumer.setAccess(new Access());
		consumer.getAccess().setIpAddress("111.222.111.214");
		consumer.getAccess().setWebServerName("webServerName");
		consumer.getAccess().setWebServerIpAddress("22.33.44.55");
		consumer.getAccess().setLogonSuccessful(new Boolean(true));
		consumer.setInternal(new MGOAttributes());

		UpdateConsumerProfileRequest request = new UpdateConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		request
				.setUpdateTasks(new UpdateConsumerProfileTask[] { UpdateConsumerProfileTask.LogAccess });

		RequestRouter requestRouter = getRequestRouter();
		UpdateConsumerProfileResponse response = (UpdateConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
	}

	public void xtestCheckIpAddressBlocked() throws ServiceException {
		logger.info("\ntestCheckIpAddressBlocked...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getConsumerBlockedStatus.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Consumer consumer = new Consumer();
		consumer.setAccess(new Access());
		consumer.getAccess().setIpAddress("111.222.111.214");

		GetConsumerBlockedStatusRequest request = new GetConsumerBlockedStatusRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumer(consumer);
		RequestRouter requestRouter = getRequestRouter();

		GetConsumerBlockedStatusResponse response = (GetConsumerBlockedStatusResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumer", response.getConsumer());
		assertNotNull("Expected not null consumer access", response
				.getConsumer().getAccess());
	}

	public void xtestCheckForDuplicateConsumers() throws ServiceException {
		logger.info("\ntestCheckForDuplicateConsumers...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getConsumerProfile.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Long consumerId = generateConsumerIdWhoIsActive();
		assertNotNull("Expected not null consumerId", consumerId);

		GetConsumerProfileRequest request = new GetConsumerProfileRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		request.setConsumerId(consumerId);
		request
				.setResponseFilter(new ProfilePart[] { ProfilePart.PersonalInfo, ProfilePart.Address });
		RequestRouter requestRouter = getRequestRouter();

		GetConsumerProfileResponse response = (GetConsumerProfileResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		Consumer consumer = response.getConsumer();
		assertNotNull("Expected not null consumer", consumer);
		assertNotNull("Expected not null consumer personal", consumer
				.getPersonal());
		assertNotNull("Expected not null consumer SSN", consumer.getPersonal()
				.getSsn());
		assertNotNull("Expected not null consumer LastName", consumer
				.getPersonal().getLastName());
		assertNotNull("Expected not null consumer DOB", consumer.getPersonal()
				.getDateOfBirth());
		assertNotNull("Expected not null consumer address", consumer.getAddress());
		assertNotNull("Expected not null consumer address line1", consumer.getAddress().getLine1());

		ProcessingInstruction processingInstruction2 = new ProcessingInstruction(
				ServiceAction.findConsumers.getValue());
		processingInstruction2.setReadOnlyFlag(Boolean.TRUE);

		FindConsumersRequest request2 = new FindConsumersRequest();
		request2.setHeader(new RequestHeader(processingInstruction2,
				new ClientHeader()));
		request2.setDateOfBirth(consumer.getPersonal().getDateOfBirth());
		request2.setSsnMask(consumer.getPersonal().getSsn());
		request2.setLastName(consumer.getPersonal().getLastName());
		request2.setAddressLine1StartsWith(consumer.getAddress().getLine1().substring(0, 5));

		RequestRouter requestRouter2 = getRequestRouter();
		FindConsumersResponse response2 = (FindConsumersResponse) requestRouter2
				.process(request2);

		assertNotNull("Expected not null response2", response2);
		assertNotNull("Expected not null consumer list", response2
				.getConsumers());
		assertTrue("Expected one or more matching consumers", response2
				.getConsumers().length > 0);
		logger.info("Number of matching consumers="
				+ response2.getConsumers().length);
	}
	
	public void xtestGetIncompleteProfiles() throws ServiceException {
		logger.info("\ntestGetIncompleteProfiles...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getIncompleteProfiles.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		GetIncompleteProfilesRequest request = new GetIncompleteProfilesRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		Calendar start = Calendar.getInstance();
		start.set(2009, 0, 1);
		request.setStartDateTime(start);
		request.setEndDateTime(Calendar.getInstance());
		request.setMaxResults(new Integer("2"));
		RequestRouter requestRouter = getRequestRouter();

		GetIncompleteProfilesResponse response = (GetIncompleteProfilesResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null consumerId Array", response.getConsumerId());
		assertEquals(2, response.getConsumerId().length);
	}	
	
	public void xtestGetProfileEvents() throws ServiceException {
		logger.info("\ntestGetProfileEvents...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.getProfileEvents.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		GetProfileEventsRequest request = new GetProfileEventsRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
		Calendar start = Calendar.getInstance();
		start.set(2009, 0, 1);
		request.setConsumerId(12345L);
		request.setStartDateTime(start);
		request.setEndDateTime(Calendar.getInstance());
		RequestRouter requestRouter = getRequestRouter();

		GetProfileEventsResponse response = (GetProfileEventsResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);
		assertNotNull("Expected not null Events Array", response.getEvents());

	}		
	
	public void xtestSaveProfileEvent() throws ServiceException {
		logger.info("\ntestSaveProfileEvent...");
		ProcessingInstruction processingInstruction = new ProcessingInstruction(
				ServiceAction.saveProfileEvent.getValue());

		Long consumerId = getConsumerIdWithIncompleteProfile();
		assertNotNull("Expected not null consumerId", consumerId);
		
		SaveProfileEventRequest request = new SaveProfileEventRequest();
		request.setHeader(new RequestHeader(processingInstruction,
				new ClientHeader()));
        
		Calendar cal = Calendar.getInstance();
		ProfileEvent event1 = generateNewConsumerProfileEvent(cal);
		assertNotNull("Expected not null event1", event1);
       
		request.setConsumerId(consumerId);
		request.setEvent(event1);

		RequestRouter requestRouter = getRequestRouter();

		SaveProfileEventResponse response = (SaveProfileEventResponse) requestRouter
				.process(request);

		assertNotNull("Expected not null response", response);

	}			
}
