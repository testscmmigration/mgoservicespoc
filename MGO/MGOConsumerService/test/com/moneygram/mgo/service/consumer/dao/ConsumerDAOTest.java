package com.moneygram.mgo.service.consumer.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.BankAccount;
import com.moneygram.mgo.service.consumer.BaseConsumerServiceTestCase;
import com.moneygram.mgo.service.consumer.Comment;
import com.moneygram.mgo.service.consumer.Consumer;
import com.moneygram.mgo.service.consumer.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer.Contact;
import com.moneygram.mgo.service.consumer.CreditCard;
import com.moneygram.mgo.service.consumer.CreditCardReponse;
import com.moneygram.mgo.service.consumer.MGOAttributes;
import com.moneygram.mgo.service.consumer.PersonalInfo;
import com.moneygram.mgo.service.consumer.ProfileEvent;
import com.moneygram.mgo.service.consumer.SavedAgent;
import com.moneygram.mgo.service.consumer.TransactionPreferences;
import com.moneygram.mgo.service.consumer.extended.ExtendedContact;
import com.moneygram.mgo.service.consumer.util.ConsumerUtil;
import com.moneygram.mgo.shared.ConsumerAddress;

public class ConsumerDAOTest extends BaseConsumerServiceTestCase {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			ConsumerDAOTest.class);

	public void testGetAgentsByConsumerId() throws DAOException {
		logger.info("\ntestGetAgentsByConsumerId...");
		Long consumerId = generateConsumerIdForGettingAgents();
		assertNotNull("Expected not null consumerId", consumerId);
		logger.info("consumerId=" + consumerId);
		List<SavedAgent> agents1 = getConsumerDAO().getAgents(consumerId, null);
		assertNotNull("Expected not null agents1", agents1);
		logger.info("agents1=" + agents1);
		List<SavedAgent> agents2 = getSavedAgents(consumerId);
		assertNotNull("Expected not null agents2", agents2);
		logger.info("agents2=" + agents2);
		assertTrue("Expected agents1 and agents2 to have equal contents",
				areListsEqual(agents1, agents2));
	}

	public void testGetAgentsByLoginId() throws DAOException {
		logger.info("\ntestGetAgentsByLoginId...");
		Long consumerId = generateConsumerIdForGettingAgents();
		assertNotNull("Expected not null consumerId", consumerId);
		String loginId = getConsumerLoginId(consumerId);
		assertNotNull("Expected not null loginId", loginId);
		List<SavedAgent> agents1 = getConsumerDAO().getAgents(null, loginId);
		assertNotNull("Expected not null agents1", agents1);
		logger.info("agents1=" + agents1);
		List<SavedAgent> agents2 = getSavedAgents(consumerId);
		assertNotNull("Expected not null agents2", agents2);
		logger.info("agents2=" + agents2);
		assertTrue("Expected agents1 and agents2 to have equal contents",
				areListsEqual(agents1, agents2));
	}

	public void testSaveAgentsByLoginId() throws DAOException {
		logger.info("\ntestSaveAgentsByLoginId...");
		Long consumerId = generateConsumerIdForConsumerWithNoAgents();
		assertNotNull("Expected not null consumerId", consumerId);
		String loginId = getConsumerLoginId(consumerId);
		assertNotNull("Expected not null loginId", loginId);

		SavedAgent[] agents1 = generateSavedAgents();
		assertNotNull("Expected not null agents1", agents1);
		List<SavedAgent> saList = new ArrayList<SavedAgent>();
		for (int i = 0; i < agents1.length; i++)
			saList.add(agents1[i]);
		logger.info("agents1=" + saList);

		Consumer consumer = new Consumer();
		consumer.setLoginId(loginId);
		TransactionPreferences tp = new TransactionPreferences();
		tp.setSavedAgents(agents1);
		consumer.setTransactionPreferences(tp);
		getConsumerDAO().saveAgents(consumer);

		List<SavedAgent> agents2 = getConsumerDAO().getAgents(null, loginId);
		assertNotNull("Expected not null agents2", agents2);
		logger.info("agents2=" + agents2);
		assertTrue("Expected agents1 and agents2 to have equal contents",
				areListsEqual(saList, agents2));
	}

	@SuppressWarnings("deprecation")
	public void testGetConsumer() throws DAOException {
	    Long consumerId = generateConsumerId();
        Consumer consumer = getConsumerDAO().getConsumer(consumerId, null);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetConsumer: consumer="+ consumer);
        }
        assertNotNull("Expected not null consumer", consumer);
        assertNotNull("Expected not null consumer id", consumer.getConsumerId());
        assertNotNull("Expected not null consumer SourceSite", consumer.getSourceSite());

        /* Use any code from :
         	SELECT cei.cust_id
         	FROM cust_extnl_ident cei,
         	subdiv s,
         	identification_doc_cntry  idc
         	WHERE cei.ident_doc_id = idc.ident_doc_id
         	AND cei.issu_subdiv_code = s.subdiv_code

         	On this example:1590596L
         */

		consumer = getConsumerDAO().getConsumer(1590596L, null);
        //assertNotNull("Expected not null Personal Info - External ID", consumer.getPersonal().getExternalId());


    }

	public void xtestGetConsumerWithSourceSiteId() throws DAOException{
		_testGetConsumerWithSourceSiteId("MGO");
		_testGetConsumerWithSourceSiteId("WAP");
	}

	public void _testGetConsumerWithSourceSiteId(String sourceSiteId)
			throws DAOException {


		logger.info( //
				(new StringBuilder()) //
						.append("testGetConsumerWithSourceSiteId(\"") //
						.append(sourceSiteId) //
						.append("\")").toString() //
				);

		Long consumerId = generateConsumerId();

		Consumer consumer = getConsumerDAO().getConsumer(consumerId, null,
				sourceSiteId);

		if (logger.isDebugEnabled()) {
            logger.debug("testGetConsumer: consumer="+ consumer);
        }
        assertNotNull("Expected not null consumer", consumer);
        assertNotNull("Expected not null consumer id", consumer.getConsumerId());
        assertNotNull("Expected not null consumer SourceSite", consumer.getSourceSite());
	}

	public void xtestAddConsumerProfileWithPersonalAndContact()
			throws DAOException {
		logger.info("\ntestAddConsumerProfileWithPersonalAndContact...");
		Consumer consumer1 = generateNewConsumerProfileWithPersonalAndContact();
		assertNotNull("Expected not null consumer1", consumer1);
		// logger.info("consumer1=" + consumer1);
		Long consumerId = getConsumerDAO().addConsumerProfile(consumer1,null);
		assertNotNull("Expected not null consumer1 id", consumerId);
		consumer1.setConsumerId(consumerId);

		Consumer consumer2 = getConsumerDAO().getConsumer(consumerId, null);
		assertNotNull("Expected not null consumer2", consumer2);
		assertNotNull("Expected not null consumer2 id", consumer2
				.getConsumerId());
		List<ConsumerAddress> addressList = getConsumerDAO()
				.getConsumerAddresses(consumer2.getConsumerId());
		assertNotNull("Expected not null addressList", addressList);
		if (!addressList.isEmpty()) {
			if (consumer2.getPersonal() == null)
				consumer2.setPersonal(new PersonalInfo());
			consumer2.setAddress(addressList.get(0));
		}
		List<ExtendedContact> contactList = getConsumerDAO().getConsumerPhoneContacts(
				consumer2.getConsumerId());
		assertNotNull("Expected not null phone contactList", contactList);
		if (!contactList.isEmpty()) {
			if (consumer2.getContact() == null)
				consumer2.setContact(new Contact());
			for (Contact c : contactList) {
				if (c.getPrimaryPhone() != null)
					consumer2.getContact().setPrimaryPhone(c.getPrimaryPhone());
				if (c.getAlternatePhone() != null)
					consumer2.getContact().setAlternatePhone(
							c.getAlternatePhone());
			}
		}
		contactList = getConsumerDAO().getConsumerEmailContacts(
				consumer2.getConsumerId());
		assertNotNull("Expected not null email contactList", contactList);
		if (contactList != null && !contactList.isEmpty()) {
			if (consumer2.getContact() == null)
				consumer2.setContact(new Contact());
			for (Contact c : contactList) {
				if (c.getEmail() != null)
					consumer2.getContact().setEmail(c.getEmail());
			}
		}
		logger.info("consumer1=" + consumer1);
		logger.info("consumer2=" + consumer2);
		//add code comparison method
	}

	public void testAddConsumerComment() throws DAOException {
		logger.info("\ntestAddConsumerComment...");
		Long consumerId = generateConsumerIdForConsumerWithNoComments();
		assertNotNull("Expected not null consumerId", consumerId);

		Comment comment1 = generateNewConsumerComment();
		assertNotNull("Expected not null comment1", comment1);

		Long commentId = getConsumerDAO().addConsumerComment(consumerId,
				comment1);
		assertNotNull("Expected not null commentId", commentId);

	}

	public void testAddAccountComment() throws DAOException {
		logger.info("\ntestAddAccountComment...");
		Long accountId = generateAccountIdForAccountWithNoComments();
		assertNotNull("Expected not null accountId", accountId);

		Comment comment1 = generateNewAccountComment();
		assertNotNull("Expected not null comment1", comment1);

		Long commentId = getConsumerDAO()
				.addAccountComment(accountId, comment1);
		assertNotNull("Expected not null commentId", commentId);

	}

	public void xtestAddCreditCardAccount() throws DAOException {
		logger.info("\ntestAddCreditCardAccount...");
		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);

		CreditCard creditCard1 = generateNewCreditCardAccount();
		assertNotNull("Expected not null creditCard1", creditCard1);

		Long addressId = getConsumerAddressId(consumerId);
		assertNotNull("Expected not null addressId", addressId);

		String consumerName = getConsumerName(consumerId);
		assertNotNull("Expected not null consumerName", consumerName);

		Long accountId = getConsumerDAO().addCreditCardAccount(consumerId,
				addressId, creditCard1, consumerName, null);
		assertNotNull("Expected not null accountId", accountId);

		List<ConsumerFIAccount> accountList = getConsumerDAO()
				.getConsumerAccounts(null, accountId, null);
		assertNotNull("Expected not null accountList", accountList);
		assertTrue("Expected accountList to contain one account", accountList
				.size() == 1);
		assertNotNull("Expected not null account", accountList.get(0));
		assertNotNull("Expected not null creditCard2", accountList.get(0)
				.getCreditCard());
		logger.info("creditCard1=" + creditCard1);
		logger.info("creditCard2=" + accountList.get(0).getCreditCard());
	}

	public void xtestAddBankAccount() throws DAOException {
		logger.info("\ntestAddBankAccount...");
		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);

		BankAccount bankAccount1 = generateNewBankAccount();
		assertNotNull("Expected not null bankAccount1", bankAccount1);

		Long addressId = getConsumerAddressId(consumerId);
		assertNotNull("Expected not null addressId", addressId);

		String consumerName = getConsumerName(consumerId);
		assertNotNull("Expected not null consumerName", consumerName);

		Long accountId = getConsumerDAO().addBankAccount(consumerId, addressId,
				bankAccount1, consumerName);
		assertNotNull("Expected not null accountId", accountId);

		List<ConsumerFIAccount> accountList = getConsumerDAO()
				.getConsumerAccounts(null, accountId, null);
		assertNotNull("Expected not null accountList", accountList);
		assertTrue("Expected accountList to contain one account", accountList
				.size() == 1);
		assertNotNull("Expected not null account", accountList.get(0));
		assertNotNull("Expected not null bankAccount2", accountList.get(0)
				.getBankAccount());
		logger.info("bankAccount1=" + bankAccount1);
		logger.info("bankAccount2=" + accountList.get(0).getBankAccount());
	}

	public void xtestUpdateConsumerStatus() throws DAOException {
		logger.info("\ntestUpdateConsumerStatus...");
		Long consumerId = generateConsumerId();
		assertNotNull("Expected not null consumerId", consumerId);
		getConsumerDAO().updateConsumerStatus(consumerId, "NAT", "USR");
	}

	public void xtestGetConsumerBlockedCodes() throws DAOException {
		logger.info("\ntestGetConsumerBlockedCodes...");
		// Bad values 1111111111 lindanoye@aim.com

		//get blocked domain from database & prepend test@
		String blockedEmail = "test@" + this.getBlockedEmail(true);

		MGOAttributes internal = getConsumerDAO().getConsumerBlockedCodes(
				"1111111111", "6666666666", blockedEmail);
		assertNotNull("Expected not null internal", internal);
		assertNotNull("Expected not null internal consumerBlocked", internal
				.getConsumerBlocked());
		assertNotNull("Expected not null internal primaryPhoneBlocked",
				internal.getPrimaryPhoneBlocked());
		assertNotNull("Expected not null internal alternatePhoneBlocked",
				internal.getAlternatePhoneBlocked());
		assertNotNull("Expected not null internal emailBlocked", internal
				.getEmailBlocked());
		assertNotNull("Expected not null internal emailDomainBlocked", internal
				.getEmailDomainBlocked());
		assertTrue(internal.getEmailDomainBlocked().booleanValue());
		logger.info("internal=" + internal);
	}

	public void xtestGetCreditCardBlockedCodes() throws DAOException {
		logger.info("\ntestGetCreditCardBlockedCodes...");
		CreditCardReponse ccr = getConsumerDAO().getCreditCardBlockedCodes(
				"1234567890129689", null);
		CreditCard cc = ccr.getCreditCard();
		assertNotNull("Expected not null creditCard", cc);
		assertNotNull("Expected not null creditCard blocked", cc.getBlocked());
		assertNotNull("Expected not null creditCard binBlocked", cc
				.getBinBlocked());
		logger.info("creditCard=" + cc);
	}

	public void xtestGetBankAccountBlockedCodes() throws DAOException {
		logger.info("\ntestGetBankAccountBlockedCodes...");
		BankAccount ba = getConsumerDAO().getBankAccountBlockedCodes(
				"660000036", "1111111111111111");
		assertNotNull("Expected not null bankAccount", ba);
		assertNotNull("Expected not null bankAccount blocked", ba.getBlocked());
		assertNotNull("Expected not null bankAccount abaBlocked", ba
				.getAbaBlocked());
		logger.info("bankAccount=" + ba);
	}

	public void xtestIsBankAccountBlocked_ABABlocked() throws DAOException {
		logger.info("\ntestIsBankAccountBlocked...");
		String hashedMask = ConsumerUtil.hash(ConsumerDAO.ACCT_NBR_WILDCARDED);
		String blockedAba = this.getBlockedAba(hashedMask);
		assertTrue(getConsumerDAO().isBankAccountBlocked(blockedAba, "5674839393", true));
	}

	public void xtestUpdateConsumerPhone() throws DAOException {
		logger.info("\ntestUpdateConsumerPhone...");
		Long consumerId = generateConsumerIdWithAlternatePhone();
		getConsumerDAO().updateConsumerPhone(consumerId, "1113334444", "HOME" , "N");
	}

	public void testIsPhoneNumberBlocked() throws DAOException {
		logger.info("\ntestIsPhoneNumberBlocked...");
		String blockedNumber = generateBlockedPhoneNumber();
		if(blockedNumber != null){
			assertTrue(getConsumerDAO().isPhoneNumberBlocked(blockedNumber));
		}

		String notBlockedNumber = generateNotBlockedPhoneNumber();
		if(notBlockedNumber != null){
			assertFalse(getConsumerDAO().isPhoneNumberBlocked(notBlockedNumber));
		}
	}

	public void xtestAddConsumerPasswordHash() throws DAOException {
		logger.info("\ntestAddConsumerPasswordHash...");
		Long consumerId = generateConsumerId();
		getConsumerDAO().addConsumerPasswordHash(consumerId, "someHash");
	}

	public void xtestUpdateConsumerLoyalty() throws DAOException {
		logger.info("\ntestUpdateConsumerLoyalty...");
		Long consumerId = generateConsumerId();
		getConsumerDAO().updateConsumerLoyalty(consumerId, Boolean.FALSE,
				"123456789");
	}

	public void xtestIsIpAddressBlocked() throws DAOException {
		logger.info("\ntestIsIpAddressBlocked...");
		String address = "111.222.111.214";
		logger.info("address " + address + " blocked="
				+ getConsumerDAO().isIpAddressBlocked(address));
		address = "111.222.77.122";
		logger.info("address " + address + " blocked="
				+ getConsumerDAO().isIpAddressBlocked(address));
	}

	public void testAddLoginTryAndBlockedAttempts() throws DAOException {
		logger.info("\ntestAddLoginTryAndBlockedAttempts...");

		Long loginTryId = getConsumerDAO().addLoginTry("invalidLogon@invalid.com", "123.123.123.123",
				"prodWebServer", "12.12.12.12", false, false, false, getSourceSite());
		assertNotNull(loginTryId);
		assertTrue (loginTryId.longValue() > 0L);

		getConsumerDAO().addBlockedAttemptsNoConsumer(true, loginTryId);
	}

    public void xtestCheckForDuplicateConsumers() throws DAOException {
		logger.info("\ntestCheckForDuplicateConsumers...");
		Long consumerId = generateConsumerIdWhoIsActive();
		Consumer consumer = getConsumerDAO().getConsumer(consumerId, null);
		assertNotNull("Expected not null consumer", consumer);
		assertNotNull("Expected not null consumer personal", consumer
				.getPersonal());
		assertNotNull("Expected not null consumer SSN", consumer.getPersonal()
				.getSsn());
		assertNotNull("Expected not null consumer LastName", consumer
				.getPersonal().getLastName());
		assertNotNull("Expected not null consumer DOB", consumer.getPersonal()
				.getDateOfBirth());
		List<ConsumerAddress> addresses = getConsumerDAO()
				.getConsumerAddresses(consumer.getConsumerId());
		assertNotNull("Expected not null consumer addresses", addresses);
		assertTrue("Expected one or more addresses", addresses.size() > 0);
		consumer.setAddress(addresses.get(0));
		assertNotNull("Expected not null consumer address", consumer
				.getAddress());
		assertNotNull("Expected not null consumer address line1", consumer
				.getAddress().getLine1());
		logger.info("consumer=" + consumer);

		List<Consumer> duplicateConsumers = getConsumerDAO()
				.getActiveConsumers(
						consumer.getPersonal().getDateOfBirth().getTime(),
						consumer.getPersonal().getSsn(),
						consumer.getPersonal().getLastName(),
						consumer.getAddress().getLine1()
								.substring(0, 5));
		assertNotNull("Expected not null duplicateConsumers",
				duplicateConsumers);
		assertTrue("Expected one or more duplicateConsumers",
				duplicateConsumers.size() > 0);
	}

	public void xtestUpdateConsumerSecurityQuestionsCollectionDate() throws DAOException {
        Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
        ConsumerDAO dao = getConsumerDAO();
        Long consumerId = dao.addConsumerProfile(consumer,null);
        assertNotNull("Expected not null consumerId", consumerId);
        getConsumerDAO().updateConsumerSecurityQuestionsCollectionDate(consumerId, new Date());
    }

    public void testUpdateAccountStatus() throws DAOException {
        getConsumerDAO().updateAccountStatus(getConsumerAccount(), "DEL", "DEL");
    }

    public void xtestUpdateConsumerPromoEmailFlag() throws DAOException {
    	_testUpdateconsumerPromoEmailFlag("MGO");
    	_testUpdateconsumerPromoEmailFlag("WAP");
    }

    public void _testUpdateconsumerPromoEmailFlag(String partnerSiteId)
			throws DAOException {

		String logMsg = new StringBuilder("testUpdateconsumerPromoEmailFlag(\"") //
				.append(partnerSiteId) //
				.append("\")") //
				.toString();

		logger.info(logMsg);

		Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
		ConsumerDAO dao = getConsumerDAO();
        Long consumerId = dao.addConsumerProfile(consumer,null);
        assertNotNull("Expected not null consumerId", consumerId);
        dao.updateConsumerPromoEmailFlag(consumerId, true, partnerSiteId);
    }

    public void xtestUpdateConsumerAddress() throws DAOException {
        Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
        ConsumerDAO dao = getConsumerDAO();
        Long consumerId = dao.addConsumerProfile(consumer,null);
        assertNotNull("Expected not null consumerId", consumerId);
        dao.updateConsumerAddress(consumerId, generateConsumerAddress());
    }

    public void xtestUpdatePrimaryPhone_NbrBlocked() throws DAOException {
        Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
        ConsumerDAO dao = getConsumerDAO();
        Long consumerId = dao.addConsumerProfile(consumer,null);
        String blockedPhoneNbr = getPhoneNumber(true);

        assertNotNull("Expected not null consumerId", consumerId);
        boolean blocked = dao.updatePrimaryPhone(consumerId, "HOME",blockedPhoneNbr);
        assertTrue(blocked);
    }

    public void xtestUpdateAlternatePhone() throws DAOException {
        Consumer consumer = generateNewConsumerProfileWithPersonalAndContact();
        ConsumerDAO dao = getConsumerDAO();
        Long consumerId = dao.addConsumerProfile(consumer,null);
        String unblockedPhoneNbr = getPhoneNumber(false);
        assertNotNull("Expected not null consumerId", consumerId);
        boolean blocked = dao.updateAlternatePhone(consumerId, "HOME", unblockedPhoneNbr);
        assertFalse(blocked);
    }

	public void testGetProfileEvents() throws DAOException {
		logger.info("\ntestGetProfileEvents...");
		Long consumerId = generateConsumerIdForConsumerWithNoComments();
		assertNotNull("Expected not null consumerId", consumerId);

		Calendar start = Calendar.getInstance();
		start.set(2009,0,1); //2009-01-01
		getConsumerDAO().getProfileEvents(consumerId, null, null, null);
		getConsumerDAO().getProfileEvents(consumerId, start, Calendar.getInstance(), "KBA_BAILER1_SENT");
	}

	public void testSaveAndGetProfileEvents() throws DAOException {
		logger.info("\ntestSaveProfileEvent...");
		Long consumerId = generateConsumerIdForConsumerWithNoEvents();
		assertNotNull("Expected not null consumerId", consumerId);

		Calendar cal = Calendar.getInstance();
		ProfileEvent event1 = generateNewConsumerProfileEvent(cal);
		assertNotNull("Expected not null event1", event1);

		getConsumerDAO().saveProfileEvent(consumerId, event1);

		List<ProfileEvent> events = getConsumerDAO().getProfileEvents(consumerId, cal, cal, null);
		assertNotNull(events);
		assertEquals(1, events.size());
		assertEquals(event1.getEvent(), events.get(0).getEvent());
		assertEquals(event1.getEventDateTime(),events.get(0).getEventDateTime());

	}

	public void testSaveAndGetRSAProfileEvents() throws DAOException {
	    saveAndGetProfileEvent("RSA_NAP_BYPASS_KBA");
        saveAndGetProfileEvent("RSA_NAP_PROCEED_WITH_KBA");
        saveAndGetProfileEvent("RSA_NAP_ACCESS_DENIED");
    }

	private void saveAndGetProfileEvent(String eventType) throws DAOException {
        Long consumerId = generateConsumerId();
        assertNotNull("Expected not null consumerId", consumerId);

        Calendar cal = Calendar.getInstance();
        ProfileEvent event = generateNewConsumerProfileEvent(cal, eventType);
        assertNotNull("Expected not null event", event);

        getConsumerDAO().saveProfileEvent(consumerId, event);

        List<ProfileEvent> events = getConsumerDAO().getProfileEvents(consumerId, cal, cal, null);
        assertNotNull(events);
        assertTrue("Expected events saved", events.size() > 0);

        ProfileEvent matchedEvent = null;
        for (ProfileEvent savedEvent : events) {
            if (eventType.equals(savedEvent.getEvent())) {
                matchedEvent = savedEvent;
                break;
            }
        }
        assertEquals(event.getEvent(), matchedEvent.getEvent());
        assertEquals(event.getEventDateTime(), matchedEvent.getEventDateTime());
    }

	public void testGetIncompleteProfiles() throws DAOException {
		logger.info("\ntestGetIncompleteProfiles...");

		Long consumerId = getConsumerIdWithIncompleteProfile();
		assertNotNull("Expected not null consumerId", consumerId);

		Calendar cal = Calendar.getInstance();
		ProfileEvent event1 = generateNewConsumerProfileEvent(cal);
		assertNotNull("Expected not null event1", event1);

		//save an event so consumer profile not complete
		getConsumerDAO().saveProfileEvent(consumerId, event1);
		List<Long> consumerIds = getConsumerDAO().getIncompleteProfiles(cal,cal, null, null);

		assertNotNull(consumerIds);
//TODO: figure out why it fails always
//		assertEquals(1, consumerIds.size());
//		assertEquals(consumerId, consumerIds.get(0));

	}

	public void testSetBankCardBlockingWithBlockedAccts() throws DAOException {
		logger.info("\ntestSetBankCardBlockingWithBlockedAccts...");

		HashMap<String,Boolean> map = new HashMap<String,Boolean>();

		ConsumerFIAccount[] accounts = generateUnBlockedAccounts(2,3);
		accounts[0].getCreditCard().setBinBlocked(new Boolean(true));
		accounts[1].getCreditCard().setBlocked(new Boolean(true));
		accounts[2].getBankAccount().setAbaBlocked(new Boolean(true));
		accounts[3].getBankAccount().setBlocked(new Boolean(true));

		Consumer consumer = new Consumer();
		consumer.setAccounts(accounts);
		getConsumerDAO().setBankCardBlocking(consumer, map);

		assertTrue(map.get(ConsumerDAO.BANK_ABA_BLOCKED).booleanValue());
		assertTrue(map.get(ConsumerDAO.BANK_ACCT_BLOCKED).booleanValue());
		assertTrue(map.get(ConsumerDAO.CC_BIN_BLOCKED).booleanValue());
		assertTrue(map.get(ConsumerDAO.CC_BLOCKED).booleanValue());
	}


	public void testSetBankCardBlockingWithNoBlockedAccts() throws DAOException {
		logger.info("\ntestSetBankCardBlockingWithNoBlockedAccts...");

		HashMap<String,Boolean> map = new HashMap<String,Boolean>();

		ConsumerFIAccount[] accounts = generateUnBlockedAccounts(2,2);

		Consumer consumer = new Consumer();
		consumer.setAccounts(accounts);
		getConsumerDAO().setBankCardBlocking(consumer, map);

		assertEquals(null, map.get(ConsumerDAO.BANK_ABA_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.BANK_ACCT_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.CC_BIN_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.CC_BLOCKED));

	}

	public void testSetBankCardBlockingWithNoAccts() throws DAOException {
		logger.info("\ntestSetBankCardBlockingWithNoAccts...");

		HashMap<String,Boolean> map = new HashMap<String,Boolean>();

		Consumer consumer = new Consumer();
		consumer.setAccounts(null);
		getConsumerDAO().setBankCardBlocking(consumer, map);

		assertEquals(null, map.get(ConsumerDAO.BANK_ABA_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.BANK_ACCT_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.CC_BIN_BLOCKED));
		assertEquals(null, map.get(ConsumerDAO.CC_BLOCKED));

	}

}
