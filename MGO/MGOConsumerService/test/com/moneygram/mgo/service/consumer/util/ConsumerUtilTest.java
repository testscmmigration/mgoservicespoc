package com.moneygram.mgo.service.consumer.util;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.BaseConsumerServiceTestCase;

public class ConsumerUtilTest extends BaseConsumerServiceTestCase {

	private static final Logger logger = LogFactory.getInstance().getLogger(ConsumerUtilTest.class);

	public void testExtractDomainFromEmail() throws DAOException {
		logger.info("\ntestExtractDomainFromEmail...");
		assertEquals("domain.com", ConsumerUtil.extractDomainFromEmail("name@domain.com"));
		assertEquals("domain.com@xyz.com", ConsumerUtil.extractDomainFromEmail("name@domain.com@xyz.com"));
		assertEquals("domain.com", ConsumerUtil.extractDomainFromEmail("@domain.com"));
		assertEquals(null, ConsumerUtil.extractDomainFromEmail("abcdomain.com"));

	}

}
