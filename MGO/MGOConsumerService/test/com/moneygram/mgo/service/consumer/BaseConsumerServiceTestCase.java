/*
 * Created on Jun 24, 2009
 *
 */
package com.moneygram.mgo.service.consumer;

import java.security.Security;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.util.ResourceConfig;
import com.moneygram.common.service.util.TestResourceConfigFactory;
import com.moneygram.mgo.security.MyProvider;
import com.moneygram.mgo.service.consumer.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer.util.MGOConsumerServiceResourceConfig;
import com.moneygram.mgo.shared.ConsumerAddress;
import com.moneygram.service.BaseServiceTest;

public abstract class BaseConsumerServiceTestCase extends BaseServiceTest {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			BaseConsumerServiceTestCase.class);

	protected static final Map<String, String> attributes = new HashMap<String, String>();
	static {
		attributes.put(MGOConsumerServiceResourceConfig.PUBLIC_KEY_FILE,
				"test/emg_dev.crt.der");
		attributes.put(MGOConsumerServiceResourceConfig.SECURE_HASH_SEED,
				"k@#D$%^A*&a");
		attributes
				.put(
						MGOConsumerServiceResourceConfig.PCI_ENCRYPT_URL,
						"https://d2wsintsvcs.qacorp.moneygram.com/PCIEncryptService/services/PCIEncryptServiceSOAP");
		attributes.put(MGOConsumerServiceResourceConfig.PCI_ENCRYPT_TIMEOUT,
				"5000");
	}

	private static ConsumerDAO consumerDAO = null;

	// agentIds are not foreign keys so can use any values
	protected static SavedAgent[] generateSavedAgents() {
		SavedAgent[] agents = new SavedAgent[] { new SavedAgent(43417032),
				new SavedAgent(43678114) };
		return agents;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		// Install the all-trusting trust manager
		Security.addProvider( new MyProvider() );
		Security.setProperty("ssl.TrustManagerFactory.algorithm", "TrustAllCertificates");

		Security.setProperty("ssl.SocketFactory.provider", "com.ibm.jsse2.SSLSocketFactoryImpl");
		Security.setProperty("ssl.ServerSocketFactory.provider", "com.ibm.jsse2.SSLServerSocketFactoryImpl");
		
		ResourceConfig.setResourceConfigFactory(new TestResourceConfigFactory(attributes));
	}

	@SuppressWarnings("unchecked")
	protected Long generateConsumerIdForGettingAgents() {
		String sql = "select cust_id from cust_favorite_agent where rownum < 50";
		try {
			List<Long> custIds = getConsumerDAO().getJdbcTemplate()
					.queryForList(sql, Long.class);
			if (custIds == null || custIds.isEmpty())
				return null;
			List<SavedAgent> agentIds;
			for (Long custId : custIds) {
				agentIds = getSavedAgents(custId);
				if (agentIds != null && agentIds.size() > 1)
					return custId;
			}
			return null;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Consumer generateNewConsumerProfileWithPersonalAndContact() {
		Consumer c = new Consumer();
		String loginId;
		for (;;) {
			loginId = "eric" + getRandomNumberString() + "@mgi.com";
			if (getConsumerId(loginId) == null)
				break;
		}
		c.setLoginId(loginId);
		c.setPersonal(new PersonalInfo());
		c.getPersonal().setFirstName("Eric");
		c.getPersonal().setMiddleName("John");
		c.getPersonal().setLastName("Torg");
		c.getPersonal().setDateOfBirth(Calendar.getInstance());
		c.getPersonal().setSsn("1234");
		c.setAddress(generateConsumerAddress());
		c.setContact(new Contact());
		c.getContact().setPrimaryPhone("111-123-4567");
		c.getContact().setAlternatePhone("222-123-4567");
		c.getContact().setEmail(loginId);
		c.getContact().setPromoEmail(new Boolean(true));
		c.setInternal(new MGOAttributes());
		c.getInternal().setConsumerStatus("PEN");
		c.getInternal().setConsumerSubStatus("INI");
		c.getInternal().setConsumerBlocked(new Boolean(false));
		c.getInternal().setPrimaryPhoneBlocked(new Boolean(false));
		c.getInternal().setAlternatePhoneBlocked(new Boolean(false));
		c.getInternal().setEmailBlocked(new Boolean(false));
		c.getInternal().setEmailDomainBlocked(new Boolean(false));
		c.getInternal().setGlobalUniqueId(loginId);
		c.setSourceSite(getSourceSite());
		return c;
	}

	protected ConsumerAddress generateConsumerAddress() {
        ConsumerAddress address = new ConsumerAddress();
        address.setLine1("Address 1");
        address.setLine2("Address 2");
        address.setLine3("Address 3");
        address.setCity("Raleigh");
        address.setState("NC");
        address.setZipCode("12345");
        address.setCountry("USA");
        return address;
    }
	
	protected CreditCard generateNewCreditCardAccount() {
		CreditCard cc = new CreditCard();
		cc.setAccountNumber("1234567890123456");
		cc.setAccountType(FIAccountType.fromValue("CC-VISA"));
		cc.setExpMonth(new Integer(12));
		cc.setExpYear(new Integer(2012));
		cc.setAccountStatus("ACT");
		cc.setAccountSubStatus("L01");
		cc.setBlocked(new Boolean(false));
		cc.setBinBlocked(new Boolean(false));
		return cc;
	}

	protected BankAccount generateNewBankAccount() {
		BankAccount ba = new BankAccount();
		ba.setAbaNumber("123456789");
		ba.setFiName("Greed Bank");
		ba.setAccountNumber("1234567890123456");
		ba.setAccountType(FIAccountType.fromValue("BANK-CHK"));
		ba.setAccountStatus("ACT");
		ba.setAccountSubStatus("L01");
		ba.setBlocked(new Boolean(false));
		ba.setAbaBlocked(new Boolean(false));
		return ba;
	}

	protected ConsumerFIAccount[] generateUnBlockedAccounts(int nbrCCAccts, int nbrBankAccts) {

		ConsumerFIAccount[] accounts = new ConsumerFIAccount[nbrCCAccts+nbrBankAccts];
		
		for (int i = 0; i < nbrCCAccts; i++) {
			CreditCard cc = generateNewCreditCardAccount();
			cc.setBlocked(new Boolean(false));
			cc.setBinBlocked(new Boolean(false));
			accounts[i] = new ConsumerFIAccount();
			accounts[i].setCreditCard(cc);
		}
		
		for (int j = nbrCCAccts;j < nbrCCAccts+nbrBankAccts;j++) {
			BankAccount ba = generateNewBankAccount();
			ba.setAbaBlocked(new Boolean(false));
			ba.setBlocked(new Boolean(false));
			accounts[j] = new ConsumerFIAccount();
			accounts[j].setBankAccount(ba);
		}

		return accounts;
	}
	
	protected Comment generateNewAccountComment() {
		Comment c = new Comment();
		c.setReasonCode("OTH");
		c.setCommentText("Some account comment");
		return c;
	}

	protected Comment generateNewConsumerComment() {
		Comment c = new Comment();
		c.setReasonCode("OTH");
		c.setCommentText("Some consumer comment");
		return c;
	}

	protected ProfileEvent generateNewConsumerProfileEvent(Calendar cal) {
		return generateNewConsumerProfileEvent(cal, "RSA_BAILER2_SENT");
	}

    protected ProfileEvent generateNewConsumerProfileEvent(Calendar cal, String eventType) {
        if (cal != null) {
            cal.set(Calendar.MILLISECOND, 0); //because DB doesn't store to this detail
        }
        ProfileEvent e = new ProfileEvent();
        e.setEvent(eventType);
        e.setEventDateTime(cal == null ? Calendar.getInstance() : cal);
        return e;
    }

	protected Consumer generateNewConsumerProfileforGettingBlockedStatus() {
		Consumer c = new Consumer();
		c.setPersonal(new PersonalInfo());
		c.getPersonal().setFirstName("Eric");
		c.getPersonal().setMiddleName("John");
		c.getPersonal().setLastName("Torg");
		c.getPersonal().setDateOfBirth(Calendar.getInstance());
		c.getPersonal().setSsn("1234");
        c.setAddress(generateConsumerAddress());
		c.setContact(new Contact());
		c.getContact().setPrimaryPhone("111-123-4567");
		c.getContact().setAlternatePhone("222-123-4567");
		c.getContact().setEmail("lindanoye@aim.com");
		c.getContact().setPromoEmail(new Boolean(true));
		c.setInternal(new MGOAttributes());
		c.getInternal().setConsumerStatus("PEN");
		c.getInternal().setConsumerSubStatus("INI");

		CreditCard cc = new CreditCard();
		cc.setAccountNumber("1234567890123456");
		cc.setAccountType(FIAccountType.fromValue("CC-VISA"));
		cc.setExpMonth(new Integer(12));
		cc.setExpYear(new Integer(2012));

		BankAccount ba = new BankAccount();
		ba.setAbaNumber("123456789");
		ba.setFiName("Greed Bank");
		ba.setAccountNumber("1234567890123456");
		ba.setAccountType(FIAccountType.fromValue("BANK-CHK"));

		c.setAccounts(new ConsumerFIAccount[2]);
		c.getAccounts()[0] = new ConsumerFIAccount();
		c.getAccounts()[0].setCreditCard(cc);
		c.getAccounts()[1] = new ConsumerFIAccount();
		c.getAccounts()[1].setBankAccount(ba);

        c.setSourceSite(getSourceSite());

		return c;
	}

	protected Long generateConsumerIdForConsumerWithNoAgents() {
		String sql1 = "select cust_id from cust_account where rownum = 1";
		String sql2 = "delete from cust_favorite_agent where cust_id = ?";
		try {
			Long consumerId = (Long) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql1, Long.class);
			if (consumerId == null)
				return null;

			// remove all agents from consumerId
			getConsumerDAO().getJdbcTemplate().update(sql2,
					new Object[] { consumerId });
			return consumerId;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Long generateConsumerIdForConsumerWithNoComments() {
		String sql1 = "select cust_id from customer where rownum = 1";
		String sql2 = "delete from cust_comment where cust_id = ?";
		try {
			Long consumerId = (Long) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql1, Long.class);
			if (consumerId == null)
				return null;

			// remove all comments from consumerId
			getConsumerDAO().getJdbcTemplate().update(sql2,
					new Object[] { consumerId });
			return consumerId;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	   protected Long generateConsumerIdForConsumerWithNoEvents() {
	        String sql1 = "select cust_id from customer where rownum = 1";
	        String sql2 = "delete from cust_acty_log where cust_id = ?";
	        try {
	            Long consumerId = (Long) getConsumerDAO().getJdbcTemplate()
	                    .queryForObject(sql1, Long.class);
	            if (consumerId == null)
	                return null;

	            // remove all comments from consumerId
	            getConsumerDAO().getJdbcTemplate().update(sql2,
	                    new Object[] { consumerId });
	            return consumerId;
	        } catch (EmptyResultDataAccessException ex) {
	            return null;
	        }
	    }

	protected Long getConsumerIdWithIncompleteProfile() {
        String sql1 = 
        	"select DISTINCT (c.cust_id)\n" +
        	"  from customer c, cust_acty_log cal\n" + 
        	" where rownum = 1\n" + 
        	"   and c.cust_id = cal.cust_id\n" + 
        	"   and c.cust_stat_code = 'ACT'\n" + 
        	"   and c.cust_sub_stat_code in ('VAB', 'L02')\n" + 
        	"   and c.cust_blkd_code != 'B'\n" + 
        	"   and c.cust_id not in (SELECT DISTINCT (cal.cust_id)\n" + 
        	"                           FROM cust_acty_log cal, acty_log al\n" + 
        	"                          WHERE cal.acty_log_code = al.acty_log_code\n" + 
        	"                            AND al.acty_log_code = 8) --FIRST TRANSACTION COMPLETED";

		try {
			Long consumerId = (Long) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql1, Long.class);
			if (consumerId == null)
				return null;

			return consumerId;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected Long generateAccountIdForAccountWithNoComments() {
		String sql1 = "select cust_acct_id from cust_account where rownum = 1";
		String sql2 = "delete from cust_account_comment where cust_acct_id = ?";
		try {
			Long accountId = (Long) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql1, Long.class);
			if (accountId == null)
				return null;

			// remove all comments from accountId
			getConsumerDAO().getJdbcTemplate().update(sql2,
					new Object[] { accountId });
			return accountId;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Long generateConsumerId() {
		String sql = "select cust_id from customer where rownum = 1";
		try {
			return (Long) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Long generateConsumerIdWhoIsActive() {
		String sql = "select cust_id from customer where cust_stat_code = 'ACT' and rownum = 1";
		try {
			return (Long) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected Long generateConsumerIdWithAlternatePhone() {
		String sql = "select cust_id from cust_phone where ph_type_code = 'ALT' and rownum = 1";
		try {
			return (Long) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected String generateBlockedPhoneNumber() {
		String sql = "select BLKD_PH_NBR from BLOCKED_PHONE where BLKD_STAT_CODE = 'BLK' and rownum = 1";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected String generateNotBlockedPhoneNumber() {
		String sql = "select t.cust_ph_nbr from CUST_PHONE t WHERE t.cust_ph_nbr NOT IN " +
					"(select BLKD_PH_NBR from BLOCKED_PHONE where BLKD_STAT_CODE = 'BLK') and rownum = 1";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected String getBlockedEmail(boolean isDomainTypeBlock) {
		String type;
		if (isDomainTypeBlock) {
			type = "D";
		} else {
			type = "A";
		}
		String sql = "select blkd_elec_addr_id from blocked_electronic_addr where addr_fmt_code = '" + type+ "' and blkd_stat_code = 'BLK' and rownum = 1";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected String getBlockedEmail() {
		String sql = "select blkd_elec_addr_id from blocked_electronic_addr where addr_fmt_code = 'D' and rownum = 1";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected String getBlockedAba(String hashedMaskValue) {		
		String sql = "select bank_aba_nbr from blocked_bank_account where blkd_acct_hash_text = '" + hashedMaskValue + 
		              "' and blkd_stat_code = 'BLK' and rownum = 1";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	protected String getPhoneNumber(boolean blocked) {
		String blockedStatus = null;
		if (blocked) {
			blockedStatus = "BLK";
		} else {
			blockedStatus = "NBK";
		}
        String sql = "select bp.blkd_ph_nbr\n" + 
			"  from blocked_phone bp\n" + 
			" where bp.blkd_stat_code = '" + blockedStatus + "'" +
			"   and rownum = 1";

		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	// Helper Methods

	protected String getConsumerLoginId(Long consumerId) {
		String sql = "select cust_logon_id from customer where cust_id = ?";
		try {
			return (String) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, new Object[] { consumerId }, String.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected String getConsumerName(Long consumerId) {
		String sql1 = "select cust_frst_name from customer where cust_id = ?";
		String sql2 = "select cust_last_name from customer where cust_id = ?";
		try {
			String first = (String) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql1, new Object[] { consumerId },
							String.class);
			if (first == null)
				return null;
			String last = (String) getConsumerDAO().getJdbcTemplate()
					.queryForObject(sql2, new Object[] { consumerId },
							String.class);
			if (last == null)
				return null;
			return first + " " + last;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Long getConsumerId(String loginId) {
		String sql = "select cust_id from customer where cust_logon_id = ?";
		try {
			return (Long) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, new Object[] { loginId }, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected Long getConsumerId(Long accountId) {
		String sql = "select cust_id from cust_account where cust_acct_id = ";
		try {
			return (Long) getConsumerDAO().getJdbcTemplate().queryForObject(
					sql, new Object[] { accountId }, Long.class);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	protected Long getConsumerAddressId(Long consumerId) {
		String sql = "select addr_id from cust_address where cust_id = ?";
		try {
			List<Long> list = getConsumerDAO().getJdbcTemplate().queryForList(
					sql, new Object[] { consumerId }, Long.class);
			if (list == null || list.size() != 1)
				return null;
			return list.get(0);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	protected List<SavedAgent> getSavedAgents(Long consumerId) {
		String sql = "select agent_id from cust_favorite_agent where cust_id = ?";
		try {
			List<Long> list = getConsumerDAO().getJdbcTemplate().queryForList(
					sql, new Object[] { consumerId }, Long.class);
			if (list == null)
				return null;
			SavedAgent sa;
			List<SavedAgent> agentList = new ArrayList<SavedAgent>();
			for (Long agentId : list) {
				sa = new SavedAgent();
				sa.setAgentId(agentId.longValue());
				agentList.add(sa);
			}
			return agentList;
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	protected String getRandomNumberString() {
		return String.valueOf((int) (Math.random() * 10000));
	}

	protected boolean areListsEqual(List<SavedAgent> agents1,
			List<SavedAgent> agents2) {
		if (agents1 == null || agents2 == null) {
			fail("Neither agents1 nor agents2 can be null");
			return false;
		}
		if (agents1.size() != agents2.size())
			return false;
		Set<Long> agentSet = new HashSet<Long>();
		for (SavedAgent sa : agents1) {
			if (sa == null) {
				fail("Bad elements in agents1");
				return false;
			}
			agentSet.add(new Long(sa.getAgentId()));
		}
		for (SavedAgent sa : agents2) {
			if (sa == null) {
				fail("Bad elements in agents2");
				return false;
			}
			if (!agentSet.contains(new Long(sa.getAgentId())))
				return false;
		}
		return true;
	}

	/**
	 * Returns a spring-configured instance of ConsumerDAO.
	 * 
	 * @return spring-configured instance of ConsumerDAO.
	 */
	protected ConsumerDAO getConsumerDAO() {
		if (consumerDAO != null)
			return consumerDAO;
		ApplicationContext context = getContext();
		consumerDAO = (ConsumerDAO) context.getBean("consumerDAO");
		return consumerDAO;
	}
	
   protected Long getConsumerAccount() {
        String sql = "select cust_acct_id from cust_account where rownum = 1";
            return (Long) getConsumerDAO().getJdbcTemplate()
                    .queryForObject(sql, new Object[] {}, Long.class);
   }

   @Override
   protected String[] getContextResources() {
       return new String[] { "ConsumerTestContext.xml" };
   }

   protected String getSourceSite() {
       return "MGO";
   }


}
