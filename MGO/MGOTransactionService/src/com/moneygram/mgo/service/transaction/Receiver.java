/**
 * Receiver.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.mgo.shared.ReceiverConsumerAddress;
import com.moneygram.mgo.shared.ReceiverConsumerName;


/**
 * Either receiver name and address need to be specified for the Send
 * Money flow or
 * 					receiverConsumerAccountNbr needs to be specified for the Pay
 * Bill flow.
 */
public class Receiver  implements java.io.Serializable {
    private ReceiverConsumerName receiverName;

    private ReceiverConsumerAddress receiverAddress;

    private java.lang.String receiverConsumerAccountNbr;

    private java.lang.String receiverPhone;

    private java.lang.String receiverMTAccountNbrMask;

    public Receiver() {
    }

    public Receiver(
           ReceiverConsumerName receiverName,
           ReceiverConsumerAddress receiverAddress,
           java.lang.String receiverConsumerAccountNbr,
           java.lang.String receiverPhone,
           java.lang.String receiverMTAccountNbrMask) {
           this.receiverName = receiverName;
           this.receiverAddress = receiverAddress;
           this.receiverConsumerAccountNbr = receiverConsumerAccountNbr;
           this.receiverPhone = receiverPhone;
           this.receiverMTAccountNbrMask = receiverMTAccountNbrMask;
    }


    /**
     * Gets the receiverName value for this Receiver.
     * 
     * @return receiverName
     */
    public ReceiverConsumerName getReceiverName() {
        return receiverName;
    }


    /**
     * Sets the receiverName value for this Receiver.
     * 
     * @param receiverName
     */
    public void setReceiverName(ReceiverConsumerName receiverName) {
        this.receiverName = receiverName;
    }


    /**
     * Gets the receiverAddress value for this Receiver.
     * 
     * @return receiverAddress
     */
    public ReceiverConsumerAddress getReceiverAddress() {
        return receiverAddress;
    }


    /**
     * Sets the receiverAddress value for this Receiver.
     * 
     * @param receiverAddress
     */
    public void setReceiverAddress(ReceiverConsumerAddress receiverAddress) {
        this.receiverAddress = receiverAddress;
    }


    /**
     * Gets the receiverConsumerAccountNbr value for this Receiver.
     * 
     * @return receiverConsumerAccountNbr
     */
    public java.lang.String getReceiverConsumerAccountNbr() {
        return receiverConsumerAccountNbr;
    }


    /**
     * Sets the receiverConsumerAccountNbr value for this Receiver.
     * 
     * @param receiverConsumerAccountNbr
     */
    public void setReceiverConsumerAccountNbr(java.lang.String receiverConsumerAccountNbr) {
        this.receiverConsumerAccountNbr = receiverConsumerAccountNbr;
    }


    /**
     * Gets the receiverPhone value for this Receiver.
     * 
     * @return receiverPhone
     */
    public java.lang.String getReceiverPhone() {
        return receiverPhone;
    }


    /**
     * Sets the receiverPhone value for this Receiver.
     * 
     * @param receiverPhone
     */
    public void setReceiverPhone(java.lang.String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }


    /**
     * Gets the receiverMTAccountNbrMask value for this Receiver.
     * 
     * @return receiverMTAccountNbrMask
     */
    public java.lang.String getReceiverMTAccountNbrMask() {
        return receiverMTAccountNbrMask;
    }


    /**
     * Sets the receiverMTAccountNbrMask value for this Receiver.
     * 
     * @param receiverMTAccountNbrMask
     */
    public void setReceiverMTAccountNbrMask(java.lang.String receiverMTAccountNbrMask) {
        this.receiverMTAccountNbrMask = receiverMTAccountNbrMask;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Receiver)) return false;
        Receiver other = (Receiver) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.receiverName==null && other.getReceiverName()==null) || 
             (this.receiverName!=null &&
              this.receiverName.equals(other.getReceiverName()))) &&
            ((this.receiverAddress==null && other.getReceiverAddress()==null) || 
             (this.receiverAddress!=null &&
              this.receiverAddress.equals(other.getReceiverAddress()))) &&
            ((this.receiverConsumerAccountNbr==null && other.getReceiverConsumerAccountNbr()==null) || 
             (this.receiverConsumerAccountNbr!=null &&
              this.receiverConsumerAccountNbr.equals(other.getReceiverConsumerAccountNbr()))) &&
            ((this.receiverPhone==null && other.getReceiverPhone()==null) || 
             (this.receiverPhone!=null &&
              this.receiverPhone.equals(other.getReceiverPhone()))) &&
            ((this.receiverMTAccountNbrMask==null && other.getReceiverMTAccountNbrMask()==null) || 
             (this.receiverMTAccountNbrMask!=null &&
              this.receiverMTAccountNbrMask.equals(other.getReceiverMTAccountNbrMask())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReceiverName() != null) {
            _hashCode += getReceiverName().hashCode();
        }
        if (getReceiverAddress() != null) {
            _hashCode += getReceiverAddress().hashCode();
        }
        if (getReceiverConsumerAccountNbr() != null) {
            _hashCode += getReceiverConsumerAccountNbr().hashCode();
        }
        if (getReceiverPhone() != null) {
            _hashCode += getReceiverPhone().hashCode();
        }
        if (getReceiverMTAccountNbrMask() != null) {
            _hashCode += getReceiverMTAccountNbrMask().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(" super=").append(super.toString());
		buffer.append(" ReceiverName=").append(getReceiverName());
		buffer.append(" ReceiverAddress=").append(getReceiverAddress());
		buffer.append(" ReceiverConsumerAccountNbr=").append(getReceiverConsumerAccountNbr());
		buffer.append(" ReceiverPhone=").append(getReceiverPhone());
		buffer.append(" ReceiverMTAccountNbrMask=").append(getReceiverMTAccountNbrMask());
		return buffer.toString();
}
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Receiver.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "Receiver"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverConsumerAccountNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverConsumerAccountNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverMTAccountNbrMask");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverMTAccountNbrMask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
