/**
 * UpdateTransactionStatusRequest.java
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;

/**
 * 
 * Update Transaction Status Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/09/09 21:46:35 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateTransactionStatusRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 5731771360893545574L;

    private long transactionId;

    private java.lang.String status;

    private java.lang.String subStatus;

    private java.lang.Integer authenticationScore;

    public UpdateTransactionStatusRequest() {
    }

    /**
     * Gets the transactionId value for this UpdateTransactionStatusRequest.
     * 
     * @return transactionId
     */
    public long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this UpdateTransactionStatusRequest.
     * 
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the status value for this UpdateTransactionStatusRequest.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this UpdateTransactionStatusRequest.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the subStatus value for this UpdateTransactionStatusRequest.
     * 
     * @return subStatus
     */
    public java.lang.String getSubStatus() {
        return subStatus;
    }


    /**
     * Sets the subStatus value for this UpdateTransactionStatusRequest.
     * 
     * @param subStatus
     */
    public void setSubStatus(java.lang.String subStatus) {
        this.subStatus = subStatus;
    }


    /**
     * Gets the authenticationScore value for this UpdateTransactionStatusRequest.
     * 
     * @return authenticationScore
     */
    public java.lang.Integer getAuthenticationScore() {
        return authenticationScore;
    }


    /**
     * Sets the authenticationScore value for this UpdateTransactionStatusRequest.
     * 
     * @param authenticationScore
     */
    public void setAuthenticationScore(java.lang.Integer authenticationScore) {
        this.authenticationScore = authenticationScore;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateTransactionStatusRequest)) return false;
        UpdateTransactionStatusRequest other = (UpdateTransactionStatusRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.transactionId == other.getTransactionId() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.subStatus==null && other.getSubStatus()==null) || 
             (this.subStatus!=null &&
              this.subStatus.equals(other.getSubStatus()))) &&
            ((this.authenticationScore==null && other.getAuthenticationScore()==null) || 
             (this.authenticationScore!=null &&
              this.authenticationScore.equals(other.getAuthenticationScore())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getTransactionId()).hashCode();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getSubStatus() != null) {
            _hashCode += getSubStatus().hashCode();
        }
        if (getAuthenticationScore() != null) {
            _hashCode += getAuthenticationScore().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" Status=").append(getStatus());
        buffer.append(" SubStatus=").append(getSubStatus());
        buffer.append(" AuthenticationScore=").append(getAuthenticationScore());
        return buffer.toString();
    }
}
