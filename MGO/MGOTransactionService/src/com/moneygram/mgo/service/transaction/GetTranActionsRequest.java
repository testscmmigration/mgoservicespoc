/**
 * GetTranActionsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import java.util.Calendar;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class GetTranActionsRequest  extends BaseOperationRequest   {
	private long transactionId;
	   
    private java.util.Calendar startDate;

	private java.util.Calendar endDate;

 
  public GetTranActionsRequest() {
	    }

	 
  public GetTranActionsRequest(RequestHeader header, long transactionId, Calendar startDate,
			Calendar endDate) {
		super(header);
		this.transactionId = transactionId;
		this.startDate = startDate;
		this.endDate = endDate;
	}


  /**
   * Gets the transactionId value for this GetTranActionsRequest.
   * 
   * @return transactionId
   */
  public long getTransactionId() {
      return transactionId;
  }


  /**
   * Sets the transactionId value for this GetTranActionsRequest.
   * 
   * @param transactionId
   */
  public void setTransactionId(long transactionId) {
      this.transactionId = transactionId;
  }

   public java.util.Calendar getStartDate() {
		return startDate;
	}


	public void setStartDate(java.util.Calendar startDate) {
		this.startDate = startDate;
	}

	public java.util.Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(java.util.Calendar endDate) {
		this.endDate = endDate;
	}

     private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTranActionsRequest)) return false;
        GetTranActionsRequest other = (GetTranActionsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.transactionId == other.getTransactionId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getTransactionId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

   
	@Override
	public String toString() {
		return "GetTranActionsRequest [transactionId=" + transactionId
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}
