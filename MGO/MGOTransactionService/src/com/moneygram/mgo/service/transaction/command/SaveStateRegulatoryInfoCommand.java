/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.transaction.SaveStateRegulatoryInfoRequest;
import com.moneygram.mgo.service.transaction.SaveStateRegulatoryInfoResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Save State Regulatory Info Command. (Dodd Frank)
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/03 13:55:29 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class SaveStateRegulatoryInfoCommand extends TransactionalCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(SaveStateRegulatoryInfoCommand.class);

    /**
     *
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        SaveStateRegulatoryInfoRequest commandRequest = (SaveStateRegulatoryInfoRequest) request;

        //process save request
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            dao.saveStateRegulatoryInfo(commandRequest.getStateRegulatoryInfo());
        } catch (Exception e) {
            throw new CommandException("Failed to save state regulatory info", e);
        }

        SaveStateRegulatoryInfoResponse response = new SaveStateRegulatoryInfoResponse();
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof SaveStateRegulatoryInfoRequest;
    }

}
