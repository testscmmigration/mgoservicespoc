/**
 * LegalIdType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * Legal Id Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/08 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class LegalIdType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LegalIdType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SSN = "SSN";
    public static final java.lang.String _INT = "INT";
    public static final java.lang.String _TAX = "TAX";
    public static final java.lang.String _ALN = "ALN";
    public static final LegalIdType SSN = new LegalIdType(_SSN);
    public static final LegalIdType INT = new LegalIdType(_INT);
    public static final LegalIdType TAX = new LegalIdType(_TAX);
    public static final LegalIdType ALN = new LegalIdType(_ALN);
    public java.lang.String getValue() { return _value_;}
    public static LegalIdType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LegalIdType enumeration = (LegalIdType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LegalIdType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
