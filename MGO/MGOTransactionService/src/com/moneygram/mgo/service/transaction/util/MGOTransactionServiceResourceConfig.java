/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.mgo.service.transaction.util;

import com.moneygram.common.service.util.ResourceConfig;

/**
 * 
 * MGO Consumer Service Resource Config. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOTransactionService</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.1 $ $Date: 2010/01/07 18:50:30 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: w162 $ </td>
 *        </table>
 *        </div>
 */
public class MGOTransactionServiceResourceConfig extends ResourceConfig {

	public static final String PEND_TXN_EXPIRE_TIME_HOURS = "pendingTxnExpireTimeHours";
	private static final int DEFAULT_PENDING_EXPIRE_TIME = 96;
	public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOConsumerServiceResourceReference";

	/**
	 * singleton instance.
	 */
	private static MGOTransactionServiceResourceConfig instance = null;

	/**
	 * Creates new instance of ResourceConfig
	 * 
	 */
	private MGOTransactionServiceResourceConfig() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static MGOTransactionServiceResourceConfig getInstance() {
		if (instance == null) {
			synchronized (MGOTransactionServiceResourceConfig.class) {
				if (instance == null) {
					instance = new MGOTransactionServiceResourceConfig();
					instance.initResourceConfig();
				}
			}
		}
		return instance;
	}

	public int getPendingTxnExpireTimeHours() {
		return this.getIntegerAttributeValue(PEND_TXN_EXPIRE_TIME_HOURS, DEFAULT_PENDING_EXPIRE_TIME);
	}

	@Override
	protected String getResourceConfigurationJndiName() {
		return RESOURCE_REFERENCE_JNDI;
	}

}
