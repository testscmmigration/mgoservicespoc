/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.transaction.UpdateTransactionRequest;
import com.moneygram.mgo.service.transaction.UpdateTransactionResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Update Transaction Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/04/29 16:41:03 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class UpdateTransactionCommand extends TransactionalCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(UpdateTransactionCommand.class);

    /**
     *
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        UpdateTransactionRequest commandRequest = (UpdateTransactionRequest) request;

        //process update request
        try {
            TransactionDAO dao = (TransactionDAO) getDataAccessObject();
            dao.updateTransaction(commandRequest.getTransaction(), commandRequest.getTransactionId());
        } catch (Exception e) {
            throw new CommandException("Failed to update transaction", e);
        }

        UpdateTransactionResponse response = new UpdateTransactionResponse();
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof UpdateTransactionRequest;
    }

}
