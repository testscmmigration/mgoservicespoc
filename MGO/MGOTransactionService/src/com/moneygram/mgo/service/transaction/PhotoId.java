/**
 * PhotoId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * Photo Id.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/08 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class PhotoId  implements java.io.Serializable {
    private PhotoIdType type;

    private java.lang.String issueCountry;

    private java.lang.String issueState;

    private java.lang.String number;
    
    private java.util.Calendar expiryDate;

    public PhotoId() {
    }

    public PhotoId(
           PhotoIdType type,
           java.lang.String issueCountry,
           java.lang.String issueState,
           java.lang.String number) {
           this.type = type;
           this.issueCountry = issueCountry;
           this.issueState = issueState;
           this.number = number;
    }
    
    public PhotoId(
            PhotoIdType type,
            java.lang.String issueCountry,
            java.lang.String issueState,
            java.lang.String number,
            java.util.Calendar expiryDate) {
            this.type = type;
            this.issueCountry = issueCountry;
            this.issueState = issueState;
            this.number = number;
            this.expiryDate = expiryDate;
     }


    /**
     * Gets the type value for this PhotoId.
     * 
     * @return type
     */
    public PhotoIdType getType() {
        return type;
    }


    /**
     * Sets the type value for this PhotoId.
     * 
     * @param type
     */
    public void setType(PhotoIdType type) {
        this.type = type;
    }


    /**
     * Gets the issueCountry value for this PhotoId.
     * 
     * @return issueCountry
     */
    public java.lang.String getIssueCountry() {
        return issueCountry;
    }


    /**
     * Sets the issueCountry value for this PhotoId.
     * 
     * @param issueCountry
     */
    public void setIssueCountry(java.lang.String issueCountry) {
        this.issueCountry = issueCountry;
    }


    /**
     * Gets the issueState value for this PhotoId.
     * 
     * @return issueState
     */
    public java.lang.String getIssueState() {
        return issueState;
    }


    /**
     * Sets the issueState value for this PhotoId.
     * 
     * @param issueState
     */
    public void setIssueState(java.lang.String issueState) {
        this.issueState = issueState;
    }


    /**
     * Gets the number value for this PhotoId.
     * 
     * @return number
     */
    public java.lang.String getNumber() {
        return number;
    }
    
    
	/**
     * Sets the number value for this PhotoId.
     * 
     * @param number
     */
    public void setNumber(java.lang.String number) {
        this.number = number;
    }
    
    /**
     * @return java.util.Calendar
     */
    public java.util.Calendar getExpiryDate() {
		return expiryDate;
	}

    /**
     * @param expiryDate
     */
	public void setExpiryDate(java.util.Calendar expiryDate) {
		this.expiryDate = expiryDate;
	}

    

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((expiryDate == null) ? 0 : expiryDate.hashCode());
		result = prime * result
				+ ((issueCountry == null) ? 0 : issueCountry.hashCode());
		result = prime * result
				+ ((issueState == null) ? 0 : issueState.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhotoId other = (PhotoId) obj;
		if (expiryDate == null) {
			if (other.expiryDate != null)
				return false;
		} else if (!expiryDate.equals(other.expiryDate))
			return false;
		if (issueCountry == null) {
			if (other.issueCountry != null)
				return false;
		} else if (!issueCountry.equals(other.issueCountry))
			return false;
		if (issueState == null) {
			if (other.issueState != null)
				return false;
		} else if (!issueState.equals(other.issueState))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Type=").append(getType());
        buffer.append(" IssueCountry=").append(getIssueCountry());
        buffer.append(" IssueState=").append(getIssueState());
        buffer.append(" Number=").append(getNumber());
        buffer.append(" ExpiryDate=").append(getExpiryDate());
        return buffer.toString();
    }
}
