/**
 * GetConsumerTransactionsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationResponse;

/**
 * 
 * Get Consumer Transactions Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/07/24 16:34:48 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetConsumerTransactionsResponse  extends BaseOperationResponse {
    private static final long serialVersionUID = -1678345247407279444L;

    private TransactionSummary[] transactions;

    public GetConsumerTransactionsResponse() {
    }

    /**
     * Gets the transactions value for this GetConsumerTransactionsResponse.
     * 
     * @return transactions
     */
    public TransactionSummary[] getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this GetConsumerTransactionsResponse.
     * 
     * @param transactions
     */
    public void setTransactions(TransactionSummary[] transactions) {
        this.transactions = transactions;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsumerTransactionsResponse)) return false;
        GetConsumerTransactionsResponse other = (GetConsumerTransactionsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              java.util.Arrays.equals(this.transactions, other.getTransactions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransactions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransactions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransactions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" Transactions="+ getTransactions();
    }

}
