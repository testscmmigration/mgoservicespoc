/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetCCAuthConfIdRequest;
import com.moneygram.mgo.service.transaction.GetCCAuthConfIdResponse;
import com.moneygram.mgo.service.transaction.TranActionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get Max State Regulatory Version Command. (Dodd Frank)
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/03 21:17:36 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class GetCCAuthConfIdCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetCCAuthConfIdCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }
        String CCConfId = null;
        String reasonCode = null;

        GetCCAuthConfIdRequest commandRequest = (GetCCAuthConfIdRequest) request;
        List<TranActionSummary> tempTranActions = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            tempTranActions = dao.getTranActions(commandRequest.getTransactionId(), commandRequest.getStartDate(), commandRequest.getEndDate());

            for (TranActionSummary tranAction : tempTranActions) {
                reasonCode = tranAction.getTranReasonCode();
                if (reasonCode != null) {
                    if (reasonCode.trim().equals("140")) {
                        CCConfId = tranAction.getTranConfId();
                        break;
                    }
                }
            }
            if ((CCConfId==null) || (CCConfId.length() ==0)) {
                for (TranActionSummary tranAction1 : tempTranActions) {
                    reasonCode = tranAction1.getTranReasonCode();
                    if (reasonCode != null) {
                        if (reasonCode.trim().equals("131")) {
                            CCConfId = tranAction1.getTranConfId();
                            break;
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve max state regulatory content version", e);
        }

        GetCCAuthConfIdResponse response = new GetCCAuthConfIdResponse();
        response.setCCAuthConfId(CCConfId);
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetCCAuthConfIdRequest;
    }

}
