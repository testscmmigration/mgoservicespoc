/**
 * CountConsumerTransactionsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationResponse;

/**
 * 
 * Count Consumer Transactions Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/04/25 04:57:25 $ </td><tr><td>
 * @author   </td><td>$Author: uo87 $ </td>
 *</table>
 *</div>
 */
public class CountConsumerTransactionsResponse  extends BaseOperationResponse {
    private static final long serialVersionUID = -1678345247407279444L;

    private Integer transactionCount;

    public CountConsumerTransactionsResponse() {
    }

    /**
     * Gets the transactionCount value for this CountConsumerTransactionsResponse.
     * 
     * @return transactionCount
     */
    public Integer getTransactionCount() {
        return transactionCount;
    }


    /**
     * Sets the transactionCount value for this CountConsumerTransactionsResponse.
     * 
     * @param transactionCount
     */
    public void setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CountConsumerTransactionsResponse)) return false;
        CountConsumerTransactionsResponse other = (CountConsumerTransactionsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transactionCount==null && other.getTransactionCount()==null) || 
             (this.transactionCount!=null &&
              (this.transactionCount.intValue() == other.getTransactionCount().intValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransactionCount() != null) {
            _hashCode += getTransactionCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" TransactionCount="+ getTransactionCount();
    }

}
