/*
 * Created on Dec 10, 2007
 *
 */
package com.moneygram.mgo.service.transaction.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.MGOTransaction;
import com.moneygram.mgo.service.transaction.MGOTransactionProcessing;
import com.moneygram.mgo.service.transaction.Receiver;
import com.moneygram.mgo.service.transaction.TransactionAmount;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * Transaction Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2010/07/27 14:52:26 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TransactionRowMapper extends BaseRowMapper {

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        /*
        emg_tran_id        number(9)       out
        emg_tran_type_code     varchar2(8)     out
        lgcy_ref_nbr       varchar2(8)     out
        snd_tran_date      date            out
        tran_stat_code     char(3)         out
        tran_sub_stat_code     char(3)         out
        tran_stat_date     date            out
        snd_iso_crncy_id       varchar2(3)     out
        snd_face_amt       number(15,3)        out
        rcv_iso_cntry_code     varchar2(3)     out
        rcv_cust_frst_name     varchar2(40)        out
        rcv_cust_last_name     varchar2(40)        out
        rcv_cust_matrnl_name   varchar2(40)        out
        rcv_cust_mid_name      varchar2(40)        out
        intnd_dest_stateprovince_code varchar2(2)         out
            */

        MGOTransactionProcessing transactionProcessing = new MGOTransactionProcessing();
        transactionProcessing.setTransactionId(rs.getLong("emg_tran_id"));
        transactionProcessing.setReferenceNumber(rs.getString("lgcy_ref_nbr"));
        transactionProcessing.setSendDate(DAOUtils.toCalendar(rs.getTimestamp("snd_tran_date")));
        transactionProcessing.setStatusUpdateDate(DAOUtils.toCalendar(rs.getTimestamp("tran_stat_date")));
        
        TransactionAmount transactionAmount = new TransactionAmount();
        transactionAmount.setFaceAmount(rs.getBigDecimal("snd_face_amt"));

        ReceiverConsumerName receiverName = new ReceiverConsumerName();
        receiverName.setFirstName(rs.getString("rcv_cust_frst_name"));
        receiverName.setLastName(rs.getString("rcv_cust_last_name"));
        receiverName.setMaternalName(rs.getString("rcv_cust_matrnl_name"));
        receiverName.setMiddleName(rs.getString("rcv_cust_mid_name"));

        Receiver receiver = new Receiver();
        receiver.setReceiverName(receiverName);
       
        MGOTransaction transaction = new MGOTransaction();
        transaction.setMgoProcessing(transactionProcessing);
        transaction.setProductType(MGOProductType.fromString(rs.getString("emg_tran_type_code")));
        transaction.setSendCurrency(rs.getString("snd_iso_crncy_id"));
        transaction.setTransactionAmount(transactionAmount);
        transaction.setReceiveCountry(rs.getString("rcv_iso_cntry_code"));
        transaction.setReceiver(receiver);
        transaction.setReceiveState(rs.getString("intnd_dest_stateprovince_code"));
        transaction.setStatus(rs.getString("tran_stat_code"));
        transaction.setSubStatus(rs.getString("tran_sub_stat_code"));

        transaction.setSourceSite(rs.getString("src_web_site_bsns_cd"));

        return transaction;
    }
    

}
