/**
 * GetStateRegulatoryInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationResponse;


public class GetStateRegulatoryInfoResponse  extends BaseOperationResponse{
	 private com.moneygram.mgo.service.transaction.StateRegulatoryInfo[] stateRegulatoryInfoList;

	    public GetStateRegulatoryInfoResponse() {
	    }

	    


	    /**
	     * Gets the stateRegulatoryInfoList value for this GetStateRegulatoryInfoResponse.
	     * 
	     * @return stateRegulatoryInfoList
	     */
	    public com.moneygram.mgo.service.transaction.StateRegulatoryInfo[] getStateRegulatoryInfoList() {
	        return stateRegulatoryInfoList;
	    }


	    /**
	     * Sets the stateRegulatoryInfoList value for this GetStateRegulatoryInfoResponse.
	     * 
	     * @param stateRegulatoryInfoList
	     */
	    public void setStateRegulatoryInfoList(com.moneygram.mgo.service.transaction.StateRegulatoryInfo[] stateRegulatoryInfoList) {
	        this.stateRegulatoryInfoList = stateRegulatoryInfoList;
	    }

	    private java.lang.Object __equalsCalc = null;
	    public synchronized boolean equals(java.lang.Object obj) {
	        if (!(obj instanceof GetStateRegulatoryInfoResponse)) return false;
	        GetStateRegulatoryInfoResponse other = (GetStateRegulatoryInfoResponse) obj;
	        if (obj == null) return false;
	        if (this == obj) return true;
	        if (__equalsCalc != null) {
	            return (__equalsCalc == obj);
	        }
	        __equalsCalc = obj;
	        boolean _equals;
	        _equals = super.equals(obj) && 
	            ((this.stateRegulatoryInfoList==null && other.getStateRegulatoryInfoList()==null) || 
	             (this.stateRegulatoryInfoList!=null &&
	              java.util.Arrays.equals(this.stateRegulatoryInfoList, other.getStateRegulatoryInfoList())));
	        __equalsCalc = null;
	        return _equals;
	    }

	    private boolean __hashCodeCalc = false;
	    public synchronized int hashCode() {
	        if (__hashCodeCalc) {
	            return 0;
	        }
	        __hashCodeCalc = true;
	        int _hashCode = super.hashCode();
	        if (getStateRegulatoryInfoList() != null) {
	            for (int i=0;
	                 i<java.lang.reflect.Array.getLength(getStateRegulatoryInfoList());
	                 i++) {
	                java.lang.Object obj = java.lang.reflect.Array.get(getStateRegulatoryInfoList(), i);
	                if (obj != null &&
	                    !obj.getClass().isArray()) {
	                    _hashCode += obj.hashCode();
	                }
	            }
	        }
	        __hashCodeCalc = false;
	        return _hashCode;
	    }

	    // Type metadata
	    private static org.apache.axis.description.TypeDesc typeDesc =
	        new org.apache.axis.description.TypeDesc(GetStateRegulatoryInfoResponse.class, true);

	    static {
	        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "GetStateRegulatoryInfoResponse"));
	        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
	        elemField.setFieldName("stateRegulatoryInfoList");
	        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "stateRegulatoryInfoList"));
	        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "StateRegulatoryInfo"));
	        elemField.setMinOccurs(0);
	        elemField.setNillable(false);
	        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "item"));
	        typeDesc.addFieldDesc(elemField);
	    }

	    /**
	     * Return type metadata object
	     */
	    public static org.apache.axis.description.TypeDesc getTypeDesc() {
	        return typeDesc;
	    }

	    /**
	     * Get Custom Serializer
	     */
	    public static org.apache.axis.encoding.Serializer getSerializer(
	           java.lang.String mechType, 
	           java.lang.Class _javaType,  
	           javax.xml.namespace.QName _xmlType) {
	        return 
	          new  org.apache.axis.encoding.ser.BeanSerializer(
	            _javaType, _xmlType, typeDesc);
	    }

	    /**
	     * Get Custom Deserializer
	     */
	    public static org.apache.axis.encoding.Deserializer getDeserializer(
	           java.lang.String mechType, 
	           java.lang.Class _javaType,  
	           javax.xml.namespace.QName _xmlType) {
	        return 
	          new  org.apache.axis.encoding.ser.BeanDeserializer(
	            _javaType, _xmlType, typeDesc);
	    }


}
