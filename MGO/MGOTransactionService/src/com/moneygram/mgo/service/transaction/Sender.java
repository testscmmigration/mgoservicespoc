/**
 * Sender.java
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * Sender.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/08/14 21:34:35 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class Sender  implements java.io.Serializable {
    private static final long serialVersionUID = -44756899732539044L;

    private long senderConsumerId;

    private java.lang.String senderIPAddress;

    private java.lang.String senderLoyaltyMemberId;

    private PhotoId senderPhotoId;

    private LegalId senderLegalId;

    private java.lang.String senderOccupation;

    private java.lang.Long fiAccountId;

    private java.lang.Long backupFIAccountId;

    public Sender() {
    }

    public Sender(
           long senderConsumerId,
           java.lang.String senderIPAddress,
           java.lang.String senderLoyaltyMemberId,
           PhotoId senderPhotoId,
           LegalId senderLegalId,
           java.lang.String senderOccupation,
           java.lang.Long fiAccountId,
           java.lang.Long backupFIAccountId) {
           this.senderConsumerId = senderConsumerId;
           this.senderIPAddress = senderIPAddress;
           this.senderLoyaltyMemberId = senderLoyaltyMemberId;
           this.senderPhotoId = senderPhotoId;
           this.senderLegalId = senderLegalId;
           this.senderOccupation = senderOccupation;
           this.fiAccountId = fiAccountId;
           this.backupFIAccountId = backupFIAccountId;
    }


    /**
     * Gets the senderConsumerId value for this Sender.
     * 
     * @return senderConsumerId
     */
    public long getSenderConsumerId() {
        return senderConsumerId;
    }


    /**
     * Sets the senderConsumerId value for this Sender.
     * 
     * @param senderConsumerId
     */
    public void setSenderConsumerId(long senderConsumerId) {
        this.senderConsumerId = senderConsumerId;
    }


    /**
     * Gets the senderIPAddress value for this Sender.
     * 
     * @return senderIPAddress
     */
    public java.lang.String getSenderIPAddress() {
        return senderIPAddress;
    }


    /**
     * Sets the senderIPAddress value for this Sender.
     * 
     * @param senderIPAddress
     */
    public void setSenderIPAddress(java.lang.String senderIPAddress) {
        this.senderIPAddress = senderIPAddress;
    }


    /**
     * Gets the senderLoyaltyMemberId value for this Sender.
     * 
     * @return senderLoyaltyMemberId
     */
    public java.lang.String getSenderLoyaltyMemberId() {
        return senderLoyaltyMemberId;
    }


    /**
     * Sets the senderLoyaltyMemberId value for this Sender.
     * 
     * @param senderLoyaltyMemberId
     */
    public void setSenderLoyaltyMemberId(java.lang.String senderLoyaltyMemberId) {
        this.senderLoyaltyMemberId = senderLoyaltyMemberId;
    }


    /**
     * Gets the senderPhotoId value for this Sender.
     * 
     * @return senderPhotoId
     */
    public PhotoId getSenderPhotoId() {
        return senderPhotoId;
    }


    /**
     * Sets the senderPhotoId value for this Sender.
     * 
     * @param senderPhotoId
     */
    public void setSenderPhotoId(PhotoId senderPhotoId) {
        this.senderPhotoId = senderPhotoId;
    }


    /**
     * Gets the senderLegalId value for this Sender.
     * 
     * @return senderLegalId
     */
    public LegalId getSenderLegalId() {
        return senderLegalId;
    }


    /**
     * Sets the senderLegalId value for this Sender.
     * 
     * @param senderLegalId
     */
    public void setSenderLegalId(LegalId senderLegalId) {
        this.senderLegalId = senderLegalId;
    }


    /**
     * Gets the senderOccupation value for this Sender.
     * 
     * @return senderOccupation
     */
    public java.lang.String getSenderOccupation() {
        return senderOccupation;
    }


    /**
     * Sets the senderOccupation value for this Sender.
     * 
     * @param senderOccupation
     */
    public void setSenderOccupation(java.lang.String senderOccupation) {
        this.senderOccupation = senderOccupation;
    }


    /**
     * Gets the fiAccountId value for this Sender.
     * 
     * @return fiAccountId
     */
    public java.lang.Long getFiAccountId() {
        return fiAccountId;
    }


    /**
     * Sets the fiAccountId value for this Sender.
     * 
     * @param fiAccountId
     */
    public void setFiAccountId(java.lang.Long fiAccountId) {
        this.fiAccountId = fiAccountId;
    }


    /**
     * Gets the backupFIAccountId value for this Sender.
     * 
     * @return backupFIAccountId
     */
    public java.lang.Long getBackupFIAccountId() {
        return backupFIAccountId;
    }


    /**
     * Sets the backupFIAccountId value for this Sender.
     * 
     * @param backupFIAccountId
     */
    public void setBackupFIAccountId(java.lang.Long backupFIAccountId) {
        this.backupFIAccountId = backupFIAccountId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Sender)) return false;
        Sender other = (Sender) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.senderConsumerId == other.getSenderConsumerId() &&
            ((this.senderIPAddress==null && other.getSenderIPAddress()==null) || 
             (this.senderIPAddress!=null &&
              this.senderIPAddress.equals(other.getSenderIPAddress()))) &&
            ((this.senderLoyaltyMemberId==null && other.getSenderLoyaltyMemberId()==null) || 
             (this.senderLoyaltyMemberId!=null &&
              this.senderLoyaltyMemberId.equals(other.getSenderLoyaltyMemberId()))) &&
            ((this.senderPhotoId==null && other.getSenderPhotoId()==null) || 
             (this.senderPhotoId!=null &&
              this.senderPhotoId.equals(other.getSenderPhotoId()))) &&
            ((this.senderLegalId==null && other.getSenderLegalId()==null) || 
             (this.senderLegalId!=null &&
              this.senderLegalId.equals(other.getSenderLegalId()))) &&
            ((this.senderOccupation==null && other.getSenderOccupation()==null) || 
             (this.senderOccupation!=null &&
              this.senderOccupation.equals(other.getSenderOccupation()))) &&
            ((this.fiAccountId==null && other.getFiAccountId()==null) || 
             (this.fiAccountId!=null &&
              this.fiAccountId.equals(other.getFiAccountId()))) &&
            ((this.backupFIAccountId==null && other.getBackupFIAccountId()==null) || 
             (this.backupFIAccountId!=null &&
              this.backupFIAccountId.equals(other.getBackupFIAccountId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getSenderConsumerId()).hashCode();
        if (getSenderIPAddress() != null) {
            _hashCode += getSenderIPAddress().hashCode();
        }
        if (getSenderLoyaltyMemberId() != null) {
            _hashCode += getSenderLoyaltyMemberId().hashCode();
        }
        if (getSenderPhotoId() != null) {
            _hashCode += getSenderPhotoId().hashCode();
        }
        if (getSenderLegalId() != null) {
            _hashCode += getSenderLegalId().hashCode();
        }
        if (getSenderOccupation() != null) {
            _hashCode += getSenderOccupation().hashCode();
        }
        if (getFiAccountId() != null) {
            _hashCode += getFiAccountId().hashCode();
        }
        if (getBackupFIAccountId() != null) {
            _hashCode += getBackupFIAccountId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" SenderConsumerId=").append(getSenderConsumerId());
        buffer.append(" SenderIPAddress=").append(getSenderIPAddress());
        buffer.append(" SenderLoyaltyMemberId=").append(getSenderLoyaltyMemberId());
        buffer.append(" SenderPhotoId=").append(getSenderPhotoId());
        buffer.append(" SenderLegalId=").append(getSenderLegalId());
        buffer.append(" SenderOccupation=").append(getSenderOccupation());
        buffer.append(" FiAccountId=").append(getFiAccountId());
        buffer.append(" BackupFIAccountId=").append(getBackupFIAccountId());
        return buffer.toString();
    }
}
