/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.transaction.dao;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.common.dao.DAOException;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.PanIdInfo;
import com.moneygram.common.util.PanIdentification;
import com.moneygram.mgo.service.consumer.util.ConsumerUtil;
import com.moneygram.mgo.service.transaction.InitiatedTransaction;
import com.moneygram.mgo.service.transaction.MGOTransaction;
import com.moneygram.mgo.service.transaction.StateRegulatoryInfo;
import com.moneygram.mgo.service.transaction.TranActionSummary;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.UpdateAccountRequest;
import com.moneygram.mgo.service.transaction.UpdateTransactionStatusRequest;
import com.moneygram.mgo.service.transaction.util.MGOTransactionServiceResourceConfig;

/**
 *
 * Transaction DAO.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.31 $ $Date: 2011/09/18 00:23:40 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class TransactionDAO extends BaseDAO {
    private static final Logger logger = LogFactory.getInstance().getLogger(TransactionDAO.class);

    private static final String INSERT_TRANSACTION_PROC = "pkg_mgo_transactions.PRC_INSERT_TRANSACTION";
    private static final String UPDATE_TRANSACTION_PROC = "pkg_mgo_transactions.PRC_UPDATE_TRANSACTION";
    private static final String GET_TRANSACTION_PROC = "pkg_mgo_transactions.PRC_GET_EMG_TRAN_DETAILS_CV";
    private static final String GET_TRANSACTIONS_BY_CONSUMER_PROC = "pkg_mgo_transactions.PRC_GET_EMG_TRAN_LIST_CV";
    private static final String GET_TRAN_ACTIONS_BY_TRAN_ID_PROC = "pkg_mgo_transactions.prc_get_emg_tran_actions_cv";
    private static final String COUNT_TRANSACTIONS_BY_CONSUMER_PROC = "pkg_mgo_transactions.prc_get_emg_tran_count";
    private static final String UPDATE_TRANSACTION_STATUS_PROC = "pkg_mgo_transactions.PRC_SET_TRAN_STAT_CODE";
    private static final String UPDATE_TRANSACTION_ACCOUNT_PROC = "pkg_mgo_transactions.PRC_SET_TRAN_ACCT_AND_STAT_CD";
    private static final String GET_TRANSACTIONS_TOTAL_PROC = "pkg_mgo_transactions.prc_get_last_30_days_trans_cv";
    
    
    //Start Dodd Frank
    private static final String INSERT_STATEREGINFO_PROC = "pkg_mgo_transactions.prc_insert_state_reg_info";
    private static final String GET_STATEREGINFO_PROC = "pkg_mgo_transactions.prc_get_state_reg_info_cv";
    private static final String GET_MAXSTATEREG_VERSION_PROC = "pkg_mgo_transactions.prc_get_max_state_reg_version";
    //End Dodd Frank

    //vl58, MGO-1265 : new operation for inserting transaction comments
    private static final String INSERT_TRANSACTION_COMMENT = "pkg_mgo_transactions.prc_insrt_emg_tran_comment";

    private static final String DB_CALLER_ID = "MGO";

    /**
     * Returns procedure log id.
     * @param result
     * @return procedure log id.
     */
    private Number getProcedureLogId(Map result) {
        return (Number)result.get("ov_prcs_log_id");
    }

    private void addCallTypeParameter(MapSqlParameterSource parameters) {
        parameters.addValue("iv_call_type_code", "MGO");
    }
    
    public List<TransactionSummary> getTransactions(Number consumerId, Calendar startDate, Calendar endDate, int maxRecords, String receiveAgentCode, String sendCountry, String receiveCountry, String productTypes, String partnerSiteId) throws DAOException {
    	return getTransactions(consumerId, startDate, endDate, maxRecords, receiveAgentCode, sendCountry, receiveCountry, productTypes, partnerSiteId, null, null);
    }
    /**
     * Returns list of transactions for consumer.
     * @param consumerId
     * @param startDate
     * @param endDate
     * @param maxRecords
     * @param receiveAgentCode
     * @param productTypes
     * @return list of transactions for consumer.
     * @throws DAOException
     */
    public List<TransactionSummary> getTransactions(Number consumerId, Calendar startDate, Calendar endDate, int maxRecords, String receiveAgentCode, String sendCountry, String receiveCountry, String productTypes, String partnerSiteId, Integer deliveryOption, Long receiveAgent) throws DAOException {

        return getTransactions(consumerId, (startDate == null ? null : startDate.getTime()),
                (endDate == null ? null : endDate.getTime()), maxRecords, receiveAgentCode, sendCountry, receiveCountry, productTypes, partnerSiteId, deliveryOption, receiveAgent);
    }

    public List<TransactionSummary> getTransactions(Number consumerId, Date startDate, Date endDate, int maxRecords, String receiveAgentCode, String sendCountry, String receiveCountry, String productTypes, String partnerSiteId) throws DAOException {
    	 return getTransactions(consumerId, startDate, endDate, maxRecords, receiveAgentCode, sendCountry, receiveCountry, productTypes, partnerSiteId, null, null);
    }
    /**
     * Returns list of transactions for consumer.
     * @param consumerId
     * @param startDate
     * @param endDate
     * @param receiveAgentCode
     * @param productTypes
     *
     * @return list of transactions for consumer.
     * @throws DAOException
     */
    public List<TransactionSummary> getTransactions(Number consumerId, Date startDate, Date endDate, int maxRecords, String receiveAgentCode, String sendCountry, String receiveCountry, String productTypes, String partnerSiteId, Integer deliveryOption, Long receiveAgent) throws DAOException {
        SimpleJdbcCall getTransactionsByConsumer =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_TRANSACTIONS_BY_CONSUMER_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_snd_cust_id", Types.INTEGER),
                        new SqlParameter("iv_begin_date", Types.TIMESTAMP),
                        new SqlParameter("iv_end_date", Types.TIMESTAMP),
                        new SqlParameter("iv_rec_count", Types.INTEGER),
                        new SqlParameter("iv_rcv_agcy_code", Types.VARCHAR),
                        new SqlParameter("iv_emg_tran_type_code", Types.VARCHAR), //list of product types
                        new SqlParameter("iv_bsns_code", Types.VARCHAR), //partner site id
                        new SqlParameter("iv_pending_tran_beg_date", Types.TIMESTAMP), 
	                    new SqlParameter("iv_rcv_iso_cntry_code", Types.VARCHAR),
                        new SqlParameter("iv_dlvr_optn_id", Types.INTEGER),
                        new SqlParameter("iv_rcv_agent_id", Types.INTEGER),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_emg_tran_list_cv", OracleTypes.CURSOR, new TransactionSummaryRowMapper())
                        );

        //Get start date for retrieving pendings - if earlier than requested start date, set it to the same value
        int pendTxnExpHours = MGOTransactionServiceResourceConfig.getInstance().getPendingTxnExpireTimeHours();
        Date pendTxnStartDate = getPendingStartDate(pendTxnExpHours);
        if (startDate != null && pendTxnStartDate.before(startDate)) {
        	pendTxnStartDate = startDate;
        }

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        addCallTypeParameter(parameters);

        parameters.addValue("iv_snd_cust_id", consumerId);
        parameters.addValue("iv_begin_date", startDate);
        parameters.addValue("iv_end_date", endDate);
        parameters.addValue("iv_rec_count", maxRecords);       
        parameters.addValue("iv_rcv_agcy_code", receiveAgentCode);
        parameters.addValue("iv_emg_tran_type_code", productTypes);
        parameters.addValue("iv_bsns_code", partnerSiteId);
        parameters.addValue("iv_pending_tran_beg_date", pendTxnStartDate);
	    parameters.addValue("iv_rcv_iso_cntry_code", receiveCountry);
        parameters.addValue("iv_dlvr_optn_id", deliveryOption);
        parameters.addValue("iv_rcv_agent_id", receiveAgent);
        

        if (logger.isDebugEnabled()) {
            logger.debug("getTransactions: calling procedure="+ GET_TRANSACTIONS_BY_CONSUMER_PROC +" with "+ parameters.getValues().size() +" input parameters including consumerId="+ consumerId +" productTypes="+ productTypes);
        }

        Map result = null;
        try {
            result = getTransactionsByConsumer.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve transactions for consumerId="+ consumerId, e);
        }

        List<TransactionSummary> transactions = (List<TransactionSummary>)result.get("ov_emg_tran_list_cv");
        
        if (logger.isDebugEnabled()) {
            logger.debug("getTransactions: procedure log id="+ getProcedureLogId(result) +" transactions="+ (transactions == null ? null : transactions.size()));
        }

        return transactions;
    }

    /**
     * Returns list of Items from tranAction table for a given EMG TranId.
     * @param tranId
     * @param startDate
     * @param endDate
     * @return list of tranActions for TranId.
     * @throws DAOException
     */
    public List<TranActionSummary> getTranActions(Number tranId, Calendar startDate, Calendar endDate) throws DAOException {

        return getTranActions(tranId, (startDate == null ? null : startDate.getTime()),
                (endDate == null ? null : endDate.getTime()));
    }

    /**
     * Returns list of transactions for consumer.
     * @param tranId
     * @param startDate
     * @param endDate
     *
     * @return list of tranActions for transaction.
     * @throws DAOException
     */
    public List<TranActionSummary> getTranActions(Number tranId, Date startDate, Date endDate) throws DAOException {
        SimpleJdbcCall getTranActions =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_TRAN_ACTIONS_BY_TRAN_ID_PROC )
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_emg_tran_id", Types.INTEGER),
                        new SqlParameter("iv_begin_date", Types.TIMESTAMP),
                        new SqlParameter("iv_end_date", Types.TIMESTAMP),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_tran_action_cv", OracleTypes.CURSOR, new TranActionSummaryRowMapper())
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("iv_user_id", null); //TODO: should it be consumer_id: Unique, system assigned identifier of a comment or narrative text entry pertaining to a Customer Account.
        addCallTypeParameter(parameters);
        parameters.addValue("iv_emg_tran_id", tranId);
        parameters.addValue("iv_begin_date", startDate);
        parameters.addValue("iv_end_date", endDate);

        if (logger.isDebugEnabled()) {
            logger.debug("getTransactions: calling procedure="+ GET_TRAN_ACTIONS_BY_TRAN_ID_PROC  +" with "+ parameters.getValues().size() +" input parameters including tranId="+ tranId );
        }

        Map result = null;
        try {
            result = getTranActions.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve transactions for tranId="+ tranId, e);
        }

        List<TranActionSummary> tranActions = (List<TranActionSummary>)result.get("ov_tran_action_cv");

        if (logger.isDebugEnabled()) {
            logger.debug("getTranActions: procedure log id="+ getProcedureLogId(result) +" tranActions="+ (tranActions == null ? null : tranActions.size()));
        }

        return tranActions;
    }

    /**
     * Returns count of transactions for consumer.
     * @param consumerId
     * @param startDate
     * @param endDate
     * @param maxRecords
     * @param sendCountry
     * @param receiveCountry
     * @param productTypes
     * @return list of transactions for consumer.
     * @throws DAOException
     */
    public int countTransactions(Number consumerId, Calendar startDate, Calendar endDate, String sendCountry, String receiveCountry, String productTypes) throws DAOException {

        return countTransactions(consumerId, (startDate == null ? null : startDate.getTime()),
                (endDate == null ? null : endDate.getTime()), sendCountry, receiveCountry, productTypes);
    }

	/**
	 * Returns count of transactions for consumer.
	 * @param consumerId
	 * @param startDate
	 * @param endDate
	 * @param sendCountry
	 * @param receiveCountry
	 * @param productTypes
	 *
	 * @return count of transactions for consumer.
	 * @throws DAOException
	 */
	public int countTransactions(Number consumerId, Date startDate, Date endDate, String sendCountry, String receiveCountry, String productTypes) throws DAOException {
	    SimpleJdbcCall countTransactionsByConsumer =
	        new SimpleJdbcCall(getJdbcTemplate())
	                .withProcedureName(COUNT_TRANSACTIONS_BY_CONSUMER_PROC)
	                .withoutProcedureColumnMetaDataAccess()
	                .declareParameters(
	                    new SqlParameter("iv_call_type_code", Types.VARCHAR),
	                    new SqlParameter("iv_snd_cust_id", Types.INTEGER),
	                    new SqlParameter("iv_begin_date", Types.TIMESTAMP),
	                    new SqlParameter("iv_end_date", Types.TIMESTAMP),
	                    new SqlParameter("iv_snd_iso_cntry_code", Types.VARCHAR),
	                    new SqlParameter("iv_rcv_iso_cntry_code", Types.VARCHAR),
	                    new SqlParameter("iv_emg_tran_type_code", Types.VARCHAR), //list of product types
	                    new SqlParameter("iv_pending_tran_beg_date", Types.TIMESTAMP),
	                    new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
	                    new SqlOutParameter("ov_emg_tran_count", OracleTypes.INTEGER, new TransactionSummaryRowMapper())
	                    );

	    //Get start date for retrieving pendings - if earlier than requested start date, set it to the same value
	    int pendTxnExpHours = MGOTransactionServiceResourceConfig.getInstance().getPendingTxnExpireTimeHours();
	    Date pendTxnStartDate = getPendingStartDate(pendTxnExpHours);
	    if (startDate != null && pendTxnStartDate.before(startDate)) {
	    	pendTxnStartDate = startDate;
	    }

	    MapSqlParameterSource parameters = new MapSqlParameterSource();
	    addCallTypeParameter(parameters);

	    parameters.addValue("iv_snd_cust_id", consumerId);
	    parameters.addValue("iv_begin_date", startDate);
	    parameters.addValue("iv_end_date", endDate);
	    parameters.addValue("iv_snd_iso_cntry_code", sendCountry);
	    parameters.addValue("iv_rcv_iso_cntry_code", receiveCountry);
	    parameters.addValue("iv_emg_tran_type_code", productTypes);
	    parameters.addValue("iv_pending_tran_beg_date", pendTxnStartDate);

	    if (logger.isDebugEnabled()) {
	        logger.debug("countTransactions: calling procedure="+ COUNT_TRANSACTIONS_BY_CONSUMER_PROC +" with "+ parameters.getValues().size() +" input parameters including consumerId="+ consumerId +", productTypes="+ productTypes + ", startDate=" + startDate + ", endDate=" + endDate + ", sendLocation=" + sendCountry + ", receiveLocation=" + receiveCountry);
	    }

	    Map result = null;
	    try {
	        result = countTransactionsByConsumer.execute(parameters);
	    } catch (Exception e) {
	        throw new DAOException("Failed to retrieve transaction count for consumerId="+ consumerId, e);
	    }

	    Integer transactionCount =
//	    	20;
	    	((Integer)result.get("ov_emg_tran_count")).intValue();

	    if (logger.isDebugEnabled()) {
	        logger.debug("countTransactions: procedure log id="+ getProcedureLogId(result) +" transactions="+ (transactionCount == null ? null : transactionCount));
	    }

	    return transactionCount;
	}

	public static Date getPendingStartDate(int pendTxnExpHours) {
		Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, pendTxnExpHours * -1);
        return cal.getTime();
	}

    /**
     *
     * @param transactionId
     * @return
     * @throws DAOException
     */
    public MGOTransaction getTransaction(Number transactionId) throws DAOException {
        SimpleJdbcCall getTransactionDetails =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_TRANSACTION_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_emg_tran_id", Types.INTEGER),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_transaction_cv", OracleTypes.CURSOR, new TransactionDetailsRowMapper())
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_emg_tran_id", transactionId);
        addCallTypeParameter(parameters);

        if (logger.isDebugEnabled()) {
            logger.debug("getTransaction: calling procedure="+ GET_TRANSACTION_PROC +" with "+ parameters.getValues().size() +" input parameters. TransactionId="+ transactionId);
        }

        Map result = null;
        try {
            result = getTransactionDetails.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve transaction with TransactionId="+ transactionId, e);
        }

        List transactions = (List)result.get("ov_transaction_cv");

        if (logger.isDebugEnabled()) {
            logger.debug("getTransaction: procedure log id="+ getProcedureLogId(result));
        }

        return transactions == null || transactions.size() == 0 ? null : (MGOTransaction)(transactions.get(0));
    }

    /**
     * Saves transaction.
     * @param consumerId
     * @param agents
     */
    public Number saveTransaction(InitiatedTransaction transaction) throws DAOException {
        SimpleJdbcCall saveTransaction =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(INSERT_TRANSACTION_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                            new SqlParameter("iv_user_id", Types.VARCHAR),
                            new SqlParameter("iv_call_type_code", Types.VARCHAR),
                            new SqlParameter("iv_emg_tran_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_acct_id", Types.INTEGER),
                            new SqlParameter("iv_snd_cust_bkup_acct_id", Types.INTEGER),
                            new SqlParameter("iv_tran_stat_code", Types.VARCHAR),
                            new SqlParameter("iv_tran_sub_stat_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_agent_id", Types.INTEGER),
                            new SqlParameter("iv_dlvr_optn_id", Types.INTEGER),
                            new SqlParameter("iv_snd_iso_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_iso_crncy_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_face_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_fee_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_tot_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_fx_cnsmr_rate", Types.DECIMAL),
                            new SqlParameter("iv_form_free_conf_nbr", Types.INTEGER),
                            new SqlParameter("iv_intnd_rcv_agent_id", Types.INTEGER),
                            new SqlParameter("iv_rcv_agent_id", Types.INTEGER),
                            new SqlParameter("iv_rcv_agent_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_iso_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_iso_crncy_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_addr_id", Types.INTEGER),
                            new SqlParameter("iv_snd_cust_ip_addr_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_msg_1_text", Types.VARCHAR),
                            new SqlParameter("iv_snd_msg_2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_frst_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_last_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_mid_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_acct_encryp_nbr", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_acct_mask_nbr", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr1_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr3_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_matrnl_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_state_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line1_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line3_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_city_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_postal_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_ph_nbr", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_id", Types.INTEGER),
                            new SqlParameter("iv_inet_purch_flag", Types.VARCHAR),
                            new SqlParameter("iv_rcvr_rgstn_id", Types.VARCHAR),
                            new SqlParameter("iv_lylty_pgm_mbshp_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_photo_id_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_pho_id_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_pho_id_state_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_photo_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_legl_id_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_legl_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_ocupn_text", Types.VARCHAR),
                            new SqlParameter("iv_test_quest_text", Types.VARCHAR),
                            new SqlParameter("iv_test_ans_text", Types.VARCHAR),
                            new SqlParameter("iv_snd_fee_no_dcnt_aply_amt", Types.DECIMAL),
                            new SqlParameter("iv_rcv_cust_addr_iso_cntry_cd", Types.VARCHAR),
                            new SqlParameter("iv_intnd_dest_stateprovince_cd", Types.VARCHAR),
                            new SqlParameter("iv_tran_risk_score_nbr", Types.INTEGER),
                            new SqlParameter("iv_tran_cmnt_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_info_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_post_time_frame_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_eod_time_text", Types.VARCHAR),
                            new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
                            // Telecheck paramenters
                            new SqlParameter("iv_pymt_svc_prvdr_code", Types.INTEGER),
                            new SqlParameter("iv_pymt_svc_prvdr_trc_nbr", Types.VARCHAR),
                            new SqlParameter("iv_intrnl_trc_nbr", Types.VARCHAR),
                            // End Telecheck parameters
                            //Begin, Dodd Frank parameters
                            new SqlParameter("iv_mg_tran_sess_id", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new mgiTransactionSessionId field
                            new SqlParameter("iv_inrcv_face_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new inrcv_face_amt field
                            new SqlParameter("iv_inrcv_non_mgi_fee_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new INRCV_NON_MGI_FEE_AMT field
                            new SqlParameter("iv_inrcv_non_mgi_tax_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new inrcv_non_mgi_tax_amt field
                            new SqlParameter("iv_inrcv_payout_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new iv_inrcv_payout_amt field
                            new SqlParameter("iv_inrcv_avl_to_rcv_lcl_date", Types.TIMESTAMP), //vl58 - S17 (Dodd Frank), new inrcv_avl_to_rcv_lcl_date field
                            new SqlParameter("iv_subdiv_reg_cntnt_ver_id", Types.VARCHAR), //vl58 - S17 (Dodd Frank), The content version identifier of the regulatory document stored in Oracle Content Management.
                            new SqlParameter("iv_tran_dsclsr_text_eng", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new tran_dsclsr_text field for English
                            new SqlParameter("iv_tran_dsclsr_text_spa", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new tran_dsclsr_text field for Spanish
                            //End, Dodd Frank parameters
                            //Begin, vl58 - new fields for Release 36
                            new SqlParameter("iv_inrcv_non_mgi_fee_estm_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_non_mgi_fee_estm_flag field
                            new SqlParameter("iv_inrcv_non_mgi_tax_estm_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_non_mgi_tax_estm_flag field
                            new SqlParameter("iv_inrcv_payout_crncy_code", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_payout_crncy_code field
                            new SqlParameter("iv_inrcv_ind_cntry_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_ind_cntry_flag field
                            //End, vl58 - new fields for Release 36
                            new SqlParameter("iv_snd_src_web_lang_tag_text", Types.VARCHAR), //vx15 - MGO-5244 (r36)
                            new SqlParameter("iv_snd_cust_photo_id_exp_date", Types.TIMESTAMP), //vx15 - MGO-5199 (r36)
                            new SqlParameter("iv_inrcv_cnsmr_acct_mask_nbr", Types.VARCHAR),//wr37 MGO-11639 ToAccount related changes                          

                            new SqlOutParameter("ov_emg_tran_id", Types.NUMERIC),
                            new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        String account = null;
        String accountMask = null;
        if (transaction.getReceiver() != null) {
            account = transaction.getReceiver().getReceiverConsumerAccountNbr();
            accountMask = account;
            if (account != null) {
                if (account.length() > 4) {
                    accountMask = account.substring(account.length() - 4);
                }
                //encrypt account (only non PAN accounts are encrypted)
                account = encryptAccount(account);
            }
        }

        String legalId = null;
        if (transaction.getSender().getSenderLegalId() != null) {
        	legalId = transaction.getSender().getSenderLegalId().getNumber();
            if (legalId != null) {
                //encrypt legalId
                legalId = encryptAccount(legalId);
            }
        }
        
        String photoIDNumber = null;
        if (transaction.getSender().getSenderPhotoId()!= null) {
        	photoIDNumber = transaction.getSender().getSenderPhotoId().getNumber();
            if (photoIDNumber != null) {
                //encrypt photoIDNumber
            	photoIDNumber = encryptAccount(photoIDNumber);
            }
        }

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", null); //TODO: should it be consumer_id: Unique, system assigned identifier of a comment or narrative text entry pertaining to a Customer Account.
        addCallTypeParameter(parameters);

        parameters.addValue("iv_emg_tran_type_code", transaction.getProductType());
        parameters.addValue("iv_snd_cust_acct_id", transaction.getSender().getFiAccountId());
        parameters.addValue("iv_snd_cust_bkup_acct_id", transaction.getSender().getBackupFIAccountId());
        parameters.addValue("iv_tran_stat_code", transaction.getStatus());
        parameters.addValue("iv_tran_sub_stat_code", transaction.getSubStatus());
        parameters.addValue("iv_snd_agent_id", transaction.getSendAgentID());
        parameters.addValue("iv_dlvr_optn_id", transaction.getDeliveryOption());
        parameters.addValue("iv_snd_iso_cntry_code", transaction.getSendCountry());
        parameters.addValue("iv_snd_iso_crncy_id", transaction.getSendCurrency());
        parameters.addValue("iv_snd_face_amt", transaction.getTransactionAmount().getFaceAmount());
        parameters.addValue("iv_snd_fee_amt", transaction.getTransactionAmount().getSendFee());
        parameters.addValue("iv_snd_tot_amt", transaction.getTransactionAmount().getTotalAmount());
        parameters.addValue("iv_snd_fx_cnsmr_rate", transaction.getTransactionAmount().getExchangeRateApplied());
        parameters.addValue("iv_form_free_conf_nbr", transaction.getConfirmationNumber());
        parameters.addValue("iv_intnd_rcv_agent_id", transaction.getIntendedReceiveAgent() == null ? 0 : transaction.getIntendedReceiveAgent());
        parameters.addValue("iv_rcv_agent_id", transaction.getReceiveAgent());
        parameters.addValue("iv_rcv_agent_name", transaction.getReceiveAgentName());
        parameters.addValue("iv_rcv_agcy_code", transaction.getReceiveAgentCode());
        parameters.addValue("iv_rcv_iso_cntry_code", transaction.getReceiveCountry());
        parameters.addValue("iv_rcv_iso_crncy_id", transaction.getReceiveCurrency());
        parameters.addValue("iv_snd_cust_addr_id", transaction.getAddressId());
        parameters.addValue("iv_snd_cust_ip_addr_id", transaction.getSender().getSenderIPAddress());
        parameters.addValue("iv_snd_msg_1_text", transaction.getMessageField1());
        parameters.addValue("iv_snd_msg_2_text", transaction.getMessageField2());
        parameters.addValue("iv_rcv_cust_frst_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getFirstName() : null));
        parameters.addValue("iv_rcv_cust_last_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getLastName() : null));
        parameters.addValue("iv_rcv_cust_mid_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getMiddleName() : null));
        parameters.addValue("iv_rcv_agcy_acct_encryp_nbr", account);
        parameters.addValue("iv_rcv_agcy_acct_mask_nbr", accountMask);
        parameters.addValue("iv_rcv_cust_dlvr_instr1_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection1() : null));
        parameters.addValue("iv_rcv_cust_dlvr_instr2_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection2() : null));
        parameters.addValue("iv_rcv_cust_dlvr_instr3_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection3() : null));
        parameters.addValue("iv_rcv_cust_matrnl_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getMaternalName() : null));
        parameters.addValue("iv_rcv_cust_addr_state_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getState() : null));
        parameters.addValue("iv_rcv_cust_addr_line1_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine1() : null));
        parameters.addValue("iv_rcv_cust_addr_line2_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine2() : null));
        parameters.addValue("iv_rcv_cust_addr_line3_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine3() : null));
        parameters.addValue("iv_rcv_cust_addr_city_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getCity() : null));
        parameters.addValue("iv_rcv_cust_addr_postal_code", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getZipCode() : null));
        parameters.addValue("iv_rcv_cust_ph_nbr", (transaction.getReceiver() != null ? transaction.getReceiver().getReceiverPhone() : null));
        parameters.addValue("iv_snd_cust_id", transaction.getSender().getSenderConsumerId());
        parameters.addValue("iv_inet_purch_flag", DAOUtils.toString(transaction.getInternetPurchaseFlag()));
        parameters.addValue("iv_rcvr_rgstn_id", transaction.getReceiverRegistrationNumber());
        parameters.addValue("iv_lylty_pgm_mbshp_id", transaction.getSender().getSenderLoyaltyMemberId());
        parameters.addValue("iv_snd_cust_photo_id_type_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getType());
        parameters.addValue("iv_snd_cust_pho_id_cntry_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getIssueCountry());
        parameters.addValue("iv_snd_cust_pho_id_state_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getIssueState());
        parameters.addValue("iv_snd_cust_photo_id", photoIDNumber);
        parameters.addValue("iv_snd_cust_legl_id_type_code", transaction.getSender().getSenderLegalId() == null ? null : transaction.getSender().getSenderLegalId().getType());
        parameters.addValue("iv_snd_cust_legl_id", legalId);
        parameters.addValue("iv_snd_cust_ocupn_text", transaction.getSender().getSenderOccupation());
        parameters.addValue("iv_test_quest_text", transaction.getTestQuestion());
        parameters.addValue("iv_test_ans_text", transaction.getTestAnswer());
        parameters.addValue("iv_snd_fee_no_dcnt_aply_amt", transaction.getTransactionAmount().getNonDiscountedFee());
        parameters.addValue("iv_rcv_cust_addr_iso_cntry_cd", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getCountry() : null));
        parameters.addValue("iv_intnd_dest_stateprovince_cd", transaction.getReceiveState());
        parameters.addValue("iv_tran_risk_score_nbr", transaction.getAuthenticationScore());
        parameters.addValue("iv_tran_cmnt_text", transaction.getComment());
        parameters.addValue("iv_biller_info_text", transaction.getBillerInfoText());
        parameters.addValue("iv_biller_post_time_frame_text", transaction.getBillerPostTimeframeText());
        parameters.addValue("iv_biller_eod_time_text", transaction.getBillerEndOfDayTimeText());
        parameters.addValue("iv_src_web_site_bsns_cd", transaction.getSourceSite());
        // Telecheck parameters
        parameters.addValue("iv_pymt_svc_prvdr_code", transaction.getProviderCode());
        parameters.addValue("iv_pymt_svc_prvdr_trc_nbr", transaction.getProviderTransactionNumber());
        parameters.addValue("iv_intrnl_trc_nbr", transaction.getInternalTransactionNumber());
        // End Telecheck parameters
        //Start: S17 -- Dodd Frank
        parameters.addValue("iv_mg_tran_sess_id", transaction.getMgTransactionSessionId());
        parameters.addValue("iv_inrcv_face_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvFaceAmt() : null);
        parameters.addValue("iv_inrcv_non_mgi_fee_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvNonMgiFeeAmt() : null);
        parameters.addValue("iv_inrcv_non_mgi_tax_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvNonMgiTaxAmt() : null);
        parameters.addValue("iv_inrcv_payout_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvPayoutAmt() : null);
        parameters.addValue("iv_inrcv_avl_to_rcv_lcl_date", transaction.getInrcvAvlToRcvLclDate());
        parameters.addValue("iv_subdiv_reg_cntnt_ver_id", transaction.getSubdivRegCntntVerId());
        parameters.addValue("iv_tran_dsclsr_text_eng", transaction.getTranDsclsrTextEng());
        parameters.addValue("iv_tran_dsclsr_text_spa", transaction.getTranDsclsrTextSpa());
        //End: S17 -- Dodd Frank

        //Begin, vl58 - new fields for Release 36
        parameters.addValue("iv_inrcv_non_mgi_fee_estm_flag", transaction.getInrcvNonMgiFeeEstmFlag());
        parameters.addValue("iv_inrcv_non_mgi_tax_estm_flag", transaction.getInrcvNonMgiTaxEstmFlag());
        parameters.addValue("iv_inrcv_payout_crncy_code", transaction.getInrcvPayoutCrncyCode());
        parameters.addValue("iv_inrcv_ind_cntry_flag", transaction.getInrcvIndCntryFlag());
        //End, vl58 - new fields for Release 36
        //vx15 MGO-5244 (r36)
        parameters.addValue("iv_snd_src_web_lang_tag_text", transaction.getPreferredLanguage() == null ? null : transaction.getPreferredLanguage());
        //vx15 MGO-5199 (r36)
        parameters.addValue("iv_snd_cust_photo_id_exp_date", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getExpiryDate());
        //wr37 MGO-11639 ToAccount related changes
        parameters.addValue("iv_inrcv_cnsmr_acct_mask_nbr",transaction.getReceiver().getReceiverMTAccountNbrMask());

        if (logger.isDebugEnabled()) {
            logger.debug("saveTransaction: calling procedure="+ INSERT_TRANSACTION_PROC +" with "+ parameters.getValues().size() +" input parameters");
        }

        Map result = null;
        try {
            result = saveTransaction.execute(parameters);
            
            
        } catch (Exception e) {
            throw new DAOException("Failed to save transaction for consumer="+ transaction.getSender(), e);
        }

        Number transactionId = (Number)result.get("ov_emg_tran_id");

        if (logger.isDebugEnabled()) {
            logger.debug("saveTransaction: procedure log id="+ getProcedureLogId(result) +" transactionId="+ transactionId);
        }

        return transactionId;

    }

    /**
     * Updates transaction. Null values will be interpreted as "update with null" and not as "do not update"
     * @param transaction
     */

    public void updateTransaction(InitiatedTransaction transaction, Long transactionId) throws DAOException {
        SimpleJdbcCall updateTransaction =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_TRANSACTION_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                            new SqlParameter("iv_user_id", Types.VARCHAR),
                            new SqlParameter("iv_call_type_code", Types.VARCHAR),
                            new SqlParameter("iv_emg_tran_id", Types.VARCHAR),	
                            new SqlParameter("iv_emg_tran_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_acct_id", Types.INTEGER),
                            new SqlParameter("iv_snd_cust_bkup_acct_id", Types.INTEGER),
                            new SqlParameter("iv_tran_stat_code", Types.VARCHAR),
                            new SqlParameter("iv_tran_sub_stat_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_agent_id", Types.INTEGER),
                            new SqlParameter("iv_dlvr_optn_id", Types.INTEGER),
                            new SqlParameter("iv_snd_iso_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_iso_crncy_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_face_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_fee_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_tot_amt", Types.DECIMAL),
                            new SqlParameter("iv_snd_fx_cnsmr_rate", Types.DECIMAL),
                            new SqlParameter("iv_form_free_conf_nbr", Types.INTEGER),
                            new SqlParameter("iv_intnd_rcv_agent_id", Types.INTEGER),
                            new SqlParameter("iv_rcv_agent_id", Types.INTEGER),
                            new SqlParameter("iv_rcv_agent_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_iso_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_iso_crncy_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_addr_id", Types.INTEGER),
                            new SqlParameter("iv_snd_cust_ip_addr_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_msg_1_text", Types.VARCHAR),
                            new SqlParameter("iv_snd_msg_2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_frst_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_last_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_mid_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_acct_encryp_nbr", Types.VARCHAR),
                            new SqlParameter("iv_rcv_agcy_acct_mask_nbr", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr1_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_dlvr_instr3_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_matrnl_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_state_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line1_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line2_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_line3_text", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_city_name", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_addr_postal_code", Types.VARCHAR),
                            new SqlParameter("iv_rcv_cust_ph_nbr", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_id", Types.INTEGER),
                            new SqlParameter("iv_inet_purch_flag", Types.VARCHAR),
                            new SqlParameter("iv_rcvr_rgstn_id", Types.VARCHAR),
                            new SqlParameter("iv_lylty_pgm_mbshp_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_photo_id_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_pho_id_cntry_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_pho_id_state_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_photo_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_legl_id_type_code", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_legl_id", Types.VARCHAR),
                            new SqlParameter("iv_snd_cust_ocupn_text", Types.VARCHAR),
                            new SqlParameter("iv_test_quest_text", Types.VARCHAR),
                            new SqlParameter("iv_test_ans_text", Types.VARCHAR),
                            new SqlParameter("iv_snd_fee_no_dcnt_aply_amt", Types.DECIMAL),
                            new SqlParameter("iv_rcv_cust_addr_iso_cntry_cd", Types.VARCHAR),
                            new SqlParameter("iv_intnd_dest_stateprovince_cd", Types.VARCHAR),
                            new SqlParameter("iv_tran_risk_score_nbr", Types.INTEGER),
                            new SqlParameter("iv_tran_cmnt_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_info_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_post_time_frame_text", Types.VARCHAR),
                            new SqlParameter("iv_biller_eod_time_text", Types.VARCHAR),
                            new SqlParameter("iv_src_web_site_bsns_cd", Types.VARCHAR),
                            // Telecheck paramenters
                            new SqlParameter("iv_pymt_svc_prvdr_code", Types.INTEGER),
                            new SqlParameter("iv_pymt_svc_prvdr_trc_nbr", Types.VARCHAR),
                            new SqlParameter("iv_intrnl_trc_nbr", Types.VARCHAR),
                            // End Telecheck parameters
                            //Begin, Dodd Frank parameters
                            new SqlParameter("iv_mg_tran_sess_id", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new mgiTransactionSessionId field
                            new SqlParameter("iv_inrcv_face_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new inrcv_face_amt field
                            new SqlParameter("iv_inrcv_non_mgi_fee_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new INRCV_NON_MGI_FEE_AMT field
                            new SqlParameter("iv_inrcv_non_mgi_tax_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new inrcv_non_mgi_tax_amt field
                            new SqlParameter("iv_inrcv_payout_amt", Types.DECIMAL), //vl58 - S17 (Dodd Frank), new iv_inrcv_payout_amt field
                            new SqlParameter("iv_inrcv_avl_to_rcv_lcl_date", Types.TIMESTAMP), //vl58 - S17 (Dodd Frank), new inrcv_avl_to_rcv_lcl_date field
                            new SqlParameter("iv_subdiv_reg_cntnt_ver_id", Types.VARCHAR), //vl58 - S17 (Dodd Frank), The content version identifier of the regulatory document stored in Oracle Content Management.
                            new SqlParameter("iv_tran_dsclsr_text_eng", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new tran_dsclsr_text field for English
                            new SqlParameter("iv_tran_dsclsr_text_spa", Types.VARCHAR), //vl58 - S17 (Dodd Frank), new tran_dsclsr_text field for Spanish
                            //End, Dodd Frank parameters
                            //Begin, vl58 - new fields for Release 36
                            new SqlParameter("iv_inrcv_non_mgi_fee_estm_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_non_mgi_fee_estm_flag field
                            new SqlParameter("iv_inrcv_non_mgi_tax_estm_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_non_mgi_tax_estm_flag field
                            new SqlParameter("iv_inrcv_payout_crncy_code", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_payout_crncy_code field
                            new SqlParameter("iv_inrcv_ind_cntry_flag", Types.VARCHAR), //vl58 - S27 (Release 36), new inrcv_ind_cntry_flag field
                            //End, vl58 - new fields for Release 36
                            new SqlParameter("iv_snd_src_web_lang_tag_text", Types.VARCHAR), //vx15 - MGO-5244 (r36)

                            //added by Ankit Bhatt for 8873
                            new SqlParameter("iv_snd_cust_photo_id_exp_date", Types.TIMESTAMP), 
                            new SqlParameter("iv_inrcv_cnsmr_acct_mask_nbr", Types.VARCHAR),
                            //ended
                            new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        String account = null;
        String accountMask = null;
        if (transaction.getReceiver() != null) {
            account = transaction.getReceiver().getReceiverConsumerAccountNbr();
            accountMask = account;
            if (account != null) {
                if (account.length() > 4) {
                    accountMask = account.substring(account.length() - 4);
                }
                //encrypt account (only non PAN accounts are encrypted)
                account = encryptAccount(account);
            }
        }

        String legalId = null;
        if (transaction.getSender().getSenderLegalId() != null) {
        	legalId = transaction.getSender().getSenderLegalId().getNumber();
            if (legalId != null) {
                //encrypt legalId
                legalId = encryptAccount(legalId);
            }
        }
        
        String photoIDNumber = null;
        if (transaction.getSender().getSenderPhotoId()!= null) {
        	photoIDNumber = transaction.getSender().getSenderPhotoId().getNumber();
            if (photoIDNumber != null) {
                //encrypt photoIDNumber
            	photoIDNumber = encryptAccount(photoIDNumber);
            }
        }
        
        MapSqlParameterSource parameters = new MapSqlParameterSource();


        parameters.addValue("iv_emg_tran_id", transactionId);
        parameters.addValue("iv_user_id", null); //TODO: should it be consumer_id: Unique, system assigned identifier of a comment or narrative text entry pertaining to a Customer Account.
        addCallTypeParameter(parameters);
        parameters.addValue("iv_emg_tran_type_code", transaction.getProductType());
        parameters.addValue("iv_snd_cust_acct_id", transaction.getSender().getFiAccountId());
        parameters.addValue("iv_snd_cust_bkup_acct_id", transaction.getSender().getBackupFIAccountId());
        parameters.addValue("iv_tran_stat_code", transaction.getStatus());
        parameters.addValue("iv_tran_sub_stat_code", transaction.getSubStatus());
        parameters.addValue("iv_snd_agent_id", transaction.getSendAgentID());
        parameters.addValue("iv_dlvr_optn_id", transaction.getDeliveryOption());
        parameters.addValue("iv_snd_iso_cntry_code", transaction.getSendCountry());
        parameters.addValue("iv_snd_iso_crncy_id", transaction.getSendCurrency());
        parameters.addValue("iv_snd_face_amt", transaction.getTransactionAmount().getFaceAmount());
        parameters.addValue("iv_snd_fee_amt", transaction.getTransactionAmount().getSendFee());
        parameters.addValue("iv_snd_tot_amt", transaction.getTransactionAmount().getTotalAmount());
        parameters.addValue("iv_snd_fx_cnsmr_rate", transaction.getTransactionAmount().getExchangeRateApplied());
        parameters.addValue("iv_form_free_conf_nbr", transaction.getConfirmationNumber());
        parameters.addValue("iv_intnd_rcv_agent_id", transaction.getIntendedReceiveAgent() == null ? 0 : transaction.getIntendedReceiveAgent());
        parameters.addValue("iv_rcv_agent_id", transaction.getReceiveAgent());
        parameters.addValue("iv_rcv_agent_name", transaction.getReceiveAgentName());
        parameters.addValue("iv_rcv_agcy_code", transaction.getReceiveAgentCode());
        parameters.addValue("iv_rcv_iso_cntry_code", transaction.getReceiveCountry());
        parameters.addValue("iv_rcv_iso_crncy_id", transaction.getReceiveCurrency());
        parameters.addValue("iv_snd_cust_addr_id", transaction.getAddressId());
        parameters.addValue("iv_snd_cust_ip_addr_id", transaction.getSender().getSenderIPAddress());
        parameters.addValue("iv_snd_msg_1_text", transaction.getMessageField1());
        parameters.addValue("iv_snd_msg_2_text", transaction.getMessageField2());
        parameters.addValue("iv_rcv_cust_frst_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getFirstName() : null));
        parameters.addValue("iv_rcv_cust_last_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getLastName() : null));
        parameters.addValue("iv_rcv_cust_mid_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getMiddleName() : null));
        parameters.addValue("iv_rcv_agcy_acct_encryp_nbr", account);
        parameters.addValue("iv_rcv_agcy_acct_mask_nbr", accountMask);
        parameters.addValue("iv_rcv_cust_dlvr_instr1_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection1() : null));
        parameters.addValue("iv_rcv_cust_dlvr_instr2_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection2() : null));
        parameters.addValue("iv_rcv_cust_dlvr_instr3_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getDirection3() : null));
        parameters.addValue("iv_rcv_cust_matrnl_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverName() != null ? transaction.getReceiver().getReceiverName().getMaternalName() : null));
        parameters.addValue("iv_rcv_cust_addr_state_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getState() : null));
        parameters.addValue("iv_rcv_cust_addr_line1_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine1() : null));
        parameters.addValue("iv_rcv_cust_addr_line2_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine2() : null));
        parameters.addValue("iv_rcv_cust_addr_line3_text", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getLine3() : null));
        parameters.addValue("iv_rcv_cust_addr_city_name", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getCity() : null));
        parameters.addValue("iv_rcv_cust_addr_postal_code", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getZipCode() : null));
        parameters.addValue("iv_rcv_cust_ph_nbr", (transaction.getReceiver() != null ? transaction.getReceiver().getReceiverPhone() : null));
        parameters.addValue("iv_snd_cust_id", transaction.getSender().getSenderConsumerId());
        parameters.addValue("iv_inet_purch_flag", DAOUtils.toString(transaction.getInternetPurchaseFlag()));
        parameters.addValue("iv_rcvr_rgstn_id", transaction.getReceiverRegistrationNumber());
        parameters.addValue("iv_lylty_pgm_mbshp_id", transaction.getSender().getSenderLoyaltyMemberId());
        parameters.addValue("iv_snd_cust_photo_id_type_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getType());
        parameters.addValue("iv_snd_cust_pho_id_cntry_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getIssueCountry());
        parameters.addValue("iv_snd_cust_pho_id_state_code", transaction.getSender().getSenderPhotoId() == null ? null : transaction.getSender().getSenderPhotoId().getIssueState());
        parameters.addValue("iv_snd_cust_photo_id", photoIDNumber);
        parameters.addValue("iv_snd_cust_legl_id_type_code", transaction.getSender().getSenderLegalId() == null ? null : transaction.getSender().getSenderLegalId().getType());
        parameters.addValue("iv_snd_cust_legl_id", legalId);
        parameters.addValue("iv_snd_cust_ocupn_text", transaction.getSender().getSenderOccupation());
        parameters.addValue("iv_test_quest_text", transaction.getTestQuestion());
        parameters.addValue("iv_test_ans_text", transaction.getTestAnswer());
        parameters.addValue("iv_snd_fee_no_dcnt_aply_amt", transaction.getTransactionAmount().getNonDiscountedFee());
        parameters.addValue("iv_rcv_cust_addr_iso_cntry_cd", (transaction.getReceiver() != null && transaction.getReceiver().getReceiverAddress() != null ? transaction.getReceiver().getReceiverAddress().getCountry() : null));
        parameters.addValue("iv_intnd_dest_stateprovince_cd", transaction.getReceiveState());
        parameters.addValue("iv_tran_risk_score_nbr", transaction.getAuthenticationScore());
        parameters.addValue("iv_tran_cmnt_text", transaction.getComment());
        parameters.addValue("iv_biller_info_text", transaction.getBillerInfoText());
        parameters.addValue("iv_biller_post_time_frame_text", transaction.getBillerPostTimeframeText());
        parameters.addValue("iv_biller_eod_time_text", transaction.getBillerEndOfDayTimeText());
        parameters.addValue("iv_src_web_site_bsns_cd", transaction.getSourceSite());
        // Telecheck parameters
        parameters.addValue("iv_pymt_svc_prvdr_code", transaction.getProviderCode());
        parameters.addValue("iv_pymt_svc_prvdr_trc_nbr", transaction.getProviderTransactionNumber());
        parameters.addValue("iv_intrnl_trc_nbr", transaction.getInternalTransactionNumber());
        // End Telecheck parameters
        //Start: S17 -- Dodd Frank
        parameters.addValue("iv_mg_tran_sess_id", transaction.getMgTransactionSessionId());
        parameters.addValue("iv_inrcv_face_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvFaceAmt() : null);
        parameters.addValue("iv_inrcv_non_mgi_fee_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvNonMgiFeeAmt() : null);
        parameters.addValue("iv_inrcv_non_mgi_tax_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvNonMgiTaxAmt() : null);
        parameters.addValue("iv_inrcv_payout_amt", transaction.getTransactionAmount() != null ? transaction.getTransactionAmount().getInrcvPayoutAmt() : null);
        parameters.addValue("iv_inrcv_avl_to_rcv_lcl_date", transaction.getInrcvAvlToRcvLclDate());
        parameters.addValue("iv_subdiv_reg_cntnt_ver_id", transaction.getSubdivRegCntntVerId());
        parameters.addValue("iv_tran_dsclsr_text_eng", transaction.getTranDsclsrTextEng());
        parameters.addValue("iv_tran_dsclsr_text_spa", transaction.getTranDsclsrTextSpa());
        //End: S17 -- Dodd Frank
        //Begin, vl58 - new fields for Release 36
        parameters.addValue("iv_inrcv_non_mgi_fee_estm_flag", transaction.getInrcvNonMgiFeeEstmFlag());
        parameters.addValue("iv_inrcv_non_mgi_tax_estm_flag", transaction.getInrcvNonMgiTaxEstmFlag());
        parameters.addValue("iv_inrcv_payout_crncy_code", transaction.getInrcvPayoutCrncyCode());
        parameters.addValue("iv_inrcv_ind_cntry_flag", transaction.getInrcvIndCntryFlag());
        //End, vl58 - new fields for Release 36
        //vx15 MGO-5244 (r36)
        
        //added by Ankit Bhatt for 8873
        if(transaction!=null && transaction.getSender()!=null && transaction.getSender().getSenderPhotoId()!=null
        		&& transaction.getSender().getSenderPhotoId().getExpiryDate()!=null)
           parameters.addValue("iv_snd_cust_photo_id_exp_date", transaction.getSender().getSenderPhotoId().getExpiryDate());
        else
        	parameters.addValue("iv_snd_cust_photo_id_exp_date", null);
        
        if(transaction!=null && transaction.getSender()!=null)
        	parameters.addValue("iv_inrcv_cnsmr_acct_mask_nbr",transaction.getReceiver().getReceiverMTAccountNbrMask());
        else
        	parameters.addValue("iv_inrcv_cnsmr_acct_mask_nbr",null);
        //ended
        parameters.addValue("iv_snd_src_web_lang_tag_text", transaction.getPreferredLanguage() == null ? null : transaction.getPreferredLanguage());
        
        
        
        if (logger.isDebugEnabled()) {
            logger.debug("updateTransaction: calling procedure="+ UPDATE_TRANSACTION_PROC +" with "+ parameters.getValues().size() + " input parameters");
        }

        Map result = null;
        try {
            result = updateTransaction.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update transaction for consumer=" + transaction.getSender(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("updateTransaction: procedure log id=" + getProcedureLogId(result) + " transactionId=" + transactionId);
        }

    }

    /**
     * Updates transaction status.
     * @param commandRequest
     * @throws DAOException
     */
    public void updateTransactionStatus(UpdateTransactionStatusRequest commandRequest) throws DAOException {
        SimpleJdbcCall updateTransactionStatus =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_TRANSACTION_STATUS_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_emg_tran_id", Types.INTEGER),
                        new SqlParameter("iv_tran_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_tran_sub_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_tran_sub_reas_code", Types.VARCHAR),
                        new SqlParameter("iv_tran_risk_score_nbr", Types.INTEGER),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", null); //TODO: iv_user_id = null?
        addCallTypeParameter(parameters);
        parameters.addValue("iv_emg_tran_id", commandRequest.getTransactionId());
        parameters.addValue("iv_tran_stat_code", commandRequest.getStatus());
        parameters.addValue("iv_tran_sub_stat_code", commandRequest.getSubStatus());
        parameters.addValue("iv_tran_sub_reas_code", null); //TODO: iv_tran_sub_reas_code = null?
        parameters.addValue("iv_tran_risk_score_nbr", commandRequest.getAuthenticationScore());

        if (logger.isDebugEnabled()) {
            logger.debug("updateTransactionStatus: calling procedure="+ UPDATE_TRANSACTION_ACCOUNT_PROC +" with "+ parameters.getValues().size() +" input parameters. TransactionId="+ commandRequest.getTransactionId() +" Status="+ commandRequest.getStatus() +" SubStatus="+ commandRequest.getSubStatus() +" AuthenticationScore="+ commandRequest.getAuthenticationScore());
        }

        Map result = null;
        try {
            result = updateTransactionStatus.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update transaction sttaus for TransactionId="+ commandRequest.getTransactionId(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateTransactionStatus: procedure log id="+ getProcedureLogId(result));
        }
    }

    /**
     * Updates account.
     * @param commandRequest
     * @throws DAOException
     */
    public void updateAccount(UpdateAccountRequest commandRequest) throws DAOException {
        SimpleJdbcCall updateAccount =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(UPDATE_TRANSACTION_ACCOUNT_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_emg_tran_id", Types.INTEGER),
                        new SqlParameter("iv_tran_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_tran_sub_stat_code", Types.VARCHAR),
                        new SqlParameter("iv_tran_sub_reas_code", Types.VARCHAR),
                        new SqlParameter("iv_snd_cust_acct_id", Types.INTEGER),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", null); //TODO: iv_user_id = null?
        addCallTypeParameter(parameters);
        parameters.addValue("iv_emg_tran_id", commandRequest.getTransactionId());
        parameters.addValue("iv_tran_stat_code", commandRequest.getStatus());
        parameters.addValue("iv_tran_sub_stat_code", commandRequest.getSubStatus());
        parameters.addValue("iv_tran_sub_reas_code", null); //TODO: iv_tran_sub_reas_code = null?
        parameters.addValue("iv_snd_cust_acct_id", commandRequest.getFiAccountId());

        if (logger.isDebugEnabled()) {
            logger.debug("updateAccount: calling procedure="+ UPDATE_TRANSACTION_ACCOUNT_PROC +" with "+ parameters.getValues().size() +" input parameters. TransactionId="+ commandRequest.getTransactionId() +" Status="+ commandRequest.getStatus() +" SubStatus="+ commandRequest.getSubStatus() +" FiAccountId="+ commandRequest.getFiAccountId());
        }

        Map result = null;
        try {
            result = updateAccount.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to update transaction account for TransactionId="+ commandRequest.getTransactionId(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("updateAccount: procedure log id="+ getProcedureLogId(result));
        }
    }

    /**
     * Returns transactions total by consumer.
     * @param consumerId
     * @return
     * @throws DAOException
     */
    public BigDecimal getTransactionsTotal(long consumerId) throws DAOException {
        SimpleJdbcCall getTransactionTotal =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_TRANSACTIONS_TOTAL_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cust_id", Types.INTEGER),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_snd_tot_amt", Types.DECIMAL)
                        );

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", null); //TODO: should it be a specific user id?
        parameters.addValue("iv_cust_id", consumerId);
        addCallTypeParameter(parameters);

        if (logger.isDebugEnabled()) {
            logger.debug("getTransactionsTotal: calling procedure="+ GET_TRANSACTIONS_TOTAL_PROC +" with "+ parameters.getValues().size() +" input parameters. ConsumerId="+ consumerId);
        }

        Map result = null;
        try {
            result = getTransactionTotal.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve transaction with consumerId="+ consumerId, e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getTransaction: procedure log id="+ getProcedureLogId(result));
        }

        return (BigDecimal)result.get("ov_snd_tot_amt");
    }



    /**
     * Returns encrypted account value. For PAN accounts returns null.
     * Note. PAN accounts are not sent to PANDA during the transaction save.
     * PAN accounts are encrypted by the AC call (during transaction validation/staging).
     * @param account
     * @return
     * @throws DAOException
     */
    private String encryptAccount(String account) throws DAOException {
        String encrypted = null;
        PanIdInfo panInfo = PanIdentification.isPanNumber(account);

        if (panInfo.isPan()) {
            //ConsumerUtil.encryptUsingPCI(account, systemId, cardType);
        } else {
            //use local encryption
            encrypted = ConsumerUtil.encrypt(account);
        }
        return encrypted;
    }

    /**
     * Save state regulatory info (Dodd Frank)
     * @param stateRegulatoryInfo
     * @throws DAOException
     */
    public void saveStateRegulatoryInfo(StateRegulatoryInfo stateRegulatoryInfo) throws DAOException {
        SimpleJdbcCall saveStateRegulatoryInfo =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(INSERT_STATEREGINFO_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                            new SqlParameter("iv_user_id", Types.VARCHAR),
                            new SqlParameter("iv_call_type_code", Types.VARCHAR),
                            new SqlParameter("iv_cntnt_ver_id", Types.VARCHAR),
                            new SqlParameter("iv_iso_subdiv_code", Types.VARCHAR),
                            new SqlParameter("iv_iso_lang_mnemonic_code", Types.VARCHAR),
                            new SqlParameter("iv_reg_agcy_name", Types.VARCHAR),
                            new SqlParameter("iv_reg_agcy_url_text", Types.VARCHAR),
                            new SqlParameter("iv_reg_agcy_phn_nbr", Types.VARCHAR),
                            new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
                        );


        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        addCallTypeParameter(parameters);

        parameters.addValue("iv_cntnt_ver_id", stateRegulatoryInfo.getContentVerId());
        parameters.addValue("iv_iso_subdiv_code", stateRegulatoryInfo.getIsoSubDivCode());
        parameters.addValue("iv_iso_lang_mnemonic_code", stateRegulatoryInfo.getIsoLangCode());
        parameters.addValue("iv_reg_agcy_name", stateRegulatoryInfo.getRegulatoryAgencyName());
        parameters.addValue("iv_reg_agcy_url_text", stateRegulatoryInfo.getRegulatoryAgencyUrl());
        parameters.addValue("iv_reg_agcy_phn_nbr", stateRegulatoryInfo.getRegulatoryAgencyPhone());

        if (logger.isDebugEnabled()) {
            logger.debug("saveStateRegulatoryInfo: calling procedure="+ INSERT_STATEREGINFO_PROC +" with "+ parameters.getValues().size() +" input parameters");
        }

        Map result = null;
        try {
            result = saveStateRegulatoryInfo.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to save state regulatory info: "+ stateRegulatoryInfo.toString(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("saveStateRegulatoryInfo: procedure log id="+ getProcedureLogId(result));
        }
    }


    /**
     * Retrieves State Regulatory Info. (Dodd Frank)
     *
     * @param contentVerId
     * @param isoSubDivCode
     * @param isoLangNemonicCode
     * @param addressId
     * @return
     * @throws DAOException
     */
    public List<StateRegulatoryInfo> getStateRegulatoryInfo(String contentVerId, String isoSubDivCode, String isoLangNemonicCode, Long addressId) throws DAOException {
        SimpleJdbcCall getStateRegulatoryInfo =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(GET_STATEREGINFO_PROC)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                        new SqlParameter("iv_user_id", Types.VARCHAR),
                        new SqlParameter("iv_call_type_code", Types.VARCHAR),
                        new SqlParameter("iv_cntnt_ver_id", Types.VARCHAR),
                        new SqlParameter("iv_iso_subdiv_code", Types.VARCHAR),
                        new SqlParameter("iv_addr_id", Types.INTEGER),
                        new SqlParameter("iv_iso_lang_mnemonic_code", Types.VARCHAR),
                        new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                        new SqlOutParameter("ov_state_regulatory_info_cv", OracleTypes.CURSOR, new StateRegulatoryInfoRowMapper())
                        );


        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("iv_user_id", DB_CALLER_ID);
        addCallTypeParameter(parameters);
        parameters.addValue("iv_cntnt_ver_id", contentVerId);
        parameters.addValue("iv_iso_subdiv_code", isoSubDivCode);
        parameters.addValue("iv_addr_id", addressId);
        parameters.addValue("iv_iso_lang_mnemonic_code", isoLangNemonicCode);

        if (logger.isDebugEnabled()) {
            logger.debug("getStateRegulatoryInfo: calling procedure="+ GET_STATEREGINFO_PROC +" with "+ parameters.getValues().size() +" input parameters including contentVerId="+ contentVerId +" isoSubDivCode="+ isoSubDivCode+" isoLangNemonicCode "+isoLangNemonicCode+" addressId "+addressId);
        }

        Map result = null;
        List<StateRegulatoryInfo> stateRegulatoryInfoList = null;
        try {
            result = getStateRegulatoryInfo.execute(parameters);
            stateRegulatoryInfoList = (List<StateRegulatoryInfo>)result.get("ov_state_regulatory_info_cv");
        } catch (Exception e) {
            throw new DAOException("Failed to retrieve state regulatory info", e);
        }


        if (logger.isDebugEnabled()) {
            logger.debug("getStateRegulatoryInfo: procedure log id="+ getProcedureLogId(result) +", stateRegulatoryInfoList="+ (stateRegulatoryInfoList == null ? null : stateRegulatoryInfoList.size()));
        }

        return stateRegulatoryInfoList;
    }
    /**
     * Returns max content version for state regulatory info
     *
     * @param isoSubDivCode
     * @return
     * @throws DAOException
     */
    public String getMaxStateRegulatoryVersion(String isoSubDivCode) throws DAOException {
	    SimpleJdbcCall getMaxStateRegulatoryVersion =
	        new SimpleJdbcCall(getJdbcTemplate())
	                .withProcedureName(GET_MAXSTATEREG_VERSION_PROC)
	                .withoutProcedureColumnMetaDataAccess()
	                .declareParameters(
	                	new SqlParameter("iv_user_id", Types.VARCHAR),
	                    new SqlParameter("iv_call_type_code", Types.VARCHAR),
	                    new SqlParameter("iv_iso_subdiv_code", Types.VARCHAR),
	                    new SqlOutParameter("ov_cntnt_ver_id", Types.VARCHAR),
	                	new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC)
	                    );



	    MapSqlParameterSource parameters = new MapSqlParameterSource();
	    parameters.addValue("iv_user_id", DB_CALLER_ID);
	    addCallTypeParameter(parameters);

	    parameters.addValue("iv_iso_subdiv_code", isoSubDivCode);

	    if (logger.isDebugEnabled()) {
	        logger.debug("getMaxStateRegulatoryVersion: calling procedure="+ GET_MAXSTATEREG_VERSION_PROC +" with "+ parameters.getValues().size() +" input parameters including isoSubDivCode="+ isoSubDivCode);
	    }

	    Map result = null;
	    String  maxStateRegVersion = null;
	    try {
	        result = getMaxStateRegulatoryVersion.execute(parameters);
	        maxStateRegVersion =   (String) result.get("ov_cntnt_ver_id");
	    } catch (Exception e) {
	        throw new DAOException("Failed to retrieve max state regulatory version for isoSubDivCode = "+ isoSubDivCode, e);
	    }

	    if (logger.isDebugEnabled()) {
	        logger.debug("getMaxStateRegulatoryVersion: procedure log id="+ getProcedureLogId(result) +", maxStateRegVersion ="+maxStateRegVersion);
	    }

	    return maxStateRegVersion;
	}
    
    /**
     * Insert transaction comment
     * @param transaction
     * @param commentReasonCode
     * @author vl58
     * 
     */
    public Number saveTransactionComment(Long transactionId, String commentReasonCode, String commentText) throws DAOException {
        SimpleJdbcCall saveTransactionComment =
            new SimpleJdbcCall(getJdbcTemplate())
                    .withProcedureName(INSERT_TRANSACTION_COMMENT)
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                            new SqlParameter("iv_user_id", Types.VARCHAR),
                            new SqlParameter("iv_call_type_code", Types.VARCHAR),
                            new SqlParameter("iv_emg_tran_id", Types.INTEGER),
                            new SqlParameter("iv_emg_tran_cmnt_reas_code", Types.VARCHAR),
                            new SqlParameter("iv_cmnt_text", Types.VARCHAR),
                            new SqlOutParameter("ov_prcs_log_id", Types.NUMERIC),
                    		new SqlOutParameter("ov_emg_tran_cmnt_id", Types.NUMERIC)
                        );

       
        MapSqlParameterSource parameters = new MapSqlParameterSource();

        parameters.addValue("iv_user_id", DB_CALLER_ID);
        addCallTypeParameter(parameters);
        parameters.addValue("iv_emg_tran_id", transactionId);
        parameters.addValue("iv_emg_tran_cmnt_reas_code", commentReasonCode);
        parameters.addValue("iv_cmnt_text", commentText);
        
        if (logger.isDebugEnabled()) {
            logger.debug("saveTransactionComment: calling procedure="+ INSERT_TRANSACTION_COMMENT +" with "+ parameters.getValues().size() +" input parameters");
        }

        Map result = null;
        try {
            result = saveTransactionComment.execute(parameters);
        } catch (Exception e) {
            throw new DAOException("Failed to save transaction comment for transaction id="+ transactionId, e);
        }

        Number transactionCommentId = (Number)result.get("ov_emg_tran_cmnt_id");

        if (logger.isDebugEnabled()) {
            logger.debug("saveTransactionComment: procedure log id="+ getProcedureLogId(result) +" transactionCommentId="+ transactionCommentId);
        }

        return transactionCommentId;

    }

}
