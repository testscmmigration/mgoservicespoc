/**
 * TransactionAmount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * Transaction Amount.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/08 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TransactionAmount  implements java.io.Serializable {
    private java.math.BigDecimal nonDiscountedFee;

    private java.math.BigDecimal sendFee;

    private java.math.BigDecimal returnFee;

    private java.math.BigDecimal faceAmount;

    private java.math.BigDecimal totalAmount;

    private java.math.BigDecimal exchangeRateApplied;
    
    //Start - S17 -- Dodd Frank new fields
    
    private java.math.BigDecimal inrcvFaceAmt;
    
    private java.math.BigDecimal inrcvNonMgiFeeAmt;
    
    private java.math.BigDecimal inrcvNonMgiTaxAmt;
    
    private java.math.BigDecimal inrcvPayoutAmt;
    
	//End - S17 -- Dodd Frank new fields
    public TransactionAmount() {
    }

    public TransactionAmount(
           java.math.BigDecimal nonDiscountedFee,
           java.math.BigDecimal sendFee,
           java.math.BigDecimal returnFee,
           java.math.BigDecimal faceAmount,
           java.math.BigDecimal totalAmount,
           java.math.BigDecimal exchangeRateApplied,
           java.math.BigDecimal inrcvFaceAmt,
           java.math.BigDecimal inrcvNonMgiFeeAmt,
           java.math.BigDecimal inrcvNonMgiTaxAmt,
           java.math.BigDecimal inrcvPayoutAmt) {
           this.nonDiscountedFee = nonDiscountedFee;
           this.sendFee = sendFee;
           this.returnFee = returnFee;
           this.faceAmount = faceAmount;
           this.totalAmount = totalAmount;
           this.exchangeRateApplied = exchangeRateApplied;
           this.inrcvFaceAmt = inrcvFaceAmt;
           this.inrcvNonMgiFeeAmt = inrcvNonMgiFeeAmt;
           this.inrcvNonMgiTaxAmt = inrcvNonMgiTaxAmt;
           this.inrcvPayoutAmt = inrcvPayoutAmt;
    }


    /**
     * Gets the nonDiscountedFee value for this TransactionAmount.
     * 
     * @return nonDiscountedFee
     */
    public java.math.BigDecimal getNonDiscountedFee() {
        return nonDiscountedFee;
    }


    /**
     * Sets the nonDiscountedFee value for this TransactionAmount.
     * 
     * @param nonDiscountedFee
     */
    public void setNonDiscountedFee(java.math.BigDecimal nonDiscountedFee) {
        this.nonDiscountedFee = nonDiscountedFee;
    }


    /**
     * Gets the sendFee value for this TransactionAmount.
     * 
     * @return sendFee
     */
    public java.math.BigDecimal getSendFee() {
        return sendFee;
    }


    /**
     * Sets the sendFee value for this TransactionAmount.
     * 
     * @param sendFee
     */
    public void setSendFee(java.math.BigDecimal sendFee) {
        this.sendFee = sendFee;
    }


    /**
     * Gets the returnFee value for this TransactionAmount.
     * 
     * @return returnFee
     */
    public java.math.BigDecimal getReturnFee() {
        return returnFee;
    }


    /**
     * Sets the returnFee value for this TransactionAmount.
     * 
     * @param returnFee
     */
    public void setReturnFee(java.math.BigDecimal returnFee) {
        this.returnFee = returnFee;
    }


    /**
     * Gets the faceAmount value for this TransactionAmount.
     * 
     * @return faceAmount
     */
    public java.math.BigDecimal getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this TransactionAmount.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(java.math.BigDecimal faceAmount) {
        this.faceAmount = faceAmount;
    }


    /**
     * Gets the totalAmount value for this TransactionAmount.
     * 
     * @return totalAmount
     */
    public java.math.BigDecimal getTotalAmount() {
        return totalAmount;
    }


    /**
     * Sets the totalAmount value for this TransactionAmount.
     * 
     * @param totalAmount
     */
    public void setTotalAmount(java.math.BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }


    /**
     * Gets the exchangeRateApplied value for this TransactionAmount.
     * 
     * @return exchangeRateApplied
     */
    public java.math.BigDecimal getExchangeRateApplied() {
        return exchangeRateApplied;
    }


    /**
     * Sets the exchangeRateApplied value for this TransactionAmount.
     * 
     * @param exchangeRateApplied
     */
    public void setExchangeRateApplied(java.math.BigDecimal exchangeRateApplied) {
        this.exchangeRateApplied = exchangeRateApplied;
    }

    /**
	 * @return the inrcvFaceAmt
	 */
	public java.math.BigDecimal getInrcvFaceAmt() {
		return inrcvFaceAmt;
	}

	/**
	 * @param inrcvFaceAmt the inrcvFaceAmt to set
	 */
	public void setInrcvFaceAmt(java.math.BigDecimal inrcvFaceAmt) {
		this.inrcvFaceAmt = inrcvFaceAmt;
	}

	/**
	 * @return the inrcvNonMgiFeeAmt
	 */
	public java.math.BigDecimal getInrcvNonMgiFeeAmt() {
		return inrcvNonMgiFeeAmt;
	}

	/**
	 * @param inrcvNonMgiFeeAmt the inrcvNonMgiFeeAmt to set
	 */
	public void setInrcvNonMgiFeeAmt(java.math.BigDecimal inrcvNonMgiFeeAmt) {
		this.inrcvNonMgiFeeAmt = inrcvNonMgiFeeAmt;
	}

	/**
	 * @return the inrcvNonMgiTaxAmt
	 */
	public java.math.BigDecimal getInrcvNonMgiTaxAmt() {
		return inrcvNonMgiTaxAmt;
	}

	/**
	 * @param inrcvNonMgiTaxAmt the inrcvNonMgiTaxAmt to set
	 */
	public void setInrcvNonMgiTaxAmt(java.math.BigDecimal inrcvNonMgiTaxAmt) {
		this.inrcvNonMgiTaxAmt = inrcvNonMgiTaxAmt;
	}

	/**
	 * @return the inrcvPayoutAmt
	 */
	public java.math.BigDecimal getInrcvPayoutAmt() {
		return inrcvPayoutAmt;
	}

	/**
	 * @param inrcvPayoutAmt the inrcvPayoutAmt to set
	 */
	public void setInrcvPayoutAmt(java.math.BigDecimal inrcvPayoutAmt) {
		this.inrcvPayoutAmt = inrcvPayoutAmt;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionAmount)) return false;
        TransactionAmount other = (TransactionAmount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nonDiscountedFee==null && other.getNonDiscountedFee()==null) || 
             (this.nonDiscountedFee!=null &&
              this.nonDiscountedFee.equals(other.getNonDiscountedFee()))) &&
            ((this.sendFee==null && other.getSendFee()==null) || 
             (this.sendFee!=null &&
              this.sendFee.equals(other.getSendFee()))) &&
            ((this.returnFee==null && other.getReturnFee()==null) || 
             (this.returnFee!=null &&
              this.returnFee.equals(other.getReturnFee()))) &&
            ((this.faceAmount==null && other.getFaceAmount()==null) || 
             (this.faceAmount!=null &&
              this.faceAmount.equals(other.getFaceAmount()))) &&
            ((this.totalAmount==null && other.getTotalAmount()==null) || 
             (this.totalAmount!=null &&
              this.totalAmount.equals(other.getTotalAmount()))) &&
            ((this.exchangeRateApplied==null && other.getExchangeRateApplied()==null) || 
             (this.exchangeRateApplied!=null &&
              this.exchangeRateApplied.equals(other.getExchangeRateApplied()))) &&
            //S17 - Dodd Frank
            ((this.inrcvFaceAmt==null && other.getInrcvFaceAmt()==null) || 
             (this.inrcvFaceAmt!=null &&
              this.inrcvFaceAmt.equals(other.getInrcvFaceAmt()))) &&
            ((this.inrcvNonMgiFeeAmt==null && other.getInrcvNonMgiFeeAmt()==null) || 
             (this.inrcvNonMgiFeeAmt!=null &&
              this.inrcvNonMgiFeeAmt.equals(other.getInrcvNonMgiFeeAmt()))) &&
            ((this.inrcvNonMgiTaxAmt==null && other.getInrcvNonMgiTaxAmt()==null) || 
             (this.inrcvNonMgiTaxAmt!=null &&
              this.inrcvNonMgiTaxAmt.equals(other.getInrcvNonMgiTaxAmt()))) && 
            ((this.inrcvPayoutAmt==null && other.getInrcvPayoutAmt()==null) || 
             (this.inrcvPayoutAmt!=null &&
              this.inrcvPayoutAmt.equals(other.getInrcvPayoutAmt())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNonDiscountedFee() != null) {
            _hashCode += getNonDiscountedFee().hashCode();
        }
        if (getSendFee() != null) {
            _hashCode += getSendFee().hashCode();
        }
        if (getReturnFee() != null) {
            _hashCode += getReturnFee().hashCode();
        }
        if (getFaceAmount() != null) {
            _hashCode += getFaceAmount().hashCode();
        }
        if (getTotalAmount() != null) {
            _hashCode += getTotalAmount().hashCode();
        }
        if (getExchangeRateApplied() != null) {
            _hashCode += getExchangeRateApplied().hashCode();
        }
        //S17 -- dodd Frank
        if (getInrcvFaceAmt()!= null) {
            _hashCode += getInrcvFaceAmt().hashCode();
        }
        if (getInrcvNonMgiFeeAmt() != null) {
            _hashCode += getInrcvNonMgiFeeAmt().hashCode();
        }
        if (getInrcvNonMgiTaxAmt() != null) {
            _hashCode += getInrcvNonMgiTaxAmt().hashCode();
        }
        if (getInrcvPayoutAmt() != null) {
            _hashCode += getInrcvPayoutAmt().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" NonDiscountedFee=").append(getNonDiscountedFee());
        buffer.append(" SendFee=").append(getSendFee());
        buffer.append(" ReturnFee=").append(getReturnFee());
        buffer.append(" FaceAmount=").append(getFaceAmount());
        buffer.append(" TotalAmount=").append(getTotalAmount());
        buffer.append(" ExchangeRateApplied=").append(getExchangeRateApplied());
        //S17 -- dodd Frank
        buffer.append(" InrcvFaceAmt=").append(getInrcvFaceAmt());
        buffer.append(" InrcvNonMgiFeeAmt=").append(getInrcvNonMgiFeeAmt());
        buffer.append(" InrcvNonMgiTaxAmt=").append(getInrcvNonMgiTaxAmt());
        buffer.append(" InrcvPayoutAmt=").append(getInrcvPayoutAmt());
        return buffer.toString();
    }
}
