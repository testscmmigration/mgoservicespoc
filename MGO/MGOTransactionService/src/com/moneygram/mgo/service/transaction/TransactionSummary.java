/**
 * TransactionSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * Transaction Summary.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2012/01/30 17:38:45 $ </td><tr><td>
 * @author   </td><td>$Author: vf69 $ </td>
 *</table>
 *</div>
 */
public class TransactionSummary  implements java.io.Serializable {
    private static final long serialVersionUID = -352461225356672387L;

    private long transactionId;

    private java.lang.String referenceNumber;

    private java.util.Calendar sendDate;

    private java.lang.String status;

    private java.lang.String subStatus;

    private java.util.Calendar statusDate;

    private java.math.BigDecimal faceAmount;

    private ReceiverConsumerName receiverName;

    private MGOProductType productType;

    private java.lang.String sendCurrency;

    private java.lang.String receiveCountry;

    private java.lang.String receiveState;

    private java.lang.Long receiveAgent;

    private java.lang.String receiveAgentName;

    private java.lang.String receiveAgentCode;

    private int deliveryOption;

    private java.util.Calendar receivedDate;

    private java.lang.String sourceSite;
    
    // Start: S17 -- dodd Frank
    private java.lang.String mgTransactionSessionId;
    private java.lang.String subdivRegCntntVerId;
    // End: S17 -- dodd Frank
    private java.lang.String receiveAgentRefNbr;

    private java.lang.String receiverMTAccountNbrMask;

    private java.lang.String receiverRegistrationNumber;

    public TransactionSummary() {
    }

    public TransactionSummary(
           long transactionId,
           java.lang.String referenceNumber,
           java.util.Calendar sendDate,
           java.lang.String status,
           java.lang.String subStatus,
           java.util.Calendar statusDate,
           java.math.BigDecimal faceAmount,
           ReceiverConsumerName receiverName,
           MGOProductType productType,
           java.lang.String sendCurrency,
           java.lang.String receiveCountry,
           java.lang.String receiveState,
           java.lang.Long receiveAgent,
           java.lang.String receiveAgentName,
           java.lang.String receiveAgentCode,
           int deliveryOption,
           java.util.Calendar receivedDate,
           java.lang.String sourceSite,
           java.lang.String mgTransactionSessionId,
           java.lang.String subdivRegCntntVerId,
           java.lang.String receiveAgentRefNbr,
           java.lang.String receiverMTAccountNbrMask,
           java.lang.String receiverRegistrationNumber) {
           this.transactionId = transactionId;
           this.referenceNumber = referenceNumber;
           this.sendDate = sendDate;
           this.status = status;
           this.subStatus = subStatus;
           this.statusDate = statusDate;
           this.faceAmount = faceAmount;
           this.receiverName = receiverName;
           this.productType = productType;
           this.sendCurrency = sendCurrency;
           this.receiveCountry = receiveCountry;
           this.receiveState = receiveState;
           this.receiveAgent = receiveAgent;
           this.receiveAgentName = receiveAgentName;
           this.receiveAgentCode = receiveAgentCode;
           this.deliveryOption = deliveryOption;
           this.receivedDate = receivedDate;
           this.sourceSite = sourceSite;
           this.mgTransactionSessionId = mgTransactionSessionId;
           this.subdivRegCntntVerId = subdivRegCntntVerId;
           this.receiveAgentRefNbr = receiveAgentRefNbr;
           this.receiverMTAccountNbrMask = receiverMTAccountNbrMask;
           this.receiverRegistrationNumber = receiverRegistrationNumber;
    }


    /**
     * Gets the transactionId value for this TransactionSummary.
     * 
     * @return transactionId
     */
    public long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this TransactionSummary.
     * 
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the referenceNumber value for this TransactionSummary.
     * 
     * @return referenceNumber
     */
    public java.lang.String getReferenceNumber() {
        return referenceNumber;
    }


    /**
     * Sets the referenceNumber value for this TransactionSummary.
     * 
     * @param referenceNumber
     */
    public void setReferenceNumber(java.lang.String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }


    /**
     * Gets the sendDate value for this TransactionSummary.
     * 
     * @return sendDate
     */
    public java.util.Calendar getSendDate() {
        return sendDate;
    }


    /**
     * Sets the sendDate value for this TransactionSummary.
     * 
     * @param sendDate
     */
    public void setSendDate(java.util.Calendar sendDate) {
        this.sendDate = sendDate;
    }


    /**
     * Gets the status value for this TransactionSummary.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this TransactionSummary.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the subStatus value for this TransactionSummary.
     * 
     * @return subStatus
     */
    public java.lang.String getSubStatus() {
        return subStatus;
    }


    /**
     * Sets the subStatus value for this TransactionSummary.
     * 
     * @param subStatus
     */
    public void setSubStatus(java.lang.String subStatus) {
        this.subStatus = subStatus;
    }


    /**
     * Gets the statusDate value for this TransactionSummary.
     * 
     * @return statusDate
     */
    public java.util.Calendar getStatusDate() {
        return statusDate;
    }


    /**
     * Sets the statusDate value for this TransactionSummary.
     * 
     * @param statusDate
     */
    public void setStatusDate(java.util.Calendar statusDate) {
        this.statusDate = statusDate;
    }


    /**
     * Gets the faceAmount value for this TransactionSummary.
     * 
     * @return faceAmount
     */
    public java.math.BigDecimal getFaceAmount() {
        return faceAmount;
    }


    /**
     * Sets the faceAmount value for this TransactionSummary.
     * 
     * @param faceAmount
     */
    public void setFaceAmount(java.math.BigDecimal faceAmount) {
        this.faceAmount = faceAmount;
    }


    /**
     * Gets the receiverName value for this TransactionSummary.
     * 
     * @return receiverName
     */
    public ReceiverConsumerName getReceiverName() {
        return receiverName;
    }


    /**
     * Sets the receiverName value for this TransactionSummary.
     * 
     * @param receiverName
     */
    public void setReceiverName(ReceiverConsumerName receiverName) {
        this.receiverName = receiverName;
    }


    /**
     * Gets the productType value for this TransactionSummary.
     * 
     * @return productType
     */
    public MGOProductType getProductType() {
        return productType;
    }


    /**
     * Sets the productType value for this TransactionSummary.
     * 
     * @param productType
     */
    public void setProductType(MGOProductType productType) {
        this.productType = productType;
    }


    /**
     * Gets the sendCurrency value for this TransactionSummary.
     * 
     * @return sendCurrency
     */
    public java.lang.String getSendCurrency() {
        return sendCurrency;
    }


    /**
     * Sets the sendCurrency value for this TransactionSummary.
     * 
     * @param sendCurrency
     */
    public void setSendCurrency(java.lang.String sendCurrency) {
        this.sendCurrency = sendCurrency;
    }


    /**
     * Gets the receiveCountry value for this TransactionSummary.
     * 
     * @return receiveCountry
     */
    public java.lang.String getReceiveCountry() {
        return receiveCountry;
    }


    /**
     * Sets the receiveCountry value for this TransactionSummary.
     * 
     * @param receiveCountry
     */
    public void setReceiveCountry(java.lang.String receiveCountry) {
        this.receiveCountry = receiveCountry;
    }


    /**
     * Gets the receiveState value for this TransactionSummary.
     * 
     * @return receiveState
     */
    public java.lang.String getReceiveState() {
        return receiveState;
    }


    /**
     * Sets the receiveState value for this TransactionSummary.
     * 
     * @param receiveState
     */
    public void setReceiveState(java.lang.String receiveState) {
        this.receiveState = receiveState;
    }


    /**
     * Gets the receiveAgent value for this TransactionSummary.
     * 
     * @return receiveAgent
     */
    public java.lang.Long getReceiveAgent() {
        return receiveAgent;
    }


    /**
     * Sets the receiveAgent value for this TransactionSummary.
     * 
     * @param receiveAgent
     */
    public void setReceiveAgent(java.lang.Long receiveAgent) {
        this.receiveAgent = receiveAgent;
    }


    /**
     * Gets the receiveAgentName value for this TransactionSummary.
     * 
     * @return receiveAgentName
     */
    public java.lang.String getReceiveAgentName() {
        return receiveAgentName;
    }


    /**
     * Sets the receiveAgentName value for this TransactionSummary.
     * 
     * @param receiveAgentName
     */
    public void setReceiveAgentName(java.lang.String receiveAgentName) {
        this.receiveAgentName = receiveAgentName;
    }


    /**
     * Gets the receiveAgentCode value for this TransactionSummary.
     * 
     * @return receiveAgentCode
     */
    public java.lang.String getReceiveAgentCode() {
        return receiveAgentCode;
    }


    /**
     * Sets the receiveAgentCode value for this TransactionSummary.
     * 
     * @param receiveAgentCode
     */
    public void setReceiveAgentCode(java.lang.String receiveAgentCode) {
        this.receiveAgentCode = receiveAgentCode;
    }


    /**
     * Gets the deliveryOption value for this TransactionSummary.
     * 
     * @return deliveryOption
     */
    public int getDeliveryOption() {
        return deliveryOption;
    }


    /**
     * Sets the deliveryOption value for this TransactionSummary.
     * 
     * @param deliveryOption
     */
    public void setDeliveryOption(int deliveryOption) {
        this.deliveryOption = deliveryOption;
    }


    /**
     * Gets the receivedDate value for this TransactionSummary.
     * 
     * @return receivedDate
     */
    public java.util.Calendar getReceivedDate() {
        return receivedDate;
    }


    /**
     * Sets the receivedDate value for this TransactionSummary.
     * 
     * @param receivedDate
     */
    public void setReceivedDate(java.util.Calendar receivedDate) {
        this.receivedDate = receivedDate;
    }


    /**
     * Gets the sourceSite value for this TransactionSummary.
     * 
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this TransactionSummary.
     * 
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }


    /**
     * Gets the mgTransactionSessionId value for this TransactionSummary.
     * 
     * @return mgTransactionSessionId
     */
    public java.lang.String getMgTransactionSessionId() {
        return mgTransactionSessionId;
    }


    /**
     * Sets the mgTransactionSessionId value for this TransactionSummary.
     * 
     * @param mgTransactionSessionId
     */
    public void setMgTransactionSessionId(java.lang.String mgTransactionSessionId) {
        this.mgTransactionSessionId = mgTransactionSessionId;
    }


    /**
     * Gets the subdivRegCntntVerId value for this TransactionSummary.
     * 
     * @return subdivRegCntntVerId
     */
    public java.lang.String getSubdivRegCntntVerId() {
        return subdivRegCntntVerId;
    }


    /**
     * Sets the subdivRegCntntVerId value for this TransactionSummary.
     * 
     * @param subdivRegCntntVerId
     */
    public void setSubdivRegCntntVerId(java.lang.String subdivRegCntntVerId) {
        this.subdivRegCntntVerId = subdivRegCntntVerId;
    }


    /**
     * Gets the receiveAgentRefNbr value for this TransactionSummary.
     * 
     * @return receiveAgentRefNbr
     */
    public java.lang.String getReceiveAgentRefNbr() {
        return receiveAgentRefNbr;
    }


    /**
     * Sets the receiveAgentRefNbr value for this TransactionSummary.
     * 
     * @param receiveAgentRefNbr
     */
    public void setReceiveAgentRefNbr(java.lang.String receiveAgentRefNbr) {
        this.receiveAgentRefNbr = receiveAgentRefNbr;
    }


    /**
     * Gets the receiverMTAccountNbrMask value for this TransactionSummary.
     * 
     * @return receiverMTAccountNbrMask
     */
    public java.lang.String getReceiverMTAccountNbrMask() {
        return receiverMTAccountNbrMask;
    }


    /**
     * Sets the receiverMTAccountNbrMask value for this TransactionSummary.
     * 
     * @param receiverMTAccountNbrMask
     */
    public void setReceiverMTAccountNbrMask(java.lang.String receiverMTAccountNbrMask) {
        this.receiverMTAccountNbrMask = receiverMTAccountNbrMask;
    }


    /**
     * Gets the receiverRegistrationNumber value for this TransactionSummary.
     * 
     * @return receiverRegistrationNumber
     */
    public java.lang.String getReceiverRegistrationNumber() {
        return receiverRegistrationNumber;
    }


    /**
     * Sets the receiverRegistrationNumber value for this TransactionSummary.
     * 
     * @param receiverRegistrationNumber
     */
    public void setReceiverRegistrationNumber(java.lang.String receiverRegistrationNumber) {
        this.receiverRegistrationNumber = receiverRegistrationNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionSummary)) return false;
        TransactionSummary other = (TransactionSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.transactionId == other.getTransactionId() &&
            ((this.referenceNumber==null && other.getReferenceNumber()==null) || 
             (this.referenceNumber!=null &&
              this.referenceNumber.equals(other.getReferenceNumber()))) &&
            ((this.sendDate==null && other.getSendDate()==null) || 
             (this.sendDate!=null &&
              this.sendDate.equals(other.getSendDate()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.subStatus==null && other.getSubStatus()==null) || 
             (this.subStatus!=null &&
              this.subStatus.equals(other.getSubStatus()))) &&
            ((this.statusDate==null && other.getStatusDate()==null) || 
             (this.statusDate!=null &&
              this.statusDate.equals(other.getStatusDate()))) &&
            ((this.faceAmount==null && other.getFaceAmount()==null) || 
             (this.faceAmount!=null &&
              this.faceAmount.equals(other.getFaceAmount()))) &&
            ((this.receiverName==null && other.getReceiverName()==null) || 
             (this.receiverName!=null &&
              this.receiverName.equals(other.getReceiverName()))) &&
            ((this.productType==null && other.getProductType()==null) || 
             (this.productType!=null &&
              this.productType.equals(other.getProductType()))) &&
            ((this.sendCurrency==null && other.getSendCurrency()==null) || 
             (this.sendCurrency!=null &&
              this.sendCurrency.equals(other.getSendCurrency()))) &&
            ((this.receiveCountry==null && other.getReceiveCountry()==null) || 
             (this.receiveCountry!=null &&
              this.receiveCountry.equals(other.getReceiveCountry()))) &&
            ((this.receiveState==null && other.getReceiveState()==null) || 
             (this.receiveState!=null &&
              this.receiveState.equals(other.getReceiveState()))) &&
            ((this.receiveAgent==null && other.getReceiveAgent()==null) || 
             (this.receiveAgent!=null &&
              this.receiveAgent.equals(other.getReceiveAgent()))) &&
            ((this.receiveAgentName==null && other.getReceiveAgentName()==null) || 
             (this.receiveAgentName!=null &&
              this.receiveAgentName.equals(other.getReceiveAgentName()))) &&
            ((this.receiveAgentCode==null && other.getReceiveAgentCode()==null) || 
             (this.receiveAgentCode!=null &&
              this.receiveAgentCode.equals(other.getReceiveAgentCode()))) &&
            this.deliveryOption == other.getDeliveryOption() &&
            ((this.receivedDate==null && other.getReceivedDate()==null) || 
             (this.receivedDate!=null &&
              this.receivedDate.equals(other.getReceivedDate()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) || 
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite()))) &&
            ((this.mgTransactionSessionId==null && other.getMgTransactionSessionId()==null) || 
             (this.mgTransactionSessionId!=null &&
              this.mgTransactionSessionId.equals(other.getMgTransactionSessionId()))) &&
            ((this.subdivRegCntntVerId==null && other.getSubdivRegCntntVerId()==null) || 
             (this.subdivRegCntntVerId!=null &&
              this.subdivRegCntntVerId.equals(other.getSubdivRegCntntVerId()))) &&
            ((this.receiveAgentRefNbr==null && other.getReceiveAgentRefNbr()==null) || 
             (this.receiveAgentRefNbr!=null &&
              this.receiveAgentRefNbr.equals(other.getReceiveAgentRefNbr()))) &&
            ((this.receiverMTAccountNbrMask==null && other.getReceiverMTAccountNbrMask()==null) || 
             (this.receiverMTAccountNbrMask!=null &&
              this.receiverMTAccountNbrMask.equals(other.getReceiverMTAccountNbrMask()))) &&
            ((this.receiverRegistrationNumber==null && other.getReceiverRegistrationNumber()==null) || 
             (this.receiverRegistrationNumber!=null &&
              this.receiverRegistrationNumber.equals(other.getReceiverRegistrationNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getTransactionId()).hashCode();
        if (getReferenceNumber() != null) {
            _hashCode += getReferenceNumber().hashCode();
        }
        if (getSendDate() != null) {
            _hashCode += getSendDate().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getSubStatus() != null) {
            _hashCode += getSubStatus().hashCode();
        }
        if (getStatusDate() != null) {
            _hashCode += getStatusDate().hashCode();
        }
        if (getFaceAmount() != null) {
            _hashCode += getFaceAmount().hashCode();
        }
        if (getReceiverName() != null) {
            _hashCode += getReceiverName().hashCode();
        }
        if (getProductType() != null) {
            _hashCode += getProductType().hashCode();
        }
        if (getSendCurrency() != null) {
            _hashCode += getSendCurrency().hashCode();
        }
        if (getReceiveCountry() != null) {
            _hashCode += getReceiveCountry().hashCode();
        }
        if (getReceiveState() != null) {
            _hashCode += getReceiveState().hashCode();
        }
        if (getReceiveAgent() != null) {
            _hashCode += getReceiveAgent().hashCode();
        }
        if (getReceiveAgentName() != null) {
            _hashCode += getReceiveAgentName().hashCode();
        }
        if (getReceiveAgentCode() != null) {
            _hashCode += getReceiveAgentCode().hashCode();
        }
        _hashCode += getDeliveryOption();
        if (getReceivedDate() != null) {
            _hashCode += getReceivedDate().hashCode();
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        if (getMgTransactionSessionId() != null) {
            _hashCode += getMgTransactionSessionId().hashCode();
        }
        if (getSubdivRegCntntVerId() != null) {
            _hashCode += getSubdivRegCntntVerId().hashCode();
        }
        if (getReceiveAgentRefNbr() != null) {
            _hashCode += getReceiveAgentRefNbr().hashCode();
        }
        if (getReceiverMTAccountNbrMask() != null) {
            _hashCode += getReceiverMTAccountNbrMask().hashCode();
        }
        if (getReceiverRegistrationNumber() != null) {
            _hashCode += getReceiverRegistrationNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "TransactionSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "transactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "referenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "sendDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "subStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "statusDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faceAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "faceAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "productType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "MGOProductType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "sendCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryOption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "deliveryOption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receivedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receivedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "sourceSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mgTransactionSessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "mgTransactionSessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subdivRegCntntVerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "subdivRegCntntVerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgentRefNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgentRefNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverMTAccountNbrMask");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverMTAccountNbrMask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverRegistrationNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiverRegistrationNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
