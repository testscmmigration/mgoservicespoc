/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetTransactionRequest;
import com.moneygram.mgo.service.transaction.GetTransactionResponse;
import com.moneygram.mgo.service.transaction.MGOTransaction;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Get Transaction Details Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2009/08/19 13:55:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetTransactionCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetTransactionCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetTransactionRequest commandRequest = (GetTransactionRequest) request;

        MGOTransaction transaction = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            transaction = dao.getTransaction(commandRequest.getTransactionId());
            
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve transactions", e);
        }

        GetTransactionResponse response = new GetTransactionResponse();
        response.setTransaction(transaction);
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetTransactionRequest;
    }

}
