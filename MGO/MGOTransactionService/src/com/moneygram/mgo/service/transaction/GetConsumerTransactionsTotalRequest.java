/**
 * GetConsumerTransactionsTotalRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Get Consumer Transactions Total Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/08 19:37:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetConsumerTransactionsTotalRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 3583498113878302395L;

    private long consumerId;

    public GetConsumerTransactionsTotalRequest() {
    }

    public GetConsumerTransactionsTotalRequest(
           RequestHeader header,
           long consumerId) {
        super(
            header);
        this.consumerId = consumerId;
    }


    /**
     * Gets the consumerId value for this GetConsumerTransactionsTotalRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this GetConsumerTransactionsTotalRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsumerTransactionsTotalRequest)) return false;
        GetConsumerTransactionsTotalRequest other = (GetConsumerTransactionsTotalRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" ConsumerId="+ getConsumerId();
    }
}
