/**
 * MGOTransactionProcessing.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;




/**
 * 
 * MGOTransaction Processing.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/10/13 22:09:32 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MGOTransactionProcessing  implements java.io.Serializable {
    private static final long serialVersionUID = -32854277784982630L;

    private long transactionId;

    private java.lang.String referenceNumber;

    private java.util.Calendar sendDate;

    private java.util.Calendar submittedDate;

    private java.lang.String riskLevelCode;

    private java.lang.String csrProcessUserId;

    private java.util.Calendar csrProcessDate;

    private long achControlFileSeqNbr;

    private java.util.Calendar receivedDate;

    private java.util.Calendar statusUpdateDate;

    private java.lang.String receiveAgentConfirmationNumber;

    public MGOTransactionProcessing() {
    }

    public MGOTransactionProcessing(
           long transactionId,
           java.lang.String referenceNumber,
           java.util.Calendar sendDate,
           java.util.Calendar submittedDate,
           java.lang.String riskLevelCode,
           java.lang.String csrProcessUserId,
           java.util.Calendar csrProcessDate,
           long achControlFileSeqNbr,
           java.util.Calendar receivedDate,
           java.util.Calendar statusUpdateDate,
           java.lang.String receiveAgentConfirmationNumber) {
           this.transactionId = transactionId;
           this.referenceNumber = referenceNumber;
           this.sendDate = sendDate;
           this.submittedDate = submittedDate;
           this.riskLevelCode = riskLevelCode;
           this.csrProcessUserId = csrProcessUserId;
           this.csrProcessDate = csrProcessDate;
           this.achControlFileSeqNbr = achControlFileSeqNbr;
           this.receivedDate = receivedDate;
           this.statusUpdateDate = statusUpdateDate;
           this.receiveAgentConfirmationNumber = receiveAgentConfirmationNumber;
    }


    /**
     * Gets the transactionId value for this MGOTransactionProcessing.
     * 
     * @return transactionId
     */
    public long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this MGOTransactionProcessing.
     * 
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the referenceNumber value for this MGOTransactionProcessing.
     * 
     * @return referenceNumber
     */
    public java.lang.String getReferenceNumber() {
        return referenceNumber;
    }


    /**
     * Sets the referenceNumber value for this MGOTransactionProcessing.
     * 
     * @param referenceNumber
     */
    public void setReferenceNumber(java.lang.String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }


    /**
     * Gets the sendDate value for this MGOTransactionProcessing.
     * 
     * @return sendDate
     */
    public java.util.Calendar getSendDate() {
        return sendDate;
    }


    /**
     * Sets the sendDate value for this MGOTransactionProcessing.
     * 
     * @param sendDate
     */
    public void setSendDate(java.util.Calendar sendDate) {
        this.sendDate = sendDate;
    }


    /**
     * Gets the submittedDate value for this MGOTransactionProcessing.
     * 
     * @return submittedDate
     */
    public java.util.Calendar getSubmittedDate() {
        return submittedDate;
    }


    /**
     * Sets the submittedDate value for this MGOTransactionProcessing.
     * 
     * @param submittedDate
     */
    public void setSubmittedDate(java.util.Calendar submittedDate) {
        this.submittedDate = submittedDate;
    }


    /**
     * Gets the riskLevelCode value for this MGOTransactionProcessing.
     * 
     * @return riskLevelCode
     */
    public java.lang.String getRiskLevelCode() {
        return riskLevelCode;
    }


    /**
     * Sets the riskLevelCode value for this MGOTransactionProcessing.
     * 
     * @param riskLevelCode
     */
    public void setRiskLevelCode(java.lang.String riskLevelCode) {
        this.riskLevelCode = riskLevelCode;
    }


    /**
     * Gets the csrProcessUserId value for this MGOTransactionProcessing.
     * 
     * @return csrProcessUserId
     */
    public java.lang.String getCsrProcessUserId() {
        return csrProcessUserId;
    }


    /**
     * Sets the csrProcessUserId value for this MGOTransactionProcessing.
     * 
     * @param csrProcessUserId
     */
    public void setCsrProcessUserId(java.lang.String csrProcessUserId) {
        this.csrProcessUserId = csrProcessUserId;
    }


    /**
     * Gets the csrProcessDate value for this MGOTransactionProcessing.
     * 
     * @return csrProcessDate
     */
    public java.util.Calendar getCsrProcessDate() {
        return csrProcessDate;
    }


    /**
     * Sets the csrProcessDate value for this MGOTransactionProcessing.
     * 
     * @param csrProcessDate
     */
    public void setCsrProcessDate(java.util.Calendar csrProcessDate) {
        this.csrProcessDate = csrProcessDate;
    }


    /**
     * Gets the achControlFileSeqNbr value for this MGOTransactionProcessing.
     * 
     * @return achControlFileSeqNbr
     */
    public long getAchControlFileSeqNbr() {
        return achControlFileSeqNbr;
    }


    /**
     * Sets the achControlFileSeqNbr value for this MGOTransactionProcessing.
     * 
     * @param achControlFileSeqNbr
     */
    public void setAchControlFileSeqNbr(long achControlFileSeqNbr) {
        this.achControlFileSeqNbr = achControlFileSeqNbr;
    }


    /**
     * Gets the receivedDate value for this MGOTransactionProcessing.
     * 
     * @return receivedDate
     */
    public java.util.Calendar getReceivedDate() {
        return receivedDate;
    }


    /**
     * Sets the receivedDate value for this MGOTransactionProcessing.
     * 
     * @param receivedDate
     */
    public void setReceivedDate(java.util.Calendar receivedDate) {
        this.receivedDate = receivedDate;
    }


    /**
     * Gets the statusUpdateDate value for this MGOTransactionProcessing.
     * 
     * @return statusUpdateDate
     */
    public java.util.Calendar getStatusUpdateDate() {
        return statusUpdateDate;
    }


    /**
     * Sets the statusUpdateDate value for this MGOTransactionProcessing.
     * 
     * @param statusUpdateDate
     */
    public void setStatusUpdateDate(java.util.Calendar statusUpdateDate) {
        this.statusUpdateDate = statusUpdateDate;
    }


    /**
     * Gets the receiveAgentConfirmationNumber value for this MGOTransactionProcessing.
     * 
     * @return receiveAgentConfirmationNumber
     */
    public java.lang.String getReceiveAgentConfirmationNumber() {
        return receiveAgentConfirmationNumber;
    }


    /**
     * Sets the receiveAgentConfirmationNumber value for this MGOTransactionProcessing.
     * 
     * @param receiveAgentConfirmationNumber
     */
    public void setReceiveAgentConfirmationNumber(java.lang.String receiveAgentConfirmationNumber) {
        this.receiveAgentConfirmationNumber = receiveAgentConfirmationNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MGOTransactionProcessing)) return false;
        MGOTransactionProcessing other = (MGOTransactionProcessing) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.transactionId == other.getTransactionId() &&
            ((this.referenceNumber==null && other.getReferenceNumber()==null) || 
             (this.referenceNumber!=null &&
              this.referenceNumber.equals(other.getReferenceNumber()))) &&
            ((this.sendDate==null && other.getSendDate()==null) || 
             (this.sendDate!=null &&
              this.sendDate.equals(other.getSendDate()))) &&
            ((this.submittedDate==null && other.getSubmittedDate()==null) || 
             (this.submittedDate!=null &&
              this.submittedDate.equals(other.getSubmittedDate()))) &&
            ((this.riskLevelCode==null && other.getRiskLevelCode()==null) || 
             (this.riskLevelCode!=null &&
              this.riskLevelCode.equals(other.getRiskLevelCode()))) &&
            ((this.csrProcessUserId==null && other.getCsrProcessUserId()==null) || 
             (this.csrProcessUserId!=null &&
              this.csrProcessUserId.equals(other.getCsrProcessUserId()))) &&
            ((this.csrProcessDate==null && other.getCsrProcessDate()==null) || 
             (this.csrProcessDate!=null &&
              this.csrProcessDate.equals(other.getCsrProcessDate()))) &&
            this.achControlFileSeqNbr == other.getAchControlFileSeqNbr() &&
            ((this.receivedDate==null && other.getReceivedDate()==null) || 
             (this.receivedDate!=null &&
              this.receivedDate.equals(other.getReceivedDate()))) &&
            ((this.statusUpdateDate==null && other.getStatusUpdateDate()==null) || 
             (this.statusUpdateDate!=null &&
              this.statusUpdateDate.equals(other.getStatusUpdateDate()))) &&
            ((this.receiveAgentConfirmationNumber==null && other.getReceiveAgentConfirmationNumber()==null) || 
             (this.receiveAgentConfirmationNumber!=null &&
              this.receiveAgentConfirmationNumber.equals(other.getReceiveAgentConfirmationNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getTransactionId()).hashCode();
        if (getReferenceNumber() != null) {
            _hashCode += getReferenceNumber().hashCode();
        }
        if (getSendDate() != null) {
            _hashCode += getSendDate().hashCode();
        }
        if (getSubmittedDate() != null) {
            _hashCode += getSubmittedDate().hashCode();
        }
        if (getRiskLevelCode() != null) {
            _hashCode += getRiskLevelCode().hashCode();
        }
        if (getCsrProcessUserId() != null) {
            _hashCode += getCsrProcessUserId().hashCode();
        }
        if (getCsrProcessDate() != null) {
            _hashCode += getCsrProcessDate().hashCode();
        }
        _hashCode += new Long(getAchControlFileSeqNbr()).hashCode();
        if (getReceivedDate() != null) {
            _hashCode += getReceivedDate().hashCode();
        }
        if (getStatusUpdateDate() != null) {
            _hashCode += getStatusUpdateDate().hashCode();
        }
        if (getReceiveAgentConfirmationNumber() != null) {
            _hashCode += getReceiveAgentConfirmationNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" ReferenceNumber=").append(getReferenceNumber());
        buffer.append(" SendDate=").append(getSendDate());
        buffer.append(" SubmittedDate=").append(getSubmittedDate());
        buffer.append(" RiskLevelCode=").append(getRiskLevelCode());
        buffer.append(" CsrProcessUserId=").append(getCsrProcessUserId());
        buffer.append(" CsrProcessDate=").append(getCsrProcessDate());
        buffer.append(" AchControlFileSeqNbr=").append(getAchControlFileSeqNbr());
        buffer.append(" ReceivedDate=").append(getReceivedDate());
        buffer.append(" StatusUpdateDate=").append(getStatusUpdateDate());
        buffer.append(" ReceiveAgentConfirmationNumber=").append(getReceiveAgentConfirmationNumber());
        
        return buffer.toString();
    }
}
