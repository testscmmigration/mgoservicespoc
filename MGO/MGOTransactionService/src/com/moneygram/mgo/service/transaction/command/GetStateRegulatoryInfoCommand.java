/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.util.List;

import com.moneygram.common.dao.DAOException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.GetStateRegulatoryInfoRequest;
import com.moneygram.mgo.service.transaction.GetStateRegulatoryInfoResponse;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.StateRegulatoryInfo;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get State Regulatory Info Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 20013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/03 01:03:15 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class GetStateRegulatoryInfoCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetStateRegulatoryInfoCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetStateRegulatoryInfoRequest commandRequest = (GetStateRegulatoryInfoRequest) request;
        String contentVerId = commandRequest.getContentVerId();
        String isoSubDivCode = commandRequest.getIsoSubDivCode();
        String isoLangNemonicCode = commandRequest.getIsoLangCode();
        Long addressId = commandRequest.getAddressId();
        logger.debug("Request: contentVerId = "+contentVerId+", isoSubDivCode = "+isoSubDivCode+", isoLangNemonicCode = "+isoLangNemonicCode+", addressId = "+addressId);
        List<StateRegulatoryInfo> stateRegulatoryInfoList = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            stateRegulatoryInfoList = dao.getStateRegulatoryInfo(contentVerId, isoSubDivCode, isoLangNemonicCode, addressId);
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve state regulatory info", e);
        }

        GetStateRegulatoryInfoResponse response = new GetStateRegulatoryInfoResponse();
        StateRegulatoryInfo[] array = null;
        if (stateRegulatoryInfoList != null && stateRegulatoryInfoList.size() > 0) {
            array = (StateRegulatoryInfo[])stateRegulatoryInfoList.toArray(new StateRegulatoryInfo[stateRegulatoryInfoList.size()]); 
        }
        response.setStateRegulatoryInfoList(array);
        
        return response;
    }

   

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetStateRegulatoryInfoRequest;
    }

}
