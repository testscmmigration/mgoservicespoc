package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Save Transaction Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/07/08 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SaveTransactionRequest  extends BaseOperationRequest {
    private InitiatedTransaction transaction;

    public SaveTransactionRequest() {
    }

    public SaveTransactionRequest(
           RequestHeader header,
           InitiatedTransaction transaction) {
        super(header);
        this.transaction = transaction;
    }


    /**
     * Gets the transaction value for this SaveTransactionRequest.
     * 
     * @return transaction
     */
    public InitiatedTransaction getTransaction() {
        return transaction;
    }


    /**
     * Sets the transaction value for this SaveTransactionRequest.
     * 
     * @param transaction
     */
    public void setTransaction(InitiatedTransaction transaction) {
        this.transaction = transaction;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveTransactionRequest)) return false;
        SaveTransactionRequest other = (SaveTransactionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transaction==null && other.getTransaction()==null) || 
             (this.transaction!=null &&
              this.transaction.equals(other.getTransaction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransaction() != null) {
            _hashCode += getTransaction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Transaction=").append(getTransaction());
        return buffer.toString();
    }
}
