/**
 * GetMaxStateRegulatoryVersionResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationResponse;

public class GetMaxStateRegulatoryVersionResponse  extends BaseOperationResponse{
	private java.lang.String maxContentVerId;

    public GetMaxStateRegulatoryVersionResponse() {
    }

  


    /**
     * Gets the maxContentVerId value for this GetMaxStateRegulatoryVersionResponse.
     * 
     * @return maxContentVerId
     */
    public java.lang.String getMaxContentVerId() {
        return maxContentVerId;
    }


    /**
     * Sets the maxContentVerId value for this GetMaxStateRegulatoryVersionResponse.
     * 
     * @param maxContentVerId
     */
    public void setMaxContentVerId(java.lang.String maxContentVerId) {
        this.maxContentVerId = maxContentVerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMaxStateRegulatoryVersionResponse)) return false;
        GetMaxStateRegulatoryVersionResponse other = (GetMaxStateRegulatoryVersionResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.maxContentVerId==null && other.getMaxContentVerId()==null) || 
             (this.maxContentVerId!=null &&
              this.maxContentVerId.equals(other.getMaxContentVerId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMaxContentVerId() != null) {
            _hashCode += getMaxContentVerId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMaxStateRegulatoryVersionResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "GetMaxStateRegulatoryVersionResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxContentVerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "maxContentVerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
