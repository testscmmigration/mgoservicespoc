/**
 * GetCCAuthConfIdResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationResponse;

public class GetCCAuthConfIdResponse   extends BaseOperationResponse{
	
    private java.lang.String cCAuthConfId;

    public GetCCAuthConfIdResponse() {
    }

    
    /**
     * Gets the cCAuthConfId value for this GetCCAuthConfIdResponse.
     * 
     * @return cCAuthConfId
     */
    public java.lang.String getCCAuthConfId() {
        return cCAuthConfId;
    }


    /**
     * Sets the cCAuthConfId value for this GetCCAuthConfIdResponse.
     * 
     * @param cCAuthConfId
     */
    public void setCCAuthConfId(java.lang.String cCAuthConfId) {
        this.cCAuthConfId = cCAuthConfId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCCAuthConfIdResponse)) return false;
        GetCCAuthConfIdResponse other = (GetCCAuthConfIdResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.cCAuthConfId==null && other.getCCAuthConfId()==null) || 
             (this.cCAuthConfId!=null &&
              this.cCAuthConfId.equals(other.getCCAuthConfId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCCAuthConfId() != null) {
            _hashCode += getCCAuthConfId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }


	@Override
	public String toString() {
		return "GetCCAuthConfIdResponse [cCAuthConfId=" + cCAuthConfId
				+ ", __equalsCalc=" + __equalsCalc + ", __hashCodeCalc="
				+ __hashCodeCalc + "]";
	}

    
}
