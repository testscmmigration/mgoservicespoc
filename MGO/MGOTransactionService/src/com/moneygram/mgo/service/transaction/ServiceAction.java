/**
 * ServiceAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;



/**
 *
 * Service Action.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/09/08 19:37:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ServiceAction implements java.io.Serializable {
    private static final long serialVersionUID = 4237846894848532433L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ServiceAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _saveTransaction = "saveTransaction";
    public static final java.lang.String _updateTransaction = "updateTransaction";
    public static final java.lang.String _getTransaction = "getTransaction";
    public static final java.lang.String _getTransactionsByConsumer = "getTransactionsByConsumer";
    public static final java.lang.String _updateTransactionStatus = "updateTransactionStatus";
    public static final java.lang.String _updateAccount = "updateAccount";
    public static final java.lang.String _getConsumerTransactionsTotal = "getConsumerTransactionsTotal";
    public static final ServiceAction saveTransaction = new ServiceAction(_saveTransaction);
    public static final ServiceAction updateTransaction = new ServiceAction(_updateTransaction);
    public static final ServiceAction getTransaction = new ServiceAction(_getTransaction);
    public static final ServiceAction getTransactionsByConsumer = new ServiceAction(_getTransactionsByConsumer);
    public static final ServiceAction updateTransactionStatus = new ServiceAction(_updateTransactionStatus);
    public static final ServiceAction updateAccount = new ServiceAction(_updateAccount);
    public static final ServiceAction getConsumerTransactionsTotal = new ServiceAction(_getConsumerTransactionsTotal);

    public java.lang.String getValue() { return _value_;}

    public static ServiceAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ServiceAction enumeration = (ServiceAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ServiceAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
