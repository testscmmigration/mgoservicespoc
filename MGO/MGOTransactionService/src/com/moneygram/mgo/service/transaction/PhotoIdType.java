/**
 * PhotoIdType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * Photo Id Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/08 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class PhotoIdType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PhotoIdType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _DRV = "DRV";
    public static final java.lang.String _PAS = "PAS";
    public static final java.lang.String _STA = "STA";
    public static final java.lang.String _GOV = "GOV";
    public static final java.lang.String _ALN = "ALN";
    public static final PhotoIdType DRV = new PhotoIdType(_DRV);
    public static final PhotoIdType PAS = new PhotoIdType(_PAS);
    public static final PhotoIdType STA = new PhotoIdType(_STA);
    public static final PhotoIdType GOV = new PhotoIdType(_GOV);
    public static final PhotoIdType ALN = new PhotoIdType(_ALN);
    public java.lang.String getValue() { return _value_;}
    public static PhotoIdType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PhotoIdType enumeration = (PhotoIdType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PhotoIdType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}

}
