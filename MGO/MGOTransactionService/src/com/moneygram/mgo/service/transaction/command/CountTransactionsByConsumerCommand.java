/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.CountConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.CountConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Count Transactions By Consumer Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2011</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/04/26 21:17:36 $ </td><tr><td>
 * @author   </td><td>$Author: uo87 $ </td>
 *</table>
 *</div>
 */
public class CountTransactionsByConsumerCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(CountTransactionsByConsumerCommand.class);
    private static final int MAX_RECORDS = 200;

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        CountConsumerTransactionsRequest commandRequest = (CountConsumerTransactionsRequest) request;
        logger.error("Request: " + commandRequest.getConsumerId());
        int transactionCount = 0;
        List<TransactionSummary> transactions = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            transactionCount = dao.countTransactions(commandRequest.getConsumerId(), commandRequest.getStartDate(), commandRequest.getEndDate(), commandRequest.getSendCountryCode(), commandRequest.getReceiveCountryCode(), createProductList(commandRequest.getProductTypes()));
            
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve transaction count", e);
        }

        CountConsumerTransactionsResponse response = new CountConsumerTransactionsResponse();
        response.setTransactionCount(transactionCount);
        
        return response;
    }

    private String createProductList(MGOProductType[] productTypes) throws CommandException {
        StringBuffer buffer = new StringBuffer();
        if (productTypes == null || productTypes.length == 0) {
            throw new DataFormatException("At least one productType is required");
        }
        for (int i = 0; i < productTypes.length; i++) {
            if (i != 0) {
                buffer.append(",");
            }
            buffer.append(productTypes[i].getValue());
        }
        return buffer.toString();
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof CountConsumerTransactionsRequest;
    }

}
