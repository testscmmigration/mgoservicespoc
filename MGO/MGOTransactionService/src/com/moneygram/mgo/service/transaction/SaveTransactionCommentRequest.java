package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Save Transaction Comment Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/05/15 15:23:00 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class SaveTransactionCommentRequest  extends BaseOperationRequest {
    private Long transactionId;
    private String commentReasoncode;
    private String commentText;

    public SaveTransactionCommentRequest() {
    }

    public SaveTransactionCommentRequest(
           RequestHeader header,
           Long transactionId,
           String commentReasonCode,
           String commentText) {
        super(header);
        this.setTransactionId(transactionId);
        this.commentReasoncode = commentReasonCode;
        this.setCommentText(commentText);
    }



    /**
	 * @return the commentReasoncode
	 */
	public String getCommentReasoncode() {
		return commentReasoncode;
	}

	/**
	 * @param commentReasoncode the commentReasoncode to set
	 */
	public void setCommentReasoncode(String commentReasoncode) {
		this.commentReasoncode = commentReasoncode;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getCommentText() {
		return commentText;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveTransactionCommentRequest)) return false;
        SaveTransactionCommentRequest other = (SaveTransactionCommentRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.commentReasoncode==null && other.getCommentReasoncode()==null) || 
             (this.commentReasoncode!=null &&
              this.commentReasoncode.equals(other.getCommentReasoncode()))) &&
            ((this.commentText==null && other.getCommentText()==null) || 
             (this.commentText!=null &&
              this.commentText.equals(other.getCommentText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getCommentReasoncode() != null) {
            _hashCode += getCommentReasoncode().hashCode();
        }
        if (getCommentText() != null) {
            _hashCode += getCommentText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" CommentReasonCode=").append(getCommentReasoncode());
        buffer.append(" CommentText=").append(getCommentText());
        return buffer.toString();
    }
}
