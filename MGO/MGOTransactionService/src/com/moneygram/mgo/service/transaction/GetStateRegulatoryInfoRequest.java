/**
 * GetStateRegulatoryInfoRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class GetStateRegulatoryInfoRequest extends BaseOperationRequest {
    private java.lang.String contentVerId;

    private java.lang.String isoSubDivCode;

    private java.lang.String isoLangCode;

    private java.lang.Long addressId;

    public GetStateRegulatoryInfoRequest() {
    }

    public GetStateRegulatoryInfoRequest(
    		RequestHeader header,
           java.lang.String contentVerId,
           java.lang.String isoSubDivCode,
           java.lang.String isoLangCode,
           java.lang.Long addressId) {
        super(
            header);
        this.contentVerId = contentVerId;
        this.isoSubDivCode = isoSubDivCode;
        this.isoLangCode = isoLangCode;
        this.addressId = addressId;
    }

  

    /**
     * Gets the contentVerId value for this GetStateRegulatoryInfoRequest.
     * 
     * @return contentVerId
     */
    public java.lang.String getContentVerId() {
        return contentVerId;
    }


    /**
     * Sets the contentVerId value for this GetStateRegulatoryInfoRequest.
     * 
     * @param contentVerId
     */
    public void setContentVerId(java.lang.String contentVerId) {
        this.contentVerId = contentVerId;
    }


    /**
     * Gets the isoSubDivCode value for this GetStateRegulatoryInfoRequest.
     * 
     * @return isoSubDivCode
     */
    public java.lang.String getIsoSubDivCode() {
        return isoSubDivCode;
    }


    /**
     * Sets the isoSubDivCode value for this GetStateRegulatoryInfoRequest.
     * 
     * @param isoSubDivCode
     */
    public void setIsoSubDivCode(java.lang.String isoSubDivCode) {
        this.isoSubDivCode = isoSubDivCode;
    }


    /**
     * Gets the isoLangCode value for this GetStateRegulatoryInfoRequest.
     * 
     * @return isoLangCode
     */
    public java.lang.String getIsoLangCode() {
        return isoLangCode;
    }


    /**
     * Sets the isoLangCode value for this GetStateRegulatoryInfoRequest.
     * 
     * @param isoLangCode
     */
    public void setIsoLangCode(java.lang.String isoLangCode) {
        this.isoLangCode = isoLangCode;
    }


    /**
     * Gets the addressId value for this GetStateRegulatoryInfoRequest.
     * 
     * @return addressId
     */
    public java.lang.Long getAddressId() {
        return addressId;
    }


    /**
     * Sets the addressId value for this GetStateRegulatoryInfoRequest.
     * 
     * @param addressId
     */
    public void setAddressId(java.lang.Long addressId) {
        this.addressId = addressId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetStateRegulatoryInfoRequest)) return false;
        GetStateRegulatoryInfoRequest other = (GetStateRegulatoryInfoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.contentVerId==null && other.getContentVerId()==null) || 
             (this.contentVerId!=null &&
              this.contentVerId.equals(other.getContentVerId()))) &&
            ((this.isoSubDivCode==null && other.getIsoSubDivCode()==null) || 
             (this.isoSubDivCode!=null &&
              this.isoSubDivCode.equals(other.getIsoSubDivCode()))) &&
            ((this.isoLangCode==null && other.getIsoLangCode()==null) || 
             (this.isoLangCode!=null &&
              this.isoLangCode.equals(other.getIsoLangCode()))) &&
            ((this.addressId==null && other.getAddressId()==null) || 
             (this.addressId!=null &&
              this.addressId.equals(other.getAddressId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getContentVerId() != null) {
            _hashCode += getContentVerId().hashCode();
        }
        if (getIsoSubDivCode() != null) {
            _hashCode += getIsoSubDivCode().hashCode();
        }
        if (getIsoLangCode() != null) {
            _hashCode += getIsoLangCode().hashCode();
        }
        if (getAddressId() != null) {
            _hashCode += getAddressId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetStateRegulatoryInfoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "GetStateRegulatoryInfoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentVerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "contentVerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoSubDivCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "isoSubDivCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoLangCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "isoLangCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "addressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
