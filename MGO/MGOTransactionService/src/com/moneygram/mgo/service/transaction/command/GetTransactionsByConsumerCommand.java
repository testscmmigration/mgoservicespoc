/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get Transactions By Consumer Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.9 $ $Date: 2011/09/16 01:03:15 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class GetTransactionsByConsumerCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetTransactionsByConsumerCommand.class);
    private static final int MAX_RECORDS = 200;

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetConsumerTransactionsRequest commandRequest = (GetConsumerTransactionsRequest) request;

        List<TransactionSummary> transactions = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            int maxRecords = commandRequest.getMaxRecords() == null ? MAX_RECORDS : commandRequest.getMaxRecords();
            transactions = dao.getTransactions(commandRequest.getConsumerId(), commandRequest.getStartDate(), commandRequest.getEndDate(), maxRecords, commandRequest.getReceiveAgentCode(), null, commandRequest.getReceiveCountry(), createProductList(commandRequest.getProductTypes()), commandRequest.getPartnerSiteId(), commandRequest.getDeliveryOption(), commandRequest.getReceiveAgent());
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve transactions", e);
        }

        GetConsumerTransactionsResponse response = new GetConsumerTransactionsResponse();
        TransactionSummary[] array = null;
        if (transactions != null && transactions.size() > 0) {
            array = (TransactionSummary[])transactions.toArray(new TransactionSummary[transactions.size()]); 
        }
        response.setTransactions(array);
        
        return response;
    }

    private String createProductList(MGOProductType[] productTypes) throws CommandException {
        StringBuffer buffer = new StringBuffer();
        if (productTypes == null || productTypes.length == 0) {
            throw new DataFormatException("At least one productType is required");
        }
        for (int i = 0; i < productTypes.length; i++) {
            if (i != 0) {
                buffer.append(",");
            }
            buffer.append(productTypes[i].getValue());
        }
        return buffer.toString();
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetConsumerTransactionsRequest;
    }

}
