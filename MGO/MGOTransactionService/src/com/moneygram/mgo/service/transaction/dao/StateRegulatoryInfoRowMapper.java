/*
 * Created on Dec 10, 2007
 *
 */
package com.moneygram.mgo.service.transaction.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.StateRegulatoryInfo;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * State Regulatory Info Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 20013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/03 17:38:45 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class StateRegulatoryInfoRowMapper extends BaseRowMapper {

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        

    	StateRegulatoryInfo stateRegulatoryInfo = new StateRegulatoryInfo();
        stateRegulatoryInfo.setIsoSubDivCode(rs.getString("iso_subdiv_code"));
        stateRegulatoryInfo.setRegulatoryAgencyName(rs.getString("reg_agcy_name"));
        stateRegulatoryInfo.setRegulatoryAgencyUrl(rs.getString("reg_agcy_url_text"));
        stateRegulatoryInfo.setRegulatoryAgencyPhone(rs.getString("reg_agcy_phn_nbr"));
        
        return stateRegulatoryInfo;
    }
    

}
