/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.OperationResponse;

/**
 * 
 * Get Transaction Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/07/23 20:41:55 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetTransactionResponse implements OperationResponse {

    private MGOTransaction transaction;

    public GetTransactionResponse() {
    }

    /**
     * Gets the transaction value for this GetTransactionResponse.
     * 
     * @return transaction
     */
    public MGOTransaction getTransaction() {
        return transaction;
    }


    /**
     * Sets the transaction value for this GetTransactionResponse.
     * 
     * @param transaction
     */
    public void setTransaction(MGOTransaction transaction) {
        this.transaction = transaction;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTransactionResponse)) return false;
        GetTransactionResponse other = (GetTransactionResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transaction==null && other.getTransaction()==null) || 
             (this.transaction!=null &&
              this.transaction.equals(other.getTransaction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransaction() != null) {
            _hashCode += getTransaction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" Transaction="+ getTransaction();
    }
}
