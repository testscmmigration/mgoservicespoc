/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.transaction.SaveTransactionRequest;
import com.moneygram.mgo.service.transaction.SaveTransactionResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Save Transaction Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/08/19 13:55:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SaveTransactionCommand extends TransactionalCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(SaveTransactionCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        SaveTransactionRequest commandRequest = (SaveTransactionRequest) request;

        //process update request
        Number transactionId = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            transactionId = dao.saveTransaction(commandRequest.getTransaction());
        } catch (Exception e) {
            throw new CommandException("Failed to save transaction", e);
        }
        
        SaveTransactionResponse response = new SaveTransactionResponse();
        response.setTransactionId(transactionId.longValue());
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof SaveTransactionRequest;
    }

}
