package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Save State Regulatory Info Request. Dodd Frank implementation
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/02 22:21:34 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class SaveStateRegulatoryInfoRequest  extends BaseOperationRequest {
	
    private com.moneygram.mgo.service.transaction.StateRegulatoryInfo stateRegulatoryInfo;

    public SaveStateRegulatoryInfoRequest() {
    }

    public SaveStateRegulatoryInfoRequest(
           RequestHeader header,
           com.moneygram.mgo.service.transaction.StateRegulatoryInfo stateRegulatoryInfo) {
        super(
            header);
        this.stateRegulatoryInfo = stateRegulatoryInfo;
    }


    /**
     * Gets the stateRegulatoryInfo value for this SaveStateRegulatoryInfoRequest.
     * 
     * @return stateRegulatoryInfo
     */
    public com.moneygram.mgo.service.transaction.StateRegulatoryInfo getStateRegulatoryInfo() {
        return stateRegulatoryInfo;
    }


    /**
     * Sets the stateRegulatoryInfo value for this SaveStateRegulatoryInfoRequest.
     * 
     * @param stateRegulatoryInfo
     */
    public void setStateRegulatoryInfo(com.moneygram.mgo.service.transaction.StateRegulatoryInfo stateRegulatoryInfo) {
        this.stateRegulatoryInfo = stateRegulatoryInfo;
    }

   

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveStateRegulatoryInfoRequest)) return false;
        SaveStateRegulatoryInfoRequest other = (SaveStateRegulatoryInfoRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.stateRegulatoryInfo==null && other.getStateRegulatoryInfo()==null) || 
             (this.stateRegulatoryInfo!=null &&
              this.stateRegulatoryInfo.equals(other.getStateRegulatoryInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getStateRegulatoryInfo() != null) {
            _hashCode += getStateRegulatoryInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaveStateRegulatoryInfoRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "SaveStateRegulatoryInfoRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateRegulatoryInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "stateRegulatoryInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "StateRegulatoryInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
