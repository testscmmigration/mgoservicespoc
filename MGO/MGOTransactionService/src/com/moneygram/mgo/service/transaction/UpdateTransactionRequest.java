package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 *
 * Update Transaction Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/04/29 13:27:23 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class UpdateTransactionRequest  extends BaseOperationRequest {
    private InitiatedTransaction transaction;

    private Long transactionId;


    public UpdateTransactionRequest() {
    }

    public UpdateTransactionRequest(
           RequestHeader header,
           InitiatedTransaction transaction,
           Long transactionId) {
        super(header);
        this.transaction = transaction;
        this.transactionId = transactionId;
    }


    /**
     * Gets the transaction value for this UpdateTransactionRequest.
     *
     * @return transaction
     */
    public InitiatedTransaction getTransaction() {
        return transaction;
    }


    /**
     * Sets the transaction value for this UpdateTransactionRequest.
     *
     * @param transaction
     */
    public void setTransaction(InitiatedTransaction transaction) {
        this.transaction = transaction;
    }

    public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateTransactionRequest)) return false;
        UpdateTransactionRequest other = (UpdateTransactionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.transaction==null && other.getTransaction()==null) ||
             (this.transaction!=null &&
              this.transaction.equals(other.getTransaction()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) ||
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransaction() != null) {
            _hashCode += getTransaction().hashCode();
        }

        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }

        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Transaction=").append(getTransaction());
        buffer.append(" TransactionId=").append(getTransactionId());
        return buffer.toString();
    }
}
