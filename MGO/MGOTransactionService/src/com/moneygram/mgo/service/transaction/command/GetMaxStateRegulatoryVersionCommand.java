/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetMaxStateRegulatoryVersionRequest;
import com.moneygram.mgo.service.transaction.GetMaxStateRegulatoryVersionResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get Max State Regulatory Version Command. (Dodd Frank)
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/01/03 21:17:36 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class GetMaxStateRegulatoryVersionCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetMaxStateRegulatoryVersionCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetMaxStateRegulatoryVersionRequest commandRequest = (GetMaxStateRegulatoryVersionRequest) request;
        logger.debug("Request: isoSubdivCode = " + commandRequest.getIsoSubDivCode());
        String maxContentVerId = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            maxContentVerId = dao.getMaxStateRegulatoryVersion(commandRequest.getIsoSubDivCode());
            
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve max state regulatory content version", e);
        }

        GetMaxStateRegulatoryVersionResponse response = new GetMaxStateRegulatoryVersionResponse();
        response.setMaxContentVerId(maxContentVerId);
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetMaxStateRegulatoryVersionRequest;
    }

}
