/**
 * GetConsumerTransactionsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Count Consumer Transactions Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2011/04/25 04:57:25 $ </td><tr><td>
 * @author   </td><td>$Author: uo87 $ </td>
 *</table>
 *</div>
 */
public class CountConsumerTransactionsRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 5389556608113380591L;

    private long consumerId;

    private java.util.Calendar startDate;

    private java.util.Calendar endDate;

    private java.lang.Integer maxRecords;

    private java.lang.String sendCountryCode;
    
    private java.lang.String receiveCountryCode;

    private MGOProductType[] productTypes;

    public CountConsumerTransactionsRequest() {
    }

    public CountConsumerTransactionsRequest(
           RequestHeader header,
           long consumerId,
           java.util.Calendar startDate,
           java.util.Calendar endDate,
           java.lang.Integer maxRecords,
           java.lang.String sendCountryCode,
           java.lang.String receiveCountryCode,
           MGOProductType[] productTypes) {
        super(
            header);
        this.consumerId = consumerId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maxRecords = maxRecords;
        this.sendCountryCode = sendCountryCode;
        this.receiveCountryCode = receiveCountryCode;
        this.productTypes = productTypes;
    }


    /**
     * Gets the consumerId value for this GetConsumerTransactionsRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this GetConsumerTransactionsRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the startDate value for this GetConsumerTransactionsRequest.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this GetConsumerTransactionsRequest.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this GetConsumerTransactionsRequest.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this GetConsumerTransactionsRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the maxRecords value for this GetConsumerTransactionsRequest.
     * 
     * @return maxRecords
     */
    public java.lang.Integer getMaxRecords() {
        return maxRecords;
    }


    /**
     * Sets the maxRecords value for this GetConsumerTransactionsRequest.
     * 
     * @param maxRecords
     */
    public void setMaxRecords(java.lang.Integer maxRecords) {
        this.maxRecords = maxRecords;
    }


    /**
     * Gets the sendCountryCode value for this CountConsumerTransactionsRequest.
     * 
     * @return sendCountryCode
     */
    public java.lang.String getSendCountryCode() {
        return sendCountryCode;
    }


    /**
     * Sets the sendCountryCode value for this CountConsumerTransactionsRequest.
     * 
     * @param sendCountryCode
     */
    public void setSendCountryCode(java.lang.String sendCountryCode) {
        this.sendCountryCode = sendCountryCode;
    }
    
    /**
     * Gets the receiveCountryCode value for this CountConsumerTransactionsRequest.
     * 
     * @return receiveCountryCode
     */
    public java.lang.String getReceiveCountryCode() {
        return receiveCountryCode;
    }


    /**
     * Sets the receiveCountryCode value for this CountConsumerTransactionsRequest.
     * 
     * @param receiveCountryCode
     */
    public void setReceiveCountryCode(java.lang.String receiveCountryCode) {
        this.receiveCountryCode = receiveCountryCode;
    }


    /**
     * Gets the productTypes value for this GetConsumerTransactionsRequest.
     * 
     * @return productTypes
     */
    public MGOProductType[] getProductTypes() {
        return productTypes;
    }


    /**
     * Sets the productTypes value for this GetConsumerTransactionsRequest.
     * 
     * @param productTypes
     */
    public void setProductTypes(MGOProductType[] productTypes) {
        this.productTypes = productTypes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CountConsumerTransactionsRequest)) return false;
        CountConsumerTransactionsRequest other = (CountConsumerTransactionsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId() &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.maxRecords==null && other.getMaxRecords()==null) || 
             (this.maxRecords!=null &&
              this.maxRecords.equals(other.getMaxRecords()))) &&
            ((this.receiveCountryCode==null && other.getReceiveCountryCode()==null) || 
             (this.receiveCountryCode!=null &&
              this.receiveCountryCode.equals(other.getReceiveCountryCode()))) &&
            ((this.sendCountryCode==null && other.getSendCountryCode()==null) || 
             (this.sendCountryCode!=null &&
              this.sendCountryCode.equals(other.getSendCountryCode()))) &&
            ((this.productTypes==null && other.getProductTypes()==null) || 
             (this.productTypes!=null &&
              java.util.Arrays.equals(this.productTypes, other.getProductTypes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getMaxRecords() != null) {
            _hashCode += getMaxRecords().hashCode();
        }
        if (getSendCountryCode() != null) {
            _hashCode += getSendCountryCode().hashCode();
        }
        if (getReceiveCountryCode() != null) {
            _hashCode += getReceiveCountryCode().hashCode();
        }
        if (getProductTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ConsumerId=").append(getConsumerId());
        buffer.append(" StartDate=").append(getStartDate());
        buffer.append(" EndDate=").append(getEndDate());
        buffer.append(" MaxRecords=").append(getMaxRecords());
        buffer.append(" SendCountryCode=").append(getSendCountryCode());
        buffer.append(" ReceiveCountryCode=").append(getReceiveCountryCode());
        buffer.append(" ProductTypes=").append(getProductTypes());
        return buffer.toString();
    }
}
