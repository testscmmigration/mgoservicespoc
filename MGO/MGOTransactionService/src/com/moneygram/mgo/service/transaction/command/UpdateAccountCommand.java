/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.UpdateAccountRequest;
import com.moneygram.mgo.service.transaction.UpdateAccountResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Update Account Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/19 13:55:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateAccountCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(UpdateAccountCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        UpdateAccountRequest commandRequest = (UpdateAccountRequest) request;

        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            dao.updateAccount(commandRequest);
            
        } catch (Exception e) {
            throw new CommandException("Failed to update transaction account", e);
        }

        UpdateAccountResponse response = new UpdateAccountResponse();
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof UpdateAccountRequest;
    }

}
