/**
 * StateRegulatoryInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

public class StateRegulatoryInfo  implements java.io.Serializable {
    private java.lang.String contentVerId;

    private java.lang.String isoSubDivCode;

    private java.lang.String isoLangCode;

    private java.lang.String regulatoryAgencyName;

    private java.lang.String regulatoryAgencyUrl;

    private java.lang.String regulatoryAgencyPhone;

    public StateRegulatoryInfo() {
    }

    public StateRegulatoryInfo(
           java.lang.String contentVerId,
           java.lang.String isoSubDivCode,
           java.lang.String isoLangCode,
           java.lang.String regulatoryAgencyName,
           java.lang.String regulatoryAgencyUrl,
           java.lang.String regulatoryAgencyPhone) {
           this.contentVerId = contentVerId;
           this.isoSubDivCode = isoSubDivCode;
           this.isoLangCode = isoLangCode;
           this.regulatoryAgencyName = regulatoryAgencyName;
           this.regulatoryAgencyUrl = regulatoryAgencyUrl;
           this.regulatoryAgencyPhone = regulatoryAgencyPhone;
    }


    /**
     * Gets the contentVerId value for this StateRegulatoryInfo.
     * 
     * @return contentVerId
     */
    public java.lang.String getContentVerId() {
        return contentVerId;
    }


    /**
     * Sets the contentVerId value for this StateRegulatoryInfo.
     * 
     * @param contentVerId
     */
    public void setContentVerId(java.lang.String contentVerId) {
        this.contentVerId = contentVerId;
    }


    /**
     * Gets the isoSubDivCode value for this StateRegulatoryInfo.
     * 
     * @return isoSubDivCode
     */
    public java.lang.String getIsoSubDivCode() {
        return isoSubDivCode;
    }


    /**
     * Sets the isoSubDivCode value for this StateRegulatoryInfo.
     * 
     * @param isoSubDivCode
     */
    public void setIsoSubDivCode(java.lang.String isoSubDivCode) {
        this.isoSubDivCode = isoSubDivCode;
    }


    /**
     * Gets the isoLangCode value for this StateRegulatoryInfo.
     * 
     * @return isoLangCode
     */
    public java.lang.String getIsoLangCode() {
        return isoLangCode;
    }


    /**
     * Sets the isoLangCode value for this StateRegulatoryInfo.
     * 
     * @param isoLangCode
     */
    public void setIsoLangCode(java.lang.String isoLangCode) {
        this.isoLangCode = isoLangCode;
    }


    /**
     * Gets the regulatoryAgencyName value for this StateRegulatoryInfo.
     * 
     * @return regulatoryAgencyName
     */
    public java.lang.String getRegulatoryAgencyName() {
        return regulatoryAgencyName;
    }


    /**
     * Sets the regulatoryAgencyName value for this StateRegulatoryInfo.
     * 
     * @param regulatoryAgencyName
     */
    public void setRegulatoryAgencyName(java.lang.String regulatoryAgencyName) {
        this.regulatoryAgencyName = regulatoryAgencyName;
    }


    /**
     * Gets the regulatoryAgencyUrl value for this StateRegulatoryInfo.
     * 
     * @return regulatoryAgencyUrl
     */
    public java.lang.String getRegulatoryAgencyUrl() {
        return regulatoryAgencyUrl;
    }


    /**
     * Sets the regulatoryAgencyUrl value for this StateRegulatoryInfo.
     * 
     * @param regulatoryAgencyUrl
     */
    public void setRegulatoryAgencyUrl(java.lang.String regulatoryAgencyUrl) {
        this.regulatoryAgencyUrl = regulatoryAgencyUrl;
    }


    /**
     * Gets the regulatoryAgencyPhone value for this StateRegulatoryInfo.
     * 
     * @return regulatoryAgencyPhone
     */
    public java.lang.String getRegulatoryAgencyPhone() {
        return regulatoryAgencyPhone;
    }


    /**
     * Sets the regulatoryAgencyPhone value for this StateRegulatoryInfo.
     * 
     * @param regulatoryAgencyPhone
     */
    public void setRegulatoryAgencyPhone(java.lang.String regulatoryAgencyPhone) {
        this.regulatoryAgencyPhone = regulatoryAgencyPhone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StateRegulatoryInfo)) return false;
        StateRegulatoryInfo other = (StateRegulatoryInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.contentVerId==null && other.getContentVerId()==null) || 
             (this.contentVerId!=null &&
              this.contentVerId.equals(other.getContentVerId()))) &&
            ((this.isoSubDivCode==null && other.getIsoSubDivCode()==null) || 
             (this.isoSubDivCode!=null &&
              this.isoSubDivCode.equals(other.getIsoSubDivCode()))) &&
            ((this.isoLangCode==null && other.getIsoLangCode()==null) || 
             (this.isoLangCode!=null &&
              this.isoLangCode.equals(other.getIsoLangCode()))) &&
            ((this.regulatoryAgencyName==null && other.getRegulatoryAgencyName()==null) || 
             (this.regulatoryAgencyName!=null &&
              this.regulatoryAgencyName.equals(other.getRegulatoryAgencyName()))) &&
            ((this.regulatoryAgencyUrl==null && other.getRegulatoryAgencyUrl()==null) || 
             (this.regulatoryAgencyUrl!=null &&
              this.regulatoryAgencyUrl.equals(other.getRegulatoryAgencyUrl()))) &&
            ((this.regulatoryAgencyPhone==null && other.getRegulatoryAgencyPhone()==null) || 
             (this.regulatoryAgencyPhone!=null &&
              this.regulatoryAgencyPhone.equals(other.getRegulatoryAgencyPhone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContentVerId() != null) {
            _hashCode += getContentVerId().hashCode();
        }
        if (getIsoSubDivCode() != null) {
            _hashCode += getIsoSubDivCode().hashCode();
        }
        if (getIsoLangCode() != null) {
            _hashCode += getIsoLangCode().hashCode();
        }
        if (getRegulatoryAgencyName() != null) {
            _hashCode += getRegulatoryAgencyName().hashCode();
        }
        if (getRegulatoryAgencyUrl() != null) {
            _hashCode += getRegulatoryAgencyUrl().hashCode();
        }
        if (getRegulatoryAgencyPhone() != null) {
            _hashCode += getRegulatoryAgencyPhone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StateRegulatoryInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "StateRegulatoryInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contentVerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "contentVerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoSubDivCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "isoSubDivCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoLangCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "isoLangCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regulatoryAgencyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "regulatoryAgencyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regulatoryAgencyUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "regulatoryAgencyUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regulatoryAgencyPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "regulatoryAgencyPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
