/**
 * MGOProductType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;


/**
 * 
 * MGOProduct Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2011/09/16 01:03:15 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class MGOProductType implements java.io.Serializable {
    private static final long serialVersionUID = -5963170509777394261L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MGOProductType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _EPSEND = "EPSEND";
    public static final java.lang.String _MGSEND = "MGSEND";
    public static final java.lang.String _DSSEND = "DSSEND";
    public static final java.lang.String _ESSEND = "ESSEND";
    public static final java.lang.String _MGCASH = "MGCASH";
    public static final java.lang.String _EPCASH = "EPCASH";
    public static final java.lang.String _CARD = "CARD";
    public static final MGOProductType EPSEND = new MGOProductType(_EPSEND);
    public static final MGOProductType MGSEND = new MGOProductType(_MGSEND);
    public static final MGOProductType DSSEND = new MGOProductType(_DSSEND);
    public static final MGOProductType ESSEND = new MGOProductType(_ESSEND);
    public static final MGOProductType MGCASH = new MGOProductType(_MGCASH);
    public static final MGOProductType EPCASH = new MGOProductType(_EPCASH);
    public static final MGOProductType CARD = new MGOProductType(_CARD);

    public java.lang.String getValue() { return _value_;}
    public static MGOProductType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        MGOProductType enumeration = (MGOProductType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static MGOProductType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
