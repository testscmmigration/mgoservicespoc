/*
 * Created on Dec 10, 2007
 *
 */
package com.moneygram.mgo.service.transaction.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * Transaction Summary Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.9 $ $Date: 2012/01/30 17:38:45 $ </td><tr><td>
 * @author   </td><td>$Author: vf69 $ </td>
 *</table>
 *</div>
 */
public class TransactionSummaryRowMapper extends BaseRowMapper {

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        /*
        emg_tran_id        number(9)       out
        emg_tran_type_code     varchar2(8)     out
        lgcy_ref_nbr       varchar2(8)     out
        snd_tran_date      date            out
        tran_stat_code     char(3)         out
        tran_sub_stat_code     char(3)         out
        tran_stat_date     date            out
        snd_iso_crncy_id       varchar2(3)     out
        snd_face_amt       number(15,3)        out
        rcv_iso_cntry_code     varchar2(3)     out
        rcv_cust_frst_name     varchar2(40)        out
        rcv_cust_last_name     varchar2(40)        out
        rcv_cust_matrnl_name   varchar2(40)        out
        rcv_cust_mid_name      varchar2(40)        out
        intnd_dest_stateprovince_code varchar2(2)         out
        rcv_agent_id       number(8)       out
        rcv_agent_name     varchar2(40)        out
        rcv_agcy_code      varchar2(4)     out
        rcv_agent_ref_nbr  varchar2(15)     out
            */

        ReceiverConsumerName receiverName = new ReceiverConsumerName();
        receiverName.setFirstName(rs.getString("rcv_cust_frst_name"));
        receiverName.setLastName(rs.getString("rcv_cust_last_name"));
        receiverName.setMaternalName(rs.getString("rcv_cust_matrnl_name"));
        receiverName.setMiddleName(rs.getString("rcv_cust_mid_name"));

        TransactionSummary transaction = new TransactionSummary();
        transaction.setStatus(rs.getString("tran_stat_code"));
        transaction.setSubStatus(rs.getString("tran_sub_stat_code"));
        transaction.setStatusDate(DAOUtils.toCalendar(rs.getTimestamp("tran_stat_date")));
        transaction.setTransactionId(rs.getLong("emg_tran_id"));
        transaction.setReferenceNumber(rs.getString("lgcy_ref_nbr"));
        transaction.setSendDate(DAOUtils.toCalendar(rs.getTimestamp("snd_tran_date")));
        transaction.setFaceAmount(rs.getBigDecimal("snd_face_amt"));
        transaction.setReceiverName(receiverName);
        transaction.setProductType(MGOProductType.fromString(rs.getString("emg_tran_type_code")));
        transaction.setSendCurrency(rs.getString("snd_iso_crncy_id"));
        transaction.setFaceAmount(rs.getBigDecimal("snd_face_amt"));
        transaction.setReceiveCountry(rs.getString("rcv_iso_cntry_code"));
        String state = rs.getString("intnd_dest_stateprovince_code");
        if (state != null) {
        	transaction.setReceiveState(state);
        } else {
        	//Note old pre-MGO users don't have 'intnd_dest_stateprovince_code' but will have below column
        	transaction.setReceiveState(rs.getString("rcv_cust_addr_state_name"));
        }
        transaction.setReceiveAgent(new Long(rs.getLong("rcv_agent_id")));
        if (rs.wasNull()) {
            transaction.setReceiveAgent(null);
        }
        transaction.setReceiveAgentName(rs.getString("rcv_agent_name")); 
        transaction.setReceiveAgentCode(rs.getString("rcv_agcy_code")); 

        transaction.setDeliveryOption(rs.getInt("dlvr_optn_id"));
        transaction.setReceivedDate(DAOUtils.toCalendar(rs.getTimestamp("rcv_datetime")));

        transaction.setSourceSite(rs.getString("src_web_site_bsns_cd"));
       
        //S17 -- Dodd Frank
        transaction.setMgTransactionSessionId(rs.getString("mg_tran_sess_id"));
        transaction.setSubdivRegCntntVerId(rs.getString("subdiv_reg_cntnt_ver_id"));
		transaction.setReceiveAgentRefNbr(rs.getString("rcv_agent_ref_nbr"));
		transaction.setReceiverMTAccountNbrMask(rs.getString("inrcv_cnsmr_acct_mask_nbr"));
		transaction.setReceiverRegistrationNumber(rs.getString("rcvr_rgstn_id"));
        return transaction;
    }
    

}
