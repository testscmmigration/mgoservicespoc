/**
 * MGOTransaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

/**
 * 
 * MGOTransaction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/08/18 14:28:30 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MGOTransaction  extends InitiatedTransaction {
    private static final long serialVersionUID = 7514255535981843916L;

    private MGOTransactionProcessing mgoProcessing;

    /**
     * 
     * Creates new instance of MGOTransaction
     */
    public MGOTransaction() {
    }


    /**
     * Gets the mgoProcessing value for this MGOTransaction.
     * 
     * @return mgoProcessing
     */
    public MGOTransactionProcessing getMgoProcessing() {
        return mgoProcessing;
    }


    /**
     * Sets the mgoProcessing value for this MGOTransaction.
     * 
     * @param mgoProcessing
     */
    public void setMgoProcessing(MGOTransactionProcessing mgoProcessing) {
        this.mgoProcessing = mgoProcessing;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MGOTransaction)) return false;
        MGOTransaction other = (MGOTransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.mgoProcessing==null && other.getMgoProcessing()==null) || 
             (this.mgoProcessing!=null &&
              this.mgoProcessing.equals(other.getMgoProcessing())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMgoProcessing() != null) {
            _hashCode += getMgoProcessing().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" MgoProcessing=").append(getMgoProcessing());
        return buffer.toString();
    }
}
