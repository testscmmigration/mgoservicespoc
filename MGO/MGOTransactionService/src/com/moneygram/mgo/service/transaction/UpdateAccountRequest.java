/**
 * UpdateAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;


/**
 * 
 * Update Account Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/09/09 21:46:35 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateAccountRequest  extends UpdateTransactionStatusRequest {
    private static final long serialVersionUID = 3460634206310565385L;

    private long fiAccountId;

    public UpdateAccountRequest() {
    }

    /**
     * Gets the fiAccountId value for this UpdateAccountRequest.
     * 
     * @return fiAccountId
     */
    public long getFiAccountId() {
        return fiAccountId;
    }


    /**
     * Sets the fiAccountId value for this UpdateAccountRequest.
     * 
     * @param fiAccountId
     */
    public void setFiAccountId(long fiAccountId) {
        this.fiAccountId = fiAccountId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateAccountRequest)) return false;
        UpdateAccountRequest other = (UpdateAccountRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.fiAccountId == other.getFiAccountId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getFiAccountId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" FiAccountId="+ getFiAccountId();
    }
}
