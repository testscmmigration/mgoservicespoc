/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.OperationResponse;

/**
 * 
 * SaveTransactionCommentResponse
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2013</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/05/15 15:23:00 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class SaveTransactionCommentResponse implements OperationResponse {
    private long transactionCommentId;

    public SaveTransactionCommentResponse() {
    }

    /**
     * Gets the transactionCommentId value for this SaveTransactionCommentResponse.
     * 
     * @return transactionCommentId
     */
    public long getTransactionCommentId() {
        return transactionCommentId;
    }


    /**
     * Sets the transactionCommentId value for this SaveTransactionCommentResponse.
     * 
     * @param transactionCommentId
     */
    public void setTransactionCommentId(long transactionCommentId) {
        this.transactionCommentId = transactionCommentId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveTransactionCommentResponse)) return false;
        SaveTransactionCommentResponse other = (SaveTransactionCommentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.transactionCommentId == other.getTransactionCommentId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getTransactionCommentId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return super.toString() +" transactionCommentId="+ getTransactionCommentId();
    }
}
