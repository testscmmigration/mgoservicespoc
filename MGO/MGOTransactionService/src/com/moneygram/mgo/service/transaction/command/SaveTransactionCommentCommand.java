/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.transaction.SaveTransactionCommentRequest;
import com.moneygram.mgo.service.transaction.SaveTransactionCommentResponse;
import com.moneygram.mgo.service.transaction.SaveTransactionRequest;
import com.moneygram.mgo.service.transaction.SaveTransactionResponse;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * Save Transaction Comment Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.0 $ $Date: 2013/05/15 15:23:00 $ </td><tr><td>
 * @author   </td><td>$Author: vl58 $ </td>
 *</table>
 *</div>
 */
public class SaveTransactionCommentCommand extends TransactionalCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(SaveTransactionCommentCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        SaveTransactionCommentRequest commandRequest = (SaveTransactionCommentRequest) request;

        //process save request
        Number transactionCommentId = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            transactionCommentId = dao.saveTransactionComment(commandRequest.getTransactionId(),commandRequest.getCommentReasoncode(),commandRequest.getCommentText());
        } catch (Exception e) {
            throw new CommandException("Failed to save transaction comment", e);
        }
        
        SaveTransactionCommentResponse response = new SaveTransactionCommentResponse();
        response.setTransactionCommentId(transactionCommentId.longValue());
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof SaveTransactionCommentRequest;
    }

}
