/**
 * InitiatedTransaction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;


/**
 *
 * Initiated Transaction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.13 $ $Date: 2012/04/11 20:45:55 $ </td><tr><td>
 * @author   </td><td>$Author: w162 $ </td>
 *</table>
 *</div>
 */
public class InitiatedTransaction  implements java.io.Serializable {

    private static final long serialVersionUID = 3087959527081442681L;

    private Sender sender;

    private Receiver receiver;

    private TransactionAmount transactionAmount;

    private MGOProductType productType;

    private long sendAgentID;

    private java.lang.String sendCountry;

    private java.lang.String sendCurrency;

    private java.lang.String receiveCountry;

    private java.lang.String receiveState;

    private java.lang.String receiveCurrency;

    private java.lang.Long intendedReceiveAgent;

    private java.lang.Long receiveAgent;

    private java.lang.String receiveAgentName;

    private java.lang.String receiveAgentCode;

    private java.lang.String receiveAgentReferenceNumber;

    private int deliveryOption;

    private java.lang.String messageField1;

    private java.lang.String messageField2;

    private java.lang.Boolean internetPurchaseFlag;

    private java.lang.String receiverRegistrationNumber;

    private java.lang.String testQuestion;

    private java.lang.String testAnswer;

    private java.lang.Integer authenticationScore;

    private java.lang.String status;

    private java.lang.String subStatus;

    private long addressId;

    private java.lang.String confirmationNumber;

    private java.lang.String comment;

    private java.lang.String billerInfoText;

    private java.lang.String billerPostTimeframeText;

    private java.lang.String billerEndOfDayTimeText;

    private java.lang.String sourceSite;

    // Telecheck parameters
    private java.lang.Integer providerCode;
    private java.lang.String providerTransactionNumber;
    private java.lang.String internalTransactionNumber;
    // end

  //S29a
    // Three minute phone call parameters
    private java.lang.String threeMinuteFreePhoneNumber;
    private java.lang.String threeMinuteFreePinNumber;
    // END

    // Start: S17 -- dodd Frank
	private java.lang.String mgTransactionSessionId;
	private java.lang.String subdivRegCntntVerId;
	private java.util.Calendar inrcvAvlToRcvLclDate;
	private java.lang.String tranDsclsrTextEng;
	private java.lang.String tranDsclsrTextSpa;
    // End: S17 -- dodd Frank
	
	//Begin, vl58 - S27, new fields for Release 36
	private java.lang.String inrcvNonMgiFeeEstmFlag;
	private java.lang.String inrcvNonMgiTaxEstmFlag;
	private java.lang.String inrcvPayoutCrncyCode;
	private java.lang.String inrcvIndCntryFlag;
	//End, vl58 - S27, new fields for Release 36
	
	//Begin, vx15 - 5244, new fields for Release 36
	private java.lang.String preferredLanguage;
	
	//singleMessaging - MGO-5000
	private java.lang.String receiptTextInfoEng;
	private java.lang.String receiptTextInfoSpa;

    private java.lang.String mccPartnerProfileId;

    public InitiatedTransaction() {
    }

	public InitiatedTransaction(Sender sender,
			Receiver receiver,
			TransactionAmount transactionAmount,
			MGOProductType productType, long sendAgentID,
			java.lang.String sendCountry, java.lang.String sendCurrency,
			java.lang.String receiveCountry, java.lang.String receiveState,
			java.lang.String receiveCurrency, java.lang.Long intendedReceiveAgent,
			java.lang.Long receiveAgent, java.lang.String receiveAgentName,
			java.lang.String receiveAgentCode, java.lang.String receiveAgentReferenceNumber,
			int deliveryOption, java.lang.String messageField1, java.lang.String messageField2,
			java.lang.Boolean internetPurchaseFlag, java.lang.String receiverRegistrationNumber,
			java.lang.String testQuestion, java.lang.String testAnswer,
			java.lang.Integer authenticationScore, java.lang.String status,
			java.lang.String subStatus, long addressId, java.lang.String confirmationNumber,
			java.lang.String comment, java.lang.String billerInfoText,
			java.lang.String billerPostTimeframeText, java.lang.String billerEndOfDayTimeText,
			java.lang.String sourceSite) {
		this(sender, receiver, transactionAmount, productType, sendAgentID, sendCountry,
				sendCurrency, receiveCountry, receiveState, receiveCurrency, intendedReceiveAgent,
				receiveAgent, receiveAgentName, receiveAgentCode, receiveAgentReferenceNumber,
				deliveryOption, messageField1, messageField2, internetPurchaseFlag,
				receiverRegistrationNumber, testQuestion, testAnswer, authenticationScore, status,
				subStatus, addressId, confirmationNumber, comment, billerInfoText,
				billerPostTimeframeText, billerEndOfDayTimeText, sourceSite, null, null, null,
				//s29A
				null,
				null ,
				//S17 -- Dodd Frank
				null,null,null,null,null,
				//vl58 - S27, new fields for Release 36
				null,null,null,null,
				//MGO-5000 single messging
				null,null, null);
	}

	public InitiatedTransaction(Sender sender,
			Receiver receiver,
			TransactionAmount transactionAmount,
			MGOProductType productType, long sendAgentID,
			java.lang.String sendCountry, java.lang.String sendCurrency,
			java.lang.String receiveCountry, java.lang.String receiveState,
			java.lang.String receiveCurrency, java.lang.Long intendedReceiveAgent,
			java.lang.Long receiveAgent, java.lang.String receiveAgentName,
			java.lang.String receiveAgentCode, java.lang.String receiveAgentReferenceNumber,
			int deliveryOption, java.lang.String messageField1, java.lang.String messageField2,
			java.lang.Boolean internetPurchaseFlag, java.lang.String receiverRegistrationNumber,
			java.lang.String testQuestion, java.lang.String testAnswer,
			java.lang.Integer authenticationScore, java.lang.String status,
			java.lang.String subStatus, long addressId, java.lang.String confirmationNumber,
			java.lang.String comment, java.lang.String billerInfoText,
			java.lang.String billerPostTimeframeText, java.lang.String billerEndOfDayTimeText,
			java.lang.String sourceSite, java.lang.Integer providerCode,
			java.lang.String providerTransactionNumber, java.lang.String internalTransactionNumber
			//s29A
			,java.lang.String threeMinuteFreePhoneNumber
			,java.lang.String threeMinuteFreePinNumber,
			//S17 -- Dodd Frank
			java.lang.String mgTransactionSessionId,
			java.lang.String subdivRegCntntVerId,
			java.util.Calendar inrcvAvlToRcvLclDate,
			java.lang.String tranDsclsrTextEng,
			java.lang.String tranDsclsrTextSpa,
			//vl58 - S27, new fields for Release 36
			java.lang.String inrcvNonMgiFeeEstmFlag,
			java.lang.String inrcvNonMgiTaxEstmFlag,
			java.lang.String inrcvPayoutCrncyCode,
			java.lang.String inrcvIndCntryFlag,
			//MGO-5000 Single messaging
			java.lang.String receiptTextInfoEng,
			java.lang.String receiptTextInfoSpa,
			java.lang.String mccPartnerProfileId) {
		this.sender = sender;
		this.receiver = receiver;
		this.transactionAmount = transactionAmount;
		this.productType = productType;
		this.sendAgentID = sendAgentID;
		this.sendCountry = sendCountry;
		this.sendCurrency = sendCurrency;
		this.receiveCountry = receiveCountry;
		this.receiveState = receiveState;
		this.receiveCurrency = receiveCurrency;
		this.intendedReceiveAgent = intendedReceiveAgent;
		this.receiveAgent = receiveAgent;
		this.receiveAgentName = receiveAgentName;
		this.receiveAgentCode = receiveAgentCode;
		this.receiveAgentReferenceNumber = receiveAgentReferenceNumber;
		this.deliveryOption = deliveryOption;
		this.messageField1 = messageField1;
		this.messageField2 = messageField2;
		this.internetPurchaseFlag = internetPurchaseFlag;
		this.receiverRegistrationNumber = receiverRegistrationNumber;
		this.testQuestion = testQuestion;
		this.testAnswer = testAnswer;
		this.authenticationScore = authenticationScore;
		this.status = status;
		this.subStatus = subStatus;
		this.addressId = addressId;
		this.confirmationNumber = confirmationNumber;
		this.comment = comment;
		this.billerInfoText = billerInfoText;
		this.billerPostTimeframeText = billerPostTimeframeText;
		this.billerEndOfDayTimeText = billerEndOfDayTimeText;
		this.sourceSite = sourceSite;
		this.providerCode = providerCode;
		this.providerTransactionNumber = providerTransactionNumber;
		this.internalTransactionNumber = internalTransactionNumber;
		//s29A
		this.threeMinuteFreePhoneNumber = threeMinuteFreePhoneNumber;
		this.threeMinuteFreePinNumber = threeMinuteFreePinNumber;
		//S17 -- Dodd Frank
		this.mgTransactionSessionId = mgTransactionSessionId;
		this.subdivRegCntntVerId = subdivRegCntntVerId;
		this.inrcvAvlToRcvLclDate = inrcvAvlToRcvLclDate;
		this.tranDsclsrTextEng = tranDsclsrTextEng;
		this.tranDsclsrTextSpa = tranDsclsrTextSpa;
		//vl58 - S27, new fields for Release 36
		this.inrcvNonMgiFeeEstmFlag = inrcvNonMgiFeeEstmFlag;
		this.inrcvNonMgiTaxEstmFlag = inrcvNonMgiTaxEstmFlag;
		this.inrcvPayoutCrncyCode = inrcvPayoutCrncyCode;
		this.inrcvIndCntryFlag = inrcvIndCntryFlag;
		//MGO-5000 Single messaging
		this.receiptTextInfoEng = receiptTextInfoEng;
		this.receiptTextInfoSpa = receiptTextInfoSpa;
	    this.mccPartnerProfileId = mccPartnerProfileId;
	}

     /**
     * Gets the sender value for this InitiatedTransaction.
     *
     * @return sender
     */
    public Sender getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this InitiatedTransaction.
     *
     * @param sender
     */
    public void setSender(Sender sender) {
        this.sender = sender;
    }


    /**
     * Gets the receiver value for this InitiatedTransaction.
     *
     * @return receiver
     */
    public Receiver getReceiver() {
        return receiver;
    }


    /**
     * Sets the receiver value for this InitiatedTransaction.
     *
     * @param receiver
     */
    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }


    /**
     * Gets the transactionAmount value for this InitiatedTransaction.
     *
     * @return transactionAmount
     */
    public TransactionAmount getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this InitiatedTransaction.
     *
     * @param transactionAmount
     */
    public void setTransactionAmount(TransactionAmount transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the productType value for this InitiatedTransaction.
     *
     * @return productType
     */
    public MGOProductType getProductType() {
        return productType;
    }


    /**
     * Sets the productType value for this InitiatedTransaction.
     *
     * @param productType
     */
    public void setProductType(MGOProductType productType) {
        this.productType = productType;
    }


    /**
     * Gets the sendAgentID value for this InitiatedTransaction.
     *
     * @return sendAgentID
     */
    public long getSendAgentID() {
        return sendAgentID;
    }


    /**
     * Sets the sendAgentID value for this InitiatedTransaction.
     *
     * @param sendAgentID
     */
    public void setSendAgentID(long sendAgentID) {
        this.sendAgentID = sendAgentID;
    }


    /**
     * Gets the sendCountry value for this InitiatedTransaction.
     *
     * @return sendCountry
     */
    public java.lang.String getSendCountry() {
        return sendCountry;
    }


    /**
     * Sets the sendCountry value for this InitiatedTransaction.
     *
     * @param sendCountry
     */
    public void setSendCountry(java.lang.String sendCountry) {
        this.sendCountry = sendCountry;
    }


    /**
     * Gets the sendCurrency value for this InitiatedTransaction.
     *
     * @return sendCurrency
     */
    public java.lang.String getSendCurrency() {
        return sendCurrency;
    }


    /**
     * Sets the sendCurrency value for this InitiatedTransaction.
     *
     * @param sendCurrency
     */
    public void setSendCurrency(java.lang.String sendCurrency) {
        this.sendCurrency = sendCurrency;
    }


    /**
     * Gets the receiveCountry value for this InitiatedTransaction.
     *
     * @return receiveCountry
     */
    public java.lang.String getReceiveCountry() {
        return receiveCountry;
    }


    /**
     * Sets the receiveCountry value for this InitiatedTransaction.
     *
     * @param receiveCountry
     */
    public void setReceiveCountry(java.lang.String receiveCountry) {
        this.receiveCountry = receiveCountry;
    }


    /**
     * Gets the receiveState value for this InitiatedTransaction.
     *
     * @return receiveState
     */
    public java.lang.String getReceiveState() {
        return receiveState;
    }


    /**
     * Sets the receiveState value for this InitiatedTransaction.
     *
     * @param receiveState
     */
    public void setReceiveState(java.lang.String receiveState) {
        this.receiveState = receiveState;
    }


    /**
     * Gets the receiveCurrency value for this InitiatedTransaction.
     *
     * @return receiveCurrency
     */
    public java.lang.String getReceiveCurrency() {
        return receiveCurrency;
    }


    /**
     * Sets the receiveCurrency value for this InitiatedTransaction.
     *
     * @param receiveCurrency
     */
    public void setReceiveCurrency(java.lang.String receiveCurrency) {
        this.receiveCurrency = receiveCurrency;
    }


    /**
     * Gets the intendedReceiveAgent value for this InitiatedTransaction.
     *
     * @return intendedReceiveAgent
     */
    public java.lang.Long getIntendedReceiveAgent() {
        return intendedReceiveAgent;
    }


    /**
     * Sets the intendedReceiveAgent value for this InitiatedTransaction.
     *
     * @param intendedReceiveAgent
     */
    public void setIntendedReceiveAgent(java.lang.Long intendedReceiveAgent) {
        this.intendedReceiveAgent = intendedReceiveAgent;
    }


    /**
     * Gets the receiveAgent value for this InitiatedTransaction.
     *
     * @return receiveAgent
     */
    public java.lang.Long getReceiveAgent() {
        return receiveAgent;
    }


    /**
     * Sets the receiveAgent value for this InitiatedTransaction.
     *
     * @param receiveAgent
     */
    public void setReceiveAgent(java.lang.Long receiveAgent) {
        this.receiveAgent = receiveAgent;
    }


    /**
     * Gets the receiveAgentName value for this InitiatedTransaction.
     *
     * @return receiveAgentName
     */
    public java.lang.String getReceiveAgentName() {
        return receiveAgentName;
    }


    /**
     * Sets the receiveAgentName value for this InitiatedTransaction.
     *
     * @param receiveAgentName
     */
    public void setReceiveAgentName(java.lang.String receiveAgentName) {
        this.receiveAgentName = receiveAgentName;
    }


    /**
     * Gets the receiveAgentCode value for this InitiatedTransaction.
     *
     * @return receiveAgentCode
     */
    public java.lang.String getReceiveAgentCode() {
        return receiveAgentCode;
    }


    /**
     * Sets the receiveAgentCode value for this InitiatedTransaction.
     *
     * @param receiveAgentCode
     */
    public void setReceiveAgentCode(java.lang.String receiveAgentCode) {
        this.receiveAgentCode = receiveAgentCode;
    }


    /**
     * Gets the receiveAgentReferenceNumber value for this InitiatedTransaction.
     *
     * @return receiveAgentReferenceNumber
     */
    public java.lang.String getReceiveAgentReferenceNumber() {
        return receiveAgentReferenceNumber;
    }


    /**
     * Sets the receiveAgentReferenceNumber value for this InitiatedTransaction.
     *
     * @param receiveAgentReferenceNumber
     */
    public void setReceiveAgentReferenceNumber(java.lang.String receiveAgentReferenceNumber) {
        this.receiveAgentReferenceNumber = receiveAgentReferenceNumber;
    }


    /**
     * Gets the deliveryOption value for this InitiatedTransaction.
     *
     * @return deliveryOption
     */
    public int getDeliveryOption() {
        return deliveryOption;
    }


    /**
     * Sets the deliveryOption value for this InitiatedTransaction.
     *
     * @param deliveryOption
     */
    public void setDeliveryOption(int deliveryOption) {
        this.deliveryOption = deliveryOption;
    }


    /**
     * Gets the messageField1 value for this InitiatedTransaction.
     *
     * @return messageField1
     */
    public java.lang.String getMessageField1() {
        return messageField1;
    }


    /**
     * Sets the messageField1 value for this InitiatedTransaction.
     *
     * @param messageField1
     */
    public void setMessageField1(java.lang.String messageField1) {
        this.messageField1 = messageField1;
    }


    /**
     * Gets the messageField2 value for this InitiatedTransaction.
     *
     * @return messageField2
     */
    public java.lang.String getMessageField2() {
        return messageField2;
    }


    /**
     * Sets the messageField2 value for this InitiatedTransaction.
     *
     * @param messageField2
     */
    public void setMessageField2(java.lang.String messageField2) {
        this.messageField2 = messageField2;
    }


    /**
     * Gets the internetPurchaseFlag value for this InitiatedTransaction.
     *
     * @return internetPurchaseFlag
     */
    public java.lang.Boolean getInternetPurchaseFlag() {
        return internetPurchaseFlag;
    }


    /**
     * Sets the internetPurchaseFlag value for this InitiatedTransaction.
     *
     * @param internetPurchaseFlag
     */
    public void setInternetPurchaseFlag(java.lang.Boolean internetPurchaseFlag) {
        this.internetPurchaseFlag = internetPurchaseFlag;
    }


    /**
     * Gets the receiverRegistrationNumber value for this InitiatedTransaction.
     *
     * @return receiverRegistrationNumber
     */
    public java.lang.String getReceiverRegistrationNumber() {
        return receiverRegistrationNumber;
    }


    /**
     * Sets the receiverRegistrationNumber value for this InitiatedTransaction.
     *
     * @param receiverRegistrationNumber
     */
    public void setReceiverRegistrationNumber(java.lang.String receiverRegistrationNumber) {
        this.receiverRegistrationNumber = receiverRegistrationNumber;
    }


    /**
     * Gets the testQuestion value for this InitiatedTransaction.
     *
     * @return testQuestion
     */
    public java.lang.String getTestQuestion() {
        return testQuestion;
    }


    /**
     * Sets the testQuestion value for this InitiatedTransaction.
     *
     * @param testQuestion
     */
    public void setTestQuestion(java.lang.String testQuestion) {
        this.testQuestion = testQuestion;
    }


    /**
     * Gets the testAnswer value for this InitiatedTransaction.
     *
     * @return testAnswer
     */
    public java.lang.String getTestAnswer() {
        return testAnswer;
    }


    /**
     * Sets the testAnswer value for this InitiatedTransaction.
     *
     * @param testAnswer
     */
    public void setTestAnswer(java.lang.String testAnswer) {
        this.testAnswer = testAnswer;
    }


    /**
     * Gets the authenticationScore value for this InitiatedTransaction.
     *
     * @return authenticationScore
     */
    public java.lang.Integer getAuthenticationScore() {
        return authenticationScore;
    }


    /**
     * Sets the authenticationScore value for this InitiatedTransaction.
     *
     * @param authenticationScore
     */
    public void setAuthenticationScore(java.lang.Integer authenticationScore) {
        this.authenticationScore = authenticationScore;
    }


    /**
     * Gets the status value for this InitiatedTransaction.
     *
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this InitiatedTransaction.
     *
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the subStatus value for this InitiatedTransaction.
     *
     * @return subStatus
     */
    public java.lang.String getSubStatus() {
        return subStatus;
    }


    /**
     * Sets the subStatus value for this InitiatedTransaction.
     *
     * @param subStatus
     */
    public void setSubStatus(java.lang.String subStatus) {
        this.subStatus = subStatus;
    }


    /**
     * Gets the addressId value for this InitiatedTransaction.
     *
     * @return addressId
     */
    public long getAddressId() {
        return addressId;
    }


    /**
     * Sets the addressId value for this InitiatedTransaction.
     *
     * @param addressId
     */
    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }


    /**
     * Gets the confirmationNumber value for this InitiatedTransaction.
     *
     * @return confirmationNumber
     */
    public java.lang.String getConfirmationNumber() {
        return confirmationNumber;
    }


    /**
     * Sets the confirmationNumber value for this InitiatedTransaction.
     *
     * @param confirmationNumber
     */
    public void setConfirmationNumber(java.lang.String confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }


    /**
     * Gets the comment value for this InitiatedTransaction.
     *
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this InitiatedTransaction.
     *
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }


    /**
     * Gets the billerInfoText value for this InitiatedTransaction.
     *
     * @return billerInfoText
     */
    public java.lang.String getBillerInfoText() {
        return billerInfoText;
    }


    /**
     * Sets the billerInfoText value for this InitiatedTransaction.
     *
     * @param billerInfoText
     */
    public void setBillerInfoText(java.lang.String billerInfoText) {
        this.billerInfoText = billerInfoText;
    }


    /**
     * Gets the billerPostTimeframeText value for this InitiatedTransaction.
     *
     * @return billerPostTimeframeText
     */
    public java.lang.String getBillerPostTimeframeText() {
        return billerPostTimeframeText;
    }


    /**
     * Sets the billerPostTimeframeText value for this InitiatedTransaction.
     *
     * @param billerPostTimeframeText
     */
    public void setBillerPostTimeframeText(java.lang.String billerPostTimeframeText) {
        this.billerPostTimeframeText = billerPostTimeframeText;
    }


    /**
     * Gets the billerEndOfDayTimeText value for this InitiatedTransaction.
     *
     * @return billerEndOfDayTimeText
     */
    public java.lang.String getBillerEndOfDayTimeText() {
        return billerEndOfDayTimeText;
    }


    /**
     * Sets the billerEndOfDayTimeText value for this InitiatedTransaction.
     *
     * @param billerEndOfDayTimeText
     */
    public void setBillerEndOfDayTimeText(java.lang.String billerEndOfDayTimeText) {
        this.billerEndOfDayTimeText = billerEndOfDayTimeText;
    }


    /**
     * Gets the sourceSite value for this InitiatedTransaction.
     *
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this InitiatedTransaction.
     *
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }



    /**
	 * @param mgTransactionSessionId the mgTransactionSessionId to set
	 */
	public void setMgTransactionSessionId(java.lang.String mgTransactionSessionId) {
		this.mgTransactionSessionId = mgTransactionSessionId;
	}

	/**
	 * @return the mgTransactionSessionId
	 */
	public java.lang.String getMgTransactionSessionId() {
		return mgTransactionSessionId;
	}



	/**
	 * @param subdivRegCntntVerId the subdivRegCntntVerId to set
	 */
	public void setSubdivRegCntntVerId(java.lang.String subdivRegCntntVerId) {
		this.subdivRegCntntVerId = subdivRegCntntVerId;
	}

	/**
	 * @return the subdivRegCntntVerId
	 */
	public java.lang.String getSubdivRegCntntVerId() {
		return subdivRegCntntVerId;
	}



	/**
	 * @return the inrcvAvlToRcvLclDate
	 */
	public java.util.Calendar getInrcvAvlToRcvLclDate() {
		return inrcvAvlToRcvLclDate;
	}

	/**
	 * @param inrcvAvlToRcvLclDate the inrcvAvlToRcvLclDate to set
	 */
	public void setInrcvAvlToRcvLclDate(java.util.Calendar inrcvAvlToRcvLclDate) {
		this.inrcvAvlToRcvLclDate = inrcvAvlToRcvLclDate;
	}

	/**
	 * @return the tranDsclsrTextEng
	 */
	public java.lang.String getTranDsclsrTextEng() {
		return tranDsclsrTextEng;
	}

	/**
	 * @param tranDsclsrTextEng the tranDsclsrTextEng to set
	 */
	public void setTranDsclsrTextEng(java.lang.String tranDsclsrTextEng) {
		this.tranDsclsrTextEng = tranDsclsrTextEng;
	}

	/**
	 * @return the tranDsclsrTextSpa
	 */
	public java.lang.String getTranDsclsrTextSpa() {
		return tranDsclsrTextSpa;
	}

	/**
	 * @param tranDsclsrTextSpa the tranDsclsrTextSpa to set
	 */
	public void setTranDsclsrTextSpa(java.lang.String tranDsclsrTextSpa) {
		this.tranDsclsrTextSpa = tranDsclsrTextSpa;
	}
	
	/**
	 * @return the receiptTextInfoEng
	 */
	public java.lang.String getReceiptTextInfoEng() {
		return receiptTextInfoEng;
	}
	
	/**
	 * @param receiptTextInfoEng the receiptTextInfoEng to set
	 */
	public void setReceiptTextInfoEng(java.lang.String receiptTextInfoEng) {
		this.receiptTextInfoEng = receiptTextInfoEng;
	}

	/**
	 * @return the receiptTextInfoSpa
	 */
	public java.lang.String getReceiptTextInfoSpa() {
		return receiptTextInfoSpa;
	}
	
	/**
	 * @param receiptTextInfoSpa the receiptTextInfoSpa to set
	 */
	public void setReceiptTextInfoSpa(java.lang.String receiptTextInfoSpa) {
		this.receiptTextInfoSpa = receiptTextInfoSpa;
	}
	
	
    /**
     * Gets the providerCode value for this InitiatedTransaction.
     *
     * @return providerCode
     */
    public java.lang.Integer getProviderCode() {
		return providerCode;
	}


    /**
     * Sets the providerCode value for this InitiatedTransaction.
     *
     * @param providerCode
     */
	public void setProviderCode(java.lang.Integer providerCode) {
		this.providerCode = providerCode;
	}


    /**
     * Gets the providerTransactionNumber value for this InitiatedTransaction.
     *
     * @return providerTransactionNumber
     */
	public java.lang.String getProviderTransactionNumber() {
		return providerTransactionNumber;
	}


    /**
     * Sets the providerTransactionNumber value for this InitiatedTransaction.
     *
     * @param providerTransactionNumber
     */
	public void setProviderTransactionNumber(java.lang.String providerTransactionNumber) {
		this.providerTransactionNumber = providerTransactionNumber;
	}


    /**
     * Gets the internalTransactionNumber value for this InitiatedTransaction.
     *
     * @return internalTransactionNumber
     */
	public java.lang.String getInternalTransactionNumber() {
		return internalTransactionNumber;
	}


    /**
     * Sets the internalTransactionNumber value for this InitiatedTransaction.
     *
     * @param internalTransactionNumber
     */
	public void setInternalTransactionNumber(java.lang.String internalTransactionNumber) {
		this.internalTransactionNumber = internalTransactionNumber;
	}

//S29A
  //Sprint29A Begin
    /**
     * Gets the threeMinuteFreePhoneNumber value for this InitiatedTransaction.
     *
     * @return threeMinuteFreePhoneNumber
     */
	public java.lang.String getThreeMinuteFreePhoneNumber() {
		return threeMinuteFreePhoneNumber;
	}

    /**
     * Sets the threeMinuteFreePhoneNumber value for this InitiatedTransaction.
     *
     * @param threeMinuteFreePhoneNumber
     */
	public void setThreeMinuteFreePhoneNumber(java.lang.String threeMinuteFreePhoneNumber) {
		this.threeMinuteFreePhoneNumber = threeMinuteFreePhoneNumber;
	}

    /**
     * Gets the threeMinuteFreePinNumber value for this InitiatedTransaction.
     *
     * @return threeMinuteFreePinNumber
     */
	public java.lang.String getThreeMinuteFreePinNumber() {
		return threeMinuteFreePinNumber;
	}

    /**
     * Sets the threeMinuteFreePinNumber value for this InitiatedTransaction.
     *
     * @param threeMinuteFreePinNumber
     */
	public void setThreeMinuteFreePinNumber(java.lang.String threeMinuteFreePinNumber) {
		this.threeMinuteFreePinNumber = threeMinuteFreePinNumber;
	}
  //Sprint29A end

	public void setInrcvNonMgiFeeEstmFlag(java.lang.String inrcvNonMgiFeeEstmFlag) {
		this.inrcvNonMgiFeeEstmFlag = inrcvNonMgiFeeEstmFlag;
	}

	public java.lang.String getInrcvNonMgiFeeEstmFlag() {
		return inrcvNonMgiFeeEstmFlag;
	}

	public void setInrcvNonMgiTaxEstmFlag(java.lang.String inrcvNonMgiTaxEstmFlag) {
		this.inrcvNonMgiTaxEstmFlag = inrcvNonMgiTaxEstmFlag;
	}

	public java.lang.String getInrcvNonMgiTaxEstmFlag() {
		return inrcvNonMgiTaxEstmFlag;
	}

	public void setInrcvPayoutCrncyCode(java.lang.String inrcvPayoutCrncyCode) {
		this.inrcvPayoutCrncyCode = inrcvPayoutCrncyCode;
	}

	public java.lang.String getInrcvPayoutCrncyCode() {
		return inrcvPayoutCrncyCode;
	}

	public void setInrcvIndCntryFlag(java.lang.String inrcvIndCntryFlag) {
		this.inrcvIndCntryFlag = inrcvIndCntryFlag;
	}

	public java.lang.String getInrcvIndCntryFlag() {
		return inrcvIndCntryFlag;
	}
	
	public void setPreferredLanguage(java.lang.String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	
	public java.lang.String getPreferredLanguage() {
		return preferredLanguage;
	}
    public java.lang.String getMccPartnerProfileId() {
        return mccPartnerProfileId;
    }

    public void setMccPartnerProfileId(java.lang.String mccPartnerProfileId) {
        this.mccPartnerProfileId = mccPartnerProfileId;
    }


	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InitiatedTransaction)) return false;
        InitiatedTransaction other = (InitiatedTransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.sender==null && other.getSender()==null) ||
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.receiver==null && other.getReceiver()==null) ||
             (this.receiver!=null &&
              this.receiver.equals(other.getReceiver()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) ||
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.productType==null && other.getProductType()==null) ||
             (this.productType!=null &&
              this.productType.equals(other.getProductType()))) &&
            this.sendAgentID == other.getSendAgentID() &&
            ((this.sendCountry==null && other.getSendCountry()==null) ||
             (this.sendCountry!=null &&
              this.sendCountry.equals(other.getSendCountry()))) &&
            ((this.sendCurrency==null && other.getSendCurrency()==null) ||
             (this.sendCurrency!=null &&
              this.sendCurrency.equals(other.getSendCurrency()))) &&
            ((this.receiveCountry==null && other.getReceiveCountry()==null) ||
             (this.receiveCountry!=null &&
              this.receiveCountry.equals(other.getReceiveCountry()))) &&
            ((this.receiveState==null && other.getReceiveState()==null) ||
             (this.receiveState!=null &&
              this.receiveState.equals(other.getReceiveState()))) &&
            ((this.receiveCurrency==null && other.getReceiveCurrency()==null) ||
             (this.receiveCurrency!=null &&
              this.receiveCurrency.equals(other.getReceiveCurrency()))) &&
            ((this.intendedReceiveAgent==null && other.getIntendedReceiveAgent()==null) ||
             (this.intendedReceiveAgent!=null &&
              this.intendedReceiveAgent.equals(other.getIntendedReceiveAgent()))) &&
            ((this.receiveAgent==null && other.getReceiveAgent()==null) ||
             (this.receiveAgent!=null &&
              this.receiveAgent.equals(other.getReceiveAgent()))) &&
            ((this.receiveAgentName==null && other.getReceiveAgentName()==null) ||
             (this.receiveAgentName!=null &&
              this.receiveAgentName.equals(other.getReceiveAgentName()))) &&
            ((this.receiveAgentCode==null && other.getReceiveAgentCode()==null) ||
             (this.receiveAgentCode!=null &&
              this.receiveAgentCode.equals(other.getReceiveAgentCode()))) &&
            ((this.receiveAgentReferenceNumber==null && other.getReceiveAgentReferenceNumber()==null) ||
             (this.receiveAgentReferenceNumber!=null &&
              this.receiveAgentReferenceNumber.equals(other.getReceiveAgentReferenceNumber()))) &&
            this.deliveryOption == other.getDeliveryOption() &&
            ((this.messageField1==null && other.getMessageField1()==null) ||
             (this.messageField1!=null &&
              this.messageField1.equals(other.getMessageField1()))) &&
            ((this.messageField2==null && other.getMessageField2()==null) ||
             (this.messageField2!=null &&
              this.messageField2.equals(other.getMessageField2()))) &&
            ((this.internetPurchaseFlag==null && other.getInternetPurchaseFlag()==null) ||
             (this.internetPurchaseFlag!=null &&
              this.internetPurchaseFlag.equals(other.getInternetPurchaseFlag()))) &&
            ((this.receiverRegistrationNumber==null && other.getReceiverRegistrationNumber()==null) ||
             (this.receiverRegistrationNumber!=null &&
              this.receiverRegistrationNumber.equals(other.getReceiverRegistrationNumber()))) &&
            ((this.testQuestion==null && other.getTestQuestion()==null) ||
             (this.testQuestion!=null &&
              this.testQuestion.equals(other.getTestQuestion()))) &&
            ((this.testAnswer==null && other.getTestAnswer()==null) ||
             (this.testAnswer!=null &&
              this.testAnswer.equals(other.getTestAnswer()))) &&
            ((this.authenticationScore==null && other.getAuthenticationScore()==null) ||
             (this.authenticationScore!=null &&
              this.authenticationScore.equals(other.getAuthenticationScore()))) &&
            ((this.status==null && other.getStatus()==null) ||
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.subStatus==null && other.getSubStatus()==null) ||
             (this.subStatus!=null &&
              this.subStatus.equals(other.getSubStatus()))) &&
            this.addressId == other.getAddressId() &&
            ((this.confirmationNumber==null && other.getConfirmationNumber()==null) ||
             (this.confirmationNumber!=null &&
              this.confirmationNumber.equals(other.getConfirmationNumber()))) &&
            ((this.comment==null && other.getComment()==null) ||
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.billerInfoText==null && other.getBillerInfoText()==null) ||
             (this.billerInfoText!=null &&
              this.billerInfoText.equals(other.getBillerInfoText()))) &&
            ((this.billerPostTimeframeText==null && other.getBillerPostTimeframeText()==null) ||
             (this.billerPostTimeframeText!=null &&
              this.billerPostTimeframeText.equals(other.getBillerPostTimeframeText()))) &&
            ((this.billerEndOfDayTimeText==null && other.getBillerEndOfDayTimeText()==null) ||
             (this.billerEndOfDayTimeText!=null &&
              this.billerEndOfDayTimeText.equals(other.getBillerEndOfDayTimeText()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) ||
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite()))) &&
            ((this.providerCode==null && other.getProviderCode()==null) ||
             (this.providerCode!=null &&
              this.providerCode.equals(other.getProviderCode()))) &&
            ((this.providerTransactionNumber==null && other.getProviderTransactionNumber()==null) ||
             (this.providerTransactionNumber!=null &&
              this.providerTransactionNumber.equals(other.getProviderTransactionNumber()))) &&
            ((this.internalTransactionNumber==null && other.getInternalTransactionNumber()==null) ||
             (this.internalTransactionNumber!=null &&
              this.internalTransactionNumber.equals(other.getInternalTransactionNumber()))) &&
            //s29a
            ((this.threeMinuteFreePhoneNumber==null && other.getThreeMinuteFreePhoneNumber()==null) ||
             (this.threeMinuteFreePhoneNumber!=null &&
             this.threeMinuteFreePhoneNumber.equals(other.getThreeMinuteFreePhoneNumber()))) &&
            ((this.threeMinuteFreePinNumber==null && other.getThreeMinuteFreePinNumber()==null) ||
             (this.threeMinuteFreePinNumber!=null &&
             this.threeMinuteFreePinNumber.equals(other.getThreeMinuteFreePinNumber()))) &&
            //S17 - Dodd Frank
            ((this.mgTransactionSessionId==null && other.getMgTransactionSessionId()==null) ||
             (this.mgTransactionSessionId!=null &&
              this.mgTransactionSessionId.equals(other.getMgTransactionSessionId()))) &&
            ((this.subdivRegCntntVerId==null && other.getSubdivRegCntntVerId()==null) ||
             (this.subdivRegCntntVerId!=null &&
              this.subdivRegCntntVerId.equals(other.getSubdivRegCntntVerId()))) &&
            ((this.inrcvAvlToRcvLclDate==null && other.getInrcvAvlToRcvLclDate()==null) ||
             (this.inrcvAvlToRcvLclDate!=null &&
              this.inrcvAvlToRcvLclDate.equals(other.getInrcvAvlToRcvLclDate()))) &&
            ((this.tranDsclsrTextEng==null && other.getTranDsclsrTextEng()==null) ||
             (this.tranDsclsrTextEng!=null &&
              this.tranDsclsrTextEng.equals(other.getTranDsclsrTextEng()))) &&
            ((this.tranDsclsrTextSpa==null && other.getTranDsclsrTextSpa()==null) ||
             (this.tranDsclsrTextSpa!=null &&
              this.tranDsclsrTextSpa.equals(other.getTranDsclsrTextSpa()))) &&
            //vl58 - S27, new fields for Release 36
            ((this.inrcvNonMgiFeeEstmFlag==null && other.getInrcvNonMgiFeeEstmFlag()==null) ||
             (this.inrcvNonMgiFeeEstmFlag!=null &&
              this.inrcvNonMgiFeeEstmFlag.equals(other.getInrcvNonMgiFeeEstmFlag()))) &&
            ((this.inrcvNonMgiTaxEstmFlag==null && other.getInrcvNonMgiTaxEstmFlag()==null) ||
             (this.inrcvNonMgiTaxEstmFlag!=null &&
              this.inrcvNonMgiTaxEstmFlag.equals(other.getInrcvNonMgiTaxEstmFlag()))) &&
            ((this.inrcvPayoutCrncyCode==null && other.getInrcvPayoutCrncyCode()==null) ||
             (this.inrcvPayoutCrncyCode!=null &&
              this.inrcvPayoutCrncyCode.equals(other.getInrcvPayoutCrncyCode()))) &&
            ((this.inrcvIndCntryFlag==null && other.getInrcvIndCntryFlag()==null) ||
             (this.inrcvIndCntryFlag!=null &&
              this.inrcvIndCntryFlag.equals(other.getInrcvIndCntryFlag()))) &&
              //MGO-5000 single messaging
            ((this.receiptTextInfoEng==null && other.getReceiptTextInfoEng()==null) ||
             (this.receiptTextInfoEng!=null &&
              this.receiptTextInfoEng.equals(other.getReceiptTextInfoEng()))) &&
            ((this.receiptTextInfoSpa==null && other.getReceiptTextInfoSpa()==null) ||
             (this.receiptTextInfoSpa!=null &&
              this.receiptTextInfoSpa.equals(other.getReceiptTextInfoSpa()))) &&
             ((this.mccPartnerProfileId==null && other.getMccPartnerProfileId()==null) ||
             (this.mccPartnerProfileId!=null &&
              this.mccPartnerProfileId.equals(other.getMccPartnerProfileId())));

        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getReceiver() != null) {
            _hashCode += getReceiver().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getProductType() != null) {
            _hashCode += getProductType().hashCode();
        }
        _hashCode += new Long(getSendAgentID()).hashCode();
        if (getSendCountry() != null) {
            _hashCode += getSendCountry().hashCode();
        }
        if (getSendCurrency() != null) {
            _hashCode += getSendCurrency().hashCode();
        }
        if (getReceiveCountry() != null) {
            _hashCode += getReceiveCountry().hashCode();
        }
        if (getReceiveState() != null) {
            _hashCode += getReceiveState().hashCode();
        }
        if (getReceiveCurrency() != null) {
            _hashCode += getReceiveCurrency().hashCode();
        }
        if (getIntendedReceiveAgent() != null) {
            _hashCode += getIntendedReceiveAgent().hashCode();
        }
        if (getReceiveAgent() != null) {
            _hashCode += getReceiveAgent().hashCode();
        }
        if (getReceiveAgentName() != null) {
            _hashCode += getReceiveAgentName().hashCode();
        }
        if (getReceiveAgentCode() != null) {
            _hashCode += getReceiveAgentCode().hashCode();
        }
        if (getReceiveAgentReferenceNumber() != null) {
            _hashCode += getReceiveAgentReferenceNumber().hashCode();
        }
        _hashCode += getDeliveryOption();
        if (getMessageField1() != null) {
            _hashCode += getMessageField1().hashCode();
        }
        if (getMessageField2() != null) {
            _hashCode += getMessageField2().hashCode();
        }
        if (getInternetPurchaseFlag() != null) {
            _hashCode += getInternetPurchaseFlag().hashCode();
        }
        if (getReceiverRegistrationNumber() != null) {
            _hashCode += getReceiverRegistrationNumber().hashCode();
        }
        if (getTestQuestion() != null) {
            _hashCode += getTestQuestion().hashCode();
        }
        if (getTestAnswer() != null) {
            _hashCode += getTestAnswer().hashCode();
        }
        if (getAuthenticationScore() != null) {
            _hashCode += getAuthenticationScore().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getSubStatus() != null) {
            _hashCode += getSubStatus().hashCode();
        }
        _hashCode += new Long(getAddressId()).hashCode();
        if (getConfirmationNumber() != null) {
            _hashCode += getConfirmationNumber().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getBillerInfoText() != null) {
            _hashCode += getBillerInfoText().hashCode();
        }
        if (getBillerPostTimeframeText() != null) {
            _hashCode += getBillerPostTimeframeText().hashCode();
        }
        if (getBillerEndOfDayTimeText() != null) {
            _hashCode += getBillerEndOfDayTimeText().hashCode();
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        if (getProviderCode() != null) {
            _hashCode += getProviderCode().hashCode();
        }
        if (getProviderTransactionNumber() != null) {
            _hashCode += getProviderTransactionNumber().hashCode();
        }
        if (getInternalTransactionNumber() != null) {
            _hashCode += getInternalTransactionNumber().hashCode();
        }
//        s29a
        if (getThreeMinuteFreePhoneNumber() != null) {
            _hashCode += getThreeMinuteFreePhoneNumber().hashCode();
        }
        if (getThreeMinuteFreePinNumber() != null) {
            _hashCode += getThreeMinuteFreePinNumber().hashCode();
        }
        //S17 -- Dodd Frank
        if(getMgTransactionSessionId() != null){
        	_hashCode += getMgTransactionSessionId().hashCode();
        }
        if(getSubdivRegCntntVerId() != null){
        	_hashCode += getSubdivRegCntntVerId().hashCode();
        }
        if(getInrcvAvlToRcvLclDate() != null){
        	_hashCode += getInrcvAvlToRcvLclDate().hashCode();
        }
        if(getTranDsclsrTextEng() != null){
        	_hashCode += getTranDsclsrTextEng().hashCode();
        }
        if(getTranDsclsrTextSpa() != null){
        	_hashCode += getTranDsclsrTextSpa().hashCode();
        }
        
        //vl58 - S27, new fields for Release 36
        if(getInrcvNonMgiFeeEstmFlag() != null){
        	_hashCode += getInrcvNonMgiFeeEstmFlag().hashCode();
        }
        if(getInrcvNonMgiTaxEstmFlag() != null){
        	_hashCode += getInrcvNonMgiTaxEstmFlag().hashCode();
        }
        if(getInrcvPayoutCrncyCode() != null){
        	_hashCode += getInrcvPayoutCrncyCode().hashCode();
        }
        if(getInrcvIndCntryFlag() != null){
        	_hashCode += getInrcvIndCntryFlag().hashCode();
        }
        
        //MGO-5000 single messaging
        if(getReceiptTextInfoEng() != null){
        	_hashCode += getReceiptTextInfoEng().hashCode();
        }
        if(getReceiptTextInfoSpa() != null){
        	_hashCode += getReceiptTextInfoSpa().hashCode();
        }
        if(getMccPartnerProfileId() != null){
        	_hashCode += getMccPartnerProfileId().hashCode();
        }
        
        __hashCodeCalc = false;
        return _hashCode;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Sender=").append(getSender());
        buffer.append(" Receiver=").append(getReceiver());
        buffer.append(" TransactionAmount=").append(getTransactionAmount());
        buffer.append(" ProductType=").append(getProductType());
        buffer.append(" SendAgentID=").append(getSendAgentID());
        buffer.append(" SendCountry=").append(getSendCountry());
        buffer.append(" SendCurrency=").append(getSendCurrency());
        buffer.append(" ReceiveCountry=").append(getReceiveCountry());
        buffer.append(" ReceiveState=").append(getReceiveState());
        buffer.append(" ReceiveCurrency=").append(getReceiveCurrency());
        buffer.append(" IntendedReceiveAgent=").append(getIntendedReceiveAgent());
        buffer.append(" ReceiveAgent=").append(getReceiveAgent());
        buffer.append(" ReceiveAgentName=").append(getReceiveAgentName());
        buffer.append(" ReceiveAgentCode=").append(getReceiveAgentCode());
        buffer.append(" ReceiveAgentReferenceNumber=").append(getReceiveAgentReferenceNumber());
        buffer.append(" DeliveryOption=").append(getDeliveryOption());
        buffer.append(" MessageField1=").append(getMessageField1());
        buffer.append(" MessageField2=").append(getMessageField2());
        buffer.append(" InternetPurchaseFlag=").append(getInternetPurchaseFlag());
        buffer.append(" ReceiverRegistrationNumber=").append(getReceiverRegistrationNumber());
        buffer.append(" TestQuestion=").append(getTestQuestion());
        buffer.append(" TestAnswer=").append(getTestAnswer());
        buffer.append(" AuthenticationScore=").append(getAuthenticationScore());
        buffer.append(" Status=").append(getStatus());
        buffer.append(" SubStatus=").append(getSubStatus());
        buffer.append(" AddressId=").append(getAddressId());
        buffer.append(" ConfirmationNumber=").append(getConfirmationNumber());
        buffer.append(" Comment=").append(getComment());
        buffer.append(" BillerInfoText=").append(getBillerInfoText());
        buffer.append(" BillerPostTimeframeText=").append(getBillerPostTimeframeText());
        buffer.append(" BillerEndOfDayTimeText=").append(getBillerEndOfDayTimeText());
        buffer.append(" SourceSite=").append(getSourceSite());
        buffer.append(" ProviderCode=").append(getProviderCode());
        buffer.append(" ProviderTransactionNumber=").append(getProviderTransactionNumber());
        buffer.append(" InternalTransactionNumber=").append(getInternalTransactionNumber());
        //s29a
        buffer.append(" ThreeMinuteFreePhoneNumber=").append(getThreeMinuteFreePhoneNumber());
        buffer.append(" ThreeMinuteFreePinNumber=").append(getThreeMinuteFreePinNumber());
        //S17 -- Dodd Frank
        buffer.append(" mgTransactionSessionId=").append(getMgTransactionSessionId());
        buffer.append(" subdivRegCntntVerId=").append(getSubdivRegCntntVerId());
        buffer.append(" InrcvAvlToRcvLclDate=").append(getInrcvAvlToRcvLclDate());
        buffer.append(" TranDsclsrTextEng=").append(getTranDsclsrTextEng());
        buffer.append(" TranDsclsrTextSpa=").append(getTranDsclsrTextSpa());
        //vl58 - S27, new fields for Release 36
        buffer.append(" InrcvNonMgiFeeEstmFlag=").append(getInrcvNonMgiFeeEstmFlag());
        buffer.append(" InrcvNonMgiTaxEstmFlag=").append(getInrcvNonMgiTaxEstmFlag());
        buffer.append(" InrcvPayoutCrncyCode=").append(getInrcvPayoutCrncyCode());
        buffer.append(" InrcvIndCntryFlag=").append(getInrcvIndCntryFlag());
        
        //vx15 r36 mgo-5244
        buffer.append(" referredLanguage=").append(getPreferredLanguage());
        
        //MGO-5000 single messaging
        buffer.append(" ReceiptTextInfoEng=").append(getReceiptTextInfoEng());
        buffer.append(" ReceiptTextInfoSpa=").append(getReceiptTextInfoSpa());
        buffer.append(" MccPartnerProfileId=").append(getMccPartnerProfileId());
        
        return buffer.toString();
    }



}
