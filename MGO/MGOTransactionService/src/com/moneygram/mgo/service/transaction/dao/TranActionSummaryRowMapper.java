/*
 * Created on Dec 10, 2007
 *
 */
package com.moneygram.mgo.service.transaction.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.transaction.MGOProductType;
import com.moneygram.mgo.service.transaction.TranActionSummary;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * Transaction Summary Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.9 $ $Date: 2012/01/30 17:38:45 $ </td><tr><td>
 * @author   </td><td>$Author: vf69 $ </td>
 *</table>
 *</div>
 */
public class TranActionSummaryRowMapper extends BaseRowMapper {

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        /*
        emg_tran_id        number(9)       out
        emg_tran_type_code     varchar2(8)     out
        lgcy_ref_nbr       varchar2(8)     out
        snd_tran_date      date            out
        tran_stat_code     char(3)         out
        tran_sub_stat_code     char(3)         out
        tran_stat_date     date            out
        snd_iso_crncy_id       varchar2(3)     out
        snd_face_amt       number(15,3)        out
        rcv_iso_cntry_code     varchar2(3)     out
        rcv_cust_frst_name     varchar2(40)        out
        rcv_cust_last_name     varchar2(40)        out
        rcv_cust_matrnl_name   varchar2(40)        out
        rcv_cust_mid_name      varchar2(40)        out
        intnd_dest_stateprovince_code varchar2(2)         out
        rcv_agent_id       number(8)       out
        rcv_agent_name     varchar2(40)        out
        rcv_agcy_code      varchar2(4)     out
            */

        TranActionSummary tranAction = new TranActionSummary();
        tranAction.setTranConfId(rs.getString("tran_conf_id")); 
        tranAction.setTranReasonCode(rs.getString("tran_reas_code"));
        return tranAction;
    }
    

}
