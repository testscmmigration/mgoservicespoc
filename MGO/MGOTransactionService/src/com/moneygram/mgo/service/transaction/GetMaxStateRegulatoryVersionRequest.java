/**
 * GetMaxStateRegulatoryVersionRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

public class GetMaxStateRegulatoryVersionRequest  extends BaseOperationRequest {
    private java.lang.String isoSubDivCode;

    public GetMaxStateRegulatoryVersionRequest() {
    }

    public GetMaxStateRegulatoryVersionRequest(
    		RequestHeader header,
           java.lang.String isoSubDivCode) {
        super(
            header);
        this.isoSubDivCode = isoSubDivCode;
    }


    /**
     * Gets the isoSubDivCode value for this GetMaxStateRegulatoryVersionRequest.
     * 
     * @return isoSubDivCode
     */
    public java.lang.String getIsoSubDivCode() {
        return isoSubDivCode;
    }


    /**
     * Sets the isoSubDivCode value for this GetMaxStateRegulatoryVersionRequest.
     * 
     * @param isoSubDivCode
     */
    public void setIsoSubDivCode(java.lang.String isoSubDivCode) {
        this.isoSubDivCode = isoSubDivCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMaxStateRegulatoryVersionRequest)) return false;
        GetMaxStateRegulatoryVersionRequest other = (GetMaxStateRegulatoryVersionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.isoSubDivCode==null && other.getIsoSubDivCode()==null) || 
             (this.isoSubDivCode!=null &&
              this.isoSubDivCode.equals(other.getIsoSubDivCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getIsoSubDivCode() != null) {
            _hashCode += getIsoSubDivCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMaxStateRegulatoryVersionRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "GetMaxStateRegulatoryVersionRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoSubDivCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "isoSubDivCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
