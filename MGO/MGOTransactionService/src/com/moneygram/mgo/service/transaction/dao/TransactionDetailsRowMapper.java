/*
 * Created on Jul 22, 2009
 *
 */
package com.moneygram.mgo.service.transaction.dao;

import java.sql.ResultSet;
import java.sql.SQLException;




import com.moneygram.common.dao.DAOUtils;

import com.moneygram.mgo.service.transaction.MGOTransaction;
import com.moneygram.mgo.service.transaction.MGOTransactionProcessing;
import com.moneygram.mgo.service.transaction.PhotoId;
import com.moneygram.mgo.service.transaction.PhotoIdType;
import com.moneygram.mgo.service.transaction.Receiver;
import com.moneygram.mgo.service.transaction.Sender;
import com.moneygram.mgo.service.transaction.TransactionAmount;
import com.moneygram.mgo.shared.ReceiverConsumerAddress;



/**
 *
 * Transaction Details Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.13 $ $Date: 2012/04/11 20:45:55 $ </td><tr><td>
 * @author   </td><td>$Author: w162 $ </td>
 *</table>
 *</div>
 */
public class TransactionDetailsRowMapper extends TransactionRowMapper {

    /**
     *
     * @see com.moneygram.mgo.service.transaction.dao.TransactionRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        MGOTransaction transaction = (MGOTransaction)super.mapRow(rs, rowNum);

        /* set in super
        emg_tran_id        number(9)       out
        emg_tran_type_code     varchar2(8)     out
        lgcy_ref_nbr       varchar2(8)     out
        snd_tran_date      date            out
        tran_stat_code     char(3)         out
        tran_sub_stat_code     char(3)         out
        tran_stat_date     date            out
        snd_iso_crncy_id       varchar2(3)     out
        snd_face_amt       number(15,3)        out
        rcv_iso_cntry_code     varchar2(3)     out
        rcv_cust_frst_name     varchar2(40)        out
        rcv_cust_last_name     varchar2(40)        out
        rcv_cust_matrnl_name   varchar2(40)        out
        rcv_cust_mid_name      varchar2(40)        out
        intnd_dest_stateprovince_code varchar2(2)         out
            */

        /*
            emg_tran_id     number(9)       out
            emg_tran_type_code  varchar2(8)     out
            - snd_cust_acct_id        number(7)       out
            - snd_cust_bkup_acct_id   number(7)       out
            lgcy_ref_nbr        varchar2(8)     out
            snd_tran_date       date            out
            tran_stat_code      char(3)         out
            tran_sub_stat_code  char(3)         out
            tran_stat_date      date            out
            - dlvr_optn_id        number(4)       out
            snd_iso_crncy_id        varchar2(3)     out
            snd_face_amt        number(15,3)        out
            - snd_fee_amt     number(15,3)        out
            - rtn_fee_amt     number(15,3)        out
            - snd_tot_amt     number(15,3)        out
            - snd_fx_cnsmr_rate       number(22,11)       out
            - intnd_rcv_agent_id  number(8)       out
            rcv_iso_cntry_code  varchar2(3)     out
            - rcv_iso_crncy_id        varchar2(3)     out
            rcv_cust_frst_name  varchar2(40)        out
            rcv_cust_last_name  varchar2(40)        out
            ? create_date     date            out
            - rcv_agent_name      varchar2(40)        out
            ? rcv_agcy_acct_encryp_nbr    varchar2(256)       out
            ? rcv_agcy_acct_mask_nbr  varchar2(4)     out
            rcv_cust_matrnl_name    varchar2(40)        out
            rcv_cust_mid_name       varchar2(40)        out
            - rcv_cust_addr_state_name    varchar2(40)        out
            - rcv_cust_addr_line1_text    varchar2(60)        out
            - rcv_cust_addr_line2_text    varchar2(60)        out
            - rcv_cust_addr_line3_text    varchar2(60)        out
            - rcv_cust_addr_city_name varchar2(40)        out
            - rcv_cust_addr_postal_code   varchar2(14)        out
            - rcv_cust_dlvr_instr1_text   varchar2(40)        out
            - rcv_cust_dlvr_instr2_text   varchar2(40)        out
            - rcv_cust_dlvr_instr3_text   varchar2(40)        out
            - rcv_cust_ph_nbr     varchar2(20)        out
            - rcvr_rgstn_id       varchar2(20)        out
            - lylty_pgm_mbshp_id  varchar2(20)        out
            - rcv_cust_addr_iso_cntry_code varchar2(3)          out
            intnd_dest_stateprovince_code varchar2(2)          out
            rcv_date                         emg_receive_transaction.rcv_date%TYPE
            snd_cust_addr_id
         */

        Sender sender = new Sender();
        sender.setFiAccountId(rs.getLong("snd_cust_acct_id"));
        sender.setBackupFIAccountId(rs.getLong("snd_cust_bkup_acct_id"));
        sender.setSenderLoyaltyMemberId(rs.getString("lylty_pgm_mbshp_id"));
        sender.setSenderConsumerId(rs.getLong("snd_cust_id"));
       
        /* Code Changes for Story : MGO 11900 */
        
        PhotoId senderPhotoID = new PhotoId();
        senderPhotoID.setExpiryDate((DAOUtils.toCalendar(rs.getTimestamp("snd_cust_photo_id_exp_date"))));
        senderPhotoID.setIssueCountry(rs.getString("snd_cust_photo_id_cntry_code"));
        senderPhotoID.setIssueState(rs.getString("snd_cust_photo_id_state_code"));
        if(rs.getString("snd_cust_photo_id_type_code")!=null){
        senderPhotoID.setType(PhotoIdType.fromString(rs.getString("snd_cust_photo_id_type_code")));
        }
        senderPhotoID.setNumber(rs.getString("snd_cust_photo_id"));
        sender.setSenderPhotoId(senderPhotoID);
        
        /* End of Code Changes for Story : MGO 11900 */
        
        TransactionAmount transactionAmount = transaction.getTransactionAmount();
        transactionAmount.setSendFee(rs.getBigDecimal("snd_fee_amt"));
        transactionAmount.setReturnFee(rs.getBigDecimal("rtn_fee_amt"));
        transactionAmount.setReturnFee(rs.getBigDecimal("rtn_fee_amt"));
        transactionAmount.setTotalAmount(rs.getBigDecimal("snd_tot_amt"));
        transactionAmount.setExchangeRateApplied(rs.getBigDecimal("snd_fx_cnsmr_rate"));
        transactionAmount.setNonDiscountedFee(rs.getBigDecimal("snd_fee_no_dcnt_aply_amt"));

        ReceiverConsumerAddress receiverAddress = new ReceiverConsumerAddress();
        receiverAddress.setState(rs.getString("rcv_cust_addr_state_name"));
        receiverAddress.setLine1(rs.getString("rcv_cust_addr_line1_text"));
        receiverAddress.setLine2(rs.getString("rcv_cust_addr_line2_text"));
        receiverAddress.setLine3(rs.getString("rcv_cust_addr_line3_text"));
        receiverAddress.setCity(rs.getString("rcv_cust_addr_city_name"));
        receiverAddress.setZipCode(rs.getString("rcv_cust_addr_postal_code"));
        receiverAddress.setDirection1(rs.getString("rcv_cust_dlvr_instr1_text"));
        receiverAddress.setDirection2(rs.getString("rcv_cust_dlvr_instr2_text"));
        receiverAddress.setDirection3(rs.getString("rcv_cust_dlvr_instr3_text"));
        receiverAddress.setCountry(rs.getString("rcv_cust_addr_iso_cntry_code"));

        Receiver receiver = transaction.getReceiver();
        receiver.setReceiverAddress(receiverAddress);
        receiver.setReceiverPhone(rs.getString("rcv_cust_ph_nbr"));
        receiver.setReceiverConsumerAccountNbr(rs.getString("rcv_agcy_acct_mask_nbr"));
        receiver.setReceiverMTAccountNbrMask(rs.getString("inrcv_cnsmr_acct_mask_nbr"));

        MGOTransactionProcessing transactionProcessing = transaction.getMgoProcessing();
        transactionProcessing.setSubmittedDate(DAOUtils.toCalendar(rs.getTimestamp("create_date")));
        transactionProcessing.setReceivedDate(DAOUtils.toCalendar(rs.getTimestamp("rcv_datetime")));
        transactionProcessing.setReceiveAgentConfirmationNumber(rs.getString("rcv_agent_ref_nbr")); //for receipt

        transaction.setSender(sender);
        transaction.setDeliveryOption(rs.getInt("dlvr_optn_id"));
        transaction.setIntendedReceiveAgent(rs.getLong("intnd_rcv_agent_id"));
        transaction.setReceiveCurrency(rs.getString("rcv_iso_crncy_id"));
        transaction.setReceiverRegistrationNumber(rs.getString("rcvr_rgstn_id"));

        transaction.setReceiveAgent(new Long(rs.getLong("rcv_agent_id")));
        if (rs.wasNull()) {
            transaction.setReceiveAgent(null);
        }
        transaction.setReceiveAgentName(rs.getString("rcv_agent_name"));
        transaction.setReceiveAgentCode(rs.getString("rcv_agcy_code"));

        transaction.setMessageField1(rs.getString("snd_msg_1_text")); //for receipt
        transaction.setMessageField2(rs.getString("snd_msg_2_text")); //for receipt
        transaction.setConfirmationNumber(rs.getString("form_free_conf_nbr")); //for receipt
        transaction.setBillerInfoText(rs.getString("biller_info_text")); //for receipt
        transaction.setBillerPostTimeframeText(rs.getString("biller_post_time_frame_text")); //for receipt
        transaction.setBillerEndOfDayTimeText(rs.getString("biller_eod_time_text")); //for receipt
        transaction.setAddressId(rs.getLong("snd_cust_addr_id"));
        //s29a
        transaction.setThreeMinuteFreePhoneNumber(rs.getString("phn_call_toll_free_nbr"));
        transaction.setThreeMinuteFreePinNumber(rs.getString("phn_call_pin_nbr"));
        //Start S18 -- Dodd Frank
        transactionAmount.setInrcvFaceAmt(rs.getBigDecimal("inrcv_face_amt"));
        transactionAmount.setInrcvNonMgiFeeAmt(rs.getBigDecimal("inrcv_non_mgi_fee_amt"));
        transactionAmount.setInrcvNonMgiTaxAmt(rs.getBigDecimal("inrcv_non_mgi_tax_amt"));
        transactionAmount.setInrcvPayoutAmt(rs.getBigDecimal("inrcv_payout_amt"));
        transaction.setInrcvAvlToRcvLclDate(rs.getTimestamp("inrcv_avl_to_rcv_lcl_date") != null ? DAOUtils.toCalendar(rs.getTimestamp("inrcv_avl_to_rcv_lcl_date")): null);
        transaction.setTranDsclsrTextEng(rs.getString("tran_dsclsr_text_eng"));
        transaction.setTranDsclsrTextSpa(rs.getString("tran_dsclsr_text_spa"));
        transaction.setMgTransactionSessionId(rs.getString("mg_tran_sess_id"));
        transaction.setSubdivRegCntntVerId(rs.getString("subdiv_reg_cntnt_ver_id"));
        //End S18 -- Dodd Frank
        //Begin, vl58 - new fields for Release 36
        transaction.setInrcvNonMgiFeeEstmFlag(rs.getString("inrcv_non_mgi_fee_estm_flag"));
        transaction.setInrcvNonMgiTaxEstmFlag(rs.getString("inrcv_non_mgi_tax_estm_flag"));
        transaction.setInrcvPayoutCrncyCode(rs.getString("inrcv_payout_crncy_code"));
        transaction.setInrcvIndCntryFlag(rs.getString("inrcv_ind_cntry_flag"));
        //End, vl58 - new fields for Release 36
        //singleMessaging - MGO-5000
        transaction.setReceiptTextInfoEng(rs.getString("rcpt_txt_info_eng"));
        transaction.setReceiptTextInfoSpa(rs.getString("rcpt_txt_info_spa"));
        transaction.setMccPartnerProfileId(rs.getString("MCC_PTNR_PRFL_ID"));  
        //end
        transaction.setReceiverRegistrationNumber(rs.getString("rcvr_rgstn_id"));
        return transaction;
    }
}
