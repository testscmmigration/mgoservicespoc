/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.mgo.service.transaction;

import java.util.Arrays;

import com.moneygram.common.service.BaseOperationRequest;

/**
 * 
 * Get Transaction Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/07/09 19:59:55 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetTransactionRequest extends BaseOperationRequest {
    private long transactionId;

    private java.lang.String[] responseFilter;

    public GetTransactionRequest() {
    }

    /**
     * Gets the transactionId value for this GetTransactionRequest.
     * 
     * @return transactionId
     */
    public long getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this GetTransactionRequest.
     * 
     * @param transactionId
     */
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the responseFilter value for this GetTransactionRequest.
     * 
     * @return responseFilter
     */
    public java.lang.String[] getResponseFilter() {
        return responseFilter;
    }


    /**
     * Sets the responseFilter value for this GetTransactionRequest.
     * 
     * @param responseFilter
     */
    public void setResponseFilter(java.lang.String[] responseFilter) {
        this.responseFilter = responseFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTransactionRequest)) return false;
        GetTransactionRequest other = (GetTransactionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.transactionId == other.getTransactionId() &&
            ((this.responseFilter==null && other.getResponseFilter()==null) || 
             (this.responseFilter!=null &&
              java.util.Arrays.equals(this.responseFilter, other.getResponseFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getTransactionId()).hashCode();
        if (getResponseFilter() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponseFilter());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponseFilter(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * 
     * @see com.moneygram.common.service.BaseServiceRequest#toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ResponseFilter=").append(getResponseFilter() == null ? null : Arrays.asList(getResponseFilter()));
        buffer.append(" TransactionId=").append(getTransactionId());
        return buffer.toString();
    }
}
