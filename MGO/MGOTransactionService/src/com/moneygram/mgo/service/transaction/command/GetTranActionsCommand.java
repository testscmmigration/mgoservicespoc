/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetTranActionsRequest;
import com.moneygram.mgo.service.transaction.GetTranActionsResponse;
import com.moneygram.mgo.service.transaction.TranActionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get TranActions By tranId Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.9 $ $Date: 2011/09/16 01:03:15 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class GetTranActionsCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetTranActionsCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetTranActionsRequest commandRequest = (GetTranActionsRequest) request;

        List<TranActionSummary> tranActions = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            tranActions = dao.getTranActions(commandRequest.getTransactionId(), commandRequest.getStartDate(), commandRequest.getEndDate());
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve transactions", e);
        }

        GetTranActionsResponse response = new GetTranActionsResponse();
        TranActionSummary[] array = null;
        if (tranActions != null && tranActions.size() > 0) {
            array = (TranActionSummary[])tranActions.toArray(new TranActionSummary[tranActions.size()]); 
        }
        //TranActionSummary[] array1 = new TranActionSummary[1];
        //array1[0]= new TranActionSummary("leena", "Test");
        response.setTranActions(array);
        
        return response;
    }


    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetTranActionsRequest;
    }

}
