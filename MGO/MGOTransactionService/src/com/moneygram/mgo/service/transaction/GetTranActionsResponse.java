/**
 * GetTranActionsResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import java.util.Arrays;

import com.moneygram.common.service.BaseOperationResponse;

public class GetTranActionsResponse  extends BaseOperationResponse{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8909829322766116124L;
	/**
	 * 
	 */

	private TranActionSummary[] tranActions;

    public GetTranActionsResponse() {
    }


    /**
     * Gets the tranActions value for this GetTranActionsResponse.
     * 
     * @return tranActions
     */
    public TranActionSummary[] getTranActions() {
        return tranActions;
    }


    /**
     * Sets the tranActions value for this GetTranActionsResponse.
     * 
     * @param tranActions
     */
    public void setTranActions(TranActionSummary[] tranActions) {
        this.tranActions = tranActions;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetTranActionsResponse)) return false;
        GetTranActionsResponse other = (GetTranActionsResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.tranActions==null && other.getTranActions()==null) || 
             (this.tranActions!=null &&
              java.util.Arrays.equals(this.tranActions, other.getTranActions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTranActions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTranActions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTranActions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }


	@Override
	public String toString() {
		return "GetTranActionsResponse [tranActions="
				+ Arrays.toString(tranActions) + "]";
	}
    
}
