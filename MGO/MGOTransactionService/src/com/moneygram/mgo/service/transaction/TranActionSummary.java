/**
 * TranActionSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

public class TranActionSummary  implements java.io.Serializable {

	private java.lang.String tranReasonCode;

    private java.lang.String tranConfId;

    public TranActionSummary() {
    }

    public TranActionSummary(
           java.lang.String tranReasonCode,
           java.lang.String tranConfId) {
           this.tranReasonCode = tranReasonCode;
           this.tranConfId = tranConfId;
    }


    /**
     * Gets the tranReasonCode value for this TranActionSummary.
     * 
     * @return tranReasonCode
     */
    public java.lang.String getTranReasonCode() {
        return tranReasonCode;
    }


    /**
     * Sets the tranReasonCode value for this TranActionSummary.
     * 
     * @param tranReasonCode
     */
    public void setTranReasonCode(java.lang.String tranReasonCode) {
        this.tranReasonCode = tranReasonCode;
    }


    /**
     * Gets the tranConfId value for this TranActionSummary.
     * 
     * @return tranConfId
     */
    public java.lang.String getTranConfId() {
        return tranConfId;
    }


    /**
     * Sets the tranConfId value for this TranActionSummary.
     * 
     * @param tranConfId
     */
    public void setTranConfId(java.lang.String tranConfId) {
        this.tranConfId = tranConfId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TranActionSummary)) return false;
        TranActionSummary other = (TranActionSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tranReasonCode==null && other.getTranReasonCode()==null) || 
             (this.tranReasonCode!=null &&
              this.tranReasonCode.equals(other.getTranReasonCode()))) &&
            ((this.tranConfId==null && other.getTranConfId()==null) || 
             (this.tranConfId!=null &&
              this.tranConfId.equals(other.getTranConfId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTranReasonCode() != null) {
            _hashCode += getTranReasonCode().hashCode();
        }
        if (getTranConfId() != null) {
            _hashCode += getTranConfId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TranConfId=").append(getTranConfId());
        buffer.append(" TranReasonCode=").append(getTranReasonCode());
        return buffer.toString();
    }

}
