/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.transaction.command;

import java.math.BigDecimal;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsResponse;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsTotalRequest;
import com.moneygram.mgo.service.transaction.GetConsumerTransactionsTotalResponse;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Get Transactions total By Consumer Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/08 19:37:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetConsumerTransactionsTotalCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetConsumerTransactionsTotalCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        GetConsumerTransactionsTotalRequest commandRequest = (GetConsumerTransactionsTotalRequest) request;

        BigDecimal total = null;
        try {
            TransactionDAO dao = (TransactionDAO)getDataAccessObject();
            total = dao.getTransactionsTotal(commandRequest.getConsumerId());
        } catch (Exception e) {
            throw new CommandException("Failed to retrieve transactions total", e);
        }

        GetConsumerTransactionsTotalResponse response = new GetConsumerTransactionsTotalResponse();
        response.setTotalAmount(total);
        
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetConsumerTransactionsTotalRequest;
    }

}
