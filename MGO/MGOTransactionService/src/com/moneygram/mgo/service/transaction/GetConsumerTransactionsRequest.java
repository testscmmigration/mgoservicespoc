/**
 * GetConsumerTransactionsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.transaction;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Get Consumer Transactions Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2011/09/16 01:03:15 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class GetConsumerTransactionsRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 5389556608113380591L;

    private long consumerId;

    private java.util.Calendar startDate;

    private java.util.Calendar endDate;

    private java.lang.Integer maxRecords;

    private java.lang.String receiveAgentCode;

    private MGOProductType[] productTypes;

    private java.lang.String partnerSiteId;

    private java.lang.String receiveCountry;

    private java.lang.Integer deliveryOption;

    private java.lang.Long receiveAgent;

    public GetConsumerTransactionsRequest() {
    }

    public GetConsumerTransactionsRequest(
           RequestHeader header,
           long consumerId,
           java.util.Calendar startDate,
           java.util.Calendar endDate,
           java.lang.Integer maxRecords,
           java.lang.String receiveAgentCode,
           MGOProductType[] productTypes,
           java.lang.String partnerSiteId,
           java.lang.String receiveCountry,
           java.lang.Integer deliveryOption,
           java.lang.Long receiveAgent) {
        super(
            header);
        this.consumerId = consumerId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.maxRecords = maxRecords;
        this.receiveAgentCode = receiveAgentCode;
        this.productTypes = productTypes;
        this.partnerSiteId = partnerSiteId;
        this.receiveCountry = receiveCountry;
        this.deliveryOption = deliveryOption;
        this.receiveAgent = receiveAgent;
    }


    /**
     * Gets the consumerId value for this GetConsumerTransactionsRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this GetConsumerTransactionsRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the startDate value for this GetConsumerTransactionsRequest.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this GetConsumerTransactionsRequest.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this GetConsumerTransactionsRequest.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this GetConsumerTransactionsRequest.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the maxRecords value for this GetConsumerTransactionsRequest.
     * 
     * @return maxRecords
     */
    public java.lang.Integer getMaxRecords() {
        return maxRecords;
    }


    /**
     * Sets the maxRecords value for this GetConsumerTransactionsRequest.
     * 
     * @param maxRecords
     */
    public void setMaxRecords(java.lang.Integer maxRecords) {
        this.maxRecords = maxRecords;
    }


    /**
     * Gets the receiveAgentCode value for this GetConsumerTransactionsRequest.
     * 
     * @return receiveAgentCode
     */
    public java.lang.String getReceiveAgentCode() {
        return receiveAgentCode;
    }


    /**
     * Sets the receiveAgentCode value for this GetConsumerTransactionsRequest.
     * 
     * @param receiveAgentCode
     */
    public void setReceiveAgentCode(java.lang.String receiveAgentCode) {
        this.receiveAgentCode = receiveAgentCode;
    }


    /**
     * Gets the productTypes value for this GetConsumerTransactionsRequest.
     * 
     * @return productTypes
     */
    public MGOProductType[] getProductTypes() {
        return productTypes;
    }


    /**
     * Sets the productTypes value for this GetConsumerTransactionsRequest.
     * 
     * @param productTypes
     */
    public void setProductTypes(MGOProductType[] productTypes) {
        this.productTypes = productTypes;
    }


    /**
     * Gets the partnerSiteId value for this GetConsumerTransactionsRequest.
     * 
     * @return partnerSiteId
     */
    public java.lang.String getPartnerSiteId() {
        return partnerSiteId;
    }


    /**
     * Sets the partnerSiteId value for this GetConsumerTransactionsRequest.
     * 
     * @param partnerSiteId
     */
    public void setPartnerSiteId(java.lang.String partnerSiteId) {
        this.partnerSiteId = partnerSiteId;
    }


    /**
     * Gets the receiveCountry value for this GetConsumerTransactionsRequest.
     * 
     * @return receiveCountry
     */
    public java.lang.String getReceiveCountry() {
        return receiveCountry;
    }


    /**
     * Sets the receiveCountry value for this GetConsumerTransactionsRequest.
     * 
     * @param receiveCountry
     */
    public void setReceiveCountry(java.lang.String receiveCountry) {
        this.receiveCountry = receiveCountry;
    }


    /**
     * Gets the deliveryOption value for this GetConsumerTransactionsRequest.
     * 
     * @return deliveryOption
     */
    public java.lang.Integer getDeliveryOption() {
        return deliveryOption;
    }


    /**
     * Sets the deliveryOption value for this GetConsumerTransactionsRequest.
     * 
     * @param deliveryOption
     */
    public void setDeliveryOption(java.lang.Integer deliveryOption) {
        this.deliveryOption = deliveryOption;
    }


    /**
     * Gets the receiveAgent value for this GetConsumerTransactionsRequest.
     * 
     * @return receiveAgent
     */
    public java.lang.Long getReceiveAgent() {
        return receiveAgent;
    }


    /**
     * Sets the receiveAgent value for this GetConsumerTransactionsRequest.
     * 
     * @param receiveAgent
     */
    public void setReceiveAgent(java.lang.Long receiveAgent) {
        this.receiveAgent = receiveAgent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsumerTransactionsRequest)) return false;
        GetConsumerTransactionsRequest other = (GetConsumerTransactionsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId() &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.maxRecords==null && other.getMaxRecords()==null) || 
             (this.maxRecords!=null &&
              this.maxRecords.equals(other.getMaxRecords()))) &&
            ((this.receiveAgentCode==null && other.getReceiveAgentCode()==null) || 
             (this.receiveAgentCode!=null &&
              this.receiveAgentCode.equals(other.getReceiveAgentCode()))) &&
            ((this.productTypes==null && other.getProductTypes()==null) || 
             (this.productTypes!=null &&
              java.util.Arrays.equals(this.productTypes, other.getProductTypes()))) &&
            ((this.partnerSiteId==null && other.getPartnerSiteId()==null) || 
             (this.partnerSiteId!=null &&
              this.partnerSiteId.equals(other.getPartnerSiteId()))) &&
            ((this.receiveCountry==null && other.getReceiveCountry()==null) || 
             (this.receiveCountry!=null &&
              this.receiveCountry.equals(other.getReceiveCountry()))) &&
            ((this.deliveryOption==null && other.getDeliveryOption()==null) || 
             (this.deliveryOption!=null &&
              this.deliveryOption.equals(other.getDeliveryOption()))) &&
            ((this.receiveAgent==null && other.getReceiveAgent()==null) || 
             (this.receiveAgent!=null &&
              this.receiveAgent.equals(other.getReceiveAgent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getMaxRecords() != null) {
            _hashCode += getMaxRecords().hashCode();
        }
        if (getReceiveAgentCode() != null) {
            _hashCode += getReceiveAgentCode().hashCode();
        }
        if (getProductTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPartnerSiteId() != null) {
            _hashCode += getPartnerSiteId().hashCode();
        }
        if (getReceiveCountry() != null) {
            _hashCode += getReceiveCountry().hashCode();
        }
        if (getDeliveryOption() != null) {
            _hashCode += getDeliveryOption().hashCode();
        }
        if (getReceiveAgent() != null) {
            _hashCode += getReceiveAgent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetConsumerTransactionsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "GetConsumerTransactionsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxRecords");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "maxRecords"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "productTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "MGOProductType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerSiteId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "partnerSiteId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryOption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "deliveryOption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiveAgent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/transaction_v1", "receiveAgent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ConsumerId=").append(getConsumerId());
        buffer.append(" StartDate=").append(getStartDate());
        buffer.append(" EndDate=").append(getEndDate());
        buffer.append(" MaxRecords=").append(getMaxRecords());
        buffer.append(" ReceiveAgentCode=").append(getReceiveAgentCode());
        buffer.append(" ProductTypes=").append(getProductTypes());
        buffer.append(" PartnerSiteId=").append(getPartnerSiteId());
        buffer.append(" ReceiveCountry=").append(getReceiveCountry());
        buffer.append(" DeliveryOption=").append(getDeliveryOption());
        buffer.append(" ReceiveAgent=").append(getReceiveAgent());
        return buffer.toString();
    }
}
