/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.mgo.service.transaction;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;

/**
 * Transaction Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.24 $ $Date: 2012/04/11 20:45:55 $ </td><tr><td>
 * @author   </td><td>$Author: w162 $ </td>
 *</table>
 *</div>
 */
public class TransactionServiceTest extends BaseTransactionServiceTestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(TransactionServiceTest.class);

    /**
     * @throws ServiceException
     *
     */
    public void testSaveTransaction() throws ServiceException {
        saveTransaction("5108 0412 3456 7604"); //MC -> PCI encryption
        saveTransaction("123456789"); //local encryption
    }

    public void testSaveTransactionForBillPay() throws ServiceException {
        InitiatedTransaction transaction = generateInitiatedTransaction();
        transaction.setReceiver(null);
        saveTransaction(transaction, "123456789"); //local encryption
    }

    private SaveTransactionResponse saveTransaction(String account) throws CommandException {
        return saveTransaction(generateInitiatedTransaction(), account);
    }

    private SaveTransactionResponse saveTransaction(InitiatedTransaction transaction, String account) throws CommandException {
        if (transaction.getReceiver() != null) {
            transaction.getReceiver().setReceiverConsumerAccountNbr(account);
        }

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.saveTransaction.getValue());
        SaveTransactionRequest request = new SaveTransactionRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        request.setTransaction(transaction);

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("saveTransaction: response="+ response);
        }

        assertTrue("Expected SaveTransactionResponse", response instanceof SaveTransactionResponse);
        SaveTransactionResponse saveTransactionResponse = (SaveTransactionResponse)response;
        assertTrue("Expected transaction id returned", saveTransactionResponse.getTransactionId() > 0);

        return saveTransactionResponse;
    }

    public void xtestGetTransaction() throws ServiceException {

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.getTransaction.getValue());
        GetTransactionRequest request = new GetTransactionRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        Number transactionId = getTransactionId();
        request.setTransactionId(transactionId.longValue());

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetTransaction: response="+ response);
        }

        assertTrue("Expected GetTransactionResponse", response instanceof GetTransactionResponse);
        GetTransactionResponse transactionResponse = (GetTransactionResponse)response;
        assertNotNull("Expected not-null transaction", transactionResponse.getTransaction());
    }

    public void testGetConsumerTransactions() throws ServiceException {

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.getTransactionsByConsumer.getValue());
        GetConsumerTransactionsRequest request = new GetConsumerTransactionsRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        request.setConsumerId(getConsumerId().longValue());
        MGOProductType[] productTypes = new MGOProductType[]{
                MGOProductType.EPCASH,
                MGOProductType.EPSEND,
                MGOProductType.MGCASH,
                MGOProductType.MGSEND,
                MGOProductType.ESSEND,
                };
        request.setProductTypes(productTypes );

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetConsumerTransactions: response="+ response);
        }

        assertTrue("Expected GetConsumerTransactionsResponse", response instanceof GetConsumerTransactionsResponse);
    }

    public void testGetWAPConsumerTransactions() throws ServiceException {

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.getTransactionsByConsumer.getValue());
        GetConsumerTransactionsRequest request = new GetConsumerTransactionsRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        request.setConsumerId(getConsumerId().longValue());
        MGOProductType[] productTypes = new MGOProductType[]{
                MGOProductType.MGSEND,
                MGOProductType.DSSEND,
                };
        request.setProductTypes(productTypes );

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetConsumerTransactions: response="+ response);
        }

        assertTrue("Expected GetConsumerTransactionsResponse", response instanceof GetConsumerTransactionsResponse);
    }

    public void testUpdateTransactionStatus() throws ServiceException {

        Number transactionId = getTransactionId("SAV");

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.updateTransactionStatus.getValue());
        UpdateTransactionStatusRequest request = new UpdateTransactionStatusRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        request.setTransactionId(transactionId.longValue());
        request.setStatus("SAV");
        request.setSubStatus("NOF");
        request.setAuthenticationScore(new Integer(99));
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testUpdateTransactionStatus: response="+ response);
        }

        assertTrue("Expected UpdateTransactionStatusResponse", response instanceof UpdateTransactionStatusResponse);
    }

    public void testUpdateAccount() throws ServiceException {

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.updateAccount.getValue());
        UpdateAccountRequest request = new UpdateAccountRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        Number transactionId = getTransactionId("SAV");
        request.setTransactionId(transactionId.longValue());
        request.setStatus("SAV");
        request.setSubStatus("NOF");
        //request.setFiAccountId(getConsumerAccountId(getConsumerId()).longValue());
        request.setFiAccountId(getConsumerAccountId().longValue());

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testUpdateAccount: response="+ response);
        }

        assertTrue("Expected UpdateAccountResponse", response instanceof UpdateAccountResponse);
    }

    public void testGetConsumerTransactionsTotal() throws ServiceException {

        ProcessingInstruction processingInstruction = new ProcessingInstruction(ServiceAction.getConsumerTransactionsTotal.getValue());
        GetConsumerTransactionsTotalRequest request = new GetConsumerTransactionsTotalRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        request.setConsumerId(getConsumerId().longValue());

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetConsumerTransactionsTotal: response="+ response);
        }

        assertTrue("Expected GetConsumerTransactionsTotalResponse", response instanceof GetConsumerTransactionsTotalResponse);
//        GetConsumerTransactionsTotalResponse totalResponse = (GetConsumerTransactionsTotalResponse)response;
//        assertNotNull("Expected not null total amount", totalResponse.getTotalAmount());
    }

}
