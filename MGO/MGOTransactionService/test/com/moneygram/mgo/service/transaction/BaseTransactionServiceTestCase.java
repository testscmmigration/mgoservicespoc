package com.moneygram.mgo.service.transaction;


import java.math.BigDecimal;

import org.springframework.context.ApplicationContext;

import com.moneygram.common.dao.SingleObjectRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer.BaseConsumerServiceTestCase;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;
import com.moneygram.mgo.service.transaction.dao.TransactionDetailsRowMapper;
import com.moneygram.mgo.service.transaction.util.MGOTransactionServiceResourceConfig;
import com.moneygram.mgo.shared.ReceiverConsumerAddress;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * Base Consumer Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2010/07/27 14:52:26 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseTransactionServiceTestCase extends BaseConsumerServiceTestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseTransactionServiceTestCase.class);
    
    static {
        //set the pending expiration time for 1 year to get more pending transactions 
        attributes.put(MGOTransactionServiceResourceConfig.PEND_TXN_EXPIRE_TIME_HOURS, String.valueOf(365 * 24));
    }

    /**
     * Returns a test consumer id.
     * @return test consumer id.
     */
    protected Number getConsumerId() {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select SND_CUST_ID from emg_transaction where  rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerId: id="+ id);
        }
        assertNotNull("Expected not-null consumer id", id);
        return id;
    }

    protected Number getConsumerAddressId(Number consumerId) {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select addr_id from customer where cust_id = ?";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new Object[]{consumerId}, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerAddressId: id="+ id);
        }
        assertNotNull("Expected not-null Consumer Address Id", id);
        return id;
    }

    protected Number getConsumerAccountId(Number consumerId) {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select cust_acct_id from cust_account where cust_id = ? and rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new Object[]{consumerId}, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerAccountId: id="+ id);
        }
        assertNotNull("Expected not-null Consumer account Id", id);
        return id;
    }

    protected Number getConsumerAccountId() {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select cust_acct_id from cust_account where rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getConsumerAccountId: id="+ id);
        }
        assertNotNull("Expected not-null Consumer account Id", id);
        return id;
    }

    /**
     * Returns first transaction id for consumer.
     * @param consumerId
     * @return first transaction id
     */
    protected Number getTransactionId(Number consumerId) {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select EMG_TRAN_ID from emg_transaction where SND_CUST_ID = ? and rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new Object[]{consumerId}, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getTransactionId: id="+ id);
        }
        assertNotNull("Expected not-null transaction id", id);
        return id;
    }

    protected Number getTransactionId(String status) {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select EMG_TRAN_ID from emg_transaction where TRAN_STAT_CODE = ? and rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new Object[]{status}, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getTransactionId: id="+ id);
        }
        assertNotNull("Expected not-null transaction id", id);
        return id;
    }

    protected MGOTransaction getTransactionDetails(Number transactionId) {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select * from emg_transaction where EMG_TRAN_ID = ?";
        
        MGOTransaction transaction = (MGOTransaction)dao.getJdbcTemplate().queryForObject(sql, new Object[]{transactionId}, new TransactionDetailsRowMapper());
        if (logger.isDebugEnabled()) {
            logger.debug("getTransactionDetails: transaction="+ transaction);
        }
        assertNotNull("Expected not-null transaction", transaction);
        return transaction;
    }

    /**
     * Returns first transaction id for first consumer.
     * @param consumerId
     * @return first transaction id
     */
    protected Number getTransactionId() {
        TransactionDAO dao = getTransactionDAO();
        String sql = "select EMG_TRAN_ID from emg_transaction where rownum = 1";
        
        Number id = (Number)dao.getJdbcTemplate().queryForObject(sql, new SingleObjectRowMapper(1));
        if (logger.isDebugEnabled()) {
            logger.debug("getTransactionId: id="+ id);
        }
        assertNotNull("Expected not-null transaction id", id);
        return id;
    }

    /**
     * Returns a spring-configured instance of TransactionDAO.
     * @return spring-configured instance of TransactionDAO.
     */
    protected TransactionDAO getTransactionDAO() {
        ApplicationContext context = getContext(); 
        TransactionDAO dao = (TransactionDAO)context.getBean("transactionDAO");
        return dao;
    }
    
    protected InitiatedTransaction generateInitiatedTransaction() {
        Sender sender = new Sender();
        Number consumerId = getConsumerId();
        sender.setSenderConsumerId(consumerId.longValue());
        sender.setSenderIPAddress("1.2.3.4");
        sender.setSenderLoyaltyMemberId("12345678890");
        //sender.setFiAccountId(12);
        //sender.setBackupFIAccountId(123);

        ReceiverConsumerAddress address = new ReceiverConsumerAddress();
        address.setLine1("123 great lane");
        address.setCity("minnetonka");
        address.setState("MN");
        address.setCountry("USA");

        Receiver receiver = new Receiver();
        receiver.setReceiverName(new ReceiverConsumerName("first", "last", null, null));
        receiver.setReceiverAddress(address);
        
        InitiatedTransaction transaction = new InitiatedTransaction();
        transaction.setDeliveryOption(0);
        transaction.setSender(sender);
        transaction.setReceiver(receiver);
        transaction.setTransactionAmount(new TransactionAmount(new BigDecimal(5), new BigDecimal(4), null, new BigDecimal(100), new BigDecimal(104), null,null,null,null,null));
        transaction.setProductType(MGOProductType.MGSEND);
        transaction.setSendAgentID(43636949);
        transaction.setSendCountry("USA");
        transaction.setSendCurrency("USD");
        transaction.setReceiveCountry("RUS");
        transaction.setReceiveCurrency("USD");
        transaction.setReceiveState("AZ");
        transaction.setStatus("SAV");
        transaction.setSubStatus("NOF");
        transaction.setAddressId(getConsumerAddressId(consumerId).longValue());
        transaction.setConfirmationNumber("1234567");
        transaction.setComment("Test transaction comment");
        
        transaction.setSourceSite("MGO");
        
        return transaction;
    }
    
    @Override
    protected String[] getContextResources() {
        return new String[] { "TransactionTestContext.xml" };
    }

}
