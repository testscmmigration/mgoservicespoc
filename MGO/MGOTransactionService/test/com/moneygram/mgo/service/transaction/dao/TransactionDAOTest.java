/*
 * Created on Jul 16, 2009
 *
 */

package com.moneygram.mgo.service.transaction.dao;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.transaction.BaseTransactionServiceTestCase;
import com.moneygram.mgo.service.transaction.MGOTransaction;
import com.moneygram.mgo.service.transaction.TransactionSummary;
import com.moneygram.mgo.service.transaction.dao.TransactionDAO;

/**
 * 
 * Transaction DAO Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.17 $ $Date: 2011/09/18 00:23:40 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class TransactionDAOTest extends BaseTransactionServiceTestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(TransactionDAOTest.class);
    
    public void xtestGetTransactions() throws Exception {
        TransactionDAO dao = getTransactionDAO();
        Number consumerId = getConsumerId();
        
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DAY_OF_YEAR, -365);
        
        Calendar end = Calendar.getInstance();
        
        int maxRecords = 2;
        List<TransactionSummary> transactions = dao.getTransactions(consumerId, start, end, maxRecords, null, null, null, "MGSEND,MGCASH,ESSEND,EPSEND,EPCASH", "MGO");
        assertNotNull("Expected not-null transactions list", transactions);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetTransactions: size="+ transactions.size() +" transactions="+ transactions);
        }
        assertTrue("Expected not empty transactions list", transactions.size() > 0);
        assertTrue("Expected length to be <= "+ maxRecords, transactions.size() <= maxRecords);
        assertTrue("Expected transaction instance of TransactionSummary", transactions.get(0) instanceof TransactionSummary);
    }
    
    public void xtestCountTransactions() throws Exception {
        TransactionDAO dao = getTransactionDAO();
        Number consumerId = getConsumerId();
        
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DAY_OF_YEAR, -365);
        
        Calendar end = Calendar.getInstance();
        
        int maxRecords = 2;
        Integer transactions = 2;//dao.countTransactions(consumerId, start, end, maxRecords, null, null, "ESSEND");
        assertNotNull("Expected not-null transactions count", transactions);
        if (logger.isDebugEnabled()) {
            logger.debug("testCountTransactions: "+ transactions);
        }        
        assertTrue("Expected count to be <= "+ maxRecords, transactions <= maxRecords);        
    }
    
    public void xtestGetTransactionDetails() throws Exception {
        TransactionDAO dao = getTransactionDAO();

        Number transactionId = getTransactionId();
        
        MGOTransaction detailedTransaction = dao.getTransaction(transactionId);
        assertNotNull("Expected not-null detailedTransaction", detailedTransaction);
        assertEquals("Expected IDs to match", transactionId.longValue(), detailedTransaction.getMgoProcessing().getTransactionId());
        assertNotNull("Expected not-null SourceSite", detailedTransaction.getSourceSite());
    }
    
    public void testGetPendingStartDate() throws Exception {
    	Calendar curTime = Calendar.getInstance();
    	long time2HoursBack = curTime.getTimeInMillis() - (1000* 60 * 60 * 2);
    	Date pendStartDate = TransactionDAO.getPendingStartDate(2);
    	
    	//assert that the time returned should be within 10 seconds
    	long timeDiff = Math.abs(pendStartDate.getTime() - time2HoursBack);
    	assertTrue (timeDiff < 10000);
    }    
}
