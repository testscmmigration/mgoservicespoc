package com.moneygram.mgo.shared;

import java.util.HashMap;
import java.util.Map;

public class ErrorCodes {

	public final static String CONSUMER_NOT_FOUND = "301";
	public final static String ACCOUNT_NOT_FOUND = "302";
	public final static String RECV_AGENT_NOT_FOUND = "303";
    public final static String ACCOUNT_ALREADY_EXISTS = "304";

	private final static Map<String, String> codes = new HashMap<String, String>();
	static {
		codes.put(CONSUMER_NOT_FOUND, "Consumer not found");
		codes.put(ACCOUNT_NOT_FOUND, "Account not found");
		codes.put(RECV_AGENT_NOT_FOUND, "Receive agent not found");
        codes.put(ACCOUNT_ALREADY_EXISTS, "Account already exists");
	}

	public static String getMessage(String code) {
		if (code == null)
			return "Unknown";
		String msg = codes.get(code);
		if (msg == null)
			return "Unknown";
		return msg;
	}
}
