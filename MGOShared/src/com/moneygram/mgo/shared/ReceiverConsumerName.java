/**
 * ReceiverConsumerName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.shared;

/**
 * 
 * Receiver Consumer Name.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOShared</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/07/08 20:49:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ReceiverConsumerName  extends ConsumerName  implements java.io.Serializable {
    private java.lang.String maternalName;

    public ReceiverConsumerName() {
    }

    public ReceiverConsumerName(
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.String middleName,
           java.lang.String maternalName) {
        super(
            firstName,
            lastName,
            middleName);
        this.maternalName = maternalName;
    }


    /**
     * Gets the maternalName value for this ReceiverConsumerName.
     * 
     * @return maternalName
     */
    public java.lang.String getMaternalName() {
        return maternalName;
    }


    /**
     * Sets the maternalName value for this ReceiverConsumerName.
     * 
     * @param maternalName
     */
    public void setMaternalName(java.lang.String maternalName) {
        this.maternalName = maternalName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReceiverConsumerName)) return false;
        ReceiverConsumerName other = (ReceiverConsumerName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.maternalName==null && other.getMaternalName()==null) || 
             (this.maternalName!=null &&
              this.maternalName.equals(other.getMaternalName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMaternalName() != null) {
            _hashCode += getMaternalName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" MaternalName=").append(getMaternalName());
        return buffer.toString();
    }
}
