/**
 * ConsumerAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.shared;

public class ConsumerAddress implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private java.lang.Long addressId;
	private java.lang.String line1;
	private java.lang.String line2;
	private java.lang.String line3;
    private java.lang.String buildingName;
    private java.lang.String county;
	private java.lang.String city;
	private java.lang.String state;
	private java.lang.String zipCode;
	private java.lang.String country;

	public ConsumerAddress() {
	}

    public ConsumerAddress(
           java.lang.Long addressId,
           java.lang.String line1,
           java.lang.String line2,
           java.lang.String line3,
           java.lang.String buildingName,
           java.lang.String county,
           java.lang.String city,
           java.lang.String state,
           java.lang.String zipCode,
           java.lang.String country) {
		this.addressId = addressId;
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
           this.buildingName = buildingName;
           this.county = county;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.country = country;
	}

	/**
	 * Gets the addressId value for this ConsumerAddress.
	 *
	 * @return addressId
	 */
	public java.lang.Long getAddressId() {
		return addressId;
	}

	/**
	 * Sets the addressId value for this ConsumerAddress.
	 *
	 * @param addressId
	 */
	public void setAddressId(java.lang.Long addressId) {
		this.addressId = addressId;
	}

	/**
	 * Gets the line1 value for this ConsumerAddress.
	 *
	 * @return line1
	 */
	public java.lang.String getLine1() {
		return line1;
	}

	/**
	 * Sets the line1 value for this ConsumerAddress.
	 *
	 * @param line1
	 */
	public void setLine1(java.lang.String line1) {
		this.line1 = line1;
	}

	/**
	 * Gets the line2 value for this ConsumerAddress.
	 *
	 * @return line2
	 */
	public java.lang.String getLine2() {
		return line2;
	}

	/**
	 * Sets the line2 value for this ConsumerAddress.
	 *
	 * @param line2
	 */
	public void setLine2(java.lang.String line2) {
		this.line2 = line2;
	}

	/**
	 * Gets the line3 value for this ConsumerAddress.
	 *
	 * @return line3
	 */
	public java.lang.String getLine3() {
		return line3;
	}

	/**
	 * Sets the line3 value for this ConsumerAddress.
	 *
	 * @param line3
	 */
	public void setLine3(java.lang.String line3) {
		this.line3 = line3;
	}
	/**
	 * @return the buildingName
	 */
	public java.lang.String getBuildingName() {
		return buildingName;
	}

	/**
	 * @param buildingName the buildingName to set
	 */
	public void setBuildingName(java.lang.String buildingName) {
		this.buildingName = buildingName;
	}

	/**
	 * @return the county
	 */
	public java.lang.String getCounty() {
		return county;
	}

	/**
	 * @param county the county to set
	 */
	public void setCounty(java.lang.String county) {
		this.county = county;
	}

	/**
	 * Gets the city value for this ConsumerAddress.
	 *
	 * @return city
	 */
	public java.lang.String getCity() {
		return city;
	}

	/**
	 * Sets the city value for this ConsumerAddress.
	 *
	 * @param city
	 */
	public void setCity(java.lang.String city) {
		this.city = city;
	}

	/**
	 * Gets the state value for this ConsumerAddress.
	 *
	 * @return state
	 */
	public java.lang.String getState() {
		return state;
	}

	/**
	 * Sets the state value for this ConsumerAddress.
	 *
	 * @param state
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}

	/**
	 * Gets the zipCode value for this ConsumerAddress.
	 *
	 * @return zipCode
	 */
	public java.lang.String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the zipCode value for this ConsumerAddress.
	 *
	 * @param zipCode
	 */
	public void setZipCode(java.lang.String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Gets the country value for this ConsumerAddress.
	 *
	 * @return country
	 */
	public java.lang.String getCountry() {
		return country;
	}

	/**
	 * Sets the country value for this ConsumerAddress.
	 *
	 * @param country
	 */
	public void setCountry(java.lang.String country) {
		this.country = country;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof ConsumerAddress))
			return false;
		ConsumerAddress other = (ConsumerAddress) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.addressId == null && other.getAddressId() == null) || (this.addressId != null && this.addressId
						.equals(other.getAddressId())))
				&& ((this.line1 == null && other.getLine1() == null) || (this.line1 != null && this.line1
						.equals(other.getLine1())))
				&& ((this.line2 == null && other.getLine2() == null) || (this.line2 != null && this.line2
						.equals(other.getLine2())))
				&& ((this.line3 == null && other.getLine3() == null) || (this.line3 != null && this.line3
						.equals(other.getLine3())))
				&& ((this.buildingName == null && other.getBuildingName() == null) || (this.buildingName != null && this.buildingName
						.equals(other.getBuildingName())))
				&& ((this.county == null && other.getCounty() == null) || (this.county != null && this.county
						.equals(other.getCounty())))
				&& ((this.city == null && other.getCity() == null) || (this.city != null && this.city
						.equals(other.getCity())))
				&& ((this.state == null && other.getState() == null) || (this.state != null && this.state
						.equals(other.getState())))
				&& ((this.zipCode == null && other.getZipCode() == null) || (this.zipCode != null && this.zipCode
						.equals(other.getZipCode())))
				&& ((this.country == null && other.getCountry() == null) || (this.country != null && this.country
						.equals(other.getCountry())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getAddressId() != null) {
			_hashCode += getAddressId().hashCode();
		}
		if (getLine1() != null) {
			_hashCode += getLine1().hashCode();
		}
		if (getLine2() != null) {
			_hashCode += getLine2().hashCode();
		}
		if (getLine3() != null) {
			_hashCode += getLine3().hashCode();
		}
        if (getBuildingName() != null) {
            _hashCode += getBuildingName().hashCode();
        }
        if (getCounty() != null) {
            _hashCode += getCounty().hashCode();
        }
		if (getCity() != null) {
			_hashCode += getCity().hashCode();
		}
		if (getState() != null) {
			_hashCode += getState().hashCode();
		}
		if (getZipCode() != null) {
			_hashCode += getZipCode().hashCode();
		}
		if (getCountry() != null) {
			_hashCode += getCountry().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(super.toString());
		buffer.append("[ AddressId=").append(getAddressId());
		buffer.append(" Line1=").append(getLine1());
		buffer.append(" Line2=").append(getLine2());
		buffer.append(" Line3=").append(getLine3());
		buffer.append(" City=").append(getCity());
		buffer.append(" State=").append(getState());
		buffer.append(" ZipCode=").append(getZipCode());
		buffer.append(" Country=").append(getCountry());
		buffer.append(" ]");
		return buffer.toString();
	}


}
