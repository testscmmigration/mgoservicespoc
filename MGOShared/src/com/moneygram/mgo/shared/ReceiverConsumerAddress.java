/**
 * ReceiverConsumerAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.shared;

/**
 *
 * Receiver Consumer Address.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOShared</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2012/01/22 18:12:33 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class ReceiverConsumerAddress  extends ConsumerAddress  implements java.io.Serializable {
    private java.lang.String direction1;

    private java.lang.String direction2;

    private java.lang.String direction3;

    public ReceiverConsumerAddress() {
    }

    public ReceiverConsumerAddress(
           java.lang.Long addressId,
           java.lang.String line1,
           java.lang.String line2,
           java.lang.String line3,
           java.lang.String buildingName,
           java.lang.String county,
           java.lang.String city,
           java.lang.String state,
           java.lang.String zipCode,
           java.lang.String country,
           java.lang.String direction1,
           java.lang.String direction2,
           java.lang.String direction3) {
        super(
            addressId,
            line1,
            line2,
            line3,
            buildingName,
            county,
            city,
            state,
            zipCode,
            country);
        this.direction1 = direction1;
        this.direction2 = direction2;
        this.direction3 = direction3;
    }


    /**
     * Gets the direction1 value for this ReceiverConsumerAddress.
     *
     * @return direction1
     */
    public java.lang.String getDirection1() {
        return direction1;
    }


    /**
     * Sets the direction1 value for this ReceiverConsumerAddress.
     *
     * @param direction1
     */
    public void setDirection1(java.lang.String direction1) {
        this.direction1 = direction1;
    }


    /**
     * Gets the direction2 value for this ReceiverConsumerAddress.
     *
     * @return direction2
     */
    public java.lang.String getDirection2() {
        return direction2;
    }


    /**
     * Sets the direction2 value for this ReceiverConsumerAddress.
     *
     * @param direction2
     */
    public void setDirection2(java.lang.String direction2) {
        this.direction2 = direction2;
    }


    /**
     * Gets the direction3 value for this ReceiverConsumerAddress.
     *
     * @return direction3
     */
    public java.lang.String getDirection3() {
        return direction3;
    }


    /**
     * Sets the direction3 value for this ReceiverConsumerAddress.
     *
     * @param direction3
     */
    public void setDirection3(java.lang.String direction3) {
        this.direction3 = direction3;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals2(java.lang.Object obj) {
        if (!(obj instanceof ReceiverConsumerAddress)) return false;
        ReceiverConsumerAddress other = (ReceiverConsumerAddress) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) &&
            ((this.direction1==null && other.getDirection1()==null) ||
             (this.direction1!=null &&
              this.direction1.equals(other.getDirection1()))) &&
            ((this.direction2==null && other.getDirection2()==null) ||
             (this.direction2!=null &&
              this.direction2.equals(other.getDirection2()))) &&
            ((this.direction3==null && other.getDirection3()==null) ||
             (this.direction3!=null &&
              this.direction3.equals(other.getDirection3())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode2() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDirection1() != null) {
            _hashCode += getDirection1().hashCode();
        }
        if (getDirection2() != null) {
            _hashCode += getDirection2().hashCode();
        }
        if (getDirection3() != null) {
            _hashCode += getDirection3().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" AddressId=").append(getAddressId());
        buffer.append(" Line1=").append(getLine1());
        buffer.append(" Line2=").append(getLine2());
        buffer.append(" Line3=").append(getLine3());
        buffer.append(" City=").append(getCity());
        buffer.append(" State=").append(getState());
        buffer.append(" ZipCode=").append(getZipCode());
        buffer.append(" Country=").append(getCountry());
        return buffer.toString();
    }
}
