package com.moneygram.ccp.command;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckResponse;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;

public class HealthCheckCommandTest extends BaseTest {
	private HealthCheckCommand instance;
	private GlobalCollectImpl globalCollectImplMock;
	
	@Before
	public void before() {
		globalCollectImplMock = createStrictMock(GlobalCollectImpl.class);
		
		instance = new HealthCheckCommand();
		instance.setGlobalCollectImpl(globalCollectImplMock);
	}
	
	@Test
	public void testProcess() throws Exception {
		HealthCheckRequest healthCheckRequest = RequestObjectMother.buildHealthCheckRequest();
		HealthCheckResponse healthCheckResponse = new HealthCheckResponse();
		
		expect(globalCollectImplMock.process(healthCheckRequest)).andReturn(healthCheckResponse);
		
		replay(globalCollectImplMock);
		BaseServiceResponseMessage response = instance.process(healthCheckRequest);

		verify(globalCollectImplMock);
		assertEquals(healthCheckResponse, response);
	}

	@Test
	public void testGetResponseToReturnError() throws Exception {
		assertTrue(instance.getResponseToReturnError() instanceof HealthCheckResponse);
	}
	
	@Test
	public void testIsRequestSupported() throws Exception {
		assertTrue(instance.isRequestSupported(new HealthCheckRequest()));
		assertFalse(instance.isRequestSupported(new PostTransactionRequest()));
	}
}

