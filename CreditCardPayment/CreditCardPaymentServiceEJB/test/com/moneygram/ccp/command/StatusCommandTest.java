package com.moneygram.ccp.command;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;

public class StatusCommandTest extends BaseTest {
	private StatusCommand instance;
	private GlobalCollectImpl globalCollectImplMock;
	
	@Before
	public void before() {
		globalCollectImplMock = createStrictMock(GlobalCollectImpl.class);
		
		instance = new StatusCommand();
		instance.setGlobalCollectImpl(globalCollectImplMock);
	}
	
	@Test
	public void testProcess() throws Exception {
		StatusRequest statusRequest = RequestObjectMother.buildStatusRequest();
		statusRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, null));
		StatusResponse orderStatusResponse = new StatusResponse();
		
		expect(globalCollectImplMock.process(statusRequest)).andReturn(orderStatusResponse);
		
		replay(globalCollectImplMock);
		BaseServiceResponseMessage response = instance.process(statusRequest);

		verify(globalCollectImplMock);
		assertEquals(orderStatusResponse, response);
	}

	@Test
	public void testGetResponseToReturnError() throws Exception {
		assertTrue(instance.getResponseToReturnError() instanceof StatusResponse);
	}
	
	@Test
	public void testIsRequestSupported() throws Exception {
		assertTrue(instance.isRequestSupported(new StatusRequest()));
		assertFalse(instance.isRequestSupported(new PostTransactionRequest()));
	}
}
