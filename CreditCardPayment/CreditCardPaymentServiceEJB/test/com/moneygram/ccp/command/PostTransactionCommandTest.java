package com.moneygram.ccp.command;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionResponse;

public class PostTransactionCommandTest extends BaseTest {
	private PostTransactionCommand instance;
	private GlobalCollectImpl globalCollectImplMock;
	
	@Before
	public void before() {
		globalCollectImplMock = createStrictMock(GlobalCollectImpl.class);
		
		instance = new PostTransactionCommand();
		instance.setGlobalCollectImpl(globalCollectImplMock);
	}
	
	@Test
	public void testProcess() throws Exception {
		PostTransactionRequest postTransactionRequest = RequestObjectMother.buildPostTransactionRequest();
		PostTransactionResponse postTransactionResponse = new PostTransactionResponse();
		
		expect(globalCollectImplMock.process(postTransactionRequest)).andReturn(postTransactionResponse);
		
		replay(globalCollectImplMock);
		BaseServiceResponseMessage response = instance.process(postTransactionRequest);

		verify(globalCollectImplMock);
		assertEquals(postTransactionResponse, response);
	}

	@Test
	public void testGetResponseToReturnError() throws Exception {
		assertTrue(instance.getResponseToReturnError() instanceof PostTransactionResponse);
	}
	
	@Test
	public void testIsRequestSupported() throws Exception {
		assertTrue(instance.isRequestSupported(new PostTransactionRequest()));
		assertFalse(instance.isRequestSupported(new AuthorizationRequest()));
	}
}

