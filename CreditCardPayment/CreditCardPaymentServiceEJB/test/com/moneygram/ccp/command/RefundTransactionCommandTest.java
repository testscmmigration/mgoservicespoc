package com.moneygram.ccp.command;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.globalcollect.domain.PaymentStatusId;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectStatusResponse;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class RefundTransactionCommandTest extends BaseTest {
	private RefundTransactionCommand instance;
	private GlobalCollectImpl globalCollectGetOrderStatusImplMock;
	private GlobalCollectImpl globalCollectCancelSetPaymentImplMock;
	private GlobalCollectImpl globalCollectDoRefundImplMock;
	
	@Before
	public void before() {
		globalCollectGetOrderStatusImplMock = createStrictMock(GlobalCollectImpl.class);
		globalCollectCancelSetPaymentImplMock = createStrictMock(GlobalCollectImpl.class);
		globalCollectDoRefundImplMock = createStrictMock(GlobalCollectImpl.class);
		
		instance = new RefundTransactionCommand();
		instance.setGlobalCollectCancelSetPaymentImpl(globalCollectCancelSetPaymentImplMock);
		instance.setGlobalCollectDoRefundImpl(globalCollectDoRefundImplMock);
		instance.setGlobalCollectGetOrderStatusImpl(globalCollectGetOrderStatusImplMock);
	}
	
	@Test
	public void testProcess_FullCancel() throws Exception {
		GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
		globalCollect.setStatusId(PaymentStatusId.PAYMENT_STATUS_READY);

		CurrencyAmount statusAmount = new CurrencyAmount();
		statusAmount.setAmount(10.99f);
		
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setGlobalCollect(globalCollect);
		statusResponse.setAmount(statusAmount);

		CurrencyAmount refundAmount = new CurrencyAmount();
		refundAmount.setAmount(10.99f);
		
		RefundTransactionRequest refundTransactionRequest = RequestObjectMother.buildRefundTransactionRequest();
		refundTransactionRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));
		refundTransactionRequest.setAmount(refundAmount);

		RefundTransactionResponse refundTransactionResponse = new RefundTransactionResponse();

		expect(globalCollectGetOrderStatusImplMock.process(anyObject(StatusRequest.class))).andReturn(statusResponse);
		expect(globalCollectCancelSetPaymentImplMock.process(refundTransactionRequest)).andReturn(refundTransactionResponse);
				
		replay(globalCollectGetOrderStatusImplMock);
		replay(globalCollectCancelSetPaymentImplMock);
		BaseServiceResponseMessage response = instance.process(refundTransactionRequest);

		verify(globalCollectGetOrderStatusImplMock);
		verify(globalCollectCancelSetPaymentImplMock);
		assertEquals(refundTransactionResponse, response);
	}
	
	@Test(expected=SystemException.class)
	public void testProcess_PartialCancel() throws Exception {
		GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
		globalCollect.setStatusId(PaymentStatusId.PAYMENT_STATUS_READY);

		CurrencyAmount statusAmount = new CurrencyAmount();
		statusAmount.setAmount(5.99f);
		
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setGlobalCollect(globalCollect);
		statusResponse.setAmount(statusAmount);

		CurrencyAmount refundAmount = new CurrencyAmount();
		refundAmount.setAmount(10.99f);
		
		RefundTransactionRequest refundTransactionRequest = RequestObjectMother.buildRefundTransactionRequest();
		refundTransactionRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));
		refundTransactionRequest.setAmount(refundAmount);

		RefundTransactionResponse refundTransactionResponse = new RefundTransactionResponse();

		expect(globalCollectGetOrderStatusImplMock.process(anyObject(StatusRequest.class))).andReturn(statusResponse);
		expect(globalCollectCancelSetPaymentImplMock.process(refundTransactionRequest)).andReturn(refundTransactionResponse);
				
		replay(globalCollectGetOrderStatusImplMock);
		replay(globalCollectCancelSetPaymentImplMock);
		instance.process(refundTransactionRequest);
	}
	
	@Test
	public void testProcess_FullRefund() throws Exception {
		GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
		globalCollect.setStatusId(PaymentStatusId.PAYMENT_STATUS_PROCESSED);
		
		CurrencyAmount statusAmount = new CurrencyAmount();
		statusAmount.setAmount(10.99f);
		
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setGlobalCollect(globalCollect);
		statusResponse.setAmount(statusAmount);

		CurrencyAmount refundAmount = new CurrencyAmount();
		refundAmount.setAmount(10.99f);

		RefundTransactionRequest refundTransactionRequest = RequestObjectMother.buildRefundTransactionRequest();
		refundTransactionRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));
		refundTransactionRequest.setAmount(refundAmount);

		RefundTransactionResponse refundTransactionResponse = new RefundTransactionResponse();

		expect(globalCollectGetOrderStatusImplMock.process(anyObject(StatusRequest.class))).andReturn(statusResponse);
		expect(globalCollectDoRefundImplMock.process(refundTransactionRequest)).andReturn(refundTransactionResponse);
				
		replay(globalCollectGetOrderStatusImplMock);
		replay(globalCollectDoRefundImplMock);
		BaseServiceResponseMessage response = instance.process(refundTransactionRequest);

		verify(globalCollectGetOrderStatusImplMock);
		verify(globalCollectDoRefundImplMock);
		assertEquals(refundTransactionResponse, response);
	}

	@Test
	public void testProcess_PartialRefund() throws Exception {
		GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
		globalCollect.setStatusId(PaymentStatusId.PAYMENT_STATUS_PROCESSED);
		
		CurrencyAmount statusAmount = new CurrencyAmount();
		statusAmount.setAmount(5.99f);
		
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setGlobalCollect(globalCollect);
		statusResponse.setAmount(statusAmount);

		CurrencyAmount refundAmount = new CurrencyAmount();
		refundAmount.setAmount(10.99f);

		RefundTransactionRequest refundTransactionRequest = RequestObjectMother.buildRefundTransactionRequest();
		refundTransactionRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));
		refundTransactionRequest.setAmount(refundAmount);

		RefundTransactionResponse refundTransactionResponse = new RefundTransactionResponse();

		expect(globalCollectGetOrderStatusImplMock.process(anyObject(StatusRequest.class))).andReturn(statusResponse);
		expect(globalCollectDoRefundImplMock.process(refundTransactionRequest)).andReturn(refundTransactionResponse);
				
		replay(globalCollectGetOrderStatusImplMock);
		replay(globalCollectDoRefundImplMock);
		BaseServiceResponseMessage response = instance.process(refundTransactionRequest);

		verify(globalCollectGetOrderStatusImplMock);
		verify(globalCollectDoRefundImplMock);
		assertEquals(refundTransactionResponse, response);
	}

	@Test(expected=CommandException.class)
	public void testProcess_InvalidStatus() throws Exception {
		GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
		globalCollect.setStatusId(100);
		
		StatusResponse statusResponse = new StatusResponse();
		statusResponse.setGlobalCollect(globalCollect);

		RefundTransactionRequest refundTransactionRequest = RequestObjectMother.buildRefundTransactionRequest();
		refundTransactionRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));

		expect(globalCollectGetOrderStatusImplMock.process(anyObject(StatusRequest.class))).andReturn(statusResponse);
				
		replay(globalCollectGetOrderStatusImplMock);
		instance.process(refundTransactionRequest);
	}
	
	@Test
	public void testGetResponseToReturnError() throws Exception {
		assertTrue(instance.getResponseToReturnError() instanceof RefundTransactionResponse);
	}
	
	@Test
	public void testIsRequestSupported() throws Exception {
		assertTrue(instance.isRequestSupported(new RefundTransactionRequest()));
		assertFalse(instance.isRequestSupported(new AuthorizationRequest()));
	}
}
