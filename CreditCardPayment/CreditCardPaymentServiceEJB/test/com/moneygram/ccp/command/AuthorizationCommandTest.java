package com.moneygram.ccp.command;

import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.cybersource.CybersourceAuthorizeImpl;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.matchers.BinLookupRequestMatcher;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;
import com.moneygram.service.creditcardpaymentservice_v1.BinlookupResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.framework.command.SystemException;

public class AuthorizationCommandTest extends BaseTest {
	private AuthorizationCommand instance;
	private CybersourceAuthorizeImpl cybersourceAuthorizeImplMock;
	private GlobalCollectImpl globalCollectInsertOrderWithPaymentImplMock;
	private GlobalCollectImpl globalCollectDoPaymentImplMock;
	private GlobalCollectImpl globalCollectDoBinLookupImplMock;
	
	@Before
	public void before() {
		cybersourceAuthorizeImplMock = createStrictMock(CybersourceAuthorizeImpl.class);
		globalCollectInsertOrderWithPaymentImplMock = createStrictMock(GlobalCollectImpl.class);
		globalCollectDoPaymentImplMock = createStrictMock(GlobalCollectImpl.class);
		globalCollectDoBinLookupImplMock = createStrictMock(GlobalCollectImpl.class);
		
		instance = new AuthorizationCommand();
		instance.setCybersourceAuthorizeImpl(cybersourceAuthorizeImplMock);
		instance.setGlobalCollectDoPaymentImpl(globalCollectDoPaymentImplMock);
		instance.setGlobalCollectInsertOrderWithPaymentImpl(globalCollectInsertOrderWithPaymentImplMock);
		instance.setGlobalCollectDoBinLookupImpl(globalCollectDoBinLookupImplMock);
	}
	
	@Test
	public void testProcess_CyberSource() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "US", false,"CC");
		AuthorizationResponse authorizationResponse = new AuthorizationResponse(); 
		
		expect(cybersourceAuthorizeImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		replay(cybersourceAuthorizeImplMock);
		BaseServiceResponseMessage response = instance.process(authorizationRequest);

		verify(cybersourceAuthorizeImplMock);
		assertEquals(authorizationResponse, response);
	}

	@Test
	public void testProcess_UK_EffortId_1() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		authorizationRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 1L));
		
		AuthorizationResponse authorizationResponse = new AuthorizationResponse(); 
		
		expect(globalCollectInsertOrderWithPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		replay(globalCollectInsertOrderWithPaymentImplMock);
		BaseServiceResponseMessage response = instance.process(authorizationRequest);

		verify(globalCollectInsertOrderWithPaymentImplMock);
		assertEquals(authorizationResponse, response);
	}
	
	@Test
	public void testProcess_UK_EffortId_Null_No_orderFoundGetStausErrorCode() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		authorizationRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, null));
		
		AuthorizationResponse authorizationResponse = RequestObjectMother.buildAuthorizationResponse(AuthorizationCommand.GC_ORDER_STATUS_NOT_FOUND_ERROR_CODE, "GC_ORDER_STATUS_NOT_FOUND_ERROR_CODE");
		
		AuthorizationResponse authorizationResponseForInsert = RequestObjectMother.buildAuthorizationResponse(null,null);
		
		expect(globalCollectDoPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		expect(globalCollectInsertOrderWithPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponseForInsert);
		
		replay(globalCollectInsertOrderWithPaymentImplMock);
		
		replay(globalCollectDoPaymentImplMock);
		
		AuthorizationResponse response = (AuthorizationResponse) instance.process(authorizationRequest);

		verify(globalCollectDoPaymentImplMock);
		verify(globalCollectInsertOrderWithPaymentImplMock);
		assertEquals(new Long(1), authorizationRequest.getGlobalCollect().getEffortId());
		assertEquals(authorizationResponseForInsert, response);
	}
	
	
	@Test
	public void testProcess_UK_EffortId_Null_No_orderFoundDOPaymentErrorCode() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		authorizationRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, null));
		
		AuthorizationResponse authorizationResponse = RequestObjectMother.buildAuthorizationResponse(AuthorizationCommand.GC_DOPAYMENT_ORDER_NOT_FOUND, "GC_DOPAYMENT_ORDER_NOT_FOUND");
		
		AuthorizationResponse authorizationResponseForInsert = RequestObjectMother.buildAuthorizationResponse(null,null);
		
		expect(globalCollectDoPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		expect(globalCollectInsertOrderWithPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponseForInsert);
		
		replay(globalCollectInsertOrderWithPaymentImplMock);
		
		replay(globalCollectDoPaymentImplMock);
		
		AuthorizationResponse response = (AuthorizationResponse) instance.process(authorizationRequest);

		verify(globalCollectDoPaymentImplMock);
		verify(globalCollectInsertOrderWithPaymentImplMock);
		assertEquals(new Long(1), authorizationRequest.getGlobalCollect().getEffortId());
		assertEquals(authorizationResponseForInsert, response);
	}	
	
	
	@Test
	public void testProcess_UK_EffortId_Null_No_orderFound_CardType_unknown() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequestwithCard(null, "GB", false);
		authorizationRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, null));
		authorizationRequest.getCard().setCardType(CardType.UNKNOWN);
		
		BinLookupRequest binReq = RequestObjectMother.buildDoBinlookupRequest(authorizationRequest.getCard().getAccountNumber(),authorizationRequest.getMerchantID());
		
		BinlookupResponse binRes = RequestObjectMother.buildBinLookupResponse(CardType.DEBIT,"GB",null,null);
		
		AuthorizationResponse authorizationResponse = RequestObjectMother.buildAuthorizationResponse(AuthorizationCommand.GC_ORDER_STATUS_NOT_FOUND_ERROR_CODE, "GC_ORDER_STATUS_NOT_FOUND_ERROR_CODE");
		
		AuthorizationResponse authorizationResponseForInsert = RequestObjectMother.buildAuthorizationResponse(null,null);
		
		expect(globalCollectDoBinLookupImplMock.process(eqBinLookupRequest(binReq))).andReturn(binRes);
		
		expect(globalCollectDoPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		expect(globalCollectInsertOrderWithPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponseForInsert);
		
		replay(globalCollectInsertOrderWithPaymentImplMock);
		
		replay(globalCollectDoPaymentImplMock);
		
		replay(globalCollectDoBinLookupImplMock);
		
		AuthorizationResponse response = (AuthorizationResponse) instance.process(authorizationRequest);
		
		
		verify(globalCollectInsertOrderWithPaymentImplMock);
		verify(globalCollectDoPaymentImplMock);
		verify(globalCollectInsertOrderWithPaymentImplMock);
		assertEquals(new Long(1), authorizationRequest.getGlobalCollect().getEffortId());
		assertEquals(authorizationResponseForInsert, response);
		assertEquals(CardType.DEBIT, response.getCardType());
	}
	
	
	public static BinLookupRequest eqBinLookupRequest(BinLookupRequest in) {
	    EasyMock.reportMatcher(new BinLookupRequestMatcher(in));
	    return null;
	}
	
	@Test
	public void testProcess_UK_EffortId_2() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		authorizationRequest.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(12345L, 2L));
		
		AuthorizationResponse authorizationResponse = new AuthorizationResponse(); 
		
		expect(globalCollectDoPaymentImplMock.process(authorizationRequest)).andReturn(authorizationResponse);
		
		replay(globalCollectDoPaymentImplMock);
		BaseServiceResponseMessage response = instance.process(authorizationRequest);

		verify(globalCollectDoPaymentImplMock);
		assertEquals(authorizationResponse, response);
	}

	@Test(expected=SystemException.class)
	public void testProcess_UK_NullGlobalCollectSection() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		
		instance.process(authorizationRequest);
	}

	@Test(expected=SystemException.class)
	public void testProcess_UK_NullEffortId() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, "GB", false);
		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		globalCollect.setOrderid(12345L);
		
		instance.process(authorizationRequest);
	}

	@Test(expected=SystemException.class)
	public void testProcess_NullCountryCode() throws Exception {
		AuthorizationRequest authorizationRequest = RequestObjectMother.buildAuthorizationRequest(null, null, false);

		instance.process(authorizationRequest);
	}
	
	@Test
	public void testGetResponseToReturnError() throws Exception {
		assertTrue(instance.getResponseToReturnError() instanceof AuthorizationResponse);
	}
	
	@Test
	public void testIsRequestSupported() throws Exception {
		assertTrue(instance.isRequestSupported(new AuthorizationRequest()));
		assertFalse(instance.isRequestSupported(new PostTransactionRequest()));
	}
}
