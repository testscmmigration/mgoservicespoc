package com.moneygram.ccp.test;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.security.Security;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.ElementNameAndTextQualifier;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.BeforeClass;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.util.ReflectionUtils;
import org.w3c.dom.Node;

import com.moneygram.ree.lib.Config;

public abstract class BaseTest {
	private static final String RESOURCE_REF_JNDI_NAME = "java:comp/env/rep/CCPSResourceReference";
	
	private static Config resourceRefConfig = new Config();
	private static SimpleNamingContextBuilder builder;
	
	@BeforeClass
	public static void beforeBaseTest() throws Exception {
		ConsoleHandler handler = new ConsoleHandler();
		handler.setFormatter(new TestLogFormat());
		handler.setLevel(Level.ALL);
				
		Logger rootLogger = Logger.getLogger("");
		rootLogger.setLevel(Level.INFO);
		
		Logger moneygramLogger = Logger.getLogger("com.moneygram");
		moneygramLogger.setLevel(Level.ALL);

		for (Handler h : rootLogger.getHandlers()) {
			rootLogger.removeHandler(h);
		}
		rootLogger.addHandler(handler);
				
		builder = SimpleNamingContextBuilder.emptyActivatedContextBuilder();
		builder.bind(RESOURCE_REF_JNDI_NAME, resourceRefConfig);

		String configPath = new File("test", "config").getCanonicalPath();
		String logPath    = new File("logs").getCanonicalPath();
		
		new File(logPath).mkdirs();
		
		clearResourceRefs();
		addResourceRef("CyberSourceKeysDirectory",       configPath);
		addResourceRef("CyberSourceAPIVersion",          "1.11");
		addResourceRef("CyberSourceUseProduction",       "false");
		addResourceRef("CyberSourceLogEnabled",          "true");
		addResourceRef("CyberSourceLogDirectory",        logPath);
		addResourceRef("CyberSourceLogFilename",         "cybersource.log");		
		addResourceRef("CyberSourceLogMaxSizeMb",        "10");

		addResourceRef("GlobalCollectURL",               "https://ps.gcsip.nl/wdl/wdl");
		addResourceRef("GlobalCollectRetryCount",        "3");
		addResourceRef("GlobalCollectRequestSentRetry",  "true");
		addResourceRef("GlobalCollectConnectionTimeout", "70000");
		addResourceRef("GlobalCollectSocketTimeout",     "70000");

		// To avoid cannot find the specified class java.security.PrivilegedActionException: java.lang.ClassNotFoundException: com.ibm.websphere.ssl.protocol.SSLSocketFactory
		Security.setProperty("ssl.SocketFactory.provider", "com.ibm.jsse2.SSLSocketFactoryImpl");
		Security.setProperty("ssl.ServerSocketFactory.provider", "com.ibm.jsse2.SSLServerSocketFactoryImpl");
	}

	private static void clearResourceRefs() throws Exception {
		Field field = ReflectionUtils.findField(Config.class, "attributes");
		field.setAccessible(true);
		
		Method method = ReflectionUtils.findMethod(Map.class, "clear");
		method.setAccessible(true);

		ReflectionUtils.invokeMethod(method, field.get(resourceRefConfig));
	}
		
	private static void addResourceRef(String key, String value) {
		resourceRefConfig.setAttribute(key, value);
	}

	public static String getResourceRef(String key) {
		return resourceRefConfig.getProperties().getProperty(key);
	}
	
    public static void assertXMLEqual(String expected, String actual, final List<String> ignoredElementNames) 
		throws Throwable {
		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		
		final Diff diff = new Diff(expected, actual);
	    
		// Causes XMLUnit to ignore element order
		diff.overrideElementQualifier(new ElementNameAndTextQualifier());
		
		// Create a custom DifferenceListener to ignore any items in ignoredElementNames.
	    diff.overrideDifferenceListener(new DifferenceListener() {
	        public int differenceFound(final Difference theDifference) {
	            final int diffFound;
	            final String name = getNodeName(theDifference);
	            // Ignore all ID nodes since they are automatically updated.
	            if (ignoredElementNames != null && ignoredElementNames.contains(name)) {
	                diffFound = DifferenceListener.RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL;
	            } else {
	                diffFound = DifferenceListener.RETURN_ACCEPT_DIFFERENCE;
	            }
	            return diffFound;
	        }
	
	        public void skippedComparison(final Node theNode, final Node theNode1) {
	        }
	    });
	
	    try {
	    	XMLAssert.assertXMLEqual(diff, true);
	    } catch (Throwable t) {
	    	System.err.println("XML Comparison failed.  Expected:\n\n" + expected + "\nbut was:\n\n" + actual);
	    	throw t;
		}
	}
	
	private static String getNodeName(Difference d) {
		String name = null;
	    if (d.getTestNodeDetail().getNode() != null) {
	    	name = getNode(d.getTestNodeDetail().getNode()).getNodeName();
	    } else {
	    	name = getNode(d.getControlNodeDetail().getNode()).getNodeName();
	    }
	    
	    return name;
	}
	
	private static Node getNode (Node n) {
		while (n != null && n.getNodeType() == Node.TEXT_NODE) {
			n = n.getParentNode();
		}
		
		return n;
	}
}
