package com.moneygram.ccp.test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

public class TestLogFormat extends Formatter {
	public String format(LogRecord record) {
		String logger = (record.getLoggerName() == null) ? "root" : padClassName(record.getLoggerName(), 50);
		String date   = new SimpleDateFormat("h:mm:ss").format(new Date(record.getMillis()));
		String level  = pad(record.getLevel().toString(), 7);
		String thread = pad(Thread.currentThread().getName(), 16);

		StringBuilder builder = new StringBuilder();
		builder.append(date);
		builder.append(" ");
		builder.append(level); 
		builder.append(" ");
		builder.append(logger); 
		builder.append(" [");
		builder.append(thread);
		builder.append("]: ");
		builder.append(record.getMessage());
		builder.append("\n");
		
		if (record.getThrown() != null) {
			builder.append(ExceptionUtils.getFullStackTrace(record.getThrown()));
		}
		
		return builder.toString();
	}
	
	private String pad(String s, int length) {
		if ( s.length() < length) {
			s = StringUtils.rightPad(s, length);
		} else if ( s.length() > length) {
			s = s.substring(length);
		}
		return s;
	}

	private String padClassName(String s, int length) {
		if ( s.length() < length) {
			s = StringUtils.rightPad(s, length);
		} else if ( s.length() > length) {
			try {
				int prefixLength = s.indexOf(".");
				prefixLength = s.indexOf(".", prefixLength+1);
				String prefix = s.substring(0, prefixLength);
	
				int suffixLength = length - (prefix.length()+3);
				String suffix = s.substring(s.length() - suffixLength);
				
				s = prefix + "..." + suffix;
			} catch (Exception e) {
				s = "..." + StringUtils.right(s, length-3);
			}
		}		
		return s;
	}
}
