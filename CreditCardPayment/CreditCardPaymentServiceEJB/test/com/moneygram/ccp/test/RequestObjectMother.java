package com.moneygram.ccp.test;

import java.rmi.server.UID;

import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;
import com.moneygram.service.creditcardpaymentservice_v1.BinlookupResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BusinessRules;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;

public class RequestObjectMother {
	public static final String MERCHANT_ID = "6277";
	
	public static Header buildHeader(String action) {
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(action);
		pi.setReturnErrorsAsException(true);
		
		Header header = new Header();
		header.setProcessingInstruction(pi);
		return header;
	}
	
	public static Card buildCard(String name, String accountNumber, CardBrandType cardType, Integer expMonth, Integer expYear, String ccv) {
		Card card = new Card();
		card.setFullName(name);
		card.setAccountNumber(accountNumber);
		
		if (expMonth != null) {
			card.setExpirationMonth(expMonth);
		}
		
		if (expYear != null) {
			card.setExpirationYear(expYear);
		}
		
		if (cardType != null) {
			card.setCardBrandType(cardType.getName());
		}
		
		card.setCvv(ccv);
		return card;
	}
	
	public static AuthorizationResponse buildAuthorizationResponse(String errorCode,String errorMessage){
		AuthorizationResponse response = new AuthorizationResponse();
		if(errorCode!=null){
			CCPError error = new CCPError();
			error.setReasonCode(errorCode);
			error.setErrorMessage(errorMessage);
			CCPErrors ccpErrors = new CCPErrors();
			ccpErrors.getErrors().add(error);
			response.setErrors(ccpErrors);
		}
		return response;
	}
	
	public static CurrencyAmount buildCurrencyAmount(String currency, float amount) {
		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setCurrency(currency);
		currencyAmount.setAmount(amount);
		return currencyAmount;
	}
	
	public static BusinessRules buildBusinessRules() {
		BusinessRules businessRules = new BusinessRules();
		businessRules.setDeclineAVSFlags("");
		businessRules.setIgnoreAVSResult(false);
		return businessRules;
	}
	
	public static GlobalCollectRequest buildGlobalCollectRequest(Long orderId, Long effortId) {
		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		globalCollect.setOrderid(orderId);
		
		if (effortId != null) {
			globalCollect.setEffortId(effortId);
		}
		
		return globalCollect;
	}
	
	public static AuthorizationRequest buildAuthorizationRequestwithCard(String country, String countryCode, boolean performCVVAVS) {
		AuthorizationRequest request=  buildAuthorizationRequest(country,countryCode,performCVVAVS,MERCHANT_ID);
		Card card = new Card();
		card.setAccountNumber("1234567890123456");
		card.setCardType(CardType.CREDIT);
		request.setCard(card);
		return request;
		
	}

	public static AuthorizationRequest buildAuthorizationRequest(String country, String countryCode, boolean performCVVAVS) {
		return buildAuthorizationRequest(country,countryCode,performCVVAVS,MERCHANT_ID);
	}
	
	public static AuthorizationRequest buildAuthorizationRequest(String country, String countryCode, boolean performCVVAVS, String MerchantCode) {
		String mgiReferenceCode = new UID().toString();

		AuthorizationRequest request = new AuthorizationRequest();
		request.setMerchantID(MerchantCode);
		request.setMGIReferenceCode(mgiReferenceCode);
		request.setFirstName("Homer");
		request.setLastName("Simpson");
		request.setStreet1("123 Main Street");
		request.setStreet2("#100");
		request.setCity("New City");
		request.setState("New State");
		request.setPostalCode("55011");
		request.setCountry(country);
		request.setCountryCode2Char(countryCode);
		request.setEmail("homer@simpson.com");
		request.setCustomerId("999998");
		request.setPerformCVVAVS(performCVVAVS);
		request.setHeader(buildHeader("authorize"));
		request.setBusinessRules(buildBusinessRules());
		return request;
	}	
	
	public static AuthorizationRequest buildAuthorizationRequest(String street1, String postalCode, String country, String countryCode, 
			boolean performCVVAVS) {
		String mgiReferenceCode = new UID().toString();

		AuthorizationRequest request = new AuthorizationRequest();
		request.setMerchantID(MERCHANT_ID);
		request.setMGIReferenceCode(mgiReferenceCode);
		request.setFirstName("Homer");
		request.setLastName("Simpson");
		request.setStreet1(street1);
		request.setStreet2(null);
		request.setCity("New City");
		request.setState("New State");
		request.setPostalCode(postalCode);
		request.setCountry(country);
		request.setCountryCode2Char(countryCode);
		request.setEmail("homer@simpson.com");
		request.setCustomerId("999998");
		request.setPerformCVVAVS(performCVVAVS);
		request.setHeader(buildHeader("authorize"));
		request.setBusinessRules(buildBusinessRules());
		return request;
	}

	public static RefundTransactionRequest buildRefundTransactionRequest() {
		String mgiReferenceCode = new UID().toString();

		RefundTransactionRequest request = new RefundTransactionRequest();
		request.setMerchantID(MERCHANT_ID);
		request.setMGIReferenceCode(mgiReferenceCode);
		request.setHeader(buildHeader("refundTransaction"));
		return request;
	}

	public static PostTransactionRequest buildPostTransactionRequest() {
		String mgiReferenceCode = new UID().toString();

		PostTransactionRequest request = new PostTransactionRequest();
		request.setMerchantID(MERCHANT_ID);
		request.setMGIReferenceCode(mgiReferenceCode);
		request.setHeader(buildHeader("postTransaction"));
		return request;
	}
	
	public static HealthCheckRequest buildHealthCheckRequest() {
		HealthCheckRequest request = new HealthCheckRequest();
		request.setHeader(buildHeader("healthCheck"));
		request.setMerchantID(MERCHANT_ID);
		return request;
	}

	public static StatusRequest buildStatusRequest() {
		StatusRequest request = new StatusRequest();
		request.setHeader(buildHeader("orderStatus"));
		request.setMerchantID(MERCHANT_ID);
		return request;
	}

	public static BinLookupRequest buildDoBinlookupRequest(String accountNumber, String merchantID) {
		BinLookupRequest req = new BinLookupRequest();
		req.setAccountNumber(accountNumber);
		req.setMerchantID(merchantID);
		return req;
		
		
	}

	public static BinlookupResponse buildBinLookupResponse(CardType cardType, String countryCode,String errorCode,String errorMessage) {
		BinlookupResponse res = new BinlookupResponse();
		res.setCardType(cardType);
		res.setCountryCode(countryCode);
		if(errorCode!=null){
			CCPError error = new CCPError();
			error.setReasonCode(errorCode);
			error.setErrorMessage(errorMessage);
			CCPErrors ccpErrors = new CCPErrors();
			ccpErrors.getErrors().add(error);
			res.setErrors(ccpErrors);
		}
		return res;
	}
}
