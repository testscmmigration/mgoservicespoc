package com.moneygram.ccp.util;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;

public class CreditCardUtilTest extends BaseTest {
	@Test
	public void testMaskLastN() throws Exception {
		String actual = CreditCardUtil.maskLastN("1234567890123456", 8, 'X');
		assertEquals("XXXXXXXX90123456", actual);
	}

	@Test
	public void testMaskLast4() throws Exception {
		String actual = CreditCardUtil.maskLast4("1234567890123456");
		assertEquals("************3456", actual);
	}
	
	@Test
	public void testPciLogFilter_GC() throws Exception {
		String orginal = "blablabla<CREDITCARDNUMBER>4263982640269299</CREDITCARDNUMBER>blablabal<CVV>837</CVV>blablabla";
		String expected = "blablabla<CREDITCARDNUMBER>*****</CREDITCARDNUMBER>blablabal<CVV>*****</CVV>blablabla";
		String actual = CreditCardUtil.pciLogFilter(orginal);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testPciLogFilter_CCPS() throws Exception {
		String orginal = "blablabla<ns2:accountNumber>4263982640269299</ns2:accountNumber>blablabal<ns2:cvv>837</ns2:cvv>>blablabla";
		String expected = "blablabla<accountNumber>*****</accountNumber>blablabal<cvv>*****</cvv>>blablabla";
		String actual = CreditCardUtil.pciLogFilter(orginal);
		assertEquals(expected, actual);
	}

	@Test
	public void testcalculateAgeAbove65() throws Exception{
		Calendar cal =  Calendar.getInstance();
		cal.set(1949,9,26);
		int actual= CreditCardUtil.calculateAge(cal);
		assertEquals(65, actual);
	}
	
	
	@Test
	public void testcalculateAgeBelo65() throws Exception{
		Calendar cal =  Calendar.getInstance();
		cal.set(1949,10,28);
		int actual= CreditCardUtil.calculateAge(cal);
		assertEquals(64, actual);
	}
	
	
	
	
	
	
	
	
	

	
}
