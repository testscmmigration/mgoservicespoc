package com.moneygram.ccp.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;

public class CardTypeTest extends BaseTest {

	@Test
	public void testIsMastercard() throws Exception {
		assertTrue(CardBrandType.isMastercard("CC-MSTR"));
		assertFalse(CardBrandType.isMastercard("CC-VISA"));
		assertFalse(CardBrandType.isMastercard("ABC"));
	}
	
	@Test
	public void testIsVisa() throws Exception {
		assertTrue(CardBrandType.isVisa("CC-VISA"));
		assertFalse(CardBrandType.isVisa("CC-MSTR"));
		assertFalse(CardBrandType.isVisa("ABC"));
	}
}