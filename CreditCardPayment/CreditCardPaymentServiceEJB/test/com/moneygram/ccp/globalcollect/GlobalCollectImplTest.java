package com.moneygram.ccp.globalcollect;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.globalcollect.domain.GlobalCollectRequest;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectResponse;
import com.moneygram.ccp.globalcollect.request.BaseGlobalCollectRequestMapper;
import com.moneygram.ccp.globalcollect.response.BaseGlobalCollectResponseMapper;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentResponse;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.framework.translator.JAXBXMLMessageTranslator;
import com.moneygram.service.framework.translator.JAXBXMLMessageTranslatorImpl;

public class GlobalCollectImplTest extends BaseTest {
	private GlobalCollectImpl instance;
	private RequestMapperStub requestMapper;
	private RequestMapperGetStatusStub getOrderRequestMapper;
	private ResponseMapperStub responseMapper;
	private JAXBXMLMessageTranslator translator;
	private HttpClient httpClientMock;
	private ProtocolVersion version;
	private HttpParams httpParams;
	private Capture<HttpPost> httpPostCapture;
	private String requestXML; 
	private String responseXML; 
	private String responseXMLMaestro;
	private String requestXMLMaestro;
	private String requestGetStatusXML;
	private String responseGetStatusXML;
	private String requestXMLDoFinishPayment;
	private String responseDoFinishPaymentXML;

	@Before
	public void before() throws IOException {
		requestXML = loadXMLFromClasspath("GlobalCollectImplTest_Request.xml");
		requestXMLMaestro = loadXMLFromClasspath("GlobalCollectImplTest_Request_Maestro.xml");
		requestGetStatusXML = loadXMLFromClasspath("GlobalCollectImplTest_Request_GetStatus.xml");
		responseGetStatusXML = loadXMLFromClasspath("GlobalCollectImplTest_Response_GetStatus.xml");
		responseXML = loadXMLFromClasspath("GlobalCollectImplTest_Response.xml");
		responseXMLMaestro = loadXMLFromClasspath("GlobalCollectImplTest_Response_Maestro.xml");
		requestXMLDoFinishPayment = loadXMLFromClasspath("GlobalCollectImplTest_Request_DoFinishPayment.xml");
		responseDoFinishPaymentXML = loadXMLFromClasspath("GlobalCollectImplTest_Response_DoFinishPayment.xml");
		translator = new JAXBXMLMessageTranslatorImpl();		
		requestMapper = new RequestMapperStub();
		getOrderRequestMapper = new RequestMapperGetStatusStub();
		responseMapper = new ResponseMapperStub();
		httpClientMock = createStrictMock(HttpClient.class);
		httpPostCapture = new Capture<HttpPost>();
		version = new ProtocolVersion("HTTP", 1, 1);
		httpParams = new BasicHttpParams();

		instance = new GlobalCollectImpl() {
			public HttpClient createHttpClient() {
				return httpClientMock;
			}
		};
		instance.setRequestMapper(requestMapper);
		instance.setResponseMapper(responseMapper);
		instance.setTranslator(translator);		
		instance.setSoTimeout(70000);
		instance.setConnectionTimeout(60000);	
		instance.setMaxMessageSize(262144);  // 256K
		instance.setEndpoint("https://ps.gcsip.nl/wdl/wdl");
		instance.setMimeType("text/xml");
		instance.setEncoding("utf-8");
		instance.setUserAgent("GlobalCollectImplTest/1.0");
	}

	@Test
	public void testMarshalRequest() throws Throwable {
       	JAXBContext ctx = JAXBContext.newInstance(requestMapper.getSchemaPackageName());
        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        GlobalCollectRequest request = (GlobalCollectRequest)unmarshaller.unmarshal(new ByteArrayInputStream(requestXML.getBytes()));

		String actual = instance.marshalRequest(request);
	    assertXMLEqual(requestXML, actual, null); 
	}
	
	@Test
	public void testMarshalGetStatusRequest() throws Throwable {
		instance.setRequestMapper(getOrderRequestMapper);
       	JAXBContext ctx = JAXBContext.newInstance(getOrderRequestMapper.getSchemaPackageName());
        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        GlobalCollectRequest request = (GlobalCollectRequest)unmarshaller.unmarshal(new ByteArrayInputStream(requestGetStatusXML.getBytes()));

		String actual = instance.marshalRequest(request);
	    assertXMLEqual(requestGetStatusXML, actual, null); 
	}
	
	@Test
	public void testMarshalRequestMaestro() throws Throwable {
       	JAXBContext ctx = JAXBContext.newInstance(requestMapper.getSchemaPackageName());
        Unmarshaller unmarshaller = ctx.createUnmarshaller();
        GlobalCollectRequest request = (GlobalCollectRequest)unmarshaller.unmarshal(new ByteArrayInputStream(requestXMLMaestro.getBytes()));

		String actual = instance.marshalRequest(request);
	    assertXMLEqual(requestXMLMaestro, actual, null); 
	}
	
	@Test
	public void testUnmarshalResponse() throws Throwable {
		GlobalCollectResponse response = instance.unmarshalResponse(responseXML);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        JAXBContext ctx = JAXBContext.newInstance(responseMapper.getSchemaPackageName());
        Marshaller marshaller = ctx.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
        marshaller.marshal(response, bos);

        String xml = bos.toString();
	    assertXMLEqual(responseXML, xml, null); 
	}
	
	@Test
	public void testUnmarshalResponseMaestro() throws Throwable {
		GlobalCollectResponse response = instance.unmarshalResponse(responseXML);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        JAXBContext ctx = JAXBContext.newInstance(responseMapper.getSchemaPackageName());
        Marshaller marshaller = ctx.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", Boolean.valueOf(true));
        marshaller.marshal(response, bos);

        String xml = bos.toString();
	    assertXMLEqual(responseXML, xml, null); 
	}
	
	@Test
	public void testBuildHttpPost() throws Exception {
		HttpPost httpPost = instance.buildHttpPost("Payload");
		
		Header[] headers = httpPost.getHeaders("User-Agent");
		assertEquals(1, headers.length);
		assertEquals("GlobalCollectImplTest/1.0", headers[0].getValue());
	
		HttpEntity entity = httpPost.getEntity();
		assertEquals("text/xml; charset=utf-8", entity.getContentType().getValue());
		assertEquals("Payload", IOUtils.toString(entity.getContent()));
	}
	
	@Test
	public void testSetHttpParams() throws Exception {
		expect(httpClientMock.getParams()).andReturn(httpParams);
		
		replay(httpClientMock);
		instance.setHttpParams(httpClientMock);
				
		verify(httpClientMock);
		assertEquals(70000, httpParams.getIntParameter(CoreConnectionPNames.SO_TIMEOUT, -1));
		assertEquals(60000, httpParams.getIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, -1));
		assertEquals(262144, httpParams.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1));
	}
	
	@Test
	public void testHandleHttpErrors_NoError() throws Exception {
		instance.handleHttpErrors("Response", new BasicStatusLine(version, 200, "OK"));
	}
	
	@Test(expected=SystemException.class)
	public void testHandleHttpErrors_BadStatusCode() throws Exception {
		instance.handleHttpErrors("Response", new BasicStatusLine(version, 404, "Error"));
	}

	@Test(expected=SystemException.class)
	public void testHandleHttpErrors_EmptyResponse() throws Exception {
		instance.handleHttpErrors("", new BasicStatusLine(version, 200, "OK"));
	}

	@Test
	public void testHttpPost() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 200, "OK");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		httpResponse.setEntity(new StringEntity(responseXML));
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
		String response = instance.httpPost(requestXML);
		
		verify(httpClientMock);
		assertEquals(70000, httpParams.getIntParameter(CoreConnectionPNames.SO_TIMEOUT, -1));
		assertEquals(60000, httpParams.getIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, -1));
		assertEquals(262144, httpParams.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1));
		
		HttpPost httpPost = httpPostCapture.getValue();
		assertEquals("GlobalCollectImplTest/1.0", httpPost.getFirstHeader("User-Agent").getValue());
		assertEquals("https://ps.gcsip.nl/wdl/wdl", httpPost.getURI().toString());
		
		HttpEntity entity = httpPost.getEntity();
		assertEquals("text/xml; charset=utf-8", entity.getContentType().getValue());
		
		String request = IOUtils.toString(entity.getContent());
		assertXMLEqual(requestXML, request, null); 

		assertXMLEqual(responseXML, response, null); 
	}
	
	@Test
	public void testHttpPostGetStatus() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 200, "OK");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		httpResponse.setEntity(new StringEntity(responseGetStatusXML));
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
		String response = instance.httpPost(requestGetStatusXML);
		
		verify(httpClientMock);
		assertEquals(70000, httpParams.getIntParameter(CoreConnectionPNames.SO_TIMEOUT, -1));
		assertEquals(60000, httpParams.getIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, -1));
		assertEquals(262144, httpParams.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1));
		
		HttpPost httpPost = httpPostCapture.getValue();
		assertEquals("GlobalCollectImplTest/1.0", httpPost.getFirstHeader("User-Agent").getValue());
		assertEquals("https://ps.gcsip.nl/wdl/wdl", httpPost.getURI().toString());
		
		HttpEntity entity = httpPost.getEntity();
		assertEquals("text/xml; charset=utf-8", entity.getContentType().getValue());
		
		String request = IOUtils.toString(entity.getContent());
		assertXMLEqual(requestGetStatusXML, request, null); 

		assertXMLEqual(responseGetStatusXML, response, null); 
	}
	
	@Test
	public void testHttpPostMaestro() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 200, "OK");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		httpResponse.setEntity(new StringEntity(requestXMLMaestro));
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
		String response = instance.httpPost(requestXMLMaestro);
		
		verify(httpClientMock);
		assertEquals(70000, httpParams.getIntParameter(CoreConnectionPNames.SO_TIMEOUT, -1));
		assertEquals(60000, httpParams.getIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, -1));
		assertEquals(262144, httpParams.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1));
		
		HttpPost httpPost = httpPostCapture.getValue();
		assertEquals("GlobalCollectImplTest/1.0", httpPost.getFirstHeader("User-Agent").getValue());
		assertEquals("https://ps.gcsip.nl/wdl/wdl", httpPost.getURI().toString());
		
		HttpEntity entity = httpPost.getEntity();
		assertEquals("text/xml; charset=utf-8", entity.getContentType().getValue());
		
		String request = IOUtils.toString(entity.getContent());
		assertXMLEqual(requestXMLMaestro, request, null); 

		assertXMLEqual(responseXMLMaestro, response, null); 
	}
	
	@Test
	public void testHttpDoFinishPayment() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 200, "OK");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		httpResponse.setEntity(new StringEntity(responseDoFinishPaymentXML));
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
		String response = instance.httpPost(requestXMLDoFinishPayment);
		
		verify(httpClientMock);
		assertEquals(70000, httpParams.getIntParameter(CoreConnectionPNames.SO_TIMEOUT, -1));
		assertEquals(60000, httpParams.getIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, -1));
		assertEquals(262144, httpParams.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1));
		
		HttpPost httpPost = httpPostCapture.getValue();
		assertEquals("GlobalCollectImplTest/1.0", httpPost.getFirstHeader("User-Agent").getValue());
		assertEquals("https://ps.gcsip.nl/wdl/wdl", httpPost.getURI().toString());
		
		HttpEntity entity = httpPost.getEntity();
		assertEquals("text/xml; charset=utf-8", entity.getContentType().getValue());
		
		String request = IOUtils.toString(entity.getContent());
		assertXMLEqual(requestXMLDoFinishPayment, request, null); 

		assertXMLEqual(responseDoFinishPaymentXML, response, null); 
	}
	
	@Test
	public void testHttpPost_HttpError() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 404, "Some Error");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
	
		try {
			instance.httpPost(requestXML);
			fail("Expected SystemException");
		} catch (SystemException e) {
			assertEquals("Received error HTTP status code: HTTP/1.1 404 Some Error", e.getMessage());
		}
	}
	
	@Test
	public void testHttpPost_EmptyPayload() throws Throwable {
		StatusLine statusline = new BasicStatusLine(version, 200, "OK");
		HttpResponse httpResponse = new BasicHttpResponse(statusline);
		
		expect(httpClientMock.getParams()).andReturn(httpParams);
		expect(httpClientMock.execute(capture(httpPostCapture))).andReturn(httpResponse);
		
		replay(httpClientMock);
	
		try {
			instance.httpPost(requestXML);
			fail("Expected SystemException");
		} catch (SystemException e) {
			assertEquals("Received empty HTTP response.", e.getMessage());
		}
	}

	@Test
	public void testHttpPost_NullRequest() throws Throwable {
		expect(httpClientMock.getParams()).andReturn(httpParams);
		
		replay(httpClientMock);
	
		try {
			instance.httpPost(null);
			fail("Expected SystemException");
		} catch (SystemException e) {
			assertEquals("Unable to send request to global collect.", e.getMessage());
		}
	}

	private String loadXMLFromClasspath(String name) throws IOException {
		URL url = getClass().getResource(name);
		return FileUtils.readFileToString(new File("C:\\"+name));
	}

	@SuppressWarnings("rawtypes")
	public class RequestMapperStub extends BaseGlobalCollectRequestMapper {
		public GlobalCollectRequest buildRequest(BaseCreditCardPaymentRequest baseRequest) throws Exception {
			return new com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.XML();
		}

		public String getSchemaPackageName() {
			return "com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1";
		}

		public String getGlobalCollectApiName() {
			return "Test";
		}
	}
	
	@SuppressWarnings("rawtypes")
	public class RequestMapperGetStatusStub extends BaseGlobalCollectRequestMapper {
		public GlobalCollectRequest buildRequest(BaseCreditCardPaymentRequest baseRequest) throws Exception {
			return new com.moneygram.service.globalcollect.getOrderStatusRequest_v2.XML();
		}
		public String getSchemaPackageName() {
			return "com.moneygram.service.globalcollect.getOrderStatusRequest_v2";
		}
		public String getGlobalCollectApiName() {
			return "Test";
		}
	}
	
	@SuppressWarnings("rawtypes")
	public class ResponseMapperStub extends BaseGlobalCollectResponseMapper {
		public BaseCreditCardPaymentResponse buildResponse(GlobalCollectResponse response) {
			return new AuthorizationResponse();
		}

		public BaseCreditCardPaymentResponse buildErrorResponse(Throwable t) throws CommandException {
			return new AuthorizationResponse();
		}

		public String getSchemaPackageName() {
			return "com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1";
		}
	}
}
