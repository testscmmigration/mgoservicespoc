package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Meta;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.XML;

public class GlobalCollectSetPaymentRequestMapperTest extends BaseTest {
	private GlobalCollectSetPaymentRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectSetPaymentRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest() throws Exception {
		PostTransactionRequest request = RequestObjectMother.buildPostTransactionRequest();
		request.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount(null, 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.SET_PAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(123456L, payment.getORDERID());
		assertEquals(1L, payment.getEFFORTID());
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals(null, payment.getCURRENCYCODE());
	}
}
