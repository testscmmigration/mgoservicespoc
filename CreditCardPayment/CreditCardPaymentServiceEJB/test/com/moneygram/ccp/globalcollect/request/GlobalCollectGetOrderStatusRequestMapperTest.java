package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Action;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Meta;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Order;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.XML;

public class GlobalCollectGetOrderStatusRequestMapperTest extends BaseTest {
	private GlobalCollectGetOrderStatusRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectGetOrderStatusRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest_WithCVV() throws Exception {
		StatusRequest request = RequestObjectMother.buildStatusRequest();
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456789L, null));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.GET_ORDERSTATUS, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("2.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertEquals(123456789L, order.getORDERID());
	}
}
