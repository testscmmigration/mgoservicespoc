package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectRequest;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Meta;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Order;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Request;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.XML;

public class BaseGlobalCollectRequestMapperTest extends BaseTest {
	@SuppressWarnings("rawtypes")
	private BaseGlobalCollectRequestMapper instance;
	
	@Before
	public void before() {
		instance = new MapperStub();
	}
	
	@Test
	public void testCreateXML() throws Exception {
		GlobalCollectBean bean = instance.createXML("INSERT_ORDERWITHPAYMENT", "MerchantId", "v1");
		assertTrue(bean instanceof XML);
		
		XML xml = (XML)bean;
		Request request = xml.getREQUEST();
		assertTrue(request instanceof Request);
		assertTrue(request.getACTION() instanceof Action);
		assertTrue(request.getMETA() instanceof Meta);
		assertTrue(request.getPARAMS() instanceof Params);

		Meta meta = request.getMETA();
		assertEquals("MerchantId", meta.getMERCHANTID());
		assertEquals(instance.getIPAddress(), meta.getIPADDRESS());
		assertEquals("1.0", meta.getVERSION().value());
	}

	@Test
	public void testCreateRequest() throws Exception {
		GlobalCollectBean bean = instance.createRequest("INSERT_ORDERWITHPAYMENT", "MerchantId", "v1");
		assertTrue(bean instanceof Request);

		Request request = (Request)bean;
		assertTrue(request.getACTION() instanceof Action);
		assertTrue(request.getMETA() instanceof Meta);
		assertTrue(request.getPARAMS() instanceof Params);

		Meta meta = request.getMETA();
		assertEquals("MerchantId", meta.getMERCHANTID());
		assertEquals(instance.getIPAddress(), meta.getIPADDRESS());
		assertEquals("1.0", meta.getVERSION().value());
	}
	
	@Test
	public void testCreateMeta() throws Exception {
		GlobalCollectBean bean = instance.createMeta("MerchantId", "v1");
		assertTrue(bean instanceof Meta);

		Meta meta = (Meta)bean;
		assertEquals("MerchantId", meta.getMERCHANTID());
		assertEquals(instance.getIPAddress(), meta.getIPADDRESS());
		assertEquals("1.0", meta.getVERSION().value());
	}
	
	@Test
	public void testCreateParams() throws Exception {
		GlobalCollectBean bean = instance.createParams();
		assertTrue(bean instanceof Params);
	}

	@Test
	public void testAddOrder() throws Exception {
		Params params = new Params();
		
		GlobalCollectBean order = instance.addOrder(params);
		assertTrue(order instanceof Order);
	}

	@Test
	public void testAddPayment() throws Exception {
		Params params = new Params();
		
		GlobalCollectBean payment = instance.addPayment(params);
		assertTrue(payment instanceof Payment);
	}

	@Test
	public void testCreateBean() throws Exception {
		GlobalCollectBean bean = instance.createBean("Params");
		assertTrue(bean instanceof Params);
	}
	
	@Test
	@SuppressWarnings("rawtypes")
	public void testCreateAction() throws Exception {
		Enum bean = instance.createAction("INSERT_ORDERWITHPAYMENT");
		assertEquals(Action.INSERT_ORDERWITHPAYMENT, bean);
	}

	@Test
	@SuppressWarnings("rawtypes")
	public void testCreateVersion() throws Exception {
		Enum bean = instance.createVersion("v1");
		assertEquals(Version.v1, bean);
	}

	@Test
	public void testAssertType() throws Exception {
		Params params = new Params();
		instance.assertType("Params", params);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAssertType_AssertFailed() throws Exception {
		Params params = new Params();
		instance.assertType("Payment", params);
	}

	@Test
	public void testConvertExpireDate_2DigitYear() throws Exception {
		String actual = instance.convertExpireDate(12, 99);
		assertEquals("1299", actual);
	}
	
	@Test
	public void testConvertExpireDate_4DigitYear() throws Exception {
		String actual = instance.convertExpireDate(12, 2099);
		assertEquals("1299", actual);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertExpireDate_BadYear_Low() throws Exception {
		instance.convertExpireDate(12, 200);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testConvertExpireDate_BadYear_High() throws Exception {
		instance.convertExpireDate(12, 3000);
	}
	
	@Test
	public void testConvertCardType_MasterCard() throws Exception {
		int actual = instance.convertCardType(CardBrandType.MASTERCARD.getName(),null);
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_MASTERCARD, actual);
	}

	@Test
	public void testConvertCardType_Visa() throws Exception {
		int actual = instance.convertCardType(CardBrandType.VISA.getName(),null);
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, actual);
	}

	@Test
	public void testConvertCardType_InvalidType() throws Exception {
		Integer actual = instance.convertCardType("Invalid",null);
		assertNull(actual);
	}
	
	public void testConvertCardType_MasterCard_DEBIT() throws Exception {
		int actual = instance.convertCardType(CardBrandType.MASTERCARD.getName(),CardType.DEBIT);
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_MASTERCARD_DEBIT, actual);
	}

	@Test
	public void testConvertCardType_Visa_DEBIT() throws Exception {
		int actual = instance.convertCardType(CardBrandType.VISA.getName(),CardType.DEBIT);
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA_DEBIT, actual);
	}

	@Test
	public void testConvertCardType_InvalidType_DEBIT() throws Exception {
		Integer actual = instance.convertCardType("Invalid",CardType.DEBIT);
		assertNull(actual);
	}

	@SuppressWarnings("rawtypes")
	public class MapperStub extends BaseGlobalCollectRequestMapper {
		public GlobalCollectRequest buildRequest(BaseCreditCardPaymentRequest baseRequest) throws Exception {
			return new com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.XML();
		}

		public String getSchemaPackageName() {
			return "com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1";
		}

		public String getGlobalCollectApiName() {
			return "Test";
		}
	}
}
