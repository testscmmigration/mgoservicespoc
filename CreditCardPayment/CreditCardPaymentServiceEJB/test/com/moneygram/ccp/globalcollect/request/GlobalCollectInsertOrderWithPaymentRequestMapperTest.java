package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Meta;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Order;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.XML;

public class GlobalCollectInsertOrderWithPaymentRequestMapperTest extends BaseTest {
	private GlobalCollectInsertOrderWithPaymentRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectInsertOrderWithPaymentRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest_WithCVV() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, "123"));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals("123", payment.getCVV());
		assertEquals("1", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("Main Street", payment.getSTREET());
		assertEquals("123",payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}

	@Test
	public void testBuildRequest_WithoutCVV() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("Main Street", payment.getSTREET());
		assertEquals("123",payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	@Test
	public void testBuildRequest_WithBuildingNumber() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("Main Street", payment.getSTREET());
		assertEquals("123",payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	
	public void testBuildRequest_WithBuildingNumberLongStreet() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		request.setStreet1("123 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", payment.getSTREET());
		assertEquals("123",payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	
	@Test
	public void testBuildRequest_WithoutBuildingNumber() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		request.setStreet1("Main Street");
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("Main Street", payment.getSTREET());
		assertEquals(null,payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	
	@Test
	public void testBuildRequest_WithoutBuildingNumberLongStreet() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		request.setStreet1("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", payment.getSTREET());
		assertEquals(null,payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	
	@Test
	public void testBuildRequest_WithoutBuildingNumberNoSpace() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		request.setStreet1("123Main Street");
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("123Main Street", payment.getSTREET());
		assertEquals(null,payment.getHOUSENUMBER());
		assertEquals("55011", payment.getZIP());
	}
	
	@Test
	public void testBuildRequest_WithoutBuildingNumberonlyNumber() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		request.setStreet1("123");
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.INSERT_ORDERWITHPAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Order order = gcXml.getREQUEST().getPARAMS().getORDER();
		assertNotNull(order.getMERCHANTREFERENCE());
		assertEquals(123456L, order.getORDERID());
		assertEquals(GlobalCollectInsertOrderWithPaymentRequestMapper.ORDER_TYPE, order.getORDERTYPE().intValue());
		assertEquals(50099L, order.getAMOUNT());
		assertEquals("USD", order.getCURRENCYCODE());
		assertEquals("en", order.getLANGUAGECODE());
		assertEquals("US", order.getCOUNTRYCODE());
		assertEquals("999998", order.getCUSTOMERID());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertEquals(PaymentProduct.PAYMENT_PRODUCT_VISA, payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("123", payment.getSTREET());
		assertEquals("55011", payment.getZIP());
	}		
	
}
