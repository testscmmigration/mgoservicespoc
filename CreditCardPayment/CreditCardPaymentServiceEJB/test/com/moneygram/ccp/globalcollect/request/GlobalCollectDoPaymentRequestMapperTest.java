package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Meta;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.XML;

public class GlobalCollectDoPaymentRequestMapperTest extends BaseTest {
	private GlobalCollectDoPaymentRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectDoPaymentRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest_WithCVV() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, "123"));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.DO_PAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertNotNull(payment.getMERCHANTREFERENCE());
		assertEquals(123456L, payment.getORDERID());
		assertEquals("1", payment.getEFFORTID().toString());
		assertNull(payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals("123", payment.getCVV());
		assertEquals("1", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("123 Main Street", payment.getSTREET());
		assertEquals("55011", payment.getZIP());
	}

	@Test
	public void testBuildRequest_WithoutCVV() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", true);
		request.setCard(RequestObjectMother.buildCard("John X Doe", "123412341234", CardBrandType.VISA, 2, 2011, null));
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.DO_PAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertNotNull(payment.getMERCHANTREFERENCE());
		assertEquals(123456L, payment.getORDERID());
		assertEquals("1", payment.getEFFORTID().toString());
		assertNull(payment.getPAYMENTPRODUCTID());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals("0211", payment.getEXPIRYDATE());
		assertEquals("123412341234", payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("1", payment.getAVSINDICATOR().toString());
		assertEquals("123 Main Street", payment.getSTREET());
		assertEquals("55011", payment.getZIP());
	}

	@Test
	public void testBuildRequest_DontPerformCVVAVS() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", false);
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.DO_PAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertNotNull(payment.getMERCHANTREFERENCE());
		assertEquals(123456L, payment.getORDERID());
		assertEquals("1", payment.getEFFORTID().toString());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals(null, payment.getEXPIRYDATE());
		assertEquals(null, payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("0", payment.getAVSINDICATOR().toString());
		assertEquals(null, payment.getSTREET());
		assertEquals(null, payment.getZIP());
	}
	
	
	public void testBuildRequest_DontPerformCVVAVSLongStreet() throws Exception {
		AuthorizationRequest request = RequestObjectMother.buildAuthorizationRequest("USA", "US", false);
		request.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 500.99f));
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(123456L, 1L));
		request.setStreet1("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.DO_PAYMENT, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		Payment payment = gcXml.getREQUEST().getPARAMS().getPAYMENT();
		assertNotNull(payment.getMERCHANTREFERENCE());
		assertEquals(123456L, payment.getORDERID());
		assertEquals("1", payment.getEFFORTID().toString());
		assertEquals(50099L, payment.getAMOUNT());
		assertEquals("USD", payment.getCURRENCYCODE());
		assertEquals("en", payment.getLANGUAGECODE());
		assertEquals("US", payment.getCOUNTRYCODE());
		assertEquals(null, payment.getEXPIRYDATE());
		assertEquals(null, payment.getCREDITCARDNUMBER());
		assertEquals(null, payment.getCVV());
		assertEquals("0", payment.getCVVINDICATOR().toString());
		assertEquals("0", payment.getAVSINDICATOR().toString());
		assertEquals("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", payment.getSTREET());
		assertEquals(null, payment.getZIP());
	}	
}
