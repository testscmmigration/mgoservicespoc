package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.Action;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.General;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.Meta;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.XML;

public class GlobalCollectDoBinLookupRequestMapperTest extends BaseTest {
	private GlobalCollectDoBinLookupRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectDoBinLookupRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest() throws Exception {
		String merchantID = "ABC";
		String accountNumber = "1234567890";
		BinLookupRequest request = RequestObjectMother.buildDoBinlookupRequest(accountNumber, merchantID);
		
		
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.DO_BINLOOKUP, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(merchantID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
		
		General general = gcXml.getREQUEST().getPARAMS().getGENERAL();
		assertEquals(accountNumber,general.getBIN());
	}
}
