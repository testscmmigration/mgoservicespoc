package com.moneygram.ccp.globalcollect.request;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.Action;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.Meta;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.XML;

public class GlobalCollectTestConnectionRequestMapperTest extends BaseTest {
	private GlobalCollectTestConnectionRequestMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectTestConnectionRequestMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildRequest() throws Exception {
		HealthCheckRequest request = RequestObjectMother.buildHealthCheckRequest();
	
		XML gcXml = instance.buildRequest(request);
		
		Action action = gcXml.getREQUEST().getACTION();
		assertEquals (Action.TEST_CONNECTION, action);
		
		Meta gcMeta = gcXml.getREQUEST().getMETA();
		assertEquals(instance.getIPAddress(), gcMeta.getIPADDRESS());
		assertEquals(RequestObjectMother.MERCHANT_ID, gcMeta.getMERCHANTID());
		assertEquals("1.0", gcMeta.getVERSION().value());
	}
}
