package com.moneygram.ccp.globalcollect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.http.conn.util.InetAddressUtils;
import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;

public class BaseGlobalCollectMapperTest extends BaseTest {
	private BaseGlobalCollectMapper instance;

	@Before
	public void before() {
		instance = new MapperStub();
	}
	
	@Test
	public void testGetIPAddress() throws Exception {
		String actual = instance.getIPAddress();
		assertTrue(InetAddressUtils.isIPv4Address(actual));
	}
	
	@Test
	public void testAmountToLong() throws Exception {
		assertEquals(1234567L, instance.amountToLong(12345.67f).longValue());
	}
	
	@Test
	public void testLongToAmount() throws Exception {
		assertEquals(0.0, instance.longToAmount(null), 0.0);
		assertEquals(12345.67f, instance.longToAmount(1234567L), 0.0);
	}
	
	public class MapperStub extends BaseGlobalCollectMapper {
		public String getSchemaPackageName() {
			return null;
		}
	}
}
