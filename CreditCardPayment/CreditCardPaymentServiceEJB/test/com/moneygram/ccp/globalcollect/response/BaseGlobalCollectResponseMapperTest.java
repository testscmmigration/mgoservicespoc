package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectResponse;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.common_v1.Header;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Request;
import com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Response;
import com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Row;
import com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.XML;

public class BaseGlobalCollectResponseMapperTest extends BaseTest {
	@SuppressWarnings("rawtypes")
	private BaseGlobalCollectResponseMapper instance;
	
	@Before
	@SuppressWarnings("unchecked")
	public void before() throws Exception {
		instance = new MapperStub();
		
		Set<String> canRetryCodes = instance.getCanRetryCodes();
		Collections.addAll(canRetryCodes, "100", "200", "300");
	}
	
	@Test
	public void testBuildHeader() throws Exception {
		Header header = instance.buildHeader();
		
		assertNotNull(header);
		assertNotNull(header.getProcessingInstruction());
		assertFalse(header.getProcessingInstruction().isRollbackTransaction());
	}

	@Test
	public void testConvertResult_Accept() throws Exception {
		assertEquals(Decision.ACCEPT, instance.convertResult("OK"));
		assertEquals(Decision.ACCEPT, instance.convertResult("Ok"));
		assertEquals(Decision.ACCEPT, instance.convertResult("ok"));
	}
	
	@Test
	public void testConvertResult_Reject() throws Exception {
		assertEquals(Decision.REJECT, instance.convertResult("NOK"));
		assertEquals(Decision.REJECT, instance.convertResult("Nok"));
		assertEquals(Decision.REJECT, instance.convertResult("nok"));
	}
	
	@Test
	public void testConvertResult_InvalidInput() throws Exception {
		assertEquals(Decision.REVIEW, instance.convertResult("Invalid"));
	}
	
	@Test
	public void testCanRetry_CanRetry() throws Exception {
		assertTrue(instance.canRetry("100"));
		assertTrue(instance.canRetry("200"));
	}
	
	@Test
	public void testCanRetry_CantRetry() throws Exception {
		assertFalse(instance.canRetry("101"));
		assertFalse(instance.canRetry("201"));
	}
	
	@Test
	public void testDidCVVPass() throws Exception {
		assertTrue(instance.didCVVPass("M"));
		assertFalse(instance.didCVVPass("N"));
		assertFalse(instance.didCVVPass("0"));
		assertFalse(instance.didCVVPass(" "));
		assertFalse(instance.didCVVPass(""));
		assertFalse(instance.didCVVPass(null));
	}
	
	@Test
	public void testConvertPaymentProduct() throws Exception {
		assertEquals(CardBrandType.MASTERCARD, instance.convertPaymentProduct(PaymentProduct.PAYMENT_PRODUCT_MASTERCARD));
		assertEquals(CardBrandType.VISA, instance.convertPaymentProduct(PaymentProduct.PAYMENT_PRODUCT_VISA));
		assertNull(instance.convertPaymentProduct(999));
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testMapErrors_NoErrors() throws Exception {
		XML xgcXml = createGCXml("OK", "M", "M");
		
		CCPErrors errors = instance.mapErrors(xgcXml);
		assertNull(errors);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testMapErrors_Errors() throws Exception {
		XML gcXml = createGCXml("NOK", "N", "N");
		List<com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Error> gcErrors = gcXml.getREQUEST().getRESPONSE().getERRORS();
		gcErrors.add(createGCError(100, "Message 1"));
		gcErrors.add(createGCError(110, "Message 2"));

		CCPErrors errors = instance.mapErrors(gcXml);
		
		List<CCPError> errorList = errors.getErrors();
		assertEquals(2, errorList.size());
		
		CCPError error1 = errorList.get(0);
		assertEquals("100", error1.getReasonCode());
		assertEquals("Message 1", error1.getErrorMessage());
		assertTrue(error1.isCanRetry());

		CCPError error2 = errorList.get(1);
		assertEquals("110", error2.getReasonCode());
		assertEquals("Message 2", error2.getErrorMessage());
		assertFalse(error2.isCanRetry());
	}

	private com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Error createGCError(int code, String message) {
		com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Error error = 
				new com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1.Error();
		error.setCODE(code);
		error.setMESSAGE(message);
		return error;
	}
	
	private XML createGCXml(String result, String avsResult, String ccvResult) {
		Row row = new Row();
		row.setAVSRESULT(avsResult);
		row.setCVVRESULT(ccvResult);
				
		Response response = new Response();
		response.setRESULT(result);
		response.setROW(row);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
	
	@SuppressWarnings("rawtypes")
	public class MapperStub extends BaseGlobalCollectResponseMapper {
		public BaseCreditCardPaymentResponse buildResponse(GlobalCollectResponse response) {
			return new AuthorizationResponse();
		}

		public BaseCreditCardPaymentResponse buildErrorResponse(Throwable t) throws CommandException {
			return new AuthorizationResponse();
		}

		public String getSchemaPackageName() {
			return "com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1";
		}
	}
}
