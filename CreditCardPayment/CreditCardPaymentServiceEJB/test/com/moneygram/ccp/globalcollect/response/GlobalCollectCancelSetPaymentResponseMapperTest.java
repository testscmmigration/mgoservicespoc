package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;
import com.moneygram.common_v1.Header;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Request;
import com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Response;
import com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.XML;

public class GlobalCollectCancelSetPaymentResponseMapperTest extends BaseTest {
	private GlobalCollectCancelSetPaymentResponseMapper instance;
	
	@Before
	public void before() throws Exception {
		instance = new GlobalCollectCancelSetPaymentResponseMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildResponse() throws Exception {
		XML gcXml = createGCXml("OK");
		
		RefundTransactionResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.ACCEPT, response.getDecision());
		assertNull(response.getErrors());
	}

	@Test
	public void testBuildResponse_Errors() throws Exception {
		XML gcXml = createGCXml("NOK");
		List<com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Error> gcErrors = gcXml.getREQUEST().getRESPONSE().getERRORS();
		gcErrors.add(createGCError(100, "Message 1"));
		gcErrors.add(createGCError(200, "Message 2"));
		
		RefundTransactionResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.REJECT, response.getDecision());
		
		List<CCPError> errors = response.getErrors().getErrors();
		assertEquals(2, errors.size());
		
		CCPError error1 = errors.get(0);
		assertEquals("100", error1.getReasonCode());
		assertEquals("Message 1", error1.getErrorMessage());

		CCPError error2 = errors.get(1);
		assertEquals("200", error2.getReasonCode());
		assertEquals("Message 2", error2.getErrorMessage());
	}
	
	@Test
	public void testBuildErrorResponse() throws Exception {
		Exception e = new NullPointerException("Oops.");
		e.fillInStackTrace();
		
		try {
			instance.buildErrorResponse(e);
			fail("Expected SystemException");
		} catch (SystemException e2) {
			assertEquals(e, e2.getCause());
		}
	}

	private com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Error createGCError(int code, String message) {
		com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Error error = 
				new com.moneygram.service.globalcollect.cancelSetPaymentResponse_v1.Error();
		error.setCODE(code);
		error.setMESSAGE(message);
		return error;
	}
	
	private XML createGCXml(String result) {
		Response response = new Response();
		response.setRESULT(result);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
}
