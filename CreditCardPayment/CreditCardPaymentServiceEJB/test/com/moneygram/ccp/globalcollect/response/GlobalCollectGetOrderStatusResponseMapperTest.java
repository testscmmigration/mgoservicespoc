package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.common_v1.Header;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectStatusResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Request;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Response;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Status;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.XML;

public class GlobalCollectGetOrderStatusResponseMapperTest extends BaseTest {
	private GlobalCollectGetOrderStatusResponseMapper instance;
	
	@Before
	public void before() throws Exception {
		instance = new GlobalCollectGetOrderStatusResponseMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildResponse() throws Exception {
		XML gcXml = createGCXml("OK");
		
		StatusResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		CurrencyAmount amount = response.getAmount();
		assertEquals(10.99f, amount.getAmount().floatValue(), 0.0f);
		assertEquals("USD", amount.getCurrency());
		
		GlobalCollectStatusResponse globalCollect = response.getGlobalCollect();
		assertEquals(3, globalCollect.getEffortId().intValue());
		assertEquals(1, globalCollect.getAttemptId().intValue());
		assertEquals("PaymentRef", globalCollect.getPaymentReference());
		assertEquals(100, (long)globalCollect.getStatusId());
		
		assertNull(response.getErrors());
	}

	@Test
	public void testBuildResponse_Errors() throws Exception {
		XML gcXml = createGCXml("NOK");
		List<com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Error> gcErrors = gcXml.getREQUEST().getRESPONSE().getERRORS();
		gcErrors.add(createGCError(100, "Message 1"));
		gcErrors.add(createGCError(200, "Message 2"));
		
		StatusResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		CurrencyAmount amount = response.getAmount();
		assertEquals(10.99f, amount.getAmount().floatValue(), 0.0f);
		assertEquals("USD", amount.getCurrency());
		
		GlobalCollectStatusResponse globalCollect = response.getGlobalCollect();
		assertEquals(3, globalCollect.getEffortId().intValue());
		assertEquals(1, globalCollect.getAttemptId().intValue());
		assertEquals("PaymentRef", globalCollect.getPaymentReference());
		assertEquals(100, (long)globalCollect.getStatusId());
		
		List<CCPError> errors = response.getErrors().getErrors();
		assertEquals(2, errors.size());
		
		CCPError error1 = errors.get(0);
		assertEquals("100", error1.getReasonCode());
		assertEquals("Message 1", error1.getErrorMessage());

		CCPError error2 = errors.get(1);
		assertEquals("200", error2.getReasonCode());
		assertEquals("Message 2", error2.getErrorMessage());
	}

	@Test
	public void testBuildErrorResponse() throws Exception {
		Exception e = new NullPointerException("Oops.");
		e.fillInStackTrace();
		
		try {
			instance.buildErrorResponse(e);
			fail("Expected SystemException");
		} catch (SystemException e2) {
			assertEquals(e, e2.getCause());
		}
	}

	private com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Error createGCError(int code, String message) {
		com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Error error = 
				new com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Error();
		error.setCODE(code);
		error.setMESSAGE(message);
		return error;
	}
	
	private XML createGCXml(String result) {
		Status status = new Status();
		status.setAMOUNT(1099L);
		status.setATTEMPTID(1L);
		status.setCURRENCYCODE("USD");
		status.setEFFORTID(3L);
		status.setMERCHANTID(RequestObjectMother.MERCHANT_ID);
		status.setMERCHANTREFERENCE("MerchantRef");
		status.setORDERID(12345L);
		status.setPAYMENTPRODUCTID(PaymentProduct.PAYMENT_PRODUCT_VISA);
		status.setPAYMENTREFERENCE("PaymentRef");
		status.setSTATUSID(100);
		status.setTOTALAMOUNTPAID(999900L);
		status.setTOTALAMOUNTREFUNDED(499L);
		
		Response response = new Response();
		response.setRESULT(result);
		response.setSTATUS(status);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
}
