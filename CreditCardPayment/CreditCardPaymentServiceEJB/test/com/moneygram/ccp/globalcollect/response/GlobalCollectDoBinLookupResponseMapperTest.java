package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ibm.ObjectQuery.eval.Row;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.common_v1.Header;
import com.moneygram.service.creditcardpaymentservice_v1.BinlookupResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.doBinLookupResponse_v1.Request;
import com.moneygram.service.globalcollect.doBinLookupResponse_v1.Response;
import com.moneygram.service.globalcollect.doBinLookupResponse_v1.XML;

public class GlobalCollectDoBinLookupResponseMapperTest extends BaseTest {
	private GlobalCollectDoBinLookupResponseMapper instance;
	
	@Before
	public void before() throws Exception {
		instance = new GlobalCollectDoBinLookupResponseMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildResponse() throws Exception {
		XML gcXml = createGCXml("OK","US", new Integer(1));
		
		BinlookupResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.ACCEPT, response.getDecision());
		assertNull(response.getErrors());
		assertEquals("US", response.getCountryCode());
		assertEquals(CardType.CREDIT, response.getCardType());
	}
	
	public void testBuildResponseDebit() throws Exception {
		XML gcXml = createGCXml("OK","US", new Integer(119));
		
		BinlookupResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.ACCEPT, response.getDecision());
		assertNull(response.getErrors());
		assertEquals("US", response.getCountryCode());
		assertEquals(CardType.DEBIT, response.getCardType());
	}	
	
	public void testBuildResponseUnknown() throws Exception {
		XML gcXml = createGCXml("OK","US", new Integer(5));
		
		BinlookupResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.ACCEPT, response.getDecision());
		assertNull(response.getErrors());
		assertEquals("US", response.getCountryCode());
		assertEquals(CardType.UNKNOWN, response.getCardType());
	}		

	@Test
	public void testBuildResponse_Errors() throws Exception {
		XML gcXml = createGCXml("NOK",null,null);
		List<com.moneygram.service.globalcollect.doBinLookupResponse_v1.Error> gcErrors = gcXml.getREQUEST().getRESPONSE().getERRORS();
		gcErrors.add(createGCError(100, "Message 1"));
		gcErrors.add(createGCError(200, "Message 2"));
		
		BinlookupResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.REJECT, response.getDecision());
		
		List<CCPError> errors = response.getErrors().getErrors();
		assertEquals(2, errors.size());
		
		CCPError error1 = errors.get(0);
		assertEquals("100", error1.getReasonCode());
		assertEquals("Message 1", error1.getErrorMessage());

		CCPError error2 = errors.get(1);
		assertEquals("200", error2.getReasonCode());
		assertEquals("Message 2", error2.getErrorMessage());
	}

	@Test
	public void testBuildErrorResponse() throws Exception {
		Exception e = new NullPointerException("Oops.");
		e.fillInStackTrace();
		
		try {
			instance.buildErrorResponse(e);
			fail("Expected SystemException");
		} catch (SystemException e2) {
			assertEquals(e, e2.getCause());
		}
	}

	private com.moneygram.service.globalcollect.doBinLookupResponse_v1.Error createGCError(int code, String message) {
		com.moneygram.service.globalcollect.doBinLookupResponse_v1.Error error = 
				new com.moneygram.service.globalcollect.doBinLookupResponse_v1.Error();
		error.setCODE(code);
		error.setMESSAGE(message);
		return error;
	}
	
	private XML createGCXml(String result, String countryCode, Integer paymentProductId) {
		Response response = new Response();
		response.setRESULT(result);
		com.moneygram.service.globalcollect.doBinLookupResponse_v1.Row row = new com.moneygram.service.globalcollect.doBinLookupResponse_v1.Row();
		row.setCOUNTRYCODE(countryCode);
		row.setPAYMENTPRODUCTID(paymentProductId);
		response.setROW(row);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
}
