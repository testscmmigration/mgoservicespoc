package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.SocketTimeoutException;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.common_v1.Header;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointStatus;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointVendor;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckResponse;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckStatus;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.testConnectionResponse_v1.Request;
import com.moneygram.service.globalcollect.testConnectionResponse_v1.Response;
import com.moneygram.service.globalcollect.testConnectionResponse_v1.XML;

public class GlobalCollectTestConnectionResponseMapperTest extends BaseTest {
	private GlobalCollectTestConnectionResponseMapper instance;
	
	@Before
	public void before() {
		instance = new GlobalCollectTestConnectionResponseMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildResponse_OK() throws Exception {
		XML gcXml = createGCXml("OK");
		
		HealthCheckResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		HealthCheckEndpointStatus endpointStatuse = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, endpointStatuse.getVendor());
		assertEquals("EndPoint", endpointStatuse.getEndpoint());
		assertEquals(HealthCheckStatus.SUCCESS, endpointStatuse.getStatus());

		assertEquals(Decision.ACCEPT, response.getDecision());
		assertNull(response.getErrors());
	}

	@Test
	public void testBuildResponse_NOK() throws Exception {
		XML gcXml = createGCXml("NOK");
		
		HealthCheckResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		HealthCheckEndpointStatus endpointStatuse = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, endpointStatuse.getVendor());
		assertEquals("EndPoint", endpointStatuse.getEndpoint());
		assertEquals(HealthCheckStatus.ERROR, endpointStatuse.getStatus());

		assertEquals(Decision.REJECT, response.getDecision());
		assertNull(response.getErrors());
	}

	@Test
	public void testBuildErrorResponse_Exception() throws Exception {
		Exception e = new NullPointerException("Oops.");
		e.fillInStackTrace();
		
		HealthCheckResponse response = instance.buildErrorResponse(e);

		Header header = response.getHeader();
		assertNotNull(header);
		
		HealthCheckEndpointStatus endpointStatuse = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, endpointStatuse.getVendor());
		assertEquals("EndPoint", endpointStatuse.getEndpoint());
		assertEquals(HealthCheckStatus.ERROR, endpointStatuse.getStatus());

		assertEquals(Decision.REJECT, response.getDecision());
		assertNull(response.getErrors());
	}
	
	@Test
	public void testBuildErrorResponse_SocketTimeoutException() throws Exception {
		Exception e = new SocketTimeoutException("Oops.");
		e.fillInStackTrace();
		
		HealthCheckResponse response = instance.buildErrorResponse(e);

		Header header = response.getHeader();
		assertNotNull(header);
		
		HealthCheckEndpointStatus endpointStatuse = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, endpointStatuse.getVendor());
		assertEquals("EndPoint", endpointStatuse.getEndpoint());
		assertEquals(HealthCheckStatus.TIMEOUT, endpointStatuse.getStatus());

		assertEquals(Decision.REJECT, response.getDecision());
		assertNull(response.getErrors());
	}
	
	@Test
	public void testBuildErrorResponse_SocketTimeoutExceptionWrappedInCommandException() throws Exception {
		Exception e = new SocketTimeoutException("Oops.");
		e.fillInStackTrace();
		
		SystemException e2 = new SystemException("100", "Error", e);
		e2.fillInStackTrace();
		
		HealthCheckResponse response = instance.buildErrorResponse(e2);

		Header header = response.getHeader();
		assertNotNull(header);
		
		HealthCheckEndpointStatus endpointStatuse = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, endpointStatuse.getVendor());
		assertEquals("EndPoint", endpointStatuse.getEndpoint());
		assertEquals(HealthCheckStatus.TIMEOUT, endpointStatuse.getStatus());

		assertEquals(Decision.REJECT, response.getDecision());
		assertNull(response.getErrors());
	}
		
	private XML createGCXml(String result) {
		Response response = new Response();
		response.setRESULT(result);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
}
