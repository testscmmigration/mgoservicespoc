package com.moneygram.ccp.globalcollect.response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.test.BaseTest;
import com.moneygram.common_v1.Header;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.Request;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.Response;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.Row;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.XML;

public class GlobalCollectDoPaymentResponseMapperTest extends BaseTest {
	private GlobalCollectDoPaymentResponseMapper instance;
	
	@Before
	public void before() throws Exception {
		instance = new GlobalCollectDoPaymentResponseMapper();
		instance.setEndpoint("EndPoint");
	}
	
	@Test
	public void testBuildResponse_AVSCCVPassed() throws Exception {
		XML gcXml = createGCXml("OK", "M", "M", "9988776655", 100,new Long(2),new Long(1));
		
		AuthorizationResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.ACCEPT, response.getDecision());
		assertEquals("M", response.getAddressVerificationCode());
		assertNull(response.getErrors());

		GlobalCollectResponse globalCollect = response.getGlobalCollect().getGcResponse();
		assertEquals("M", globalCollect.getCvvResult());
		assertTrue(globalCollect.isCvvPassed());
		assertEquals("9988776655", globalCollect.getPaymentReference());
		assertEquals(100L, (long)globalCollect.getStatusId());
		
		GlobalCollectRequest gcRequest = response.getGlobalCollect().getGcRequest();
		
		assertEquals(new Long(2),gcRequest.getOrderid());
		assertEquals(new Long(1),gcRequest.getEffortId());
		
	}

	@Test
	public void testBuildResponse_AVSCCVFailed() throws Exception {
		XML gcXml = createGCXml("NOK", "N", "N", "9988776655", 100,null,null);

		AuthorizationResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.REJECT, response.getDecision());
		assertEquals("N", response.getAddressVerificationCode());
		assertNull(response.getErrors());

		GlobalCollectResponse globalCollect = response.getGlobalCollect().getGcResponse();
		assertEquals("N", globalCollect.getCvvResult());
		assertFalse(globalCollect.isCvvPassed());
		assertEquals("9988776655", globalCollect.getPaymentReference());
		assertEquals(100L, (long)globalCollect.getStatusId());
	}
	
	@Test
	public void testBuildResponse_Errors() throws Exception {
		XML gcXml = createGCXml("NOK", "N", "N", "9988776655", 100,null,null);
		List<com.moneygram.service.globalcollect.doPaymentResponse_v1.Error> gcErrors = gcXml.getREQUEST().getRESPONSE().getERRORS();
		gcErrors.add(createGCError(100, "Message 1"));
		gcErrors.add(createGCError(200, "Message 2"));
		
		AuthorizationResponse response = instance.buildResponse(gcXml);
		
		Header header = response.getHeader();
		assertNotNull(header);
		
		assertEquals(Decision.REJECT, response.getDecision());
		assertEquals("N", response.getAddressVerificationCode());
		
		List<CCPError> errors = response.getErrors().getErrors();
		assertEquals(2, errors.size());
		
		CCPError error1 = errors.get(0);
		assertEquals("100", error1.getReasonCode());
		assertEquals("Message 1", error1.getErrorMessage());

		CCPError error2 = errors.get(1);
		assertEquals("200", error2.getReasonCode());
		assertEquals("Message 2", error2.getErrorMessage());
		
		GlobalCollectResponse globalCollect = response.getGlobalCollect().getGcResponse();
		assertEquals("N", globalCollect.getCvvResult());
		assertFalse(globalCollect.isCvvPassed());
		assertEquals("9988776655", globalCollect.getPaymentReference());
		assertEquals(100, (long)globalCollect.getStatusId());
	}
	
	@Test
	public void testBuildErrorResponse() throws Exception {
		Exception e = new NullPointerException("Oops.");
		e.fillInStackTrace();
		
		try {
			instance.buildErrorResponse(e);
			fail("Expected SystemException");
		} catch (SystemException e2) {
			assertEquals(e, e2.getCause());
		}
	}

	private com.moneygram.service.globalcollect.doPaymentResponse_v1.Error createGCError(int code, String message) {
		com.moneygram.service.globalcollect.doPaymentResponse_v1.Error error = 
				new com.moneygram.service.globalcollect.doPaymentResponse_v1.Error();
		error.setCODE(code);
		error.setMESSAGE(message);
		return error;
	}
	
	private XML createGCXml(String result, String avsResult, String ccvResult, String ref, int statusId,Long orderId, Long effortId) {
		Row row = new Row();
		row.setORDERID(orderId);
		row.setEFFORTID(effortId);
		row.setAVSRESULT(avsResult);
		row.setCVVRESULT(ccvResult);
		row.setPAYMENTREFERENCE(ref);
		row.setSTATUSID(statusId);
				
		Response response = new Response();
		response.setRESULT(result);
		response.setROW(row);
		
		Request request = new Request();
		request.setRESPONSE(response);
		
		XML xml = new XML();
		xml.setREQUEST(request);
		return xml;
	}
}
