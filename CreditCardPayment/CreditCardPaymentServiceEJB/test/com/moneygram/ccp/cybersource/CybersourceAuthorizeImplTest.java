package com.moneygram.ccp.cybersource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.ccp.test.BaseSpringTest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.creditcardpaymentservice_v1.BusinessRules;
import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.ObjectFactory;
import com.moneygram.service.framework.bo.EJBResponse;
import com.moneygram.service.framework.router.BaseRequestRouter;
import com.moneygram.service.framework.translator.JAXBXMLMessageTranslator;

public class CybersourceAuthorizeImplTest extends BaseSpringTest {
	private static final Logger logger = LoggerFactory.getLogger(CybersourceAuthorizeImplTest.class);

	@Autowired
	private BaseRequestRouter router;
	
	@Autowired
	private JAXBXMLMessageTranslator translator;
	
	@Test
	public void testAuthorization() throws Exception {
		AuthorizationRequest request = new AuthorizationRequest();
		setupBaseRequest(request, "authorize");

		request.setMerchantID("emgqa");
		request.setMGIReferenceCode("emg");
		request.setFirstName("John");
		request.setLastName("Doe");
		request.setStreet1("321 Kennedy St");
		request.setCity("Anoka");
		request.setState("MN");
		request.setPostalCode("55303");
		request.setCountry("USA");
		request.setCountryCode2Char("US");
		request.setEmail("tinpawn@mailinator.com");

		Card card = new Card();
		card.setAccountNumber("4111111111111111"); // test card
		card.setExpirationMonth(01);
		card.setExpirationYear(2012);
		card.setCardBrandType("visa");
		card.setCvv("608");
		card.setFullName("John A Doe");
		request.setCard(card);

		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(".01"));
		currencyAmount.setCurrency("USD");
		request.setAmount(currencyAmount);

		BusinessRules businessRules = new BusinessRules();
		businessRules.setIgnoreAVSResult(Boolean.FALSE);
		businessRules.setDeclineAVSFlags("C E N R");
		request.setBusinessRules(businessRules);
		
		String baseSchemaPackageName = ObjectFactory.class.getPackage().getName();
		String xmlRequest = translator.ConstructXMLMessage(request, baseSchemaPackageName);

		EJBResponse xmlResponse = router.process(xmlRequest);
		
		AuthorizationResponse response = (AuthorizationResponse)translator.parseXMLMessage(xmlResponse.getResponsePayload(), baseSchemaPackageName, null);
		
		logger.debug("testAuthorize: response=" + response);

		assertTrue("Expected AnalyzeEventResponse", response instanceof AuthorizationResponse);
		AuthorizationResponse authorizationResponse = (AuthorizationResponse) response;
		assertEquals("Expected accept decision", Decision.ACCEPT, authorizationResponse.getDecision());
	}

	private void setupBaseRequest(BaseCreditCardPaymentRequest request, String action) {
		String requestId = UUID.randomUUID().toString();
		String sessionId = String.valueOf(System.currentTimeMillis()); //http session id

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(action);
		processingInstruction.setReturnErrorsAsException(false);

		ClientHeader clientHeader = new ClientHeader();
		clientHeader.setClientRequestID(requestId);
		clientHeader.setClientSessionID(sessionId);
		
		Header header = new Header();
		header.setClientHeader(clientHeader);
		header.setProcessingInstruction(processingInstruction);
		
		request.setHeader(header);
	}
}
