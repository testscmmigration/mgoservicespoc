package com.moneygram.ccp.matchers;

import org.easymock.IArgumentMatcher;

import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;



public class BinLookupRequestMatcher implements IArgumentMatcher {
	
	BinLookupRequest expectedBinLookupRequest;
	
	public BinLookupRequestMatcher(BinLookupRequest expectedBinLookupRequest){
		this.expectedBinLookupRequest = expectedBinLookupRequest;
	}
		

	@Override
	public void appendTo(StringBuffer buffer) {
		buffer.append("BinLookupRequest Expected was Acc#"+expectedBinLookupRequest.getAccountNumber()+" MerchantId"+expectedBinLookupRequest.getMerchantID());

		
	}

	@Override
	public boolean matches(Object actual) {
		if (!(actual instanceof BinLookupRequest)) {
	        return false;
	      }
		BinLookupRequest actualBinLookupRequest = (BinLookupRequest) actual;
	      if(!(expectedBinLookupRequest.getAccountNumber()==actualBinLookupRequest.getAccountNumber() &&
	    		  expectedBinLookupRequest.getMerchantID()==actualBinLookupRequest.getMerchantID())) {
	        return false;
	      }
	      return true;

	}

}
