package com.moneygram.ccp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.PaymentStatusId;
import com.moneygram.ccp.test.BaseTest;
import com.moneygram.ccp.test.RequestObjectMother;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectResponse;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectStatusResponse;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointStatus;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointVendor;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckResponse;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckStatus;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;

public class GlobalCollectIntegrationTests extends BaseTest {
	//private static final String URL = "http://localhost:9089/CreditCardPaymentServiceWeb/CreditCardPaymentService_v1?wsdl";
	private static final String URL = "http://d3wsintsvcs.qacorp.moneygram.com/CCPaymentService/CreditCardPaymentService_v1?wsdl";
	
	private CreditCardPaymentServicePortType port;
	
	@Before
	public void before() throws Exception {
		URL url = new URL(URL);
	    QName qname = new QName("http://moneygram.com/service/CreditCardPaymentService_v1", "CreditCardPaymentService_v1");
		 
        Service service = Service.create(url, qname);
        port = service.getPort(CreditCardPaymentServicePortType.class);
	}

	@Test
	public void testHealthCheck() throws Exception {
		HealthCheckResponse response = healthCheck();
		assertEquals(1, response.getEndpointStatuses().size());
		HealthCheckEndpointStatus status = response.getEndpointStatuses().get(0);
		assertEquals(HealthCheckEndpointVendor.GLOBALCOLLECT, status.getVendor());
		assertEquals(HealthCheckStatus.SUCCESS, status.getStatus());
		assertNotNull(status.getEndpoint());
		assertNull(response.getErrors());
	}

	@Test
	public void testAuthAndPost_WithValidCVV() throws Exception {
		final long orderId = getOrderId();
		List<CCPError> errors;
		
		// Get Order Status - Order should not be found.	
		StatusResponse response1 = getStatus(orderId, null);
		assertEquals(Decision.REJECT, response1.getDecision());
		errors = response1.getErrors().getErrors();
		assertEquals(1, errors.size());
		assertEquals("300240", errors.get(0).getReasonCode()); // GET_ORDERSTATUS ORDER NOT FOUND
		
		long effortId = getNextEffortId(response1);
		assertEquals(1, effortId);
		
		// First Authorization - INSERT_ORDERWITHPAYMENT
		AuthorizationRequest request2 = RequestObjectMother.buildAuthorizationRequest("GB", "GB", true);
		request2.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4263982640269299", CardBrandType.VISA, 2, 2012, "837"));
		request2.setAmount(RequestObjectMother.buildCurrencyAmount("GBP", 10.99f));
		request2.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response2 = authorization(request2);
		assertEquals(Decision.ACCEPT, response2.getDecision());
		assertEquals("0", response2.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response2.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertEquals("M", response2.getGlobalCollect().getGcResponse().getCvvResult());
		assertTrue(response2.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("0", response2.getAddressVerificationCode());	// AVS is disabled for this account.
		assertNull(response2.getErrors());
		
		// Get Order Status	
		StatusResponse response3 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response3.getDecision());
		assertEquals("0", response3.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response3.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response3.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response3.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response3.getAmount().getCurrency());
		assertEquals(10.99f, response3.getAmount().getAmount(), 0.0);
		assertNull(response3.getErrors());
				
		// Post Transaction
		PostTransactionRequest request4 = RequestObjectMother.buildPostTransactionRequest();
		request4.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request4.setAmount(RequestObjectMother.buildCurrencyAmount(null, 10.99f));
		request4.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		PostTransactionResponse response4 = post(request4);
		assertEquals(Decision.ACCEPT, response4.getDecision());
		assertNull(response4.getErrors());

		// Get Order Status	
		StatusResponse response5 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response5.getDecision());
		assertEquals("0", response5.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response5.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response5.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response5.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response5.getAmount().getCurrency());
		assertEquals(10.99f, response5.getAmount().getAmount(), 0.0);
		assertNull(response5.getErrors());

		effortId++;
		assertEquals(2, effortId);


		// Second Authorization - DO_PAYMENT
		AuthorizationRequest request6 = RequestObjectMother.buildAuthorizationRequest("GB", "GB", true);
		request6.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4263982640269299", CardBrandType.VISA, 2, 2012, "837"));
		request6.setAmount(RequestObjectMother.buildCurrencyAmount("GBP", 30.00f));
		request6.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response6 = authorization(request6);
		assertEquals(Decision.ACCEPT, response6.getDecision());
		assertEquals("0", response6.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response6.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertEquals("M", response6.getGlobalCollect().getGcResponse().getCvvResult());
		assertTrue(response6.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("0", response6.getAddressVerificationCode());	// AVS is disabled for this account.
		assertNull(response6.getErrors());

		// Get Order Status	
		StatusResponse response7 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response7.getDecision());
		assertEquals("0", response7.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response7.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response7.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response7.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response7.getAmount().getCurrency());
		assertEquals(30.00f, response7.getAmount().getAmount(), 0.0);
		assertNull(response7.getErrors());
		
		// Post Transaction
		PostTransactionRequest request8 = RequestObjectMother.buildPostTransactionRequest();
		request8.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request8.setAmount(RequestObjectMother.buildCurrencyAmount(null, 20.00f));
		request8.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));

		PostTransactionResponse response8 = post(request8);
		assertEquals(Decision.ACCEPT, response8.getDecision());
		assertNull(response8.getErrors());
	
		// Get Order Status	
		StatusResponse response9 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response9.getDecision());
		assertEquals("0", response9.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response9.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response9.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response9.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response9.getAmount().getCurrency());
		assertEquals(20.00f, response9.getAmount().getAmount(), 0.0);
		assertNull(response9.getErrors());
	}

	@Test
	public void testAuthAndPost_WithValidAVS() throws Exception {
		final long orderId = getOrderId();
		List<CCPError> errors;
		
		// Get Order Status - Order should not be found.	
		StatusResponse response1 = getStatus(orderId, null);
		assertEquals(Decision.REJECT, response1.getDecision());
		errors = response1.getErrors().getErrors();
		assertEquals(1, errors.size());
		assertEquals("300240", errors.get(0).getReasonCode()); // GET_ORDERSTATUS ORDER NOT FOUND
		
		long effortId = getNextEffortId(response1);
		assertEquals(1, effortId);
		
		// First Authorization - INSERT_ORDERWITHPAYMENT
		AuthorizationRequest request2 = RequestObjectMother.buildAuthorizationRequest("12115 LACKLAND", "631461234", "USA", "US", true);
		request2.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4012000033330026", CardBrandType.VISA, 4, 2012, "837"));
		request2.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 12.00f));
		request2.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response2 = authorization(request2);
		assertEquals(Decision.ACCEPT, response2.getDecision());
		assertEquals("0", response2.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response2.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertEquals("0", response2.getGlobalCollect().getGcResponse().getCvvResult());	// CCV is disabled for this account.
		assertFalse(response2.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("Y", response2.getAddressVerificationCode());
		assertNull(response2.getErrors());
		
		// Get Order Status	
		StatusResponse response3 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response3.getDecision());
		assertEquals("0", response3.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response3.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response3.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response3.getGlobalCollect().getAttemptId().intValue());
		assertEquals("USD", response3.getAmount().getCurrency());
		assertEquals(12.00f, response3.getAmount().getAmount(), 0.0);
		assertNull(response3.getErrors());
				
		// Post Transaction
		PostTransactionRequest request4 = RequestObjectMother.buildPostTransactionRequest();
		request4.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request4.setAmount(RequestObjectMother.buildCurrencyAmount(null, 12.00f));
		request4.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		PostTransactionResponse response4 = post(request4);
		assertEquals(Decision.ACCEPT, response4.getDecision());
		assertNull(response4.getErrors());

		// Get Order Status	
		StatusResponse response5 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response5.getDecision());
		assertEquals("0", response5.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response5.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response5.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response5.getGlobalCollect().getAttemptId().intValue());
		assertEquals("USD", response5.getAmount().getCurrency());
		assertEquals(12.00f, response5.getAmount().getAmount(), 0.0);
		assertNull(response5.getErrors());

		effortId++;
		assertEquals(2, effortId);


		// Second Authorization - DO_PAYMENT
		AuthorizationRequest request6 = RequestObjectMother.buildAuthorizationRequest("12115 LACKLAND", "631461234", "USA", "US", true);
		request6.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4012000033330026", CardBrandType.VISA, 4, 2012, "837"));
		request6.setAmount(RequestObjectMother.buildCurrencyAmount("USD", 12.00f));
		request6.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response6 = authorization(request6);
		assertEquals(Decision.ACCEPT, response6.getDecision());
		assertEquals("0", response6.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response6.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertEquals("0", response6.getGlobalCollect().getGcResponse().getCvvResult());	// CCV is disabled for this account.
		assertFalse(response6.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("Y", response6.getAddressVerificationCode());
		assertNull(response6.getErrors());

		// Get Order Status	
		StatusResponse response7 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response7.getDecision());
		assertEquals("0", response7.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response7.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response7.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response7.getGlobalCollect().getAttemptId().intValue());
		assertEquals("USD", response7.getAmount().getCurrency());
		assertEquals(12.00f, response7.getAmount().getAmount(), 0.0);
		assertNull(response7.getErrors());
		
		// Post Transaction
		PostTransactionRequest request8 = RequestObjectMother.buildPostTransactionRequest();
		request8.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request8.setAmount(RequestObjectMother.buildCurrencyAmount(null, 12.00f));
		request8.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));

		PostTransactionResponse response8 = post(request8);
		assertEquals(Decision.ACCEPT, response8.getDecision());
		assertNull(response8.getErrors());
	
		// Get Order Status	
		StatusResponse response9 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response9.getDecision());
		assertEquals("0", response9.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response9.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response9.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response9.getGlobalCollect().getAttemptId().intValue());
		assertEquals("USD", response9.getAmount().getCurrency());
		assertEquals(12.00f, response9.getAmount().getAmount(), 0.0);
		assertNull(response9.getErrors());
	}

	@Test
	public void testAuthAndPost_WithoutCVVAVS() throws Exception {
		final long orderId = getOrderId();
		List<CCPError> errors;
		
		// Get Order Status - Order should not be found.	
		StatusResponse response1 = getStatus(orderId, null);
		assertEquals(Decision.REJECT, response1.getDecision());
		errors = response1.getErrors().getErrors();
		assertEquals(1, errors.size());
		assertEquals("300240", errors.get(0).getReasonCode()); // GET_ORDERSTATUS ORDER NOT FOUND
		
		long effortId = getNextEffortId(response1);
		assertEquals(1, effortId);
		
		// First Authorization - INSERT_ORDERWITHPAYMENT
		AuthorizationRequest request2 = RequestObjectMother.buildAuthorizationRequest("GB", "GB", false);
		request2.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4263982640269299", CardBrandType.VISA, 2, 2012, null));
		request2.setAmount(RequestObjectMother.buildCurrencyAmount("GBP", 10.99f));
		request2.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response2 = authorization(request2);
		assertEquals(Decision.ACCEPT, response2.getDecision());
		assertEquals("0", response2.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response2.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertNull(response2.getGlobalCollect().getGcResponse().getCvvResult());
		assertNull(response2.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("0", response2.getAddressVerificationCode());
		assertNull(response2.getErrors());
		
		// Get Order Status	
		StatusResponse response3 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response3.getDecision());
		assertEquals("0", response3.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response3.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response3.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response3.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response3.getAmount().getCurrency());
		assertEquals(10.99f, response3.getAmount().getAmount(), 0.0);
		assertNull(response3.getErrors());
				
		// Post Transaction
		PostTransactionRequest request4 = RequestObjectMother.buildPostTransactionRequest();
		request4.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request4.setAmount(RequestObjectMother.buildCurrencyAmount(null, 10.99f));
		request4.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		PostTransactionResponse response4 = post(request4);
		assertEquals(Decision.ACCEPT, response4.getDecision());
		assertNull(response4.getErrors());

		// Get Order Status	
		StatusResponse response5 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response5.getDecision());
		assertEquals("0", response5.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response5.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response5.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response5.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response5.getAmount().getCurrency());
		assertEquals(10.99f, response5.getAmount().getAmount(), 0.0);
		assertNull(response5.getErrors());

		effortId++;
		assertEquals(2, effortId);

		
		// Second Authorization - DO_PAYMENT
		AuthorizationRequest request6 = RequestObjectMother.buildAuthorizationRequest("GB", "GB", false);
		request6.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request6.setAmount(RequestObjectMother.buildCurrencyAmount("GBP", 30.00f));
		request6.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response6 = authorization(request6);
		assertEquals(Decision.ACCEPT, response6.getDecision());
		assertEquals("0", response6.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response6.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertNull(response6.getGlobalCollect().getGcResponse().getCvvResult());
		assertNull(response6.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("0", response6.getAddressVerificationCode());
		assertNull(response6.getErrors());

		// Get Order Status	
		StatusResponse response7 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response7.getDecision());
		assertEquals("0", response7.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response7.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response7.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response7.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response7.getAmount().getCurrency());
		assertEquals(30.00f, response7.getAmount().getAmount(), 0.0);
		assertNull(response7.getErrors());
		
		// Post Transaction
		PostTransactionRequest request8 = RequestObjectMother.buildPostTransactionRequest();
		request8.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request8.setAmount(RequestObjectMother.buildCurrencyAmount(null, 20.00f));
		request8.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));

		PostTransactionResponse response8 = post(request8);
		assertEquals(Decision.ACCEPT, response8.getDecision());
		assertNull(response8.getErrors());
	
		// Get Order Status	
		StatusResponse response9 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response9.getDecision());
		assertEquals("0", response9.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response9.getGlobalCollect().getStatusId().intValue());
		assertEquals(2, response9.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response9.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response9.getAmount().getCurrency());
		assertEquals(20.00f, response9.getAmount().getAmount(), 0.0);
		assertNull(response9.getErrors());
	}
	
	@Test
	public void testAuthPostAndCancel() throws Exception {
		final long orderId = getOrderId();
		List<CCPError> errors;
		
		// Get Order Status - Order should not be found.	
		StatusResponse response1 = getStatus(orderId, null);
		assertEquals(Decision.REJECT, response1.getDecision());
		errors = response1.getErrors().getErrors();
		assertEquals(1, errors.size());
		assertEquals("300240", errors.get(0).getReasonCode()); // GET_ORDERSTATUS ORDER NOT FOUND
		
		long effortId = getNextEffortId(response1);
		assertEquals(1, effortId);
		
		// First Authorization - INSERT_ORDERWITHPAYMENT
		AuthorizationRequest request2 = RequestObjectMother.buildAuthorizationRequest("GB", "GB", true);
		request2.setCard(RequestObjectMother.buildCard("Homer J Simpson", "4263982640269299", CardBrandType.VISA, 2, 2012, "837"));
		request2.setAmount(RequestObjectMother.buildCurrencyAmount("GBP", 10.99f));
		request2.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		AuthorizationResponse response2 = authorization(request2);
		assertEquals(Decision.ACCEPT, response2.getDecision());
		assertEquals("0", response2.getGlobalCollect().getGcResponse().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response2.getGlobalCollect().getGcResponse().getStatusId().intValue());
		assertEquals("M", response2.getGlobalCollect().getGcResponse().getCvvResult());
		assertTrue(response2.getGlobalCollect().getGcResponse().isCvvPassed());
		assertEquals("0", response2.getAddressVerificationCode());	// AVS is disabled for this account.
		assertNull(response2.getErrors());
		
		// Get Order Status	
		StatusResponse response3 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response3.getDecision());
		assertEquals("0", response3.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response3.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response3.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response3.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response3.getAmount().getCurrency());
		assertEquals(10.99f, response3.getAmount().getAmount(), 0.0);
		assertNull(response3.getErrors());
				
		// Post Transaction
		PostTransactionRequest request4 = RequestObjectMother.buildPostTransactionRequest();
		request4.setCard(RequestObjectMother.buildCard(null, null, CardBrandType.VISA, null, null, null));
		request4.setAmount(RequestObjectMother.buildCurrencyAmount(null, 10.99f));
		request4.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		
		PostTransactionResponse response4 = post(request4);
		assertEquals(Decision.ACCEPT, response4.getDecision());
		assertNull(response4.getErrors());

		// Get Order Status	
		StatusResponse response5 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response5.getDecision());
		assertEquals("0", response5.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_READY, response5.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response5.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response5.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response5.getAmount().getCurrency());
		assertEquals(10.99f, response5.getAmount().getAmount(), 0.0);
		assertNull(response5.getErrors());

		// Cancel Transaction - CANCEL_SETPAYMENT
		RefundTransactionRequest request6 = RequestObjectMother.buildRefundTransactionRequest();
		request6.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		request6.setAmount(RequestObjectMother.buildCurrencyAmount(null, 10.99f));
		
		RefundTransactionResponse response6 = refund(request6);
		assertEquals(Decision.ACCEPT, response6.getDecision());
		assertNull(response6.getErrors());
		
		// Get Order Status	
		StatusResponse response7 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response7.getDecision());
		assertEquals("0", response7.getGlobalCollect().getPaymentReference()); 
		assertEquals(PaymentStatusId.PAYMENT_STATUS_PENDING, response7.getGlobalCollect().getStatusId().intValue());
		assertEquals(1, response7.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response7.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response7.getAmount().getCurrency());
		assertEquals(10.99f, response7.getAmount().getAmount(), 0.0);
		assertNull(response7.getErrors());
	}

	@Test
	public void testRefund() throws Exception {
		final long orderId = 9928917619L;

		float orginalAmount = 20.00f;
		float refundAmount = 15.00f;
		float remainingAmount = orginalAmount - refundAmount;
			
		// Get Order Status	
		StatusResponse response1 = getStatus(orderId, null);
		assertEquals(Decision.ACCEPT, response1.getDecision());
		assertEquals("0", response1.getGlobalCollect().getPaymentReference()); 
		assertTrue(response1.getGlobalCollect().getStatusId().intValue() > PaymentStatusId.PAYMENT_STATUS_READY);
		assertTrue(response1.getGlobalCollect().getEffortId().intValue() > 1);
		assertEquals(1, response1.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response1.getAmount().getCurrency());
		assertEquals(orginalAmount, response1.getAmount().getAmount().floatValue(), 0.0);
		assertNull(response1.getErrors());

		long effortId = response1.getGlobalCollect().getEffortId();

		// Cancel Transaction - DO_REFUND
		RefundTransactionRequest request2 = RequestObjectMother.buildRefundTransactionRequest();
		request2.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));
		request2.setAmount(RequestObjectMother.buildCurrencyAmount(null, refundAmount));
		
		RefundTransactionResponse response2 = refund(request2);
		assertEquals(Decision.ACCEPT, response2.getDecision());
		assertNull(response2.getErrors());
		
		// Get Order Status	
		StatusResponse response3 = getStatus(orderId, effortId);
		assertEquals(Decision.ACCEPT, response3.getDecision());
		assertEquals("0", response3.getGlobalCollect().getPaymentReference()); 
		assertTrue(response3.getGlobalCollect().getStatusId().intValue() > PaymentStatusId.PAYMENT_STATUS_READY);
		assertEquals(effortId, response3.getGlobalCollect().getEffortId().intValue());
		assertEquals(1, response3.getGlobalCollect().getAttemptId().intValue());
		assertEquals("GBP", response3.getAmount().getCurrency());
		assertEquals(remainingAmount, response3.getAmount().getAmount(), 0.0);
		assertNull(response3.getErrors());
	}
	
	private HealthCheckResponse healthCheck() {
		System.out.println("\nSending Health Check");
		
		HealthCheckRequest request = RequestObjectMother.buildHealthCheckRequest();
		HealthCheckResponse response = port.healthCheck(request);
		
		System.out.println("\nHealth Check Response");
		for (HealthCheckEndpointStatus status : response.getEndpointStatuses()) {
			System.out.println("  Vendor:            " + status.getVendor());
			System.out.println("  Status:            " + status.getStatus());
			System.out.println("  Endpoint:          " + status.getEndpoint());
			System.out.println("");
		}
		logGlobalCollectErrors(response.getErrors());
		return response;
	}
	
	private StatusResponse getStatus(long orderId, Long effortId) {
		System.out.println("\nSending Order Status");
		StatusRequest request = RequestObjectMother.buildStatusRequest();
		request.setGlobalCollect(RequestObjectMother.buildGlobalCollectRequest(orderId, effortId));

		StatusResponse response = port.orderStatus(request);
		
		System.out.println("\nOrder Status Response");
		GlobalCollectStatusResponse globalCollect = response.getGlobalCollect();
		if (globalCollect != null) {
			System.out.println("  Effort Id:         " + globalCollect.getEffortId());
			System.out.println("  Attempt Id:        " + globalCollect.getAttemptId());
			System.out.println("  Payment Reference: " + globalCollect.getPaymentReference());
			System.out.println("  Status Id:         " + globalCollect.getStatusId());
		}
		
		CurrencyAmount amount = response.getAmount();
		if (amount != null) {
			System.out.println("  Currency Code:     " + amount.getCurrency());
			System.out.println("  Amount:            " + amount.getAmount());
		}

		logGlobalCollectErrors(response.getErrors());
		return response;
	}
	
	private AuthorizationResponse authorization(AuthorizationRequest request) {
		System.out.println("\nSending Authorization");
		AuthorizationResponse response = port.authorize(request);
		
		System.out.println("\nAuthorization Response");
		System.out.println("  Decision:          " + response.getDecision());
		System.out.println("  AVS Code:          " + response.getAddressVerificationCode());
		System.out.println("  Payment Reference: " + response.getGlobalCollect().getGcResponse().getPaymentReference());
		System.out.println("  Status Id:         " + response.getGlobalCollect().getGcResponse().getStatusId());
		System.out.println("  CVV Result:        " + response.getGlobalCollect().getGcResponse().getCvvResult());
		System.out.println("  CVV Passed:        " + response.getGlobalCollect().getGcResponse().isCvvPassed());
		logGlobalCollectErrors(response.getErrors());
		return response;
	}
	
	private PostTransactionResponse post(PostTransactionRequest request) {
		System.out.println("\nSending Post Transaction");
		PostTransactionResponse response = port.postTransaction(request); 

		System.out.println("\nPost Transaction Response");
		System.out.println("  Decision:          " + response.getDecision());
		logGlobalCollectErrors(response.getErrors());
		return response;
	}
	
	private RefundTransactionResponse refund (RefundTransactionRequest request) {
		System.out.println("\nSending Refund Transaction");
		RefundTransactionResponse response = port.refundTransaction(request);
		
		System.out.println("\nRefund Transaction Response");
		System.out.println("  Decision:          " + response.getDecision());
		
		GlobalCollectResponse globalCollect = response.getGlobalCollect();
		if (globalCollect != null) {
			System.out.println("  Payment Reference: " + globalCollect.getPaymentReference());
		}

		logGlobalCollectErrors(response.getErrors());
		return response;
	}

	private long getNextEffortId(StatusResponse response) {
		GlobalCollectStatusResponse globalCollect = response.getGlobalCollect();
		if (globalCollect != null) {
			return globalCollect.getEffortId() + 1;
		}
		
		return 1;
	}
	
	private long getOrderId() {
		return 9900000000L + (int)DateUtils.getFragmentInSeconds(new Date(), Calendar.YEAR);
	}

	private void logGlobalCollectErrors(CCPErrors ccpErrors) {
		if (ccpErrors != null) {
			List<CCPError> errors = ccpErrors.getErrors();
			if (errors != null && !errors.isEmpty()) {
				System.out.println("  Global Collect Errors:");
				for (CCPError error : errors) {
					System.out.println("    Reason Code:     " + error.getReasonCode());
					System.out.println("    Message:         " + error.getErrorMessage());
					System.out.println("    Can Retry:       " + error.isCanRetry());
					System.out.println("");
				}
			}
		}
	}
}
