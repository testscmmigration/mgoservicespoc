package com.moneygram.ccp;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckResponse;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;

@WebService(name = "CreditCardPaymentServicePortType", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    com.moneygram.common_v1.ObjectFactory.class,
    com.moneygram.service.creditcardpaymentservice_v1.ObjectFactory.class
})
public interface CreditCardPaymentServicePortType {
    @WebMethod(operationName = "Authorize", action = "http://moneygram.com/service/CreditCardService_v1/Authorize")
    @WebResult(name = "authorizationResponse", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
    public AuthorizationResponse authorize(
        @WebParam(name = "authorizationRequest", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
        AuthorizationRequest parameters);

    @WebMethod(operationName = "Status", action = "http://moneygram.com/service/CreditCardService_v1/Status")
    @WebResult(name = "statusResponse", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
    public StatusResponse orderStatus(
        @WebParam(name = "statusRequest", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
        StatusRequest parameters);

    @WebMethod(operationName = "PostTransaction", action = "http://moneygram.com/service/CreditCardService_v1/PostTransaction")
    @WebResult(name = "postTransactionResponse", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
    public PostTransactionResponse postTransaction(
        @WebParam(name = "postTransactionRequest", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
        PostTransactionRequest parameters);

    @WebMethod(operationName = "RefundTransaction", action = "http://moneygram.com/service/CreditCardService_v1/RefundTransaction")
    @WebResult(name = "refundTransactionResponse", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
    public RefundTransactionResponse refundTransaction(
        @WebParam(name = "refundTransactionRequest", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
        RefundTransactionRequest parameters);

    @WebMethod(operationName = "HealthCheck", action = "http://moneygram.com/service/CreditCardService_v1/HealthCheck")
    @WebResult(name = "healthCheckResponse", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
    public HealthCheckResponse healthCheck(
        @WebParam(name = "healthCheckRequest", targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", partName = "parameters")
        HealthCheckRequest parameters);
}
