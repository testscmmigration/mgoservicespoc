//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-05/12/2011 11:54 AM(foreman)-)
//


package com.moneygram.service.creditcardpaymentservice_v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for AuthorizationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuthorizationRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/service/CreditCardPaymentService_v1}BaseCreditCardPaymentRequest">
 *       &lt;sequence>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="street1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="street2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="countryCode2Char" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="returnURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="performCVVAVS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="card" type="{http://moneygram.com/service/CreditCardPaymentService_v1}Card" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://moneygram.com/service/CreditCardPaymentService_v1}CurrencyAmount" minOccurs="0"/>
 *         &lt;element name="businessRules" type="{http://moneygram.com/service/CreditCardPaymentService_v1}BusinessRules" minOccurs="0"/>
 *         &lt;element name="globalCollect" type="{http://moneygram.com/service/CreditCardPaymentService_v1}GlobalCollectRequest" minOccurs="0"/>
 *         &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secondLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderDOB" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="senderPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderIPAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fullAuthAmount" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="dmSessionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiver" type="{http://moneygram.com/service/CreditCardPaymentService_v1}Receiver" minOccurs="0"/>
 *         &lt;element name="cyberSourceDMRequest" type="{http://moneygram.com/service/CreditCardPaymentService_v1}CyberSourceDMRequest" minOccurs="0"/>
 *         &lt;element name="premierCode" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ageOfProfile" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nbrPositiveTransactions" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="daysSinceLastSentTxn" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizationRequest", propOrder = {
    "firstName",
    "lastName",
    "street1",
    "street2",
    "city",
    "state",
    "postalCode",
    "country",
    "countryCode2Char",
    "email",
    "customerId",
    "returnURL",
    "performCVVAVS",
    "card",
    "amount",
    "businessRules",
    "globalCollect",
    "middleName",
    "secondLastName",
    "senderDOB",
    "senderPhoneNumber",
    "senderIPAddress",
    "fullAuthAmount",
    "dmSessionId",
    "receiver",
    "cyberSourceDMRequest",
    "premierCode",
    "ageOfProfile",
    "nbrPositiveTransactions",
    "daysSinceLastSentTxn"
})
@XmlRootElement(name = "authorizationRequest")
public class AuthorizationRequest
    extends BaseCreditCardPaymentRequest
{

    protected String firstName;
    protected String lastName;
    protected String street1;
    protected String street2;
    protected String city;
    protected String state;
    protected String postalCode;
    protected String country;
    protected String countryCode2Char;
    protected String email;
    protected String customerId;
    protected String returnURL;
    protected Boolean performCVVAVS;
    protected Card card;
    protected CurrencyAmount amount;
    protected BusinessRules businessRules;
    protected GlobalCollectRequest globalCollect;
    protected String middleName;
    protected String secondLastName;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar senderDOB;
    protected String senderPhoneNumber;
    protected String senderIPAddress;
    protected Float fullAuthAmount;
    protected String dmSessionId;
    protected Receiver receiver;
    protected CyberSourceDMRequest cyberSourceDMRequest;
    protected Boolean premierCode;
    protected Long ageOfProfile;
    protected Long nbrPositiveTransactions;
    protected Integer daysSinceLastSentTxn;

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the street1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet1() {
        return street1;
    }

    /**
     * Sets the value of the street1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet1(String value) {
        this.street1 = value;
    }

    /**
     * Gets the value of the street2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet2() {
        return street2;
    }

    /**
     * Sets the value of the street2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet2(String value) {
        this.street2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the countryCode2Char property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode2Char() {
        return countryCode2Char;
    }

    /**
     * Sets the value of the countryCode2Char property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode2Char(String value) {
        this.countryCode2Char = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the returnURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnURL() {
        return returnURL;
    }

    /**
     * Sets the value of the returnURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnURL(String value) {
        this.returnURL = value;
    }

    /**
     * Gets the value of the performCVVAVS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPerformCVVAVS() {
        return performCVVAVS;
    }

    /**
     * Sets the value of the performCVVAVS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPerformCVVAVS(Boolean value) {
        this.performCVVAVS = value;
    }

    /**
     * Gets the value of the card property.
     * 
     * @return
     *     possible object is
     *     {@link Card }
     *     
     */
    public Card getCard() {
        return card;
    }

    /**
     * Sets the value of the card property.
     * 
     * @param value
     *     allowed object is
     *     {@link Card }
     *     
     */
    public void setCard(Card value) {
        this.card = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmount }
     *     
     */
    public CurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmount }
     *     
     */
    public void setAmount(CurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the businessRules property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessRules }
     *     
     */
    public BusinessRules getBusinessRules() {
        return businessRules;
    }

    /**
     * Sets the value of the businessRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessRules }
     *     
     */
    public void setBusinessRules(BusinessRules value) {
        this.businessRules = value;
    }

    /**
     * Gets the value of the globalCollect property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalCollectRequest }
     *     
     */
    public GlobalCollectRequest getGlobalCollect() {
        return globalCollect;
    }

    /**
     * Sets the value of the globalCollect property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalCollectRequest }
     *     
     */
    public void setGlobalCollect(GlobalCollectRequest value) {
        this.globalCollect = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the secondLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondLastName() {
        return secondLastName;
    }

    /**
     * Sets the value of the secondLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondLastName(String value) {
        this.secondLastName = value;
    }

    /**
     * Gets the value of the senderDOB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getSenderDOB() {
        return senderDOB;
    }

    /**
     * Sets the value of the senderDOB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderDOB(Calendar value) {
        this.senderDOB = value;
    }

    /**
     * Gets the value of the senderPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    /**
     * Sets the value of the senderPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderPhoneNumber(String value) {
        this.senderPhoneNumber = value;
    }

    /**
     * Gets the value of the senderIPAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderIPAddress() {
        return senderIPAddress;
    }

    /**
     * Sets the value of the senderIPAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderIPAddress(String value) {
        this.senderIPAddress = value;
    }

    /**
     * Gets the value of the fullAuthAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getFullAuthAmount() {
        return fullAuthAmount;
    }

    /**
     * Sets the value of the fullAuthAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFullAuthAmount(Float value) {
        this.fullAuthAmount = value;
    }

    /**
     * Gets the value of the dmSessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmSessionId() {
        return dmSessionId;
    }

    /**
     * Sets the value of the dmSessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmSessionId(String value) {
        this.dmSessionId = value;
    }

    /**
     * Gets the value of the receiver property.
     * 
     * @return
     *     possible object is
     *     {@link Receiver }
     *     
     */
    public Receiver getReceiver() {
        return receiver;
    }

    /**
     * Sets the value of the receiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link Receiver }
     *     
     */
    public void setReceiver(Receiver value) {
        this.receiver = value;
    }
    
    /**
     * Gets the value of the cyberSourceDMRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CyberSourceDMRequest }
     *     
     */
    public CyberSourceDMRequest getCyberSourceDMRequest() {
        return cyberSourceDMRequest;
    }

    /**
     * Sets the value of the cyberSourceDMRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CyberSourceDMRequest }
     *     
     */
    public void setCyberSourceDMRequest(CyberSourceDMRequest value) {
        this.cyberSourceDMRequest = value;
    }
    /**
     * Gets the value of the premierCode property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPremierCode() {
        return premierCode;
    }

    /**
     * Sets the value of the premierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPremierCode(Boolean value) {
        this.premierCode = value;
    }

    /**
     * Gets the value of the ageOfProfile property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAgeOfProfile() {
        return ageOfProfile;
    }

    /**
     * Sets the value of the ageOfProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAgeOfProfile(Long value) {
        this.ageOfProfile = value;
    }

    /**
     * Gets the value of the nbrPositiveTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNbrPositiveTransactions() {
        return nbrPositiveTransactions;
    }

    /**
     * Sets the value of the nbrPositiveTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNbrPositiveTransactions(Long value) {
        this.nbrPositiveTransactions = value;
    }

    /**
     * Gets the value of the daysSinceLastSentTxn property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDaysSinceLastSentTxn() {
        return daysSinceLastSentTxn;
    }

    /**
     * Sets the value of the daysSinceLastSentTxn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDaysSinceLastSentTxn(Integer value) {
        this.daysSinceLastSentTxn = value;
    }
}
