
package com.moneygram.service.globalcollect.getOrderStatusRequest_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="order">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORDERID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="EFFORTID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "order", propOrder = {
    "orderid",
    "effortid"
})
public class Order
    implements GlobalCollectBean
{

    @XmlElement(name = "ORDERID")
    protected long orderid;
    @XmlElement(name = "EFFORTID")
    protected Long effortid;

    /**
     * Gets the value of the orderid property.
     * 
     */
    public long getORDERID() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     */
    public void setORDERID(long value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the effortid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEFFORTID() {
        return effortid;
    }

    /**
     * Sets the value of the effortid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEFFORTID(Long value) {
        this.effortid = value;
    }

}
