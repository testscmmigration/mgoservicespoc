
package com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="order">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORDERID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ORDERTYPE" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CURRENCYCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LANGUAGECODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="COUNTRYCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CUSTOMERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MERCHANTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "order", propOrder = {
    "orderid",
    "ordertype",
    "amount",
    "currencycode",
    "languagecode",
    "countrycode",
    "customerid",
    "merchantreference"
})
public class Order
    implements GlobalCollectBean
{

    @XmlElement(name = "ORDERID")
    protected long orderid;
    @XmlElement(name = "ORDERTYPE")
    protected Integer ordertype;
    @XmlElement(name = "AMOUNT")
    protected long amount;
    @XmlElement(name = "CURRENCYCODE", required = true)
    protected String currencycode;
    @XmlElement(name = "LANGUAGECODE", required = true)
    protected String languagecode;
    @XmlElement(name = "COUNTRYCODE", required = true)
    protected String countrycode;
    @XmlElement(name = "CUSTOMERID")
    protected String customerid;
    @XmlElement(name = "MERCHANTREFERENCE", required = true)
    protected String merchantreference;

    /**
     * Gets the value of the orderid property.
     * 
     */
    public long getORDERID() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     */
    public void setORDERID(long value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the ordertype property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getORDERTYPE() {
        return ordertype;
    }

    /**
     * Sets the value of the ordertype property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setORDERTYPE(Integer value) {
        this.ordertype = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public long getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAMOUNT(long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYCODE() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYCODE(String value) {
        this.currencycode = value;
    }

    /**
     * Gets the value of the languagecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUAGECODE() {
        return languagecode;
    }

    /**
     * Sets the value of the languagecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUAGECODE(String value) {
        this.languagecode = value;
    }

    /**
     * Gets the value of the countrycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTRYCODE() {
        return countrycode;
    }

    /**
     * Sets the value of the countrycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTRYCODE(String value) {
        this.countrycode = value;
    }

    /**
     * Gets the value of the customerid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTOMERID() {
        return customerid;
    }

    /**
     * Sets the value of the customerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTOMERID(String value) {
        this.customerid = value;
    }

    /**
     * Gets the value of the merchantreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMERCHANTREFERENCE() {
        return merchantreference;
    }

    /**
     * Sets the value of the merchantreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMERCHANTREFERENCE(String value) {
        this.merchantreference = value;
    }

}
