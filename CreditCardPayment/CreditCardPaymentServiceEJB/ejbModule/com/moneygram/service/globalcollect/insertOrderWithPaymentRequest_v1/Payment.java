
package com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PAYMENTPRODUCTID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CURRENCYCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LANGUAGECODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="COUNTRYCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EXPIRYDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CREDITCARDNUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CVVINDICATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AVSINDICATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="HOUSENUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STREET" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HOSTEDINDICATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AUTHENTICATIONINDICATOR" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RETURNURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payment", propOrder = {
    "paymentproductid",
    "amount",
    "currencycode",
    "languagecode",
    "countrycode",
    "expirydate",
    "creditcardnumber",
    "cvv",
    "cvvindicator",
    "avsindicator",
    "housenumber",
    "street",
    "zip",
    "hostedindicator",
    "authenticationindicator",
    "returnurl"
})
public class Payment
    implements GlobalCollectBean
{

    @XmlElement(name = "PAYMENTPRODUCTID")
    protected int paymentproductid;
    @XmlElement(name = "AMOUNT")
    protected long amount;
    @XmlElement(name = "CURRENCYCODE", required = true)
    protected String currencycode;
    @XmlElement(name = "LANGUAGECODE", required = true)
    protected String languagecode;
    @XmlElement(name = "COUNTRYCODE", required = true)
    protected String countrycode;
    @XmlElement(name = "EXPIRYDATE", required = true)
    protected String expirydate;
    @XmlElement(name = "CREDITCARDNUMBER", required = true)
    protected String creditcardnumber;
    @XmlElement(name = "CVV")
    protected String cvv;
    @XmlElement(name = "CVVINDICATOR")
    protected Integer cvvindicator;
    @XmlElement(name = "AVSINDICATOR")
    protected Integer avsindicator;
    @XmlElement(name = "HOUSENUMBER")
    protected String housenumber;
    @XmlElement(name = "STREET")
    protected String street;
    @XmlElement(name = "ZIP")
    protected String zip;
    @XmlElement(name = "HOSTEDINDICATOR")
    protected int hostedindicator;
    @XmlElement(name = "AUTHENTICATIONINDICATOR")
    protected int authenticationindicator;
    @XmlElement(name = "RETURNURL")
    protected String returnurl;

    /**
     * Gets the value of the paymentproductid property.
     * 
     */
    public int getPAYMENTPRODUCTID() {
        return paymentproductid;
    }

    /**
     * Sets the value of the paymentproductid property.
     * 
     */
    public void setPAYMENTPRODUCTID(int value) {
        this.paymentproductid = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public long getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAMOUNT(long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYCODE() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYCODE(String value) {
        this.currencycode = value;
    }

    /**
     * Gets the value of the languagecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUAGECODE() {
        return languagecode;
    }

    /**
     * Sets the value of the languagecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUAGECODE(String value) {
        this.languagecode = value;
    }

    /**
     * Gets the value of the countrycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTRYCODE() {
        return countrycode;
    }

    /**
     * Sets the value of the countrycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTRYCODE(String value) {
        this.countrycode = value;
    }

    /**
     * Gets the value of the expirydate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPIRYDATE() {
        return expirydate;
    }

    /**
     * Sets the value of the expirydate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPIRYDATE(String value) {
        this.expirydate = value;
    }

    /**
     * Gets the value of the creditcardnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCREDITCARDNUMBER() {
        return creditcardnumber;
    }

    /**
     * Sets the value of the creditcardnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCREDITCARDNUMBER(String value) {
        this.creditcardnumber = value;
    }

    /**
     * Gets the value of the cvv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVV() {
        return cvv;
    }

    /**
     * Sets the value of the cvv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVV(String value) {
        this.cvv = value;
    }

    /**
     * Gets the value of the cvvindicator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCVVINDICATOR() {
        return cvvindicator;
    }

    /**
     * Sets the value of the cvvindicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCVVINDICATOR(Integer value) {
        this.cvvindicator = value;
    }

    /**
     * Gets the value of the avsindicator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAVSINDICATOR() {
        return avsindicator;
    }

    /**
     * Sets the value of the avsindicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAVSINDICATOR(Integer value) {
        this.avsindicator = value;
    }

    /**
     * Gets the value of the housenumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHOUSENUMBER() {
        return housenumber;
    }

    /**
     * Sets the value of the housenumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOUSENUMBER(String value) {
        this.housenumber = value;
    }

    /**
     * Gets the value of the street property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTREET() {
        return street;
    }

    /**
     * Sets the value of the street property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTREET(String value) {
        this.street = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIP() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIP(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the hostedindicator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
	public int getHOSTEDINDICATOR() {
		return hostedindicator;
	}

	/**
     * Sets the value of the hostedindicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
	public void setHOSTEDINDICATOR(int hostedindicator) {
		this.hostedindicator = hostedindicator;
	}

	/**
     * Gets the value of the authenticationindicator property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
	public int getAUTHENTICATIONINDICATOR() {
		return authenticationindicator;
	}

	/**
     * Sets the value of the authenticationindicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
	public void setAUTHENTICATIONINDICATOR(int authenticationindicator) {
		this.authenticationindicator = authenticationindicator;
	}

	/**
     * Gets the value of the returnurl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	public String getRETURNURL() {
		return returnurl;
	}

	/**
     * Sets the value of the returnurl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public void setRETURNURL(String returnurl) {
		this.returnurl = returnurl;
	}

}
