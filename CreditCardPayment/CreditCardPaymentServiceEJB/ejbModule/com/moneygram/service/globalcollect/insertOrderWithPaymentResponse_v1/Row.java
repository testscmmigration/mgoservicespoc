
package com.moneygram.service.globalcollect.insertOrderWithPaymentResponse_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for row complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="row">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORDERID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="EFFORTID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="AVSRESULT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CVVRESULT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PAYMENTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATUSID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FORMACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECI" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "row", propOrder = {
    "orderid",
    "effortid",
    "avsresult",
    "cvvresult",
    "paymentreference",
    "statusid",
    "ACSURL",
    "cavv",
    "eci"
})
public class Row
    implements GlobalCollectBean
{

    @XmlElement(name = "ORDERID")
    protected Long orderid;
    @XmlElement(name = "EFFORTID")
    protected Long effortid;
    @XmlElement(name = "AVSRESULT")
    protected String avsresult;
    @XmlElement(name = "CVVRESULT")
    protected String cvvresult;
    @XmlElement(name = "PAYMENTREFERENCE")
    protected String paymentreference;
    @XmlElement(name = "STATUSID")
    protected Integer statusid;
    @XmlElement(name = "FORMACTION")
    protected String ACSURL;
    @XmlElement(name = "CAVV")
    protected String cavv;
    @XmlElement(name = "ECI")
    protected Integer eci;

    /**
     * Gets the value of the orderid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getORDERID() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setORDERID(Long value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the effortid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEFFORTID() {
        return effortid;
    }

    /**
     * Sets the value of the effortid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEFFORTID(Long value) {
        this.effortid = value;
    }

    /**
     * Gets the value of the avsresult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSRESULT() {
        return avsresult;
    }

    /**
     * Sets the value of the avsresult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSRESULT(String value) {
        this.avsresult = value;
    }

    /**
     * Gets the value of the cvvresult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVVRESULT() {
        return cvvresult;
    }

    /**
     * Sets the value of the cvvresult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVVRESULT(String value) {
        this.cvvresult = value;
    }

    /**
     * Gets the value of the paymentreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMENTREFERENCE() {
        return paymentreference;
    }

    /**
     * Sets the value of the paymentreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMENTREFERENCE(String value) {
        this.paymentreference = value;
    }

    /**
     * Gets the value of the statusid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTATUSID() {
        return statusid;
    }

    /**
     * Sets the value of the statusid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTATUSID(Integer value) {
        this.statusid = value;
    }

    /**
     * Sets the value of the ACSURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public String getACSURL() {
		return ACSURL;
	}

	/**
     * Gets the value of the ACSURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	public void setACSURL(String aCSURL) {
		ACSURL = aCSURL;
	}

	/**
     * Gets the value of the CAVV property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	public String getCAVV() {
		return cavv;
	}

	/**
     * Sets the value of the CAVV property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public void setCAVV(String cavv) {
		this.cavv = cavv;
	}

	/**
     * Gets the value of the ECI property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
	public Integer getECI() {
		return eci;
	}

	/**
     * Sets the value of the ECI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
	public void setECI(Integer eci) {
		this.eci = eci;
	}
	
}
