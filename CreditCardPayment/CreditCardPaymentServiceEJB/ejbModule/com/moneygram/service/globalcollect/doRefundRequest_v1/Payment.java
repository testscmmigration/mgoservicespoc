
package com.moneygram.service.globalcollect.doRefundRequest_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for payment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="payment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MERCHANTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ORDERID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="EFFORTID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CURRENCYCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "payment", propOrder = {
    "merchantreference",
    "orderid",
    "effortid",
    "amount",
    "currencycode"
})
public class Payment
    implements GlobalCollectBean
{

    @XmlElement(name = "MERCHANTREFERENCE", required = true)
    protected String merchantreference;
    @XmlElement(name = "ORDERID")
    protected long orderid;
    @XmlElement(name = "EFFORTID")
    protected Long effortid;
    @XmlElement(name = "AMOUNT")
    protected long amount;
    @XmlElement(name = "CURRENCYCODE", required = true)
    protected String currencycode;

    /**
     * Gets the value of the merchantreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMERCHANTREFERENCE() {
        return merchantreference;
    }

    /**
     * Sets the value of the merchantreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMERCHANTREFERENCE(String value) {
        this.merchantreference = value;
    }

    /**
     * Gets the value of the orderid property.
     * 
     */
    public long getORDERID() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     */
    public void setORDERID(long value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the effortid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEFFORTID() {
        return effortid;
    }

    /**
     * Sets the value of the effortid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEFFORTID(Long value) {
        this.effortid = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public long getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAMOUNT(long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYCODE() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYCODE(String value) {
        this.currencycode = value;
    }

}
