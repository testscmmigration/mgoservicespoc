
package com.moneygram.service.globalcollect.doFinishPaymentResponse_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for row complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="row">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CURRENCYCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="STATUSID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="STATUSDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PAYMENTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ADDITIONALREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EXTERNALREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AVSRESULT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CVVRESULT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FRAUDRESULT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FRAUDCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUTHORISATIONCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "row", propOrder = {
     "currencycode",
     "amount",
     "statusid",
     "statusdate",
     "paymentreference",
     "additionalreference",
     "externalreference",
     "avsresult",
     "cvvresult",
     "fraudresult",
     "fraudcode",
     "authorisationcode"
})
public class Row
    implements GlobalCollectBean
{

    @XmlElement(name = "CURRENCYCODE")
    protected String currencycode;
    @XmlElement(name = "AMOUNT")
    protected long amount;
    @XmlElement(name = "STATUSID")
    protected Integer statusid;
	@XmlElement(name = "STATUSDATE")
	protected String statusdate;
	@XmlElement(name = "PAYMENTREFERENCE")
	protected String paymentreference;
	@XmlElement(name = "ADDITIONALREFERENCE")
	protected String additionalreference;
	@XmlElement(name = "EXTERNALREFERENCE")
	protected String externalreference;
    @XmlElement(name = "AVSRESULT")
	protected String avsresult;
	@XmlElement(name = "CVVRESULT")
	protected String cvvresult;
    @XmlElement(name = "FRAUDRESULT")
	protected String fraudresult;
    @XmlElement(name = "FRAUDCODE")
	protected String fraudcode;
    @XmlElement(name = "AUTHORISATIONCODE")
	protected String authorisationcode;

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYCODE() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYCODE(String value) {
        this.currencycode = value;
    }
    
    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAMOUNT(Long value) {
        this.amount = value;
    }
    
    /**
     * Gets the value of the statusid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTATUSID() {
        return statusid;
    }

    /**
     * Sets the value of the statusid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTATUSID(Integer value) {
        this.statusid = value;
    }

    /**
     * Gets the value of the statusdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTATUSDATE() {
        return statusdate;
    }

    /**
     * Sets the value of the statusdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTATUSDATE(String value) {
        this.statusdate = value;
    }

    /**
     * Gets the value of the paymentreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMENTREFERENCE() {
        return paymentreference;
    }

    /**
     * Sets the value of the paymentreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMENTREFERENCE(String value) {
        this.paymentreference = value;
    }
    
    /**
     * Gets the value of the additonalreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDITIONALREFERENCE() {
        return additionalreference;
    }

    /**
     * Sets the value of the additionalreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDITIONALREFERENCE(String value) {
        this.additionalreference = value;
    }

    /**
     * Gets the value of the externalreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXTERNALREFERENCE() {
        return externalreference;
    }

    /**
     * Sets the value of the externalreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXTERNALREFERENCE(String value) {
        this.externalreference = value;
    }
    
    /**
     * Gets the value of the avsresult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAVSRESULT() {
        return avsresult;
    }

    /**
     * Sets the value of the avsresult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAVSRESULT(String value) {
        this.avsresult = value;
    }
    
    /**
     * Gets the value of the cvvresult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVVRESULT() {
        return cvvresult;
    }

    /**
     * Sets the value of the cvvresult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVVRESULT(String value) {
        this.cvvresult = value;
    }
    
    /**
     * Gets the value of the fraudresult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRAUDRESULT() {
        return fraudresult;
    }

    /**
     * Sets the value of the fraudresult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRAUDRESULT(String value) {
        this.fraudresult = value;
    }
    
    /**
     * Gets the value of the fraudcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRAUDCODE() {
        return fraudcode;
    }

    /**
     * Sets the value of the fraudcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRAUDCODE(String value) {
        this.fraudcode = value;
    }
    
    /**
     * Gets the value of the authorisationcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAUTHORISATIOCODE() {
        return authorisationcode;
    }

    /**
     * Sets the value of the authorisationcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAUTHORISATIONCODE(String value) {
        this.authorisationcode = value;
    }
 
}
