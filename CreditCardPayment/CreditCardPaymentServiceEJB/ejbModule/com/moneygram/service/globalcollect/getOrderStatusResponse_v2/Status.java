
package com.moneygram.service.globalcollect.getOrderStatusResponse_v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;


/**
 * <p>Java class for status complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="status">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MERCHANTID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ORDERID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="EFFORTID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ATTEMPTID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="PAYMENTPRODUCTID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PAYMENTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MERCHANTREFERENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATUSID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CURRENCYCODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AMOUNT" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TOTALAMOUNTPAID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="TOTALAMOUNTREFUNDED" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CAVV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ECI" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status", propOrder = {
    "merchantid",
    "orderid",
    "effortid",
    "attemptid",
    "paymentproductid",
    "paymentreference",
    "merchantreference",
    "statusid",
    "currencycode",
    "amount",
    "totalamountpaid",
    "totalamountrefunded",
    "cavv",
    "eci"
})
public class Status
    implements GlobalCollectBean
{

    @XmlElement(name = "MERCHANTID")
    protected String merchantid;
    @XmlElement(name = "ORDERID")
    protected Long orderid;
    @XmlElement(name = "EFFORTID")
    protected Long effortid;
    @XmlElement(name = "ATTEMPTID")
    protected Long attemptid;
    @XmlElement(name = "PAYMENTPRODUCTID")
    protected Integer paymentproductid;
    @XmlElement(name = "PAYMENTREFERENCE")
    protected String paymentreference;
    @XmlElement(name = "MERCHANTREFERENCE")
    protected String merchantreference;
    @XmlElement(name = "STATUSID")
    protected Integer statusid;
    @XmlElement(name = "CURRENCYCODE")
    protected String currencycode;
    @XmlElement(name = "AMOUNT")
    protected Long amount;
    @XmlElement(name = "TOTALAMOUNTPAID")
    protected Long totalamountpaid;
    @XmlElement(name = "TOTALAMOUNTREFUNDED")
    protected Long totalamountrefunded;
    @XmlElement(name = "CAVV")
    protected String cavv;
    @XmlElement(name = "ECI")
    protected Integer eci;

    /**
     * Gets the value of the merchantid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMERCHANTID() {
        return merchantid;
    }

    /**
     * Sets the value of the merchantid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMERCHANTID(String value) {
        this.merchantid = value;
    }

    /**
     * Gets the value of the orderid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getORDERID() {
        return orderid;
    }

    /**
     * Sets the value of the orderid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setORDERID(Long value) {
        this.orderid = value;
    }

    /**
     * Gets the value of the effortid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEFFORTID() {
        return effortid;
    }

    /**
     * Sets the value of the effortid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEFFORTID(Long value) {
        this.effortid = value;
    }

    /**
     * Gets the value of the attemptid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getATTEMPTID() {
        return attemptid;
    }

    /**
     * Sets the value of the attemptid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setATTEMPTID(Long value) {
        this.attemptid = value;
    }

    /**
     * Gets the value of the paymentproductid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPAYMENTPRODUCTID() {
        return paymentproductid;
    }

    /**
     * Sets the value of the paymentproductid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPAYMENTPRODUCTID(Integer value) {
        this.paymentproductid = value;
    }

    /**
     * Gets the value of the paymentreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMENTREFERENCE() {
        return paymentreference;
    }

    /**
     * Sets the value of the paymentreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMENTREFERENCE(String value) {
        this.paymentreference = value;
    }

    /**
     * Gets the value of the merchantreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMERCHANTREFERENCE() {
        return merchantreference;
    }

    /**
     * Sets the value of the merchantreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMERCHANTREFERENCE(String value) {
        this.merchantreference = value;
    }

    /**
     * Gets the value of the statusid property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSTATUSID() {
        return statusid;
    }

    /**
     * Sets the value of the statusid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSTATUSID(Integer value) {
        this.statusid = value;
    }

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYCODE() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYCODE(String value) {
        this.currencycode = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAMOUNT() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAMOUNT(Long value) {
        this.amount = value;
    }

    /**
     * Gets the value of the totalamountpaid property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTOTALAMOUNTPAID() {
        return totalamountpaid;
    }

    /**
     * Sets the value of the totalamountpaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTOTALAMOUNTPAID(Long value) {
        this.totalamountpaid = value;
    }

    /**
     * Gets the value of the totalamountrefunded property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getTOTALAMOUNTREFUNDED() {
        return totalamountrefunded;
    }

    /**
     * Sets the value of the totalamountrefunded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setTOTALAMOUNTREFUNDED(Long value) {
        this.totalamountrefunded = value;
    }
    
    /**
     * Gets the value of the CAVV property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
	public String getCAVV() {
		return cavv;
	}

	/**
     * Sets the value of the CAVV property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public void setCAVV(String cavv) {
		this.cavv = cavv;
	}

	/**
     * Gets the value of the ECI property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
	public Integer getECI() {
		return eci;
	}

	/**
     * Sets the value of the ECI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
	public void setECI(Integer eci) {
		this.eci = eci;
	}

}
