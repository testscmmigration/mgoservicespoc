package com.moneygram.ccp.domain;

public enum CardBrandType {
	MASTERCARD("CC-MSTR"),
	VISA("CC-VISA"),
	MAESTRO("CC-MAEST");
	
	private String name;
	
	private CardBrandType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static boolean isMastercard(String name) {
		return name != null && name.equalsIgnoreCase(CardBrandType.MASTERCARD.getName());
	}

	public static boolean isVisa(String name) {
		return name != null && name.equalsIgnoreCase(CardBrandType.VISA.getName());
	}
	
	public static boolean isMaestro(String name) {
		return name != null && name.equalsIgnoreCase(CardBrandType.MAESTRO.getName());
	}
}
