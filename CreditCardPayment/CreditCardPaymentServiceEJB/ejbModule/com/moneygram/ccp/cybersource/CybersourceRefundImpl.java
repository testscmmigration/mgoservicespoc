package com.moneygram.ccp.cybersource;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.CyberSourceResponse;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.framework.command.ClientIntegrationException;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class CybersourceRefundImpl {
	private static final Logger logger = LoggerFactory.getLogger(CybersourceRefundImpl.class);
	private static final String CARD_ACCOUNT_NUMBER = "card_accountNumber";

	// Injected
	private Properties baseCybersourceConfig = new Properties();
	private Set<String> canRetryCodes = new HashSet<String>();
	
	public BaseServiceResponseMessage process(BaseServiceRequestMessage request) throws CommandException {
		logger.debug("process: request=" + request);

		DecimalFormat amountFormatter = new DecimalFormat("#0.00");

		// request coming in
		RefundTransactionRequest refundRequest = (RefundTransactionRequest) request;
		RefundTransactionResponse refundResponse = new RefundTransactionResponse();

		CurrencyAmount currencyAmount = refundRequest.getAmount();
		
		Properties cybersourceConfig = new Properties();
		cybersourceConfig.putAll(baseCybersourceConfig);
		cybersourceConfig.put("merchantID", refundRequest.getMerchantID());


		// cybersource requests going in/out
		Map<String, String> cybersourceRequest = new HashMap<String, String>();

		cybersourceRequest.put("ccCreditService_run", "true");
		cybersourceRequest.put("merchantId", refundRequest.getMerchantID());
		// this is your own tracking number.  CyberSource recommends that you
		// use a unique one for each order.
		String referenceCode = refundRequest.getMGIReferenceCode() == null ? "emg" : refundRequest.getMGIReferenceCode();
		cybersourceRequest.put("merchantReferenceCode", referenceCode);

		// reference the requestId returned by the previous auth. leena
		cybersourceRequest.put("ccCreditService_captureRequestID", (String) refundRequest.getCyberSource().getRequestId());
		// CurrencyAmount parameters
		cybersourceRequest.put("purchaseTotals_currency", currencyAmount.getCurrency());
		cybersourceRequest.put("purchaseTotals_grandTotalAmount",amountFormatter.format(currencyAmount.getAmount()));
		

		try {
			logProperties("CREDIT CARD REQUEST:", cybersourceRequest);

			@SuppressWarnings("unchecked")
			Map<String, String> serviceResponse = Client.runTransaction(cybersourceRequest, cybersourceConfig);

			logProperties("CREDIT CARD RESPONSE:", serviceResponse);

			String decision = (String) serviceResponse.get("decision");

			// TODO: no error returned which is different from 
			if (decision.equalsIgnoreCase("ACCEPT")) {
				refundResponse.setDecision(Decision.ACCEPT);
			} else if (decision.equalsIgnoreCase("REJECT")) {
				refundResponse.setDecision(Decision.REJECT);
			} else {
				// TODO: this should be an error or an error should be thrown
				refundResponse.setDecision(Decision.REVIEW);
			}
			CyberSourceResponse cybResp = new CyberSourceResponse();
			//refundResponse.setVendorRequestId(serviceResponse.get("requestID"));
			//refundResponse.setAddressVerificationCode(serviceResponse.get("ccAuthReply_avsCode"));
			String requestId = (String) serviceResponse.get("requestID");
			cybResp.setRequestId(requestId);
			//response = new CyberSourceResponse(requestId, decision, reasonCode);

			String reasonCode = serviceResponse.get("reasonCode");
			cybResp.setReasonCode(reasonCode);
			refundResponse.setCyberSource(cybResp);
			if (StringUtils.isBlank(reasonCode)) {
				CCPError error = new CCPError();
				error.setReasonCode(reasonCode);
				error.setCanRetry(canRetryCodes.contains(reasonCode));
				
				CCPErrors errors = new CCPErrors();
				errors.getErrors().add(error);
				refundResponse.setErrors(errors);
			}

			logger.debug("response=" + refundResponse);

			return refundResponse;
//TODO: the isCritical logic is commented out because it was found if the emg.p12 keystore isn't found,
//the 'isCritical=false' - but this is a critical error.  Old EMT code returned the response
//even though values would be null because of exceptions.  This (bad?) logic was copied from Cybersource API doc
		} catch (ClientException e) {
			logger.error("isCritical="+e.isCritical()+"; "+e.getLogString(), e);
//			if (e.isCritical()) {
				throw new ClientIntegrationException("Failed to process Authorization request-ClientException", e);
//			}
		} catch (FaultException e) {
			logger.error("isCritical="+e.isCritical()+"; "+e.getLogString(), e);
//			if (e.isCritical()) {
				throw new SystemException("Failed to process Authorization request-FaultException", e);
//			}
		} catch (Exception e) {		
			throw new SystemException("Failed to process Authorization request", e);
		}

	}

	/**
	 * Displays the content of the Map object.
	 *
	 * @param header	Header text.
	 * @param map		Map object to display.
	 */
	// BAB - copied over verbatum (mostly) from the old code
	private void logProperties(String header, Map<String, String> map) {
		if (logger.isDebugEnabled()) {
			logger.debug(header);

			StringBuffer dest = new StringBuffer();

			if (map != null && !map.isEmpty()) {
				for (Entry<String,String> entry : map.entrySet()) {
					String key = entry.getKey();
					String val = entry.getValue();
				
					if (CARD_ACCOUNT_NUMBER.equalsIgnoreCase(key)) {
						// this was the original
						// val = "*****" + StringUtils.right(val, 4);
						if (val != null) {
							val = "*****" + val.substring(val.length() - 4, val.length());
						}

					}
					dest.append(key + "=" + val + "\n");
				}
			}

			logger.debug(dest.toString());
		}
	}

	public void setBaseCybersourceConfig(Properties baseCybersourceConfig) {
		this.baseCybersourceConfig = baseCybersourceConfig;
	}
	
	public void setCanRetryCodes(Set<String> canRetryCodes) {
		this.canRetryCodes = canRetryCodes;
	}
}