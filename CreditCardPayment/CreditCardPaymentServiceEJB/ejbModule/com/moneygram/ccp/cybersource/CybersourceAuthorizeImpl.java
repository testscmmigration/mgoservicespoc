package com.moneygram.ccp.cybersource;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.util.CreditCardUtil;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BusinessRules;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.framework.command.ClientIntegrationException;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.DataFormatException;
import com.moneygram.service.framework.command.SystemException;

public class CybersourceAuthorizeImpl {
	private static final Logger logger = LoggerFactory.getLogger(CybersourceAuthorizeImpl.class);
	private static final String CARD_ACCOUNT_NUMBER = "card_accountNumber";

	// Injected
	private Properties baseCybersourceConfig = new Properties();
	private Set<String> canRetryCodes = new HashSet<String>();
	
	//added by Ankit Bhatt for 12370
	private static final int MAX_PHONE_NUMBER_LEN = 15;
	//ended
	
	public BaseServiceResponseMessage process(BaseServiceRequestMessage request) throws CommandException {
		logger.info("Inside process of CybersourceAuthorizeImpl");
		logger.info("process: request=" + request);

		DecimalFormat amountFormatter = new DecimalFormat("#0.00");

		// request coming in
		AuthorizationRequest authorizationRequest = (AuthorizationRequest) request;
		AuthorizationResponse authorizationResponse = new AuthorizationResponse();

		Card card = authorizationRequest.getCard();
		
		if (card == null) {
			throw new DataFormatException("Card is required");
		}

		//****************************************************************************
		//Jun24,2011 - TEMP CODE to change $0 MC auths to $1 auths.
		if (CardBrandType.isMastercard(card.getCardBrandType()) &&
			authorizationRequest.getAmount().getAmount() == 0f) {
			authorizationRequest.getAmount().setAmount(1f);
		}
		
		if (authorizationRequest.getAmount().getAmount() < 5f) {
		logger.warn("CCType=" + card.getCardBrandType()+ "; MGIRef#="+authorizationRequest.getMGIReferenceCode() +
				"; authAmt = " + authorizationRequest.getAmount().getAmount());
		}
		//*************************************************************************
		
		CurrencyAmount currencyAmount = authorizationRequest.getAmount();
		BusinessRules businessRules = authorizationRequest.getBusinessRules();
		
		Properties cybersourceConfig = new Properties();
		cybersourceConfig.putAll(baseCybersourceConfig);
		cybersourceConfig.put("merchantID", authorizationRequest.getMerchantID());

		// cybersource requests going in/out
		Map<String, String> cybersourceRequest = new HashMap<String, String>();

		// set up an authorization request
		cybersourceRequest.put("ccAuthService_run", "true");

		// these are from the properties set in the container / in the base
		//  request

		// TODO: Old EMT code had this line, but this gets added in 'cybersourceConfig'.properties
		//Shouldn't need the following line
		//cybersourceRequest.put("merchantId", creditCardResourceConfig.getCyberSourceMerchantId()); // emg, emgqa, emgprod

		// this is your own tracking number.  CyberSource recommends that you
		// use a unique one for each order.
		// TODO: emg should come from a container property
		String referenceCode = authorizationRequest.getMGIReferenceCode() == null ? "emg" : authorizationRequest.getMGIReferenceCode();
		cybersourceRequest.put("merchantReferenceCode", referenceCode);

		// Authorization Request Parameters
		cybersourceRequest.put("billTo_firstName", authorizationRequest.getFirstName());
		cybersourceRequest.put("billTo_lastName", authorizationRequest.getLastName());
		cybersourceRequest.put("billTo_street1", authorizationRequest.getStreet1());
		cybersourceRequest.put("billTo_city", authorizationRequest.getCity());
		cybersourceRequest.put("billTo_state", authorizationRequest.getState());
		cybersourceRequest.put("billTo_postalCode", authorizationRequest.getPostalCode());
		cybersourceRequest.put("billTo_country", "US");
		cybersourceRequest.put("billTo_email", authorizationRequest.getEmail()); // this is optional
		
		// Card parameters
		cybersourceRequest.put(CARD_ACCOUNT_NUMBER, card.getAccountNumber());
		cybersourceRequest.put("card_expirationMonth", card.getExpirationMonth().toString());
		cybersourceRequest.put("card_expirationYear", card.getExpirationYear().toString());

		// TODO: the old code used StringUtils.isNotBlank() which checked for
		//  a string that was not null, empty (""), or whitespace only.  I'm
		//  checking for two of the three.  If the client sends over whitespace,
		//  well, then the request deserves to fail
		if (card.getCvv() != null && !card.getCvv().equals("")) {
			cybersourceRequest.put("card_cvNumber", card.getCvv());
		}

		// CurrencyAmount parameters
		cybersourceRequest.put("purchaseTotals_currency", currencyAmount.getCurrency());
		cybersourceRequest.put("purchaseTotals_grandTotalAmount",amountFormatter.format(currencyAmount.getAmount()));

		//addded by Ankit Bhatt for 12370
		populateCybersourceDMFields(cybersourceRequest,authorizationRequest);
		//ended
		// Business Rule parameters
		// this works as follows according to the documentation:
		//  if ignoreAVSResult is set to true then address verification is not performed
		//  by cybersource and there will not be any AVS decline.  i.e. if ignoreAVSResult == true
		//  then the value of declineAVSFlags doesn't matter.
		if (businessRules.isIgnoreAVSResult()) {
			cybersourceRequest.put("businessRules_ignoreAVSResult", "true");
		} else {
			// the old code only had this set if address verification was performed so I am doing this here also...
			//  in reality it shouldn't matter since if ignoreAVSResult is true cybersource should simply
			//  ignore this
			// NOTE, the AvsDeclineFlags were originally a websphere container property.  The property
			//  was set to 'C E N R'.  With this interface the client is supposed to send in these
			//  flags...
			cybersourceRequest.put("businessRules_declineAVSFlags",businessRules.getDeclineAVSFlags());
		}

		try {
			logProperties("CREDIT CARD REQUEST:", cybersourceRequest);

			@SuppressWarnings("unchecked")
			Map<String, String> cybersourceResponse = Client.runTransaction(cybersourceRequest, cybersourceConfig);

			logProperties("CREDIT CARD RESPONSE:", cybersourceResponse);

			String decision = (String) cybersourceResponse.get("decision");

			// TODO: no error returned which is different from 
			if (decision.equalsIgnoreCase("ACCEPT")) {
				authorizationResponse.setDecision(Decision.ACCEPT);
			} else if (decision.equalsIgnoreCase("REJECT")) {
				authorizationResponse.setDecision(Decision.REJECT);
			} else {
				// TODO: this should be an error or an error should be thrown
				authorizationResponse.setDecision(Decision.REVIEW);
			}

			authorizationResponse.setVendorRequestId(cybersourceResponse.get("requestID"));
			authorizationResponse.setAddressVerificationCode(cybersourceResponse.get("ccAuthReply_avsCode"));
			
			String reasonCode = cybersourceResponse.get("reasonCode");
			if (StringUtils.isBlank(reasonCode)) {
				CCPError error = new CCPError();
				error.setReasonCode(reasonCode);
				error.setCanRetry(canRetryCodes.contains(reasonCode));
				
				CCPErrors errors = new CCPErrors();
				errors.getErrors().add(error);
				authorizationResponse.setErrors(errors);
			}

			logger.debug("response=" + authorizationResponse);

			return authorizationResponse;
//TODO: the isCritical logic is commented out because it was found if the emg.p12 keystore isn't found,
//the 'isCritical=false' - but this is a critical error.  Old EMT code returned the response
//even though values would be null because of exceptions.  This (bad?) logic was copied from Cybersource API doc
		} catch (ClientException e) {
			logger.error("isCritical="+e.isCritical()+"; "+e.getLogString(), e);
//			if (e.isCritical()) {
				throw new ClientIntegrationException("Failed to process Authorization request-ClientException", e);
//			}
		} catch (FaultException e) {
			logger.error("isCritical="+e.isCritical()+"; "+e.getLogString(), e);
//			if (e.isCritical()) {
				throw new SystemException("Failed to process Authorization request-FaultException", e);
//			}
		} catch (Exception e) {		
			throw new SystemException("Failed to process Authorization request", e);
		}
	}

	/**
	 * Displays the content of the Map object.
	 *
	 * @param header	Header text.
	 * @param map		Map object to display.
	 */
	// BAB - copied over verbatum (mostly) from the old code
	private void logProperties(String header, Map<String, String> map) {
		if (logger.isDebugEnabled()) {
			logger.debug(header);

			StringBuffer dest = new StringBuffer();

			if (map != null && !map.isEmpty()) {
				for (Entry<String,String> entry : map.entrySet()) {
					String key = entry.getKey();
					String val = entry.getValue();
				
					if (CARD_ACCOUNT_NUMBER.equalsIgnoreCase(key)) {
						// this was the original
						// val = "*****" + StringUtils.right(val, 4);
						if (val != null) {
							val = "*****" + val.substring(val.length() - 4, val.length());
						}

					}
					dest.append(key + "=" + val + "\n");
				}
			}

			logger.debug(dest.toString());
		}
	}
	
	
	//added by Ankit Bhatt for 12370
	void populateCybersourceDMFields(Map<String, String> csRequest, AuthorizationRequest authRequest){
		String senderPhoneNo = authRequest.getSenderPhoneNumber();
		String senderIPAddress = authRequest.getSenderIPAddress();
		String senderDOB = null;
		if(authRequest.getSenderDOB()!=null) {
			Calendar calSenderDOB = Calendar.getInstance(); 
			calSenderDOB.setTime(authRequest.getSenderDOB().getTime());
			DateFormat df = new SimpleDateFormat("yyyyMMdd");	
			senderDOB = df.format(calSenderDOB.getTime());
			System.out.println("senderDOB in YYYYMMDD format is " + senderDOB);
		}
		logger.info("original phone no passed is " + senderPhoneNo);	
		if(null!=senderPhoneNo) {
			senderPhoneNo=senderPhoneNo.replace(" ","").replace("(","").replace(")", "").replace("-","").trim();
			logger.info("after replacing special chars from phone no " + senderPhoneNo);
		}
		if(null!=senderPhoneNo && !"null".equals(senderPhoneNo) && !senderPhoneNo.matches("^[0-9]+$")){
			logger.error("Only numeric fields allowed in Sender Phone number");
			logger.info("Only numeric fields allowed in Sender Phone number");
		}
		else if(null!=senderPhoneNo && !"null".equals(senderPhoneNo)) {
			logger.info("Sender Phone no is validated for Numeric characters");
			logger.info("Putting all fields in Cybersource map after checking for blank");				
			senderPhoneNo = StringUtils.left(senderPhoneNo,MAX_PHONE_NUMBER_LEN);
			logger.info("after truncating to provided length (15) phone no passed in req is " + senderPhoneNo);
				csRequest.put("billTo_phoneNumber",senderPhoneNo);
		}
		
			if(null!=authRequest.getFullAuthAmount())
			  csRequest.put("item_0_unitPrice",authRequest.getFullAuthAmount().toString());
			
			if(null!=senderDOB)
			   csRequest.put("billTo_dateOfBirth",senderDOB);
			
			if(null!=senderIPAddress && !senderIPAddress.trim().equals(""))
				csRequest.put("billTo_ipAddress",senderIPAddress);
			
			if(null!=authRequest.getDmSessionId() && !authRequest.getDmSessionId().trim().equals(""))
			     csRequest.put("deviceFingerprintID",authRequest.getDmSessionId());
			
			if(null!=authRequest.getReceiver() &&  null!=authRequest.getReceiver().getFirstName() && !authRequest.getReceiver().getFirstName().trim().equals(""))
			     csRequest.put("merchantDefinedData_mddField_1",authRequest.getReceiver().getFirstName());
			
			if(null!=authRequest.getReceiver() &&  null!=authRequest.getReceiver().getLastName() && !authRequest.getReceiver().getLastName().trim().equals(""))
			     csRequest.put("merchantDefinedData_mddField_2",authRequest.getReceiver().getLastName());
			
			if(null!=authRequest.getReceiver() &&  null!=authRequest.getReceiver().getState() && !authRequest.getReceiver().getState().trim().equals(""))
			     csRequest.put("merchantDefinedData_mddField_5",authRequest.getReceiver().getState());
			
			if(null!=authRequest.getReceiver() &&  null!=authRequest.getReceiver().getCountry3Char() && !authRequest.getReceiver().getCountry3Char().trim().equals(""))
			    csRequest.put("merchantDefinedData_mddField_6",authRequest.getReceiver().getCountry3Char());	
			
			// Added by Arumguam for MGO-12571
			if( null!= authRequest.getSenderDOB() ) 
				if( null!= calculateSenderAge(authRequest.getSenderDOB())&& StringUtils.isNotBlank(calculateSenderAge(authRequest.getSenderDOB()))){
					csRequest.put("merchantDefinedData_mddField_7",calculateSenderAge(authRequest.getSenderDOB()).trim());
				}	
			
			if( null!= authRequest.getCyberSourceDMRequest() ){
				if( null!= authRequest.getCyberSourceDMRequest().getNbrNegativeTransactions() ){
					csRequest.put("merchantDefinedData_mddField_8", authRequest.getCyberSourceDMRequest().getNbrNegativeTransactions().toString());
				}
				
				if( null!= authRequest.getCyberSourceDMRequest().isPasswordChangeInLast24Hours() ){
            		csRequest.put("merchantDefinedData_mddField_9", authRequest.getCyberSourceDMRequest().isPasswordChangeInLast24Hours().toString());
            	}    
			}                
             
            if ( null!= authRequest.getCustomerId() && StringUtils.isNotBlank(authRequest.getCustomerId())){
            	csRequest.put("billTo_customerID", authRequest.getCustomerId());
            }    
            populateAdditionalFiledsToCybersource(csRequest,authRequest);
	}
	//ended

	private void populateAdditionalFiledsToCybersource(
			Map<String, String> csRequest, AuthorizationRequest authRequest) {

		if (authRequest.isPremierCode() != null) {
			csRequest.put("merchantDefinedData_mddField_21",
					String.valueOf(authRequest.isPremierCode()));
		}
		if (authRequest.getAgeOfProfile() != null) {
			csRequest.put("merchantDefinedData_mddField_22",
					String.valueOf(authRequest.getAgeOfProfile()));
		}
		if (authRequest.getNbrPositiveTransactions() != null) {
			csRequest.put("merchantDefinedData_mddField_23",
					String.valueOf(authRequest.getNbrPositiveTransactions()));
		}
		if (authRequest.getDaysSinceLastSentTxn() != null) {
			csRequest.put("merchantDefinedData_mddField_26",
					String.valueOf(authRequest.getDaysSinceLastSentTxn()));
		}
	}

	public void setBaseCybersourceConfig(Properties baseCybersourceConfig) {
		this.baseCybersourceConfig = baseCybersourceConfig;
	}
	
	public void setCanRetryCodes(Set<String> canRetryCodes) {
		this.canRetryCodes = canRetryCodes;
	}
	
	// addded by Arumugam for MGO-12571
	
	/**
	 * @author xz74
	 * @param senderDOB
	 * @return String
	 */
	
	public String calculateSenderAge(Calendar senderDOB) {
		
		String senderAge = null;
		
		if( null!= senderDOB ){
			senderAge = Integer.toString(CreditCardUtil.calculateAge(senderDOB));			 		
		}
		//logger.info("current Age of sender is ::::" + senderAge);		
		return senderAge;
	}
	
}