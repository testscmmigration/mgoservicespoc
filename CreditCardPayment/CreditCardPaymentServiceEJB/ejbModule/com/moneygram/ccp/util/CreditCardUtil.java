package com.moneygram.ccp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class CreditCardUtil {
	private static final List<String> XML_ELEMENTS_TO_FILTER = Arrays.asList("CREDITCARDNUMBER", "CVV", "accountNumber", "cvv", "BIN");

	/**
	 * Masks the string with last N characters left.
	 */
	public static String maskLastN(String value, int n, char mask) {
		StringBuffer accountNumber = null;
		if (value != null && StringUtils.isNotEmpty(value)) {
			accountNumber = new StringBuffer(value);
			for (int i = 0; i < accountNumber.length() - n; i++) {
				accountNumber.setCharAt(i, mask);
			}
		}

		return (accountNumber == null ? null : accountNumber.toString());
	}

	/**
	 * Masks the string with last 4 characters left using '*'.
	 */
	public static String maskLast4(String value) {
		return maskLastN(value, 4, '*');
	}
	
	public static String pciLogFilter(String xml) {
		if(null == xml) return xml;
		for (String tag : XML_ELEMENTS_TO_FILTER) {
			xml = xml.replaceAll("<\\w*:?" + tag + ">.*?</\\w*:?" + tag + ">", "<" + tag + ">*****</" + tag + ">");
		}
					
		return xml;
	}
	
	// Added by Arumugam for for MGO-12571
	/**
	 * @author xz74
	 * @param senderDOB
	 * @return int 
	 * 
	 */
	
	public static int calculateAge(Calendar senderDOB) {

        Calendar now = new GregorianCalendar();
        int curretnAge = now.get(Calendar.YEAR) - senderDOB.get(Calendar.YEAR);
        if ((senderDOB.get(Calendar.MONTH) > now.get(Calendar.MONTH))) {
              curretnAge--;

        } else if ((senderDOB.get(Calendar.MONTH) == now.get(Calendar.MONTH) && senderDOB
                    .get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
              curretnAge--;
        }
       
        return curretnAge;
  }

}
