package com.moneygram.ccp.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Provider;
import javax.xml.ws.ServiceMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.moneygram.ccp.util.CreditCardUtil;
import com.moneygram.common.util.ApplicationContextFactory;
import com.moneygram.ree.lib.Config;
import com.moneygram.service.framework.bo.EJBResponse;
import com.moneygram.service.framework.router.RequestRouter;

/**
 * Session Bean implementation class CreditCardPaymentServiceBean
 */
@Stateless
@ServiceMode(value = javax.xml.ws.Service.Mode.PAYLOAD)
@javax.xml.ws.WebServiceProvider(targetNamespace = "http://moneygram.com/service/CreditCardPaymentService_v1", 
		serviceName = "CreditCardPaymentService_v1", 
		portName = "CreditCardPaymentServicePort", 
		wsdlLocation = "META-INF/wsdl/CreditCardPaymentService_v1.wsdl")
public class CreditCardPaymentServiceBean implements CreditCardPaymentServiceBeanRemote, CreditCardPaymentServiceBeanLocal, Provider<Source> {
	@Resource 
	private SessionContext ctx;
	
	@Resource(name="rep/CCPSResourceReference")
	private Config config;
		
	private static final Logger logger = LoggerFactory.getLogger(CreditCardPaymentServiceBean.class);

	@Override
	public Source invoke(Source request) {
		String xmlRequest = getXmlFromSource(request);
		String responseXml = processRequest(xmlRequest);
		Source response = new StreamSource(new ByteArrayInputStream(responseXml.getBytes()));
		return response;
	}

	private String getXmlFromSource(Source xmlSource) {
		try{
			String xml = null;
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			StreamResult sr = new StreamResult(bos);
			Transformer tf = TransformerFactory.newInstance().newTransformer();
			tf.transform(xmlSource, sr);
			xml = bos.toString();
			
			if (logger.isDebugEnabled()) {
				logger.debug("XML Request: " + CreditCardUtil.pciLogFilter(xml));
			}
		    return xml;
		} catch(Exception e){
			logger.error("Failed to covert a raw request to XML payload!", e);
			throw new RuntimeException(e);
		}
	}
	
	public String processRequest(String request) {
		ApplicationContext context = ApplicationContextFactory.getApplicationContext(); 
		RequestRouter router = (RequestRouter)context.getBean(ApplicationContextFactory.REQUEST_ROUTER);

		EJBResponse response = router.process(request);
		if(response.isRollback()){
			ctx.setRollbackOnly();
		}

		String responsePayload = response.getResponsePayload();
		if (logger.isDebugEnabled()) {
			logger.debug("Response XML: " + CreditCardUtil.pciLogFilter(responsePayload));
		}
		
		if (response.isReturnResponseAsException()){
			throw new RuntimeException(responsePayload);
		}

		return responsePayload;
	}
}
