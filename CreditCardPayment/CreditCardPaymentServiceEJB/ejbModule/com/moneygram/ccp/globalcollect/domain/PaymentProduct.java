package com.moneygram.ccp.globalcollect.domain;

public interface PaymentProduct {
	public static final int PAYMENT_PRODUCT_MASTERCARD = 3;
	public static final int PAYMENT_PRODUCT_VISA       = 1;
	public static final int PAYMENT_PRODUCT_MASTERCARD_DEBIT = 119;
	public static final int PAYMENT_PRODUCT_VISA_DEBIT       = 114;
	public static final int PAYMENT_PRODUCT_MAESTRO_DEBIT       = 117;
}
