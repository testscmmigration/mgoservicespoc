package com.moneygram.ccp.globalcollect.domain;

public interface PaymentStatusId {
	public static final int PAYMENT_STATUS_PENDING   = 600;
	public static final int PAYMENT_STATUS_READY     = 800;
	public static final int PAYMENT_STATUS_PROCESSED = 900;
}
