package com.moneygram.ccp.globalcollect;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseGlobalCollectMapper {
	private static final Logger logger = LoggerFactory.getLogger(BaseGlobalCollectMapper.class);

	public static final String LANGUAGE_CODE = "en";

	// Injected
	private String endpoint;

	public abstract String getSchemaPackageName();

	public String getIPAddress() {
		try {
		    return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logger.error("Unable to get host ip address.", e);
			return "";
		}
	}
	
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public Long amountToLong(float amount) {
		return (long)(amount * 100.0f);
	}
	
	public float longToAmount(Long l) {
		if (l == null) {
			return 0.0f;
		} else {
			return l.floatValue() / 100.0f;
		}
	}
}
