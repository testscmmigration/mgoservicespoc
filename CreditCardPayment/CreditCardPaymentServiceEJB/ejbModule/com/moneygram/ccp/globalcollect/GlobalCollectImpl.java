package com.moneygram.ccp.globalcollect;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectRequest;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectResponse;
import com.moneygram.ccp.globalcollect.request.BaseGlobalCollectRequestMapper;
import com.moneygram.ccp.globalcollect.response.BaseGlobalCollectResponseMapper;
import com.moneygram.ccp.util.CreditCardUtil;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.framework.translator.JAXBXMLMessageTranslator;

public class GlobalCollectImpl {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectImpl.class);
	private static final boolean PRETTY_PRINT_XML = true;

	// Injected
	private int soTimeout;
	private int connectionTimeout;
	private int maxMessageSize;
	private int retryCount;
	private boolean requestSentRetry;
	private String endpoint;
	private String mimeType;
	private String encoding;
	private String userAgent;
	private JAXBXMLMessageTranslator translator;

	@SuppressWarnings("rawtypes")
	private BaseGlobalCollectRequestMapper requestMapper;
	
	@SuppressWarnings("rawtypes")
	private BaseGlobalCollectResponseMapper responseMapper;

	@SuppressWarnings("unchecked")
	public BaseServiceResponseMessage process(BaseCreditCardPaymentRequest request) throws CommandException {
		try {
			GlobalCollectRequest gcRequest = requestMapper.buildRequest(request);
			String requestXML = marshalRequest(gcRequest);
			String responseXML = httpPost(requestXML);
			GlobalCollectResponse gcResponse = unmarshalResponse(responseXML);
			return responseMapper.buildResponse(gcResponse);
		} catch (Throwable t) {
			try {
				logger.error("An error occured in process().", t);
				return responseMapper.buildErrorResponse(t);
			} catch (Throwable t2) {
				logger.error("Unable to process error in process().", t2);
				throw new SystemException("Unable to process error in process().", t2);
			}
		}
	}
	
	protected String marshalRequest(GlobalCollectRequest request) throws CommandException {
		try {
			String packageName = requestMapper.getSchemaPackageName();
			return translator.ConstructXMLMessage(request, packageName, PRETTY_PRINT_XML, true);
		} catch (Exception e) {
			logger.error("Unable to marshal request to from pojo to xml.", e);
			throw new SystemException("Unable to marshal request to from pojo to xml.", e);
		}
	}
	
	protected GlobalCollectResponse unmarshalResponse(String xml) throws CommandException {
		try {
			String packageName = responseMapper.getSchemaPackageName();
			return (GlobalCollectResponse)translator.parseXMLMessage(xml, packageName, null);
		} catch (Exception e) {
			logger.error("Unable to unmarshal response from xml to pojo.", e);
			throw new SystemException("Unable to unmarshal response from xml to pojo.", e);
		}
	}
	
	protected String httpPost(String request) throws CommandException {
		String response;
		StatusLine statusLine = null;
		HttpEntity httpEntity = null;
		boolean failure = true;
		
		StopWatch sw = new StopWatch();
		sw.start();

		if (logger.isDebugEnabled()) {
			logger.debug("Global collect request: " + CreditCardUtil.pciLogFilter(request));
		}

		try {
			try {
				HttpPost httpPost = buildHttpPost(request);
				HttpClient httpClient = createHttpClient();
				setRetryHandler(httpClient);
				setHttpParams(httpClient);
	
				HttpResponse httpResponse = httpClient.execute(httpPost);
				statusLine = httpResponse.getStatusLine();
				httpEntity = httpResponse.getEntity();
				response = (httpEntity == null) ? null : EntityUtils.toString(httpEntity);
			} catch (Exception e) {
				logger.error("Unable to send request to global collect.", e);
				throw new SystemException("Unable to send request to global collect.", e);
			} finally {
	    		close(httpEntity);
			}
			
			if (logger.isDebugEnabled()) {
				logger.debug("Global collect response (" + statusLine.toString() + "): " +  CreditCardUtil.pciLogFilter(response));
			}
			
			handleHttpErrors(response, statusLine);
			failure = false;
			return response;
		} finally {
			sw.stop();

			String status  = failure ? "failure" : "successful";
			if (statusLine != null) {
				status += " (" + statusLine.toString() + ")";
			}
			logger.info("Global Collect " + requestMapper.getGlobalCollectApiName() + " message " + status + " round trip time: " + sw);
		}
	}
	
	protected HttpPost buildHttpPost(String payload) throws IOException {
		HttpPost httpPost = new HttpPost(endpoint);
		httpPost.setHeader("User-Agent", userAgent);
		httpPost.setEntity(new StringEntity(payload, mimeType, encoding));
		return httpPost;
	}
	
	protected HttpClient createHttpClient() {
		return new DefaultHttpClient();
	}

	protected void setHttpParams(HttpClient httpClient) {
		HttpParams httpParams = httpClient.getParams();
		httpParams.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, soTimeout);
		httpParams.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, connectionTimeout);
		httpParams.setIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, maxMessageSize);
	}
	
	protected void setRetryHandler(HttpClient httpClient) {
		if (httpClient instanceof AbstractHttpClient) {
			HttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(retryCount, requestSentRetry);
			((AbstractHttpClient)httpClient).setHttpRequestRetryHandler(retryHandler);
		}
	}
	
	protected void handleHttpErrors(String response, StatusLine statusLine) throws CommandException {
		int statusCode = statusLine.getStatusCode();
		if (statusCode != 200) {
			throw new SystemException("Received error HTTP status code: " + statusLine.toString() );
		}
		
		// Check for empty response.		
		if (StringUtils.isBlank(response)) {
			throw new SystemException("Received empty HTTP response." );
		}
	}

	protected void close(HttpEntity httpEntity) {
		try {
			if (httpEntity != null) {
				EntityUtils.consume(httpEntity);
			}
		} catch (Exception e) {
			logger.warn("Cannot consume HttpEntity.", e);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void setRequestMapper(BaseGlobalCollectRequestMapper requestMapper) {
		this.requestMapper = requestMapper;
	}

	@SuppressWarnings("rawtypes")
	public void setResponseMapper(BaseGlobalCollectResponseMapper responseMapper) {
		this.responseMapper = responseMapper;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void setMaxMessageSize(int maxMessageSize) {
		this.maxMessageSize = maxMessageSize;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public void setSoTimeout(int soTimeout) {
		this.soTimeout = soTimeout;
	}

	public void setTranslator(JAXBXMLMessageTranslator translator) {
		this.translator = translator;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public synchronized void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public synchronized void setRequestSentRetry(boolean requestSentRetry) {
		this.requestSentRetry = requestSentRetry;
	}
}
