package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Action;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.ObjectFactory;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Order;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Params;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.Version;
import com.moneygram.service.globalcollect.getOrderStatusRequest_v2.XML;

public class GlobalCollectGetOrderStatusRequestMapper extends BaseGlobalCollectRequestMapper<StatusRequest, XML> {
	public XML buildRequest(StatusRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();

		XML xml = createXML(Action.GET_ORDERSTATUS.name(), request.getMerchantID(), Version.v2.name());
		Params params = xml.getREQUEST().getPARAMS();
		
		Order order = addOrder(params);
		order.setORDERID(globalCollect.getOrderid());
		order.setEFFORTID(globalCollect.getEffortId());
		
		return xml;
	}

	public String getGlobalCollectApiName() {
		return Action.GET_ORDERSTATUS.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
