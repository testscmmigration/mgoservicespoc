package com.moneygram.ccp.globalcollect.request;

import org.apache.commons.lang.StringUtils;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Order;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.insertOrderWithPaymentRequest_v1.XML;

public class GlobalCollectInsertOrderWithPaymentRequestMapper extends BaseGlobalCollectRequestMapper<AuthorizationRequest, XML> {
	protected static final int ORDER_TYPE = 1;  // Variable amount not recurring
	
	//injected
	private int globalCollectHostedIndicator;
	private int globalCollectAuthenticationIndicator;
	
	public XML buildRequest(AuthorizationRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();
		CurrencyAmount amount = request.getAmount();
		Card card = request.getCard();
				
		XML xml = createXML(Action.INSERT_ORDERWITHPAYMENT.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();
		
		Order order = addOrder(params);
		order.setORDERID(globalCollect.getOrderid());
		/*MGO-4559: For all payment methods order type is 1*/
		order.setORDERTYPE(ORDER_TYPE);
		
		order.setAMOUNT(amountToLong(amount.getAmount()));
		order.setCURRENCYCODE(amount.getCurrency());
		order.setLANGUAGECODE(LANGUAGE_CODE);
		order.setCOUNTRYCODE(request.getCountryCode2Char());
		order.setCUSTOMERID(request.getCustomerId());
		order.setMERCHANTREFERENCE(request.getMGIReferenceCode());
		
		Payment payment = addPayment(params);
		payment.setPAYMENTPRODUCTID(convertCardType(card.getCardBrandType(),card.getCardType()));
		payment.setAMOUNT(amountToLong(amount.getAmount()));
		payment.setCURRENCYCODE(amount.getCurrency());
		payment.setLANGUAGECODE(LANGUAGE_CODE);
		payment.setCOUNTRYCODE(request.getCountryCode2Char());
		payment.setCREDITCARDNUMBER(card.getAccountNumber());
		payment.setEXPIRYDATE(convertExpireDate(card.getExpirationMonth(), card.getExpirationYear()));
		payment.setSTREET(StringUtils.left(request.getStreet1(),GC_MAX_STREET_SIZE));
		payment.setZIP(request.getPostalCode());
		if(CardBrandType.isMaestro(card.getCardBrandType())){
			payment.setHOSTEDINDICATOR(globalCollectHostedIndicator);
			payment.setAUTHENTICATIONINDICATOR(globalCollectAuthenticationIndicator);
			payment.setRETURNURL(request.getReturnURL());
		}
		
		if(request.isPerformCVVAVS()) {
			payment.setAVSINDICATOR(1);
			//Set Building if present in Street1 since it is needs for AVS check
			setBuildingOnPaymentIfPresent(payment,request.getStreet1());

			if (StringUtils.isBlank(card.getCvv())) {
				payment.setCVVINDICATOR(0);
			} else {
				payment.setCVVINDICATOR(1);
				payment.setCVV(card.getCvv());
			}
		} else {
			payment.setCVVINDICATOR(0);
			payment.setAVSINDICATOR(0);
		}

		return xml;
	}

	private void setBuildingOnPaymentIfPresent(Payment payment, String street1) {
		String[] splitStreet1 = StringUtils.split(street1);
		if(null!=splitStreet1 && splitStreet1.length > 1 && StringUtils.isNumeric(splitStreet1[0])){
			StringBuffer actualStreet1 = new StringBuffer(splitStreet1[1]);
			for(int i=2;i<splitStreet1.length;i++){
				actualStreet1.append(" "+ splitStreet1[i]);
			}
			payment.setSTREET(StringUtils.left(actualStreet1.toString(),GC_MAX_STREET_SIZE));
			
			payment.setHOUSENUMBER(splitStreet1[0]);
		}
		
	}

	public String getGlobalCollectApiName() {
		return Action.INSERT_ORDERWITHPAYMENT.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}

	public int getGlobalCollectHostedIndicator() {
		return globalCollectHostedIndicator;
	}

	public void setGlobalCollectHostedIndicator(int globalCollectHostedIndicator) {
		this.globalCollectHostedIndicator = globalCollectHostedIndicator;
	}

	public int getGlobalCollectAuthenticationIndicator() {
		return globalCollectAuthenticationIndicator;
	}

	public void setGlobalCollectAuthenticationIndicator(
			int globalCollectAuthenticationIndicator) {
		this.globalCollectAuthenticationIndicator = globalCollectAuthenticationIndicator;
	}
}
