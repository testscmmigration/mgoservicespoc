package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.globalcollect.doRefundRequest_v1.Action;
import com.moneygram.service.globalcollect.doRefundRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doRefundRequest_v1.Params;
import com.moneygram.service.globalcollect.doRefundRequest_v1.Payment;
import com.moneygram.service.globalcollect.doRefundRequest_v1.Version;
import com.moneygram.service.globalcollect.doRefundRequest_v1.XML;

public class GlobalCollectDoRefundRequestMapper extends BaseGlobalCollectRequestMapper<RefundTransactionRequest, XML> {
	public XML buildRequest(RefundTransactionRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();
		CurrencyAmount amount = request.getAmount();
	
		XML xml = createXML(Action.DO_REFUND.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();

		Payment payment = addPayment(params);
		payment.setMERCHANTREFERENCE(request.getMGIReferenceCode());
		payment.setORDERID(globalCollect.getOrderid());
		payment.setEFFORTID(globalCollect.getEffortId());
		payment.setAMOUNT(amountToLong(amount.getAmount()));

		return xml;
	}

	public String getGlobalCollectApiName() {
		return Action.DO_REFUND.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
