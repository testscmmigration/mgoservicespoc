package com.moneygram.ccp.globalcollect.request;

import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectRequest;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.ccp.globalcollect.BaseGlobalCollectMapper;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;

public abstract class BaseGlobalCollectRequestMapper<R extends BaseCreditCardPaymentRequest, G extends GlobalCollectRequest> extends
		BaseGlobalCollectMapper {

	public abstract String getGlobalCollectApiName();

	public abstract G buildRequest(R baseRequest) throws Exception;

	protected static final int GC_MAX_STREET_SIZE = 50;

	protected <T extends GlobalCollectRequest> T createXML(String action, String merchantId, String apiVersion) throws Exception {
		T xml = this.<T>createBean("XML");
		PropertyUtils.setProperty(xml, "REQUEST", createRequest(action, merchantId, apiVersion));
		return xml;
	}

	protected <T extends GlobalCollectBean> T createRequest(String action, String merchantId, String apiVersion) throws Exception {
		T request = this.<T>createBean("Request");
		PropertyUtils.setProperty(request, "ACTION", createAction(action));
		PropertyUtils.setProperty(request, "META", createMeta(merchantId, apiVersion));
		PropertyUtils.setProperty(request, "PARAMS", createParams());
		return request;
	}

	protected <T extends GlobalCollectBean> T createMeta(String merchantId, String apiVersion) throws Exception {
		T meta = this.<T>createBean("Meta");
		PropertyUtils.setProperty(meta, "MERCHANTID", merchantId);
		PropertyUtils.setProperty(meta, "IPADDRESS", getIPAddress());
		PropertyUtils.setProperty(meta, "VERSION", createVersion(apiVersion));
		return meta;
	}

	protected <T extends GlobalCollectBean> T createParams() throws Exception {
		T params = this.<T>createBean("Params");
		return params;
	}
	
	protected <T extends GlobalCollectBean> T addGeneral(GlobalCollectBean params) throws Exception {
		assertType("Params", params);
		T general = this.<T>createBean("General");
		PropertyUtils.setProperty(params, "GENERAL", general);
		return general;
	}	

	protected <T extends GlobalCollectBean> T addOrder(GlobalCollectBean params) throws Exception {
		assertType("Params", params);
		T order = this.<T>createBean("Order");
		PropertyUtils.setProperty(params, "ORDER", order);
		return order;
	}

	protected <T extends GlobalCollectBean> T addPayment(GlobalCollectBean params) throws Exception {
		assertType("Params", params);
		T payment = this.<T>createBean("Payment");
		PropertyUtils.setProperty(params, "PAYMENT", payment);
		return payment;
	}

	@SuppressWarnings("unchecked")
	protected <T extends GlobalCollectBean> T createBean(String className) throws Exception {
		Class<T> clazz = ClassUtils.getClass(getSchemaPackageName() + "." + className);
		return (T) ConstructorUtils.invokeConstructor(clazz, null);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Enum createAction(String actionName) throws Exception {
		Class<Enum> clazz = ClassUtils.getClass(getSchemaPackageName() + ".Action");
		return Enum.valueOf(clazz, actionName);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Enum createVersion(String apiVersion) throws Exception {
		Class<Enum> clazz = ClassUtils.getClass(getSchemaPackageName() + ".Version");
		return Enum.valueOf(clazz, apiVersion);
	}

	@SuppressWarnings("unchecked")
	protected void assertType(String expectedClassName, GlobalCollectBean bean) throws IllegalArgumentException {
		Class<GlobalCollectBean> expectedClass;
		Class<GlobalCollectBean> actualClass = (Class<GlobalCollectBean>) bean.getClass();

		try {
			expectedClass = ClassUtils.getClass(getSchemaPackageName() + "." + expectedClassName);
		} catch (Exception e) {
			expectedClass = null;
		}

		if (expectedClassName == null || !expectedClass.equals(actualClass)) {
			throw new IllegalArgumentException("Expected class " + expectedClassName + ", but got " + actualClass.getSimpleName());
		}
	}

	protected String convertExpireDate(int month, int year) {
		String s = StringUtils.leftPad(Integer.toString(month), 2, '0');

		if (year < 100) {
			s += StringUtils.leftPad(Integer.toString(year), 2, '0');
		} else if (year < 2000 || year > 2999) {
			throw new IllegalArgumentException("Invalid year specified: " + year);
		} else {
			s += StringUtils.right(Integer.toString(year), 2);
		}

		return s;
	}

	protected Integer convertCardType(String cardBrandType, CardType cardType) {
		if (CardType.DEBIT==cardType) {
			if (CardBrandType.isMastercard(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_MASTERCARD_DEBIT;
			} else if (CardBrandType.isVisa(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_VISA_DEBIT;
			} else if (CardBrandType.isMaestro(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_MAESTRO_DEBIT;
			} else {
				return null;
			}

		} else {
			if (CardBrandType.isMastercard(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_MASTERCARD;
			} else if (CardBrandType.isVisa(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_VISA;
			} else if (CardBrandType.isMaestro(cardBrandType)) {
				return PaymentProduct.PAYMENT_PRODUCT_MAESTRO_DEBIT;
			} else {
				return null;
			}
		}

	}
}
