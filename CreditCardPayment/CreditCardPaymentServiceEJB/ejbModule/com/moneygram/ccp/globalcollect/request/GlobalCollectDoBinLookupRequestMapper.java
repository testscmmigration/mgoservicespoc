package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.Action;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.General;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.Params;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.Version;
import com.moneygram.service.globalcollect.doBinLookupRequest_v1.XML;

public class GlobalCollectDoBinLookupRequestMapper extends BaseGlobalCollectRequestMapper<BinLookupRequest, XML> {
	public XML buildRequest(BinLookupRequest request) throws Exception {

		XML xml = createXML(Action.DO_BINLOOKUP.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();
		
		General general = addGeneral(params);
		general.setBIN(request.getAccountNumber());
		
		return xml;
	}

	public String getGlobalCollectApiName() {
		return Action.DO_BINLOOKUP.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
