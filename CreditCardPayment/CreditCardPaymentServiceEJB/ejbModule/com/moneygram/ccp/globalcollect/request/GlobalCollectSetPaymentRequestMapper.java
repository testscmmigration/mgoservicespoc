package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.setPaymentRequest_v1.XML;

public class GlobalCollectSetPaymentRequestMapper extends BaseGlobalCollectRequestMapper<PostTransactionRequest, XML> {
	public XML buildRequest(PostTransactionRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();
		CurrencyAmount amount = request.getAmount();
		Card card = request.getCard();
				
		XML xml = createXML(Action.SET_PAYMENT.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();
		
		Payment payment = addPayment(params);
		payment.setORDERID(globalCollect.getOrderid());
		payment.setEFFORTID(globalCollect.getEffortId());
		payment.setPAYMENTPRODUCTID(convertCardType(card.getCardBrandType(),card.getCardType()));
		payment.setAMOUNT(amountToLong(amount.getAmount()));
		
		return xml;
	}

	public String getGlobalCollectApiName() {
		return Action.SET_PAYMENT.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
