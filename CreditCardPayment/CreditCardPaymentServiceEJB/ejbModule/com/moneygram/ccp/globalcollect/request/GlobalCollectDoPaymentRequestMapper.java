package com.moneygram.ccp.globalcollect.request;

import org.apache.commons.lang.StringUtils;

import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.Card;
import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.doPaymentRequest_v1.XML;

public class GlobalCollectDoPaymentRequestMapper extends BaseGlobalCollectRequestMapper<AuthorizationRequest, XML> {
	

	public XML buildRequest(AuthorizationRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();
		CurrencyAmount amount = request.getAmount();
				
		XML xml = createXML(Action.DO_PAYMENT.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();
		
		Payment payment = addPayment(params);
		payment.setMERCHANTREFERENCE(request.getMGIReferenceCode());
		payment.setORDERID(globalCollect.getOrderid());
		payment.setEFFORTID(globalCollect.getEffortId());
		payment.setAMOUNT(amountToLong(amount.getAmount()));
		payment.setCURRENCYCODE(amount.getCurrency());
		payment.setLANGUAGECODE(LANGUAGE_CODE);
		payment.setCOUNTRYCODE(request.getCountryCode2Char());
		
		if(request.isPerformCVVAVS()) {
			Card card = request.getCard();
            //PEr Email from Andy P 2/21/2012 not setting this anymore
			//payment.setPAYMENTPRODUCTID(convertCardType(card.getCardType()));
			payment.setCREDITCARDNUMBER(card.getAccountNumber());
			payment.setEXPIRYDATE(convertExpireDate(card.getExpirationMonth(), card.getExpirationYear()));
			payment.setSTREET(StringUtils.left(request.getStreet1(),GC_MAX_STREET_SIZE));
			payment.setZIP(request.getPostalCode());
			payment.setAVSINDICATOR(1);

			if (StringUtils.isBlank(card.getCvv())) {
				payment.setCVVINDICATOR(0);
			} else {
				payment.setCVVINDICATOR(1);
				payment.setCVV(card.getCvv());
			}
		} else {
			payment.setCVVINDICATOR(0);
			payment.setAVSINDICATOR(0);
		}

		return xml;
	}
	
	public String getGlobalCollectApiName() {
		return Action.DO_PAYMENT.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
