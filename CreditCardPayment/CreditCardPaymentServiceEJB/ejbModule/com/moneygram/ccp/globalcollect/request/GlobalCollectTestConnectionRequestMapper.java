package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckRequest;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.Action;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.Version;
import com.moneygram.service.globalcollect.testConnectionRequest_v1.XML;

public class GlobalCollectTestConnectionRequestMapper extends BaseGlobalCollectRequestMapper<HealthCheckRequest, XML> {
	public XML buildRequest(HealthCheckRequest request) throws Exception {
		return createXML(Action.TEST_CONNECTION.name(), request.getMerchantID(), Version.v1.name());
	}

	public String getGlobalCollectApiName() {
		return Action.TEST_CONNECTION.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
