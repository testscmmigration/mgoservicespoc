package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.cancelSetPaymentRequest_v1.XML;

public class GlobalCollectCancelSetPaymentRequestMapper extends BaseGlobalCollectRequestMapper<RefundTransactionRequest, XML> {
	public XML buildRequest(RefundTransactionRequest request) throws Exception {
		GlobalCollectRequest globalCollect = request.getGlobalCollect();
	
		XML xml = createXML(Action.CANCEL_SET_PAYMENT.name(), request.getMerchantID(), Version.v1.name());
		Params params = xml.getREQUEST().getPARAMS();

		Payment payment = addPayment(params);
		payment.setORDERID(globalCollect.getOrderid());
		payment.setEFFORTID(globalCollect.getEffortId());

		return xml;
	}

	public String getGlobalCollectApiName() {
		return Action.CANCEL_SET_PAYMENT.name();
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
