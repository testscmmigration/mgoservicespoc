package com.moneygram.ccp.globalcollect.request;

import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.Action;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.Params;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.Payment;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.Version;
import com.moneygram.service.globalcollect.doFinishPaymentRequest_v1.XML;

public class GlobalCollectDoFinishPaymentRequestMapper extends BaseGlobalCollectRequestMapper<PostTransactionRequest, XML> {
    
    public XML buildRequest(PostTransactionRequest request)
            throws Exception {
        GlobalCollectRequest globalCollect = request.getGlobalCollect();
        XML xml = createXML(Action.DO_FINISHPAYMENT.name(),
                request.getMerchantID(), Version.v1.name());
        Params params = xml.getREQUEST().getPARAMS();
        Payment payment = addPayment(params);
        payment.setORDERID(globalCollect.getOrderid());
        payment.setEFFORTID(globalCollect.getEffortId());
        /*
         * According to design docs, attemptId for DO_FINISHPAYMENT for
         * moneygram purposes is always set to '1'.  Later on other situations
         * may arise where globalcollect.getAttemptId()) is used instead.
         */
        payment.setATTEMPTID(1);
        return xml;
    }
    
    
    public String getGlobalCollectApiName() {
        return Action.DO_FINISHPAYMENT.name();
    }

    public String getSchemaPackageName() {
        return ObjectFactory.class.getPackage().getName();
    }

}
