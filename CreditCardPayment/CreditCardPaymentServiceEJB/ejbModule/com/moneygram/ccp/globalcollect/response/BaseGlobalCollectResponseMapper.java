package com.moneygram.ccp.globalcollect.response;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.BaseGlobalCollectMapper;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectBean;
import com.moneygram.ccp.globalcollect.domain.GlobalCollectResponse;
import com.moneygram.ccp.globalcollect.domain.PaymentProduct;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.creditcardpaymentservice_v1.BaseCreditCardPaymentResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPError;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.creditcardpaymentservice_v1.Decision;

public abstract class BaseGlobalCollectResponseMapper<R extends BaseCreditCardPaymentResponse, G extends GlobalCollectResponse> extends
		BaseGlobalCollectMapper {
	private static final Logger logger = LoggerFactory.getLogger(BaseGlobalCollectResponseMapper.class);
	private static final Set<String> canRetryCodes = new HashSet<String>();

	public abstract R buildResponse(G response) throws Exception;

	public abstract R buildErrorResponse(Throwable t) throws Exception;

	protected Header buildHeader() {
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setRollbackTransaction(false);

		Header header = new Header();
		header.setProcessingInstruction(pi);
		return header;
	}

	protected CardType convertPaymentProductToCardType(Integer paymentProduct) {
		if (paymentProduct == null) return CardType.UNKNOWN;
		
		if (paymentProduct == PaymentProduct.PAYMENT_PRODUCT_MASTERCARD_DEBIT||paymentProduct == PaymentProduct.PAYMENT_PRODUCT_VISA_DEBIT) {
			return CardType.DEBIT;
		}
		if (paymentProduct == PaymentProduct.PAYMENT_PRODUCT_MASTERCARD || paymentProduct == PaymentProduct.PAYMENT_PRODUCT_VISA) {
			return CardType.CREDIT;
		}
		return CardType.UNKNOWN;
	}

	protected Decision convertResult(String result) {
		if (result.equalsIgnoreCase("OK")) {
			return Decision.ACCEPT;
		} else if (result.equalsIgnoreCase("NOK")) {
			return Decision.REJECT;
		} else {
			logger.info("Unsupported result: " + result);
			return Decision.REVIEW;
		}
	}

	protected CardBrandType convertPaymentProduct(Integer paymentProduct) {
		if (paymentProduct != null) {
			if (paymentProduct == PaymentProduct.PAYMENT_PRODUCT_MASTERCARD) {
				return CardBrandType.MASTERCARD;
			} else if (paymentProduct == PaymentProduct.PAYMENT_PRODUCT_VISA) {
				return CardBrandType.VISA;
			}
		}

		return null;
	}

	protected Set<String> getCanRetryCodes() {
		return canRetryCodes;
	}

	protected boolean canRetry(String reasonCode) {
		return reasonCode != null && canRetryCodes.contains(reasonCode);
	}

	protected boolean didCVVPass(String cvvResult) {
		return cvvResult != null && !cvvResult.equalsIgnoreCase("N");
	}

	@SuppressWarnings("unchecked")
	protected CCPErrors mapErrors(G xml) throws Exception {
		CCPErrors errors = null;

		if (xml != null) {
			GlobalCollectBean gcRequest = (GlobalCollectBean) PropertyUtils.getProperty(xml, "REQUEST");
			GlobalCollectBean gcResponse = (GlobalCollectBean) PropertyUtils.getProperty(gcRequest, "RESPONSE");
			List<GlobalCollectBean> gcErrors = (List<GlobalCollectBean>) PropertyUtils.getProperty(gcResponse, "ERRORS");

			if (gcErrors != null && !gcErrors.isEmpty()) {
				errors = new CCPErrors();
				List<CCPError> list = errors.getErrors();

				for (GlobalCollectBean gcError : gcErrors) {
					String code = ((Integer) PropertyUtils.getProperty(gcError, "CODE")).toString();
					String msg = (String) PropertyUtils.getProperty(gcError, "MESSAGE");

					CCPError error = new CCPError();
					error.setReasonCode(code);
					error.setErrorMessage(msg);
					error.setCanRetry(canRetry(code));
					list.add(error);
				}
			}
		}

		return errors;
	}
}
