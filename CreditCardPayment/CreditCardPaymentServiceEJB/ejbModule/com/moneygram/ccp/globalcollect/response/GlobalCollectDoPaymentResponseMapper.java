package com.moneygram.ccp.globalcollect.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectAuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.Response;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.Row;
import com.moneygram.service.globalcollect.doPaymentResponse_v1.XML;

public class GlobalCollectDoPaymentResponseMapper extends BaseGlobalCollectResponseMapper<AuthorizationResponse, XML> {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectDoPaymentResponseMapper.class);

	public AuthorizationResponse buildResponse(XML gcXml) throws Exception {
		GlobalCollectAuthorizationResponse globalCollect = new GlobalCollectAuthorizationResponse();
		globalCollect.setGcRequest(new GlobalCollectRequest());
		globalCollect.setGcResponse(new GlobalCollectResponse());
		AuthorizationResponse response = new AuthorizationResponse();
		response.setHeader(buildHeader());
		response.setGlobalCollect(globalCollect);
		
		Response gcResponse = gcXml.getREQUEST().getRESPONSE();
		Row gcRow = gcResponse.getROW();

		if (gcRow != null) {
			response.setAddressVerificationCode(gcRow.getAVSRESULT());
			globalCollect.getGcRequest().setOrderid(gcRow.getORDERID());
			globalCollect.getGcRequest().setEffortId(gcRow.getEFFORTID());
			globalCollect.getGcResponse().setPaymentReference(gcRow.getPAYMENTREFERENCE());
			globalCollect.getGcResponse().setStatusId(gcRow.getSTATUSID());
			
			String cvvResult = gcRow.getCVVRESULT();
			if (cvvResult != null) {
				globalCollect.getGcResponse().setCvvResult(cvvResult);
				globalCollect.getGcResponse().setCvvPassed(didCVVPass(cvvResult));
			}
		}

		response.setDecision(convertResult(gcResponse.getRESULT()));
		response.setErrors(mapErrors(gcXml));
		return response;
	}
	
	public AuthorizationResponse buildErrorResponse(Throwable t) throws Exception {
		logger.error("An error occured", t);
		throw new SystemException("An error occured in process()", t);
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
