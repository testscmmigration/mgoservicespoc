package com.moneygram.ccp.globalcollect.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.setPaymentResponse_v1.ObjectFactory;
import com.moneygram.service.globalcollect.setPaymentResponse_v1.Response;
import com.moneygram.service.globalcollect.setPaymentResponse_v1.XML;

public class GlobalCollectSetPaymentResponseMapper extends BaseGlobalCollectResponseMapper<PostTransactionResponse, XML> {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectSetPaymentResponseMapper.class);

	public PostTransactionResponse buildResponse(XML gcXml) throws Exception {
		Response gcResponse = gcXml.getREQUEST().getRESPONSE();
		
		PostTransactionResponse response = new PostTransactionResponse();
		response.setHeader(buildHeader());
		response.setDecision(convertResult(gcResponse.getRESULT()));
		response.setErrors(mapErrors(gcXml));
		return response;
	}

	public PostTransactionResponse buildErrorResponse(Throwable t) throws Exception {
		logger.error("An error occured", t);
		throw new SystemException("An error occured in process()", t);
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
