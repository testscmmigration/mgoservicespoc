package com.moneygram.ccp.globalcollect.response;

import java.net.SocketTimeoutException;
import java.util.List;

import com.moneygram.service.creditcardpaymentservice_v1.Decision;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointStatus;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckEndpointVendor;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckResponse;
import com.moneygram.service.creditcardpaymentservice_v1.HealthCheckStatus;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.globalcollect.testConnectionResponse_v1.ObjectFactory;
import com.moneygram.service.globalcollect.testConnectionResponse_v1.XML;

public class GlobalCollectTestConnectionResponseMapper extends BaseGlobalCollectResponseMapper<HealthCheckResponse, XML> {
	public HealthCheckResponse buildResponse(XML gcXml) throws Exception {
		HealthCheckStatus status;
		try {
			XML xml = (XML)gcXml;
			String result = xml.getREQUEST().getRESPONSE().getRESULT();
			status = result.equals("OK") ? HealthCheckStatus.SUCCESS : HealthCheckStatus.ERROR;
		} catch (Exception e) {
			status = HealthCheckStatus.ERROR;
		}
		
		return buildResponse(gcXml, status);
	}

	public HealthCheckResponse buildErrorResponse(Throwable t)  throws Exception {
		if (t instanceof CommandException ) {
			t = t.getCause();
		}
		
		HealthCheckStatus status = (t instanceof SocketTimeoutException) ? HealthCheckStatus.TIMEOUT : HealthCheckStatus.ERROR;
		return buildResponse(null, status);
	}
	
	private HealthCheckResponse buildResponse(XML gcXml, HealthCheckStatus status) throws Exception {
		HealthCheckEndpointStatus endpointStatus = new HealthCheckEndpointStatus();
		endpointStatus.setVendor(HealthCheckEndpointVendor.GLOBALCOLLECT);
		endpointStatus.setEndpoint(getEndpoint());
		endpointStatus.setStatus(status);

		HealthCheckResponse response = new HealthCheckResponse();
		response.setHeader(buildHeader());
		
		if (gcXml == null) {
			response.setDecision(Decision.REJECT);
		} else {
			response.setDecision(convertResult(gcXml.getREQUEST().getRESPONSE().getRESULT()));
			response.setErrors(mapErrors(gcXml));
		}
		
		List<HealthCheckEndpointStatus> endpointStatuses = response.getEndpointStatuses();
		endpointStatuses.add(endpointStatus);
		return response;
	}
	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
