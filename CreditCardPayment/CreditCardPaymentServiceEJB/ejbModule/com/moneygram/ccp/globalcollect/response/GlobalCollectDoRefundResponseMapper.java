package com.moneygram.ccp.globalcollect.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectResponse;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.doRefundResponse_v1.ObjectFactory;
import com.moneygram.service.globalcollect.doRefundResponse_v1.Response;
import com.moneygram.service.globalcollect.doRefundResponse_v1.Row;
import com.moneygram.service.globalcollect.doRefundResponse_v1.XML;

public class GlobalCollectDoRefundResponseMapper extends BaseGlobalCollectResponseMapper<RefundTransactionResponse, XML> {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectDoRefundResponseMapper.class);

	public RefundTransactionResponse buildResponse(XML gcXml) throws Exception {
		RefundTransactionResponse response = new RefundTransactionResponse();
		response.setHeader(buildHeader());

		Response gcResponse = gcXml.getREQUEST().getRESPONSE();
		Row gcRow = gcResponse.getROW();
		
		if (gcRow != null) {
			GlobalCollectResponse globalCollect = new GlobalCollectResponse();
			globalCollect.setPaymentReference(gcRow.getPAYMENTREFERENCE());
			globalCollect.setStatusId(gcRow.getSTATUSID());
			response.setGlobalCollect(globalCollect);
		}
		
		response.setDecision(convertResult(gcResponse.getRESULT()));
		response.setErrors(mapErrors(gcXml));
		return response;
	}
	
	public RefundTransactionResponse buildErrorResponse(Throwable t) throws Exception {
		logger.error("An error occured", t);
		throw new SystemException("An error occured in process()", t);
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
