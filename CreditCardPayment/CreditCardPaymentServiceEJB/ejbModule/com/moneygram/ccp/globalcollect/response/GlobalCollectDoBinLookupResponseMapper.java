package com.moneygram.ccp.globalcollect.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.service.creditcardpaymentservice_v1.BinlookupResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.doBinLookupResponse_v1.*;

public class GlobalCollectDoBinLookupResponseMapper extends BaseGlobalCollectResponseMapper<BinlookupResponse, XML> {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectDoBinLookupResponseMapper.class);

	public BinlookupResponse buildResponse(XML gcXml) throws Exception {
		Response gcResponse = gcXml.getREQUEST().getRESPONSE();
		
		BinlookupResponse response = new BinlookupResponse();
		response.setHeader(buildHeader());
		response.setDecision(convertResult(gcResponse.getRESULT()));
		
		Row row = gcResponse.getROW();
		if (row != null) {
			response.setCountryCode(row.getCOUNTRYCODE());
			response.setCardType(convertPaymentProductToCardType(row.getPAYMENTPRODUCTID()));
		}		

		response.setErrors(mapErrors(gcXml));
		return response;
	}
	
	public BinlookupResponse buildErrorResponse(Throwable t) throws Exception {
		logger.error("An error occured", t);
		throw new SystemException("An error occured in process()", t);
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
