package com.moneygram.ccp.globalcollect.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.service.creditcardpaymentservice_v1.CurrencyAmount;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectStatusResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.SystemException;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.ObjectFactory;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Response;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.Status;
import com.moneygram.service.globalcollect.getOrderStatusResponse_v2.XML;

public class GlobalCollectGetOrderStatusResponseMapper extends BaseGlobalCollectResponseMapper<StatusResponse, XML> {
	private static final Logger logger = LoggerFactory.getLogger(GlobalCollectGetOrderStatusResponseMapper.class);

	public StatusResponse buildResponse(XML gcXml) throws Exception {
		Response gcResponse = gcXml.getREQUEST().getRESPONSE();
		
		StatusResponse response = new StatusResponse();
		response.setHeader(buildHeader());
		response.setDecision(convertResult(gcResponse.getRESULT()));
		
		Status status = gcResponse.getSTATUS();
		if (status != null) {
			CurrencyAmount amount = new CurrencyAmount();
			amount.setCurrency(status.getCURRENCYCODE());
			amount.setAmount(longToAmount(status.getAMOUNT()));
			
			GlobalCollectStatusResponse globalCollect = new GlobalCollectStatusResponse();
			globalCollect.setEffortId(status.getEFFORTID());
			globalCollect.setAttemptId(status.getATTEMPTID());
			globalCollect.setPaymentReference(status.getPAYMENTREFERENCE());
			globalCollect.setStatusId(status.getSTATUSID());
			globalCollect.setCavv(status.getCAVV());
			globalCollect.setEci(status.getECI());
			
			response.setAmount(amount);
			response.setGlobalCollect(globalCollect);
		}		

		response.setErrors(mapErrors(gcXml));
		return response;
	}
	
	public StatusResponse buildErrorResponse(Throwable t) throws Exception {
		logger.error("An error occured", t);
		throw new SystemException("An error occured in process()", t);
	}

	public String getSchemaPackageName() {
		return ObjectFactory.class.getPackage().getName();
	}
}
