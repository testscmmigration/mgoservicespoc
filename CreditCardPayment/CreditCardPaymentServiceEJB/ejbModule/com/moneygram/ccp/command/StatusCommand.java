package com.moneygram.ccp.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.BaseCommand;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class StatusCommand extends BaseCommand {
	private static final Logger logger = LoggerFactory.getLogger(StatusCommand.class);

	// Injected
	private GlobalCollectImpl globalCollectImpl;
		
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request) throws CommandException {
		try {
			StatusRequest orderStatusRequest = (StatusRequest)request;
			return globalCollectImpl.process(orderStatusRequest);
		} catch (CommandException e) {
			logger.error("An error occured in process()", e);
			throw(e);
		} catch (Exception e) {
			logger.error("An error occured in process()", e);
			throw new SystemException("An error occured in process()", e);
		}
	}

	public BaseServiceResponseMessage getResponseToReturnError() {
		return new StatusResponse();
	}

	public boolean rollbackOnError() {
		return false;
	}

	protected void validateTransactionContext(ProcessingInstruction context) throws CommandException {
	}

	protected boolean isRequestSupported(BaseServiceRequestMessage request) throws CommandException {
		return (request instanceof StatusRequest);
	}

	public void setGlobalCollectImpl(GlobalCollectImpl globalCollectImpl) {
		this.globalCollectImpl = globalCollectImpl;
	}
}
