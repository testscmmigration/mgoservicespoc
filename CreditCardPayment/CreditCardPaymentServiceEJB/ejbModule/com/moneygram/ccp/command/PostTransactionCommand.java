package com.moneygram.ccp.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.ccp.domain.CardBrandType;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.PostTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.BaseCommand;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class PostTransactionCommand extends BaseCommand {
	
	private static int GLOBAL_COLLECT_AUTHENTICATED_STATUS = 200;
	private static int GLOBAL_COLLECT_AUTHORIZED_PENDING_STATUS = 600;
	
	private static final Logger logger = 
            LoggerFactory.getLogger(PostTransactionCommand.class);

	// Injected
	private GlobalCollectImpl globalCollectImpl;
	private GlobalCollectImpl globalCollectDoFinishPaymentImpl;
	private GlobalCollectImpl globalCollectGetOrderStatusImpl;
		
	protected BaseServiceResponseMessage process(
            BaseServiceRequestMessage request) throws CommandException {
	    try {
			PostTransactionRequest postTransactionRequest =
                (PostTransactionRequest)request;

			if (postTransactionRequest.getCard()
			        .getCardBrandType()
			        .equals(CardBrandType.MAESTRO.getName())) {
				return processMaestroTransaction(postTransactionRequest);
			}else{
				return globalCollectImpl.process(postTransactionRequest);
			}
		} catch (CommandException e) {
			logger.error("An error occured in process()", e);
			throw(e);
		} catch (Exception e) {
			logger.error("An error occured in process()", e);
			throw new SystemException("An error occured in process()", e);
		}
	}

	public BaseServiceResponseMessage getResponseToReturnError() {
		return new PostTransactionResponse();
	}

	public boolean rollbackOnError() {
		return false;
	}

	protected void validateTransactionContext(ProcessingInstruction context) throws CommandException {
	}

	protected boolean isRequestSupported(BaseServiceRequestMessage request) throws CommandException {
		return (request instanceof PostTransactionRequest);
	}

	public void setGlobalCollectImpl(GlobalCollectImpl globalCollectImpl) {
		this.globalCollectImpl = globalCollectImpl;
	}

	public void setGlobalCollectDoFinishPaymentImpl(
			GlobalCollectImpl globalCollectDoFinishPaymentImpl) {
		this.globalCollectDoFinishPaymentImpl = globalCollectDoFinishPaymentImpl;
	}
	
	private BaseServiceResponseMessage processMaestroTransaction(PostTransactionRequest postTransactionRequest) throws CommandException{
		GlobalCollectRequest globalCollectRequest = new GlobalCollectRequest();
		globalCollectRequest.setOrderid(postTransactionRequest.getGlobalCollect().getOrderid());
		
		StatusRequest statusRequest = new StatusRequest();
		statusRequest.setMerchantID(postTransactionRequest.getMerchantID());
		statusRequest.setMGIReferenceCode(postTransactionRequest.getMGIReferenceCode());
		statusRequest.setGlobalCollect(globalCollectRequest);
		
		StatusResponse statusResponse = (StatusResponse)globalCollectGetOrderStatusImpl.process(statusRequest);
		
		if(statusResponse.getGlobalCollect().getStatusId().intValue() == GLOBAL_COLLECT_AUTHENTICATED_STATUS){
			return globalCollectDoFinishPaymentImpl.process(postTransactionRequest);
		}else if(statusResponse.getGlobalCollect().getStatusId().intValue() == GLOBAL_COLLECT_AUTHORIZED_PENDING_STATUS){
			return globalCollectImpl.process(postTransactionRequest);
		//This else routine is made to ensure something is going to be sent by the service and that method validates the current status
		//of the transaction
		}else{
			return globalCollectDoFinishPaymentImpl.process(postTransactionRequest);
		}
	}
	
	public synchronized void setGlobalCollectGetOrderStatusImpl(GlobalCollectImpl globalCollectGetOrderStatusImpl) {
		this.globalCollectGetOrderStatusImpl = globalCollectGetOrderStatusImpl;
	}
}
