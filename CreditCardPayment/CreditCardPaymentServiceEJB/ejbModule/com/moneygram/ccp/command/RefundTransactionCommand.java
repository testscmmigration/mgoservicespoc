package com.moneygram.ccp.command;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.ccp.cybersource.CybersourceAuthorizeImpl;
import com.moneygram.ccp.cybersource.CybersourceRefundImpl;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.ccp.globalcollect.domain.PaymentStatusId;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionRequest;
import com.moneygram.service.creditcardpaymentservice_v1.RefundTransactionResponse;
import com.moneygram.service.creditcardpaymentservice_v1.StatusRequest;
import com.moneygram.service.creditcardpaymentservice_v1.StatusResponse;
import com.moneygram.service.framework.command.BaseCommand;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class RefundTransactionCommand extends BaseCommand {
	private static final Logger logger = LoggerFactory.getLogger(RefundTransactionCommand.class);

	// Injected
	private GlobalCollectImpl globalCollectGetOrderStatusImpl;
	private GlobalCollectImpl globalCollectCancelSetPaymentImpl;
	private GlobalCollectImpl globalCollectDoRefundImpl;
	private CybersourceRefundImpl cybersourceRefundImpl;
	
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request) throws CommandException {
		try {
			RefundTransactionRequest refundTransactionRequest = (RefundTransactionRequest)request;
			
			String merchantId = refundTransactionRequest.getMerchantID();

			if (!StringUtils.isNumeric(merchantId)) {
				// Use Cybersource for Non numeric Merchant ID
				return cybersourceRefundImpl.process(refundTransactionRequest);
			} else {

			// Execute the get status command.
			StatusResponse statusResponse = getPaymentStatus(refundTransactionRequest);
			Integer statusId = statusResponse.getGlobalCollect().getStatusId();
			
			if (statusId != null) {
				// Transaction has not been settled by the bank, so do a cancel_setpayment.
				if (statusId == PaymentStatusId.PAYMENT_STATUS_READY) {
					Float statusAmount = statusResponse.getAmount().getAmount();
					Float transactionAmount = refundTransactionRequest.getAmount().getAmount();
			
					if (statusAmount.floatValue() == transactionAmount.floatValue() ) {
						return globalCollectCancelSetPaymentImpl.process(refundTransactionRequest);
					} else {
						// Partial cancels are not supported.
						throw new IllegalStateException("Partial refunds that have not been settled yet are not supported.");
					}

				// Transaction has been settled by the bank, so do a do_cancel, so do a do_refund.
				} else if (statusId >= PaymentStatusId.PAYMENT_STATUS_PROCESSED ) {
					return globalCollectDoRefundImpl.process(refundTransactionRequest);
				}
			}
			
			throw new IllegalStateException("Invalid Global Collect Payment Status Id: " + statusId);
			}
		} catch (CommandException e) {
			logger.error("An error occured in process()", e);
			throw(e);
		} catch (Exception e) {
			logger.error("An error occured in process()", e);
			throw new SystemException("An error occured in process()", e);
		}
	}
	
	protected StatusResponse getPaymentStatus(RefundTransactionRequest refundTransactionRequest) throws CommandException {
		GlobalCollectRequest globalCollectRequest = new GlobalCollectRequest();
		globalCollectRequest.setOrderid(refundTransactionRequest.getGlobalCollect().getOrderid());
		
		StatusRequest statusRequest = new StatusRequest();
		statusRequest.setMerchantID(refundTransactionRequest.getMerchantID());
		statusRequest.setMGIReferenceCode(refundTransactionRequest.getMGIReferenceCode());
		statusRequest.setGlobalCollect(globalCollectRequest);
		
		return (StatusResponse)globalCollectGetOrderStatusImpl.process(statusRequest);
	}

	public BaseServiceResponseMessage getResponseToReturnError() {
		return new RefundTransactionResponse();
	}

	public boolean rollbackOnError() {
		return false;
	}

	protected void validateTransactionContext(ProcessingInstruction context) throws CommandException {
	}

	protected boolean isRequestSupported(BaseServiceRequestMessage request) throws CommandException {
		return (request instanceof RefundTransactionRequest);
	}

	public synchronized void setGlobalCollectGetOrderStatusImpl(GlobalCollectImpl globalCollectGetOrderStatusImpl) {
		this.globalCollectGetOrderStatusImpl = globalCollectGetOrderStatusImpl;
	}

	public synchronized void setGlobalCollectCancelSetPaymentImpl(GlobalCollectImpl globalCollectCancelSetPaymentImpl) {
		this.globalCollectCancelSetPaymentImpl = globalCollectCancelSetPaymentImpl;
	}

	public synchronized void setGlobalCollectDoRefundImpl(GlobalCollectImpl globalCollectDoRefundImpl) {
		this.globalCollectDoRefundImpl = globalCollectDoRefundImpl;
	}
	public void setCybersourceRefundImpl(CybersourceRefundImpl cybersourceRefundImpl) {
		this.cybersourceRefundImpl = cybersourceRefundImpl;
	}

}
