package com.moneygram.ccp.command;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.ccp.cybersource.CybersourceAuthorizeImpl;
import com.moneygram.ccp.globalcollect.GlobalCollectImpl;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationRequest;
import com.moneygram.service.creditcardpaymentservice_v1.AuthorizationResponse;
import com.moneygram.service.creditcardpaymentservice_v1.BinLookupRequest;
import com.moneygram.service.creditcardpaymentservice_v1.BinlookupResponse;
import com.moneygram.service.creditcardpaymentservice_v1.CCPErrors;
import com.moneygram.service.creditcardpaymentservice_v1.CardType;
import com.moneygram.service.creditcardpaymentservice_v1.GlobalCollectRequest;
import com.moneygram.service.framework.command.BaseCommand;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.SystemException;

public class AuthorizationCommand extends BaseCommand {
	protected static final String GC_ORDER_STATUS_NOT_FOUND_ERROR_CODE = "300240";
	
	protected static final String GC_DOPAYMENT_ORDER_NOT_FOUND = "400220";

	private static final Logger logger = LoggerFactory.getLogger(AuthorizationCommand.class);

	// Injected
	private CybersourceAuthorizeImpl cybersourceAuthorizeImpl;
	private GlobalCollectImpl globalCollectInsertOrderWithPaymentImpl;
	private GlobalCollectImpl globalCollectDoPaymentImpl;
	private GlobalCollectImpl globalCollectDoBinLookupImpl;

	public void setGlobalCollectDoBinLookupImpl(GlobalCollectImpl globalCollectDoBinLookupImpl) {
		this.globalCollectDoBinLookupImpl = globalCollectDoBinLookupImpl;
	}

	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request) throws CommandException {
		try {
			AuthorizationRequest authorizationRequest = (AuthorizationRequest) request;
			String merchantId = authorizationRequest.getMerchantID();

			if (!StringUtils.isNumeric(merchantId)) {
				// Use Cybersource for Non numeric Merchant ID
				return cybersourceAuthorizeImpl.process(authorizationRequest);
			} else {
				// Use the Global Collect for all other countries.
				GlobalCollectRequest globalCollect = authorizationRequest.getGlobalCollect();
				if (globalCollect == null) {
					throw new SystemException("GlobalCollect element is required for the numeric MerchantID " + merchantId);
				}

				Long effortId = globalCollect.getEffortId();
				if (effortId != null && effortId < 1) {
					throw new SystemException("Effort id must be greater than 1 for GlobalCollect transactions: " + effortId);
				}

				//Per Andy's Email 5/10/12 Moving this Logic for All Authorization
		        if (authorizationRequest.getCard() != null && CardType.UNKNOWN == authorizationRequest.getCard().getCardType()) {
		          // IF cardType is not present try to set it
		          try {
		            BinLookupRequest binRequest = new BinLookupRequest();
		            binRequest.setAccountNumber(authorizationRequest.getCard().getAccountNumber());
		            binRequest.setMerchantID(authorizationRequest.getMerchantID());
		            BinlookupResponse binlookupresponse = (BinlookupResponse) globalCollectDoBinLookupImpl.process(binRequest);           
		            authorizationRequest.getCard().setCardType(binlookupresponse.getCardType());

		          } catch (Exception e) {
		            logger.warn("Bin lookup Failed Not Setting CardType", e);
		          }
		        }
		        /*MGO-4559: Because all calls will be made with effortId=1, globalCollectInsertOrderWithPaymentImpl will be called always*/
		        AuthorizationResponse response = (AuthorizationResponse) globalCollectInsertOrderWithPaymentImpl.process(authorizationRequest);
		        CardType cardType = authorizationRequest.getCard()== null ? CardType.UNKNOWN : authorizationRequest.getCard().getCardType();
		        response.setCardType(cardType);
		        return response;
		      }

		} catch (CommandException e) {
			logger.error("An error occured in process()", e);
			throw (e);
		} catch (Exception e) {
			logger.error("An error occured in process()", e);
			throw new SystemException("An error occured in process()", e);
		}
	}

	public BaseServiceResponseMessage getResponseToReturnError() {
		return new AuthorizationResponse();
	}

	public boolean rollbackOnError() {
		return false;
	}

	protected void validateTransactionContext(ProcessingInstruction context) throws CommandException {
	}

	protected boolean isRequestSupported(BaseServiceRequestMessage request) throws CommandException {
		return (request instanceof AuthorizationRequest);
	}

	public void setCybersourceAuthorizeImpl(CybersourceAuthorizeImpl cybersourceAuthorizeImpl) {
		this.cybersourceAuthorizeImpl = cybersourceAuthorizeImpl;
	}

	public void setGlobalCollectInsertOrderWithPaymentImpl(GlobalCollectImpl globalCollectInsertOrderWithPaymentImpl) {
		this.globalCollectInsertOrderWithPaymentImpl = globalCollectInsertOrderWithPaymentImpl;
	}

	public void setGlobalCollectDoPaymentImpl(GlobalCollectImpl globalCollectDoPaymentImpl) {
		this.globalCollectDoPaymentImpl = globalCollectDoPaymentImpl;
	}
}
