package com.moneygram.ccp.servlet;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import com.moneygram.ccp.client.CreditCardPaymentServicePortType;
import com.moneygram.ccp.client.CreditCardPaymentServiceV1;
import com.moneygram.ccp.client.Error;
import com.moneygram.ccp.client.Header;
import com.moneygram.ccp.client.HealthCheckEndpointStatus;
import com.moneygram.ccp.client.HealthCheckRequest;
import com.moneygram.ccp.client.HealthCheckResponse;
import com.moneygram.ccp.client.HealthCheckStatus;
import com.moneygram.ccp.client.ProcessingInstruction;

/**
 * HealthCheck Servlet
 */
public class GlobalCollectHealthCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GlobalCollectHealthCheckServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}
	
	protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), 
					request.getContextPath()+ "/CreditCardPaymentService_v1?wsdl");
				
		    QName qname = new QName("http://moneygram.com/service/CreditCardPaymentService_v1", "CreditCardPaymentService_v1");
		    CreditCardPaymentServiceV1 service = new CreditCardPaymentServiceV1(url, qname);
		    CreditCardPaymentServicePortType port = service.getCreditCardPaymentServicePort(); 
	
			HealthCheckRequest req = buildHealthCheckRequest();
	        HealthCheckResponse healthCheckResponse = port.healthCheck(req);
			        
			String resp = processResponse(healthCheckResponse);
			response.getWriter().write(resp);
		} catch (Error e) {
			throw new ServletException("An Error occurred in process()", e);
		}
	}
	
	protected HealthCheckRequest buildHealthCheckRequest() {
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction("healthCheck");
		pi.setReturnErrorsAsException(true);
		
		Header header = new Header();
		header.setProcessingInstruction(pi);
		
		HealthCheckRequest request = new HealthCheckRequest();
		request.setHeader(header);
		return request;
	}
	
	protected String processResponse(HealthCheckResponse healthCheckResponse) {
		List<HealthCheckEndpointStatus> statusList = healthCheckResponse.getEndpointStatus();
		
		if (!statusList.isEmpty()) {
			HealthCheckEndpointStatus endpointStatus = statusList.get(0);
			HealthCheckStatus status = endpointStatus.getStatus();
			String endpoint = endpointStatus.getEndpoint();
			String mainStatus = (status == HealthCheckStatus.SUCCESS) ? "UP" : "DOWN";
			
			return mainStatus + ";Endpoint=" + formatHealthCheckStatus(status) + "(" + endpoint + ")";
		} else {
			return "DOWN";
		}
	}
	
	protected String formatHealthCheckStatus(HealthCheckStatus status) {
		switch(status) {
			case ERROR:
				return "ERROR";
				
			case SUCCESS:
				return "GOOD";
				
			case TIMEOUT:
				return "TIMEOUT";
				
			default:
				return "UNKNOWN";
		}
	}
}
