//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-05/12/2011 11:54 AM(foreman)-)
//


package com.moneygram.ccp.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatusResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/service/CreditCardPaymentService_v1}BaseCreditCardPaymentResponse">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://moneygram.com/service/CreditCardPaymentService_v1}CurrencyAmount"/>
 *         &lt;element name="globalCollect" type="{http://moneygram.com/service/CreditCardPaymentService_v1}GlobalCollectStatusResponse"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusResponse", propOrder = {
    "amount",
    "globalCollect"
})
public class StatusResponse
    extends BaseCreditCardPaymentResponse
{

    @XmlElement(required = true)
    protected CurrencyAmount amount;
    @XmlElement(required = true)
    protected GlobalCollectStatusResponse globalCollect;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyAmount }
     *     
     */
    public CurrencyAmount getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyAmount }
     *     
     */
    public void setAmount(CurrencyAmount value) {
        this.amount = value;
    }

    /**
     * Gets the value of the globalCollect property.
     * 
     * @return
     *     possible object is
     *     {@link GlobalCollectStatusResponse }
     *     
     */
    public GlobalCollectStatusResponse getGlobalCollect() {
        return globalCollect;
    }

    /**
     * Sets the value of the globalCollect property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlobalCollectStatusResponse }
     *     
     */
    public void setGlobalCollect(GlobalCollectStatusResponse value) {
        this.globalCollect = value;
    }

}
