/*
 * Created on Jun 17, 2009
 *
 */
package com.moneygram.rsa.service.handler;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.web.BaseSoapRequestHandler;
import com.moneygram.common.service.web.ServiceException;
import com.moneygram.mgo.service.rsa.AnalyzeEventRequest;
import com.moneygram.mgo.service.rsa.AnalyzeEventResponse;
import com.moneygram.mgo.service.rsa.AnalyzeIdentityRequest;
import com.moneygram.mgo.service.rsa.AnalyzeIdentityResponse;
import com.moneygram.mgo.service.rsa.AuthenticateRequest;
import com.moneygram.mgo.service.rsa.AuthenticateResponse;
import com.moneygram.mgo.service.rsa.ChallengeRequest;
import com.moneygram.mgo.service.rsa.ChallengeResponse;
import com.moneygram.mgo.service.rsa.GetUserImageRequest;
import com.moneygram.mgo.service.rsa.GetUserImageResponse;
import com.moneygram.mgo.service.rsa.GetVeridKBAQuestionsRequest;
import com.moneygram.mgo.service.rsa.GetVeridKBAQuestionsResponse;
import com.moneygram.mgo.service.rsa.NotifyRequest;
import com.moneygram.mgo.service.rsa.NotifyResponse;
import com.moneygram.mgo.service.rsa.QueryRequest;
import com.moneygram.mgo.service.rsa.QueryResponse;
import com.moneygram.mgo.service.rsa.SubmitVeridKBAAnswersRequest;
import com.moneygram.mgo.service.rsa.SubmitVeridKBAAnswersResponse;
import com.moneygram.mgo.service.rsa.UpdateUserRequest;
import com.moneygram.mgo.service.rsa.UpdateUserResponse;

/**
 * 
 * RSA Service Soap Handler.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOTransactionService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2010/06/22 21:58:09 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RSAServiceSoapHandler extends BaseSoapRequestHandler {
    private static final Logger logger = LogFactory.getInstance().getLogger(RSAServiceSoapHandler.class);

    /**
     * 
     * Creates new instance of TransactionServiceSoapHandler.
     */
    public RSAServiceSoapHandler() {
        super();
        setServiceFactory(new RSAServiceFactory());
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public GetVeridKBAQuestionsResponse getKBAQuestions(GetVeridKBAQuestionsRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getVeridKBAQuestions: request=" + request);
        }
        return (GetVeridKBAQuestionsResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public SubmitVeridKBAAnswersResponse submitKBAAnswers(SubmitVeridKBAAnswersRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("submitVeridKBAAnswers: request=" + request);
        }
        return (SubmitVeridKBAAnswersResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public AnalyzeEventResponse analyzeEvent(AnalyzeEventRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("analyzeEvent: request=" + request);
        }
        return (AnalyzeEventResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public AnalyzeIdentityResponse analyzeIdentity(AnalyzeIdentityRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("analyzeIdentity: request=" + request);
        }
        return (AnalyzeIdentityResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public QueryResponse query(QueryRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("query: request=" + request);
        }
        return (QueryResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public UpdateUserResponse updateUser(UpdateUserRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("updateUser: request=" + request);
        }
        return (UpdateUserResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public ChallengeResponse challenge(ChallengeRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("challenge: request=" + request);
        }
        return (ChallengeResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public AuthenticateResponse authenticate(AuthenticateRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("authenticate: request=" + request);
        }
        return (AuthenticateResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public NotifyResponse notify(NotifyRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("notify: request=" + request);
        }
        return (NotifyResponse) process(request);
    }

    /**
     * 
     * @param request
     * @return
     * @throws ServiceException
     */
    public GetUserImageResponse getUserImage(GetUserImageRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("getUserImage: request=" + request);
        }
        return (GetUserImageResponse) process(request);
    }

    @Override
    protected String getServiceJndiName() {
        return "java:comp/env/ejb/RSAService";
    }

}
