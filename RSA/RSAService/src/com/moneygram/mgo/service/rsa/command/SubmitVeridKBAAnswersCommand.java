/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import integration.hawaii.verid.com.AnswerType;
import integration.hawaii.verid.com.AnswersType;
import integration.hawaii.verid.com.ChoicesType;
import integration.hawaii.verid.com.ModeType;
import integration.hawaii.verid.com.ResultType;
import integration.hawaii.verid.com.SimulatorModeType;
import integration.hawaii.verid.com.TransactionContinueType;
import integration.hawaii.verid.com.TransactionResponseType;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.MultipleChoiceAnswer;
import com.moneygram.mgo.service.rsa.SubmitVeridKBAAnswersRequest;
import com.moneygram.mgo.service.rsa.SubmitVeridKBAAnswersResponse;
import com.moneygram.mgo.service.rsa.util.RSAResourceConfig;
import com.verid.netview.transact.VeridAuthenticationLocator;
import com.verid.netview.transact.VeridAuthenticationPortStub;

/**
 * 
 * Submit Verid KBA Answers Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2009/10/08 21:35:18 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SubmitVeridKBAAnswersCommand extends BaseVeridKBACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(SubmitVeridKBAAnswersCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request=" + request);
        }

        SubmitVeridKBAAnswersRequest commandRequest = (SubmitVeridKBAAnswersRequest) request;

        //create verid request
        TransactionContinueType tc = createVeridAnswersRequest(commandRequest);

        TransactionResponseType transResponse = null;
        try {
            //prepare soap call
            VeridAuthenticationLocator val = new VeridAuthenticationLocator();
            VeridAuthenticationPortStub authentication = (VeridAuthenticationPortStub) val.getVeridAuthenticationPort();
            authentication.setUsername(RSAResourceConfig.getInstance().getVeridUserName());
            authentication.setPassword(RSAResourceConfig.getInstance().getVeridPassword());

            //TODO: AddProfileComment(profile, "Responding back to Verid with answers(" + veridTrans.getQuestions().length + ")");

            // Call Verid.
            transResponse = authentication.continueTransaction(tc);
        } catch (Exception e) {
            throw new CommandException("Failed Verid submit answers request due to an unexpected exception", e);
        }

        SubmitVeridKBAAnswersResponse response = new SubmitVeridKBAAnswersResponse();
        response.setVendorAuthenticationEventId(String.valueOf(transResponse.getTransactionNumber()));
        response.setResultType(com.moneygram.mgo.service.rsa.ResultType.fromString(transResponse.getResult().getValue()));
        
        // Get Verid Results.
        if (ResultType.questions.equals(transResponse.getResult())) {
            //got questions response
            //TODO: AddProfileComment(profile, "Failed first set of questions, returning second set of questions from Verid.");
            response.setQuestions(setQuestions(transResponse));
        } else if (ResultType.passed.equals(transResponse.getResult())) {
            //TODO: AddProfileComment(profile, "Passed Verid Authentication.");
        } else {
            //got failed or error response
            //TODO: A Verid Profile comment will be logged in the VeridError
            response.setAdditionalInformation(getAdditionalInformation(transResponse));
        }
        
        return response;
    }

    /**
     * Returns populated TransactionContinueType instance.
     * @param commandRequest
     * @return
     * @throws CommandException
     */
    private TransactionContinueType createVeridAnswersRequest(SubmitVeridKBAAnswersRequest commandRequest) throws CommandException {
        MultipleChoiceAnswer[] answers = commandRequest.getAnswers();

        if (answers == null) {
            throw new CommandException("Answers data is missing in the request");
        }

        AnswersType veridAnswers = new AnswersType();
        AnswerType[] answersArray = new AnswerType[answers.length];

        for (int i = 0; i < answers.length; i++) {
            AnswerType veridAnswer = new AnswerType();
            MultipleChoiceAnswer answer = answers[i]; 

            //convert to verid long id
            Long questionId = null;
            try {
                questionId = new Long(answer.getQuestionId());
            } catch (Exception e) {
                throw new DataFormatException("Invalid question id value="+ answer.getQuestionId(), e);
            }
            
            if (questionId == null) {
                throw new DataFormatException("Question id value can not be null");
            }
            
            veridAnswer.setQuestionId(questionId.longValue());

            //set choices with a single answer
            ChoicesType choices = new ChoicesType(new long[]{answer.getAnswerId()});
            veridAnswer.setChoices(choices);
            
            answersArray[i] = veridAnswer;
        }
        veridAnswers.setAnswer(answersArray);
        veridAnswers.setQuestionSetId(commandRequest.getQuestionsSetId());
        
        TransactionContinueType tc = new TransactionContinueType();
        tc.setAccountName(RSAResourceConfig.getInstance().getVeridAccountName());
        tc.setTransactionNumber(Long.valueOf(commandRequest.getVendorAuthenticationEventId()));
        tc.setAccountsTransactionId(String.valueOf(commandRequest.getEventSequenceNumber()));
        tc.setAnswers(veridAnswers);

        SimulatorModeType simulatorModeType = createSimulatorModeType(commandRequest);
        if (simulatorModeType != null) {
            tc.setSimulatorMode(simulatorModeType);
        }

        return tc;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof SubmitVeridKBAAnswersRequest;
    }

}
