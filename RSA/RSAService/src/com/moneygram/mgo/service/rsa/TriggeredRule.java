/**
 * TriggeredRule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Triggered Rule.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/31 21:46:05 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TriggeredRule  implements java.io.Serializable {
    private static final long serialVersionUID = 3231736670596253737L;

    private ActionCode actionCode;

    private java.lang.String ruleId;

    private java.lang.String ruleName;

    public TriggeredRule() {
    }

    public TriggeredRule(
           ActionCode actionCode,
           java.lang.String ruleId,
           java.lang.String ruleName) {
           this.actionCode = actionCode;
           this.ruleId = ruleId;
           this.ruleName = ruleName;
    }


    /**
     * Gets the actionCode value for this TriggeredRule.
     * 
     * @return actionCode
     */
    public ActionCode getActionCode() {
        return actionCode;
    }


    /**
     * Sets the actionCode value for this TriggeredRule.
     * 
     * @param actionCode
     */
    public void setActionCode(ActionCode actionCode) {
        this.actionCode = actionCode;
    }


    /**
     * Gets the ruleId value for this TriggeredRule.
     * 
     * @return ruleId
     */
    public java.lang.String getRuleId() {
        return ruleId;
    }


    /**
     * Sets the ruleId value for this TriggeredRule.
     * 
     * @param ruleId
     */
    public void setRuleId(java.lang.String ruleId) {
        this.ruleId = ruleId;
    }


    /**
     * Gets the ruleName value for this TriggeredRule.
     * 
     * @return ruleName
     */
    public java.lang.String getRuleName() {
        return ruleName;
    }


    /**
     * Sets the ruleName value for this TriggeredRule.
     * 
     * @param ruleName
     */
    public void setRuleName(java.lang.String ruleName) {
        this.ruleName = ruleName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TriggeredRule)) return false;
        TriggeredRule other = (TriggeredRule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actionCode==null && other.getActionCode()==null) || 
             (this.actionCode!=null &&
              this.actionCode.equals(other.getActionCode()))) &&
            ((this.ruleId==null && other.getRuleId()==null) || 
             (this.ruleId!=null &&
              this.ruleId.equals(other.getRuleId()))) &&
            ((this.ruleName==null && other.getRuleName()==null) || 
             (this.ruleName!=null &&
              this.ruleName.equals(other.getRuleName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActionCode() != null) {
            _hashCode += getActionCode().hashCode();
        }
        if (getRuleId() != null) {
            _hashCode += getRuleId().hashCode();
        }
        if (getRuleName() != null) {
            _hashCode += getRuleName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }


    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ActionCode=").append(getActionCode());
        buffer.append(" RuleId=").append(getRuleId());
        buffer.append(" RuleName=").append(getRuleName());
        return buffer.toString();
    }
}
