/*
 * Created on Aug 5, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import integration.hawaii.verid.com.ChoiceType;
import integration.hawaii.verid.com.QuestionType;
import integration.hawaii.verid.com.QuestionsType;
import integration.hawaii.verid.com.ResultType;
import integration.hawaii.verid.com.SimulatorModeType;
import integration.hawaii.verid.com.TransactionResponseType;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.rsa.AdditionalInformation;
import com.moneygram.mgo.service.rsa.AnswerType;
import com.moneygram.mgo.service.rsa.MultipleChoicePossibleAnswer;
import com.moneygram.mgo.service.rsa.MultipleChoiceQuestion;
import com.moneygram.mgo.service.rsa.MultipleChoiceQuestions;
import com.moneygram.mgo.service.rsa.util.RSAResourceConfig;

/**
 * 
 * Base Verid KBA Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/10/08 21:35:18 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseVeridKBACommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseVeridKBACommand.class);
    
    /**
     * Retrieves questions from Verid response.
     * @param transResponse
     * @return
     */
    protected MultipleChoiceQuestions setQuestions(TransactionResponseType transResponse) {
        QuestionsType veridQuestions = transResponse.getQuestions();

        MultipleChoiceQuestion[] questions = new MultipleChoiceQuestion[veridQuestions.getQuestion().length];
        
        for (int q = 0; q < veridQuestions.getQuestion().length; q++) {
            QuestionType[] veridQuestion = veridQuestions.getQuestion();

            MultipleChoiceQuestion question = new MultipleChoiceQuestion();
            question.setQuestionId(String.valueOf(veridQuestion[q].getId()));
            question.setQuestionText(veridQuestion[q].getText());
            question.setQuestionHelpText(veridQuestion[q].getHelpText());
            question.setAnswerType(AnswerType.fromString(veridQuestion[q].getAnswerType().toString()));

            ChoiceType[] veridChoices = veridQuestion[q].getChoice();
        
            MultipleChoicePossibleAnswer[] choices = new MultipleChoicePossibleAnswer[veridChoices.length];
            for (int c = 0; c < veridChoices.length; c++) {
                MultipleChoicePossibleAnswer choice = new MultipleChoicePossibleAnswer();
                choice.setChoiceId(veridChoices[c].getId());
                choice.setChoiceText(veridChoices[c].getText());
                choices[c] = choice;
            }
            question.setChoices(choices);
            questions[q] = question;
        }
        
        return new MultipleChoiceQuestions(transResponse.getQuestions().getQuestionSetId(), questions);
    }

    /**
     * Returns AdditionalInformation instance with data extracted from TransactionResponseType.
     * @param transResponse
     * @return
     */
    protected AdditionalInformation getAdditionalInformation(TransactionResponseType transResponse) {
        VeridResponseWrapper wrapper = null;

        if (ResultType.error.equals(transResponse.getResult())) {
            wrapper = new VeridErrorResponseWrapper(transResponse.getVerboseDetails().getError());
        } else if (ResultType.failed.equals(transResponse.getResult())) {
            wrapper = new VeridFailResponseWrapper(transResponse.getVerboseDetails().getFailed());
        } else {
            throw new RuntimeException("Invalid response result type: "+ transResponse.getResult());
        }

        AdditionalInformation additionalInformation = new AdditionalInformation(wrapper.getCode(), wrapper.getDescription());
        
        logger.info("Received error / failed response from Verid. Additional Information="+ additionalInformation);
        
        return additionalInformation;
    }

    /**
     * Returns an instance of the SimulatorModeType created from request.
     * @param request
     * @return
     */
    protected SimulatorModeType createSimulatorModeType(OperationRequest request) {
        SimulatorModeType mode = null;
        if (request.getHeader().getProcessingInstruction().getSimulatedModeAction() != null) {
            mode = SimulatorModeType.fromValue(request.getHeader().getProcessingInstruction().getSimulatedModeAction());
        }
        return mode;
    }

    @Override
    public boolean isInSimulatedMode() throws CommandException {
        return RSAResourceConfig.getInstance().isVeridModeSimulated();
    }

}