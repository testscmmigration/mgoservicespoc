/**
 * ResultType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Result Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/05 15:31:52 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ResultType implements java.io.Serializable {
    private static final long serialVersionUID = 422053025711075922L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ResultType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _questions = "questions";
    public static final java.lang.String _passed = "passed";
    public static final java.lang.String _failed = "failed";
    public static final java.lang.String _error = "error";
    public static final ResultType questions = new ResultType(_questions);
    public static final ResultType passed = new ResultType(_passed);
    public static final ResultType failed = new ResultType(_failed);
    public static final ResultType error = new ResultType(_error);
    public java.lang.String getValue() { return _value_;}
    public static ResultType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ResultType enumeration = (ResultType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ResultType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}

}
