/**
 * BaseQuestion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Base Question.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class BaseQuestion  implements java.io.Serializable {
    private static final long serialVersionUID = 1688528949437445683L;

    private java.lang.String questionId;

    private java.lang.String questionText;

    private java.lang.String questionHelpText;

    public BaseQuestion() {
    }

    public BaseQuestion(
           java.lang.String questionId,
           java.lang.String questionText,
           java.lang.String questionHelpText) {
           this.questionId = questionId;
           this.questionText = questionText;
           this.questionHelpText = questionHelpText;
    }


    /**
     * Gets the questionId value for this BaseQuestion.
     * 
     * @return questionId
     */
    public java.lang.String getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this BaseQuestion.
     * 
     * @param questionId
     */
    public void setQuestionId(java.lang.String questionId) {
        this.questionId = questionId;
    }


    /**
     * Gets the questionText value for this BaseQuestion.
     * 
     * @return questionText
     */
    public java.lang.String getQuestionText() {
        return questionText;
    }


    /**
     * Sets the questionText value for this BaseQuestion.
     * 
     * @param questionText
     */
    public void setQuestionText(java.lang.String questionText) {
        this.questionText = questionText;
    }


    /**
     * Gets the questionHelpText value for this BaseQuestion.
     * 
     * @return questionHelpText
     */
    public java.lang.String getQuestionHelpText() {
        return questionHelpText;
    }


    /**
     * Sets the questionHelpText value for this BaseQuestion.
     * 
     * @param questionHelpText
     */
    public void setQuestionHelpText(java.lang.String questionHelpText) {
        this.questionHelpText = questionHelpText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaseQuestion)) return false;
        BaseQuestion other = (BaseQuestion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.questionId==null && other.getQuestionId()==null) || 
             (this.questionId!=null &&
              this.questionId.equals(other.getQuestionId()))) &&
            ((this.questionText==null && other.getQuestionText()==null) || 
             (this.questionText!=null &&
              this.questionText.equals(other.getQuestionText()))) &&
            ((this.questionHelpText==null && other.getQuestionHelpText()==null) || 
             (this.questionHelpText!=null &&
              this.questionHelpText.equals(other.getQuestionHelpText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQuestionId() != null) {
            _hashCode += getQuestionId().hashCode();
        }
        if (getQuestionText() != null) {
            _hashCode += getQuestionText().hashCode();
        }
        if (getQuestionHelpText() != null) {
            _hashCode += getQuestionHelpText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" QuestionId=").append(getQuestionId());
        buffer.append(" QuestionText=").append(getQuestionText());
        buffer.append(" QuestionHelpText=").append(getQuestionHelpText());
        return buffer.toString();
    }
}
