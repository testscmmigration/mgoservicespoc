/**
 * MultipleChoiceQuestion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;


/**
 * 
 * Multiple Choice Question.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/08/31 21:49:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MultipleChoiceQuestion  extends BaseQuestion {
    private static final long serialVersionUID = -2756473870589514639L;

    private AnswerType answerType;

    private MultipleChoicePossibleAnswer[] choices;

    public MultipleChoiceQuestion() {
    }

    public MultipleChoiceQuestion(
           java.lang.String questionId,
           java.lang.String questionText,
           java.lang.String questionHelpText,
           AnswerType answerType,
           MultipleChoicePossibleAnswer[] choices) {
        super(
            questionId,
            questionText,
            questionHelpText);
        this.answerType = answerType;
        this.choices = choices;
    }


    /**
     * Gets the answerType value for this MultipleChoiceQuestion.
     * 
     * @return answerType
     */
    public AnswerType getAnswerType() {
        return answerType;
    }


    /**
     * Sets the answerType value for this MultipleChoiceQuestion.
     * 
     * @param answerType
     */
    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }


    /**
     * Gets the choices value for this MultipleChoiceQuestion.
     * 
     * @return choices
     */
    public MultipleChoicePossibleAnswer[] getChoices() {
        return choices;
    }


    /**
     * Sets the choices value for this MultipleChoiceQuestion.
     * 
     * @param choices
     */
    public void setChoices(MultipleChoicePossibleAnswer[] choices) {
        this.choices = choices;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MultipleChoiceQuestion)) return false;
        MultipleChoiceQuestion other = (MultipleChoiceQuestion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answerType==null && other.getAnswerType()==null) || 
             (this.answerType!=null &&
              this.answerType.equals(other.getAnswerType()))) &&
            ((this.choices==null && other.getChoices()==null) || 
             (this.choices!=null &&
              java.util.Arrays.equals(this.choices, other.getChoices())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswerType() != null) {
            _hashCode += getAnswerType().hashCode();
        }
        if (getChoices() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChoices());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChoices(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" AnswerType=").append(getAnswerType());
        buffer.append(" Choices=").append(getChoices());
        return buffer.toString();
    }
}
