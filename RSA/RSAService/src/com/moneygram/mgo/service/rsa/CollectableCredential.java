/**
 * CollectableCredential.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Collectable Credential.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class CollectableCredential  implements java.io.Serializable {
    private static final long serialVersionUID = 2345351113504611068L;

    private java.lang.Boolean collectionRequired;

    private CredentialType credentialType;

    public CollectableCredential() {
    }

    public CollectableCredential(
           java.lang.Boolean collectionRequired,
           CredentialType credentialType) {
           this.collectionRequired = collectionRequired;
           this.credentialType = credentialType;
    }


    /**
     * Gets the collectionRequired value for this CollectableCredential.
     * 
     * @return collectionRequired
     */
    public java.lang.Boolean getCollectionRequired() {
        return collectionRequired;
    }


    /**
     * Sets the collectionRequired value for this CollectableCredential.
     * 
     * @param collectionRequired
     */
    public void setCollectionRequired(java.lang.Boolean collectionRequired) {
        this.collectionRequired = collectionRequired;
    }


    /**
     * Gets the credentialType value for this CollectableCredential.
     * 
     * @return credentialType
     */
    public CredentialType getCredentialType() {
        return credentialType;
    }


    /**
     * Sets the credentialType value for this CollectableCredential.
     * 
     * @param credentialType
     */
    public void setCredentialType(CredentialType credentialType) {
        this.credentialType = credentialType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CollectableCredential)) return false;
        CollectableCredential other = (CollectableCredential) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.collectionRequired==null && other.getCollectionRequired()==null) || 
             (this.collectionRequired!=null &&
              this.collectionRequired.equals(other.getCollectionRequired()))) &&
            ((this.credentialType==null && other.getCredentialType()==null) || 
             (this.credentialType!=null &&
              this.credentialType.equals(other.getCredentialType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCollectionRequired() != null) {
            _hashCode += getCollectionRequired().hashCode();
        }
        if (getCredentialType() != null) {
            _hashCode += getCredentialType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" CollectionRequired=").append(getCollectionRequired());
        buffer.append(" CredentialType=").append(getCredentialType());
        return buffer.toString();
    }
}
