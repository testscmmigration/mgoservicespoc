/**
 * BrowseQuestionsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Browse Questions Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class BrowseQuestionsRequest  implements java.io.Serializable {
    private static final long serialVersionUID = 1952246920301257623L;

    private java.lang.Integer groupCount;

    private java.lang.Integer questionCount;

    public BrowseQuestionsRequest() {
    }

    public BrowseQuestionsRequest(
           java.lang.Integer groupCount,
           java.lang.Integer questionCount) {
           this.groupCount = groupCount;
           this.questionCount = questionCount;
    }


    /**
     * Gets the groupCount value for this BrowseQuestionsRequest.
     * 
     * @return groupCount
     */
    public java.lang.Integer getGroupCount() {
        return groupCount;
    }


    /**
     * Sets the groupCount value for this BrowseQuestionsRequest.
     * 
     * @param groupCount
     */
    public void setGroupCount(java.lang.Integer groupCount) {
        this.groupCount = groupCount;
    }


    /**
     * Gets the questionCount value for this BrowseQuestionsRequest.
     * 
     * @return questionCount
     */
    public java.lang.Integer getQuestionCount() {
        return questionCount;
    }


    /**
     * Sets the questionCount value for this BrowseQuestionsRequest.
     * 
     * @param questionCount
     */
    public void setQuestionCount(java.lang.Integer questionCount) {
        this.questionCount = questionCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BrowseQuestionsRequest)) return false;
        BrowseQuestionsRequest other = (BrowseQuestionsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.groupCount==null && other.getGroupCount()==null) || 
             (this.groupCount!=null &&
              this.groupCount.equals(other.getGroupCount()))) &&
            ((this.questionCount==null && other.getQuestionCount()==null) || 
             (this.questionCount!=null &&
              this.questionCount.equals(other.getQuestionCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGroupCount() != null) {
            _hashCode += getGroupCount().hashCode();
        }
        if (getQuestionCount() != null) {
            _hashCode += getQuestionCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" GroupCount=").append(getGroupCount());
        buffer.append(" QuestionCount=").append(getQuestionCount());
        return buffer.toString();
    }
}
