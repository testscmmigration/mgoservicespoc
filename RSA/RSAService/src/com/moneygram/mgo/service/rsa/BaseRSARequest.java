/**
 * BaseRSARequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Base RSA Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class BaseRSARequest  extends BaseOperationRequest {
    private static final long serialVersionUID = -7080232481206128024L;

    /* Application code that drives the configuration parameters of
     * the RSA WS call. To work properly RSA service on MGI side must be
     * configured for each application code. */
    private java.lang.String applicationCode;

    private java.lang.String transactionId;

    private Device device;

    private UserData userData;

    public BaseRSARequest() {
    }

    public BaseRSARequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData) {
        super(
            header);
        this.applicationCode = applicationCode;
        this.transactionId = transactionId;
        this.device = device;
        this.userData = userData;
    }


    /**
     * Gets the applicationCode value for this BaseRSARequest.
     * 
     * @return applicationCode   * Application code that drives the configuration parameters of
     * the RSA WS call. To work properly RSA service on MGI side must be
     * configured for each application code.
     */
    public java.lang.String getApplicationCode() {
        return applicationCode;
    }


    /**
     * Sets the applicationCode value for this BaseRSARequest.
     * 
     * @param applicationCode   * Application code that drives the configuration parameters of
     * the RSA WS call. To work properly RSA service on MGI side must be
     * configured for each application code.
     */
    public void setApplicationCode(java.lang.String applicationCode) {
        this.applicationCode = applicationCode;
    }


    /**
     * Gets the transactionId value for this BaseRSARequest.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this BaseRSARequest.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the device value for this BaseRSARequest.
     * 
     * @return device
     */
    public Device getDevice() {
        return device;
    }


    /**
     * Sets the device value for this BaseRSARequest.
     * 
     * @param device
     */
    public void setDevice(Device device) {
        this.device = device;
    }


    /**
     * Gets the userData value for this BaseRSARequest.
     * 
     * @return userData
     */
    public UserData getUserData() {
        return userData;
    }


    /**
     * Sets the userData value for this BaseRSARequest.
     * 
     * @param userData
     */
    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaseRSARequest)) return false;
        BaseRSARequest other = (BaseRSARequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.applicationCode==null && other.getApplicationCode()==null) || 
             (this.applicationCode!=null &&
              this.applicationCode.equals(other.getApplicationCode()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.device==null && other.getDevice()==null) || 
             (this.device!=null &&
              this.device.equals(other.getDevice()))) &&
            ((this.userData==null && other.getUserData()==null) || 
             (this.userData!=null &&
              this.userData.equals(other.getUserData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getApplicationCode() != null) {
            _hashCode += getApplicationCode().hashCode();
        }
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getDevice() != null) {
            _hashCode += getDevice().hashCode();
        }
        if (getUserData() != null) {
            _hashCode += getUserData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ApplicationCode=").append(getApplicationCode());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" Device=").append(getDevice());
        buffer.append(" UserData=").append(getUserData());
        return buffer.toString();
    }
}
