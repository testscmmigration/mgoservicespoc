/**
 * QueryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Query Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class QueryResponse  extends BaseRSAResponse {
    private static final long serialVersionUID = 489951811087709568L;

    private OpenEndedQuestion[][] questionGroups;

    private Image[] images;

    public QueryResponse() {
    }

    /**
     * Gets the questionGroups value for this QueryResponse.
     * 
     * @return questionGroups
     */
    public OpenEndedQuestion[][] getQuestionGroups() {
        return questionGroups;
    }


    /**
     * Sets the questionGroups value for this QueryResponse.
     * 
     * @param questionGroups
     */
    public void setQuestionGroups(OpenEndedQuestion[][] questionGroups) {
        this.questionGroups = questionGroups;
    }


    /**
     * Gets the images value for this QueryResponse.
     * 
     * @return images
     */
    public Image[] getImages() {
        return images;
    }


    /**
     * Sets the images value for this QueryResponse.
     * 
     * @param images
     */
    public void setImages(Image[] images) {
        this.images = images;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryResponse)) return false;
        QueryResponse other = (QueryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.questionGroups==null && other.getQuestionGroups()==null) || 
             (this.questionGroups!=null &&
              java.util.Arrays.equals(this.questionGroups, other.getQuestionGroups()))) &&
            ((this.images==null && other.getImages()==null) || 
             (this.images!=null &&
              java.util.Arrays.equals(this.images, other.getImages())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getQuestionGroups() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getQuestionGroups());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getQuestionGroups(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getImages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" QuestionGroups=").append(getQuestionGroups());
        buffer.append(" Images=").append(getImages());
        return buffer.toString();
    }
}
