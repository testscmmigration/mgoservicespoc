/*
 * Created on Oct 2, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.GetUserImageRequest;
import com.moneygram.mgo.service.rsa.GetUserImageResponse;
import com.moneygram.mgo.service.rsa.SiteToUserData;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.GenericActionType;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.QueryResponse;
import com.rsa.csd.ws.WSUserType;

/**
 * 
 * Get User Image Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/10/23 21:39:41 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetUserImageCommand extends QueryCommand {
    private static final GenericActionType[] actions = new GenericActionType[]{GenericActionType.GET_IMAGE, GenericActionType.GET_PHRASE, GenericActionType.GET_USER_STATUS};

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest request) throws CommandException {
        com.rsa.csd.ws.QueryRequest queryRequest = createBaseQueryRequest(request);
        setGenericActionTypes(queryRequest, actions);
        WSUserType userType = null;
        if (request.getUserData() != null && request.getUserData().isUserIdInvalid()) {
            userType = WSUserType.NONPERSISTENT;
            copyUserLoginName(queryRequest);
        } else {
            userType = WSUserType.PERSISTENT;
        }
        setUserType(queryRequest, userType);
        return queryRequest;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest,
            BaseRSARequest baseRSARequest) throws CommandException {
        com.rsa.csd.ws.QueryResponse queryResponse = null;
        try {
            com.rsa.csd.ws.QueryRequest queryRequest = (com.rsa.csd.ws.QueryRequest)genericRequest;
            //query site to user info
            queryResponse = client.query(queryRequest);
            
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA query request", e);
        }

        GetUserImageResponse response = createUserImageResponse(queryResponse);

        return response;
    }

    private GetUserImageResponse createUserImageResponse(QueryResponse queryResponse) throws CommandException {
        //check generic rsa response
        checkBaseRSAResponseStatus(queryResponse);

        GetUserImageResponse response = new GetUserImageResponse();
        response.setSiteToUserData(createSiteToUserData(queryResponse));

        copyUserStatus(queryResponse, response);
        if (response.getUserStatus() == null) {
            throw new DataFormatException("RSA response is missing UserStatus in IdentificationData");
        }

        return response;
    }

    private SiteToUserData createSiteToUserData(QueryResponse queryResponse) {
        SiteToUserData siteToUserData = null;
        if (queryResponse.getSiteToUserData() != null) {
            siteToUserData = new SiteToUserData(
                    queryResponse.getSiteToUserData().getPhrase(), 
                    createImage(queryResponse.getSiteToUserData().getUserImage())); 
        }
        return siteToUserData;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetUserImageRequest;
    }

}
