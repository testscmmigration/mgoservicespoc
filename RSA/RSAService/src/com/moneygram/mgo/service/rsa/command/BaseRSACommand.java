/*
 * Created on Aug 27, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.common.util.StringUtility;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.BaseRSAResponse;
import com.moneygram.mgo.service.rsa.Device;
import com.moneygram.mgo.service.rsa.util.RSAResourceConfig;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.APIType;
import com.rsa.csd.ws.AuthorizationMethod;
import com.rsa.csd.ws.ChallengeQuestionActionType;
import com.rsa.csd.ws.ChallengeQuestionActionTypeList;
import com.rsa.csd.ws.ChallengeQuestionConfig;
import com.rsa.csd.ws.ChallengeQuestionList;
import com.rsa.csd.ws.ChallengeQuestionManagementRequest;
import com.rsa.csd.ws.ChallengeQuestionManagementRequestPayload;
import com.rsa.csd.ws.ConfigurationHeader;
import com.rsa.csd.ws.CredentialManagementRequestList;
import com.rsa.csd.ws.DeviceRequest;
import com.rsa.csd.ws.GenericActionType;
import com.rsa.csd.ws.GenericActionTypeList;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.GenericResponse;
import com.rsa.csd.ws.IdentificationData;
import com.rsa.csd.ws.MessageHeader;
import com.rsa.csd.ws.MessageVersion;
import com.rsa.csd.ws.RequestType;
import com.rsa.csd.ws.SecurityHeader;
import com.rsa.csd.ws.UserStatus;
import com.rsa.csd.ws.WSUserType;

/**
 *
 * Base RSA Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.14 $ $Date: 2010/06/22 21:51:06 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseRSACommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseRSACommand.class);

    private static final Integer RSA_STATUS_OK = new Integer(200);



    /**
     *
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request=" + request);
        }
        long time = System.currentTimeMillis();

        BaseRSARequest commandRequest = (BaseRSARequest) request;

        String applicationCode = commandRequest.getApplicationCode();

        //create rsa request
        GenericRequest genericRequest = createRSARequest(commandRequest);

        RSAClient client = getRSAClient(applicationCode);

        OperationResponse response = null;
        try {
            response = processRSARequest(client, genericRequest, commandRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA request", e);
        }
        if (logger.isInfoEnabled()) {
            logger.info("process: elapsed="+ (System.currentTimeMillis() - time));
        }
        return response;
    }

    /**
     * Creates RSA request from the command operation request.
     * @param request
     * @return
     * @throws CommandException
     */
    protected abstract GenericRequest createRSARequest(BaseRSARequest request) throws CommandException;

    /**
     * Processes RSA request.
     * @param client
     * @param genericRequest
     * @return
     * @throws CommandException
     */
    protected abstract OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest) throws CommandException;

    /**
     * Returns transaction id extracted from the rsa response.
     * @param response
     * @return
     */
    protected String getTransactionId(GenericResponse response) {
        String id = null;
        if (response != null && response.getIdentificationData() != null) {
            id = response.getIdentificationData().getTransactionId();
        }
        return id;
    }

    /**
     * Sets up common properties of the rsa generic request.
     * @throws CommandException
     */
    protected void setupGenericRequest(GenericRequest request, BaseRSARequest baseRSARequest) throws CommandException {
        if (baseRSARequest.getApplicationCode() == null) {
            throw new DataFormatException("Required application code value is not specified");
        }

        ConfigurationHeader configurationHeader = new ConfigurationHeader();
        configurationHeader.setInstanceId(RSAResourceConfig.getInstance().getRSAInstanceId());

        DeviceRequest deviceRequest = createDeviceRequest(baseRSARequest.getDevice());

        String applicationCode = baseRSARequest.getApplicationCode();

        String sessionId = null;
        String requestId = null;
        if (baseRSARequest.getHeader().getClientHeader() != null) {
            sessionId = baseRSARequest.getHeader().getClientHeader().getClientSessionID();
            requestId = baseRSARequest.getHeader().getClientHeader().getClientRequestID();
        }

        IdentificationData identificationData = new IdentificationData();
        identificationData.setOrgName(applicationCode);
        identificationData.setSessionId(sessionId);
        if (baseRSARequest.getUserData() != null) {
            identificationData.setUserName(baseRSARequest.getUserData().getUserId());

            //Begin - vl58 - MGO-2236 :  RSA Adaptive auth calls, inclusion of parameters userCountry and userLanguage required for adaptive authentication
            //service in order to retrieve questions in a different language, i.e: userCountry -> DE (Germany), userLanguage -> de (german)
            String useRSATranslation = RSAResourceConfig.getInstance().getUseRSATranslation();
            logger.info("[setupGenericRequest] useRSATranslation = "+useRSATranslation);
            if(!StringUtility.isNullOrEmpty(useRSATranslation) && "true".equals(useRSATranslation)){
	            String userCountry = baseRSARequest.getUserData().getUserCountry();
	            String userLanguage = baseRSARequest.getUserData().getUserLanguage();
	            logger.info("[setupGenericRequest] userCountry = "+userCountry+", userLanguage = "+userLanguage);
	            if(!StringUtility.isNullOrEmpty(userCountry) && !StringUtility.isNullOrEmpty(userLanguage)){
		            identificationData.setUserCountry(userCountry.toUpperCase().trim());
		            identificationData.setUserLanguage(userLanguage.toLowerCase().trim());
	            }
            }
            //End - vl58 - MGO-2236 :  RSA Adaptive auth calls, inclusion of parameters userCountry and userLanguage required for adaptive authentication
            //service in order to retrieve questions in a different language
        }

        identificationData.setTransactionId(baseRSARequest.getTransactionId());

        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setApiType(APIType.DIRECT_SOAP_API);
        messageHeader.setRequestId(requestId);
        messageHeader.setRequestType(getRequestType());
        messageHeader.setVersion(MessageVersion.value1);

        SecurityHeader securityHeader = new SecurityHeader();
        securityHeader.setCallerCredential(RSAResourceConfig.getInstance().getRSACallerCredential(applicationCode));
        securityHeader.setCallerId(RSAResourceConfig.getInstance().getRSACallerId(applicationCode));
        securityHeader.setMethod(AuthorizationMethod.PASSWORD);

        request.setConfigurationHeader(configurationHeader);
        request.setDeviceRequest(deviceRequest);
        request.setIdentificationData(identificationData);
        request.setMessageHeader(messageHeader);
        request.setSecurityHeader(securityHeader);
    }

    /**
     * Returns RSA request type.
     * @return
     */
    protected abstract RequestType getRequestType();

    /**
     * Creates DeviceRequest.
     * @param device
     * @return
     */
    private DeviceRequest createDeviceRequest(Device device) {
        DeviceRequest deviceRequest = null;
        if (device != null) {
            deviceRequest = new DeviceRequest();
            deviceRequest.setDevicePrint(device.getDevicePrint());
            deviceRequest.setDeviceTokenCookie(device.getDeviceTokenCookie());
            deviceRequest.setDeviceTokenFSO(device.getDeviceTokenFSO());
            deviceRequest.setHttpAccept(device.getHttpAccept());
            deviceRequest.setHttpAcceptEncoding(device.getHttpAcceptEncoding());
            deviceRequest.setHttpAcceptLanguage(device.getHttpAcceptLanguage());
            deviceRequest.setHttpReferrer(device.getHttpReferrer());
            deviceRequest.setIpAddress(device.getIpAddress());
            deviceRequest.setUserAgent(device.getUserAgent());
        }
        return deviceRequest;
    }

    /**
     * Checks the rsa response and its status.
     * @param response
     * @throws CommandException
     */
    protected void checkBaseRSAResponseStatus(GenericResponse response) throws CommandException {
        if (response == null) {
            throw new CommandException("RSA response is null");
        }

        if (response.getStatusHeader() == null) {
            throw new CommandException("RSA response StatusHeader is null");
        }

        if (! RSA_STATUS_OK.equals(response.getStatusHeader().getStatusCode())) {
            throw new CommandException("RSA response status code is "+ response.getStatusHeader().getStatusCode());
        }

    }

    /**
     * Checks transaction Id in addition to the base check.
     * @param response
     * @throws CommandException
     */
    protected void checkRSAResponseStatus(GenericResponse response) throws CommandException {
        checkBaseRSAResponseStatus(response);

        if (response.getIdentificationData() == null) {
            throw new CommandException("RSA response IdentificationData is null");
        }

        if (response.getIdentificationData().getTransactionId() == null) {
            throw new CommandException("RSA response TransactionId is null");
        }

    }

    /**
     * Creates an instance of the RSAClient.
     * @param applicationCode
     * @return
     * @throws CommandException
     */
    protected RSAClient getRSAClient(String applicationCode) throws CommandException {
        RSAClient rsaClient = null;
        try {
            String endpoint = RSAResourceConfig.getInstance().getRSAEndpoint(applicationCode);
            int timeout = RSAResourceConfig.getInstance().getRSATimeout(applicationCode);
            if (logger.isInfoEnabled()) {
                logger.info("getRSAClient: creating new instance of the RSAClient for applicationCode="+ applicationCode +": endpoint="+ endpoint +" timeout="+ timeout);
            }
            if (endpoint == null) {
                throw new IllegalArgumentException("Endpoint url must be specified");
            }
            rsaClient = new RSAClient(endpoint, timeout);
        } catch (Exception e) {
            throw new CommandException("Failed to create an instance of the RSA client for applicationCode="+ applicationCode, e);
        }
        return rsaClient;
    }

    protected CredentialManagementRequestList createCredentialManagementRequestList(
            ChallengeQuestionActionType actionType) {

        ChallengeQuestionActionTypeList actionTypeList =
            new ChallengeQuestionActionTypeList(
                    new ChallengeQuestionActionType[]{actionType});

        ChallengeQuestionManagementRequestPayload payload = new ChallengeQuestionManagementRequestPayload();
        payload.setActionTypeList(actionTypeList);

        ChallengeQuestionManagementRequest challengeQuestionManagementRequest = new ChallengeQuestionManagementRequest();
        challengeQuestionManagementRequest.setPayload(payload);

        CredentialManagementRequestList credentialManagementRequestList = new CredentialManagementRequestList();
        credentialManagementRequestList.setChallengeQuestionManagementRequest(challengeQuestionManagementRequest);
        return credentialManagementRequestList;
    }

    protected CredentialManagementRequestList createCredentialManagementRequestList(
            ChallengeQuestionActionType actionType, ChallengeQuestionConfig challengeQuestionConfig) {

        CredentialManagementRequestList list = createCredentialManagementRequestList(actionType);
        list.getChallengeQuestionManagementRequest().getPayload().setChallengeQuestionConfig(challengeQuestionConfig);
        return list;
    }

    protected CredentialManagementRequestList createCredentialManagementRequestList(
            ChallengeQuestionActionType actionType, ChallengeQuestionList challengeQuestionList) {

        CredentialManagementRequestList list = createCredentialManagementRequestList(actionType);
        list.getChallengeQuestionManagementRequest().getPayload().setChallengeQuestionList(challengeQuestionList);
        return list;
    }

    protected void setGenericActionTypes(GenericRequest request, GenericActionType[] actions) {
        GenericActionTypeList actionTypeList = new GenericActionTypeList(actions);
        request.setActionTypeList(actionTypeList);
    }

    protected void setUserType(GenericRequest request, WSUserType userType) {
        if (request.getIdentificationData() != null) {
            request.getIdentificationData().setUserType(userType);
        }
    }

    protected void setUserStatus(GenericRequest request, UserStatus userStatus) {
        if (request.getIdentificationData() != null) {
            request.getIdentificationData().setUserStatus(userStatus);
        }
    }

    protected void copyUserLoginName(GenericRequest request) {
        if (request.getIdentificationData() != null) {
            request.getIdentificationData().setUserLoginName(request.getIdentificationData().getUserName());
        }
    }

    protected void copyUserStatus(GenericResponse rsaResponse, BaseRSAResponse commandResponse) {
        if (rsaResponse.getIdentificationData() != null) {
            if (rsaResponse.getIdentificationData().getUserStatus() != null) {
                commandResponse.setUserStatus(
                        com.moneygram.mgo.service.rsa.UserStatus.fromValue(
                                rsaResponse.getIdentificationData().getUserStatus().getValue()));
            }
        }
    }

}
