/**
 * MultipleChoicePossibleAnswer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Multiple Choice Possible Answer.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/04 16:22:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MultipleChoicePossibleAnswer  implements java.io.Serializable {
    private static final long serialVersionUID = 3513724641245900096L;

    private long choiceId;

    private java.lang.String choiceText;

    public MultipleChoicePossibleAnswer() {
    }

    public MultipleChoicePossibleAnswer(
           long choiceId,
           java.lang.String choiceText) {
           this.choiceId = choiceId;
           this.choiceText = choiceText;
    }


    /**
     * Gets the choiceId value for this MultipleChoicePossibleAnswer.
     * 
     * @return choiceId
     */
    public long getChoiceId() {
        return choiceId;
    }


    /**
     * Sets the choiceId value for this MultipleChoicePossibleAnswer.
     * 
     * @param choiceId
     */
    public void setChoiceId(long choiceId) {
        this.choiceId = choiceId;
    }


    /**
     * Gets the choiceText value for this MultipleChoicePossibleAnswer.
     * 
     * @return choiceText
     */
    public java.lang.String getChoiceText() {
        return choiceText;
    }


    /**
     * Sets the choiceText value for this MultipleChoicePossibleAnswer.
     * 
     * @param choiceText
     */
    public void setChoiceText(java.lang.String choiceText) {
        this.choiceText = choiceText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MultipleChoicePossibleAnswer)) return false;
        MultipleChoicePossibleAnswer other = (MultipleChoicePossibleAnswer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.choiceId == other.getChoiceId() &&
            ((this.choiceText==null && other.getChoiceText()==null) || 
             (this.choiceText!=null &&
              this.choiceText.equals(other.getChoiceText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getChoiceId()).hashCode();
        if (getChoiceText() != null) {
            _hashCode += getChoiceText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ChoiceId=").append(getChoiceId());
        buffer.append(" ChoiceText=").append(getChoiceText());
        return buffer.toString();
    }
}
