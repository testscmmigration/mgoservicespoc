/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.ActionCode;
import com.moneygram.mgo.service.rsa.AnalyzeEventRequest;
import com.moneygram.mgo.service.rsa.AnalyzeEventResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.CollectableCredential;
import com.moneygram.mgo.service.rsa.CredentialType;
import com.moneygram.mgo.service.rsa.RiskResult;
import com.moneygram.mgo.service.rsa.TriggeredRule;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.AnalyzeRequest;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.CollectableCredentialList;
import com.rsa.csd.ws.CollectionType;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.UserData;
import com.rsa.csd.ws.UserName;

/**
 * 
 * Analyze Event Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.15 $ $Date: 2010/06/22 21:58:08 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AnalyzeEventCommand extends NotifyCommand {

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest commandRequest) throws CommandException {
        AnalyzeRequest request = new AnalyzeRequest();
        setupGenericRequest(request, commandRequest);

        request.setEventDataList(createEventDataList(commandRequest));
        request.setUserData(createUserData(commandRequest));
        return request;
    }
    
    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        AnalyzeResponse analyzeResponse = null;
        try {
            AnalyzeRequest analyzeRequest = (AnalyzeRequest)genericRequest;
            analyzeResponse = client.analyze(analyzeRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA analyze event request", e);
        }

        AnalyzeEventResponse response = createAnalyzeEventResponse(analyzeResponse);

        return response;
    }

    private UserData createUserData(BaseRSARequest commandRequest) {
        UserData userData = new UserData();
        return populateUserData(userData, commandRequest);
    }

    /**
     * Converts Date instance into RSA String date. 
     * @param date
     * @return RSA String date.
     */
    protected static String toString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    protected UserData populateUserData(UserData userData, BaseRSARequest commandRequest) {
        if (commandRequest.getUserData() != null) {
            
            if (commandRequest.getUserData().getEnrollmentDate() != null) {
                String date = toString(commandRequest.getUserData().getEnrollmentDate().getTime());
                userData.setOnlineServiceEnrollDate(date);
            }
            if (commandRequest.getUserData().getUserName() != null) {
                UserName name = new UserName();
                name.setFirstName(commandRequest.getUserData().getUserName().getFirstName());
                name.setMiddleName(commandRequest.getUserData().getUserName().getMiddleName());
                name.setLastName(commandRequest.getUserData().getUserName().getLastName());
                userData.setUserNameData(name);
            }
        }

        return userData;
    }
    
    protected AnalyzeEventResponse populateAnalyzeEventResponse(AnalyzeResponse response, AnalyzeEventResponse analyzeEventResponse) throws CommandException {
        //check generic rsa response
        checkRSAResponseStatus(response);

        copyUserStatus(response, analyzeEventResponse);
        analyzeEventResponse.setTransactionId(getTransactionId(response));

        analyzeEventResponse.setRiskResult(createRiskResult(response.getRiskResult()));
        analyzeEventResponse.setCollectableCredentials(createCollectableCredentials(response.getCollectableCredentialList()));
        return analyzeEventResponse;
    }

    private AnalyzeEventResponse createAnalyzeEventResponse(AnalyzeResponse response) throws CommandException {
        AnalyzeEventResponse analyzeEventResponse = new AnalyzeEventResponse();
        return populateAnalyzeEventResponse(response, analyzeEventResponse);
    }

    private CollectableCredential[] createCollectableCredentials(CollectableCredentialList collectableCredentialList) {
        CollectableCredential[] credentials = null;
        if (collectableCredentialList != null 
                && collectableCredentialList.getCollectableCredential() != null 
                && collectableCredentialList.getCollectableCredential().length > 0) {

            com.rsa.csd.ws.CollectableCredential[] list = collectableCredentialList.getCollectableCredential();
            credentials = new CollectableCredential[list.length];
            
            for (int i = 0; i < list.length; i++) {
                com.rsa.csd.ws.CollectableCredential rsaCredential = list[i];

                CollectableCredential credential = new CollectableCredential();
                credential.setCollectionRequired(CollectionType.REQUIRED_COLLECTION.equals(rsaCredential.getCollectionType()));
                if (rsaCredential.getCredentialType() != null) {
                    credential.setCredentialType(CredentialType.fromString(rsaCredential.getCredentialType().getValue()));
                }
                credentials[i] = credential; 
            }
        }
        return credentials;
    }

    private RiskResult createRiskResult(com.rsa.csd.ws.RiskResult riskResult) throws CommandException {
        if (riskResult == null) {
            throw new CommandException("RSA Risk result is null");
        }
        
        RiskResult result = new RiskResult();
        result.setRiskScore(riskResult.getRiskScore());
        result.setRiskScoreBand(riskResult.getRiskScoreBand());
        result.setTriggeredRule(createTriggeredRule(riskResult.getTriggeredRule()));
        result.setTriggeredTestRule(createTriggeredRule(riskResult.getTriggeredTestRule()));
        return result;
    }
    
    private TriggeredRule createTriggeredRule(com.rsa.csd.ws.TriggeredRule triggeredRule) {
        TriggeredRule rule = null;
        if (triggeredRule != null) {
            rule = new TriggeredRule();
            rule.setActionCode(ActionCode.fromString(triggeredRule.getActionCode().getValue()));
            rule.setRuleId(triggeredRule.getRuleId());
            rule.setRuleName(triggeredRule.getRuleName());
        }
        return rule;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof AnalyzeEventRequest;
    }

}
