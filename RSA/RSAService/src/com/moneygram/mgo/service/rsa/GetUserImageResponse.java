/**
 * GetUserImageResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Get User Image Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetUserImageResponse  extends BaseRSAResponse  {
    private static final long serialVersionUID = 6426614765427513360L;

    private SiteToUserData siteToUserData;

    public GetUserImageResponse() {
    }

    /**
     * Gets the siteToUserData value for this GetUserImageResponse.
     * 
     * @return siteToUserData
     */
    public SiteToUserData getSiteToUserData() {
        return siteToUserData;
    }


    /**
     * Sets the siteToUserData value for this GetUserImageResponse.
     * 
     * @param siteToUserData
     */
    public void setSiteToUserData(SiteToUserData siteToUserData) {
        this.siteToUserData = siteToUserData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUserImageResponse)) return false;
        GetUserImageResponse other = (GetUserImageResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.siteToUserData==null && other.getSiteToUserData()==null) || 
             (this.siteToUserData!=null &&
              this.siteToUserData.equals(other.getSiteToUserData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSiteToUserData() != null) {
            _hashCode += getSiteToUserData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
    
    @Override
    public String toString() {
        return super.toString() +" SiteToUserData="+ getSiteToUserData();
    }

}
