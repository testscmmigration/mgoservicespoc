package com.moneygram.mgo.service.rsa;

/**
 * 
 * Execution Speed.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2010/09/07 16:44:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ExecutionSpeed implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ExecutionSpeed(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SEVERAL_DAYS = "SEVERAL_DAYS";
    public static final java.lang.String _OVER_NIGHT = "OVER_NIGHT";
    public static final java.lang.String _FEW_HOURS = "FEW_HOURS";
    public static final java.lang.String _REAL_TIME = "REAL_TIME";
    public static final ExecutionSpeed SEVERAL_DAYS = new ExecutionSpeed(_SEVERAL_DAYS);
    public static final ExecutionSpeed OVER_NIGHT = new ExecutionSpeed(_OVER_NIGHT);
    public static final ExecutionSpeed FEW_HOURS = new ExecutionSpeed(_FEW_HOURS);
    public static final ExecutionSpeed REAL_TIME = new ExecutionSpeed(_REAL_TIME);
    
    public java.lang.String getValue() { return _value_;}
    public static ExecutionSpeed fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ExecutionSpeed enumeration = (ExecutionSpeed)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ExecutionSpeed fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
