/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import integration.hawaii.verid.com.AddressContextType;
import integration.hawaii.verid.com.AddressType;
import integration.hawaii.verid.com.BirthdateType;
import integration.hawaii.verid.com.CallcenterType;
import integration.hawaii.verid.com.ContactType;
import integration.hawaii.verid.com.ModeType;
import integration.hawaii.verid.com.OnlineType;
import integration.hawaii.verid.com.PersonType;
import integration.hawaii.verid.com.PhoneNumberContextType;
import integration.hawaii.verid.com.PhoneNumberType;
import integration.hawaii.verid.com.ResultType;
import integration.hawaii.verid.com.SettingsType;
import integration.hawaii.verid.com.SimulatorModeType;
import integration.hawaii.verid.com.SsnType;
import integration.hawaii.verid.com.TransactionInitiateType;
import integration.hawaii.verid.com.TransactionResponseType;
import integration.hawaii.verid.com.VenueType;
import integration.hawaii.verid.com.VerificationTasknameType;
import integration.hawaii.verid.com.VerificationType;

import java.math.BigInteger;
import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.GetVeridKBAQuestionsRequest;
import com.moneygram.mgo.service.rsa.GetVeridKBAQuestionsResponse;
import com.moneygram.mgo.service.rsa.Person;
import com.moneygram.mgo.service.rsa.util.RSAResourceConfig;
import com.verid.netview.transact.VeridAuthenticationLocator;
import com.verid.netview.transact.VeridAuthenticationPortStub;

/**
 * 
 * Get Verid KBA Questions Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2009/10/08 21:35:18 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetVeridKBAQuestionsCommand extends BaseVeridKBACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(GetVeridKBAQuestionsCommand.class);

    /**
     * 
     * @see com.moneygram.common.service.BaseCommand#process(com.moneygram.common.service.OperationRequest)
     */
    protected OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request=" + request);
        }

        GetVeridKBAQuestionsRequest commandRequest = (GetVeridKBAQuestionsRequest) request;

        //create verid request
        TransactionInitiateType ti = createVeridQuestionsRequest(commandRequest);

        TransactionResponseType transResponse = null;
        try {
            //prepare soap call
            VeridAuthenticationLocator val = new VeridAuthenticationLocator();
            VeridAuthenticationPortStub authentication = (VeridAuthenticationPortStub) val.getVeridAuthenticationPort();
            authentication.setUsername(RSAResourceConfig.getInstance().getVeridUserName());
            authentication.setPassword(RSAResourceConfig.getInstance().getVeridPassword());

            //TODO: AddProfileComment(profile, "Initial call to Verid to find consumer and gather questions.");
            // Call Verid.
            transResponse = authentication.authenticationTransaction(ti);
        } catch (Exception e) {
            throw new CommandException("Failed Verid initiate request due to an unexpected exception", e);
        }

        GetVeridKBAQuestionsResponse response = new GetVeridKBAQuestionsResponse();
        response.setVendorAuthenticationEventId(String.valueOf(transResponse.getTransactionNumber()));
        response.setResultType(com.moneygram.mgo.service.rsa.ResultType.fromString(transResponse.getResult().getValue()));
        
        // Get Verid Results.
        if (ResultType.questions.equals(transResponse.getResult())) {
            //got questions response
            //TODO: AddProfileComment(profile, "Returning initial set of "+ veridTrans.getQuestions().length + "questions from Verid.");
            response.setQuestions(setQuestions(transResponse));
        } else {
            //got failed or error response
            //TODO: A Verid Profile comment will be logged in the VeridError
            response.setAdditionalInformation(getAdditionalInformation(transResponse));
        }
        
        return response;
    }

    /**
     * Returns TransactionInitiateType request instance.
     * @param commandRequest
     * @return
     * @throws CommandException 
     */
    private TransactionInitiateType createVeridQuestionsRequest(GetVeridKBAQuestionsRequest commandRequest) throws CommandException {

        BirthdateType birthdate = new BirthdateType();

        Person person = commandRequest.getPerson();
        
        if (person == null) {
            throw new CommandException("Person data is missing in the request");
        }
        
        Calendar dob = person.getDateOfBirth();

        if (dob != null) {
            birthdate.setDay(BigInteger.valueOf(dob.get(Calendar.DAY_OF_MONTH)));
            birthdate.setMonth(BigInteger.valueOf(dob.get(Calendar.MONTH) + 1));
            birthdate.setYear(BigInteger.valueOf(dob.get(Calendar.YEAR)));
        }

        if (person.getAddress() == null) {
            throw new CommandException("Person address data is missing in the request");
        }

        AddressType addr = new AddressType();
        addr.setAddressCity(person.getAddress().getCity());
        addr.setAddressStreet1(person.getAddress().getLine1());
        addr.setAddressStreet2(person.getAddress().getLine2());
        addr.setAddressState(person.getAddress().getState());
        addr.setAddressZip(person.getAddress().getZipCode());
        addr.setAddressZipPlus4(null);//TODO: profile.getZip4()
        addr.setAddressContext(AddressContextType.primary);
        AddressType[] address = new AddressType[] { addr };

        PhoneNumberType[] phone = new PhoneNumberType[1];
        phone[0] = new PhoneNumberType();
        phone[0].setPhoneNumber(person.getPhone());
        phone[0].setPhoneNumberContext(PhoneNumberContextType.home);

        if (person.getName() == null) {
            throw new CommandException("Person name data is missing in the request");
        }

        PersonType personType = new PersonType();
        personType.setNameFirst(person.getName().getFirstName());
        personType.setNameMiddle(person.getName().getMiddleName());
        personType.setNameLast(person.getName().getLastName());
        personType.setSsn(person.getSsn());
        personType.setSsnType(SsnType.ssn4);
        personType.setBirthdate(birthdate);
        personType.setAddress(address);
        personType.setPhoneNumber(phone);

        ContactType contact = new ContactType();
        contact.setPerson(personType);

        SettingsType settings = new SettingsType();
        settings.setPatriotActCompliance(Boolean.FALSE);

        if (commandRequest.getUserSystemCredentials() == null) {
            throw new CommandException("User System Credentials is missing in the request");
        }

        OnlineType online = new OnlineType();
        online.setCustomerIpAddress(commandRequest.getUserSystemCredentials().getIpAddress());

        CallcenterType callCenter = new CallcenterType();
        callCenter.setCallerPhoneNumber("1-800-922-7146");

        VenueType venue = new VenueType();
        venue.setOnline(online);
        venue.setCallcenter(callCenter);

        VerificationType vt = new VerificationType();
        vt.setTaskname(VerificationTasknameType.fromString(RSAResourceConfig.getInstance().getVeridVerificationType()));
        vt.setVenue(venue);
        vt.setContact(contact);

        TransactionInitiateType ti = new TransactionInitiateType();
        ti.setMode(ModeType.fromString(RSAResourceConfig.getInstance().getVeridModeType()));

        if (ti.getMode().equals(ModeType.simulated)) {
            ti.setSimulatorMode(SimulatorModeType.questions);
            // Any arbitrary number will work for simulated mode type.
            ti.setReferenceId("4353543423");
        }
        ti.setRuleset(RSAResourceConfig.getInstance().getVeridRuleSet());
        ti.setAccountName(RSAResourceConfig.getInstance().getVeridAccountName());

        ti.setSequenceNumber(commandRequest.getEventSequenceNumber());
        // This parameter needs to be unique.
        ti.setAccountsTransactionId(String.valueOf(commandRequest.getEventSequenceNumber()));
        ti.setParentTransactionNumber(new Long(0));

        ti.setSettings(settings);

        ti.setVerification(vt);
        
        SimulatorModeType simulatorModeType = createSimulatorModeType(commandRequest);
        if (simulatorModeType != null) {
            ti.setSimulatorMode(simulatorModeType);
        }

        return ti;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof GetVeridKBAQuestionsRequest;
    }


}
