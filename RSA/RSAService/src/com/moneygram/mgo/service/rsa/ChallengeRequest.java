/**
 * ChallengeRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;


/**
 * 
 * Challenge Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ChallengeRequest  extends BaseRSARequest {
    private static final long serialVersionUID = -2510399558888945030L;

    private int numberOfQuestion;

    public ChallengeRequest() {
    }

    public ChallengeRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData,
           int numberOfQuestion) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
        this.numberOfQuestion = numberOfQuestion;
    }


    /**
     * Gets the numberOfQuestion value for this ChallengeRequest.
     * 
     * @return numberOfQuestion
     */
    public int getNumberOfQuestion() {
        return numberOfQuestion;
    }


    /**
     * Sets the numberOfQuestion value for this ChallengeRequest.
     * 
     * @param numberOfQuestion
     */
    public void setNumberOfQuestion(int numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChallengeRequest)) return false;
        ChallengeRequest other = (ChallengeRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.numberOfQuestion == other.getNumberOfQuestion();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getNumberOfQuestion();
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" NumberOfQuestion="+ getNumberOfQuestion();
    }
}
