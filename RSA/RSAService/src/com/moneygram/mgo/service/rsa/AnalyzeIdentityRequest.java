/**
 * AnalyzeIdentityRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;


/**
 * 
 * Analyze Identity Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/06/23 15:45:37 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AnalyzeIdentityRequest  extends AnalyzeEventRequest {
    private static final long serialVersionUID = 7248889682722477252L;

    private com.moneygram.mgo.service.rsa.Person person;

    public AnalyzeIdentityRequest() {
    }

    public AnalyzeIdentityRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           com.moneygram.mgo.service.rsa.Device device,
           com.moneygram.mgo.service.rsa.UserData userData,
           com.moneygram.mgo.service.rsa.Event[] events,
           java.lang.Boolean startMonitoring,
           com.moneygram.mgo.service.rsa.Person person) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData,
            events,
            startMonitoring);
        this.person = person;
    }


    /**
     * Gets the person value for this AnalyzeIdentityRequest.
     * 
     * @return person
     */
    public com.moneygram.mgo.service.rsa.Person getPerson() {
        return person;
    }


    /**
     * Sets the person value for this AnalyzeIdentityRequest.
     * 
     * @param person
     */
    public void setPerson(com.moneygram.mgo.service.rsa.Person person) {
        this.person = person;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AnalyzeIdentityRequest)) return false;
        AnalyzeIdentityRequest other = (AnalyzeIdentityRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.person==null && other.getPerson()==null) || 
             (this.person!=null &&
              this.person.equals(other.getPerson())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPerson() != null) {
            _hashCode += getPerson().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" Person="+ getPerson();
    }
}
