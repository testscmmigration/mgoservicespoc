/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.AnalyzeEventResponse;
import com.moneygram.mgo.service.rsa.AnalyzeIdentityRequest;
import com.moneygram.mgo.service.rsa.AnalyzeIdentityResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.Event;
import com.moneygram.mgo.service.rsa.NotifyRequest;
import com.moneygram.mgo.service.rsa.Person;
import com.moneygram.mgo.shared.ConsumerAddress;
import com.moneygram.mgo.shared.ConsumerName;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.AnalyzeResponse;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.IdentificationNumber;
import com.rsa.csd.ws.IdentificationType;
import com.rsa.csd.ws.NAEventData;
import com.rsa.csd.ws.NAEventType;
import com.rsa.csd.ws.NAUserData;
import com.rsa.csd.ws.PhoneInfo;
import com.rsa.csd.ws.RequestType;
import com.rsa.csd.ws.TelephonesList;
import com.rsa.csd.ws.UserAddress;
import com.rsa.csd.ws.UserName;

/**
 * 
 * Analyze Identity Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/07/02 19:40:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AnalyzeIdentityCommand extends AnalyzeEventCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(AnalyzeIdentityCommand.class);

    private static final String defaultPhoneCountryCode = "1";

    private Map phoneCountryCodes = null;
    
    public Map getPhoneCountryCodes() {
        return phoneCountryCodes;
    }

    public void setPhoneCountryCodes(Map phoneCountryCodes) {
        this.phoneCountryCodes = phoneCountryCodes;
    }

    /**
     * Derives the Phone Country Code from the ISO Country Code.
     * @param isoCountryCode
     * @return Phone Country Code from the ISO Country Code.
     */
    private String getPhoneCountryCode(String isoCountryCode) {
        String phoneCountryCode = null;
        if (getPhoneCountryCodes() != null) {
            phoneCountryCode = (String) getPhoneCountryCodes().get(isoCountryCode);
        }
        if (phoneCountryCode == null) {
            phoneCountryCode = defaultPhoneCountryCode;
            logger.warn("Phone country code mapping is not found for isoCountryCode="+ isoCountryCode +". Using default phone country code="+ phoneCountryCode);
        }
        return phoneCountryCode;
    }

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest commandRequest) throws CommandException {
        com.rsa.csd.ws.AnalyzeIdentityRequest request = new com.rsa.csd.ws.AnalyzeIdentityRequest();
        setupGenericRequest(request, commandRequest);

        request.setEventData(createEventData(commandRequest));
        request.setUserData(createUserData(commandRequest));
        return request;
    }
    
    protected NAEventData createEventData(BaseRSARequest commandRequest) {
        NAEventData rsaEvent = null;

        NotifyRequest notifyRequest = (NotifyRequest)commandRequest;

        Event[] events = notifyRequest.getEvents();
        if (events != null && events.length > 0) {
            for (int i = 0; i < events.length; i++) {
                Event event = events[i];
                
                rsaEvent = new NAEventData();
                if (event.getEventType() != null) {
                    rsaEvent.setEventType(NAEventType.fromString(event.getEventType().getValue()));
                }
                rsaEvent.setClientDefinedEventType(event.getClientEventId());
                rsaEvent.setEventDescription(event.getEventDescription());
                //take the 1st event from the list - only one supported by AnalyzeIdentityRequest
                break;
            }
        }
        
        return rsaEvent;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.AnalyzeIdentityResponse analyzeResponse = null;
        try {
            com.rsa.csd.ws.AnalyzeIdentityRequest analyzeRequest = (com.rsa.csd.ws.AnalyzeIdentityRequest)genericRequest;
            analyzeResponse = client.analyzeIdentity(analyzeRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA analyze event request", e);
        }

        AnalyzeEventResponse response = createAnalyzeIdentityResponse(analyzeResponse);

        return response;
    }

    private NAUserData createUserData(BaseRSARequest commandRequest) {
        NAUserData userData = null;
        if (commandRequest.getUserData() != null) {
            userData = new NAUserData();
            userData = (NAUserData) populateUserData(userData, commandRequest);
            
            AnalyzeIdentityRequest request = (AnalyzeIdentityRequest)commandRequest;
            
            if (request.getPerson() != null) {
                Person person = request.getPerson();
                if (person.getName() != null) {
                    //if name has been provided as part of the Person then
                    //overwrite the data that was potentially provided as part of UserData
                    ConsumerName personName = request.getPerson().getName();
                    UserName name = new UserName();
                    name.setFirstName(personName.getFirstName());
                    name.setMiddleName(personName.getMiddleName());
                    name.setLastName(personName.getLastName());

                    userData.setUserNameData(name);
                }

                String isoCountryCode = null;

                if (person.getAddress() != null) {
                    ConsumerAddress address = person.getAddress();
                    UserAddress rsaAddress = new UserAddress();
                    rsaAddress.setAddressLine1(address.getLine1());
                    rsaAddress.setAddressLine2(address.getLine2());
                    rsaAddress.setCity(address.getCity());
                    rsaAddress.setState(address.getState());
                    rsaAddress.setPostalCode(address.getZipCode());
                    rsaAddress.setCountry(address.getCountry());
                    userData.setUserAddress(rsaAddress);
                    
                    isoCountryCode = address.getCountry();
                }

                if (person.getPhone() != null) {
                    PhoneInfo phoneInfo = new PhoneInfo();
                    phoneInfo.setLabel("Home");
                    phoneInfo.setPhoneNumber(person.getPhone());
                    phoneInfo.setCountryCode(getPhoneCountryCode(isoCountryCode));
                    
                    PhoneInfo[] contactList = new PhoneInfo[]{phoneInfo};
                    TelephonesList userTelephones = new TelephonesList(contactList );
                    
                    userData.setUserTelephones(userTelephones );
                }
                
                if (person.getDateOfBirth() != null) {
                    userData.setDateOfBirth(toString(person.getDateOfBirth().getTime()));
                }
                
                if (person.getSsn() != null) {
                    IdentificationNumber id = new IdentificationNumber(IdentificationType.LAST_4DIGIT_SSN, person.getSsn());
                    userData.setIdentificationNumber(id);
                }
            }
        }
        return userData;
    }

    private AnalyzeIdentityResponse createAnalyzeIdentityResponse(AnalyzeResponse response) throws CommandException {
        AnalyzeIdentityResponse analyzeEventResponse = new AnalyzeIdentityResponse();
        return (AnalyzeIdentityResponse)populateAnalyzeEventResponse(response, analyzeEventResponse);
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof AnalyzeIdentityRequest;
    }

    @Override
    protected RequestType getRequestType() {
      return RequestType.ANALYZEIDENTITY;
    }

}
