/**
 * GetUserImageRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Get User Image Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetUserImageRequest  extends BaseRSARequest  {
    private static final long serialVersionUID = -1443357895972811443L;

    public GetUserImageRequest() {
    }

    public GetUserImageRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
    }
}
