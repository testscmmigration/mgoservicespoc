/**
 * RiskResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Risk Result.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RiskResult  implements java.io.Serializable {
    private static final long serialVersionUID = 1448499802697149438L;

    private java.lang.Integer riskScore;

    private java.lang.String riskScoreBand;

    private TriggeredRule triggeredRule;

    private TriggeredRule triggeredTestRule;

    public RiskResult() {
    }

    public RiskResult(
           java.lang.Integer riskScore,
           java.lang.String riskScoreBand,
           TriggeredRule triggeredRule,
           TriggeredRule triggeredTestRule) {
           this.riskScore = riskScore;
           this.riskScoreBand = riskScoreBand;
           this.triggeredRule = triggeredRule;
           this.triggeredTestRule = triggeredTestRule;
    }


    /**
     * Gets the riskScore value for this RiskResult.
     * 
     * @return riskScore
     */
    public java.lang.Integer getRiskScore() {
        return riskScore;
    }


    /**
     * Sets the riskScore value for this RiskResult.
     * 
     * @param riskScore
     */
    public void setRiskScore(java.lang.Integer riskScore) {
        this.riskScore = riskScore;
    }


    /**
     * Gets the riskScoreBand value for this RiskResult.
     * 
     * @return riskScoreBand
     */
    public java.lang.String getRiskScoreBand() {
        return riskScoreBand;
    }


    /**
     * Sets the riskScoreBand value for this RiskResult.
     * 
     * @param riskScoreBand
     */
    public void setRiskScoreBand(java.lang.String riskScoreBand) {
        this.riskScoreBand = riskScoreBand;
    }


    /**
     * Gets the triggeredRule value for this RiskResult.
     * 
     * @return triggeredRule
     */
    public TriggeredRule getTriggeredRule() {
        return triggeredRule;
    }


    /**
     * Sets the triggeredRule value for this RiskResult.
     * 
     * @param triggeredRule
     */
    public void setTriggeredRule(TriggeredRule triggeredRule) {
        this.triggeredRule = triggeredRule;
    }


    /**
     * Gets the triggeredTestRule value for this RiskResult.
     * 
     * @return triggeredTestRule
     */
    public TriggeredRule getTriggeredTestRule() {
        return triggeredTestRule;
    }


    /**
     * Sets the triggeredTestRule value for this RiskResult.
     * 
     * @param triggeredTestRule
     */
    public void setTriggeredTestRule(TriggeredRule triggeredTestRule) {
        this.triggeredTestRule = triggeredTestRule;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RiskResult)) return false;
        RiskResult other = (RiskResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.riskScore==null && other.getRiskScore()==null) || 
             (this.riskScore!=null &&
              this.riskScore.equals(other.getRiskScore()))) &&
            ((this.riskScoreBand==null && other.getRiskScoreBand()==null) || 
             (this.riskScoreBand!=null &&
              this.riskScoreBand.equals(other.getRiskScoreBand()))) &&
            ((this.triggeredRule==null && other.getTriggeredRule()==null) || 
             (this.triggeredRule!=null &&
              this.triggeredRule.equals(other.getTriggeredRule()))) &&
            ((this.triggeredTestRule==null && other.getTriggeredTestRule()==null) || 
             (this.triggeredTestRule!=null &&
              this.triggeredTestRule.equals(other.getTriggeredTestRule())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRiskScore() != null) {
            _hashCode += getRiskScore().hashCode();
        }
        if (getRiskScoreBand() != null) {
            _hashCode += getRiskScoreBand().hashCode();
        }
        if (getTriggeredRule() != null) {
            _hashCode += getTriggeredRule().hashCode();
        }
        if (getTriggeredTestRule() != null) {
            _hashCode += getTriggeredTestRule().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" RiskScore=").append(getRiskScore());
        buffer.append(" RiskScoreBand=").append(getRiskScoreBand());
        buffer.append(" TriggeredRule=").append(getTriggeredRule());
        buffer.append(" TriggeredTestRule=").append(getTriggeredTestRule());
        return buffer.toString();
    }
}
