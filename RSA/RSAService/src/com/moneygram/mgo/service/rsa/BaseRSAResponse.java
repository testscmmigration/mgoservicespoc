/**
 * BaseRSAResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.BaseOperationResponse;

/**
 * 
 * Base RSA Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class BaseRSAResponse  extends BaseOperationResponse {
    private static final long serialVersionUID = -3103508748875284113L;

    private java.lang.String transactionId;

    private UserStatus userStatus;

    public BaseRSAResponse() {
    }

    /**
     * Gets the transactionId value for this BaseRSAResponse.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this BaseRSAResponse.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the userStatus value for this BaseRSAResponse.
     * 
     * @return userStatus
     */
    public UserStatus getUserStatus() {
        return userStatus;
    }


    /**
     * Sets the userStatus value for this BaseRSAResponse.
     * 
     * @param userStatus
     */
    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaseRSAResponse)) return false;
        BaseRSAResponse other = (BaseRSAResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.userStatus==null && other.getUserStatus()==null) || 
             (this.userStatus!=null &&
              this.userStatus.equals(other.getUserStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getUserStatus() != null) {
            _hashCode += getUserStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" UserStatus=").append(getUserStatus());
        return buffer.toString();
    }
}
