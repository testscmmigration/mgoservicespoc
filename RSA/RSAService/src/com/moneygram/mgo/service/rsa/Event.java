/**
 * Event.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Event.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2010/09/07 16:44:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class Event implements java.io.Serializable {
    private static final long serialVersionUID = 8393716268523074764L;

    private java.lang.String clientEventId;

    private EventType eventType;

    private java.lang.String eventDescription;

    private MGITransaction payment;

    public Event() {
    }

    public Event(
            java.lang.String clientEventId,
            EventType eventType,
            java.lang.String eventDescription) {
            this.clientEventId = clientEventId;
            this.eventType = eventType;
            this.eventDescription = eventDescription;
     }

    public Event(
           java.lang.String clientEventId,
           EventType eventType,
           java.lang.String eventDescription,
           MGITransaction payment) {
           this.clientEventId = clientEventId;
           this.eventType = eventType;
           this.eventDescription = eventDescription;
           this.payment = payment;
    }


    /**
     * Gets the clientEventId value for this Event.
     * 
     * @return clientEventId
     */
    public java.lang.String getClientEventId() {
        return clientEventId;
    }


    /**
     * Sets the clientEventId value for this Event.
     * 
     * @param clientEventId
     */
    public void setClientEventId(java.lang.String clientEventId) {
        this.clientEventId = clientEventId;
    }


    /**
     * Gets the eventType value for this Event.
     * 
     * @return eventType
     */
    public EventType getEventType() {
        return eventType;
    }


    /**
     * Sets the eventType value for this Event.
     * 
     * @param eventType
     */
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }


    /**
     * Gets the eventDescription value for this Event.
     * 
     * @return eventDescription
     */
    public java.lang.String getEventDescription() {
        return eventDescription;
    }


    /**
     * Sets the eventDescription value for this Event.
     * 
     * @param eventDescription
     */
    public void setEventDescription(java.lang.String eventDescription) {
        this.eventDescription = eventDescription;
    }


    /**
     * Gets the payment value for this Event.
     * 
     * @return payment
     */
    public MGITransaction getPayment() {
        return payment;
    }


    /**
     * Sets the payment value for this Event.
     * 
     * @param payment
     */
    public void setPayment(MGITransaction payment) {
        this.payment = payment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Event)) return false;
        Event other = (Event) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientEventId==null && other.getClientEventId()==null) || 
             (this.clientEventId!=null &&
              this.clientEventId.equals(other.getClientEventId()))) &&
            ((this.eventType==null && other.getEventType()==null) || 
             (this.eventType!=null &&
              this.eventType.equals(other.getEventType()))) &&
            ((this.eventDescription==null && other.getEventDescription()==null) || 
             (this.eventDescription!=null &&
              this.eventDescription.equals(other.getEventDescription()))) &&
            ((this.payment==null && other.getPayment()==null) || 
             (this.payment!=null &&
              this.payment.equals(other.getPayment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientEventId() != null) {
            _hashCode += getClientEventId().hashCode();
        }
        if (getEventType() != null) {
            _hashCode += getEventType().hashCode();
        }
        if (getEventDescription() != null) {
            _hashCode += getEventDescription().hashCode();
        }
        if (getPayment() != null) {
            _hashCode += getPayment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ClientEventId=").append(getClientEventId());
        buffer.append(" EventType=").append(getEventType());
        buffer.append(" EventDescription=").append(getEventDescription());
        buffer.append(" Payment=").append(getPayment());
        return buffer.toString();
    }

}
