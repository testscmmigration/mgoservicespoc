/*
 * Created on Aug 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import integration.hawaii.verid.com.ComplexDetailType;
import integration.hawaii.verid.com.ErrorInformationType;
import integration.hawaii.verid.com.SimpleDetailType;

/**
 * 
 * Verid Error Response Wrapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/05 15:31:52 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class VeridErrorResponseWrapper extends VeridResponseWrapper {
    private ErrorInformationType[] info = null;
    
    /**
     * 
     * Creates new instance of VeridErrorResponseWrapper
     */
    public VeridErrorResponseWrapper(ErrorInformationType[] info) {
        super();
        setInfo(info);
    }

    protected ErrorInformationType[] getInfo() {
        return info;
    }

    protected void setInfo(ErrorInformationType[] info) {
        this.info = info;
    }

    @Override
    public String getCode() {
        String code = null;
        if (info != null && info.length > 0) {
            code = info[0].getCode().getValue();
        }
        return code;
    }

    @Override
    protected ComplexDetailType[] getComplexDetail(int index) {
        ComplexDetailType[] details = null;
        if (info != null && index < info.length) {
            details = info[index].getComplexDetail();
        }
        return details;
    }

    @Override
    protected SimpleDetailType[] getSimpleDetail(int index) {
        SimpleDetailType[] details = null;
        if (info != null && index < info.length) {
            details = info[index].getSimpleDetail();
        }
        return details;
    }

    @Override
    protected String getDetailHeader() {
        return "***Received an error during verid authentication.";
    }

    @Override
    protected int getDetailsLength() {
        return (info != null ? info.length : 0);
    }

}
