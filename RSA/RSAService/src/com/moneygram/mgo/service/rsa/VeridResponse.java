/**
 * VeridResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.BaseOperationResponse;

/**
 * 
 * Verid Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/05 15:31:52 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class VeridResponse  extends BaseOperationResponse {
    private static final long serialVersionUID = -3761093734185041024L;

    private java.lang.String vendorAuthenticationEventId;

    private ResultType resultType;

    private MultipleChoiceQuestions questions;

    private AdditionalInformation additionalInformation;

    public VeridResponse() {
    }

    /**
     * Gets the vendorAuthenticationEventId value for this VeridResponse.
     * 
     * @return vendorAuthenticationEventId
     */
    public java.lang.String getVendorAuthenticationEventId() {
        return vendorAuthenticationEventId;
    }


    /**
     * Sets the vendorAuthenticationEventId value for this VeridResponse.
     * 
     * @param vendorAuthenticationEventId
     */
    public void setVendorAuthenticationEventId(java.lang.String vendorAuthenticationEventId) {
        this.vendorAuthenticationEventId = vendorAuthenticationEventId;
    }


    /**
     * Gets the resultType value for this VeridResponse.
     * 
     * @return resultType
     */
    public ResultType getResultType() {
        return resultType;
    }


    /**
     * Sets the resultType value for this VeridResponse.
     * 
     * @param resultType
     */
    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }


    /**
     * Gets the questions value for this VeridResponse.
     * 
     * @return questions
     */
    public MultipleChoiceQuestions getQuestions() {
        return questions;
    }


    /**
     * Sets the questions value for this VeridResponse.
     * 
     * @param questions
     */
    public void setQuestions(MultipleChoiceQuestions questions) {
        this.questions = questions;
    }


    /**
     * Gets the additionalInformation value for this VeridResponse.
     * 
     * @return additionalInformation
     */
    public AdditionalInformation getAdditionalInformation() {
        return additionalInformation;
    }


    /**
     * Sets the additionalInformation value for this VeridResponse.
     * 
     * @param additionalInformation
     */
    public void setAdditionalInformation(AdditionalInformation additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VeridResponse)) return false;
        VeridResponse other = (VeridResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.vendorAuthenticationEventId==null && other.getVendorAuthenticationEventId()==null) || 
             (this.vendorAuthenticationEventId!=null &&
              this.vendorAuthenticationEventId.equals(other.getVendorAuthenticationEventId()))) &&
            ((this.resultType==null && other.getResultType()==null) || 
             (this.resultType!=null &&
              this.resultType.equals(other.getResultType()))) &&
            ((this.questions==null && other.getQuestions()==null) || 
             (this.questions!=null &&
              this.questions.equals(other.getQuestions()))) &&
            ((this.additionalInformation==null && other.getAdditionalInformation()==null) || 
             (this.additionalInformation!=null &&
              this.additionalInformation.equals(other.getAdditionalInformation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getVendorAuthenticationEventId() != null) {
            _hashCode += getVendorAuthenticationEventId().hashCode();
        }
        if (getResultType() != null) {
            _hashCode += getResultType().hashCode();
        }
        if (getQuestions() != null) {
            _hashCode += getQuestions().hashCode();
        }
        if (getAdditionalInformation() != null) {
            _hashCode += getAdditionalInformation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" VendorAuthenticationEventId=").append(getVendorAuthenticationEventId());
        buffer.append(" ResultType=").append(getResultType());
        buffer.append(" Questions=").append(getQuestions());
        buffer.append(" AdditionalInformation=").append(getAdditionalInformation());
        return buffer.toString();
    }
}
