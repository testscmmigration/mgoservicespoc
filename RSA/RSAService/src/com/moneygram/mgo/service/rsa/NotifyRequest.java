/**
 * NotifyRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import java.util.Arrays;

import com.moneygram.common.service.RequestHeader;


/**
 * 
 * Notify Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2010/09/10 21:19:58 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class NotifyRequest  extends BaseRSARequest {
    private static final long serialVersionUID = -390924046546844394L;

    private Event[] events;

    private java.lang.Boolean startMonitoring;

    public NotifyRequest() {
    }

    public NotifyRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData,
           Event[] events,
           java.lang.Boolean startMonitoring) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
        this.events = events;
        this.startMonitoring = startMonitoring;
    }


    /**
     * Gets the events value for this NotifyRequest.
     * 
     * @return events
     */
    public Event[] getEvents() {
        return events;
    }


    /**
     * Sets the events value for this NotifyRequest.
     * 
     * @param events
     */
    public void setEvents(Event[] events) {
        this.events = events;
    }


    /**
     * Gets the startMonitoring value for this NotifyRequest.
     * 
     * @return startMonitoring
     */
    public java.lang.Boolean getStartMonitoring() {
        return startMonitoring;
    }


    /**
     * Sets the startMonitoring value for this NotifyRequest.
     * 
     * @param startMonitoring
     */
    public void setStartMonitoring(java.lang.Boolean startMonitoring) {
        this.startMonitoring = startMonitoring;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NotifyRequest)) return false;
        NotifyRequest other = (NotifyRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.events==null && other.getEvents()==null) || 
             (this.events!=null &&
              java.util.Arrays.equals(this.events, other.getEvents()))) &&
            ((this.startMonitoring==null && other.getStartMonitoring()==null) || 
             (this.startMonitoring!=null &&
              this.startMonitoring.equals(other.getStartMonitoring())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getEvents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEvents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEvents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStartMonitoring() != null) {
            _hashCode += getStartMonitoring().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Events=").append(getEvents() == null ? null : Arrays.asList(getEvents()));
        buffer.append(" StartMonitoring=").append(getStartMonitoring());
        return buffer.toString();
    }

}
