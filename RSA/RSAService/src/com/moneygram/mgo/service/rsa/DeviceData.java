/**
 * DeviceData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Device Data.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class DeviceData  implements java.io.Serializable {
    private static final long serialVersionUID = 3021779847919629195L;

    private java.lang.String deviceTokenCookie;

    private java.lang.String deviceTokenFSO;

    private java.lang.String lookupLabel;

    private java.lang.String newLabel;

    public DeviceData() {
    }

    public DeviceData(
           java.lang.String deviceTokenCookie,
           java.lang.String deviceTokenFSO,
           java.lang.String lookupLabel,
           java.lang.String newLabel) {
           this.deviceTokenCookie = deviceTokenCookie;
           this.deviceTokenFSO = deviceTokenFSO;
           this.lookupLabel = lookupLabel;
           this.newLabel = newLabel;
    }


    /**
     * Gets the deviceTokenCookie value for this DeviceData.
     * 
     * @return deviceTokenCookie
     */
    public java.lang.String getDeviceTokenCookie() {
        return deviceTokenCookie;
    }


    /**
     * Sets the deviceTokenCookie value for this DeviceData.
     * 
     * @param deviceTokenCookie
     */
    public void setDeviceTokenCookie(java.lang.String deviceTokenCookie) {
        this.deviceTokenCookie = deviceTokenCookie;
    }


    /**
     * Gets the deviceTokenFSO value for this DeviceData.
     * 
     * @return deviceTokenFSO
     */
    public java.lang.String getDeviceTokenFSO() {
        return deviceTokenFSO;
    }


    /**
     * Sets the deviceTokenFSO value for this DeviceData.
     * 
     * @param deviceTokenFSO
     */
    public void setDeviceTokenFSO(java.lang.String deviceTokenFSO) {
        this.deviceTokenFSO = deviceTokenFSO;
    }


    /**
     * Gets the lookupLabel value for this DeviceData.
     * 
     * @return lookupLabel
     */
    public java.lang.String getLookupLabel() {
        return lookupLabel;
    }


    /**
     * Sets the lookupLabel value for this DeviceData.
     * 
     * @param lookupLabel
     */
    public void setLookupLabel(java.lang.String lookupLabel) {
        this.lookupLabel = lookupLabel;
    }


    /**
     * Gets the newLabel value for this DeviceData.
     * 
     * @return newLabel
     */
    public java.lang.String getNewLabel() {
        return newLabel;
    }


    /**
     * Sets the newLabel value for this DeviceData.
     * 
     * @param newLabel
     */
    public void setNewLabel(java.lang.String newLabel) {
        this.newLabel = newLabel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeviceData)) return false;
        DeviceData other = (DeviceData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.deviceTokenCookie==null && other.getDeviceTokenCookie()==null) || 
             (this.deviceTokenCookie!=null &&
              this.deviceTokenCookie.equals(other.getDeviceTokenCookie()))) &&
            ((this.deviceTokenFSO==null && other.getDeviceTokenFSO()==null) || 
             (this.deviceTokenFSO!=null &&
              this.deviceTokenFSO.equals(other.getDeviceTokenFSO()))) &&
            ((this.lookupLabel==null && other.getLookupLabel()==null) || 
             (this.lookupLabel!=null &&
              this.lookupLabel.equals(other.getLookupLabel()))) &&
            ((this.newLabel==null && other.getNewLabel()==null) || 
             (this.newLabel!=null &&
              this.newLabel.equals(other.getNewLabel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDeviceTokenCookie() != null) {
            _hashCode += getDeviceTokenCookie().hashCode();
        }
        if (getDeviceTokenFSO() != null) {
            _hashCode += getDeviceTokenFSO().hashCode();
        }
        if (getLookupLabel() != null) {
            _hashCode += getLookupLabel().hashCode();
        }
        if (getNewLabel() != null) {
            _hashCode += getNewLabel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" DeviceTokenCookie=").append(getDeviceTokenCookie());
        buffer.append(" DeviceTokenFSO=").append(getDeviceTokenFSO());
        buffer.append(" LookupLabel=").append(getLookupLabel());
        buffer.append(" NewLabel=").append(getNewLabel());
        return buffer.toString();
    }
}
