/**
 * AnalyzeEventResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Analyze Event Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AnalyzeEventResponse  extends BaseRSAResponse {
    private static final long serialVersionUID = 6209907778214241334L;

    private RiskResult riskResult;

    private CollectableCredential[] collectableCredentials;

    public AnalyzeEventResponse() {
    }

    /**
     * Gets the riskResult value for this AnalyzeEventResponse.
     * 
     * @return riskResult
     */
    public RiskResult getRiskResult() {
        return riskResult;
    }


    /**
     * Sets the riskResult value for this AnalyzeEventResponse.
     * 
     * @param riskResult
     */
    public void setRiskResult(RiskResult riskResult) {
        this.riskResult = riskResult;
    }


    /**
     * Gets the collectableCredentials value for this AnalyzeEventResponse.
     * 
     * @return collectableCredentials
     */
    public CollectableCredential[] getCollectableCredentials() {
        return collectableCredentials;
    }


    /**
     * Sets the collectableCredentials value for this AnalyzeEventResponse.
     * 
     * @param collectableCredentials
     */
    public void setCollectableCredentials(CollectableCredential[] collectableCredentials) {
        this.collectableCredentials = collectableCredentials;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AnalyzeEventResponse)) return false;
        AnalyzeEventResponse other = (AnalyzeEventResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.riskResult==null && other.getRiskResult()==null) || 
             (this.riskResult!=null &&
              this.riskResult.equals(other.getRiskResult()))) &&
            ((this.collectableCredentials==null && other.getCollectableCredentials()==null) || 
             (this.collectableCredentials!=null &&
              java.util.Arrays.equals(this.collectableCredentials, other.getCollectableCredentials())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getRiskResult() != null) {
            _hashCode += getRiskResult().hashCode();
        }
        if (getCollectableCredentials() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCollectableCredentials());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCollectableCredentials(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" RiskResult=").append(getRiskResult());
        buffer.append(" CollectableCredentials=").append(getCollectableCredentials());
        return buffer.toString();
    }
}
