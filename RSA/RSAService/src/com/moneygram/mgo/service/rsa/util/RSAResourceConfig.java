/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.mgo.service.rsa.util;

import com.moneygram.common.service.util.ResourceConfig;


/**
 * 
 * RSA Resource Config.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>TransactionLoaderEJB</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2008</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2010/06/23 13:44:45 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RSAResourceConfig extends ResourceConfig {

    public static final String VERID_MODE_TYPE = "VeridModeType";
    public static final String VERID_RULE_SET = "VeridRuleSet";
    public static final String VERID_ACCOUNT_NAME = "VeridAccountName";
    public static final String VERID_VERIFICATION_TYPE = "VeridVerificationType";
    public static final String VERID_USER_NAME = "VeridUserName";
    public static final String VERID_PASSWORD = "VeridPwd";

    public static final String RSA_ENDPOINT = "RSAEndpoint";
    public static final String RSA_TIMEOUT = "RSATimeout";
    private static int RSA_DEFAULT_TIMEOUT = 15000;

    public static final String RSA_CALLER_ID = "RSACallerId";
    public static final String RSA_CALLER_CREDENTIAL = "RSACallerCredential";
    
    public static final String RSA_INSTANCE_ID = "RSAInstanceId";

    public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/RSAResourceReference";
    
    public static final String USE_RSA_TRANSLATION ="USE_RSA_TRANSLATION";

    /**
     * singleton instance.
     */
    private static RSAResourceConfig instance = null;

    /**
     * Creates new instance of ResourceConfig
     * 
     */
    private RSAResourceConfig() {
        super();
    }

    /**
     * 
     * @return
     */
    public static RSAResourceConfig getInstance() {
        if (instance == null) {
            synchronized (RSAResourceConfig.class) {
                if (instance == null) {
                    instance = new RSAResourceConfig();
                    instance.initResourceConfig();
                }
            }
        }
        return instance;
    }
    
    public String getVeridModeType() {
        return getAttributeValue(VERID_MODE_TYPE);
    }

    public boolean isVeridModeSimulated() {
        return "simulated".equals(getVeridModeType());
    }

    public String getVeridRuleSet() {
        return getAttributeValue(VERID_RULE_SET);
    }

    public String getVeridAccountName() {
        return getAttributeValue(VERID_ACCOUNT_NAME);
    }

    public String getVeridVerificationType() {
        return getAttributeValue(VERID_VERIFICATION_TYPE);
    }

    public String getVeridUserName() {
        return getAttributeValue(VERID_USER_NAME);
    }

    public String getVeridPassword() {
        return getAttributeValue(VERID_PASSWORD);
    }

    public String getRSAEndpoint(String applicationCode) {
        return getAttributeValue(applicationCode +"_"+ RSA_ENDPOINT);
    }

    public int getRSATimeout(String applicationCode) {
        return getIntegerAttributeValue(applicationCode +"_"+ RSA_TIMEOUT, RSA_DEFAULT_TIMEOUT);
    }

    public String getRSACallerId(String applicationCode) {
        return getAttributeValue(applicationCode +"_"+ RSA_CALLER_ID);
    }

    public String getRSACallerCredential(String applicationCode) {
        return getAttributeValue(applicationCode +"_"+ RSA_CALLER_CREDENTIAL);
    }

    /**
     * Returns RSA instance id that is hared by all applications.
     * @return
     */
    public String getRSAInstanceId() {
        return getAttributeValue(RSA_INSTANCE_ID);
    }

    @Override
    protected String getResourceConfigurationJndiName() {
        return RESOURCE_REFERENCE_JNDI;
    }
    
    public String getUseRSATranslation(){
    	return getAttributeValue(USE_RSA_TRANSLATION);
    }
    

}
