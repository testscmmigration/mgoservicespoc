/**
 * MultipleChoiceQuestions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Multiple Choice Questions.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/04 16:22:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MultipleChoiceQuestions  implements java.io.Serializable {
    private static final long serialVersionUID = -3465919639788435381L;

    private java.lang.Long questionsSetId;

    private MultipleChoiceQuestion[] item;

    public MultipleChoiceQuestions() {
    }

    public MultipleChoiceQuestions(
           java.lang.Long questionsSetId,
           MultipleChoiceQuestion[] item) {
           this.questionsSetId = questionsSetId;
           this.item = item;
    }


    /**
     * Gets the questionsSetId value for this MultipleChoiceQuestions.
     * 
     * @return questionsSetId
     */
    public java.lang.Long getQuestionsSetId() {
        return questionsSetId;
    }


    /**
     * Sets the questionsSetId value for this MultipleChoiceQuestions.
     * 
     * @param questionsSetId
     */
    public void setQuestionsSetId(java.lang.Long questionsSetId) {
        this.questionsSetId = questionsSetId;
    }


    /**
     * Gets the item value for this MultipleChoiceQuestions.
     * 
     * @return item
     */
    public MultipleChoiceQuestion[] getItem() {
        return item;
    }


    /**
     * Sets the item value for this MultipleChoiceQuestions.
     * 
     * @param item
     */
    public void setItem(MultipleChoiceQuestion[] item) {
        this.item = item;
    }

    public MultipleChoiceQuestion getItem(int i) {
        return this.item[i];
    }

    public void setItem(int i, MultipleChoiceQuestion _value) {
        this.item[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MultipleChoiceQuestions)) return false;
        MultipleChoiceQuestions other = (MultipleChoiceQuestions) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.questionsSetId==null && other.getQuestionsSetId()==null) || 
             (this.questionsSetId!=null &&
              this.questionsSetId.equals(other.getQuestionsSetId()))) &&
            ((this.item==null && other.getItem()==null) || 
             (this.item!=null &&
              java.util.Arrays.equals(this.item, other.getItem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQuestionsSetId() != null) {
            _hashCode += getQuestionsSetId().hashCode();
        }
        if (getItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" QuestionsSetId=").append(getQuestionsSetId());
        buffer.append(" Questions=").append(getItem());
        return buffer.toString();
    }
}
