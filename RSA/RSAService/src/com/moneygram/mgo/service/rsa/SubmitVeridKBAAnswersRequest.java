/**
 * SubmitVeridKBAAnswersRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Submit Verid KBA Answers Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/04 16:22:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SubmitVeridKBAAnswersRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 7505108627649863613L;

    private java.lang.String vendorAuthenticationEventId;

    private long eventSequenceNumber;

    private long questionsSetId;

    private MultipleChoiceAnswer[] answers;

    public SubmitVeridKBAAnswersRequest() {
    }

    public SubmitVeridKBAAnswersRequest(
           RequestHeader header,
           java.lang.String vendorAuthenticationEventId,
           long eventSequenceNumber,
           long questionsSetId,
           MultipleChoiceAnswer[] answers) {
        super(
            header);
        this.vendorAuthenticationEventId = vendorAuthenticationEventId;
        this.eventSequenceNumber = eventSequenceNumber;
        this.questionsSetId = questionsSetId;
        this.answers = answers;
    }


    /**
     * Gets the vendorAuthenticationEventId value for this SubmitVeridKBAAnswersRequest.
     * 
     * @return vendorAuthenticationEventId
     */
    public java.lang.String getVendorAuthenticationEventId() {
        return vendorAuthenticationEventId;
    }


    /**
     * Sets the vendorAuthenticationEventId value for this SubmitVeridKBAAnswersRequest.
     * 
     * @param vendorAuthenticationEventId
     */
    public void setVendorAuthenticationEventId(java.lang.String vendorAuthenticationEventId) {
        this.vendorAuthenticationEventId = vendorAuthenticationEventId;
    }


    /**
     * Gets the eventSequenceNumber value for this SubmitVeridKBAAnswersRequest.
     * 
     * @return eventSequenceNumber
     */
    public long getEventSequenceNumber() {
        return eventSequenceNumber;
    }


    /**
     * Sets the eventSequenceNumber value for this SubmitVeridKBAAnswersRequest.
     * 
     * @param eventSequenceNumber
     */
    public void setEventSequenceNumber(long eventSequenceNumber) {
        this.eventSequenceNumber = eventSequenceNumber;
    }


    /**
     * Gets the questionsSetId value for this SubmitVeridKBAAnswersRequest.
     * 
     * @return questionsSetId
     */
    public long getQuestionsSetId() {
        return questionsSetId;
    }


    /**
     * Sets the questionsSetId value for this SubmitVeridKBAAnswersRequest.
     * 
     * @param questionsSetId
     */
    public void setQuestionsSetId(long questionsSetId) {
        this.questionsSetId = questionsSetId;
    }


    /**
     * Gets the answers value for this SubmitVeridKBAAnswersRequest.
     * 
     * @return answers
     */
    public MultipleChoiceAnswer[] getAnswers() {
        return answers;
    }


    /**
     * Sets the answers value for this SubmitVeridKBAAnswersRequest.
     * 
     * @param answers
     */
    public void setAnswers(MultipleChoiceAnswer[] answers) {
        this.answers = answers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitVeridKBAAnswersRequest)) return false;
        SubmitVeridKBAAnswersRequest other = (SubmitVeridKBAAnswersRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.vendorAuthenticationEventId==null && other.getVendorAuthenticationEventId()==null) || 
             (this.vendorAuthenticationEventId!=null &&
              this.vendorAuthenticationEventId.equals(other.getVendorAuthenticationEventId()))) &&
            this.eventSequenceNumber == other.getEventSequenceNumber() &&
            this.questionsSetId == other.getQuestionsSetId() &&
            ((this.answers==null && other.getAnswers()==null) || 
             (this.answers!=null &&
              java.util.Arrays.equals(this.answers, other.getAnswers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getVendorAuthenticationEventId() != null) {
            _hashCode += getVendorAuthenticationEventId().hashCode();
        }
        _hashCode += new Long(getEventSequenceNumber()).hashCode();
        _hashCode += new Long(getQuestionsSetId()).hashCode();
        if (getAnswers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAnswers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAnswers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" VendorAuthenticationEventId=").append(getVendorAuthenticationEventId());
        buffer.append(" EventSequenceNumber=").append(getEventSequenceNumber());
        buffer.append(" QuestionsSetId=").append(getQuestionsSetId());
        buffer.append(" Answers=").append(getAnswers());
        return buffer.toString();
    }
}
