/*
 * Created on Sep 1, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.AuthenticateRequest;
import com.moneygram.mgo.service.rsa.AuthenticateResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.ChallengeResult;
import com.moneygram.mgo.service.rsa.TextAnswer;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.ChallengeQuestion;
import com.rsa.csd.ws.ChallengeQuestionData;
import com.rsa.csd.ws.ChallengeQuestionDataPayload;
import com.rsa.csd.ws.ChallengeQuestionMatchResult;
import com.rsa.csd.ws.CredentialDataList;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.RequestType;

/**
 * 
 * Authenticate Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:38:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AuthenticateCommand extends BaseRSACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(AuthenticateCommand.class);

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest baseRSARequest) throws CommandException {
        AuthenticateRequest commandRequest = (AuthenticateRequest) baseRSARequest;
        
        com.rsa.csd.ws.AuthenticateRequest request = new com.rsa.csd.ws.AuthenticateRequest();
        setupGenericRequest(request, commandRequest);

        request.setCredentialDataList(createCredentialDataList(commandRequest.getAnswers()));
        
        return request;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.AuthenticateResponse authenticateResponse = null;
        try {
            com.rsa.csd.ws.AuthenticateRequest authenticateRequest = (com.rsa.csd.ws.AuthenticateRequest)genericRequest;
            authenticateResponse = client.authenticate(authenticateRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA authenticate user request", e);
        }

        AuthenticateResponse response = createAuthenticateResponse(authenticateResponse);

        return response;
    }

    private AuthenticateResponse createAuthenticateResponse(com.rsa.csd.ws.AuthenticateResponse authenticateResponse) throws CommandException {
        if (authenticateResponse == null || 
                authenticateResponse.getCredentialAuthResultList() == null ||
                authenticateResponse.getCredentialAuthResultList().getChallengeQuestionAuthResult() == null ||
                authenticateResponse.getCredentialAuthResultList().getChallengeQuestionAuthResult().getPayload() == null ||
                authenticateResponse.getCredentialAuthResultList().getChallengeQuestionAuthResult().getPayload().getChallengeQuestionMatchResult() == null
                ) {
            throw new CommandException("RSA authenticate response is missing ChallengeQuestionMatchResult");
        }
        
        ChallengeQuestionMatchResult matchResult = authenticateResponse.getCredentialAuthResultList().getChallengeQuestionAuthResult().getPayload().getChallengeQuestionMatchResult();
        
        ChallengeResult challengeResult = new ChallengeResult(
                matchResult.getFailCount(), matchResult.getMatchCount(), 
                matchResult.getWasSuccessful(), matchResult.getShouldBlock());

        AuthenticateResponse response = new AuthenticateResponse();
        response.setChallengeResult(challengeResult );
        return response;
    }

    private CredentialDataList createCredentialDataList(TextAnswer[] answers) throws CommandException {
        if (answers == null || answers.length == 0) {
            throw new DataFormatException("Answers are mising");
        }
        
        ChallengeQuestion[] challengeQuestions = new ChallengeQuestion[answers.length];
        for (int i = 0; i < answers.length; i++) {
            ChallengeQuestion challengeQuestion = new ChallengeQuestion();
            challengeQuestion.setUserAnswer(answers[i].getAnswerText());
            challengeQuestion.setQuestionId(answers[i].getQuestionId());
            challengeQuestions[i] = challengeQuestion;
        }
        
        ChallengeQuestionDataPayload payload = new ChallengeQuestionDataPayload(challengeQuestions);
        ChallengeQuestionData challengeQuestionData = new ChallengeQuestionData(payload );

        CredentialDataList credentialDataList = new CredentialDataList();
        credentialDataList.setChallengeQuestionData(challengeQuestionData);
        
        return credentialDataList;
    }

    @Override
    protected RequestType getRequestType() {
        return RequestType.AUTHENTICATE;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof AuthenticateRequest;
    }


}
