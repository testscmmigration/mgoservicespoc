/*
 * Created on Sep 1, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.AuthenticateRequest;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.SaveAnswersRequest;
import com.moneygram.mgo.service.rsa.TextAnswer;
import com.moneygram.mgo.service.rsa.UpdateUserRequest;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.ChallengeQuestion;
import com.rsa.csd.ws.ChallengeQuestionActionType;
import com.rsa.csd.ws.ChallengeQuestionList;
import com.rsa.csd.ws.CredentialManagementRequestList;
import com.rsa.csd.ws.GenericActionType;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.Image;
import com.rsa.csd.ws.RequestType;
import com.rsa.csd.ws.SiteToUserData;
import com.rsa.csd.ws.UpdateUserResponse;
import com.rsa.csd.ws.UserStatus;
import com.rsa.csd.ws.WSUserType;

/**
 * 
 * User Update Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateUserCommand extends BaseRSACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(UpdateUserCommand.class);
    private static final GenericActionType[] actions = new GenericActionType[]{GenericActionType.SET_IMAGE, GenericActionType.SET_PHRASE, GenericActionType.SET_USER_STATUS};

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest baseRSARequest) throws CommandException {
        UpdateUserRequest commandRequest = (UpdateUserRequest) baseRSARequest;
        
        com.rsa.csd.ws.UpdateUserRequest request = new com.rsa.csd.ws.UpdateUserRequest();
        setupGenericRequest(request, commandRequest);

        CredentialManagementRequestList credentialManagementRequestList = 
            createCredentialManagementRequestList(ChallengeQuestionActionType.SET_USER_QUESTION, 
                    createChallengeQuestionsList(commandRequest.getSaveAnswersRequest())); 

        request.setCredentialManagementRequestList(credentialManagementRequestList);
        
        return request;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.UpdateUserResponse updateResponse = null;
        try {
            //process answers to security questions
            com.rsa.csd.ws.UpdateUserRequest updateRequest = (com.rsa.csd.ws.UpdateUserRequest)genericRequest;
            updateResponse = client.updateUser(updateRequest);
            
            //check generic rsa response
            checkBaseRSAResponseStatus(updateResponse);

            UpdateUserRequest commandRequest = (UpdateUserRequest) baseRSARequest;

            //if SaveImageRequest is specified then make a 2nd rsa update call
            if (commandRequest.getSaveImageRequest() != null) {
                //process image and phrase selection
                updateRequest.setCredentialManagementRequestList(null);
                setGenericActionTypes(updateRequest, actions);
                setUserType(updateRequest, WSUserType.PERSISTENT);
                setUserStatus(updateRequest, UserStatus.VERIFIED);
                updateRequest.setSiteToUserData(createSiteToUserData(commandRequest));

                //replace previous response with new one
                updateResponse = client.updateUser(updateRequest);
            }
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA update user request", e);
        }

        com.moneygram.mgo.service.rsa.UpdateUserResponse response = createUpdateResponse(updateResponse);

        return response;
    }

    private SiteToUserData createSiteToUserData(UpdateUserRequest commandRequest) throws DataFormatException {
        com.moneygram.mgo.service.rsa.SiteToUserData saveImageRequest = commandRequest.getSaveImageRequest();
        com.moneygram.mgo.service.rsa.Image mgoImage = saveImageRequest.getImage();
        Image image = new Image(mgoImage .getAltText(), mgoImage.getData(), mgoImage.getHeight(), mgoImage.getImageId(), mgoImage.getPath(), mgoImage.getWidth());

        SiteToUserData siteToUserData = new SiteToUserData(saveImageRequest.getPhrase(), image);
        return siteToUserData;
    }

    private ChallengeQuestionList createChallengeQuestionsList(SaveAnswersRequest saveAnswersRequest) throws CommandException {
        if (saveAnswersRequest == null) {
            throw new DataFormatException("SaveAnswersRequest can not be null");
        }
        if (saveAnswersRequest.getAnswers() == null) {
            throw new DataFormatException("SaveAnswersRequest is missing answers");
        }
        
        TextAnswer[] answers = saveAnswersRequest.getAnswers();
        ChallengeQuestion[] challengeQuestions = new ChallengeQuestion[answers.length];
        for (int i = 0; i < challengeQuestions.length; i++) {
            ChallengeQuestion challengeQuestion = new ChallengeQuestion();
            challengeQuestion.setUserAnswer(answers[i].getAnswerText());
            challengeQuestion.setQuestionId(answers[i].getQuestionId());
            challengeQuestions[i] = challengeQuestion; 
        }

        ChallengeQuestionList questionList = new ChallengeQuestionList();
        questionList.setChallengeQuestions(challengeQuestions);
        return questionList;
    }

    private com.moneygram.mgo.service.rsa.UpdateUserResponse createUpdateResponse(UpdateUserResponse updateResponse) throws CommandException {
        //check generic rsa response
        checkBaseRSAResponseStatus(updateResponse);
        
        com.moneygram.mgo.service.rsa.UpdateUserResponse response = 
            new com.moneygram.mgo.service.rsa.UpdateUserResponse();
        return response;
    }

    @Override
    protected RequestType getRequestType() {
        return RequestType.UPDATEUSER;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof UpdateUserRequest;
    }


}
