/**
 * EventType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;


/**
 * 
 * Event Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2010/09/07 16:44:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class EventType implements java.io.Serializable {
    private static final long serialVersionUID = 7334824505560311245L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EventType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SESSION_SIGNIN = "SESSION_SIGNIN";
    public static final java.lang.String _CHANGE_PASSWORD = "CHANGE_PASSWORD";
    public static final java.lang.String _CHANGE_ADDRESS = "CHANGE_ADDRESS";
    public static final java.lang.String _CHANGE_LIFE_QUESTIONS = "CHANGE_LIFE_QUESTIONS";
    public static final java.lang.String _FAILED_LOGIN_ATTEMPT = "FAILED_LOGIN_ATTEMPT";
    public static final java.lang.String _FAILED_CHANGE_PASSWORD_ATTEMPT = "FAILED_CHANGE_PASSWORD_ATTEMPT";
    public static final java.lang.String _CLIENT_DEFINED = "CLIENT_DEFINED";
    public static final java.lang.String _CHECK_DEVICE = "CHECK_DEVICE";
    public static final java.lang.String _OLB_ENROLL = "OLB_ENROLL";
    public static final java.lang.String _OPEN_NEW_ACCOUNT = "OPEN_NEW_ACCOUNT";
    public static final java.lang.String _SUCCESSFUL_OLB_ENROLL_ATTEMPT = "SUCCESSFUL_OLB_ENROLL_ATTEMPT";
    public static final java.lang.String _SUCCESSFUL_OPEN_NEW_ACCOUNT = "SUCCESSFUL_OPEN_NEW_ACCOUNT";
    public static final java.lang.String _FAILED_OLB_ENROLL_ATTEMPT = "FAILED_OLB_ENROLL_ATTEMPT";
    public static final java.lang.String _FAILED_OPEN_NEW_ACCOUNT = "FAILED_OPEN_NEW_ACCOUNT";
    public static final java.lang.String _PAYMENT = "PAYMENT";
    public static final EventType SESSION_SIGNIN = new EventType(_SESSION_SIGNIN);
    public static final EventType CHANGE_PASSWORD = new EventType(_CHANGE_PASSWORD);
    public static final EventType CHANGE_ADDRESS = new EventType(_CHANGE_ADDRESS);
    public static final EventType CHANGE_LIFE_QUESTIONS = new EventType(_CHANGE_LIFE_QUESTIONS);
    public static final EventType FAILED_LOGIN_ATTEMPT = new EventType(_FAILED_LOGIN_ATTEMPT);
    public static final EventType FAILED_CHANGE_PASSWORD_ATTEMPT = new EventType(_FAILED_CHANGE_PASSWORD_ATTEMPT);
    public static final EventType CLIENT_DEFINED = new EventType(_CLIENT_DEFINED);
    public static final EventType CHECK_DEVICE = new EventType(_CHECK_DEVICE);
    public static final EventType OLB_ENROLL = new EventType(_OLB_ENROLL);
    public static final EventType OPEN_NEW_ACCOUNT = new EventType(_OPEN_NEW_ACCOUNT);
    public static final EventType SUCCESSFUL_OLB_ENROLL_ATTEMPT = new EventType(_SUCCESSFUL_OLB_ENROLL_ATTEMPT);
    public static final EventType SUCCESSFUL_OPEN_NEW_ACCOUNT = new EventType(_SUCCESSFUL_OPEN_NEW_ACCOUNT);
    public static final EventType FAILED_OLB_ENROLL_ATTEMPT = new EventType(_FAILED_OLB_ENROLL_ATTEMPT);
    public static final EventType FAILED_OPEN_NEW_ACCOUNT = new EventType(_FAILED_OPEN_NEW_ACCOUNT);
    public static final EventType PAYMENT = new EventType(_PAYMENT);

    public java.lang.String getValue() { return _value_;}
    public static EventType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EventType enumeration = (EventType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EventType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
