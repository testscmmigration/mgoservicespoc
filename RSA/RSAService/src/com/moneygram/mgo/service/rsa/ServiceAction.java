/**
 * ServiceAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;




/**
 * 
 * Service Action.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2010/06/22 21:58:08 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ServiceAction implements java.io.Serializable {
    private static final long serialVersionUID = 210026108773086751L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ServiceAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _getVeridKBAQuestions = "getVeridKBAQuestions";
    public static final java.lang.String _submitVeridKBAAnswers = "submitVeridKBAAnswers";
    public static final java.lang.String _analyzeEvent = "analyzeEvent";
    public static final java.lang.String _analyzeIdentity = "analyzeIdentity";
    public static final java.lang.String _query = "query";
    public static final java.lang.String _updateUser = "updateUser";
    public static final java.lang.String _challenge = "challenge";
    public static final java.lang.String _authenticate = "authenticate";
    public static final java.lang.String _notify = "notify";
    public static final java.lang.String _getUserImage = "getUserImage";
    public static final ServiceAction getVeridKBAQuestions = new ServiceAction(_getVeridKBAQuestions);
    public static final ServiceAction submitVeridKBAAnswers = new ServiceAction(_submitVeridKBAAnswers);
    public static final ServiceAction analyzeEvent = new ServiceAction(_analyzeEvent);
    public static final ServiceAction analyzeIdentity = new ServiceAction(_analyzeIdentity);
    public static final ServiceAction query = new ServiceAction(_query);
    public static final ServiceAction updateUser = new ServiceAction(_updateUser);
    public static final ServiceAction challenge = new ServiceAction(_challenge);
    public static final ServiceAction authenticate = new ServiceAction(_authenticate);
    public static final ServiceAction notify = new ServiceAction(_notify);
    public static final ServiceAction getUserImage = new ServiceAction(_getUserImage);

    public java.lang.String getValue() { return _value_;}
    public static ServiceAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ServiceAction enumeration = (ServiceAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ServiceAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
}
