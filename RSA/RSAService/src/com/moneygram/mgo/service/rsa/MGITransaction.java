package com.moneygram.mgo.service.rsa;

import com.moneygram.mgo.shared.ConsumerName;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * MGI Transaction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/09/09 19:33:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MGITransaction  implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    /* MGI transaction id */
    private java.lang.String transactionId;

    /* will derive eventData.TransactionData.otherAccountType */
    private MGITransactionType transactionType;

    private ReceiverConsumerName receiverName;

    private java.lang.String billerName;

    /* eventData.transactionData.amount.amount */
    private java.math.BigDecimal sendAmount;

    /* eventData.transactionData.amount.currency */
    private java.lang.String sendCurrency;

    /* eventData.clientDefinedAttributeList[?] */
    private java.lang.String receiveCurrency;

    /* eventData.clientDefinedAttributeList[transaction_date] */
    private java.util.Calendar submissionDate;

    /* eventData.clientDefinedAttributeList[delivery_option]. For
     * MT use only. */
    private java.lang.String deliveryOption;

    /* eventData.transactionData.executionSpeed */
    private ExecutionSpeed executionSpeed;

    /* eventType.transactionData.myAccountData.accountType */
    private FundingMethod fundingMethod;

    /* used to derive transfer medium for MT */
    private java.lang.String sendCountry;

    /* eventData.transactionData.otherAccountData.accountCountry */
    private java.lang.String receiveCountry;

    /* eventData.clientDefinedAttributeList[intended_receiving_state] */
    private java.lang.String receiveState;

    /* eventType.transactionData.myAccountData.accountNumber */
    private java.lang.String sendAccountNumber;

    /* eventType.transactionData.otherAccountData.accountNumber */
    private java.lang.String receiveAccountNumber;

    public MGITransaction() {
    }

    public MGITransaction(
           java.lang.String transactionId,
           MGITransactionType transactionType,
           ReceiverConsumerName receiverName,
           java.lang.String billerName,
           java.math.BigDecimal sendAmount,
           java.lang.String sendCurrency,
           java.lang.String receiveCurrency,
           java.util.Calendar submissionDate,
           java.lang.String deliveryOption,
           ExecutionSpeed executionSpeed,
           FundingMethod fundingMethod,
           java.lang.String sendCountry,
           java.lang.String receiveCountry,
           java.lang.String receiveState,
           java.lang.String sendAccountNumber,
           java.lang.String receiveAccountNumber) {
           this.transactionId = transactionId;
           this.transactionType = transactionType;
           this.receiverName = receiverName;
           this.billerName = billerName;
           this.sendAmount = sendAmount;
           this.sendCurrency = sendCurrency;
           this.receiveCurrency = receiveCurrency;
           this.submissionDate = submissionDate;
           this.deliveryOption = deliveryOption;
           this.executionSpeed = executionSpeed;
           this.fundingMethod = fundingMethod;
           this.sendCountry = sendCountry;
           this.receiveCountry = receiveCountry;
           this.receiveState = receiveState;
           this.sendAccountNumber = sendAccountNumber;
           this.receiveAccountNumber = receiveAccountNumber;
    }


    /**
     * Gets the transactionId value for this MGITransaction.
     * 
     * @return transactionId   * MGI transaction id
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this MGITransaction.
     * 
     * @param transactionId   * MGI transaction id
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the transactionType value for this MGITransaction.
     * 
     * @return transactionType   * will derive eventData.TransactionData.otherAccountType
     */
    public MGITransactionType getTransactionType() {
        return transactionType;
    }


    /**
     * Sets the transactionType value for this MGITransaction.
     * 
     * @param transactionType   * will derive eventData.TransactionData.otherAccountType
     */
    public void setTransactionType(MGITransactionType transactionType) {
        this.transactionType = transactionType;
    }


    /**
     * Gets the receiverName value for this MGITransaction.
     * 
     * @return receiverName
     */
    public ReceiverConsumerName getReceiverName() {
        return receiverName;
    }


    /**
     * Sets the receiverName value for this MGITransaction.
     * 
     * @param receiverName
     */
    public void setReceiverName(ReceiverConsumerName receiverName) {
        this.receiverName = receiverName;
    }


    /**
     * Gets the billerName value for this MGITransaction.
     * 
     * @return billerName
     */
    public java.lang.String getBillerName() {
        return billerName;
    }


    /**
     * Sets the billerName value for this MGITransaction.
     * 
     * @param billerName
     */
    public void setBillerName(java.lang.String billerName) {
        this.billerName = billerName;
    }


    /**
     * Gets the sendAmount value for this MGITransaction.
     * 
     * @return sendAmount   * eventData.transactionData.amount.amount
     */
    public java.math.BigDecimal getSendAmount() {
        return sendAmount;
    }


    /**
     * Sets the sendAmount value for this MGITransaction.
     * 
     * @param sendAmount   * eventData.transactionData.amount.amount
     */
    public void setSendAmount(java.math.BigDecimal sendAmount) {
        this.sendAmount = sendAmount;
    }


    /**
     * Gets the sendCurrency value for this MGITransaction.
     * 
     * @return sendCurrency   * eventData.transactionData.amount.currency
     */
    public java.lang.String getSendCurrency() {
        return sendCurrency;
    }


    /**
     * Sets the sendCurrency value for this MGITransaction.
     * 
     * @param sendCurrency   * eventData.transactionData.amount.currency
     */
    public void setSendCurrency(java.lang.String sendCurrency) {
        this.sendCurrency = sendCurrency;
    }


    /**
     * Gets the receiveCurrency value for this MGITransaction.
     * 
     * @return receiveCurrency   * eventData.clientDefinedAttributeList[?]
     */
    public java.lang.String getReceiveCurrency() {
        return receiveCurrency;
    }


    /**
     * Sets the receiveCurrency value for this MGITransaction.
     * 
     * @param receiveCurrency   * eventData.clientDefinedAttributeList[?]
     */
    public void setReceiveCurrency(java.lang.String receiveCurrency) {
        this.receiveCurrency = receiveCurrency;
    }


    /**
     * Gets the submissionDate value for this MGITransaction.
     * 
     * @return submissionDate   * eventData.clientDefinedAttributeList[transaction_date]
     */
    public java.util.Calendar getSubmissionDate() {
        return submissionDate;
    }


    /**
     * Sets the submissionDate value for this MGITransaction.
     * 
     * @param submissionDate   * eventData.clientDefinedAttributeList[transaction_date]
     */
    public void setSubmissionDate(java.util.Calendar submissionDate) {
        this.submissionDate = submissionDate;
    }


    /**
     * Gets the deliveryOption value for this MGITransaction.
     * 
     * @return deliveryOption   * eventData.clientDefinedAttributeList[delivery_option]. For
     * MT use only.
     */
    public java.lang.String getDeliveryOption() {
        return deliveryOption;
    }


    /**
     * Sets the deliveryOption value for this MGITransaction.
     * 
     * @param deliveryOption   * eventData.clientDefinedAttributeList[delivery_option]. For
     * MT use only.
     */
    public void setDeliveryOption(java.lang.String deliveryOption) {
        this.deliveryOption = deliveryOption;
    }


    /**
     * Gets the executionSpeed value for this MGITransaction.
     * 
     * @return executionSpeed   * eventData.transactionData.executionSpeed
     */
    public ExecutionSpeed getExecutionSpeed() {
        return executionSpeed;
    }


    /**
     * Sets the executionSpeed value for this MGITransaction.
     * 
     * @param executionSpeed   * eventData.transactionData.executionSpeed
     */
    public void setExecutionSpeed(ExecutionSpeed executionSpeed) {
        this.executionSpeed = executionSpeed;
    }


    /**
     * Gets the fundingMethod value for this MGITransaction.
     * 
     * @return fundingMethod   * eventType.transactionData.myAccountData.accountType
     */
    public FundingMethod getFundingMethod() {
        return fundingMethod;
    }


    /**
     * Sets the fundingMethod value for this MGITransaction.
     * 
     * @param fundingMethod   * eventType.transactionData.myAccountData.accountType
     */
    public void setFundingMethod(FundingMethod fundingMethod) {
        this.fundingMethod = fundingMethod;
    }


    /**
     * Gets the sendCountry value for this MGITransaction.
     * 
     * @return sendCountry   * used to derive transfer medium for MT
     */
    public java.lang.String getSendCountry() {
        return sendCountry;
    }


    /**
     * Sets the sendCountry value for this MGITransaction.
     * 
     * @param sendCountry   * used to derive transfer medium for MT
     */
    public void setSendCountry(java.lang.String sendCountry) {
        this.sendCountry = sendCountry;
    }


    /**
     * Gets the receiveCountry value for this MGITransaction.
     * 
     * @return receiveCountry   * eventData.transactionData.otherAccountData.accountCountry
     */
    public java.lang.String getReceiveCountry() {
        return receiveCountry;
    }


    /**
     * Sets the receiveCountry value for this MGITransaction.
     * 
     * @param receiveCountry   * eventData.transactionData.otherAccountData.accountCountry
     */
    public void setReceiveCountry(java.lang.String receiveCountry) {
        this.receiveCountry = receiveCountry;
    }


    /**
     * Gets the receiveState value for this MGITransaction.
     * 
     * @return receiveState   * eventData.clientDefinedAttributeList[intended_receiving_state]
     */
    public java.lang.String getReceiveState() {
        return receiveState;
    }


    /**
     * Sets the receiveState value for this MGITransaction.
     * 
     * @param receiveState   * eventData.clientDefinedAttributeList[intended_receiving_state]
     */
    public void setReceiveState(java.lang.String receiveState) {
        this.receiveState = receiveState;
    }


    /**
     * Gets the sendAccountNumber value for this MGITransaction.
     * 
     * @return sendAccountNumber   * eventType.transactionData.myAccountData.accountNumber
     */
    public java.lang.String getSendAccountNumber() {
        return sendAccountNumber;
    }


    /**
     * Sets the sendAccountNumber value for this MGITransaction.
     * 
     * @param sendAccountNumber   * eventType.transactionData.myAccountData.accountNumber
     */
    public void setSendAccountNumber(java.lang.String sendAccountNumber) {
        this.sendAccountNumber = sendAccountNumber;
    }


    /**
     * Gets the receiveAccountNumber value for this MGITransaction.
     * 
     * @return receiveAccountNumber   * eventType.transactionData.otherAccountData.accountNumber
     */
    public java.lang.String getReceiveAccountNumber() {
        return receiveAccountNumber;
    }


    /**
     * Sets the receiveAccountNumber value for this MGITransaction.
     * 
     * @param receiveAccountNumber   * eventType.transactionData.otherAccountData.accountNumber
     */
    public void setReceiveAccountNumber(java.lang.String receiveAccountNumber) {
        this.receiveAccountNumber = receiveAccountNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MGITransaction)) return false;
        MGITransaction other = (MGITransaction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.transactionType==null && other.getTransactionType()==null) || 
             (this.transactionType!=null &&
              this.transactionType.equals(other.getTransactionType()))) &&
            ((this.receiverName==null && other.getReceiverName()==null) || 
             (this.receiverName!=null &&
              this.receiverName.equals(other.getReceiverName()))) &&
            ((this.billerName==null && other.getBillerName()==null) || 
             (this.billerName!=null &&
              this.billerName.equals(other.getBillerName()))) &&
            ((this.sendAmount==null && other.getSendAmount()==null) || 
             (this.sendAmount!=null &&
              this.sendAmount.equals(other.getSendAmount()))) &&
            ((this.sendCurrency==null && other.getSendCurrency()==null) || 
             (this.sendCurrency!=null &&
              this.sendCurrency.equals(other.getSendCurrency()))) &&
            ((this.receiveCurrency==null && other.getReceiveCurrency()==null) || 
             (this.receiveCurrency!=null &&
              this.receiveCurrency.equals(other.getReceiveCurrency()))) &&
            ((this.submissionDate==null && other.getSubmissionDate()==null) || 
             (this.submissionDate!=null &&
              this.submissionDate.equals(other.getSubmissionDate()))) &&
            ((this.deliveryOption==null && other.getDeliveryOption()==null) || 
             (this.deliveryOption!=null &&
              this.deliveryOption.equals(other.getDeliveryOption()))) &&
            ((this.executionSpeed==null && other.getExecutionSpeed()==null) || 
             (this.executionSpeed!=null &&
              this.executionSpeed.equals(other.getExecutionSpeed()))) &&
            ((this.fundingMethod==null && other.getFundingMethod()==null) || 
             (this.fundingMethod!=null &&
              this.fundingMethod.equals(other.getFundingMethod()))) &&
            ((this.sendCountry==null && other.getSendCountry()==null) || 
             (this.sendCountry!=null &&
              this.sendCountry.equals(other.getSendCountry()))) &&
            ((this.receiveCountry==null && other.getReceiveCountry()==null) || 
             (this.receiveCountry!=null &&
              this.receiveCountry.equals(other.getReceiveCountry()))) &&
            ((this.receiveState==null && other.getReceiveState()==null) || 
             (this.receiveState!=null &&
              this.receiveState.equals(other.getReceiveState()))) &&
            ((this.sendAccountNumber==null && other.getSendAccountNumber()==null) || 
             (this.sendAccountNumber!=null &&
              this.sendAccountNumber.equals(other.getSendAccountNumber()))) &&
            ((this.receiveAccountNumber==null && other.getReceiveAccountNumber()==null) || 
             (this.receiveAccountNumber!=null &&
              this.receiveAccountNumber.equals(other.getReceiveAccountNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getTransactionType() != null) {
            _hashCode += getTransactionType().hashCode();
        }
        if (getReceiverName() != null) {
            _hashCode += getReceiverName().hashCode();
        }
        if (getBillerName() != null) {
            _hashCode += getBillerName().hashCode();
        }
        if (getSendAmount() != null) {
            _hashCode += getSendAmount().hashCode();
        }
        if (getSendCurrency() != null) {
            _hashCode += getSendCurrency().hashCode();
        }
        if (getReceiveCurrency() != null) {
            _hashCode += getReceiveCurrency().hashCode();
        }
        if (getSubmissionDate() != null) {
            _hashCode += getSubmissionDate().hashCode();
        }
        if (getDeliveryOption() != null) {
            _hashCode += getDeliveryOption().hashCode();
        }
        if (getExecutionSpeed() != null) {
            _hashCode += getExecutionSpeed().hashCode();
        }
        if (getFundingMethod() != null) {
            _hashCode += getFundingMethod().hashCode();
        }
        if (getSendCountry() != null) {
            _hashCode += getSendCountry().hashCode();
        }
        if (getReceiveCountry() != null) {
            _hashCode += getReceiveCountry().hashCode();
        }
        if (getReceiveState() != null) {
            _hashCode += getReceiveState().hashCode();
        }
        if (getSendAccountNumber() != null) {
            _hashCode += getSendAccountNumber().hashCode();
        }
        if (getReceiveAccountNumber() != null) {
            _hashCode += getReceiveAccountNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * Returns true for MoneyTransfer transaction type.
     * @return
     */
    public boolean isMoneyTransfer() {
        return getTransactionType() != null && MGITransactionType.MoneyTransfer.getValue().equals(getTransactionType().getValue());
    }

    /**
     * Returns true for BillPay transaction type.
     * @return
     */
    public boolean isBillPay() {
        return getTransactionType() != null && MGITransactionType.BillPay.getValue().equals(getTransactionType().getValue());
    }

    /**
     * Returns true if send and receive countries are different.
     * @return
     */
    public boolean isInternationTransfer() {
        return getSendCountry() != null && (! getSendCountry().equalsIgnoreCase(getReceiveCountry()));
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" TransactionId=").append(getTransactionId());
        buffer.append(" TransactionType=").append(getTransactionType());
        buffer.append(" ReceiverName=").append(getReceiverName());
        buffer.append(" BillerName=").append(getBillerName());
        buffer.append(" SendAmount=").append(getSendAmount());
        buffer.append(" SendCurrency=").append(getSendCurrency());
        buffer.append(" ReceiveCurrency=").append(getReceiveCurrency());
        buffer.append(" SubmissionDate=").append(getSubmissionDate());
        buffer.append(" DeliveryOption=").append(getDeliveryOption());
        buffer.append(" ExecutionSpeed=").append(getExecutionSpeed());
        buffer.append(" FundingMethod=").append(getFundingMethod());
        buffer.append(" SendCountry=").append(getSendCountry());
        buffer.append(" ReceiveCountry=").append(getReceiveCountry());
        buffer.append(" ReceiveState=").append(getReceiveState());
        buffer.append(" SendAccountNumber=").append(getSendAccountNumber());
        buffer.append(" ReceiveAccountNumber=").append(getReceiveAccountNumber());
        return buffer.toString();
    }
}
