/*
 * Created on Aug 4, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import integration.hawaii.verid.com.ComplexDetailType;
import integration.hawaii.verid.com.SimpleDetailType;

/**
 * 
 * Verid Response Wrapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/05 15:31:52 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class VeridResponseWrapper {

    /**
     * Returns response description.
     * @return
     */
    public String getDescription() {
        int length = getDetailsLength();

        StringBuffer buffer = new StringBuffer();
        buffer.append(getDetailHeader());
        
        //process simple details
        for (int i = 0; i < length; i++) {
            SimpleDetailType[] simpleDetails = getSimpleDetail(i);
            if (simpleDetails != null) {
                for (int j = 0; j < simpleDetails.length; j++) {
                    SimpleDetailType detail = simpleDetails[j]; 
                    if (detail != null) {
                        buffer.append("\n");
                        buffer.append(detail.getText());
                    }
                }
            }
        }
        
        //process complex details
        for (int i = 0; i < length; i++) {
            ComplexDetailType[] complexDetails = getComplexDetail(i);
            buffer.append("\n");
            if (complexDetails != null) {
                for (int j = 0; j < complexDetails.length; j++) {
                    ComplexDetailType detail = complexDetails[j]; 
                    if (detail != null) {
                        buffer.append("\n");
                        buffer.append(detail.getHeading());
                    }
                }
            }
        }
        
        return buffer.toString();
    }

    /**
     * Returns response code.
     * @return
     */
    public abstract String getCode();
    
    /**
     * Returns response detail array length. 
     * @return
     */
    protected abstract int getDetailsLength();

    /**
     * Returns response detail at a specified index. 
     * @param index
     * @return
     */
    protected abstract ComplexDetailType[] getComplexDetail(int index);

    /**
     * Returns response detail at a specified index. 
     * @param index
     * @return
     */
    protected abstract SimpleDetailType[] getSimpleDetail(int index);

    /**
     * Returns detail message header.
     * @return
     */
    protected abstract String getDetailHeader();
}
