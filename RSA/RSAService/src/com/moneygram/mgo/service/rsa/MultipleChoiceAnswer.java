/**
 * MultipleChoiceAnswer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Multiple Choice Answer.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/31 21:49:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class MultipleChoiceAnswer  extends Answer {
    private static final long serialVersionUID = -6498095801529194007L;

    private java.lang.Long answerId;

    public MultipleChoiceAnswer() {
    }

    public MultipleChoiceAnswer(
           String questionId,
           java.lang.Long answerId) {
        super(
            questionId);
        this.answerId = answerId;
    }


    /**
     * Gets the answerId value for this MultipleChoiceAnswer.
     * 
     * @return answerId
     */
    public java.lang.Long getAnswerId() {
        return answerId;
    }


    /**
     * Sets the answerId value for this MultipleChoiceAnswer.
     * 
     * @param answerId
     */
    public void setAnswerId(java.lang.Long answerId) {
        this.answerId = answerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MultipleChoiceAnswer)) return false;
        MultipleChoiceAnswer other = (MultipleChoiceAnswer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answerId==null && other.getAnswerId()==null) || 
             (this.answerId!=null &&
              this.answerId.equals(other.getAnswerId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswerId() != null) {
            _hashCode += getAnswerId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" AnswerId="+ getAnswerId();
    }
}
