/**
 * Device.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Device.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/27 19:11:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class Device  implements java.io.Serializable {
    private static final long serialVersionUID = 5403764640575028127L;

    private java.lang.String devicePrint;

    private java.lang.String deviceTokenCookie;

    private java.lang.String deviceTokenFSO;

    private java.lang.String httpAccept;

    private java.lang.String httpAcceptChars;

    private java.lang.String httpAcceptEncoding;

    private java.lang.String httpAcceptLanguage;

    private java.lang.String httpReferrer;

    private java.lang.String ipAddress;

    private java.lang.String userAgent;

    public Device() {
    }

    public Device(
           java.lang.String devicePrint,
           java.lang.String deviceTokenCookie,
           java.lang.String deviceTokenFSO,
           java.lang.String httpAccept,
           java.lang.String httpAcceptChars,
           java.lang.String httpAcceptEncoding,
           java.lang.String httpAcceptLanguage,
           java.lang.String httpReferrer,
           java.lang.String ipAddress,
           java.lang.String userAgent) {
           this.devicePrint = devicePrint;
           this.deviceTokenCookie = deviceTokenCookie;
           this.deviceTokenFSO = deviceTokenFSO;
           this.httpAccept = httpAccept;
           this.httpAcceptChars = httpAcceptChars;
           this.httpAcceptEncoding = httpAcceptEncoding;
           this.httpAcceptLanguage = httpAcceptLanguage;
           this.httpReferrer = httpReferrer;
           this.ipAddress = ipAddress;
           this.userAgent = userAgent;
    }


    /**
     * Gets the devicePrint value for this Device.
     * 
     * @return devicePrint
     */
    public java.lang.String getDevicePrint() {
        return devicePrint;
    }


    /**
     * Sets the devicePrint value for this Device.
     * 
     * @param devicePrint
     */
    public void setDevicePrint(java.lang.String devicePrint) {
        this.devicePrint = devicePrint;
    }


    /**
     * Gets the deviceTokenCookie value for this Device.
     * 
     * @return deviceTokenCookie
     */
    public java.lang.String getDeviceTokenCookie() {
        return deviceTokenCookie;
    }


    /**
     * Sets the deviceTokenCookie value for this Device.
     * 
     * @param deviceTokenCookie
     */
    public void setDeviceTokenCookie(java.lang.String deviceTokenCookie) {
        this.deviceTokenCookie = deviceTokenCookie;
    }


    /**
     * Gets the deviceTokenFSO value for this Device.
     * 
     * @return deviceTokenFSO
     */
    public java.lang.String getDeviceTokenFSO() {
        return deviceTokenFSO;
    }


    /**
     * Sets the deviceTokenFSO value for this Device.
     * 
     * @param deviceTokenFSO
     */
    public void setDeviceTokenFSO(java.lang.String deviceTokenFSO) {
        this.deviceTokenFSO = deviceTokenFSO;
    }


    /**
     * Gets the httpAccept value for this Device.
     * 
     * @return httpAccept
     */
    public java.lang.String getHttpAccept() {
        return httpAccept;
    }


    /**
     * Sets the httpAccept value for this Device.
     * 
     * @param httpAccept
     */
    public void setHttpAccept(java.lang.String httpAccept) {
        this.httpAccept = httpAccept;
    }


    /**
     * Gets the httpAcceptChars value for this Device.
     * 
     * @return httpAcceptChars
     */
    public java.lang.String getHttpAcceptChars() {
        return httpAcceptChars;
    }


    /**
     * Sets the httpAcceptChars value for this Device.
     * 
     * @param httpAcceptChars
     */
    public void setHttpAcceptChars(java.lang.String httpAcceptChars) {
        this.httpAcceptChars = httpAcceptChars;
    }


    /**
     * Gets the httpAcceptEncoding value for this Device.
     * 
     * @return httpAcceptEncoding
     */
    public java.lang.String getHttpAcceptEncoding() {
        return httpAcceptEncoding;
    }


    /**
     * Sets the httpAcceptEncoding value for this Device.
     * 
     * @param httpAcceptEncoding
     */
    public void setHttpAcceptEncoding(java.lang.String httpAcceptEncoding) {
        this.httpAcceptEncoding = httpAcceptEncoding;
    }


    /**
     * Gets the httpAcceptLanguage value for this Device.
     * 
     * @return httpAcceptLanguage
     */
    public java.lang.String getHttpAcceptLanguage() {
        return httpAcceptLanguage;
    }


    /**
     * Sets the httpAcceptLanguage value for this Device.
     * 
     * @param httpAcceptLanguage
     */
    public void setHttpAcceptLanguage(java.lang.String httpAcceptLanguage) {
        this.httpAcceptLanguage = httpAcceptLanguage;
    }


    /**
     * Gets the httpReferrer value for this Device.
     * 
     * @return httpReferrer
     */
    public java.lang.String getHttpReferrer() {
        return httpReferrer;
    }


    /**
     * Sets the httpReferrer value for this Device.
     * 
     * @param httpReferrer
     */
    public void setHttpReferrer(java.lang.String httpReferrer) {
        this.httpReferrer = httpReferrer;
    }


    /**
     * Gets the ipAddress value for this Device.
     * 
     * @return ipAddress
     */
    public java.lang.String getIpAddress() {
        return ipAddress;
    }


    /**
     * Sets the ipAddress value for this Device.
     * 
     * @param ipAddress
     */
    public void setIpAddress(java.lang.String ipAddress) {
        this.ipAddress = ipAddress;
    }


    /**
     * Gets the userAgent value for this Device.
     * 
     * @return userAgent
     */
    public java.lang.String getUserAgent() {
        return userAgent;
    }


    /**
     * Sets the userAgent value for this Device.
     * 
     * @param userAgent
     */
    public void setUserAgent(java.lang.String userAgent) {
        this.userAgent = userAgent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Device)) return false;
        Device other = (Device) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.devicePrint==null && other.getDevicePrint()==null) || 
             (this.devicePrint!=null &&
              this.devicePrint.equals(other.getDevicePrint()))) &&
            ((this.deviceTokenCookie==null && other.getDeviceTokenCookie()==null) || 
             (this.deviceTokenCookie!=null &&
              this.deviceTokenCookie.equals(other.getDeviceTokenCookie()))) &&
            ((this.deviceTokenFSO==null && other.getDeviceTokenFSO()==null) || 
             (this.deviceTokenFSO!=null &&
              this.deviceTokenFSO.equals(other.getDeviceTokenFSO()))) &&
            ((this.httpAccept==null && other.getHttpAccept()==null) || 
             (this.httpAccept!=null &&
              this.httpAccept.equals(other.getHttpAccept()))) &&
            ((this.httpAcceptChars==null && other.getHttpAcceptChars()==null) || 
             (this.httpAcceptChars!=null &&
              this.httpAcceptChars.equals(other.getHttpAcceptChars()))) &&
            ((this.httpAcceptEncoding==null && other.getHttpAcceptEncoding()==null) || 
             (this.httpAcceptEncoding!=null &&
              this.httpAcceptEncoding.equals(other.getHttpAcceptEncoding()))) &&
            ((this.httpAcceptLanguage==null && other.getHttpAcceptLanguage()==null) || 
             (this.httpAcceptLanguage!=null &&
              this.httpAcceptLanguage.equals(other.getHttpAcceptLanguage()))) &&
            ((this.httpReferrer==null && other.getHttpReferrer()==null) || 
             (this.httpReferrer!=null &&
              this.httpReferrer.equals(other.getHttpReferrer()))) &&
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            ((this.userAgent==null && other.getUserAgent()==null) || 
             (this.userAgent!=null &&
              this.userAgent.equals(other.getUserAgent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDevicePrint() != null) {
            _hashCode += getDevicePrint().hashCode();
        }
        if (getDeviceTokenCookie() != null) {
            _hashCode += getDeviceTokenCookie().hashCode();
        }
        if (getDeviceTokenFSO() != null) {
            _hashCode += getDeviceTokenFSO().hashCode();
        }
        if (getHttpAccept() != null) {
            _hashCode += getHttpAccept().hashCode();
        }
        if (getHttpAcceptChars() != null) {
            _hashCode += getHttpAcceptChars().hashCode();
        }
        if (getHttpAcceptEncoding() != null) {
            _hashCode += getHttpAcceptEncoding().hashCode();
        }
        if (getHttpAcceptLanguage() != null) {
            _hashCode += getHttpAcceptLanguage().hashCode();
        }
        if (getHttpReferrer() != null) {
            _hashCode += getHttpReferrer().hashCode();
        }
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        if (getUserAgent() != null) {
            _hashCode += getUserAgent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" DevicePrint=").append(getDevicePrint());
        buffer.append(" DeviceTokenCookie=").append(getDeviceTokenCookie());
        buffer.append(" DeviceTokenFSO=").append(getDeviceTokenFSO());
        buffer.append(" HttpAccept=").append(getHttpAccept());
        buffer.append(" HttpAcceptChars=").append(getHttpAcceptChars());
        buffer.append(" HttpAcceptEncoding=").append(getHttpAcceptEncoding());
        buffer.append(" HttpAcceptLanguage=").append(getHttpAcceptLanguage());
        buffer.append(" HttpReferrer=").append(getHttpReferrer());
        buffer.append(" IpAddress=").append(getIpAddress());
        buffer.append(" UserAgent=").append(getUserAgent());
        return buffer.toString();
    }

}
