/*
 * Created on Sep 1, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.BrowseQuestionsRequest;
import com.moneygram.mgo.service.rsa.Image;
import com.moneygram.mgo.service.rsa.OpenEndedQuestion;
import com.moneygram.mgo.service.rsa.QueryRequest;
import com.moneygram.mgo.service.rsa.QueryResponse;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.ChallengeQuestion;
import com.rsa.csd.ws.ChallengeQuestionActionType;
import com.rsa.csd.ws.ChallengeQuestionConfig;
import com.rsa.csd.ws.ChallengeQuestionGroup;
import com.rsa.csd.ws.CredentialManagementRequestList;
import com.rsa.csd.ws.GenericActionType;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.RequestType;
import com.rsa.csd.ws.WSUserType;

/**
 * 
 * Query Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class QueryCommand extends BaseRSACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(QueryCommand.class);
    private static final GenericActionType[] actions = new GenericActionType[]{GenericActionType.BROWSE_IMAGES};

    protected com.rsa.csd.ws.QueryRequest createBaseQueryRequest(BaseRSARequest baseRSARequest) throws CommandException {
        
        com.rsa.csd.ws.QueryRequest request = new com.rsa.csd.ws.QueryRequest();
        setupGenericRequest(request, baseRSARequest);
        return request;
    }

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest baseRSARequest) throws CommandException {
        QueryRequest commandRequest = (QueryRequest) baseRSARequest;
        com.rsa.csd.ws.QueryRequest request = createBaseQueryRequest(baseRSARequest);
        CredentialManagementRequestList credentialManagementRequestList = 
            createCredentialManagementRequestList(ChallengeQuestionActionType.BROWSE_QUESTION, 
                    createChallengeQuestionConfig(commandRequest.getBrowseQuestionsRequest()));

        request.setCredentialManagementRequestList(credentialManagementRequestList);
        
        return request;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.QueryResponse queryResponse = null;
        try {
            com.rsa.csd.ws.QueryRequest queryRequest = (com.rsa.csd.ws.QueryRequest)genericRequest;
            //query questions
            queryResponse = client.query(queryRequest);
            
            //check generic rsa response
            checkBaseRSAResponseStatus(queryResponse);

            QueryRequest commandRequest = (QueryRequest) baseRSARequest;

            //if images are requested then process browse images rsa request
            if (commandRequest.getBrowseImages() != null && commandRequest.getBrowseImages().booleanValue()) {
                //query images
                queryRequest.setCredentialManagementRequestList(null);
                setGenericActionTypes(queryRequest, actions);
                setUserType(queryRequest, WSUserType.PERSISTENT);

                com.rsa.csd.ws.QueryResponse queryImagesResponse = client.query(queryRequest);
                //set images data into the previous query response
                queryResponse.setSiteToUserBrowseResponse(queryImagesResponse.getSiteToUserBrowseResponse());
            }
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA query request", e);
        }

        QueryResponse response = createQueryResponse(queryResponse);

        return response;
    }

    private ChallengeQuestionConfig createChallengeQuestionConfig(BrowseQuestionsRequest browseQuestionsRequest) throws CommandException {
        if (browseQuestionsRequest == null) {
            throw new DataFormatException("BrowseQuestionsRequest can not be null");
        }
        ChallengeQuestionConfig config = new ChallengeQuestionConfig();
        config.setGroupCount(browseQuestionsRequest.getGroupCount());
        config.setQuestionCount(browseQuestionsRequest.getQuestionCount());
        return config;
    }

    private QueryResponse createQueryResponse(com.rsa.csd.ws.QueryResponse queryResponse) throws CommandException {
        //check generic rsa response
        checkBaseRSAResponseStatus(queryResponse);

        QueryResponse response = new QueryResponse();

        response.setQuestionGroups(createQuestionGroups(queryResponse));
        response.setImages(createImages(queryResponse));
        
        return response;
    }

    private Image[] createImages(com.rsa.csd.ws.QueryResponse queryResponse) {
        Image[] images = null;
        if (queryResponse != null && 
                queryResponse.getSiteToUserBrowseResponse() != null && 
                    queryResponse.getSiteToUserBrowseResponse().getBrowsableImages() != null &&
                        queryResponse.getSiteToUserBrowseResponse().getBrowsableImages().getImage() != null) {

            com.rsa.csd.ws.Image[] rsaImages = queryResponse.getSiteToUserBrowseResponse().getBrowsableImages().getImage();
            images = new Image[rsaImages.length];
            for (int i = 0; i < images.length; i++) {
                com.rsa.csd.ws.Image rsaImage = rsaImages[i];
                Image image = createImage(rsaImage);
                images[i] = image;
            }
        }
        return images;
    }

    protected Image createImage(com.rsa.csd.ws.Image rsaImage) {
        Image image = new Image(rsaImage.getAltText(), rsaImage.getData(), rsaImage.getHeight(), rsaImage.getImageId(), rsaImage.getPath(), rsaImage.getWidth());
        return image;
    }

    private OpenEndedQuestion[][] createQuestionGroups(com.rsa.csd.ws.QueryResponse queryResponse) {
        OpenEndedQuestion[][] groups = null;
        if (queryResponse != null &&
            queryResponse.getCredentialManagementResponseList() != null &&
            queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse() != null &&
            queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload() != null &&
            queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload().getBrowsableChallQuesGroupList() != null &&
            queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload().getBrowsableChallQuesGroupList().getQuestionGroup() != null) {
        }
        ChallengeQuestionGroup[] rsaGroups = queryResponse.getCredentialManagementResponseList().getChallengeQuestionManagementResponse().getPayload().getBrowsableChallQuesGroupList().getQuestionGroup();
        groups = new OpenEndedQuestion[rsaGroups.length][];
        for (int i = 0; i < rsaGroups.length; i++) {
            ChallengeQuestion[] rsaQuestions = rsaGroups[i].getChallengeQuestion();
            if (rsaQuestions != null) {
                OpenEndedQuestion[] group = new OpenEndedQuestion[rsaQuestions.length];
                for (int j = 0; j < rsaQuestions.length; j++) {
                    group[j] = new OpenEndedQuestion(rsaQuestions[j].getQuestionId(), rsaQuestions[j].getQuestionText(), null); 
                }
                groups[i] = group;
            }
        }
        return groups;
    }

    @Override
    protected RequestType getRequestType() {
        return RequestType.QUERY;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof QueryRequest;
    }


}
