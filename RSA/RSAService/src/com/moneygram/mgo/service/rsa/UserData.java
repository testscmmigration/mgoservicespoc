/**
 * UserData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.mgo.shared.ConsumerName;



/**
 * 
 * User Data.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/10/26 21:41:00 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
/**
 * @author vl58
 *
 */
public class UserData  implements java.io.Serializable {
    private static final long serialVersionUID = 2506934977771370015L;

    /* Unique user identifier. Corresponds to the RSA AA "GenericRequest.IdentificationData.userName". */
    private java.lang.String userId;

    /* Date of user enrollment into the online system. */
    private java.util.Calendar enrollmentDate;

    /* Flag indicating that userId specified is invalid in our system. */
    private java.lang.Boolean invalidUserId;

    /* User name for descriptive purposes. Corresponds to the RSA
     * AA "AnalyzeRequest.UserData.userNameData". */
    private ConsumerName userName;
    
    /**
     * Attribute used for setting country id, in case questions must be retrieved in a different language, i.e: US (United Stages), DE (Germany)
     */
    private java.lang.String userCountry;
    
    /**
     * Attribute used for setting language, in case questions must be retrieved in a different language, i.e: en (English), de (German)
     */
    private java.lang.String userLanguage;

    public UserData() {
    }

    public UserData(
           java.lang.String userId,
           java.util.Calendar enrollmentDate,
           java.lang.Boolean invalidUserId,
           ConsumerName userName,
           String userCountry,
           String userLanguage) {
           this.userId = userId;
           this.enrollmentDate = enrollmentDate;
           this.invalidUserId = invalidUserId;
           this.userName = userName;
           this.userCountry = userCountry;
           this.userLanguage = userLanguage;
    }


    /**
     * Gets the userId value for this UserData.
     * 
     * @return userId   * Unique user identifier. Corresponds to the RSA AA "GenericRequest.IdentificationData.userName".
     */
    public java.lang.String getUserId() {
        return userId;
    }


    /**
     * Sets the userId value for this UserData.
     * 
     * @param userId   * Unique user identifier. Corresponds to the RSA AA "GenericRequest.IdentificationData.userName".
     */
    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }


    /**
     * Gets the enrollmentDate value for this UserData.
     * 
     * @return enrollmentDate   * Date of user enrollment into the online system.
     */
    public java.util.Calendar getEnrollmentDate() {
        return enrollmentDate;
    }


    /**
     * Sets the enrollmentDate value for this UserData.
     * 
     * @param enrollmentDate   * Date of user enrollment into the online system.
     */
    public void setEnrollmentDate(java.util.Calendar enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }


    /**
     * Gets the invalidUserId value for this UserData.
     * 
     * @return invalidUserId   * Flag indicating that userId specified is invalid in our system.
     */
    public java.lang.Boolean getInvalidUserId() {
        return invalidUserId;
    }


    /**
     * Sets the invalidUserId value for this UserData.
     * 
     * @param invalidUserId   * Flag indicating that userId specified is invalid in our system.
     */
    public void setInvalidUserId(java.lang.Boolean invalidUserId) {
        this.invalidUserId = invalidUserId;
    }


    /**
     * Gets the userName value for this UserData.
     * 
     * @return userName   * User name for descriptive purposes. Corresponds to the RSA
     * AA "AnalyzeRequest.UserData.userNameData".
     */
    public ConsumerName getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this UserData.
     * 
     * @param userName   * User name for descriptive purposes. Corresponds to the RSA
     * AA "AnalyzeRequest.UserData.userNameData".
     */
    public void setUserName(ConsumerName userName) {
        this.userName = userName;
    }

    /**
	 * @return the userCountry
	 */
	public java.lang.String getUserCountry() {
		return userCountry;
	}

	/**
	 * @param userCountry the userCountry to set
	 */
	public void setUserCountry(java.lang.String userCountry) {
		this.userCountry = userCountry;
	}

	/**
	 * @return the userLanguage
	 */
	public java.lang.String getUserLanguage() {
		return userLanguage;
	}

	/**
	 * @param userLanguage the userLanguage to set
	 */
	public void setUserLanguage(java.lang.String userLanguage) {
		this.userLanguage = userLanguage;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserData)) return false;
        UserData other = (UserData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userId==null && other.getUserId()==null) || 
             (this.userId!=null &&
              this.userId.equals(other.getUserId()))) &&
            ((this.enrollmentDate==null && other.getEnrollmentDate()==null) || 
             (this.enrollmentDate!=null &&
              this.enrollmentDate.equals(other.getEnrollmentDate()))) &&
            ((this.invalidUserId==null && other.getInvalidUserId()==null) || 
             (this.invalidUserId!=null &&
              this.invalidUserId.equals(other.getInvalidUserId()))) &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) && 
            ((this.userCountry==null && other.getUserCountry()==null) || 
             (this.userCountry!=null &&
              this.userCountry.equals(other.getUserCountry()))) && 
            ((this.userLanguage==null && other.getUserLanguage()==null) || 
             (this.userLanguage!=null &&
              this.userLanguage.equals(other.getUserLanguage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserId() != null) {
            _hashCode += getUserId().hashCode();
        }
        if (getEnrollmentDate() != null) {
            _hashCode += getEnrollmentDate().hashCode();
        }
        if (getInvalidUserId() != null) {
            _hashCode += getInvalidUserId().hashCode();
        }
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getUserCountry() != null) {
            _hashCode += getUserCountry().hashCode();
        }
        if (getUserLanguage() != null) {
            _hashCode += getUserLanguage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    /**
     * Returns true if UserId is Invalid.
     * @return
     */
    public boolean isUserIdInvalid() {
        return getInvalidUserId() != null && getInvalidUserId().booleanValue();
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" UserId=").append(getUserId());
        buffer.append(" EnrollmentDate=").append(getEnrollmentDate());
        buffer.append(" InvalidUserId=").append(getInvalidUserId());
        buffer.append(" UserName=").append(getUserName());
        buffer.append(" UserCountry=").append(getUserCountry());
        buffer.append(" UserLanguage=").append(getUserLanguage());
        return buffer.toString();
    }
}
