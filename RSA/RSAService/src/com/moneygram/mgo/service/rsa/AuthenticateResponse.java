/**
 * AuthenticateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;


/**
 * 
 * Authenticate Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AuthenticateResponse  extends BaseRSAResponse {
    private static final long serialVersionUID = 2825226750279339329L;

    private ChallengeResult challengeResult;

    public AuthenticateResponse() {
    }

    /**
     * Gets the challengeResult value for this AuthenticateResponse.
     * 
     * @return challengeResult
     */
    public ChallengeResult getChallengeResult() {
        return challengeResult;
    }


    /**
     * Sets the challengeResult value for this AuthenticateResponse.
     * 
     * @param challengeResult
     */
    public void setChallengeResult(ChallengeResult challengeResult) {
        this.challengeResult = challengeResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthenticateResponse)) return false;
        AuthenticateResponse other = (AuthenticateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.challengeResult==null && other.getChallengeResult()==null) || 
             (this.challengeResult!=null &&
              this.challengeResult.equals(other.getChallengeResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getChallengeResult() != null) {
            _hashCode += getChallengeResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString() +" ChallengeResult="+ getChallengeResult();
    }
}
