/**
 * QueryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Query Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class QueryRequest  extends BaseRSARequest {
    private static final long serialVersionUID = 3572479539017170435L;

    private BrowseQuestionsRequest browseQuestionsRequest;

    private java.lang.Boolean browseImages;

    public QueryRequest() {
    }

    public QueryRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData,
           BrowseQuestionsRequest browseQuestionsRequest,
           java.lang.Boolean browseImages) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
        this.browseQuestionsRequest = browseQuestionsRequest;
        this.browseImages = browseImages;
    }


    /**
     * Gets the browseQuestionsRequest value for this QueryRequest.
     * 
     * @return browseQuestionsRequest
     */
    public BrowseQuestionsRequest getBrowseQuestionsRequest() {
        return browseQuestionsRequest;
    }


    /**
     * Sets the browseQuestionsRequest value for this QueryRequest.
     * 
     * @param browseQuestionsRequest
     */
    public void setBrowseQuestionsRequest(BrowseQuestionsRequest browseQuestionsRequest) {
        this.browseQuestionsRequest = browseQuestionsRequest;
    }


    /**
     * Gets the browseImages value for this QueryRequest.
     * 
     * @return browseImages
     */
    public java.lang.Boolean getBrowseImages() {
        return browseImages;
    }


    /**
     * Sets the browseImages value for this QueryRequest.
     * 
     * @param browseImages
     */
    public void setBrowseImages(java.lang.Boolean browseImages) {
        this.browseImages = browseImages;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryRequest)) return false;
        QueryRequest other = (QueryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.browseQuestionsRequest==null && other.getBrowseQuestionsRequest()==null) || 
             (this.browseQuestionsRequest!=null &&
              this.browseQuestionsRequest.equals(other.getBrowseQuestionsRequest()))) &&
            ((this.browseImages==null && other.getBrowseImages()==null) || 
             (this.browseImages!=null &&
              this.browseImages.equals(other.getBrowseImages())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getBrowseQuestionsRequest() != null) {
            _hashCode += getBrowseQuestionsRequest().hashCode();
        }
        if (getBrowseImages() != null) {
            _hashCode += getBrowseImages().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" BrowseQuestionsRequest=").append(getBrowseQuestionsRequest());
        buffer.append(" BrowseImages=").append(getBrowseImages());
        return buffer.toString();
    }
}
