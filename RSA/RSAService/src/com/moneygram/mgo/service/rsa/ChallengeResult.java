/**
 * ChallengeResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Challenge Result.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ChallengeResult  implements java.io.Serializable {
    private static final long serialVersionUID = 1780669569867620521L;

    private java.lang.Integer failCount;

    private java.lang.Integer matchCount;

    private java.lang.Boolean successful;

    private java.lang.Boolean block;

    public ChallengeResult() {
    }

    public ChallengeResult(
           java.lang.Integer failCount,
           java.lang.Integer matchCount,
           java.lang.Boolean successful,
           java.lang.Boolean block) {
           this.failCount = failCount;
           this.matchCount = matchCount;
           this.successful = successful;
           this.block = block;
    }


    /**
     * Gets the failCount value for this ChallengeResult.
     * 
     * @return failCount
     */
    public java.lang.Integer getFailCount() {
        return failCount;
    }


    /**
     * Sets the failCount value for this ChallengeResult.
     * 
     * @param failCount
     */
    public void setFailCount(java.lang.Integer failCount) {
        this.failCount = failCount;
    }


    /**
     * Gets the matchCount value for this ChallengeResult.
     * 
     * @return matchCount
     */
    public java.lang.Integer getMatchCount() {
        return matchCount;
    }


    /**
     * Sets the matchCount value for this ChallengeResult.
     * 
     * @param matchCount
     */
    public void setMatchCount(java.lang.Integer matchCount) {
        this.matchCount = matchCount;
    }


    /**
     * Gets the successful value for this ChallengeResult.
     * 
     * @return successful
     */
    public java.lang.Boolean getSuccessful() {
        return successful;
    }


    /**
     * Sets the successful value for this ChallengeResult.
     * 
     * @param successful
     */
    public void setSuccessful(java.lang.Boolean successful) {
        this.successful = successful;
    }


    /**
     * Gets the block value for this ChallengeResult.
     * 
     * @return block
     */
    public java.lang.Boolean getBlock() {
        return block;
    }


    /**
     * Sets the block value for this ChallengeResult.
     * 
     * @param block
     */
    public void setBlock(java.lang.Boolean block) {
        this.block = block;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChallengeResult)) return false;
        ChallengeResult other = (ChallengeResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.failCount==null && other.getFailCount()==null) || 
             (this.failCount!=null &&
              this.failCount.equals(other.getFailCount()))) &&
            ((this.matchCount==null && other.getMatchCount()==null) || 
             (this.matchCount!=null &&
              this.matchCount.equals(other.getMatchCount()))) &&
            ((this.successful==null && other.getSuccessful()==null) || 
             (this.successful!=null &&
              this.successful.equals(other.getSuccessful()))) &&
            ((this.block==null && other.getBlock()==null) || 
             (this.block!=null &&
              this.block.equals(other.getBlock())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFailCount() != null) {
            _hashCode += getFailCount().hashCode();
        }
        if (getMatchCount() != null) {
            _hashCode += getMatchCount().hashCode();
        }
        if (getSuccessful() != null) {
            _hashCode += getSuccessful().hashCode();
        }
        if (getBlock() != null) {
            _hashCode += getBlock().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" FailCount=").append(getFailCount());
        buffer.append(" MatchCount=").append(getMatchCount());
        buffer.append(" Successful=").append(getSuccessful());
        buffer.append(" Block=").append(getBlock());
        return buffer.toString();
    }
}
