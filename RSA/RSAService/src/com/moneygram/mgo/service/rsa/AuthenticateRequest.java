/**
 * AuthenticateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Authenticate Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/09/01 21:09:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AuthenticateRequest  extends BaseRSARequest {
    private static final long serialVersionUID = 4564029341183379317L;

    private TextAnswer[] answers;

    public AuthenticateRequest() {
    }

    public AuthenticateRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData,
           TextAnswer[] answers) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
        this.answers = answers;
    }


    /**
     * Gets the answers value for this AuthenticateRequest.
     * 
     * @return answers
     */
    public TextAnswer[] getAnswers() {
        return answers;
    }


    /**
     * Sets the answers value for this AuthenticateRequest.
     * 
     * @param answers
     */
    public void setAnswers(TextAnswer[] answers) {
        this.answers = answers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AuthenticateRequest)) return false;
        AuthenticateRequest other = (AuthenticateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answers==null && other.getAnswers()==null) || 
             (this.answers!=null &&
              java.util.Arrays.equals(this.answers, other.getAnswers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAnswers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAnswers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString() +" Answers="+ getAnswers();
    }
}
