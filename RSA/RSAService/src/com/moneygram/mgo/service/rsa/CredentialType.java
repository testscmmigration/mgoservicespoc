/**
 * CredentialType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;


/**
 * 
 * Credential Type.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2010/09/07 16:44:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class CredentialType implements java.io.Serializable {
    private static final long serialVersionUID = -525550572953866959L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CredentialType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _QUESTION = "QUESTION";
    public static final java.lang.String _OOBPHONE = "OOBPHONE";
    public static final java.lang.String _OOBEMAIL = "OOBEMAIL";
    public static final java.lang.String _GOID = "GOID";
    public static final java.lang.String _KBA = "KBA";
    public static final java.lang.String _CLIENT_DEFINED = "CLIENT_DEFINED";
    public static final java.lang.String _OTP_EMAIL = "OTP_EMAIL";
    public static final CredentialType QUESTION = new CredentialType(_QUESTION);
    public static final CredentialType OOBPHONE = new CredentialType(_OOBPHONE);
    public static final CredentialType OOBEMAIL = new CredentialType(_OOBEMAIL);
    public static final CredentialType GOID = new CredentialType(_GOID);
    public static final CredentialType KBA = new CredentialType(_KBA);
    public static final CredentialType CLIENT_DEFINED = new CredentialType(_CLIENT_DEFINED);
    public static final CredentialType OTP_EMAIL = new CredentialType(_OTP_EMAIL);

    public java.lang.String getValue() { return _value_;}
    public static CredentialType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CredentialType enumeration = (CredentialType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CredentialType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
