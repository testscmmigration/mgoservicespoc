/**
 * TextAnswer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Text Answer.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/31 21:49:09 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TextAnswer  extends Answer  {
    private static final long serialVersionUID = -9068235995593014423L;

    private java.lang.String answerText;

    public TextAnswer() {
    }

    public TextAnswer(
           String questionId,
           java.lang.String answerText) {
        super(
            questionId);
        this.answerText = answerText;
    }


    /**
     * Gets the answerText value for this TextAnswer.
     * 
     * @return answerText
     */
    public java.lang.String getAnswerText() {
        return answerText;
    }


    /**
     * Sets the answerText value for this TextAnswer.
     * 
     * @param answerText
     */
    public void setAnswerText(java.lang.String answerText) {
        this.answerText = answerText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TextAnswer)) return false;
        TextAnswer other = (TextAnswer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answerText==null && other.getAnswerText()==null) || 
             (this.answerText!=null &&
              this.answerText.equals(other.getAnswerText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswerText() != null) {
            _hashCode += getAnswerText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        return super.toString() +" AnswerText="+ getAnswerText();
    }
}
