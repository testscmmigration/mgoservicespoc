package com.moneygram.mgo.service.rsa;

/**
 * 
 * Funding Method.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2010/09/07 16:44:25 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class FundingMethod implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected FundingMethod(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CREDIT_CARD = "CREDIT_CARD";
    public static final java.lang.String _CHECKING = "CHECKING";
    public static final java.lang.String _SAVINGS = "SAVINGS";
    public static final FundingMethod CREDIT_CARD = new FundingMethod(_CREDIT_CARD);
    public static final FundingMethod CHECKING = new FundingMethod(_CHECKING);
    public static final FundingMethod SAVINGS = new FundingMethod(_SAVINGS);
    
    public java.lang.String getValue() { return _value_;}
    public static FundingMethod fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        FundingMethod enumeration = (FundingMethod)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static FundingMethod fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
