/**
 * SiteToUserData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

/**
 * 
 * Site To User Data.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SiteToUserData implements java.io.Serializable {
    private static final long serialVersionUID = -4087634286709785808L;

    private java.lang.String phrase;

    private Image image;

    public SiteToUserData() {
    }

    public SiteToUserData(
           java.lang.String phrase,
           Image image) {
           this.phrase = phrase;
           this.image = image;
    }


    /**
     * Gets the phrase value for this SiteToUserData.
     * 
     * @return phrase
     */
    public java.lang.String getPhrase() {
        return phrase;
    }


    /**
     * Sets the phrase value for this SiteToUserData.
     * 
     * @param phrase
     */
    public void setPhrase(java.lang.String phrase) {
        this.phrase = phrase;
    }


    /**
     * Gets the image value for this SiteToUserData.
     * 
     * @return image
     */
    public Image getImage() {
        return image;
    }


    /**
     * Sets the image value for this SiteToUserData.
     * 
     * @param image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SiteToUserData)) return false;
        SiteToUserData other = (SiteToUserData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.phrase==null && other.getPhrase()==null) || 
             (this.phrase!=null &&
              this.phrase.equals(other.getPhrase()))) &&
            ((this.image==null && other.getImage()==null) || 
             (this.image!=null &&
              this.image.equals(other.getImage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPhrase() != null) {
            _hashCode += getPhrase().hashCode();
        }
        if (getImage() != null) {
            _hashCode += getImage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Phrase=").append(getPhrase());
        buffer.append(" Image=").append(getImage());
        return buffer.toString();
    }

}
