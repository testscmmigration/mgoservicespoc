/*
 * Created on Sep 1, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.ChallengeRequest;
import com.moneygram.mgo.service.rsa.ChallengeResponse;
import com.moneygram.mgo.service.rsa.OpenEndedQuestion;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.ChallengeQuestion;
import com.rsa.csd.ws.ChallengeQuestionChallengeRequest;
import com.rsa.csd.ws.ChallengeQuestionChallengeRequestPayload;
import com.rsa.csd.ws.CredentialChallengeRequestList;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.RequestType;

/**
 * 
 * Challenge Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/10/02 20:38:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ChallengeCommand extends BaseRSACommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(ChallengeCommand.class);

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest baseRSARequest) throws CommandException {
        ChallengeRequest commandRequest = (ChallengeRequest) baseRSARequest;
        
        if (commandRequest.getNumberOfQuestion() < 1) {
            throw new DataFormatException("Number of questions should be more than 0");
        }
        
        com.rsa.csd.ws.ChallengeRequest request = new com.rsa.csd.ws.ChallengeRequest();
        setupGenericRequest(request, commandRequest);

        ChallengeQuestionChallengeRequestPayload payload = new ChallengeQuestionChallengeRequestPayload();
        payload.setNumberOfQuestion(commandRequest.getNumberOfQuestion());

        ChallengeQuestionChallengeRequest challengeQuestionChallengeRequest = new ChallengeQuestionChallengeRequest();
        challengeQuestionChallengeRequest.setPayload(payload);

        CredentialChallengeRequestList credentialChallengeRequestList = new CredentialChallengeRequestList();
        credentialChallengeRequestList.setChallengeQuestionChallengeRequest(challengeQuestionChallengeRequest);

        request.setCredentialChallengeRequestList(credentialChallengeRequestList);
        
        return request;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.ChallengeResponse challengeResponse = null;
        try {
            com.rsa.csd.ws.ChallengeRequest challengeRequest = (com.rsa.csd.ws.ChallengeRequest)genericRequest;
            challengeResponse = client.challenge(challengeRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA challenge user request", e);
        }

        ChallengeResponse response = createChallengeResponse(challengeResponse);

        return response;
    }

    private ChallengeResponse createChallengeResponse(com.rsa.csd.ws.ChallengeResponse challengeResponse) throws CommandException {
        //check generic rsa response
        checkBaseRSAResponseStatus(challengeResponse);

        ChallengeResponse response = new ChallengeResponse();
        response.setQuestions(createQuestions(challengeResponse));
        return response;
    }

    private OpenEndedQuestion[] createQuestions(com.rsa.csd.ws.ChallengeResponse challengeResponse) {
        OpenEndedQuestion[] questions = null;
        if (challengeResponse != null && 
                challengeResponse.getCredentialChallengeList() != null &&
                challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge() != null &&
                challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge().getPayload() != null &&
                challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge().getPayload().getChallengeQuestions() != null &&
                challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge().getPayload().getChallengeQuestions().getChallengeQuestions() != null &&
                challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge().getPayload().getChallengeQuestions().getChallengeQuestions().length > 0
                ) {

            ChallengeQuestion[] challengeQuestions = challengeResponse.getCredentialChallengeList().getChallengeQuestionChallenge().getPayload().getChallengeQuestions().getChallengeQuestions();
            questions = new OpenEndedQuestion[challengeQuestions.length]; 
            
            for (int i = 0; i < challengeQuestions.length; i++) {
                OpenEndedQuestion question = new OpenEndedQuestion(
                        challengeQuestions[i].getQuestionId(), challengeQuestions[i].getQuestionText(), null);
                questions[i] = question;
            }
        }

        return questions;
    }

    @Override
    protected RequestType getRequestType() {
        return RequestType.CHALLENGE;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof ChallengeRequest;
    }
}
