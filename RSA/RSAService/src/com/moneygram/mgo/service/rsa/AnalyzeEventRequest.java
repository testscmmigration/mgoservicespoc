/**
 * AnalyzeEventRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Analyze Event Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2010/06/23 15:45:37 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AnalyzeEventRequest  extends NotifyRequest {
    private static final long serialVersionUID = 520729269510973976L;

    public AnalyzeEventRequest() {
    }

    public AnalyzeEventRequest(
            RequestHeader header,
            java.lang.String applicationCode,
            java.lang.String transactionId,
            Device device,
            UserData userData,
            Event[] events,
            java.lang.Boolean startMonitoring) {
         super(
             header,
             applicationCode,
             transactionId,
             device,
             userData,
             events,
             startMonitoring);
     }

}
