/**
 * UpdateUserRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Update User Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/10/02 20:39:40 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateUserRequest  extends BaseRSARequest  {
    private static final long serialVersionUID = -2472400116674699702L;

    private SaveAnswersRequest saveAnswersRequest;

    private SiteToUserData saveImageRequest;

    public UpdateUserRequest() {
    }

    public UpdateUserRequest(
           RequestHeader header,
           java.lang.String applicationCode,
           java.lang.String transactionId,
           Device device,
           UserData userData,
           SaveAnswersRequest saveAnswersRequest,
           SiteToUserData saveImageRequest) {
        super(
            header,
            applicationCode,
            transactionId,
            device,
            userData);
        this.saveAnswersRequest = saveAnswersRequest;
        this.saveImageRequest = saveImageRequest;
    }


    /**
     * Gets the saveAnswersRequest value for this UpdateUserRequest.
     * 
     * @return saveAnswersRequest
     */
    public SaveAnswersRequest getSaveAnswersRequest() {
        return saveAnswersRequest;
    }


    /**
     * Sets the saveAnswersRequest value for this UpdateUserRequest.
     * 
     * @param saveAnswersRequest
     */
    public void setSaveAnswersRequest(SaveAnswersRequest saveAnswersRequest) {
        this.saveAnswersRequest = saveAnswersRequest;
    }


    /**
     * Gets the saveImageRequest value for this UpdateUserRequest.
     * 
     * @return saveImageRequest
     */
    public SiteToUserData getSaveImageRequest() {
        return saveImageRequest;
    }


    /**
     * Sets the saveImageRequest value for this UpdateUserRequest.
     * 
     * @param saveImageRequest
     */
    public void setSaveImageRequest(SiteToUserData saveImageRequest) {
        this.saveImageRequest = saveImageRequest;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateUserRequest)) return false;
        UpdateUserRequest other = (UpdateUserRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.saveAnswersRequest==null && other.getSaveAnswersRequest()==null) || 
             (this.saveAnswersRequest!=null &&
              this.saveAnswersRequest.equals(other.getSaveAnswersRequest()))) &&
            ((this.saveImageRequest==null && other.getSaveImageRequest()==null) || 
             (this.saveImageRequest!=null &&
              this.saveImageRequest.equals(other.getSaveImageRequest())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSaveAnswersRequest() != null) {
            _hashCode += getSaveAnswersRequest().hashCode();
        }
        if (getSaveImageRequest() != null) {
            _hashCode += getSaveImageRequest().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" SaveAnswersRequest=").append(getSaveAnswersRequest());
        buffer.append(" SaveImageRequest=").append(getSaveImageRequest());
        return buffer.toString();
    }
}
