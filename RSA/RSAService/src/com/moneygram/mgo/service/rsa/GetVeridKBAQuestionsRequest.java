/**
 * GetVeridKBAQuestionsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.rsa;

import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.RequestHeader;

/**
 * 
 * Get Verid KBA Questions Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/04 16:22:38 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class GetVeridKBAQuestionsRequest  extends BaseOperationRequest {
    private static final long serialVersionUID = 4050237321976972147L;

    private long eventSequenceNumber;

    private Person person;

    private UserSystemAuthenticationCredentials userSystemCredentials;

    public GetVeridKBAQuestionsRequest() {
    }

    public GetVeridKBAQuestionsRequest(
           RequestHeader header,
           long eventSequenceNumber,
           Person person,
           UserSystemAuthenticationCredentials userSystemCredentials) {
        super(
            header);
        this.eventSequenceNumber = eventSequenceNumber;
        this.person = person;
        this.userSystemCredentials = userSystemCredentials;
    }


    /**
     * Gets the eventSequenceNumber value for this GetVeridKBAQuestionsRequest.
     * 
     * @return eventSequenceNumber
     */
    public long getEventSequenceNumber() {
        return eventSequenceNumber;
    }


    /**
     * Sets the eventSequenceNumber value for this GetVeridKBAQuestionsRequest.
     * 
     * @param eventSequenceNumber
     */
    public void setEventSequenceNumber(long eventSequenceNumber) {
        this.eventSequenceNumber = eventSequenceNumber;
    }


    /**
     * Gets the person value for this GetVeridKBAQuestionsRequest.
     * 
     * @return person
     */
    public Person getPerson() {
        return person;
    }


    /**
     * Sets the person value for this GetVeridKBAQuestionsRequest.
     * 
     * @param person
     */
    public void setPerson(Person person) {
        this.person = person;
    }


    /**
     * Gets the userSystemCredentials value for this GetVeridKBAQuestionsRequest.
     * 
     * @return userSystemCredentials
     */
    public UserSystemAuthenticationCredentials getUserSystemCredentials() {
        return userSystemCredentials;
    }


    /**
     * Sets the userSystemCredentials value for this GetVeridKBAQuestionsRequest.
     * 
     * @param userSystemCredentials
     */
    public void setUserSystemCredentials(UserSystemAuthenticationCredentials userSystemCredentials) {
        this.userSystemCredentials = userSystemCredentials;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetVeridKBAQuestionsRequest)) return false;
        GetVeridKBAQuestionsRequest other = (GetVeridKBAQuestionsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.eventSequenceNumber == other.getEventSequenceNumber() &&
            ((this.person==null && other.getPerson()==null) || 
             (this.person!=null &&
              this.person.equals(other.getPerson()))) &&
            ((this.userSystemCredentials==null && other.getUserSystemCredentials()==null) || 
             (this.userSystemCredentials!=null &&
              this.userSystemCredentials.equals(other.getUserSystemCredentials())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getEventSequenceNumber()).hashCode();
        if (getPerson() != null) {
            _hashCode += getPerson().hashCode();
        }
        if (getUserSystemCredentials() != null) {
            _hashCode += getUserSystemCredentials().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" EventSequenceNumber=").append(getEventSequenceNumber());
        buffer.append(" Person=").append(getPerson());
        buffer.append(" UserSystemCredentials=").append(getUserSystemCredentials());
        return buffer.toString();
    }
}
