/*
 * Created on Sep 22, 2009
 *
 */
package com.moneygram.mgo.service.rsa.command;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.mgo.service.rsa.BaseRSARequest;
import com.moneygram.mgo.service.rsa.Event;
import com.moneygram.mgo.service.rsa.MGITransaction;
import com.moneygram.mgo.service.rsa.NotifyRequest;
import com.moneygram.mgo.service.rsa.NotifyResponse;
import com.moneygram.mgo.service.rsa.UserData;
import com.moneygram.mgo.shared.ConsumerName;
import com.moneygram.mgo.shared.ReceiverConsumerName;
import com.moneygram.rsa.client.RSAClient;
import com.rsa.csd.ws.AccountData;
import com.rsa.csd.ws.AccountType;
import com.rsa.csd.ws.Amount;
import com.rsa.csd.ws.ClientDefinedFact;
import com.rsa.csd.ws.DataType;
import com.rsa.csd.ws.EventData;
import com.rsa.csd.ws.EventDataList;
import com.rsa.csd.ws.EventType;
import com.rsa.csd.ws.ExecutionSpeed;
import com.rsa.csd.ws.FactList;
import com.rsa.csd.ws.GenericActionType;
import com.rsa.csd.ws.GenericRequest;
import com.rsa.csd.ws.OtherAccountOwnershipType;
import com.rsa.csd.ws.OtherAccountType;
import com.rsa.csd.ws.RequestType;
import com.rsa.csd.ws.Schedule;
import com.rsa.csd.ws.TransactionData;
import com.rsa.csd.ws.TransferMediumType;

/**
 * 
 * Notify Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.8 $ $Date: 2010/09/10 21:21:05 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class NotifyCommand extends BaseRSACommand {
    private static final GenericActionType[] actions = new GenericActionType[]{GenericActionType.START_NAP_MONITORING};
    private static final int RSA_ACCOUNT_LENGTH = 45;

    @Override
    protected GenericRequest createRSARequest(BaseRSARequest commandRequest) throws CommandException {
        com.rsa.csd.ws.NotifyRequest request = new com.rsa.csd.ws.NotifyRequest();
        setupGenericRequest(request, commandRequest);
        
        request.setEventDataList(createEventDataList(commandRequest));

        NotifyRequest notifyRequest = (NotifyRequest)commandRequest;
        if (notifyRequest.getStartMonitoring() != null && notifyRequest.getStartMonitoring().booleanValue()) {
            setGenericActionTypes(request, actions);
        }

        return request;
    }

    protected EventDataList createEventDataList(BaseRSARequest commandRequest) {
        EventDataList eventDataList = null;
        
        NotifyRequest notifyRequest = (NotifyRequest)commandRequest;

        Event[] events = notifyRequest.getEvents();
        if (events != null && events.length > 0) {
            EventData[] eventData = new EventData[events.length];
            for (int i = 0; i < events.length; i++) {
                Event event = events[i];
                
                EventData rsaEvent = new EventData();
                if (event.getEventType() != null) {
                    rsaEvent.setEventType(EventType.fromString(event.getEventType().getValue()));
                }
                rsaEvent.setClientDefinedEventType(event.getClientEventId());
                rsaEvent.setEventDescription(event.getEventDescription());
                if (event.getPayment() != null) {
                    rsaEvent.setTransactionData(createTransactionData(event.getPayment(), notifyRequest.getUserData()));
                    rsaEvent.setClientDefinedAttributeList(createClientDefinedAttributeList(event.getPayment()));
                }
                eventData[i] = rsaEvent;
            }
            eventDataList = new EventDataList(eventData);
        }
        
        return eventDataList;
    }

    private FactList createClientDefinedAttributeList(MGITransaction payment) {
        
        ArrayList<ClientDefinedFact> facts = new ArrayList<ClientDefinedFact>(); 
        
        if (payment.getDeliveryOption() != null) {
            facts.add(new ClientDefinedFact("delivery_option", payment.getDeliveryOption(), DataType.STRING));
        }

        if (payment.getSubmissionDate() != null) {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            String date = format.format(payment.getSubmissionDate().getTime()); 
            facts.add(new ClientDefinedFact("transaction_date", date, DataType.DATE));
        }

        if (payment.getReceiveState() != null) {
            facts.add(new ClientDefinedFact("intended_receiving_state", payment.getReceiveState(), DataType.STRING));
        }

        FactList factList = new FactList();

        factList.setFact(facts.toArray(new ClientDefinedFact[facts.size()]));

        return factList;
    }

    private TransactionData createTransactionData(MGITransaction payment, UserData userData) {
        TransactionData transaction = new TransactionData();
        
        AccountData myAccountData = new AccountData();
        myAccountData.setAccountType(AccountType.fromString(payment.getFundingMethod().getValue()));
        //these two are not really applicable for MGI
        //myAccountData.setAccountOwnershipType(AccountOwnershipType.INDIVIDUAL);
        //myAccountData.setAccountRelationType(AccountRelationType.PRIMARY_OWNER);
        myAccountData.setAccountName(createAccountName(userData.getUserName()));
        myAccountData.setAccountNumber(payment.getSendAccountNumber() != null ? payment.getSendAccountNumber() : createDefaultAccountNumber(myAccountData.getAccountName()));
        myAccountData.setAccountCountry(payment.getSendCountry());

        AccountData accountData = new AccountData();
        accountData.setAccountName(createOtherAccountName(payment));
        accountData.setAccountNumber(payment.getReceiveAccountNumber() != null ? payment.getReceiveAccountNumber() : createDefaultAccountNumber(accountData.getAccountName()));
        accountData.setAccountCountry(payment.getReceiveCountry());

        Amount amount = new Amount();
        //The documentation stated "amount Amount. 1-18 (with last two digits to the right of the decimal place point)".
        //This mean that the amount is in "cents" (i.e. for $230.45 we should send the integer value of 23045)
        amount.setAmount((long)(payment.getSendAmount().doubleValue() * 100));
        amount.setCurrency(payment.getSendCurrency());

        TransferMediumType transferMediumType = null;
        if (payment.isBillPay()) {
            transferMediumType = TransferMediumType.BILLPAY_ELEC;
        } else {
            transferMediumType = (payment.isInternationTransfer() ? TransferMediumType.INTL_WIRE : TransferMediumType.WIRE);
        }

        transaction.setMyAccountData(myAccountData);
        transaction.setOtherAccountData(accountData);
        transaction.setAmount(amount);
        if (payment.getExecutionSpeed() != null) {
            transaction.setExecutionSpeed(ExecutionSpeed.fromString(payment.getExecutionSpeed().getValue()));
        }
        transaction.setSchedule(Schedule.IMMEDIATE);
        transaction.setOtherAccountType(payment.isBillPay() ? OtherAccountType.BILLER : OtherAccountType.PERSONAL_ACCOUNT);
        if (payment.isBillPay()) {
            transaction.setOtherAccountOwnershipType(OtherAccountOwnershipType.ME_TO_YOU);
        }
        transaction.setTransferMediumType(transferMediumType);
        
        //TODO: set payment.getTransactionId()
        

        return transaction;
    }

    private String createOtherAccountName(MGITransaction payment) {
        return (payment.isBillPay() ? payment.getBillerName().toLowerCase() : createReceiverAccountName(payment.getReceiverName())); 
    }

    private String createDefaultAccountNumber(String name) {
        if(name != null && name.length() > RSA_ACCOUNT_LENGTH) {
            int length = name.length();
            name = name.substring(length - RSA_ACCOUNT_LENGTH, length);
        }
        return name;
    }

    private String createReceiverAccountName(ReceiverConsumerName receiverName) {
        StringBuffer name = new StringBuffer(createAccountName(receiverName));
        if (receiverName.getMaternalName() != null) {
            name.append(" ").append(receiverName.getMaternalName());
        }
        return name.toString().toLowerCase();
    }
    
    private String createAccountName(ConsumerName receiverName) {
        StringBuffer name = new StringBuffer();
        if (receiverName.getFirstName() != null) {
            name.append(receiverName.getFirstName());
        }
        if (receiverName.getMiddleName() != null) {
            name.append(" ").append(receiverName.getMiddleName());
        }
        if (receiverName.getLastName() != null) {
            name.append(" ").append(receiverName.getLastName());
        }
        return name.toString().toLowerCase();
    }

    @Override
    protected RequestType getRequestType() {
      return RequestType.ANALYZE;
    }

    @Override
    protected OperationResponse processRSARequest(RSAClient client, GenericRequest genericRequest, BaseRSARequest baseRSARequest)
            throws CommandException {
        com.rsa.csd.ws.NotifyResponse notifyResponse = null;
        try {
            com.rsa.csd.ws.NotifyRequest notifyRequest = (com.rsa.csd.ws.NotifyRequest)genericRequest;
            notifyResponse = client.notify(notifyRequest);
        } catch (Exception e) {
            throw new CommandException("Failed to process RSA notify event request", e);
        }

        NotifyResponse response = createNotifyResponse(notifyResponse);

        return response;
    }

    private NotifyResponse createNotifyResponse(com.rsa.csd.ws.NotifyResponse notifyResponse) {
        //check generic rsa response
        //checkRSAResponseStatus(response);
        NotifyResponse response = new NotifyResponse();
        return response;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof NotifyRequest;
    }

}
