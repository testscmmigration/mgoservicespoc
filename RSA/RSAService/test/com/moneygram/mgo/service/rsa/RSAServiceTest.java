/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.mgo.service.rsa;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.UUID;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;
import com.moneygram.mgo.shared.ConsumerName;
import com.moneygram.mgo.shared.ReceiverConsumerName;

/**
 * 
 * RSA Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.33 $ $Date: 2010/09/13 18:44:16 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RSAServiceTest extends BaseRSAServiceTestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(RSAServiceTest.class);
    
    private static String USER_ID = "rsaservicetest"+ String.valueOf(System.currentTimeMillis()) +"@mgi.com";
    
    protected Device generateDeviceData() {
        Device deviceRequest = new Device();
        deviceRequest.setDevicePrint("version=1&amp;pm_fpua=mozilla/4.0 (compatible; msie 7.0; windows nt 5.1; .net clr 1.1.4322; .net clr 2.0.50727; infopath.1)|4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)|Win32|0|x86|en-us|5730&amp;pm_fpsc=32|1280|1024|964|1280&amp;pm_fpsw=abk=6,0,2900,2180|wnt=6,0,2900,2180|dht=7,0,5730,11|dhj=6,0,1,223|dan=6,0,3,531|dsh=10,0,0,3646|ie5=7,0,5730,11|icw=5,0,2918,1900|ieh=7,0,5730,11|iee=6,0,5730,11|wmp=10,0,0,3646|obp=7,0,5730,11|oex=6,0,2900,2180|net=4,4,0,3400|tks=4,71,1968,1|mvm=5,0,3810,0&amp;pm_fptz=-5&amp;pm_fpln=lang=en-us|syslang=en-us|userlang=en-us&amp;pm_fpjv=6.0&amp;pm_fpco=1");
        deviceRequest.setDeviceTokenCookie("user_cookie");
        deviceRequest.setDeviceTokenFSO("flash_cookie");
        deviceRequest.setHttpAccept("image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-powerpoint, application/vnd.ms-excel, application/msword, application/x-shockwave-flash, */*");
        deviceRequest.setHttpAcceptEncoding("gzip, deflate");
        deviceRequest.setHttpAcceptLanguage("en-us");
        deviceRequest.setHttpReferrer("moneygram.com");
        deviceRequest.setIpAddress("127.0.0.1");
        deviceRequest.setUserAgent("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");

        return deviceRequest;
    }

    public void testAnalyzeEvent() throws ServiceException {
//        setRSASoapMonitorEndpoint();
        submitAnalyzeRequest();
    }

    public void testAnalyzeMTTransactionEvent() throws ServiceException {
        MGITransaction transaction = generateMTTransaction();
        Event[] events = new Event[] { new Event(MGITransactionType.MoneyTransfer.getValue(), EventType.PAYMENT, null, transaction ) };

        submitAnalyzeRequest(events);
    }

    public void testAnalyzeBPTransactionEvent() throws ServiceException {
        MGITransaction transaction = generateBPTransaction();
        Event[] events = new Event[] { new Event(MGITransactionType.BillPay.getValue(), EventType.PAYMENT, null, transaction ) };

        submitAnalyzeRequest(events);
    }

    private MGITransaction generateBPTransaction() {
        MGITransaction transaction = new MGITransaction();

        Calendar date = Calendar.getInstance();

        int random = (int)(Math.random() * 10000);
        
        transaction.setTransactionId(String.valueOf(random));
        transaction.setBillerName("My favorite biller with really long name that exceeds 45 in length");
        transaction.setExecutionSpeed(ExecutionSpeed.FEW_HOURS);
        transaction.setFundingMethod(FundingMethod.CREDIT_CARD);
        transaction.setReceiveCountry("USA");
        transaction.setSendAmount(new BigDecimal(567.89));
        transaction.setSendCountry("USA");
        transaction.setSendCurrency("USD");
        transaction.setSubmissionDate(date);
        transaction.setTransactionType(MGITransactionType.BillPay);

        return transaction;
    }

    private MGITransaction generateMTTransaction() {
        MGITransaction transaction = new MGITransaction();

        ReceiverConsumerName receiver = new ReceiverConsumerName();
        receiver.setFirstName("Roger");
        receiver.setMiddleName("R");
        receiver.setLastName("Federer");
        receiver.setMaternalName("Mirka");
        
        Calendar date = Calendar.getInstance();
        
        int random = (int)(Math.random() * 10000);
        
        transaction.setTransactionId(String.valueOf(random));
        transaction.setReceiverName(receiver);
        transaction.setDeliveryOption("WILL_CALL");
        transaction.setExecutionSpeed(ExecutionSpeed.SEVERAL_DAYS);
        transaction.setFundingMethod(FundingMethod.CHECKING);
        transaction.setReceiveCountry("USA");
        transaction.setReceiveCurrency("USD");
        transaction.setReceiveState("MN");
        transaction.setSendAmount(new BigDecimal(123.45));
        transaction.setSendCountry("USA");
        transaction.setSendCurrency("USD");
        transaction.setSubmissionDate(date);
        transaction.setTransactionType(MGITransactionType.MoneyTransfer);

        return transaction;
    }

    public void testAnalyzeIdentity() throws ServiceException {
      submitAnalyzeIdentityRequest();
    }

    public void testNotifyRequest() throws CommandException {
        Event[] events = new Event[] {
                new Event(null, EventType.FAILED_LOGIN_ATTEMPT, null), 
                new Event("PASSWORD_CHANGE", EventType.CHANGE_PASSWORD, null),
                new Event(null, EventType.FAILED_CHANGE_PASSWORD_ATTEMPT, null), 
                new Event(null, EventType.CHANGE_ADDRESS, "phone"), 
                new Event(null, EventType.CHANGE_LIFE_QUESTIONS, null) 
                };
        
        String transactionId = null;
        Boolean startMonitoring = null;

        submitNotifyRequest(events, transactionId, startMonitoring);
    }

    public void testNotifyRequestNAPFailure() throws CommandException {
        AnalyzeIdentityResponse response = submitAnalyzeIdentityRequest();

        Event[] events = new Event[] {new Event(null, EventType.FAILED_OPEN_NEW_ACCOUNT, null)};

        Boolean startMonitoring = Boolean.FALSE;
        
        submitNotifyRequest(events, response.getTransactionId(), startMonitoring);
    }

    public void testNotifyRequestNAPSuccess() throws CommandException {
        AnalyzeIdentityResponse response = submitAnalyzeIdentityRequest();

        Event[] events = new Event[] {new Event(null, EventType.SUCCESSFUL_OPEN_NEW_ACCOUNT, null)};

        Boolean startMonitoring = Boolean.TRUE;
        
        submitNotifyRequest(events, response.getTransactionId(), startMonitoring);
    }

    private void submitNotifyRequest(Event[] events, String transactionId, Boolean startMonitoring) throws CommandException {
        NotifyRequest request = new NotifyRequest();
        setupBaseRequest(request, ServiceAction.notify);

        //populate device with data from the http request 
        Device device = generateDeviceData(); 
        request.setDevice(device );
        request.setEvents(events);
        request.setTransactionId(transactionId);
        request.setStartMonitoring(startMonitoring);
        
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("submitNotifyRequest: response=" + response);
        }
        
        assertTrue("Expected NotifyResponse", response instanceof NotifyResponse);
        NotifyResponse rsaResponse = (NotifyResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
    }

    private AnalyzeEventResponse submitAnalyzeRequest() throws CommandException {
        Event[] events = new Event[] { 
                new Event(null, EventType.SESSION_SIGNIN, null),
                new Event(null, EventType.CHECK_DEVICE, null) 
                };
        return submitAnalyzeRequest(events);
    }

    private AnalyzeEventResponse submitAnalyzeRequest(Event[] events) throws CommandException {
        AnalyzeEventRequest request = new AnalyzeEventRequest();
        setupBaseRequest(request, ServiceAction.analyzeEvent);

        //populate device with data from the http request 
        Device device = generateDeviceData(); 
        request.setDevice(device );
        request.setEvents(events);

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testAnalyzeEvent: response=" + response);
        }
        
        assertTrue("Expected AnalyzeEventResponse", response instanceof AnalyzeEventResponse);
        AnalyzeEventResponse rsaResponse = (AnalyzeEventResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertNotNull("Expected not null TransactionId", rsaResponse.getTransactionId());
        
        assertNotNull("Expected not null RiskResult", rsaResponse.getRiskResult());
        assertNotNull("Expected not null RiskScore", rsaResponse.getRiskResult().getRiskScore());
        return rsaResponse;
    }

    private AnalyzeIdentityResponse submitAnalyzeIdentityRequest() throws CommandException {
        AnalyzeIdentityRequest request = new AnalyzeIdentityRequest();
        setupBaseRequest(request, ServiceAction.analyzeIdentity);

        //populate device with data from the http request 
        Device device = generateDeviceData(); 
        request.setDevice(device );
        request.setEvents(new Event[] { new Event(null, EventType.OPEN_NEW_ACCOUNT, null) });

        Person person = generatePerson();
        
        request.setPerson(person);
        
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testAnalyzeIdentityEvent: response=" + response);
        }
        
        assertTrue("Expected AnalyzeIdentityResponse", response instanceof AnalyzeIdentityResponse);
        AnalyzeIdentityResponse rsaResponse = (AnalyzeIdentityResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertNotNull("Expected not null TransactionId", rsaResponse.getTransactionId());
        
        assertNotNull("Expected not null RiskResult", rsaResponse.getRiskResult());
        assertNotNull("Expected not null RiskScore", rsaResponse.getRiskResult().getRiskScore());
        return rsaResponse;
    }

    public void testQuery() throws ServiceException {
        testQuery(false);
    }

    public void testQueryWithImages() throws ServiceException {
        testQuery(true);
    }

    private void testQuery(boolean browseImages) throws ServiceException {
//        setRSASoapMonitorEndpoint();
        Integer groupCount = new Integer(3);
        Integer questionsCount = new Integer(6);

        OperationResponse response = getQuestions(groupCount, questionsCount, browseImages);
        if (logger.isDebugEnabled()) {
            logger.debug("testQuery: response=" + response);
        }

        assertTrue("Expected QueryResponse", response instanceof QueryResponse);
        QueryResponse rsaResponse = (QueryResponse) response;
        
        assertNotNull("Expected not null QuestionGroups", rsaResponse.getQuestionGroups());
        assertEquals("Expected certain number of questions returned", groupCount.intValue(), rsaResponse.getQuestionGroups().length);

        OpenEndedQuestion[][] groups = rsaResponse.getQuestionGroups();
        for (int i = 0; i < groups.length; i++) {
            OpenEndedQuestion[] questions = groups[i];
            for (int j = 0; j < questions.length; j++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("testQuery: group="+ i +" question["+j+"]: id="+ questions[j].getQuestionId()+" text="+ questions[j].getQuestionText());
                }
            }
        }
        
        if (browseImages) {
            assertNotNull("Expected not null images", rsaResponse.getImages());
            assertTrue("Expected not empty images", rsaResponse.getImages().length > 0);
            Image[] images = rsaResponse.getImages();
            for (int i = 0; i < images.length; i++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("testQuery: image["+i+"]="+ images[i]);
                }
            }
        } else {
            assertNull("Expected null images", rsaResponse.getImages());
        }
    }

    private GetUserImageResponse getUserImage() throws ServiceException {
        GetUserImageRequest request = new GetUserImageRequest();
        setupBaseRequest(request, ServiceAction.getUserImage);

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetUserImage: response="+ response);
        }
        
        assertTrue("Expected GetUserImageResponse", response instanceof GetUserImageResponse);
        
        GetUserImageResponse commandResponse = (GetUserImageResponse)response;
        
        assertNotNull("Expected not null UserStatus", commandResponse.getUserStatus());
        assertEquals("Expected certain UserStatus", UserStatus.VERIFIED, commandResponse.getUserStatus());

        assertNotNull("Expected not null SiteToUserData", commandResponse.getSiteToUserData());
        assertNotNull("Expected not null Image", commandResponse.getSiteToUserData().getImage());
        assertNotNull("Expected not null Phrase", commandResponse.getSiteToUserData().getPhrase());

        return commandResponse;
    }

    public void testGetInvalidUserImage() throws ServiceException {
        GetUserImageRequest request = new GetUserImageRequest();
        setupBaseRequest(request, ServiceAction.getUserImage);
        
        //set invalid user
        request.getUserData().setUserId("invalid-user-"+ System.currentTimeMillis());
        request.getUserData().setInvalidUserId(Boolean.TRUE);
        
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetInvalidUserImage: response="+ response);
        }
        
        assertTrue("Expected GetUserImageResponse", response instanceof GetUserImageResponse);
        
        GetUserImageResponse commandResponse = (GetUserImageResponse)response;
        
        assertNotNull("Expected not null UserStatus", commandResponse.getUserStatus());
        assertEquals("Expected certain UserStatus", UserStatus.VERIFIED, commandResponse.getUserStatus());

        assertNotNull("Expected not null SiteToUserData", commandResponse.getSiteToUserData());
        assertNotNull("Expected not null Image", commandResponse.getSiteToUserData().getImage());
        assertNotNull("Expected not null Phrase", commandResponse.getSiteToUserData().getPhrase());

    }

    private OperationResponse getQuestions(Integer groupCount, Integer questionsCount, boolean browseImages) throws ServiceException {

        QueryRequest request = new QueryRequest(); 
        setupBaseRequest(request, ServiceAction.query);

        //query specific request
        request.setBrowseQuestionsRequest(new BrowseQuestionsRequest(groupCount, questionsCount));
        request.setBrowseImages(Boolean.valueOf(browseImages));
        
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        return response;
    }

    public void testUpdateUser() throws ServiceException {

        UpdateUserRequest request = new UpdateUserRequest(); 
        setupBaseRequest(request, ServiceAction.updateUser);

        //updateUser specific request
        setupUpdateUserRequest(request);
        
//        setRSASoapMonitorEndpoint();
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testUpdateUser: response=" + response);
        }
        
        assertTrue("Expected UpdateUserResponse", response instanceof UpdateUserResponse);
        UpdateUserResponse rsaResponse = (UpdateUserResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        
        //check the updated image
        GetUserImageResponse userImageResponse = getUserImage();
        assertEquals("Expected user image id to match", request.getSaveImageRequest().getImage().getImageId(), userImageResponse.getSiteToUserData().getImage().getImageId());
        assertEquals("Expected user image phrase to match", request.getSaveImageRequest().getPhrase(), userImageResponse.getSiteToUserData().getPhrase());
    }

    public void testChallenge() throws ServiceException {
        submitChallengeRequest();
    }

    public void testAuthenticate() throws ServiceException {
        ChallengeResponse challengeResponse = submitChallengeRequest();
        OpenEndedQuestion[] questions = challengeResponse.getQuestions();
        TextAnswer[] answers = new TextAnswer[questions.length];
        for (int j = 0; j < questions.length; j++) {
            answers[j] = new TextAnswer(questions[j].getQuestionId(), generateAnswer(questions[j].getQuestionText()));
            if (logger.isDebugEnabled()) {
                logger.debug("testAuthenticate: answers["+ j + "]="+ answers[j]);
            }
        }
        
//        setRSASoapMonitorEndpoint();
 
        AuthenticateRequest request = new AuthenticateRequest();
        setupBaseRequest(request, ServiceAction.authenticate);

        request.setAnswers(answers);
        request.setTransactionId(challengeResponse.getTransactionId());

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testAuthenticate: response=" + response);
        }
        
        assertTrue("Expected AuthenticateResponse", response instanceof AuthenticateResponse);
        AuthenticateResponse rsaResponse = (AuthenticateResponse) response;
        assertNotNull("Expected not null ChallengeResult", rsaResponse.getChallengeResult());
        assertTrue("Expected successful challenge result",  rsaResponse.getChallengeResult().getSuccessful());
    }

    private ChallengeResponse submitChallengeRequest() throws CommandException {
        AnalyzeEventResponse analyzeResponse = submitAnalyzeRequest();

//        setRSASoapMonitorEndpoint();

        ChallengeRequest request = new ChallengeRequest(); 
        setupBaseRequest(request, ServiceAction.challenge);

        //challenge specific request

        int expectedNumberOfQuestions = 2;
        request.setNumberOfQuestion(expectedNumberOfQuestions);
        request.setTransactionId(analyzeResponse.getTransactionId());
        
        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);

        if (logger.isDebugEnabled()) {
            logger.debug("testChallenge: response=" + response);
        }

        assertTrue("Expected ChallengeResponse", response instanceof ChallengeResponse);
        ChallengeResponse rsaResponse = (ChallengeResponse) response;
        rsaResponse.setTransactionId(request.getTransactionId()); //only analyze returns transaction id

        assertNotNull("Expected not null Questions", rsaResponse.getQuestions());
        assertEquals("Expected certain number of questions returned", expectedNumberOfQuestions, rsaResponse.getQuestions().length);

        OpenEndedQuestion[] questions = rsaResponse.getQuestions();
        for (int j = 0; j < questions.length; j++) {
            if (logger.isDebugEnabled()) {
                logger.debug("testChallenge: question["+j+"]: id="+ questions[j].getQuestionId()+" text="+ questions[j].getQuestionText());
            }
        }

        return rsaResponse;
    }
    
    private void setupUpdateUserRequest(UpdateUserRequest request) throws ServiceException {
        Integer groupCount = new Integer(3);
        Integer questionsCount = new Integer(6);
        
        boolean browseImages = true;

        OperationResponse response = getQuestions(groupCount, questionsCount, browseImages);

        assertTrue("Expected QueryResponse", response instanceof QueryResponse);
        QueryResponse rsaResponse = (QueryResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertNotNull("Expected not null QuestionGroups", rsaResponse.getQuestionGroups());

        //take 1st question from every group
        
        OpenEndedQuestion[][] groups = rsaResponse.getQuestionGroups();
        TextAnswer[] answers = new TextAnswer[groups.length];
        for (int i = 0; i < groups.length; i++) {
            OpenEndedQuestion[] questions = groups[i];
            answers[i] = new TextAnswer(questions[0].getQuestionId(), generateAnswer(questions[0].getQuestionText()));
            if (logger.isDebugEnabled()) {
                logger.debug("generateSaveAnswersRequest: answers["+ i +"]="+ answers[i]);
            }
        }
        
        request.setSaveAnswersRequest(new SaveAnswersRequest(answers));
        
        assertNotNull("Expected not null images", rsaResponse.getImages());
        assertTrue("Expected not empty images", rsaResponse.getImages().length > 0);

        //select 1st image
        Image[] images = rsaResponse.getImages();
        SiteToUserData siteToUserData = new SiteToUserData("Test phrase for image id="+ images[0].getImageId(), images[0]);

        request.setSaveImageRequest(siteToUserData);
        
        if (logger.isDebugEnabled()) {
            logger.debug("setupUpdateUserRequest: request="+ request);
        }

    }

    private String generateAnswer(String question) {
        return "my answer for "+ (question == null ? "" : question.substring(0, Math.min(question.length(), 25)));
    }
    
    private void setupBaseRequest(BaseRSARequest request, ServiceAction action) {
        String requestId = UUID.randomUUID().toString();
        String sessionId = String.valueOf(System.currentTimeMillis()); //http session id

        ProcessingInstruction processingInstruction = 
            new ProcessingInstruction(action.getValue());
        processingInstruction.setReadOnlyFlag(Boolean.TRUE);

        ConsumerName name = new ConsumerName("first", "last", "middle");

        UserData userData = new UserData(USER_ID, Calendar.getInstance(), null, name,"US","en");

        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader(requestId, sessionId)));
        request.setApplicationCode(RSA_MGO_APP_CODE);
        request.setUserData(userData);
    }

}
