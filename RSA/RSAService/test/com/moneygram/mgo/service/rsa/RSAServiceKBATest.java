/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.mgo.service.rsa;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;

/**
 * 
 * RSA Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2010/06/22 21:53:27 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RSAServiceKBATest extends BaseRSAServiceTestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(RSAServiceKBATest.class);

    public void testGetVeridKBAQuestions() throws ServiceException {

        GetVeridKBAQuestionsRequest request = generateGetVeridKBAQuestionsRequest();

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetVeridKBAQuestions: response=" + response);
        }

        assertTrue("Expected GetVeridKBAQuestionsResponse", response instanceof GetVeridKBAQuestionsResponse);
        GetVeridKBAQuestionsResponse rsaResponse = (GetVeridKBAQuestionsResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertTrue("Expected questions response", ResultType.questions.equals(rsaResponse.getResultType()));
        assertNotNull("Expected not null questions", rsaResponse.getQuestions());
        assertNotNull("Expected not empty questions", rsaResponse.getQuestions().getItem());
        assertTrue("Expected not empty questions", rsaResponse.getQuestions().getItem().length > 0);

        MultipleChoiceQuestion[] questions = rsaResponse.getQuestions().getItem();
        for (int i = 0; i < questions.length; i++) {
            MultipleChoiceQuestion question = questions[i];
            if (logger.isDebugEnabled()) {
                logger.debug("testGetVeridKBAQuestions: index="+i +" question="+ question);
            }
            assertNotNull("Expected question id for index="+ i, question.getQuestionId());
            assertNotNull("Expected answer type for index="+ i, question.getAnswerType());
            assertTrue("Expected single answer type for index="+ i, AnswerType.single.equals(question.getAnswerType()));
            assertNotNull("Expected question text for index="+ i, question.getQuestionText());
            assertNotNull("Expected question help text for index="+ i, question.getQuestionHelpText());
            MultipleChoicePossibleAnswer[] choices = question.getChoices();
            assertNotNull("Expected choices for question at index="+ i, question.getQuestionHelpText());
            
            for (int j = 0; j < choices.length; j++) {
                MultipleChoicePossibleAnswer choice = choices[j];
                if (logger.isDebugEnabled()) {
                    logger.debug("testGetVeridKBAQuestions: index="+ j +" choice="+ choice);
                }
                assertNotNull("Expected choice id for choice at index="+ j +" for question at index="+ i, choice.getChoiceId());
                assertNotNull("Expected choice text for choice at index="+ j +" for question at index="+ i, choice.getChoiceText());
            }
        }
    }

    public void testGetVeridKBAQuestionsError() throws ServiceException {

        GetVeridKBAQuestionsRequest request = generateGetVeridKBAQuestionsRequest();
        
        Person person = request.getPerson();

        //force errors
        person.setSsn(null);
        person.setDateOfBirth(Calendar.getInstance());

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetVeridKBAQuestions: response=" + response);
        }

        assertTrue("Expected GetVeridKBAQuestionsResponse", response instanceof GetVeridKBAQuestionsResponse);
        GetVeridKBAQuestionsResponse rsaResponse = (GetVeridKBAQuestionsResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertTrue("Expected error response", ResultType.error.equals(rsaResponse.getResultType()));
        assertNotNull("Expected not null AdditionalInformation", rsaResponse.getAdditionalInformation());
    }

    public void testGetVeridKBAQuestionsSimulatedError() throws ServiceException {

        GetVeridKBAQuestionsRequest request = generateGetVeridKBAQuestionsRequest();
        request.getHeader().getProcessingInstruction().setSimulatedModeAction("invalid_information");

        RequestRouter requestRouter = getRequestRouter();

        OperationResponse response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testGetVeridKBAQuestionsSimulatedError: response=" + response);
        }

        assertTrue("Expected GetVeridKBAQuestionsResponse", response instanceof GetVeridKBAQuestionsResponse);
        GetVeridKBAQuestionsResponse rsaResponse = (GetVeridKBAQuestionsResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertTrue("Expected error response", ResultType.error.equals(rsaResponse.getResultType()));
        assertNotNull("Expected not null AdditionalInformation", rsaResponse.getAdditionalInformation());
    }

    public void testSubmitVeridKBAAnswers() throws ServiceException {

        //get questions
        GetVeridKBAQuestionsRequest questionsRequest = generateGetVeridKBAQuestionsRequest();
        RequestRouter requestRouter = getRequestRouter();
        OperationResponse response = requestRouter.process(questionsRequest);

        assertTrue("Expected GetVeridKBAQuestionsResponse", response instanceof GetVeridKBAQuestionsResponse);
        GetVeridKBAQuestionsResponse questionsResponse = (GetVeridKBAQuestionsResponse) response;
        assertNotNull("Expected not null response", questionsResponse);
        assertTrue("Expected questions response", ResultType.questions.equals(questionsResponse.getResultType()));
        assertNotNull("Expected not null questions", questionsResponse.getQuestions());
        assertNotNull("Expected not empty questions", questionsResponse.getQuestions().getItem());
        assertTrue("Expected not empty questions", questionsResponse.getQuestions().getItem().length > 0);

        //submit answers
        
        ProcessingInstruction processingInstruction = new ProcessingInstruction(
                ServiceAction.submitVeridKBAAnswers.getValue());
        processingInstruction.setSimulatedModeAction("passed");
//        processingInstruction.setSimulatedModeAction("failed_iauth_questions");
        SubmitVeridKBAAnswersRequest request = new SubmitVeridKBAAnswersRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        request.setVendorAuthenticationEventId(questionsResponse.getVendorAuthenticationEventId());
        request.setEventSequenceNumber(questionsRequest.getEventSequenceNumber());
        request.setQuestionsSetId(questionsResponse.getQuestions().getQuestionsSetId());
        request.setAnswers(generateAnswers(questionsResponse.getQuestions().getItem()));

        response = requestRouter.process(request);
        if (logger.isDebugEnabled()) {
            logger.debug("testSubmitVeridKBAAnswers: response=" + response);
        }
        
        assertTrue("Expected SubmitVeridKBAAnswersResponse", response instanceof SubmitVeridKBAAnswersResponse);
        SubmitVeridKBAAnswersResponse rsaResponse = (SubmitVeridKBAAnswersResponse) response;
        assertNotNull("Expected not null response", rsaResponse);
        assertTrue("Expected passed response", ResultType.passed.equals(rsaResponse.getResultType()));
    }

    private MultipleChoiceAnswer[] generateAnswers(MultipleChoiceQuestion[] questions) {
        MultipleChoiceAnswer[] answers = new MultipleChoiceAnswer[questions.length];
        for (int i = 0; i < questions.length; i++) {
            MultipleChoiceQuestion question = questions[i];
            MultipleChoiceAnswer answer = new MultipleChoiceAnswer();
            answer.setQuestionId(question.getQuestionId());
            answer.setAnswerId(question.getChoices()[0].getChoiceId());
            answers[i] = answer;
        }
        return answers;
    }

    private UserSystemAuthenticationCredentials generateUserSystemAuthenticationCredentials() {
        UserSystemAuthenticationCredentials credentials = new UserSystemAuthenticationCredentials();
        credentials.setIpAddress("127.0.0.1");
        return credentials;
    }

    private GetVeridKBAQuestionsRequest generateGetVeridKBAQuestionsRequest() {
        ProcessingInstruction processingInstruction = new ProcessingInstruction(
                ServiceAction.getVeridKBAQuestions.getValue());
        
        processingInstruction.setSimulatedModeAction("questions");
//      processingInstruction.setSimulatedModeAction("invalid_information");

        GetVeridKBAQuestionsRequest request = new GetVeridKBAQuestionsRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        request.setPerson(generatePerson());
        request.setUserSystemCredentials(generateUserSystemAuthenticationCredentials());

        return request;
    }
    
}
