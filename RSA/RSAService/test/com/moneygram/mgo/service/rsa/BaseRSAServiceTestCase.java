/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.mgo.service.rsa;

import java.security.Security;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.util.ResourceConfig;
import com.moneygram.common.service.util.TestResourceConfigFactory;
import com.moneygram.mgo.service.rsa.util.RSAResourceConfig;
import com.moneygram.mgo.shared.ConsumerAddress;
import com.moneygram.mgo.shared.ConsumerName;
import com.moneygram.service.BaseServiceTest;

/**
 * 
 * Base RSA Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.6 $ $Date: 2010/09/13 19:01:30 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseRSAServiceTestCase extends BaseServiceTest {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseRSAServiceTestCase.class);
    
    private static final String RSA_INSTANCE_ID = "emng";
    //private static final String RSA_INSTANCE_ID = "erbp";
    protected static final String RSA_MGO_APP_CODE = "MGO";
    //protected static final String RSA_MGO_APP_CODE = "MGO_NAP";
    private static final String RSA_MGO_APP_CODE_PREFIX = RSA_MGO_APP_CODE + "_";
    private static final String RSA_BASE_URL = "https://rbauat3.cyota.com/";
//    private static final String RSA_BASE_URL = "http://localhost:8081/";
    private static final String RSA_SERVICE_URL = "AdaptiveAuthentication_6_5/AdaptiveAuthentication";

    private static final Map attributes = new HashMap();

    
    static {
        attributes.put(RSAResourceConfig.VERID_MODE_TYPE, "simulated");
        attributes.put(RSAResourceConfig.VERID_RULE_SET, "customers.moneygram.iauth.callcenter");
        attributes.put(RSAResourceConfig.VERID_ACCOUNT_NAME, "Customers:MoneyGram");
        attributes.put(RSAResourceConfig.VERID_VERIFICATION_TYPE, "iauth");
        attributes.put(RSAResourceConfig.VERID_USER_NAME, "MGramPayINTEG");
        attributes.put(RSAResourceConfig.VERID_PASSWORD, "zbW49@y5T1#df");
        
        attributes.put(RSAResourceConfig.RSA_INSTANCE_ID, RSA_INSTANCE_ID);
        attributes.put(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_ENDPOINT, RSA_BASE_URL + RSA_SERVICE_URL);
        attributes.put(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_TIMEOUT, "60000");//this for NAP calls, should be 15000 otherwise
        attributes.put(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_CALLER_ID, "MGOSOAP");
        attributes.put(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_CALLER_CREDENTIAL, "MONEYGRAM09!");
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        //to avoid
        //Cannot find the specified class java.security.PrivilegedActionException: java.lang.ClassNotFoundException: com.ibm.websphere.ssl.protocol.SSLSocketFactory
        Security.setProperty("ssl.SocketFactory.provider", "com.ibm.jsse2.SSLSocketFactoryImpl");
        Security.setProperty("ssl.ServerSocketFactory.provider", "com.ibm.jsse2.SSLServerSocketFactoryImpl");
        ResourceConfig.setResourceConfigFactory(new TestResourceConfigFactory(attributes));
    }

    protected void setConfigurationAttribute(String name, String value) {
        attributes.put(name, value);
    }

    protected void setRSAEndpoint(String value) {
        setConfigurationAttribute(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_ENDPOINT, value);
    }

    protected void setRSASoapMonitorEndpoint() {
        String property = "rsa.service.baseurl";
        String base = System.getProperty(property);
        if (base == null) {
            throw new IllegalArgumentException("Property "+ property +" is undefined");
        }
        
        String url = base + RSA_SERVICE_URL;
        
        if (logger.isDebugEnabled()) {
            logger.debug("setRSASoapMonitorEndpoint: using url="+ url);
        }
        setConfigurationAttribute(RSA_MGO_APP_CODE_PREFIX + RSAResourceConfig.RSA_ENDPOINT, url);
    }

    protected Person generatePerson() {
        ConsumerAddress address = new ConsumerAddress();
        address.setLine1("43175 Third Street");
        address.setLine2("Unit # 4");
        address.setCity("Restong");
        address.setState("VA");
        address.setZipCode("20176");
        address.setCountry("USA");
        
        ConsumerName name = new ConsumerName();
        name.setFirstName("Robert");
        name.setMiddleName("R");
        name.setLastName("Smith");

        Person person = new Person();
        person.setAddress(address);
        person.setName(name);
        person.setPhone("7035551212");
        
        Calendar dob = Calendar.getInstance();
        dob.add(Calendar.YEAR, -20); //Invalid DOB: Must be at least 18 years old in 'person' tag.
        
        person.setDateOfBirth(dob);
        person.setSsn("8532");
        
        return person;
    }
    

}
