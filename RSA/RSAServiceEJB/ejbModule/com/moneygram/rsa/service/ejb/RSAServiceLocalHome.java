package com.moneygram.rsa.service.ejb;

/**
 * Local Home interface for Enterprise Bean: RSAService
 */
public interface RSAServiceLocalHome extends javax.ejb.EJBLocalHome {

    /**
     * Creates a default instance of Session Bean: RSAService
     */
    public com.moneygram.rsa.service.ejb.RSAServiceLocal create() throws javax.ejb.CreateException;
}
