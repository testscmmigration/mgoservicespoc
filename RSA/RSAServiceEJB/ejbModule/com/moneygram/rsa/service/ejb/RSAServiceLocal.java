package com.moneygram.rsa.service.ejb;

import com.moneygram.common.service.ServiceProcessor;

/**
 * Local interface for Enterprise Bean: RSAService
 */
public interface RSAServiceLocal extends javax.ejb.EJBLocalObject, ServiceProcessor {
}
