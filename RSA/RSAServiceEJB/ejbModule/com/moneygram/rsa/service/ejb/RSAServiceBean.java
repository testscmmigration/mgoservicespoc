package com.moneygram.rsa.service.ejb;

import org.springframework.context.ApplicationContext;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.common.service.ServiceException;
import com.moneygram.common.service.util.ApplicationContextFactory;

/**
 * Bean implementation class for Enterprise Bean: RSAService
 */
public class RSAServiceBean implements javax.ejb.SessionBean {
    private static final Logger logger = LogFactory.getInstance().getLogger(RSAServiceBean.class);
    
    static final long serialVersionUID = 3206093459760846163L;
    private javax.ejb.SessionContext mySessionCtx;
    /**
     * getSessionContext
     */
    public javax.ejb.SessionContext getSessionContext() {
        return mySessionCtx;
    }
    /**
     * setSessionContext
     */
    public void setSessionContext(javax.ejb.SessionContext ctx) {
        mySessionCtx = ctx;
    }
    /**
     * ejbCreate
     */
    public void ejbCreate() throws javax.ejb.CreateException {
    }
    /**
     * ejbActivate
     */
    public void ejbActivate() {
    }
    /**
     * ejbPassivate
     */
    public void ejbPassivate() {
    }
    /**
     * ejbRemove
     */
    public void ejbRemove() {
    }

    /**
     * Process the request within transaction context.
     * @param request
     * @return
     * @throws ServiceException
     */
    public OperationResponse process(OperationRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: processing transactional request="+ request);
        }
        return processRequest(request);
    }
    
    /**
     * Process the request without transaction context.
     * @param request
     * @return
     * @throws ServiceException
     */
    public OperationResponse processRead(OperationRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("processRead: processing read request="+ request);
        }

        return processRequest(request);
    }

    /**
     * Process the request.
     * @param request
     * @return
     * @throws CommandException
     */
    private OperationResponse processRequest(OperationRequest request) throws ServiceException {
        ApplicationContext context = ApplicationContextFactory.getApplicationContext(); 
        RequestRouter requestRouter = (RequestRouter)context.getBean(RequestRouter.REQUEST_ROUTER);
        
        OperationResponse response = requestRouter.process(request);

        return response;
    }
}
