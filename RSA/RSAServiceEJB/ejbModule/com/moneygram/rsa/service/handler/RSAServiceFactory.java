/*
 * Created on Jun 19, 2009
 *
 */
package com.moneygram.rsa.service.handler;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.ServiceException;
import com.moneygram.common.service.ServiceProcessor;
import com.moneygram.common.service.web.ServiceFactory;
import com.moneygram.rsa.service.ejb.RSAServiceLocal;
import com.moneygram.rsa.service.ejb.RSAServiceLocalHome;

/**
 * 
 * MGO Service Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceEJB</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/18 22:10:54 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RSAServiceFactory implements ServiceFactory {

    /**
     * Returns an instance of MGOService.
     * @return instance of MGOService.
     * @throws ServiceException
     */
    public ServiceProcessor createService(String jndi) throws ServiceException {

        RSAServiceLocal service = null;

        try {
            Context ctx = new InitialContext();
            RSAServiceLocalHome home = (RSAServiceLocalHome) ctx.lookup(jndi);
            service = home.create();
        } catch (Exception e) {
            throw new CommandException("Failed to create an instance of the RSAService", e);
        }

        return service;
    }   

}
