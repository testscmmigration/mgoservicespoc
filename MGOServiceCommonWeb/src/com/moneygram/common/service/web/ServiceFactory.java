/*
 * Created on Aug 5, 2009
 *
 */
package com.moneygram.common.service.web;

import com.moneygram.common.service.ServiceProcessor;
import com.moneygram.common.service.ServiceException;

/**
 * 
 * Service Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/10 22:40:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public interface ServiceFactory {
    /**
     * Returns an instance of MGOProcessor.
     * @return instance of MGOProcessor.
     * @throws ServiceException
     */
    public ServiceProcessor createService(String jndi) throws ServiceException;

}
