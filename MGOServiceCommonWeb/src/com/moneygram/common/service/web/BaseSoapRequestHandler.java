package com.moneygram.common.service.web;


import com.moneygram.common.guid.RequestGUID;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.perfmon.PerformanceMonitor;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.ServiceProcessor;
import com.moneygram.common.service.UserErrorException;

/**
 * Base Soap Request Handler.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseSoapRequestHandler {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseSoapRequestHandler.class);

    private ServiceFactory serviceFactory = null;

    protected ServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    protected void setServiceFactory(ServiceFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    /**
     * Processes the soap request. 
     * @param request
     * @return
     * @throws ServiceException
     */
    protected OperationResponse process(OperationRequest request) throws ServiceException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        String apiName = request.getClass().getName();
        RequestGUID requestGUID = RequestGUID.createRequestGUID(apiName, null);
        PerformanceMonitor perfmon = null;

        String requestId = null;
        if (request.getHeader() != null && request.getHeader().getClientHeader() != null) {
            requestId = request.getHeader().getClientHeader().getClientRequestID();
        }

        if (logger.isInfoEnabled()) {
            logger.info("process: request id="+ requestId);
        }

        OperationResponse response = null;
        try {
            perfmon = PerformanceMonitor.getInstance();
            perfmon.startRequest(requestGUID);
            
            ServiceProcessor service = getServiceFactory().createService(getServiceJndiName());
            
            RequestHeader context = request.getHeader();
            response = (context.isReadOnly() ? service.processRead(request) : service.process(request));
        } catch (UserErrorException e) {
            if (logger.isDebugEnabled()) {
                logger.debug("process: Failed to process the request id="+ requestId, e);
            }
            throw createServiceException(e);
        } catch (Exception e) {
            if (logger.isInfoEnabled()) {
                logger.info("process: Failed to process the request id="+ requestId, e);
            }
            throw createServiceException(e);
        } finally {
            if (perfmon != null) {
                perfmon.finishRequest(requestGUID);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("process: response="+ response);
        }
        return response;
    }

    /**
     * Creates ServiceException instance.
     * @param e
     * @return
     */
    private ServiceException createServiceException(Exception e) {
        ServiceException wse = new ServiceException();
        if (e instanceof com.moneygram.common.service.ServiceException) {
            com.moneygram.common.service.ServiceException se = (com.moneygram.common.service.ServiceException)e;
            wse.setErrorCategoryCode(se.getErrorCategoryCode());
            wse.setErrorCode(se.getErrorCode() == null ? String.valueOf(CommandException.MIN_ERROR_CODE) : se.getErrorCode());
            wse.setErrorHandlingCode(se.getErrorHandlingCode());
            wse.setErrorLocation(se.getErrorLocation());
            wse.setErrorSource(se.getErrorSource() == null ? "NA" : se.getErrorSource());
            wse.setErrorStackTrace(se.getErrorStackTrace());
        } else {
            wse.setErrorCategoryCode(CommandException.ERROR_CATEGORY);
            wse.setErrorCode(String.valueOf(CommandException.MIN_ERROR_CODE));
            wse.setErrorSource("NA");
        }
        wse.setErrorMessage(e.getMessage());
        return wse;
    }
    
    /**
     * Returns service jndi name.
     * @return service jndi name.
     */
    protected abstract String getServiceJndiName();
}

