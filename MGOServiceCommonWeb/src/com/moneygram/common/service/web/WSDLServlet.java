/*
 * Created on Jun 10, 2009
 *
 */
package com.moneygram.common.service.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * WSDL Servlet.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceWeb</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/10 22:40:51 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class WSDLServlet extends HttpServlet {
    private static final long serialVersionUID = -7904481139718045322L;

    private static final Logger logger = LogFactory.getInstance().getLogger(WSDLServlet.class);

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Processes the request by routing them. 
     * @param request request
     * @param response response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("processRequest: request=" + request);
        }
        
        //process parameters
        String service = request.getParameter("service");
        String version = request.getParameter("version");
        if (version == null) {
            version="1";
        }

        //get wsdl content
        String responseContent;
        try {
            responseContent = getWSDL(request, service, version);
        } catch (Exception e) {
            throw new ServletException("Failed to retrieve wsdl for service="+ service +" and version="+ version, e);
        }

        //generate response
        response.setContentType("text/xml");
        response.setContentLength(responseContent.length());
        Writer writer = response.getWriter();
        writer.write(responseContent);
        response.flushBuffer();
    }

    /**
     * Returns content of the wsdl file with ${base.url} replaced with the request's specific server url.
     * @param request
     * @param wsdl wsdl file
     * @param version wsdl file version
     * @return content of the wsdl file
     * @throws Exception
     */
    private String getWSDL(HttpServletRequest request, String wsdl, String version) throws Exception {
        String wsdlFileName = wsdl + "_v"+ version +".wsdl";
        String path = getServletContext().getRealPath(wsdlFileName);
        if (logger.isDebugEnabled()) {
            logger.debug("getWSDL: wsdlFileName="+ wsdlFileName +" real path="+ path);
        }
        File wsdlFile = new File(path);
        String content = getContent(wsdlFile);
        content = content.replaceAll("http://base.url", getBaseUrl(request));
        return content;
    }

    /**
     * Returns base server url extracted from request.
     * @param request
     * @return base server url
     * @throws Exception
     */
    private static String getBaseUrl(HttpServletRequest request) throws Exception {
        StringBuffer url = new StringBuffer();
        String scheme = request.getScheme();

        int port = request.getServerPort();
        if (port < 0) {
            port = 80;
        }

        url.append(scheme);
        url.append("://");
        url.append(request.getServerName());

        if (("http".equals(scheme) && (port != 80))
            || ("https".equals(scheme) && (port != 443))) {
            url.append(':');
            url.append(port);
        }

        return url.toString();
    }

    /**
     * Returns file's content.
     * @param aFile
     * @return file's content.
     * @throws Exception
     */
    private static String getContent(File aFile) throws Exception {
        StringBuilder content = new StringBuilder();

        BufferedReader input = new BufferedReader(new FileReader(aFile));
        try {
            String line = null;
            String lineSeparator = System.getProperty("line.separator");
            while ((line = input.readLine()) != null) {
                content.append(line);
                content.append(lineSeparator);
            }
        } finally {
            input.close();
        }

        return content.toString();
    }
}
