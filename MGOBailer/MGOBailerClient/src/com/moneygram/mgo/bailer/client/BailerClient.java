package com.moneygram.mgo.bailer.client;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

public class BailerClient 
{
	private static Logger getLogger() {
		return LogFactory.getInstance().getLogger(BailerClient.class);
	}
    private final static String USAGE =
        "Usage: mgobailer <options>\n"
            + " -u <MGOBailer URL>\n"
            + " -t <timeout value in minutes>\n";
    private final static int SUCCESS = 0;
    private final static int FAILURE = 1; 
    
    public final static void main(String[] args) throws Exception 
    {
    	getLogger().info("Starting BailerClient run");
    	
    	String url = null;
    	int timeout = 0;
    	
    	try {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equalsIgnoreCase("-u")) {
					if (i < args.length) {
						url = args[i + 1];
					} else {
						displayErrorAndExit("-u specified without URL: \n" + USAGE);
					}

				}
				if (args[i].equalsIgnoreCase("-t")) {
					if (i < args.length) {
						timeout = Integer.parseInt(args[i + 1]);
					} else {
						displayErrorAndExit("-t specified without valid numeric: \n" + USAGE);
					}
				}
			}

			if (url == null) {
				displayErrorAndExit("Couldn't find URL; must run with '-u URL' parameter: \n" + USAGE);
			}
		} catch (Exception e) {
			displayErrorAndExit("Exception encountered: " + e + "\n\n" + USAGE);
		}

		if (timeout <= 0 ||
			timeout > 360) {
			timeout = 30; //default to 30 min if not specified or if value is > 6 hours
		}
		timeout = timeout * 60 * 1000;
		
    	HttpParams params = new BasicHttpParams(); 
    	HttpConnectionParams.setConnectionTimeout(params, 60000); //only wait 60 seconds for a connection 
    	HttpConnectionParams.setSoTimeout(params, timeout); 

    	HttpClient httpClient = new DefaultHttpClient(params); 
        HttpGet httpget = new HttpGet(url); 

 
        getLogger().info("executing request " + httpget.getURI());
        //Create a response handler
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        String responseBody = (String) httpClient.execute(httpget, responseHandler);
        System.out.println(responseBody);
        httpClient.getConnectionManager().shutdown();
        //If not successful
        getLogger().info("Response from servlet: " + responseBody);
        if (responseBody.indexOf("SUCCESS") < 0) {
        	displayErrorAndExit("Bailer Run Servlet did not return success: " );
        }
        
        System.exit(SUCCESS); //good run
    }
    
    private static void displayErrorAndExit(String error) {
    	getLogger().error(error);
    	System.exit(FAILURE);
    }
}