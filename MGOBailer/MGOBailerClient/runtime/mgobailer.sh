#!/bin/sh
# required for Unicenter
#cd `dirname $0`
#. ./setenv.sh

. `dirname $0`/setenv.sh

java -Dlog.factory=com.moneygram.common.log.log4j.Log4JFactory \
     -Dlog4j.configuration=../log4j.properties \
     com.moneygram.mgo.bailer.client.BailerClient $*