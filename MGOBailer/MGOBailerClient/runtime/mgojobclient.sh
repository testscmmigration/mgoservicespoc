# required for Unicenter
cd `dirname $0`

. ./setjobenv.sh

for libname in ../lib/*.jar ; do
   CP=$CP:$libname
done

CP=$CP:$WAS_HOME/lib/j2ee.jar:../lib/roguewave.zip:$WAS_HOME/runtimes/com.ibm.ws.admin.client_8.0.0.jar:$WAS_HOME/plugins/com.ibm.ws.webcontainer.jar:$WAS_HOME/plugins/com.ibm.ws.runtime.jar:$WAS_HOME/plugins/com.ibm.ws.emf.jar:..

#echo $CP

$JAVA_HOME/bin/java -cp $CP -Dlog.factory=com.moneygram.common.log.log4j.Log4JFactory -Dlog4j.configuration=../log4j-bailerclient.properties com.moneygram.mgo.bailer.client.MGOJobClient -u $URL -a $ACTION -s $SITE -p $PARAMS -t $MAX_PROCESS_MINUTES
