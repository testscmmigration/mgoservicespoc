package com.moneygram.mgo.bailer.proxies;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;
import com.moneygram.mgo.common_v1.Header;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.consumer_v2.client.GetIncompleteFirstTxProfilesRequest;
import com.moneygram.mgo.service.consumer_v2.client.GetIncompleteFirstTxProfilesResponse;
import com.moneygram.mgo.service.consumer_v2.client.MGOConsumerServicePortType_v2;
import com.moneygram.mgo.service.consumer_v2.client.MGOConsumerServicePortType_v2Proxy;
import com.moneygram.mgo.service.consumer_v2.client.ServiceAction;

public class ConsumerServicev2Proxy
{
        private static final Logger log = LogFactory.getInstance().getLogger(ConsumerServicev2Proxy.class);
        protected MGOConsumerServicePortType_v2 consumerServiceClient;
        
        private MGOConsumerServicePortType_v2 getConsumerServiceClient() throws Exception 
        {
            if(consumerServiceClient != null)
            {
                return consumerServiceClient;
            }
            else
            {
                try 
                {
                    String url = MGOBailerResourceConfig.getInstance().getConsumerServicev2URL();
                    consumerServiceClient = new MGOConsumerServicePortType_v2Proxy(url);
                    return consumerServiceClient;
                } 
                catch (Exception e) 
                {
                    throw new Exception("Failed to create ConsumerServiceClient:" + e.getMessage(),e);
                }
            }
        }
   
   
	public long[] getIncompleteFirstTxProfiles(Calendar startDateTime, Calendar endDateTime, Integer maxResults)
			throws Exception {
		
		GetIncompleteFirstTxProfilesRequest request = new GetIncompleteFirstTxProfilesRequest();

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.getIncompleteFirstTxProfiles_v2.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);
		request.setStartDateTime(startDateTime);
		request.setEndDateTime(endDateTime);

		if (maxResults != null && maxResults.compareTo(new Integer("0")) > 0) {
			request.setMaxResults(maxResults);
		}

		GetIncompleteFirstTxProfilesResponse response = null;
		try {
			response = getConsumerServiceClient().getIncompleteFirstTxProfiles(request);
			if (response == null) {
				throw new Exception ("Empty response from getIncompleteFirstTxProfiles encountered.");
			}
		} catch (Exception e) {
			throw new Exception("Call to getIncompleteFirstTxProfiles failed", e);
		}
		return response.getConsumerId();
	}

}
