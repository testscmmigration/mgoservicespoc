package com.moneygram.mgo.bailer.proxies;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;
import com.moneygram.mgo.common_v1.Header;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.consumer_v1.Consumer;
import com.moneygram.mgo.service.consumer_v1.GetIncompleteProfilesRequest;
import com.moneygram.mgo.service.consumer_v1.GetIncompleteProfilesResponse;
import com.moneygram.mgo.service.consumer_v1.ServiceAction;
import com.moneygram.mgo.service.consumer_v1.client.ConsumerServiceClient;

public class ConsumerServiceProxy
{
        private static final Logger log = LogFactory.getInstance().getLogger(ConsumerServiceProxy.class);
        private ConsumerServiceClient consumerServiceClient;
        
        private ConsumerServiceClient getConsumerServiceClient() throws Exception 
        {
            if(consumerServiceClient != null)
            {
                return consumerServiceClient;
            }
            else
            {
                try 
                {
                    String url = MGOBailerResourceConfig.getInstance().getConsumerServiceURL();
                    int timeOut = MGOBailerResourceConfig.getInstance().getConsumerServiceTimeout();
                    consumerServiceClient = new ConsumerServiceClient(url, timeOut);
                    return consumerServiceClient;
                } 
                catch (Exception e) 
                {
                    throw new Exception("Failed to create ConsumerServiceClient:" + e.getMessage(),e);
                }
            }
        }
   
	public long[] getIncompleteProfiles(Calendar startDateTime, Calendar endDateTime, Integer maxResults)
			throws Exception {
		
		GetIncompleteProfilesRequest request = new GetIncompleteProfilesRequest();

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.getIncompleteProfiles.getValue());
		processingInstruction.setReadOnlyFlag(Boolean.TRUE);

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);
		request.setStartDateTime(startDateTime);
		request.setEndDateTime(endDateTime);

		if (maxResults != null && maxResults.compareTo(new Integer("0")) > 0) {
			request.setMaxResults(maxResults);
		}

		GetIncompleteProfilesResponse response = null;
		try {
			response = getConsumerServiceClient().getIncompleteProfiles(request);
			if (response == null) {
				throw new Exception ("Empty response from getIncompleteProfiles encountered.");
			}
		} catch (Exception e) {
			throw new Exception("Call to getConsumerProfile failed", e);
		}
		return response.getConsumerId();
	}

}
