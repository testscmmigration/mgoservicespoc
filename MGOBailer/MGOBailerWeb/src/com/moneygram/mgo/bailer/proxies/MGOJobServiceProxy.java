package com.moneygram.mgo.bailer.proxies;

import java.util.StringTokenizer;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;
import com.moneygram.mgo.common_v1.Header;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.bailer_v1.ProcessActionRequest;
import com.moneygram.mgo.service.bailer_v1.ProcessActionResponse;
import com.moneygram.mgo.service.bailer_v1.ProcessParamType;
import com.moneygram.mgo.service.bailer_v1.ServiceAction;
import com.moneygram.mgo.service.bailer_v1.client.BailerServiceClient;


public class MGOJobServiceProxy
{
        private static final Logger log = LogFactory.getInstance().getLogger(MGOJobServiceProxy.class);
        private BailerServiceClient bailerServiceClient;
        
        private BailerServiceClient getBailerServiceClient() throws Exception 
        {
            if(bailerServiceClient != null)
            {
                return bailerServiceClient;
            }
            else
            {
                try 
                {
                    String url = MGOBailerResourceConfig.getInstance().getBailerServiceURL();
                    int timeOut = MGOBailerResourceConfig.getInstance().getJobServiceTimeoutMinutes();
                    bailerServiceClient = new BailerServiceClient(url, timeOut*60*1000);
                    return bailerServiceClient;
                } 
                catch (Exception e){
                    throw new Exception("Failed to create BailerServiceClient:" + e.getMessage(),e);
                }
            }
        }
   
	public String processAction(String action, String[] sites,
			String[] strProcessParam) throws Exception {
		ProcessParamType proceesParamType = null;
		ProcessActionRequest processActionRequest = new ProcessActionRequest();
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.processAction
				.getValue());
		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		processActionRequest.setHeader(header);
		processActionRequest.setAction(action);
		processActionRequest.setSites(sites);
		String strprcParam = "";
		int i = 0;
		int lengthParam = strProcessParam.length;
		ProcessParamType[] processParamTypes = new ProcessParamType[lengthParam+1];
		if (strProcessParam != null) {
			for (String str : strProcessParam) {
				strprcParam += str;
				StringTokenizer st = new StringTokenizer(strprcParam, ",");
				while (st.hasMoreElements()) {
					String valueObject = (String) st.nextElement();
					proceesParamType = new ProcessParamType();
					proceesParamType.setParamName(valueObject.substring(0,
							valueObject.indexOf(":")));
					proceesParamType.setParamValue(valueObject
							.substring(valueObject.indexOf(":")));
					processParamTypes[i] = proceesParamType;
					i++;
					proceesParamType = null;
				}
			}
		}
		processActionRequest.setProcessParams(processParamTypes);
		ProcessActionResponse processActionResponse = null;
		try {
			processActionResponse = getBailerServiceClient().processAction(
					processActionRequest);
			if (processActionResponse == null) {
				throw new Exception(
						"Empty response from ProcessAction encountered.");
			}
		} catch (Exception e) {
			throw new Exception("Call to processAction failed", e);
		}
		return processActionResponse.getProcessingText();

	}
}
