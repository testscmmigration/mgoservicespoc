package com.moneygram.mgo.bailer.proxies;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;
import com.moneygram.mgo.common_v1.Header;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortType;
import com.moneygram.mgo.service.bailer_v1.MGOBailerServicePortTypeProxy;
import com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest;
import com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsResponse;
import com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsRequest;
import com.moneygram.mgo.service.bailer_v1.ProcessFirstTxBailerEventsResponse;
import com.moneygram.mgo.service.bailer_v1.ServiceAction;



public class BailerServiceProxy
{
        private static final Logger log = LogFactory.getInstance().getLogger(BailerServiceProxy.class);
        private MGOBailerServicePortType bailerServiceClient;
        
        private MGOBailerServicePortType getBailerServiceClient() throws Exception 
        {
            if(bailerServiceClient != null)
            {
                return bailerServiceClient;
            }
            else
            {
                try 
                {
                    String url = MGOBailerResourceConfig.getInstance().getBailerServiceURL();
                    int timeOut = MGOBailerResourceConfig.getInstance().getBailerServiceTimeout();
                    bailerServiceClient = new MGOBailerServicePortTypeProxy(url);
                    return bailerServiceClient;
                } 
                catch (Exception e) 
                {
                    throw new Exception("Failed to create BailerServiceClient:" + e.getMessage(),e);
                }
            }
        }
   
	public boolean processBailerEvents(Long consumerId)
			throws Exception {

	    ProcessBailerEventsRequest request = new ProcessBailerEventsRequest();

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.processBailerEvents.getValue());

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);
		request.setConsumerId(consumerId);


		ProcessBailerEventsResponse response = null;
		try {
			response = getBailerServiceClient().processBailerEvents(request);
			if (response == null) {
				throw new Exception ("Empty response from processBailerEvents encountered.");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new Exception("Call to processBailerEvents failed", e);
		}
		return response.isBailerEmailSent();
	}
	
	public boolean processFirstTxBailerEvents(Long consumerId) throws Exception {

		ProcessFirstTxBailerEventsRequest request = new ProcessFirstTxBailerEventsRequest();

		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(ServiceAction.processFirstTxBailerEvents
				.getValue());

		Header header = new Header();
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);
		request.setConsumerId(consumerId);

		ProcessFirstTxBailerEventsResponse response = null;
		try {
			response = getBailerServiceClient().processFirstTxBailerEvents(request);
			if (response == null) {
				throw new Exception(
						"Empty response from processFirstTxBailerEvents encountered.");
			}
		} catch (Exception e) {
			// e.printStackTrace();
			throw new Exception("Call to processFirstTxBailerEvents failed", e);
		}
		return response.isBailerEmailSent();
	}
}
