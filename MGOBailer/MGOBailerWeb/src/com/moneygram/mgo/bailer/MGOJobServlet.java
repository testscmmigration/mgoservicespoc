
package com.moneygram.mgo.bailer;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.proxies.MGOJobServiceProxy;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;


public class MGOJobServlet extends HttpServlet {
    private static final long serialVersionUID = 8637727531003658697L;
	private static final Logger logger = LogFactory.getInstance().getLogger(MGOJobServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
    	
    	logger.info("*******Processing MGOJOBService has begun...");
    	logger.info("MGOJOBServlet:Remote Address:" + request.getRemoteAddr());
    	String outPutSubStringValue = "";
    	String step = "Start of processing";
    	int nbrProfilesProcessed = 0;
       	PrintWriter out = response.getWriter();
    	response.setContentType("text/html");
    	String outPut = "";
    	int processNbrHoursBack = MGOBailerResourceConfig.getInstance().getNbrHoursBackToProcess();
    	Calendar startDateTime = null;
    	Integer maxResults = MGOBailerResourceConfig.getInstance().getMaxResults();
    	if (processNbrHoursBack > 0) {
    		startDateTime = Calendar.getInstance();
    		startDateTime.add(Calendar.HOUR, processNbrHoursBack * -1);
    	}
    	String action = request.getParameter("action");
    	logger.info("Requested MGOJOb Name=" +action);
    	String[] sites = request.getParameterValues("sites");
    	String[] stringProcessParam = request.getParameterValues("parms");
       	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	logger.info("Property maxResults=" + maxResults.intValue());
    	logger.info("Property processNbrHoursBack = " + processNbrHoursBack);
        logger.info("begin time = " + format.format(startDateTime.getTime()));
    	MGOJobServiceProxy mGOJobServiceProxy = new MGOJobServiceProxy();
    	try {
    		step = "About to Call Process Action in MGOJOBServlet";
			String processingText = mGOJobServiceProxy.processAction(action, sites, stringProcessParam);
			outPut = "Triggered JOBName==="+action +" ****** Response_From_Service==="+processingText;
			if(processingText!=null){
				outPutSubStringValue = processingText.substring(0, 7);
			}
			if(outPutSubStringValue.equals("SUCCESS")){
				out.println("<HTML>");
				out.println("<HEAD>");
				out.println("<TITLE>MGO_JOB_Framework</TITLE>");
				out.println("<BODY><CENTER>");
				out.println("JOB Status*****"+outPut);
				out.println("</CENTER></BODY>");
				out.println("</HEAD>");
				out.println("</HTML>");
	       	logger.info("*******Response From MGOJOb has SUCCESS***********"+outPut);
			}else{
				out.println("<HTML>");
				out.println("<HEAD>");
				out.println("<TITLE>MGO_JOB_Framework</TITLE>");
				out.println("<BODY><CENTER>");
				out.println("JOB Status*****"+outPut);
				out.println("</CENTER></BODY>");
				out.println("</HEAD>");
				out.println("</HTML>");
			logger.info("*******Response From MGOJOb has FAILED***********"+outPut);
			}        	
		} catch (Exception e) {
			logger.error("MGOJobServlet Servlet error in processing at step: " + step, e);
			logger.error("*******Processing MGOJob has FAILED..."+outPut+"MGOJOb Name"+action);
	        out.println("FAILURE at step: " + step + "; Exception= " + e);
		}        
        out.close();      
    }
 }
