package com.moneygram.mgo.bailer.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.moneygram.ree.lib.Config;


public class ResourceConfigFactory {

    /**
     * Creates new instance of the Config.
     * @return
     * @throws Exception
     */
    public Config createResourceConfiguration(String jndi) {
        Config config;
        try {
            Context ctx = new InitialContext();
            config = (Config) ctx.lookup(jndi);
        } catch (NamingException e) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi, e);
        }
        if (config == null) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi +". Configuration can not be null.");
        }
        return config;
    }
}
