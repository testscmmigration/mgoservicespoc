package com.moneygram.mgo.bailer.util;

public class MGOBailerResourceConfig extends ResourceConfig {

    public static final String MGO_CONSUMER_SERVICE_URL = "mgoConsumerServiceURL";
    public static final String MGO_CONSUMER_SERVICE_TIMEOUT = "mgoConsumerServiceTimeout";
    public static final String MGO_BAILER_SERVICE_URL = "mgoBailerServiceURL";
    public static final String MGO_BAILER_SERVICE_TIMEOUT = "mgoBailerServiceTimeout";
    public static final String DEFAULT_TIMEOUT = "15000";
    public static final String MAX_RESULTS = "maxResults";
    public static final String NBR_HOURS_BACK_TO_PROCESS = "nbrHoursBackToProcess";
    public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOBailerResourceReference";
    public static final String MGO_CONSUMER_SERVICE_2URL = "mgoConsumerService_v2_URL";
    /**
     * singleton instance.
     */
    private static MGOBailerResourceConfig instance = null;

    /**
     * Creates new instance of ResourceConfig
     * 
     */
    public MGOBailerResourceConfig() {
        super();
    }

    /**
     * 
     * @return
     */
    public static MGOBailerResourceConfig getInstance() {
        if (instance == null) {
            synchronized (MGOBailerResourceConfig.class) {
                if (instance == null) {
                    instance = new MGOBailerResourceConfig();
                    instance.initResourceConfig();
                }
            }
        }
        return instance;
    }

    public String getConsumerServiceURL() {
        return getAttributeValue(MGO_CONSUMER_SERVICE_URL);
    }

    public String getConsumerServicev2URL() {
        return getAttributeValue(MGO_CONSUMER_SERVICE_2URL);
    }
    
    public int getConsumerServiceTimeout() {
        return getIntegerAttributeValue(MGO_CONSUMER_SERVICE_TIMEOUT, DEFAULT_TIMEOUT);
    }

    public String getBailerServiceURL() {
        return getAttributeValue(MGO_BAILER_SERVICE_URL);
    }

    public int getBailerServiceTimeout() {
        return getIntegerAttributeValue(MGO_BAILER_SERVICE_TIMEOUT, DEFAULT_TIMEOUT);
    }

    public int getMaxResults() {
        return getIntegerAttributeValue(MAX_RESULTS, "0");
    }
    
    public int getNbrHoursBackToProcess() {
        return getIntegerAttributeValue(NBR_HOURS_BACK_TO_PROCESS, "72");
    }
    
    @Override
    protected String getResourceConfigurationJndiName() {
        return RESOURCE_REFERENCE_JNDI;
    }

	public int getJobServiceTimeoutMinutes() {
		// TODO Auto-generated method stub
		return getIntegerAttributeValue(NBR_HOURS_BACK_TO_PROCESS, "72");
	}

}
