
package com.moneygram.mgo.bailer;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.proxies.BailerServiceProxy;
import com.moneygram.mgo.bailer.proxies.ConsumerServicev2Proxy;

import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;


public class CompleteProfileIncompleteTxBailerServlet extends HttpServlet {
    private static final long serialVersionUID = 8637727531003658697L;

    private static final Logger logger = LogFactory.getInstance().getLogger(CompleteProfileIncompleteTxBailerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    protected void process(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
    	
    	logger.info("*******Processing First Tx Bailer Emails has begun...");
    	logger.info("BailerServlet:Remote Address:" + request.getRemoteAddr());
 
    	String step = "Start of processing";
    	int nbrEmailsSent = 0;
    	int nbrProfilesProcessed = 0;
    	
       	PrintWriter out = response.getWriter();
    	response.setContentType("text/html");
    	    	
    	int processNbrHoursBack = MGOBailerResourceConfig.getInstance().getNbrHoursBackToProcess();
    	Calendar startDateTime = null;
    	Integer maxResults = MGOBailerResourceConfig.getInstance().getMaxResults();

    	if (processNbrHoursBack > 0) {
    		startDateTime = Calendar.getInstance();
    		startDateTime.add(Calendar.HOUR, processNbrHoursBack * -1);
    	}
        
    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a (z)"); //ex: 12/04/2009 01:23:45 PM (CST)
    	logger.info("Property maxResults=" + maxResults.intValue());
    	logger.info("Property processNbrHoursBack = " + processNbrHoursBack);
        logger.info("begin time = " + format.format(startDateTime.getTime()));
        		
    	ConsumerServicev2Proxy csProxy = new ConsumerServicev2Proxy();
    	BailerServiceProxy bsProxy = new BailerServiceProxy();
    	try {
    		step = "About to get Incomplete Profiles";
			long consumerIds[] = csProxy.getIncompleteFirstTxProfiles(startDateTime, Calendar.getInstance(), maxResults);

			step = "About to process consumers";						
			if (consumerIds != null) {
				for (long consumerId : consumerIds) {
					step = "About to process consumerId = " + consumerId;
					boolean emailSent = bsProxy.processFirstTxBailerEvents(new Long(consumerId));
					logger.debug("Processed consumerid=" + consumerId + "; emailSent=" + emailSent);
					nbrProfilesProcessed++;
					if (emailSent) {
						nbrEmailsSent++;
					}
				}
			}
        	out.println("SUCCESS: #ProfilesProcessed= " + nbrProfilesProcessed + "; #EmailsSent=" + nbrEmailsSent);
        	logger.info("#ProfilesProcessed= " + nbrProfilesProcessed + "; #EmailsSent=" + nbrEmailsSent);
        	logger.info("*******Processing Bailer Emails has SUCCESSFULLY Completed...");
		} catch (Exception e) {
			logger.error("Bailer Servlet error in processing at step: " + step, e);
			logger.error("*******Processing Bailer Emails has FAILED...");
	        out.println("FAILURE at step: " + step + "; Exception= " + e);
		}        

        out.close();      
    }

}
