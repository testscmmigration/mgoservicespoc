package com.moneygram.mgo.bailer.proxies;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.bailer.util.MGOBailerResourceConfig;
import com.moneygram.mgo.bailer.util.ResourceConfig;
import com.moneygram.mgo.bailer.util.TestResourceConfigFactory;
import com.moneygram.mgo.common_v1.Header;
import com.moneygram.mgo.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.bailer_v1.ProcessBailerEventsRequest;
import com.moneygram.mgo.service.bailer_v1.ServiceAction;

public class BailerServiceProxyTest extends TestCase {

	private static final Logger logger = LogFactory.getInstance().getLogger(
			BailerServiceProxyTest.class);

	private static final String BASE_URL_PROPERTY = "mgo.service.baseurl";
	private static final String BASE_URL = "http://devwsintsvcs.moneygram.com/";
	private static final String SERVICE_URL = "MGOService/services/MGOBailerService_v1";
	private static final int timeout = 15000;
	private static BailerServiceProxy client = null;

	protected static final Map<String, String> attributes = new HashMap<String, String>();
	static {
		attributes.put(MGOBailerResourceConfig.MGO_CONSUMER_SERVICE_URL,
				"http://devwsintsvcs.moneygram.com/MGOService/services/MGOConsumerService_v1");
		attributes.put(MGOBailerResourceConfig.MGO_CONSUMER_SERVICE_TIMEOUT, "15000");
		attributes.put(MGOBailerResourceConfig.MGO_BAILER_SERVICE_URL,
		"http://devwsintsvcs.moneygram.com/MGOService/services/MGOConsumerService_v1");
		attributes.put(MGOBailerResourceConfig.MGO_BAILER_SERVICE_TIMEOUT, "15000");
		attributes.put(MGOBailerResourceConfig.NBR_HOURS_BACK_TO_PROCESS, "72");
	}
	
	protected void setUp() throws Exception {
		if (client != null)
			return;

		ResourceConfig.setResourceConfigFactory(new TestResourceConfigFactory(
				attributes));
		client = new BailerServiceProxy();
	}

	private String getServiceURL() {
		String url = System.getProperty(BASE_URL_PROPERTY);
		if (url == null || url.length() == 0) {
			url = BASE_URL;
			if (logger.isDebugEnabled()) {
				logger.debug("getServiceURL: property=" + BASE_URL_PROPERTY
						+ " is not defined. Use default base service url="
						+ url);
			}
		}
		url = url + SERVICE_URL;
		if (logger.isDebugEnabled()) {
			logger.debug("getServiceURL: service url=" + url);
		}
		return url;
	}
	
	public void testProcessBailerEvents() throws Exception {
		logger.info("\ntestProcessBailerEvents...");

		//boolean emailsSent = client.processBailerEvents(1546658L);

        //assertTrue(response.isBailerEmailSent());

	}
}
