package com.moneygram.mgo.bailer.util;

import java.util.Map;

import com.moneygram.mgo.bailer.util.ResourceConfigFactory;
import com.moneygram.ree.lib.Config;

public class TestResourceConfigFactory extends ResourceConfigFactory {
    private Map attributes = null;

    public TestResourceConfigFactory(Map attributes) {
        this.attributes = attributes;
    }

    @Override
    public Config createResourceConfiguration(String jndi) {
        return new TestConfig(attributes);
    }
    
}
