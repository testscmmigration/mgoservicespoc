package com.moneygram.mgo.bailer.util;

import java.util.Map;

import com.moneygram.ree.lib.Config;

public class TestConfig extends Config {
    private Map attributes = null;
    
    public TestConfig(Map attributes) {
        this.attributes = attributes;
        
    }

    @Override
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

}
