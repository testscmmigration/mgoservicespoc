package com.moneygram.mgo.service.asynctransactionprocessing.util;



public class MGOStatusCodes {
    public static final String RECIEVED = "REC";
    public static final String DELETE = "DEL";
    public static final String ACTIVE = "ACT";
    public static final String PENDING = "PEN";
    public static final String AUTOMATEDSCORINGPROCESS = "ASP";
    public static final String FORMFREESEND = "FFS";
    public static final String INCOMPLETE = "INC";
    public static final String SAVE = "SAV";
    public static final String NOTACTIVE = "NAT";
    public static final String CASH = "CSH";
    public static final String APPROVED = "APP";
    public static final String DENIED = "DEN";
    public static final String SENT = "SEN";
    public static final String ERROR = "ERR";
    public static final String CANCELED = "CXL";
    public static final String PAYMENTCOMPLETE_BILLPAY = "PMTCOMP";
    
}
