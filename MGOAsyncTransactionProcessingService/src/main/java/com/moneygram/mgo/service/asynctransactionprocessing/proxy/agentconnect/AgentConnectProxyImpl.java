package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentconnect;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.agentconnect1305.wsclient.AgentConnectProxy;
import com.moneygram.agentconnect1305.wsclient.CodeTableRequest;
import com.moneygram.agentconnect1305.wsclient.CodeTableResponse;
import com.moneygram.agentconnect1305.wsclient.Request;
import com.moneygram.mgo.service.asynctransactionprocessing.util.AgentConnectUtil;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;



@Component(value = "agentConnectServiceProxy")
@DependsOn("mgoResourceConfig")
public class AgentConnectProxyImpl implements AgentConnectProxyInterface {

	// AgentConnect Version
    public static final String AC_VERSION = "1305";
    public static final String AC_CLIENT_VERSION = "1305";

	@Autowired
	@Qualifier("agentConnectService")
	private AgentConnectProxy agentConnectService;
	
	@Autowired
	private MGOResourceConfig mgoResourceConfig;

	

	private static final Logger log = LoggerFactory.getLogger(AgentConnectProxyImpl.class);

	public CodeTableResponse codeTables(CodeTableRequest codeTableRequest) throws Exception {
		try {
			CodeTableResponse codeTableResponse = agentConnectService.codeTable(codeTableRequest);
			return codeTableResponse;
		} catch (Exception e) {
			log.error("Error in codeTables:" + AgentConnectUtil.buildErrorMessage(e), e);
			throw e;
		}
	}
	

    @Override
    public void populateRequest(Request request) throws Exception{
    	try {
	        request.setUserID(mgoResourceConfig.getAttributeValue(MGOResourceConfig.AGENT_CONNECT_USER_ID));
	        request.setToken(mgoResourceConfig.getAttributeValue(MGOResourceConfig.AGENT_CONNECT_PASSWORD));
	        request.setAgentSequence("1");
	        request.setLanguage("en");
	        request.setTimeStamp(Calendar.getInstance());
	        request.setApiVersion(AC_VERSION);
	        request.setClientSoftwareVersion(AC_CLIENT_VERSION);
	    } catch (Exception e) {
			log.error("Error in populateRequest:" +e.getMessage(), e);
			throw e;
	    }
    }

}
	
