package com.moneygram.mgo.service.asynctransactionprocessing.cache;


import java.io.Serializable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.moneygram.common.cache.Cache;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheException;
import com.moneygram.common.cache.CacheManager;
import com.moneygram.common.cache.CacheManagerFactory;
import com.moneygram.common.cache.ContextCacheKey;



@Service
public class CacheService<ElementType extends Serializable>
        implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(CacheService.class);


    /**
     * Returns the cacheElement value (usually a Map)
     */
    public Serializable getCacheElementValue(String cacheName, Serializable key) throws Exception  {
        Serializable result = null;
        try {
            CacheManager cacheManager = CacheManagerFactory.getCacheManagerInstance();
            Cache cache = cacheManager.getCache(cacheName);
            if (cache != null) {
                CacheElement cacheElement = cache.getElement(key);
                result = cacheElement.getValue();
            }
        } catch (Exception ex) {
            CacheService.log.error("Error in getMGOCacheCollection: " + ex.getMessage(), ex);
            throw new CacheException("Error retreiving element from cache: " + ex.getMessage());
        }
        return result;
    }

    public ElementType getCacheElementAttribute(String cacheName, String elementName, String key) throws CacheException {

        ElementType cacheCollection = null;
        try {
            CacheManager cacheManager = CacheManagerFactory.getCacheManagerInstance();
            Cache cache = cacheManager.getCache(cacheName);
            if (cache != null) {
                CacheElement cacheElement = cache.getElement(elementName);
                Map cacheMap = (Map) cacheElement.getValue();
                if (cacheMap == null) {
                    throw new CacheException("Failed to retrieve " + elementName + "from cache " + cacheName);
                }

                cacheCollection = (ElementType) cacheMap.get(key);

            }
        } catch (Exception ex) {
            CacheService.log.error("Error in getMGOCacheCollection: " + ex.getMessage(), ex);
            throw new CacheException("Error retreiving element from cache: " + ex.getMessage());
        }
        return cacheCollection;
    }

    public Map<String, ElementType> getCacheElement(String cacheName, String elementName) throws CacheException {

        Map<String, ElementType> cacheCollection = null;
        try {
            CacheManager cacheManager = CacheManagerFactory.getCacheManagerInstance();
            Cache cache = cacheManager.getCache(cacheName);
            if (cache != null) {
                CacheElement cacheElement = cache.getElement(elementName);
                Map cacheMap = (Map) cacheElement.getValue();
                if (cacheMap == null) {
                    throw new CacheException("Failed to retrieve " + elementName + "from cache " + cacheName);
                }

                cacheCollection = cacheMap;
            }
        } catch (Exception ex) {
            CacheService.log.error("Error in getMGOCacheCollection: " + ex.getMessage(), ex);
            throw new CacheException("Error retreiving element from cache: " + ex.getMessage());
        }
        return cacheCollection;
    }

    public Map<String, ElementType> getMGOCacheCollection(String cacheName, String elementName, String key)
            throws CacheException {
        Map<String, ElementType> cacheCollection = null;
        CacheManager cacheManager = CacheManagerFactory.getCacheManagerInstance();
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            CacheElement cacheElement = cache.getElement(elementName);
            Map cacheMap = (Map) cacheElement.getValue();
            if (cacheMap == null) {
                throw new CacheException("Failed to retrieve " + elementName + "from cache " + cacheName);
            }
            cacheCollection = (Map<String, ElementType>) cacheMap.get(key);
        }
        return cacheCollection;
    }

}
