/*
 * Created on Mar 26, 2010
 */
package com.moneygram.mgo.service.asynctransactionprocessing.util;


import java.util.Comparator;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.PaymentMethod;




/**
 * 
 * Financial Accounts Comparator. <div>
 * <table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOPortlets</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2010</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.1 $ $Date: 2010/03/26 20:49:39 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: a700 $ </td>
 *         </table>
 *         </div>
 */
public class FinancialAccountsComparator
        implements Comparator {

    public int compare(Object o1, Object o2) {
        if (o1 instanceof PaymentMethod && o2 instanceof PaymentMethod) {
            PaymentMethod p1 = (PaymentMethod) o1;
            PaymentMethod p2 = (PaymentMethod) o2;
            String v1 = p1.getPaymentType() + p1.getDisplayString();
            String v2 = p2.getPaymentType() + p2.getDisplayString();
            return v2.compareTo(v1);
        }
        return 0;
    }

}
