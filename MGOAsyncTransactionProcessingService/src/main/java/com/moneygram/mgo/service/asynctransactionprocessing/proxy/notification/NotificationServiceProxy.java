/**
 * @author Carlos Rodriguez <crodriguez@moneygram.com>
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.proxy.notification;


import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Name;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;


public interface NotificationServiceProxy {

    // --------------------------------- Methods -----------------------------------------//

	/**
	 * 
	 * @param tran
	 * @param siteId
	 * @param name
	 * @param emailAddress
	 * @param language
	 */
    public void notifySentTransaction(TransactionDetails tran, String siteId, Name name,String emailAddress, String language, String receiptData);

}