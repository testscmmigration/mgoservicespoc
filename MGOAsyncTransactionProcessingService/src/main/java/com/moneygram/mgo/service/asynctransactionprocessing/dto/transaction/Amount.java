package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.Remote;

public class Amount implements Remote, Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal value;
	private String currencyCode;
	private String payoutCurrencyCode;

	public Amount() {
	}

	public Amount(BigDecimal value, String currencyCode) {
		setValue(value);
		setCurrencyCode(currencyCode);
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPayoutCurrencyCode() {
		return payoutCurrencyCode;
	}

	public void setPayoutCurrencyCode(String payoutCurrencyCode) {
		this.payoutCurrencyCode = payoutCurrencyCode;
	}
}
