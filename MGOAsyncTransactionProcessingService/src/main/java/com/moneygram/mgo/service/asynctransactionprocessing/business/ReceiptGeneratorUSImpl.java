package com.moneygram.mgo.service.asynctransactionprocessing.business;


import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.PaymentMethod;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer.DataConvert;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.receipts.ReceiptsProxy;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOUtil;
import com.moneygram.mgo.service.asynctransactionprocessing.util.ReceiptFields;
import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;

/**
 * Implementation of {@link ReceiptGenerator} for the US/WAP receipt generation logic
 * 
 * @author vl58
 */
@Component("receiptGeneratorUS")
@DependsOn("mgoResourceConfig")
public class ReceiptGeneratorUSImpl extends ReceiptGeneratorBase implements ReceiptGenerator{

	public static final String ENGLISH_LANGUAGE_CODE = "en-US";
	public static final String SPANISH_LANGUAGE_CODE = "es-US";
	
	@Autowired
	protected MGOResourceConfig mgoResourceConfig;
	
	@Autowired 
	protected ReceiptsProxy receiptsServiceProxy;
	

	private static final Logger logger = LoggerFactory.getLogger(ReceiptGeneratorUSImpl.class);

	
	@Override
	public String generateReceipt(TransactionDetails transactionDetails, Sender sender,
			String transactionLanguageCode, String partnerSiteId) throws Exception {
		String htmlReceipt = null;
		try{
			if(transactionDetails != null){
				StringBuilder receiptContent = new StringBuilder("");
				receiptContent.append(generateReceiptContent(sender, partnerSiteId, transactionDetails, ENGLISH_LANGUAGE_CODE));
				if(!StringHelper.isNullOrEmpty(transactionLanguageCode) && SPANISH_LANGUAGE_CODE.equalsIgnoreCase(transactionLanguageCode) ){
					receiptContent.append(generateReceiptContent(sender, partnerSiteId, transactionDetails, SPANISH_LANGUAGE_CODE));
				}
				htmlReceipt = receiptContent.toString();
			}
		}catch(Exception ex){
			logger.error("Error in generateReceipt: "+ex.getMessage(), ex);
			throw new Exception("Failed to generate receipt",ex);
		}
		return htmlReceipt;
	}
	
	private String generateReceiptContent(Sender sender, String partnerSiteId, TransactionDetails transactionDetails, String userSelectedLocale) throws Exception{
		
		logger.debug("[generateReceiptContent] Sender = ("+sender.toString()+") , partnerSiteId = " + partnerSiteId + 
				", transactionDetails = ( " + transactionDetails.toString()+") , userSelectedLocale = " + userSelectedLocale );
		
		String htmlReceipt = null;
		
		try {
			//Setting up the appropriate payment method in the transaction details object
			setPaymentMethodsToTranDetails(sender, transactionDetails);
			
			//Setting up the receiver country and state info in transaction details object
			setCountryAndStateInfo(transactionDetails, sender);
			 
			Properties props = DataConvert.convertTranToProps(sender.getAddress(), sender.getName(), sender.getPrimaryPhone(), transactionDetails, userSelectedLocale);
			
			String state = sender.getAddress().getState().getCode();
			String tranType = props.getProperty(ReceiptFields.TRAN_TYPE);
			String receiptDate = props.getProperty(ReceiptFields.RECEIPT_DATE);
			
			logger.debug("[generateReceiptContent] userSelectedLocale = "+userSelectedLocale+" , state = " + state + 
				", tranType = " + tranType+" ,receiptDate= " + receiptDate );
		
			htmlReceipt = receiptsServiceProxy.getReceiptForTransaction( userSelectedLocale, state, tranType, receiptDate, props,partnerSiteId);
			
			logger.debug("[generateReceiptContent] language selected = "+userSelectedLocale+", receipt content = "+htmlReceipt);
			
		} catch (Exception ex) {
			logger.error("Error in generateReceiptContent: "+ex.getMessage(), ex);
			throw new Exception("Failed to generate receipt",ex);
		}		
		return htmlReceipt;
	}

	
}
