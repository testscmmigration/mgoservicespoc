package com.moneygram.mgo.service.asynctransactionprocessing.util;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.agentconnect1305.wsclient.Error;



public class AgentConnectUtil {

    private static final Logger log = LoggerFactory.getLogger(AgentConnectUtil.class);

   

    public static String buildErrorMessage(Exception exception) {
        if (exception == null)
            return "null";
        Error error = null;
        if (exception instanceof Error) {
            error = (Error) exception;
        } else {
            return exception.getMessage();
        }
        StringBuffer sb = new StringBuffer();
        if (error.getErrorCode() != null) {
            sb.append("Error Code:" + error.getErrorCode().toString());
        }
        if (StringUtils.isNotBlank(error.getErrorString())) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append("Error String:" + error.getErrorString());
        }
        if (StringUtils.isNotBlank(error.getOffendingField())) {
            if (sb.length() > 0)
                sb.append(", ");
            sb.append("Offending Field:" + error.getOffendingField());
        }
        return sb.toString();
    }

   

}
