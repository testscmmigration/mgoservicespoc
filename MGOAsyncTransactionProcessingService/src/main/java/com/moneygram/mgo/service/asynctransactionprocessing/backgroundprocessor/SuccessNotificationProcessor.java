package com.moneygram.mgo.service.asynctransactionprocessing.backgroundprocessor;


/**
 * This interface define the contract for a Success Notification Processor component . 
 *
 * @author vl58
 */
public interface SuccessNotificationProcessor {

	/**
	 * Mehtod for sending success notification emails to an user when transaction is processed correctly
	 * 
	 */
	public void processSuccessNotificationEmail(Long transactionId, String userLoginId,
			String transactionLanguageCode, String partnerSiteId) throws Exception;
}
