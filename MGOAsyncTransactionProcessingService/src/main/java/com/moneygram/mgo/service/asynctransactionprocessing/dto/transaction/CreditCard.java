package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;



public class CreditCard
        extends PaymentMethod {

    private static final long serialVersionUID = 1L;
    
    public static final String VISA = "CC-VISA";
    
    public static final String MASTERCARD = "CC-MSTR";
    
    public static final String MAESTRO = "CC-MAEST"; //Value required for Global Collect 

    public static final String CREDITCARD = "creditCard";
    public static final String DEBITCARD = "debitCard";
    public static final String UNKNOWN = "unknown";
    // Attributes
    
    private String cardType;

    private String cardNumber;

    private String expirationMonth;

    private String expirationYear;

    private String cvv;

    private String partnerSiteId;

    private boolean binBlocked = false;
    
    public static Map<String, String> CC_NAME_MAP = new LinkedHashMap<String, String>();
    
    static {
        CC_NAME_MAP.put(VISA, "Visa");
        CC_NAME_MAP.put(MASTERCARD, "MasterCard");
        CC_NAME_MAP.put(MAESTRO, "Maestro"); // Since Maestro integration
    }

    public CreditCard() {

    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    @Override
    public String getExpirationMonth() {
        return expirationMonth;
    }

    @Override
    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getPartnerSiteId() {
        return partnerSiteId;
    }

    public void setPartnerSiteId(String partnerSiteId) {
        this.partnerSiteId = partnerSiteId;
    }

    public boolean isBinBlocked() {
        return binBlocked;
    }

    public void setBinBlocked(boolean binBlocked) {
        this.binBlocked = binBlocked;
    }

    public String getLast4Digits() {
        return StringHelper.extractLastChars(getCardNumber(), 4);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    @Override
    public boolean isExpired() {

        try {
            Integer month = Integer.parseInt(this.getExpirationMonth());
            Integer year = Integer.parseInt(this.getExpirationYear());
            int currMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int currYear = Calendar.getInstance().get(Calendar.YEAR);
            if (currYear > year.intValue()) {
                return true;
            }
            if ((currYear == year.intValue()) && (currMonth > month.intValue())) {
                return true;
            }

        } catch (Exception e) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns the masked credit card number (**** **** **** 4158)
     */
    @Override
    public String getDisplayNumber() {
        return "**** **** **** " + getLast4Digits();
    }

    @Override
    public String getDisplayString() {
        String name = CC_NAME_MAP.get(getCardType());
        name = (name == null ? getCardType() : name);
        return name + ": **** **** **** " + getLast4Digits();
    }

    public String getDisplayCardType() {
        return CC_NAME_MAP.get(getCardType());
    }
    
    

}
