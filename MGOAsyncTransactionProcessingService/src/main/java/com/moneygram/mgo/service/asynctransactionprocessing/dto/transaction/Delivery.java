package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class Delivery
        implements Remote, Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1780401932707594047L;
	private String instruction1;
    private String instruction2;
    private String instruction3;

    public void setInstruction1(String instruction1) {
        this.instruction1 = instruction1;
    }

    public String getInstruction1() {
        return instruction1;
    }

    public void setInstruction2(String instruction2) {
        this.instruction2 = instruction2;
    }

    public String getInstruction2() {
        return instruction2;
    }

    public void setInstruction3(String instruction3) {
        this.instruction3 = instruction3;
    }

    public String getInstruction3() {
        return instruction3;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
