package com.moneygram.mgo.service.asynctransactionprocessing.parametersloader;


import java.util.Map;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;



public interface ParametersLoader {



    public Country getCountryByCode(String countryCode);

    public State getStateByCode(String stateCode, String countryCode);

	Map<String, State> getStates(String country) throws Exception;


}
