package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;


public interface AgentLocatorService {
    public List<Country> getCountries() throws Exception;

//    public HashMap<String, Services> getCountryServices() throws Exception;

    public Map<String, State> getStates(String country) throws Exception;
}
