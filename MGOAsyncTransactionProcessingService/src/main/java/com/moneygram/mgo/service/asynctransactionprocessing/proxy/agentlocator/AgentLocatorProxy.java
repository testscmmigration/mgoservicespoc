package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator;


import java.util.List;
import java.util.Map;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;


public interface AgentLocatorProxy {
    
    public Map<String, State> getStates(String countryCode) throws Exception;
    public List<Country> getCountries() throws Exception ;
}
