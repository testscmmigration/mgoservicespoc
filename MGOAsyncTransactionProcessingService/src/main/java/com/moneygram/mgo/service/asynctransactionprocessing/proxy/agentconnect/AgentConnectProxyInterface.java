package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentconnect;




import com.moneygram.agentconnect1305.wsclient.CodeTableRequest;
import com.moneygram.agentconnect1305.wsclient.CodeTableResponse;
import com.moneygram.agentconnect1305.wsclient.Request;



public interface AgentConnectProxyInterface {


    public CodeTableResponse codeTables(CodeTableRequest codeTableRequest) throws Exception;

	void populateRequest(Request request) throws Exception;

   
}
