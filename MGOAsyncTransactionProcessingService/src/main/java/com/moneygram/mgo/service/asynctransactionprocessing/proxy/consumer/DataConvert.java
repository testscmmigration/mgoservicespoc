/**
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.mgo.consumer_v2.shared_v1.ConsumerAddress;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Address;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Amount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.BankAccount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.CreditCard;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.MGOProduct;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Messages;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Name;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.PaymentMethod;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Phone;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Receiver;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionAmount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.util.FinancialAccountsComparator;
import com.moneygram.mgo.service.asynctransactionprocessing.util.GeneralUtils;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGODisplayStatusCodes;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.ReceiptFields;
import com.moneygram.mgo.service.asynctransactionprocessing.util.SpringApplicationContext;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer_v2.Contact;
import com.moneygram.mgo.service.consumer_v2.MGOAttributes;
import com.moneygram.mgo.service.consumer_v2.PersonalInfo;




/**
 * @author vl58
 * 
 */

public class DataConvert {

	private static final Logger logger = LoggerFactory.getLogger(DataConvert.class);
	private static MGOResourceConfig mgoResourceConfig = SpringApplicationContext.getBean("mgoResourceConfig");
	
	public static final String CREDITCARD_SV2 = "C";
	public static final String DEBITCARD_SV2 = "D";

	public static Properties convertTranToProps(Address senderAddress, Name senderName, Phone senderPrimaryPhone, TransactionDetails transactionDetails,
			String userSelectedLocale) {
		Properties p = new Properties();
		try {
			String signPrefix = null;
			NumberFormat nf = NumberFormat.getInstance();
			NumberFormat exRate = NumberFormat.getInstance();
			if (userSelectedLocale.equals("de-DE")) {
				nf = NumberFormat.getInstance(Locale.GERMAN);
				exRate = NumberFormat.getInstance(Locale.GERMAN);

			}
			nf.setMaximumFractionDigits(2);
			nf.setMinimumFractionDigits(2);
			exRate.setMaximumFractionDigits(4);
			exRate.setMinimumFractionDigits(4);
			p.setProperty(ReceiptFields.TRAN_TYPE,transactionDetails.getReceiptTranType());

			//Sender sender = (SenderDE) transactionDetails.getSender();

			String rf_state = senderAddress.getState().getCode() != null ? senderAddress.getState().getCode()
					: "Default";
			p.setProperty(ReceiptFields.STATE, rf_state);
			if ((userSelectedLocale != null) && !userSelectedLocale.equals("")) {
				p.setProperty(ReceiptFields.LANGUAGE, userSelectedLocale);
			} else {
				p.setProperty(ReceiptFields.LANGUAGE, "en-US");
			}

			String senderStreet = senderAddress.getAddress1();
	            String senderCity = senderAddress.getCity() + ", "
	                    + senderAddress.getState().getCode() + " " + senderAddress.getZipCode();
	            String senderCountry = senderAddress.getCountry().getName();
	            p.setProperty(ReceiptFields.SENDER_ADDRESS_STREET, senderStreet);
	            p.setProperty(ReceiptFields.SENDER_ADDRESS_CITY, senderCity);
	            p.setProperty(ReceiptFields.SENDER_ADDRESS_COUNTRY, senderCountry);
	            
	         //New address field for MGOUS
	        String senderAddressUS = senderStreet + " - " + senderCity + " - " + senderCountry;
	        p.setProperty(ReceiptFields.SENDER_ADDRESS_US, senderAddressUS);
	        
			String localeLanguage= "en";
			String localeCountry = "US";
			Locale locale = null;
			if ((userSelectedLocale != null) && !userSelectedLocale.equals("") && userSelectedLocale.contains("-")) {
				String userSelectedLocaleParts[] = userSelectedLocale.split("-");
				localeLanguage = userSelectedLocaleParts[0].trim().toLowerCase();
				localeCountry = userSelectedLocaleParts[1].trim().toLowerCase();
				locale = new Locale(localeLanguage,localeCountry);
			}
			locale = new Locale(localeLanguage,localeCountry);
			
		
			//TimeZone timeZoneByLocale = FacesUtil.getCurrentTimeZone();
			TimeZone timeZoneByLocale =	GeneralUtils.getTimeZoneByLocale(locale);

			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a");
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yy");
	        
			// For UK set timezone and exchange rate decimal digits
			if (p.getProperty(ReceiptFields.LANGUAGE).equals("en-GB")) {
				exRate.setMaximumFractionDigits(4);
				exRate.setMinimumFractionDigits(4);
				sdf.setTimeZone(timeZoneByLocale);
			}

			Date receiptDate = null;
			if (transactionDetails.getTransactionLog().getDateApproved() != null) {
				receiptDate = transactionDetails.getTransactionLog()
						.getDateApproved();
			} else {
				// this case should NEVER happen, if the tran status is SEN an
				// approved date should have been assigned
				// if this case does happen the receipt generation will fail
				// since
				// this is a key field
				receiptDate = transactionDetails.getTransactionLog()
						.getDateInitiated();
			}

			p.setProperty(ReceiptFields.RECEIPT_DATE, sdf.format(receiptDate));
			p.setProperty(ReceiptFields.LOCAL_RECEIPT_DATE, sdf1.format(receiptDate));
			p.setProperty(ReceiptFields.RECEIPT_TIMESTAMP,
					String.valueOf(receiptDate.getTime()));
			
			String mgTransactionSessionId = transactionDetails.getMgTransactionSessionId();
            // Receipts changes for dodd frank.
            // Condition added because of MGO-4046 restriction for receipts difference between AC 7.6 than new AC.
            String spanishVersion = DataConvert.getSpanishVersion(transactionDetails);
            if (spanishVersion != null) {
                p.setProperty(ReceiptFields.SPANISH_VERSION, spanishVersion);
            }
            TransactionAmount transactionAmount = transactionDetails.getTransactionAmount();
            BigDecimal recvTaxes = transactionAmount.getReceiveTaxes().getValue();
            BigDecimal recvFee = transactionAmount.getReceiveFee().getValue();
            if ((mgTransactionSessionId != null) && !"".equals(mgTransactionSessionId)) {
                if (recvFee != null) {
                    p.setProperty(ReceiptFields.RCV_FEE_AMOUNT, nf.format(recvFee.multiply(new BigDecimal(-1))));
                }
                if (recvTaxes != null) {
                    p.setProperty(ReceiptFields.RCV_TAX_AMOUNT, nf.format(recvTaxes.multiply(new BigDecimal(-1))));
                }
                if (transactionAmount.getTotalReceiveAmount().getValue() != null) {
                    p.setProperty(ReceiptFields.RCV_TOTAL_TO_RECEIVE_AMOUNT,
                        nf.format(transactionAmount.getTotalReceiveAmount().getValue()));
                }
                if (transactionAmount.getRcvTxFrAmount().getValue() != null) {
                    p.setProperty(ReceiptFields.RCV_TX_FR_AMOUNT,
                        nf.format(transactionAmount.getRcvTxFrAmount().getValue()));
                }
                if (transactionAmount.getSendTaxes().getValue() != null) {
            		
                    BigDecimal sendTaxesAmount = transactionAmount.getSendTaxes().getValue();
                    signPrefix = sendTaxesAmount.compareTo(BigDecimal.ZERO) != 0 ? "+ " : "  ";
                    p.setProperty(ReceiptFields.SENDER_TAX_AMOUNT,signPrefix + nf.format(sendTaxesAmount));
                    
                }
                if (transactionAmount.getExchangeRateApplied().getValue() != null) {
                    p.setProperty(ReceiptFields.EXCHANGE_RATE_APPLIED,
                        nf.format(transactionAmount.getExchangeRateApplied().getValue()));
                }
                if (transactionDetails.getDisclaimerTxtENG() != null) {
                    p.setProperty(ReceiptFields.DISCLAIMER_TXT_ENG, transactionDetails.getDisclaimerTxtENG());
                }
                if (transactionDetails.getDisclaimerTxtSPA() != null) {
                    p.setProperty(ReceiptFields.DISCLAIMER_TXT_SPA, transactionDetails.getDisclaimerTxtSPA());
                }
                if ((senderPrimaryPhone != null)
                        && (senderPrimaryPhone.getPhoneNumber() != null)) {
                    p.setProperty(ReceiptFields.SENDER_PHONE, DataConvert.formatPhoneNumber(senderPrimaryPhone.getPhoneNumber()));
                }
                if (transactionDetails.getTransactionLog().getDateAvailability() != null) {
                	SimpleDateFormat transferDateFormat = new SimpleDateFormat("MMM dd, yyyy");
                    String transferDate = transferDateFormat.format(transactionDetails.getTransactionLog()
                        .getDateAvailability());
                    p.setProperty(ReceiptFields.AVAILABLE_DATE, transferDate);
                }
                if (transactionDetails.getPmAccountNumber() != null) {
                    p.setProperty(ReceiptFields.PAYMENT_METHOD_ACC_NUMBER, transactionDetails.getPmAccountNumber());
                }
                if (transactionDetails.getCardType() != null) {
                    p.setProperty(ReceiptFields.CREDIT_CARD_TYPE, transactionDetails.getCardType());
                }
                if (transactionDetails.getCardbank() != null) {
                    p.setProperty(ReceiptFields.BANK, transactionDetails.getCardbank());
                }
                if (transactionDetails.getCardExpireDate() != null) {
                    p.setProperty(ReceiptFields.CREDIT_CARD_EXP_DATE, transactionDetails.getCardExpireDate());
                }
            }

			String receiveOption = transactionDetails.getReceiveOption() != null ? transactionDetails
					.getReceiveOption() : "";
			p.setProperty(ReceiptFields.DELIVERY_OPTION, receiveOption);
			
			// ALM-725 added code to display service option in mail receipt.
			String serviceOption = transactionDetails.getService() != null ? transactionDetails
					.getService() : "";
			p.setProperty(ReceiptFields.SERVICE_OPTION, serviceOption);	

			// Prior to October 2009, txns could have primary bank & backup
			// credit
			// card funding
			if (transactionDetails.getTransactionTypeCode().equals(
					MGOProduct.BILL_PAY)
					&& (transactionDetails.getBackupPaymentMethod() != null)) {
				p.setProperty(ReceiptFields.BANK_ACCOUNT_TYPE,
						transactionDetails.getPaymentMethod());
			} else {
				p.setProperty(ReceiptFields.ACCOUNT_TYPE,
						transactionDetails.getPaymentMethod());
			}
			if (transactionDetails.getBackupPaymentMethod() != null) {
				p.setProperty(ReceiptFields.BACKUP_CC,
						transactionDetails.getBackupPaymentMethod());
			}

			String refNum = transactionDetails.getReferenceNumber();
			if (refNum == null) {
				refNum = "";
			}
			if (transactionDetails.getDisplayStatus().equals(
					MGODisplayStatusCodes.CANCELED)) {
				refNum = "Canceled";
			}
			p.setProperty(ReceiptFields.REFERENCE_NUMBER, refNum);

			if (transactionDetails.getTransactionLog().getDateApproved() != null) {
				SimpleDateFormat transferDateFormat = new SimpleDateFormat(
						"MMM dd, yyyy hh:mm a z");
				if (p.getProperty(ReceiptFields.LANGUAGE).equals("en-GB")) {
					transferDateFormat.setTimeZone(timeZoneByLocale);
				}
				String transferDate = transferDateFormat
						.format(transactionDetails.getTransactionLog()
								.getDateApproved());
				p.setProperty(ReceiptFields.TRANSFER_DATE, transferDate);
			} else {
				p.setProperty(ReceiptFields.TRANSFER_DATE, "");
			}
			//Name senderName = sender.getName();
			String senderNameValue = senderName.getFirstName() + " "
					+ senderName.getLastName();
			p.setProperty(ReceiptFields.SENDER_NAME, senderNameValue);
			
			p.setProperty(ReceiptFields.SENDER_FIRST_NAME, senderName.getFirstName());
            p.setProperty(ReceiptFields.SENDER_LAST_NAME, senderName.getLastName());

            String midName = senderName.getMiddleName();
            if (midName == null) {
                midName = "";
            }
            p.setProperty(ReceiptFields.SENDER_MIDDLE_NAME, midName);

            String secondLastName = senderName.getSecondLastName();
            if (secondLastName == null) {
                secondLastName = "";
            }
            p.setProperty(ReceiptFields.SENDER_SECOND_LAST_NAME, secondLastName);

			p.setProperty(
					ReceiptFields.TRANSFER_AMOUNT,
					nf.format(transactionDetails.getTransactionAmount()
							.getSendAmount().getValue()));
			BigDecimal transferFees = transactionDetails.getTransactionAmount().getServiceFee().getValue();
			//in some cases is required add a plus sign to the amounts
			signPrefix = transferFees.compareTo(BigDecimal.ZERO) != 0 ? "+ " : "  ";
			p.setProperty(ReceiptFields.CONSUMER_FEE, signPrefix + nf.format(transferFees));
			
			p.setProperty(
					ReceiptFields.TOTAL,
					nf.format(transactionDetails.getTransactionAmount()
							.getTotalAmount().getValue()));

			Receiver receiver = transactionDetails.getReceiver();
			Name receiverName = receiver.getName();
			Address receiverAddress = receiver.getAddress();

			String receiverNameValue = "";
			if ((receiverName.getFirstName() != null)
					&& (receiverName.getLastName() != null)) {
				String middleInitial = receiverName.getMiddleName();
				if (middleInitial == null) {
					middleInitial = "";
				}
				receiverNameValue = receiverName.getFirstName() + " "
						+ middleInitial + " " + receiverName.getLastName();
				p.setProperty(ReceiptFields.RECEIVER_NAME, receiverNameValue);
			} else if (receiver.getSelectedBiller().getBillerName() != null) {
				receiverNameValue = receiver.getSelectedBiller()
						.getBillerName();
				p.setProperty(ReceiptFields.RECEIVER_NAME, receiverNameValue);
			}

			p.setProperty(ReceiptFields.RECEIVER_FIRST_NAME, receiverName.getFirstName());

            String recvMidName = receiverName.getMiddleName();
            if (recvMidName == null) {
                recvMidName = "";
            }
            p.setProperty(ReceiptFields.RECEIVER_MIDDLE_NAME, recvMidName);

            p.setProperty(ReceiptFields.RECEIVER_LAST_NAME, receiverName.getLastName());
			
			
			String receiverSecondLastName = receiverName.getSecondLastName();
			if (receiverSecondLastName == null) {
				receiverSecondLastName = "";
			}
			p.setProperty(ReceiptFields.RECEIVER_SECOND_LAST_NAME,
					receiverSecondLastName);

			if (receiverAddress.getCountry() != null) {
				p.setProperty(ReceiptFields.RECEIVE_COUNTRY, receiverAddress
						.getCountry().getName());
			}

			String rrnNumber = receiver.getRRNNumber();
			if (rrnNumber == null) {
				rrnNumber = "";
			}
			p.setProperty(ReceiptFields.RRN_NUMBER, rrnNumber);
			Messages receiverMessages = receiver.getMessages();
			String message1 = receiverMessages.getMessage1();
			if (message1 == null) {
				message1 = "";
			}
			p.setProperty(ReceiptFields.MESSAGE1, message1);
			String message2 = receiverMessages.getMessage2();
			if (message2 == null) {
				message2 = "";
			}
			p.setProperty(ReceiptFields.MESSAGE2, message2);

			
			p.setProperty(ReceiptFields.RECEIVE_AMOUNT, nf.format(transactionDetails.getTransactionAmount().getReceiveAmount().getValue()));
			
			if (recvTaxes != null) {
                p.setProperty(ReceiptFields.RECEIVE_TAXES, nf.format(recvTaxes.multiply(new BigDecimal(-1))));
            } else {
                p.setProperty(ReceiptFields.RECEIVE_TAXES, "0.00");
            }
            if (transactionAmount.getTotalReceiveAmount().getValue() != null) {
                p.setProperty(ReceiptFields.RECEIVE_TOTAL_AMOUNT,
                    nf.format(transactionAmount.getTotalReceiveAmount().getValue()));
            } else {
                p.setProperty(ReceiptFields.RECEIVE_TOTAL_AMOUNT, "0.00");
            }
            if (recvFee != null) {
                p.setProperty(ReceiptFields.RECEIVE_FEE, nf.format(recvFee.multiply(new BigDecimal(-1))));
            } else {
                p.setProperty(ReceiptFields.RECEIVE_FEE, "0.00");
            }
            if (transactionAmount.getSendTaxes().getValue() != null) {
                p.setProperty(ReceiptFields.SEND_TAXES, nf.format(transactionAmount.getSendTaxes().getValue()));
            } else {
                p.setProperty(ReceiptFields.SEND_TAXES, "0.00");
            }
			
			p.setProperty(ReceiptFields.RECEIVE_CURRENCY, transactionDetails.getTransactionAmount().getReceiveAmount().getCurrencyCode());
			p.setProperty(
					ReceiptFields.EXCHANGE_RATE,
					exRate.format(transactionDetails.getTransactionAmount()
							.getExchangeRate().getValue()));
			String rewardsNumber = transactionDetails.getRewardsNumber();
			if (rewardsNumber == null) {
				rewardsNumber = "";
			}
			p.setProperty(ReceiptFields.CARD_NUMBER, rewardsNumber);
			String receiverState = receiverAddress.getState().getName();
			if (receiverState == null) {
				receiverState = "";
			}
			p.setProperty(ReceiptFields.RECEIVE_STATE, receiverState);
			String bancomerConfirm = transactionDetails
					.getBancomerConfirmationNumber();
			if (bancomerConfirm == null) {
				bancomerConfirm = "";
			}
			p.setProperty(ReceiptFields.BANCOMER_CONFIRM_NUMBER,
					bancomerConfirm);
			p.setProperty(ReceiptFields.AUTH_NUMBER,
					transactionDetails.getTransactionCode());
			p.setProperty(
					ReceiptFields.REWARDS_DISCOUNT,
					nf.format(transactionDetails.getTransactionAmount()
							.getRewardsDiscount().getValue()));
			String accountNumber = transactionDetails.getReceiver()
					.getAccountNumber();
			if (accountNumber == null) {
				accountNumber = "";
			}
			p.setProperty(ReceiptFields.ACCOUNT_NUMBER, accountNumber);
			p.setProperty(ReceiptFields.SEND_CURRENCY, transactionDetails
					.getTransactionAmount().getSendAmount().getCurrencyCode());

			String serviceLevel = "";
			if (transactionDetails.getBillerServiceLevel() != null) {
				serviceLevel = transactionDetails.getBillerServiceLevel();
			}
			p.setProperty(ReceiptFields.BILLER_SERVICE_LEVEL, serviceLevel);

			String billerEndOfDay = "";
			if (transactionDetails.getBillerEndOfDayText() != null) {
				billerEndOfDay = transactionDetails.getBillerEndOfDayText();
			}
			p.setProperty(ReceiptFields.BILLER_END_OF_DAY, billerEndOfDay);

			String billerNotes = "";
			if (transactionDetails.getBillerNotes() != null) {
				billerNotes = transactionDetails.getBillerNotes();
			}
			p.setProperty(ReceiptFields.BILLER_NOTES, billerNotes);

			// S29a
			p.setProperty(ReceiptFields.THREE_MINUTE_PHONE_NUMBER,
					transactionDetails.getThreeMinuteFreePhoneNumber());
			p.setProperty(ReceiptFields.THREE_MINUTE_PIN_NUMBER,
					transactionDetails.getThreeMinuteFreePinNumber());

		} catch (Exception ex) {
			logger.error("Error in convertTranToProps: " + ex.getMessage(), ex);
		}
		return p;
	}
	
	private static String getSpanishVersion(TransactionDetails transactionDetails) {
        Calendar currentDate = Calendar.getInstance();
        Calendar spanishRCPTCalendar = loadSpanishStartDate();
        if ((transactionDetails.getMgTransactionSessionId() != null)
                || (currentDate.after(spanishRCPTCalendar) && MGOProduct.SAME_DAY.equals(transactionDetails
                    .getTransactionTypeCode()))) {
            return "true";
        } else {
            return null;
        }
    }

  private static Calendar loadSpanishStartDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyy");
        String mmddyyyy = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SHOW_SPANISH_RCPT_DATE);
        Calendar showDate = Calendar.getInstance();

        if ((mmddyyyy == null) || (mmddyyyy.length() < 10)) {
            logger.error("Formatting error getting in formatting Spanish receipt date: " + mmddyyyy
                    + "; Spanish Receipt display will be disabled");
            showDate.set(4000, 12, 12);
        }

        try {
            showDate.setTime(simpleDateFormat.parse(mmddyyyy));
        } catch (ParseException e) {
            logger.error("Formatting error getting in formatting Spanish receipt date: " + mmddyyyy
                    + "; Spanish Receipt display will be disabled");
            showDate.set(4000, 12, 12);
        }
        return showDate;

    }

	// Populate MGOConsumer using consumer in response from MGOConsumer Service
		public static Sender populateMGOConsumer(Consumer consumer) {
			boolean consumerBlocked = false;
			Sender sender = new Sender();

			MGOAttributes mgoAttributes = consumer.getInternal();

			PersonalInfo personalInfo = consumer.getPersonal();
			ConsumerAddress personalAddress = consumer.getAddress();
			Contact contact = consumer.getContact();
			// vl58 - S12 - preferred communication language, country of birth
			
			String partnerSiteId = consumer.getSourceSite();
			String MGODE_SiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGODE);
			if(MGODE_SiteId.equalsIgnoreCase(partnerSiteId)){
				String prefCommLanguage = contact.getPrefCommLanguage() != null ? contact
						.getPrefCommLanguage().getLanguageTagText() : null;
				sender.setPreflanguage(prefCommLanguage);	
			}else{
				String MGOUK_SiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUK);
				if(MGOUK_SiteId.equalsIgnoreCase(partnerSiteId)){
					sender.setPreflanguage("en-GB");
				}
				else{
					sender.setPreflanguage("en-US");
				}
			}
			
			

			sender.setConsumerCode(consumer.getConsumerId());
			sender.setSocialSecurityNumber(personalInfo.getSsn());
		

			Name senderName = sender.getName();
			senderName.setFirstName(personalInfo.getFirstName());
			senderName.setMiddleName(personalInfo.getMiddleName());
			senderName.setLastName(personalInfo.getLastName());
			senderName.setSecondLastName(personalInfo.getSecondLastName());
//			String idType = personalInfo.getExternalIdType();
//			if (idType != null) {
//				sender.getIdentification().setType(IdType.fromValue(idType));
//			}
//			String encryptedExternalId = personalInfo.getExternalId();
//
//			String externalId = null;

//			if (!StringHelper.isNullOrEmpty(encryptedExternalId)) {
//				externalId = decryptionService
//						.decryptWithoutPadding(encryptedExternalId);
//			}
//			sender.getIdentification().setNumber(externalId);
//			sender.getIdentification().setCountryIssuance(
//					personalInfo.getExternalIdCountryOfIssuance());
//			sender.getIdentification().setStateIssuance(
//					personalInfo.getExternalIdStateOfIssuance());
//			sender.getIdentification().setIssuance(
//					personalInfo.getExternalIdDateOfIssuance());
//			sender.getIdentification().setExpiration(
//					personalInfo.getExternalIdDateOfExpiration());
//
//			sender.setDateOfBirth(personalDOB.getTime());

			Address senderAddress = sender.getAddress();
			senderAddress.setAddress1(personalAddress.getLine1());
			senderAddress.setAddress2(personalAddress.getLine2());
			senderAddress.setAddress3(personalAddress.getLine3());
			senderAddress.setCity(personalAddress.getCity());
			senderAddress.getState().setCode(personalAddress.getState());
			senderAddress.setZipCode(personalAddress.getZipCode());
			senderAddress.getCountry().setCode(personalAddress.getCountry());
			senderAddress.setCounty(personalAddress.getCounty());
			senderAddress.setAddressCode(personalAddress.getAddressId());
			// vl58 - S12 - building name
			senderAddress.setAddress1Complement(personalAddress.getBuildingName());

			// UK
			// if(MGOConstants.SITEIDENTIFIER_UK.equals(consumer.getSourceSite())){
			// String[] parts =
			// PostalCodeTokenizerMGOUKImpl.parse(personalAddress.getZipCode());
			// senderAddress.getPostCode1().setValue(parts[0]);
			// senderAddress.getPostCode2().setValue(parts[1]);
			// }
			sender.setAddress(senderAddress);

			Phone primaryPhone = sender.getPrimaryPhone();
			String primaryPhoneNumberValue = contact.getPrimaryPhone();
			String primaryPhoneTypeValue = contact.getPrimaryPhoneType();
			Boolean primaryPhoneBlocked = mgoAttributes.getPrimaryPhoneBlocked();
			if ((primaryPhoneNumberValue != null)
					&& (primaryPhoneNumberValue.length() > 0)) {
				primaryPhone.setPhoneNumber(primaryPhoneNumberValue);
				primaryPhone.setBlocked(primaryPhoneBlocked == null ? false
						: primaryPhoneBlocked.booleanValue());
				if (primaryPhoneTypeValue != null) {
					primaryPhone.setPhoneType(primaryPhoneTypeValue);
				}

			} else {
				primaryPhone.setPhoneNumber(contact.getAlternatePhone());
				primaryPhone.setBlocked(primaryPhoneBlocked == null ? false
						: primaryPhoneBlocked.booleanValue());
			}

			Phone alternatePhone = sender.getAlternatePhone();
			String alternatePhoneNumberValue = contact.getAlternatePhone();
			String alternatePhoneTypeValue = contact.getAlternatePhoneType();
			Boolean alternatePhoneBlocked = mgoAttributes
					.getAlternatePhoneBlocked();
			if ((alternatePhoneNumberValue != null)
					&& (alternatePhoneNumberValue.length() > 0)) {
				alternatePhone.setPhoneNumber(alternatePhoneNumberValue);
				alternatePhone.setBlocked(alternatePhoneBlocked == null ? false
						: alternatePhoneBlocked.booleanValue());
				if (alternatePhoneTypeValue != null) {
					alternatePhone.setPhoneType(alternatePhoneTypeValue);
				}
			}

			

			// Accounts
			List<PaymentMethod> financialAccounts = sender.getFinancialAccounts();
			financialAccounts.clear();
			ConsumerFIAccount[] financialAccts = consumer.getAccounts();
			if (financialAccts != null) {
				for (ConsumerFIAccount financialAcct : financialAccts) {
					com.moneygram.mgo.service.consumer_v2.CreditCard consumerCreditCard = financialAcct
							.getCreditCard();
					if (consumerCreditCard != null) {
						CreditCard creditCard = new CreditCard();
						creditCard.setPartnerSiteId(consumer.getSourceSite());
						creditCard
								.setBlocked(consumerCreditCard.getBlocked() == null ? false
										: consumerCreditCard.getBlocked()
												.booleanValue());
						creditCard
								.setBinBlocked(consumerCreditCard.getBinBlocked() == null ? false
										: consumerCreditCard.getBinBlocked()
												.booleanValue());
						creditCard.setAccountCode(String.valueOf(consumerCreditCard
								.getAccountId()));
						creditCard.setAccountStatus(consumerCreditCard
								.getAccountStatus());
						creditCard.setAccountSubStatus(consumerCreditCard
								.getAccountSubStatus());
						creditCard.setCardNumber(consumerCreditCard
								.getAccountNumber());
						creditCard.setCardType(consumerCreditCard.getAccountType()
								.getValue());
						creditCard.setExpirationMonth(String
								.valueOf(consumerCreditCard.getExpMonth()));
						creditCard.setExpirationYear(String
								.valueOf(consumerCreditCard.getExpYear()));
						String paymentType = "";
						if (consumerCreditCard.getCardType().equals(CREDITCARD_SV2)) {
							paymentType = CreditCard.CREDITCARD;
						} else if (consumerCreditCard.getCardType().equals(DEBITCARD_SV2)) {
							paymentType = CreditCard.DEBITCARD;
						}
						creditCard.setPaymentType(paymentType);
						financialAccounts.add(creditCard);
						consumerBlocked = consumerBlocked
								|| creditCard.isBinBlocked()
								|| creditCard.isBlocked();
					}
					com.moneygram.mgo.service.consumer_v2.BankAccount consumerBankAccount = financialAcct
							.getBankAccount();
					if (consumerBankAccount != null) {
						BankAccount bankAccount = new BankAccount();
						bankAccount
								.setBlocked(consumerBankAccount.getBlocked() == null ? false
										: consumerBankAccount.getBlocked()
												.booleanValue());
						bankAccount.setAbaBlocked(consumerBankAccount
								.getAbaBlocked() == null ? false
								: consumerBankAccount.getAbaBlocked()
										.booleanValue());
						bankAccount.setAccountCode(String
								.valueOf(consumerBankAccount.getAccountId()));
						bankAccount.setAccountStatus(consumerBankAccount
								.getAccountStatus());
						bankAccount.setEncryptedAccountNumber(consumerBankAccount
								.getEncryptedAccountNumber());
						bankAccount.setAccountNumber(consumerBankAccount
								.getAccountNumber());
						bankAccount.setAccountType(consumerBankAccount
								.getAccountType().getValue());
						bankAccount.setBankName(consumerBankAccount.getFiName());
						bankAccount.setRoutingNumber(consumerBankAccount
								.getAbaNumber());
						bankAccount.setPaymentType(BankAccount.BANKACCOUNT);
						financialAccounts.add(bankAccount);
						consumerBlocked = consumerBlocked
								|| bankAccount.isAbaBlocked()
								|| bankAccount.isBlocked();
					}
				}
			}

			// sort the payment methods by PaymentType
			Collections.sort(financialAccounts, new FinancialAccountsComparator());

			return sender;
		}

		 private static String formatPhoneNumber(String phoneNumberRaw) {
		        if (phoneNumberRaw.length() == 10) {
		            // xxx-xxx-xxxx
		            MessageFormat phoneMsgFmt = new java.text.MessageFormat("{0}-{1}-{2}");
		            String[] phoneNumber = {phoneNumberRaw.substring(0, 3), phoneNumberRaw.substring(3, 6),
		                    phoneNumberRaw.substring(6)};
		            return phoneMsgFmt.format(phoneNumber);
		        } else {
		            StringBuffer out = new StringBuffer();
		            boolean fourChars = true;
		            for (int i = phoneNumberRaw.length() - 1; i >= 0; i--) {
		                out.insert(0, phoneNumberRaw.charAt(i));
		                // Add a hyphen if there is a block of four number or if there is a new block of three numbers
		                boolean hyphen = (fourChars && (out.length() == 4)) || ((((out.length() - 1) % 3) == 0) && !fourChars);
		                if ((i > 0) && hyphen) {
		                    out.insert(0, '-');
		                    fourChars = false;
		                }
		            }
		            return new String(out);
		        }
		    }
}
