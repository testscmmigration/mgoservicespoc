package com.moneygram.mgo.service.asynctransactionprocessing.business;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;

public interface ReceiptGenerator {


	public String generateReceipt(TransactionDetails transactionDetails, Sender sender,
			String transactionLanguageCode, String partnerSiteId) 
			throws Exception;

}
