package com.moneygram.mgo.service.asynctransactionprocessing.exception;

public class MGOAsyncTransactionProcessingServiceException extends Exception {

	public MGOAsyncTransactionProcessingServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MGOAsyncTransactionProcessingServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
