package com.moneygram.mgo.service.asynctransactionprocessing.backgroundprocessor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.business.SuccessNotificationEmailHandler;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;


/**
 * Implementation of {@link SuccessNotificationProcessor}, process success notification email requests sent via JMS
 * 
 * @author vl58
 */
@Component
@DependsOn("mgoResourceConfig")
public class SuccessNotificationProcessorImpl implements SuccessNotificationProcessor {
	
	
	@Autowired
	protected MGOResourceConfig mgoResourceConfig;
	
	@Autowired
	private SuccessNotificationEmailHandler successNotificationEmailHandler;
	
	private static final Logger logger = LoggerFactory.getLogger(SuccessNotificationProcessorImpl.class);
	
	
	public void processSuccessNotificationEmail(Long transactionId, String userLoginId,
			String transactionLanguageCode, String partnerSiteId) throws Exception {
		try{
			logger.info("[processSuccessNotificationEmail] start processing email notification for transactionId = "+transactionId+", userLoginId = "+userLoginId+", transactionLanguageCode = "+transactionLanguageCode+", partnerSiteId = "+partnerSiteId);
			successNotificationEmailHandler.handleSuccessNotificationEmail(transactionId, userLoginId, transactionLanguageCode, partnerSiteId);
			logger.info("[processSuccessNotificationEmail] email sent to user for transactionId = "+transactionId+", userLoginId = "+userLoginId+", transactionLanguageCode = "+transactionLanguageCode+", partnerSiteId = "+partnerSiteId);

		}catch(Exception ex){
			logger.error("Error in processSuccessNotificationEmail: "+ex.getMessage(), ex);
			throw ex;
		}
		
	}

}
