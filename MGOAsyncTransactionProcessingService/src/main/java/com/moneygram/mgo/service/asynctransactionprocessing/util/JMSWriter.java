/*
 * Created on Jul 20, 2011
 */
package com.moneygram.mgo.service.asynctransactionprocessing.util;


/**
 * 
 * JMS Writer. <div>
 * <table>
 * <tr>
 * <th>Title:</th>
 * <td>PortalPlatformWeb</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2011</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.1 $ $Date: 2011/07/20 15:54:49 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: a700 $ </td>
 *         </table>
 *         </div>
 */
public interface JMSWriter {

    /**
     * Writes the message to the JMS queue.
     * 
     * @param content
     * @throws MessagingException
     */
    public void writeMessage(String content) throws MessagingException;

}
