package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;


public class Currency
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private String code = "";
    private String name = "";
    private int precision = 0;

    public Currency() {
    }

    public Currency(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    @Override
    public String toString() {
        return code;
    }

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("\"code\":\"").append(code).append("\"");
        sb.append(",");
        sb.append("\"name\":\"").append(name).append("\"");
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((code == null) ? 0 : code.hashCode());
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        result = (prime * result) + precision;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Currency other = (Currency) obj;
        if (code == null) {
            if (other.code != null) {
                return false;
            }
        } else if (!code.equals(other.code)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (precision != other.precision) {
            return false;
        }
        return true;
    }

    public Currency clone() {
    	Currency res = new Currency();
    	res.setCode(code);
    	res.setName(name);
    	res.setPrecision(precision);
    	return res;
    }

    public static Currency createFromCode(String code) {
        Currency currency = new Currency();
        currency.setCode(code);
        return currency;
    }

}
