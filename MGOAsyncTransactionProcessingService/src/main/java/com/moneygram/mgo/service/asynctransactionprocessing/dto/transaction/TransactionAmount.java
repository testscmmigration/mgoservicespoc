package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;

import java.io.Serializable;
import java.rmi.Remote;


public class TransactionAmount
        implements Remote, Serializable {
    private static final long serialVersionUID = 1L;
    private Amount nonDiscountedFee;
    private Amount receiveAmount;
    private Amount sendAmount;
    private Amount sendTaxes;
    private Amount serviceFee;
    private Amount returnFee;
    private Amount totalAmount;
    private Amount rewardsDiscount;
    private Amount exchangeRate;
    private Amount receiveFee;
    private Amount receiveTaxes;
    private Amount totalReceiveAmount;
    private Amount rcvTxFrAmount;
    private Amount exchangeRateApplied;

    // Receive taxes and fees may be estimated.
    private boolean recvTaxEstimated = false;
    private boolean recvFeeEstimated = false;

    private boolean indicativeCountry;

    public Amount getExchangeRateApplied() {
        return exchangeRateApplied;
    }

    public void setExchangeRateApplied(Amount exchangeRateApplied) {
        this.exchangeRateApplied = exchangeRateApplied;
    }

    public Amount getRcvTxFrAmount() {
        return rcvTxFrAmount;
    }

    public void setRcvTxFrAmount(Amount rcvTxFrAmount) {
        this.rcvTxFrAmount = rcvTxFrAmount;
    }

    public void setNonDiscountedFee(Amount nonDiscountedFee) {
        this.nonDiscountedFee = nonDiscountedFee;
    }

    public Amount getNonDiscountedFee() {
        return nonDiscountedFee;
    }

    public void setReceiveAmount(Amount receiveAmount) {
        this.receiveAmount = receiveAmount;
    }

    public Amount getReceiveAmount() {
        return receiveAmount;
    }

    public void setSendAmount(Amount sendAmount) {
        this.sendAmount = sendAmount;
    }

    public Amount getSendAmount() {
        return sendAmount;
    }

    public void setServiceFee(Amount serviceFee) {
        this.serviceFee = serviceFee;
    }

    public Amount getServiceFee() {
        return serviceFee;
    }

    public void setReturnFee(Amount returnFee) {
        this.returnFee = returnFee;
    }

    public Amount getReturnFee() {
        return returnFee;
    }

    public void setTotalAmount(Amount totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Amount getTotalAmount() {
        return totalAmount;
    }

    public void setRewardsDiscount(Amount rewardsDiscount) {
        this.rewardsDiscount = rewardsDiscount;
    }

    public Amount getRewardsDiscount() {
        return rewardsDiscount;
    }

    public void setExchangeRate(Amount exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Amount getExchangeRate() {
        return exchangeRate;
    }

    public Amount getSendTaxes() {
        return sendTaxes;
    }

    public void setSendTaxes(Amount sendTaxes) {
        this.sendTaxes = sendTaxes;
    }

    public Amount getReceiveFee() {
        return receiveFee;
    }

    public void setReceiveFee(Amount receiveFee) {
        this.receiveFee = receiveFee;
    }

    public Amount getReceiveTaxes() {
        return receiveTaxes;
    }

    public void setReceiveTaxes(Amount receiveTaxes) {
        this.receiveTaxes = receiveTaxes;
    }

    public Amount getTotalReceiveAmount() {
        return totalReceiveAmount;
    }

    public void setTotalReceiveAmount(Amount totalReceiveAmount) {
        this.totalReceiveAmount = totalReceiveAmount;
    }

    public boolean isRecvTaxEstimated() {
        return recvTaxEstimated;
    }

    public void setRecvTaxEstimated(boolean recvTaxEstimated) {
        this.recvTaxEstimated = recvTaxEstimated;
    }

    public boolean isRecvFeeEstimated() {
        return recvFeeEstimated;
    }

    public void setRecvFeeEstimated(boolean recvFeeEstimated) {
        this.recvFeeEstimated = recvFeeEstimated;
    }

    public boolean isIndicativeCountry() {
        return indicativeCountry;
    }

    public void setIndicativeCountry(boolean indicativeCountry) {
        this.indicativeCountry = indicativeCountry;
    }
}
