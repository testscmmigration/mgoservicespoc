package com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.consumer_v2.common_v1.Header;
import com.moneygram.mgo.consumer_v2.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2;
import com.moneygram.mgo.service.consumer_v2.ProfilePart;
import com.moneygram.mgo.service.consumer_v2.ServiceAction;

@Component(value = "consumerServicev2_Proxy")
public class Consumer_v2ProxyImpl implements Consumer_v2Proxy{
	
	 @Autowired
	 protected MGOConsumerServicePortType_v2 consumerService_v2;
	 
		private static final Logger logger = LoggerFactory.getLogger(Consumer_v2ProxyImpl.class);
	 
	    @Override
	    public Sender getConsumerProfile(String userLoginId, String partnerSiteId)
	            throws Exception {
	    	Sender sender = null;
	        ProfilePart[] profilePart = new ProfilePart[]{
	        		ProfilePart.PersonalInfo, ProfilePart.Contact, ProfilePart.Accounts, 
	        		ProfilePart.Address ,ProfilePart.Internal, ProfilePart.LoyaltyInfo,
	        		ProfilePart.TransactionPreferences, ProfilePart.ProfileEvents };
	        Consumer consumer = getConsumerProfile(userLoginId, partnerSiteId, profilePart);
	        if (consumer != null) {
	        	sender = DataConvert.populateMGOConsumer(consumer);
	        	  logger.debug("[getConsumerProfile] addressId = "+sender.getAddress().getAddressCode());
	        }
	        return sender;
	    }
	    
	  
	  private Consumer getConsumerProfile(String userLoginId, String partnerSiteId, ProfilePart[] profilePart)
	            throws Exception {

	        GetConsumerProfileRequest request = new GetConsumerProfileRequest();

	    	ProcessingInstruction processingInstruction = new ProcessingInstruction();
	        processingInstruction.setAction(ServiceAction.getConsumerProfile_v2.getValue());
	        processingInstruction.setReadOnlyFlag(Boolean.TRUE);
	        processingInstruction.setPartnerSiteId(partnerSiteId);

	        Header header = new Header();
	        header.setProcessingInstruction(processingInstruction);
	        request.setHeader(header);
	        
	        
	        request.setConsumerLoginId(userLoginId);
	        logger.debug("[getConsumerProfile] consumer logon id : "+request.getConsumerLoginId());
	        request.setResponseFilter(profilePart);

	        try {
	            GetConsumerProfileResponse response = consumerService_v2.get(request);
	            if (response != null) {
	                return response.getConsumer();
	            }
	        } catch (Exception e) {
	            String email = userLoginId != null ? userLoginId : "unknown";
	            Consumer_v2ProxyImpl.logger.error("Service call to getConsumerProfile failed; logonId=" + email, e);

	            throw new Exception("Call to getConsumerProfile failed", e);
	        }
	        return null;
	    }

}
