package com.moneygram.mgo.service.asynctransactionprocessing.proxy.transaction;



import java.math.BigDecimal;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Address;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Amount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Delivery;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Messages;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Name;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Receiver;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionAmount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionLog;
import com.moneygram.mgo.service.asynctransactionprocessing.parametersloader.ParametersLoader;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOStatusCodes;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOUtil;
import com.moneygram.mgo.service.transaction_v1.GetTransactionRequest;
import com.moneygram.mgo.service.transaction_v1.GetTransactionResponse;
import com.moneygram.mgo.service.transaction_v1.MGOProductType;
import com.moneygram.mgo.service.transaction_v1.MGOTransactionProcessing;
import com.moneygram.mgo.service.transaction_v1.MGOTransactionServicePortType;
import com.moneygram.mgo.service.transaction_v1.ServiceAction;
import com.moneygram.mgo.transaction.shared_v1.ReceiverConsumerAddress;
import com.moneygram.mgo.transaction.shared_v1.ReceiverConsumerName;
import com.moneygram.mgo.transaction.common_v1.Header;
import com.moneygram.mgo.transaction.common_v1.ProcessingInstruction;


@Component(value = "transactionServiceProxy")
@DependsOn("mgoResourceConfig")
public class TransactionProxyImpl
        implements TransactionProxy {

	@Autowired
    protected ParametersLoader paramsLoader;
	
	private static final String DB_FLAG_TRUE = "Y";
    private static final String DB_FLAG_FALSE = "N";

	private static final Logger logger = LoggerFactory.getLogger(TransactionProxyImpl.class);
    public static final String EPSEND = "EPSEND";
    public static final String MGSEND = "MGSEND";
    public static final String ESSEND = "ESSEND";
    public static final String DSSEND = "DSSEND";
    public static final String MGCASH = "MGCASH";
    public static final String EPCASH = "EPCASH";

    public static final String MONEYTRANSFER = "Money Transfer";
    public static final String BILLPAY = "Bill Pay";
    public static final String ECONOMY = "Economy";
    public static final String SAMEDAY = "Same Day";
    //German
    public static final String DE_MONEYTRANSFER = "Am gleichen Tag";
    public static final String DE_BILLPAY = "Bill Pay";
    public static final String DE_ECONOMY = "Economy";
    public static final String DE_SAMEDAY = "Am gleichen Tag";
    public static final String DE_CASH = "In-Store Payment";
    public static final String DE_PAYOUT = "Payout";
    
    public static final String AFF_SAME_DAY = "AFF SAME DAY";
    public static final String AFF_DELAY_SEND = "AFF DSSEND";
    public static final String CASH = "In-Store Payment";
    public static final String PAYOUT = "Payout";
    
    
  
    
    

    @Autowired
    protected MGOTransactionServicePortType transactionService_v1;

    @Autowired
    protected MGOResourceConfig mgoResourceConfig;

   
    @Override
    public TransactionDetails getTransaction(String transactionCode) throws Exception{
        // ServiceAccess serviceAccess = new ServiceAccess();
        TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setTransactionCode(transactionCode);
        try {
            GetTransactionRequest request = new GetTransactionRequest();

            ProcessingInstruction processingInstruction = new ProcessingInstruction();
            processingInstruction.setAction(ServiceAction._getTransaction);
            processingInstruction.setReadOnlyFlag(Boolean.TRUE);

            Header header = new Header();
            header.setProcessingInstruction(processingInstruction);
            request.setHeader(header);

            request.setTransactionId(Long.parseLong(transactionCode));
            request.setResponseFilter(new String[] {});

            GetTransactionResponse response = transactionService_v1.get(request);
            com.moneygram.mgo.service.transaction_v1.MGOTransaction consumerTransaction = response.getTransaction();


            // Get the mgTransactionSessionId that new transactions have.
            transactionDetails.setMgTransactionSessionId(consumerTransaction.getMgTransactionSessionId());
    

            // POPULATE STATUS
            transactionDetails.setStatus(consumerTransaction.getStatus());
            transactionDetails.setSubStatus(consumerTransaction.getSubStatus());
            String rewardsNumber = consumerTransaction.getSender().getSenderLoyaltyMemberId();

            if (rewardsNumber != null) {
                transactionDetails.setRewardsNumber(rewardsNumber);
            }

            // POPULATE TRANSACTION INFO
            MGOTransactionProcessing mgoTransactionProcessing = consumerTransaction.getMgoProcessing();

            if (mgoTransactionProcessing.getReferenceNumber() != null) {
                transactionDetails.setShowReceipt(true);
                transactionDetails.setReferenceNumber(mgoTransactionProcessing.getReferenceNumber());
            } else {
                transactionDetails.setShowReceipt(false);
            }

            transactionDetails.setBancomerConfirmationNumber(mgoTransactionProcessing
                .getReceiveAgentConfirmationNumber());

            MGOProductType mgoProductType = consumerTransaction.getProductType();
            String mgoProductTypeValue = mgoProductType.toString();
            transactionDetails.setTransactionTypeCode(mgoProductTypeValue);
            transactionDetails.setTransactionType(getTransactionType(mgoProductTypeValue));
            transactionDetails.setPartnerSiteId(consumerTransaction.getSourceSite().trim().toUpperCase());
            transactionDetails.setReceiptTranType(getReceiptTransactionType(consumerTransaction.getSourceSite().trim().toUpperCase(),
                mgoProductTypeValue));
            transactionDetails.setService(getServiceName(mgoProductTypeValue));

            if (mgoProductTypeValue.equals(TransactionProxyImpl.MGCASH)) {
                transactionDetails.setPaymentMethod(TransactionProxyImpl.CASH);
                transactionDetails.setFiAccountId(null);
            } else {
            	transactionDetails.setFiAccountId(consumerTransaction.getSender().getFiAccountId());
            	transactionDetails.setBackupFiAccountId(consumerTransaction.getSender().getBackupFIAccountId());


            }

            String receiveAgentID = "";
            if (consumerTransaction.getReceiveAgent() != null) {
                receiveAgentID = String.valueOf(consumerTransaction.getReceiveAgent());
            }
            transactionDetails.setReceiveAgentID(receiveAgentID);
            // String receiveOption = MGOUtil.getReceiveOptionDisplayString(consumerTransaction.getReceiveCountry(),
            // consumerTransaction.getDeliveryOption(), consumerTransaction.getReceiveCurrency(), receiveAgentID);
            //String receiveOption = this.getReceiveOptionDisplayString(consumerTransaction, receiveAgentID);
            //transactionDetails.setReceiveOption(receiveOption);
            transactionDetails.setReceiveOptionID(String.valueOf(consumerTransaction.getDeliveryOption()));

            com.moneygram.mgo.service.transaction_v1.TransactionAmount consumerTransactionAmount = consumerTransaction
                .getTransactionAmount();
            TransactionAmount transactionAmount = transactionDetails.getTransactionAmount();

            String sendCurrenyCode = consumerTransaction.getSendCurrency();

            Amount exchangeRateAmount = new Amount();
            exchangeRateAmount.setValue(consumerTransactionAmount.getExchangeRateApplied());
            exchangeRateAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setExchangeRate(exchangeRateAmount);

            
            
            
            
            // POPULATE TRANSACTION AMOUNTS
            
         // POPULATE TRANSACTION AMOUNTS
            Amount receiveAmount = new Amount();
            receiveAmount.setValue(consumerTransactionAmount.getFaceAmount());
            String receiveCurrencyCode = consumerTransaction.getReceiveCurrency();
            String estimatedCurrencyCode = consumerTransaction.getInrcvPayoutCrncyCode();
            receiveAmount.setCurrencyCode(receiveCurrencyCode);
            receiveAmount.setPayoutCurrencyCode(estimatedCurrencyCode);
            BigDecimal recAmount = receiveAmount.getValue();
            BigDecimal excRate = exchangeRateAmount.getValue();
            receiveAmount.setValue(recAmount.multiply(excRate));
            transactionAmount.setReceiveAmount(receiveAmount);

            Amount sendAmount = new Amount();
            sendAmount.setValue(consumerTransactionAmount.getFaceAmount());
            sendAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setSendAmount(sendAmount);

            BigDecimal serviceFee = consumerTransactionAmount.getSendFee();
            Amount serviceFeeAmount = new Amount();
            serviceFeeAmount.setValue(serviceFee);
            serviceFeeAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setServiceFee(serviceFeeAmount);

            Amount totalAmount = new Amount();
            totalAmount.setValue(consumerTransactionAmount.getTotalAmount());
            totalAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setTotalAmount(totalAmount);

            Amount totalRecvAmount = new Amount();
            totalRecvAmount.setValue(consumerTransactionAmount.getInrcvPayoutAmt());
            totalRecvAmount.setCurrencyCode(receiveCurrencyCode);
            totalRecvAmount.setPayoutCurrencyCode(estimatedCurrencyCode);
            transactionAmount.setTotalReceiveAmount(totalRecvAmount);

            Amount receiveFeeAmount = new Amount();
            if (consumerTransactionAmount.getInrcvNonMgiFeeAmt() != null) {
                receiveFeeAmount.setValue(consumerTransactionAmount.getInrcvNonMgiFeeAmt());
            }
            receiveFeeAmount.setCurrencyCode(receiveCurrencyCode);
            receiveFeeAmount.setPayoutCurrencyCode(estimatedCurrencyCode);
            transactionAmount.setReceiveFee(receiveFeeAmount);
            transactionAmount.setRecvFeeEstimated(TransactionProxyImpl.DB_FLAG_TRUE.equals(consumerTransaction
                .getInrcvNonMgiFeeEstmFlag()));

            Amount receiveTaxesAmount = new Amount();
            if (consumerTransactionAmount.getInrcvNonMgiTaxAmt() != null) {
                receiveTaxesAmount.setValue(consumerTransactionAmount.getInrcvNonMgiTaxAmt());
            }
            receiveTaxesAmount.setCurrencyCode(receiveCurrencyCode);
            receiveTaxesAmount.setPayoutCurrencyCode(estimatedCurrencyCode);
            transactionAmount.setReceiveTaxes(receiveTaxesAmount);

            transactionAmount.setRecvTaxEstimated(TransactionProxyImpl.DB_FLAG_TRUE.equals(consumerTransaction
                .getInrcvNonMgiTaxEstmFlag()));

            transactionAmount.setIndicativeCountry(TransactionProxyImpl.DB_FLAG_TRUE.equals(consumerTransaction
                .getInrcvIndCntryFlag()));

            Amount rcvTxFrAmount = new Amount();
            rcvTxFrAmount.setValue(consumerTransactionAmount.getInrcvFaceAmt());
            rcvTxFrAmount.setCurrencyCode(receiveCurrencyCode);
            rcvTxFrAmount.setPayoutCurrencyCode(estimatedCurrencyCode);
            transactionAmount.setRcvTxFrAmount(rcvTxFrAmount);

            Amount sendTaxesAmount = new Amount();
            sendTaxesAmount.setValue(new BigDecimal(0));
            sendTaxesAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setSendTaxes(sendTaxesAmount);

            Amount exchangeRateApplied = new Amount();
            exchangeRateApplied.setValue(consumerTransactionAmount.getExchangeRateApplied());
            exchangeRateApplied.setCurrencyCode(receiveCurrencyCode);
            exchangeRateApplied.setPayoutCurrencyCode(estimatedCurrencyCode);
            transactionAmount.setExchangeRateApplied(exchangeRateApplied);

            transactionDetails.setDisclaimerTxtENG(consumerTransaction.getTranDsclsrTextEng());
            transactionDetails.setDisclaimerTxtSPA(consumerTransaction.getTranDsclsrTextSpa());

            Amount rewardsDiscountAmount = new Amount();
            BigDecimal nonDiscountedFee = consumerTransactionAmount.getNonDiscountedFee();
            if (nonDiscountedFee != null) {
                rewardsDiscountAmount.setValue(nonDiscountedFee.subtract(serviceFee));
            } else {
                rewardsDiscountAmount.setValue(BigDecimal.valueOf(0.00));
            }
            rewardsDiscountAmount.setCurrencyCode(sendCurrenyCode);
            transactionAmount.setRewardsDiscount(rewardsDiscountAmount);
            
//            Amount receiveAmount = new Amount();
//            receiveAmount.setValue(consumerTransactionAmount.getFaceAmount());
//            String receiveCurrencyCode = consumerTransaction.getReceiveCurrency();
//            receiveAmount.setCurrencyCode(receiveCurrencyCode);
//            BigDecimal recAmount = receiveAmount.getValue();
//            BigDecimal excRate = exchangeRateAmount.getValue();
//            receiveAmount.setValue(recAmount.multiply(excRate));
//            transactionAmount.setReceiveAmount(receiveAmount);
//
//            Amount sendAmount = new Amount();
//            sendAmount.setValue(consumerTransactionAmount.getFaceAmount());
//            sendAmount.setCurrencyCode(sendCurrenyCode);
//            transactionAmount.setSendAmount(sendAmount);
//
//            BigDecimal serviceFee = consumerTransactionAmount.getSendFee();
//            Amount serviceFeeAmount = new Amount();
//            serviceFeeAmount.setValue(serviceFee);
//            serviceFeeAmount.setCurrencyCode(sendCurrenyCode);
//            transactionAmount.setServiceFee(serviceFeeAmount);
//
//            Amount totalAmount = new Amount();
//            totalAmount.setValue(consumerTransactionAmount.getTotalAmount());
//            totalAmount.setCurrencyCode(sendCurrenyCode);
//            transactionAmount.setTotalAmount(totalAmount);
//
//            Amount rewardsDiscountAmount = new Amount();
//            BigDecimal nonDiscountedFee = consumerTransactionAmount.getNonDiscountedFee();
//            if (nonDiscountedFee != null) {
//                rewardsDiscountAmount.setValue(nonDiscountedFee.subtract(serviceFee));
//            } else {
//                rewardsDiscountAmount.setValue(BigDecimal.valueOf(0.00));
//            }
//            rewardsDiscountAmount.setCurrencyCode(sendCurrenyCode);
//            transactionAmount.setRewardsDiscount(rewardsDiscountAmount);
//            
//            transactionDetails.setDisclaimerTxtENG(consumerTransaction.getTranDsclsrTextEng());
//            transactionDetails.setDisclaimerTxtSPA(consumerTransaction.getTranDsclsrTextSpa());
            
         // POPULATE TRANSACTION DATES (LOG)
            TransactionLog transactionLog = transactionDetails.getTransactionLog();
            Calendar dateInitiated = mgoTransactionProcessing.getSubmittedDate();
            transactionLog.setDateInitiated(dateInitiated.getTime());

            if (mgoTransactionProcessing.getSubmittedDate() != null) {
                if (!(dateInitiated.getTime().equals(mgoTransactionProcessing.getSendDate().getTime()))) {
                    transactionLog.setDateApproved(mgoTransactionProcessing.getSendDate().getTime());
                }
            }
            if (mgoTransactionProcessing.getReceivedDate() != null) {
                transactionLog.setDateCompleted(mgoTransactionProcessing.getReceivedDate().getTime());
            }

            String timeExpirationGapInDays = mgoResourceConfig.getAttributeValue(MGOResourceConfig.TIME_EXPIRATION_GAP_IN_DAYS); 
            dateInitiated.add(Calendar.DAY_OF_MONTH, Integer.parseInt(timeExpirationGapInDays));
            transactionLog.setDateExpires(dateInitiated.getTime());

            if (transactionDetails.getTransactionType().equalsIgnoreCase("Bill Pay")) {
                if ((transactionDetails.getStatus()).equals(MGOStatusCodes.SENT)) {
                    transactionDetails.setDisplayStatus(MGOUtil.computeDisplayStatus(
                        MGOStatusCodes.PAYMENTCOMPLETE_BILLPAY, transactionDetails.getTransactionLog()
                            .getDateCompleted(), transactionDetails.getReceiveOptionID()));
                } else {
                    transactionDetails.setDisplayStatus(MGOUtil.computeDisplayStatus(transactionDetails.getStatus(),
                        transactionDetails.getTransactionLog().getDateCompleted(),
                        transactionDetails.getReceiveOptionID()));
                }
            } else {
                transactionDetails
                    .setDisplayStatus(MGOUtil.computeDisplayStatus(transactionDetails.getStatus(), transactionDetails
                        .getTransactionLog().getDateCompleted(), transactionDetails.getReceiveOptionID()));
            }

            if (consumerTransaction.getInrcvAvlToRcvLclDate() != null) {
                transactionLog.setDateAvailability(consumerTransaction.getInrcvAvlToRcvLclDate().getTime());
            }
            
//            // POPULATE TRANSACTION DATES (LOG)
//            TransactionLog transactionLog = transactionDetails.getTransactionLog();
//            Calendar dateInitiated = mgoTransactionProcessing.getSubmittedDate();
//            transactionLog.setDateInitiated(dateInitiated.getTime());
//
//            if (mgoTransactionProcessing.getSubmittedDate() != null) {
//                if (!(dateInitiated.getTime().equals(mgoTransactionProcessing.getSendDate().getTime()))) {
//                    transactionLog.setDateApproved(mgoTransactionProcessing.getSendDate().getTime());
//                }
//            }
//            if (mgoTransactionProcessing.getReceivedDate() != null) {
//                transactionLog.setDateCompleted(mgoTransactionProcessing.getReceivedDate().getTime());
//            }
//
//            String timeExpirationGapInDays = mgoResourceConfig.getAttributeValue(MGOResourceConfig.TIME_EXPIRATION_GAP_IN_DAYS); 
//            dateInitiated.add(Calendar.DAY_OF_MONTH, Integer.parseInt(timeExpirationGapInDays));
//            transactionLog.setDateExpires(dateInitiated.getTime());
//
//            if (transactionDetails.getTransactionType().equalsIgnoreCase(BILLPAY)) {
//                if ((transactionDetails.getStatus()).equals(MGOStatusCodes.SENT)) {
//                    transactionDetails.setDisplayStatus(MGOUtil.computeDisplayStatus(
//                        MGOStatusCodes.PAYMENTCOMPLETE_BILLPAY, transactionDetails.getTransactionLog()
//                            .getDateCompleted(), transactionDetails.getReceiveOptionID()));
//                } else {
//                    transactionDetails.setDisplayStatus(MGOUtil.computeDisplayStatus(transactionDetails.getStatus(),
//                        transactionDetails.getTransactionLog().getDateCompleted(),
//                        transactionDetails.getReceiveOptionID()));
//                }
//            } else {
//                transactionDetails
//                    .setDisplayStatus(MGOUtil.computeDisplayStatus(transactionDetails.getStatus(), transactionDetails
//                        .getTransactionLog().getDateCompleted(), transactionDetails.getReceiveOptionID()));
//            }

            
            // POPULATE RECEIVER
            com.moneygram.mgo.service.transaction_v1.Receiver transactionReceiver = consumerTransaction.getReceiver();
            ReceiverConsumerName receiverConsumerName = transactionReceiver.getReceiverName();
            ReceiverConsumerAddress receiverConsumerAddress = transactionReceiver.getReceiverAddress();
            Receiver receiver = new Receiver();
            Name receiverName = receiver.getName();
            receiverName.setFirstName(receiverConsumerName.getFirstName());
            receiverName.setMiddleName(receiverConsumerName.getMiddleName());
            receiverName.setLastName(receiverConsumerName.getLastName());
            receiverName.setSecondLastName(receiverConsumerName.getMaternalName());

            receiver.setReceiveOption(
                consumerTransaction.getDeliveryOption() + ":" + consumerTransaction.getReceiveCurrency() + ":"
                        + receiveAgentID);

            receiver.setRRNNumber(consumerTransaction.getReceiverRegistrationNumber());

            receiver.setAccountNumber(consumerTransaction.getReceiver().getReceiverConsumerAccountNbr());

            transactionDetails.setReceiveCountry(consumerTransaction.getReceiveCountry());
            transactionDetails.setReceiveCurrency(consumerTransaction.getReceiveCurrency());
            
            Messages receiverMessages = receiver.getMessages();
            receiverMessages.setMessage1(consumerTransaction.getMessageField1());
            receiverMessages.setMessage2(consumerTransaction.getMessageField2());

            Address receiverAddress = receiver.getAddress();
            receiverAddress.setAddress1(receiverConsumerAddress.getLine1());
            receiverAddress.setAddress2(receiverConsumerAddress.getLine2());
            receiverAddress.setAddress3(receiverConsumerAddress.getLine3());
            receiverAddress.setCity(receiverConsumerAddress.getCity());

            String stateCode = receiverConsumerAddress.getState();
            //vx04: moved to BusinessCore
            /*if (stateCode != null) {
                // TODO The next 3 lines can be safely deleted once all trxns in db that have state = 'PR' has also
                // country = 'PRI'
                if (stateCode.equals("PR")) {
                    receiverAddress.getState().setName(paramLoader.getStateByCode(stateCode, "PRI").getName());
                } else {
                    receiverAddress.getState().setName(
                        paramLoader.getStateByCode(stateCode, receiverConsumerAddress.getCountry()).getName());
                }
            }*/
            receiverAddress.getState().setCode(stateCode);
            String MGODE_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGODE);
			String MGOUK_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUK);			
			if (MGODE_siteId.equalsIgnoreCase(consumerTransaction.getSourceSite()) || MGOUK_siteId
					.equalsIgnoreCase(consumerTransaction.getSourceSite())) {
				receiverAddress.getState().setName(
                        paramsLoader.getStateByCode(stateCode, receiverConsumerAddress.getCountry()).getName());                						
			}
            String country = null;
            if (receiverConsumerAddress.getCountry() != null) {
                country = receiverConsumerAddress.getCountry();
            } else {
                // Older txns (pre-MGO) have country in different column
                country = consumerTransaction.getReceiveCountry();
            }
            
            //Vx04: Moved to BusinessCore
            //receiverAddress.setCountry(paramLoader.getCountryByCode(country));
            Country receiveCountry = new Country();
            receiveCountry.setCode(country);
            receiverAddress.setCountry(receiveCountry);
            receiver.setAddress(receiverAddress);

            Delivery receiverDelivery = receiver.getDelivery();
            receiverDelivery.setInstruction1(receiverConsumerAddress.getDirection1());
            receiverDelivery.setInstruction2(receiverConsumerAddress.getDirection2());
            receiverDelivery.setInstruction3(receiverConsumerAddress.getDirection3());

            receiver.getPhone().setPhoneNumber(transactionReceiver.getReceiverPhone());

            // FIXME vi79 Not found property internetPurchaseFlag
            // Boolean inetPurchFlag = consumerTransaction.getInternetPurchaseFlag();
            // if(inetPurchFlag != null && inetPurchFlag.booleanValue())
            // {
            // transactionDetails.getSender().setInetPurchaseFlag(MGOConstants.YES);
            // }
            // else
            // {
            // transactionDetails.getSender().setInetPurchaseFlag(MGOConstants.NO);
            // }

            receiver.getSelectedBiller().setBillerName(consumerTransaction.getReceiveAgentName());
            receiver.getSelectedBiller().setBillerCode(consumerTransaction.getReceiveAgentCode());
            receiver.getSelectedBiller().setAccountNumber(transactionReceiver.getReceiverConsumerAccountNbr());
            transactionDetails.setBillerServiceLevel(consumerTransaction.getBillerInfoText());
            transactionDetails.setBillerEndOfDayText(consumerTransaction.getBillerEndOfDayTimeText());
            transactionDetails.setBillerNotes(consumerTransaction.getBillerPostTimeframeText());

            transactionDetails.setReceiver(receiver);

            // S29a
            if ((consumerTransaction.getThreeMinuteFreePhoneNumber() != null)
                    && !"".equals(consumerTransaction.getThreeMinuteFreePhoneNumber())) {
                if ((consumerTransaction.getThreeMinuteFreePinNumber() != null)
                        && !"".equals(consumerTransaction.getThreeMinuteFreePinNumber())) {
                    transactionDetails.setThreeMinuteFreePhoneNumber(consumerTransaction
                        .getThreeMinuteFreePhoneNumber());
                    transactionDetails.setThreeMinuteFreePinNumber(consumerTransaction.getThreeMinuteFreePinNumber());
                } else {
                    transactionDetails.setThreeMinuteFreePhoneNumber("");
                    transactionDetails.setThreeMinuteFreePinNumber("");
                }
            } else {
                transactionDetails.setThreeMinuteFreePhoneNumber("");
                transactionDetails.setThreeMinuteFreePinNumber("");
            }
            // S29aEnd

            // POPULATE ACCOUNT ID USED
            Long fiAccountId = consumerTransaction.getSender().getFiAccountId();
            if (fiAccountId != null) {
                transactionDetails.setAccountId(fiAccountId.toString());
            }
            
            // INTERNATIONALIZATION
            transactionDetails.setService(getServiceName(transactionDetails.getTransactionTypeCode(), consumerTransaction.getSourceSite()));
            
        } catch (Exception e) {
            logger.error("Error in getTransaction: "+e.getMessage(),e);
            throw e;
        }
        return transactionDetails;
    }
    
    private String getServiceName(String mgoProductType , String siteId) {
        String serviceName = "";
        String mgoDeSiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGODE);
        if(mgoDeSiteId.equalsIgnoreCase(siteId)){
        	if (mgoProductType.equals(TransactionProxyImpl.MGSEND)) {
                serviceName = TransactionProxyImpl.DE_SAMEDAY;
            } else if (mgoProductType.equals(TransactionProxyImpl.MGCASH)) {
                serviceName = TransactionProxyImpl.DE_CASH;
            } else {
                serviceName = TransactionProxyImpl.DE_ECONOMY;
            }
        }
        else{
        	if (mgoProductType.equals(TransactionProxyImpl.MGSEND)) {
                serviceName = TransactionProxyImpl.SAMEDAY;
            } else if (mgoProductType.equals(TransactionProxyImpl.MGCASH)) {
                serviceName = TransactionProxyImpl.CASH;
            } else {
                serviceName = TransactionProxyImpl.ECONOMY;
            }
        }
        
        return serviceName;
    }
    
    private String getServiceName(String mgoProductType) {
        String serviceName = "";
        if (mgoProductType.equals(TransactionProxyImpl.MGSEND)) {
            serviceName = TransactionProxyImpl.SAMEDAY;
        } else if (mgoProductType.equals(TransactionProxyImpl.MGCASH)) {
            serviceName = TransactionProxyImpl.CASH;
        } else {
            serviceName = TransactionProxyImpl.ECONOMY;
        }
        return serviceName;
    }
    
    private String getReceiptTransactionType(String partnerSiteID, String mgoProductType) {
        String transactionType = "";
        String siteIdWap = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_WAP);
        if (siteIdWap.equals(partnerSiteID)) {
            if (mgoProductType.equals(TransactionProxyImpl.MGSEND)) {
                transactionType = TransactionProxyImpl.AFF_SAME_DAY;
            } else if (mgoProductType.equals(TransactionProxyImpl.DSSEND)) {
                transactionType = TransactionProxyImpl.AFF_DELAY_SEND;
            }
        } else {
            return getReceiptTransactionType(mgoProductType);
        }
        return transactionType;
    }
    
    private String getReceiptTransactionType(String mgoProductType) {
        String transactionType = "";
        if (mgoProductType.equals(TransactionProxyImpl.MGSEND)) {
            transactionType = TransactionProxyImpl.SAMEDAY;
        } else if (mgoProductType.equals(TransactionProxyImpl.ESSEND)) {
            transactionType = TransactionProxyImpl.ECONOMY;
        } else if (mgoProductType.equals(TransactionProxyImpl.EPSEND)) {
            transactionType = TransactionProxyImpl.BILLPAY;
        }
        return transactionType;
    }
    
    private String getTransactionType(String mgoProductType) {
        String transactionType = "";
        if (mgoProductType.equals(TransactionProxyImpl.MGSEND) || mgoProductType.equals(TransactionProxyImpl.ESSEND)
                || mgoProductType.equals(TransactionProxyImpl.MGCASH)) {
            transactionType = TransactionProxyImpl.MONEYTRANSFER;
        } else if (mgoProductType.equals(TransactionProxyImpl.EPSEND)
                || mgoProductType.equals(TransactionProxyImpl.EPCASH)) {
            transactionType = TransactionProxyImpl.BILLPAY;
        }
        return transactionType;
    }


}
