package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;


public class TelecheckData
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8242831768231347713L;
	// Telecheck parameters
    private Integer tCProviderCode;
    private String tCProviderTransactionNumber;
    private String tCInternalTransactionNumber;

    public Integer gettCProviderCode() {
        return tCProviderCode;
    }

    public void settCProviderCode(Integer tCProviderCode) {
        this.tCProviderCode = tCProviderCode;
    }

    public String gettCProviderTransactionNumber() {
        return tCProviderTransactionNumber;
    }

    public void settCProviderTransactionNumber(String tCProviderTransactionNumber) {
        this.tCProviderTransactionNumber = tCProviderTransactionNumber;
    }

    public String gettCInternalTransactionNumber() {
        return tCInternalTransactionNumber;
    }

    public void settCInternalTransactionNumber(String tCInternalTransactionNumber) {
        this.tCInternalTransactionNumber = tCInternalTransactionNumber;
    }

}
