package com.moneygram.mgo.service.asynctransactionprocessing.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer.Consumer_v2Proxy;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.notification.NotificationServiceProxy;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.transaction.TransactionProxy;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;

/**
 * This class will handle all the logic for generating the receipt and then sending the success notification email to the user
 * 
 * @author vl58
 */
@Component
@DependsOn("mgoResourceConfig")
public class SuccessNotificationEmailHandler {

	
	@Autowired
	@Qualifier("receiptGeneratorUS")
	private ReceiptGenerator receiptGeneratorUS;
	
	@Autowired
	@Qualifier("receiptGeneratorInternational")
	private ReceiptGenerator receiptGeneratorInternational;
	
	@Autowired
	protected MGOResourceConfig mgoResourceConfig;
	
	@Autowired
	@Qualifier("notificationServiceProxy")
	protected NotificationServiceProxy notificationServiceProxy;
	
	@Autowired
	@Qualifier("transactionServiceProxy")
	private TransactionProxy transactionServiceProxy;
	
	@Autowired
	@Qualifier("consumerServicev2_Proxy")
	private Consumer_v2Proxy consumerServicev2_Proxy;
	
	private static final Logger logger = LoggerFactory.getLogger(SuccessNotificationEmailHandler.class);
	
	
	public void handleSuccessNotificationEmail(Long transactionId, String userLoginId,
			String transactionLanguageCode, String partnerSiteId) throws Exception{
		try{
			String htmlReceipt = null;
			logger.debug("[handleSuccessNotificationEmail] partner site id = "+partnerSiteId);
			String MGODE_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGODE);
			String MGOUK_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUK);
			
			//Load the Sender And Transaction Data
			TransactionDetails transactionDetails = transactionServiceProxy.getTransaction(String.valueOf(transactionId));
			Sender sender = consumerServicev2_Proxy.getConsumerProfile(userLoginId, partnerSiteId);
			
			if(!(MGODE_siteId.equalsIgnoreCase(partnerSiteId) || MGOUK_siteId.equalsIgnoreCase(partnerSiteId))){
				htmlReceipt = receiptGeneratorUS.generateReceipt(transactionDetails, sender, transactionLanguageCode, partnerSiteId);
			}			
			logger.debug("[handleSuccessNotificationEmail]receipData = "+htmlReceipt);
			notificationServiceProxy.notifySentTransaction(transactionDetails, partnerSiteId, sender.getName(), userLoginId, sender.getPreflanguage(), htmlReceipt);			
			
		}catch(Exception ex){
			logger.error("Error in handleSuccessNotificationEmail: "+ex.getMessage());
			throw ex;
		}
		
	}
}
