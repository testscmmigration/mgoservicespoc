package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;


public class Name
        implements Remote, Serializable {
    private static final long serialVersionUID = -4031910400099419003L;

    private String title = new String();
    private String firstName = new String();
    private String middleName = new String();
    private String lastName = new String();
    private String secondLastName = new String();

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public String getNameKey() {
        return getNameKey("", true);
    }

    @Override
    public String toString() {
        StringBuilder nameToString = new StringBuilder("");
        if (firstName != null) {
            nameToString.append(firstName.trim() + " ");
        }
        if (middleName != null) {
            nameToString.append(middleName.trim() + " ");
        }
        if (lastName != null) {
            nameToString.append(lastName.trim() + " ");
        }
        if (secondLastName != null) {
            nameToString.append(secondLastName.trim());
        }

        return nameToString.toString();
    }

    public String getNameKey(String separator, boolean upperCase) {
        StringBuffer stringBuffer = new StringBuffer();
        if (firstName != null) {
            stringBuffer.append(firstName);
        }

        if (middleName != null) {
            stringBuffer.append(separator).append(middleName);
        }

        if (lastName != null) {
            stringBuffer.append(separator).append(lastName);
        }

        if (secondLastName != null) {
            stringBuffer.append(separator).append(secondLastName);
        }

        String nameKey = stringBuffer.toString();
        if (upperCase) {
            nameKey = nameKey.toUpperCase();
        }

        return nameKey;
    }
    
    public String getComparableValue(){
    	StringBuffer buffer = new StringBuffer();
    	String[] nameValues = {firstName, middleName, lastName, secondLastName};
    	for(String name : nameValues){
    		if(name != null && !name.trim().equals("")){
    			buffer.append(name.trim() + " ");
    		}
    	}
    	return buffer.toString().trim();
    }
}