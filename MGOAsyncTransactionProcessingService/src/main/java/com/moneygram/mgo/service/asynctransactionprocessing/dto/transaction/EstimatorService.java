package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.math.BigInteger;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class EstimatorService
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private String serviceCode;
    private String serviceName;
    private String currencyCode;
    private Currency currency;
    private String receiveAgentID = "";
    private String receiveAgentName;
    private BigInteger serviceID;
    private String mgManaged;
    private String agentManaged;
    private String validationExprs;
    private String checkDigitAlg;
    private boolean fullyQualifiedDelivaryOption = false;
    private String serviceLabel;
    private boolean availableForMgo = false;

    public String getReceiveAgentName() {
        return receiveAgentName;
    }

    public void setReceiveAgentName(String receiveAgentName) {
        this.receiveAgentName = receiveAgentName;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getServiceLabel() {
        return serviceLabel;
    }

    public void setServiceLabel(String serviceLabel) {
        this.serviceLabel = serviceLabel;
    }

    public String getMgManaged() {
        return mgManaged;
    }

    public void setMgManaged(String mgManaged) {
        this.mgManaged = mgManaged;
    }

    public String getAgentManaged() {
        return agentManaged;
    }

    public void setAgentManaged(String agentManaged) {
        this.agentManaged = agentManaged;
    }

    public String getValidationExprs() {
        return validationExprs;
    }

    public void setValidationExprs(String validationExprs) {
        this.validationExprs = validationExprs;
    }

    public String getCheckDigitAlg() {
        return checkDigitAlg;
    }

    public void setCheckDigitAlg(String checkDigitAlg) {
        this.checkDigitAlg = checkDigitAlg;
    }

    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    public void setReceiveAgentID(String receiveAgentID) {
        this.receiveAgentID = receiveAgentID;
    }

    public BigInteger getServiceID() {
        return serviceID;
    }

    public void setServiceID(BigInteger serviceID) {
        this.serviceID = serviceID;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public boolean isFullyQualifiedDelivaryOption() {
        return fullyQualifiedDelivaryOption;
    }

    public void setFullyQualifiedDelivaryOption(boolean fullyQualifiedDelivaryOption) {
        this.fullyQualifiedDelivaryOption = fullyQualifiedDelivaryOption;
    }

    public boolean isAvailableForMgo() {
        return availableForMgo;
    }

    public void setAvailableForMgo(boolean availableForMgo) {
        this.availableForMgo = availableForMgo;
    }
}
