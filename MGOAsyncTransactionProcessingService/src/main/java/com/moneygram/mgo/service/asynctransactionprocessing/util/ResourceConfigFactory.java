package com.moneygram.mgo.service.asynctransactionprocessing.util;


import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.ree.lib.Config;


public class ResourceConfigFactory {
    private ApplicationContext context = null;

    public ResourceConfigFactory() {
    }

    public ResourceConfigFactory(String contextLocation) {
        context = (ApplicationContext) new ClassPathXmlApplicationContext(contextLocation);
    }

    public Config createResourceConfiguration(String jndi) {
        Config config;
        try {
            // So we can run the unit tests
            config = (Config) (context != null ? context.getBean("testConfig") : new InitialContext().lookup(jndi));
        } catch (NamingException e) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi, e);
        }

        if (config == null) {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi
                    + ". Configuration can not be null.");
        }

        return config;
    }
}
