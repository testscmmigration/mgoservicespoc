/**
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;
import java.util.ArrayList;
import java.util.List;


public class Sender
        implements Remote, Serializable, Cloneable {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    public static final String PASSPORT = "SP";
    public static final String DRIVER_LICENSE = "ECDL";
    private Long consumerCode = new Long(-1);
    private Name name = new Name();
    private Address address = new Address();
    private Phone primaryPhone = new Phone();
    private Phone alternatePhone = new Phone();
    
	
	private String preflanguage;
 
 
  
    private String socialSecurityNumber;
    private List<PaymentMethod> financialAccounts = new ArrayList<PaymentMethod>();


    public Long getConsumerCode() {
        return consumerCode;
    }

    public void setConsumerCode(Long consumerCode) {
        this.consumerCode = consumerCode;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

   

    public Phone getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(Phone primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

   

    /**
     * @return the alternatePhone
     */
    public Phone getAlternatePhone() {
        return alternatePhone;
    }

    /**
     * @param alternatePhone
     *            the alternatePhone to set
     */
    public void setAlternatePhone(Phone alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    /**
     * The default protected clone does a shallow copy,
     * that is, it does not make copies of objects pointed to by the object,
     * but it copies all fields and references in an object.
     */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

  
    public List<PaymentMethod> getFinancialAccounts() {
        return financialAccounts;
    }

    public void setFinancialAccounts(List<PaymentMethod> financialAccounts) {
        this.financialAccounts = financialAccounts;
    }

    

	

    public boolean hasAccountCode(String accountCode) {
        List<PaymentMethod> pmList = (List<PaymentMethod>) this.getFinancialAccounts();
        if (pmList == null || pmList.size() < 1 || accountCode == null) {
            return false;
        }
        for (PaymentMethod pm : pmList) {
            if (accountCode.equals(pm.getAccountCode())) {
                return true;
            }
        }
        return false;
    }

	public String getPreflanguage() {
		return preflanguage;
	}

	public void setPreflanguage(String preflanguage) {
		this.preflanguage = preflanguage;
	}

	@Override
	public String toString() {
		return "Sender [consumerCode=" + consumerCode + ", name=" + name
				+ ", address=" + address + ", primaryPhone=" + primaryPhone
				+ ", alternatePhone=" + alternatePhone
				+ ", socialSecurityNumber=" + socialSecurityNumber
				+ ", financialAccounts=" + financialAccounts + "]";
	}

    
}
