package com.moneygram.mgo.service.asynctransactionprocessing.business;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.PaymentMethod;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.parametersloader.ParametersLoader;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOUtil;

public abstract class ReceiptGeneratorBase{
	
	private static final Logger log = LoggerFactory.getLogger(ReceiptGeneratorBase.class);

    @Autowired
    protected ParametersLoader paramsLoader;


    


	protected TransactionDetails setCountryAndStateInfo(TransactionDetails transaction, Sender sender) throws Exception{
    	String stateCode = transaction.getReceiver().getAddress().getState().getCode();
    	if (stateCode != null) {
        // TODO The next 3 lines can be safely deleted once all trxns in db that have state = 'PR' has also
        // country = 'PRI'
        if (stateCode.equals("PR")) {
        	transaction.getReceiver().getAddress().getState().setName(paramsLoader.getStateByCode(stateCode, "PRI").getName());
        } else {
        	transaction.getReceiver().getAddress().getState().setName(
                paramsLoader.getStateByCode(stateCode, transaction.getReceiver().getAddress().getCountry().getCode()).getName());
        	}
    	}
    	transaction.getReceiver().getAddress().getState().setCode(stateCode);
    	
    	if(sender != null && sender.getAddress().getCountry() != null){
    		String senderCountryCode = sender.getAddress().getCountry().getCode();
    		Country senderCountry = paramsLoader.getCountryByCode(senderCountryCode);
    		if(senderCountry != null){
    			sender.getAddress().setCountry(senderCountry);
    		}
    	}
    	
    	transaction.getReceiver().getAddress().setCountry(paramsLoader.getCountryByCode(transaction.getReceiver().getAddress().getCountry().getCode()));
    	
		return transaction;
	}
	
	protected void setPaymentMethodsToTranDetails(Sender sender,
			TransactionDetails transactionDetails) throws Exception {
		List<PaymentMethod> financialAccounts = sender.getFinancialAccounts();
		 transactionDetails.setPaymentMethod( MGOUtil.getPaymentMethodDisplayString(financialAccounts, String.valueOf(transactionDetails.getFiAccountId())) );
		 MGOUtil.getPaymentMethodFillTransactionDetails(transactionDetails, financialAccounts,
				 String.valueOf(transactionDetails.getFiAccountId()));
		 
         // Prior to October 2009, txns could have primary bank & backup credit card funding
         if ((transactionDetails.getBackupFiAccountId() != null)
		       && (transactionDetails.getBackupFiAccountId() > 0)) {
		   transactionDetails.setBackupPaymentMethod(MGOUtil.getPaymentMethodDisplayString(sender.getFinancialAccounts(), String.valueOf(transactionDetails.getBackupFiAccountId())));
         }
	}
	
}
