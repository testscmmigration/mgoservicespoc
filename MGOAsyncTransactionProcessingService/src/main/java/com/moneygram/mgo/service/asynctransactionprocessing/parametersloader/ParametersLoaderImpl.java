/**
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.parametersloader;


import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.moneygram.common.cache.CacheException;
import com.moneygram.mgo.service.asynctransactionprocessing.cache.CacheService;
import com.moneygram.mgo.service.asynctransactionprocessing.cache.CodeTablesCache;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator.AgentLocatorService;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;



/**
 * @author vd51 this class will be on charge of load any information that will be need it from the caches
 */
@Component(value = "generalParametersLoader")
public class ParametersLoaderImpl implements ParametersLoader {

    private static final Logger log = Logger.getLogger(ParametersLoaderImpl.class);


    @Autowired
    private CacheService<? extends Serializable> cacheService;

    @Autowired
    private MGOResourceConfig config;

    @Autowired
    AgentLocatorService agentLocatorServiceImpl;

    @Override
    public Country getCountryByCode(String countryCode) {
        Country country = new Country();
        
        try {
            @SuppressWarnings("unchecked")
            Map<String, Country> mgoCacheCollection = (Map<String, Country>) cacheService.getMGOCacheCollection(CodeTablesCache.CODE_TABLES_CACHE, 
            		CodeTablesCache.CODE_TABLES_CACHE_ELEMENT,
                CodeTablesCache.COUNTRY_MAP_KEY);
            country = mgoCacheCollection.get(countryCode);
        } catch (CacheException e) {
            log.error("Error retrieving country from cache: " + e.getMessage());
        }

        return country;
    }
    
    @Override
    public State getStateByCode(String stateCode, String countryCode) {
        State state = new State();

        if (countryCode != null) {
            if (countryCode.equalsIgnoreCase("USA")) {
                countryCode = "US";
            } else if (countryCode.equalsIgnoreCase("MEX")) {
                countryCode = "MX";
            } else if (countryCode.equalsIgnoreCase("CAN")) {
                countryCode = "CD";
            } else if (countryCode.equalsIgnoreCase("PRI")) {
                countryCode = "PR";
            }
        }
        try {
            Map<String, State> stateMap = getStates(countryCode);
            if (stateMap != null) {
                state = stateMap.get(stateCode);
            }
        } catch (Exception e) {
            log.error("Error retrieving state from cache: " + e.getMessage());
//            e.printStackTrace();
        }
        return state;
    }
    
    /**
     * retrieve the list of states
     * 
     * @param country
     *            , country code of the country requested
     * @return list of states for the country requested
     */
    @Override
    public Map<String, State> getStates(String country)  {
    	Map<String, State> stateList = null;
    	try {
            stateList = agentLocatorServiceImpl.getStates(country);
            return stateList;
        } catch (Exception e) {
        	log.error("Parameters Loader FAILED to retrieve states list for the country " + country +":"+e.getMessage());
           // throw new Exception("Parameters Loader FAILED to retrieve states list for the country " + country, e);
        }
    	return stateList;
    }


}
