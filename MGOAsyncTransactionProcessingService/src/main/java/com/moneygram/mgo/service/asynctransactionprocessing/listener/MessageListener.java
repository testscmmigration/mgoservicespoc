package com.moneygram.mgo.service.asynctransactionprocessing.listener;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.moneygram.mgo.service.asynctransactionprocessing.business.MGOAsyncTransactionProcessingService;
import com.moneygram.mgo.service.asynctransactionprocessing.exception.MGOAsyncTransactionProcessingServiceException;
import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;



/**
 * A service that sends and receives JMS messages. 
 * 
 */
@Service
public class MessageListener
{
	
	private static final Logger logger = LoggerFactory.getLogger(MessageListener.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private MGOAsyncTransactionProcessingService successNotificationService;


	/**
	 * Receives a message from a queue.
	 * 
	 * 
	 * */
	public void readMessage(Map<String, Object> message) throws Exception {
		try{
			Long transactionId = (Long) message.get("transactionId");
			String userLoginId = (String) message.get("userLoginId");
			String transactionLanguageCode = (String) message.get("transactionLanguageCode");
			String partnerSiteId = (String) message.get("partnerSiteId");
			logger.debug("[readMessage] Message received, start processing...");
			logger.debug("[readMessage] transactionId = "+ transactionId);
			logger.debug("[readMessage] userLoginId = "+ userLoginId);
			logger.debug("[readMessage] transactionLanguageCode = "+ transactionLanguageCode);
			logger.debug("[readMessage] partnerSiteId = "+ partnerSiteId);
			if (transactionId == null) {
				throw new MGOAsyncTransactionProcessingServiceException("transactionId element is missing");
			}
			if(transactionId <= 0){
				throw new MGOAsyncTransactionProcessingServiceException("transactionId element type invalid");
			}
			if (StringHelper.isNullOrEmpty(userLoginId)) {
				throw new MGOAsyncTransactionProcessingServiceException("userLoginId element is missing");
			}
			if (StringHelper.isNullOrEmpty(partnerSiteId)) {
				throw new MGOAsyncTransactionProcessingServiceException("partnerSiteId element is missing");
			}
			boolean notificationSent = successNotificationService.sendSuccessNotificationEmail(transactionId, userLoginId, transactionLanguageCode, partnerSiteId);
			logger.debug("[readMessage] notificationSent = "+ notificationSent);
		}catch(MGOAsyncTransactionProcessingServiceException ex){
			logger.error("[readMessage] Error in readMessage: "+ex.getMessage(),ex);
		}
		catch(Exception ex){
			logger.error("[readMessage] Error in readMessage: "+ex.getMessage(),ex);
		}
	}
	
//	public void readMessage(Map<String, Object> message) {
//		try{
//
//			String text = (String) message.get("text");
//			logger.debug("[readMessage] Message received = "+text);
//			
//		
//		}catch(Exception ex){
//			logger.error("[readMessage] Error in readMessage: "+ex.getMessage(),ex);
//		}
//	}
}
