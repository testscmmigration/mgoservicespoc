/*
 * Created on Feb 17, 2006
 */
package com.moneygram.mgo.service.asynctransactionprocessing.util;


/**
 * @author T007
 * 
 */
@SuppressWarnings("serial")
public class MessagingException
        extends Exception {

    /**
     * 
     */
    public MessagingException() {
        super();
        // Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public MessagingException(String message) {
        super(message);
        // Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public MessagingException(String message, Throwable cause) {
        super(message, cause);
        // Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public MessagingException(Throwable cause) {
        super(cause);
        // Auto-generated constructor stub
    }

}
