package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;



public class Address
        implements Remote, Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private Long addressCode = new Long(0);
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String city;
    private State state = new State();
    private String zipCode;
    private String postCode1; // US749 UK Address fields
    private String postCode2; // US749 UK Address fields
    private String county;
    private Country country = new Country();
    
    private String address1Complement; //Address 1 complement (ex: house number)
	private String address2Complement; //Address 2 complement
    private String address3Complement; //Address 3 complement
    private String address4Complement; //Address 4 complement

    public void setAddressCode(Long addressCode) {
        this.addressCode = addressCode;
    }

    public Long getAddressCode() {
        return addressCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAddress4() {
        return address4;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * @return the postCode1
     */
    public String getPostCode1() {
        return postCode1;
    }

    /**
     * @param postCode1
     *            the postCode1 to set
     */
    public void setPostCode1(String postCode1) {
        this.postCode1 = postCode1;
    }

    /**
     * @return the postCode2
     */
    public String getPostCode2() {
        return postCode2;
    }

    /**
     * @param postCode2
     *            the postCode2 to set
     */
    public void setPostCode2(String postCode2) {
        this.postCode2 = postCode2;
    }

    public String getMapZipCode() {
        if ((getZipCode() != null) && (getZipCode().length() >= 5)) {
            return getZipCode().substring(0, 5);
        }
        return null;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    /**
     * The default protected clone does a shallow copy, 
     * that is, it does not make copies of objects pointed to by the object, 
     * but it copies all fields and references in an object.
     */
    public Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return super.clone();
    }

    /**
     * @param county
     *            the county to set
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * @return the county
     */
    public String getCounty() {
        return county;
    }
    
    public String getAddress1Complement() {
		return address1Complement;
	}

	public void setAddress1Complement(String address1Complement) {
		this.address1Complement = address1Complement;
	}

	public String getAddress2Complement() {
		return address2Complement;
	}

	public void setAddress2Complement(String address2Complement) {
		this.address2Complement = address2Complement;
	}

	public String getAddress3Complement() {
		return address3Complement;
	}

	public void setAddress3Complement(String address3Complement) {
		this.address3Complement = address3Complement;
	}

	public String getAddress4Complement() {
		return address4Complement;
	}

	public void setAddress4Complement(String address4Complement) {
		this.address4Complement = address4Complement;
	}

	@Override
	public String toString() {
		return "Address [addressCode=" + addressCode + ", address1=" + address1
				+ ", address2=" + address2 + ", address3=" + address3
				+ ", address4=" + address4 + ", city=" + city + ", state="
				+ state + ", zipCode=" + zipCode + ", postCode1=" + postCode1
				+ ", postCode2=" + postCode2 + ", county=" + county
				+ ", country=" + country + ", address1Complement="
				+ address1Complement + ", address2Complement="
				+ address2Complement + ", address3Complement="
				+ address3Complement + ", address4Complement="
				+ address4Complement + "]";
	}


}
