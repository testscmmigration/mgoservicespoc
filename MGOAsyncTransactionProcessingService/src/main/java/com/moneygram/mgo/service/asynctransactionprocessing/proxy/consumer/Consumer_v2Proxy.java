package com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;

public interface Consumer_v2Proxy {

	Sender getConsumerProfile(String userLoginId, String partnerSiteId)
			throws Exception;

}
