package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;
import com.moneygram.mgo.service.locator_v3.AgentLocatorServiceProxy;
import com.moneygram.mgo.service.locator_v3.CountryListRequest;
import com.moneygram.mgo.service.locator_v3.CountryListResponse;
import com.moneygram.mgo.service.locator_v3.StateProvince;
import com.moneygram.mgo.service.locator_v3.StateProvinceListRequest;
import com.moneygram.mgo.service.locator_v3.StateProvinceListResponse;


@Component(value = "locatorServiceProxy")
public class AgentLocatorProxyImpl implements AgentLocatorProxy {

    @Autowired
    @Qualifier("locatorService")
    private AgentLocatorServiceProxy locatorService;
    

    public Map<String, State> getStates(String countryCode) throws Exception {
        try {
            StateProvinceListRequest splr = new StateProvinceListRequest();
            splr.setCountryMainFrameId(countryCode);
            splr.setFirstResultIndex(0);
            splr.setMaxResultsReturned(500);
            StateProvinceListResponse stateProvinceListResponse = locatorService.findAllStateProvinces(splr);

            if (stateProvinceListResponse == null || stateProvinceListResponse.getStateProvince() == null)
                return new HashMap<String, State>();

            StateProvince stateProvince;
            Map<String, State> states = new HashMap<String, State>();

            for (int i = 0; i < stateProvinceListResponse.getStateProvince().length; i++) {
                stateProvince = stateProvinceListResponse.getStateProvince()[i];
                if (stateProvince != null && !stateProvince.getName().startsWith("AIR")) {
                    State state = convertToState(stateProvince);
                    states.put(state.getCode(), state);
                }
            }

            return states;
        } catch (Exception e) {
        	
            throw new Exception("Call to findStates failed", e);
        }
    }

    public List<Country> getCountries() throws Exception {
        try {
            CountryListRequest countryListRequest = new CountryListRequest();
            countryListRequest.setFirstResultIndex(0);
            countryListRequest.setMaxResultsReturned(500);
            CountryListResponse countryListResponse = locatorService.findAllCountries(countryListRequest);
            if (countryListResponse == null || countryListResponse.getCountries() == null)
                return new ArrayList<Country>();
            com.moneygram.mgo.service.locator_v3.Country country;
            List<Country> countries = new ArrayList<Country>();
            for (int i = 0; i < countryListResponse.getCountries().length; i++) {
                country = countryListResponse.getCountries()[i];
                if (country != null) {
                    countries.add(convertToCountry(country));
                }
            }
            return countries;
        } catch (Exception e) {
            throw new Exception("Call to findCountries failed", e);
        }
    }
    
    private static State convertToState(StateProvince stateProvince) {
		if (stateProvince == null) {
			return null;
		}
		State state = new State();
		state.setCode(stateProvince.getCode());
		state.setName(stateProvince.getName());
		return state;
	}
    
    private static Country convertToCountry(com.moneygram.mgo.service.locator_v3.Country wsCountry) {
		if (wsCountry == null) {
			return null;
		}
		Country country = new Country();
		country.setCode(wsCountry.getMainFrameId());
		country.setName(wsCountry.getName());
		return country;
	}
    
}
