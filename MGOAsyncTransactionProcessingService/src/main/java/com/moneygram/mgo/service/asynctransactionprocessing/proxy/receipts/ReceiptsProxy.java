/**
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.proxy.receipts;

import java.util.Properties;

/**
 * @author vd51
 *
 */
public interface ReceiptsProxy {
	
	public String getReceiptForTransaction(String language, String state, String tranType, String receiptDate, Properties receiptProps,String partnerSiteId ) throws Exception;
	
}
