package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class Messages
        implements Remote, Serializable {
    private static final long serialVersionUID = 1L;
    private String message1;
    private String message2;

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public String getMessage2() {
        return message2;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
