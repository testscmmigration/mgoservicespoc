package com.moneygram.mgo.service.asynctransactionprocessing.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.moneygram.mgo.service.asynctransactionprocessing.backgroundprocessor.SuccessNotificationProcessor;
import com.moneygram.mgo.service.asynctransactionprocessing.exception.MGOAsyncTransactionProcessingServiceException;



/**
 * Implementation of {@link MGOAsyncTransactionProcessingService}, which sends a success notification (via email initially) to user when transaction is processed correctly
 * 
 * @author vl58
 */
@Service
public class MGOAsyncTransactionProcessingServiceImpl implements MGOAsyncTransactionProcessingService{

	private static final Logger logger = LoggerFactory.getLogger(MGOAsyncTransactionProcessingServiceImpl.class);
	
	@Autowired
	private SuccessNotificationProcessor successNotificationProcessor;
	
	@Override
	public boolean sendSuccessNotificationEmail(Long transactionId, String userLoginId,
			String transactionLanguageCode, String partnerSiteId) throws MGOAsyncTransactionProcessingServiceException {
		boolean notificationSent = false;
		try{
			logger.debug("[sendSuccessNotificationEmail] transactionId = "+transactionId+", userLoginId = "+userLoginId+", transactionLanguageCode = "+transactionLanguageCode+", partnerSiteId = "+partnerSiteId);
			//Invoke success notification email processor asynchronously
			successNotificationProcessor.processSuccessNotificationEmail(transactionId, userLoginId, transactionLanguageCode, partnerSiteId);
			logger.debug("[sendSuccessNotificationEmail] email success notification for  transactionId = "+transactionId+", userLoginId = "+userLoginId+", transactionLanguageCode = "+transactionLanguageCode+", partnerSiteId = "+partnerSiteId);
			notificationSent = true;
		}catch(Exception ex){
			logger.error("Error in sendSuccessNotificationEmail: "+ex.getMessage(), ex);
			throw new MGOAsyncTransactionProcessingServiceException("Failed to send success notification email",ex);
		}
		return notificationSent;
	}

}
