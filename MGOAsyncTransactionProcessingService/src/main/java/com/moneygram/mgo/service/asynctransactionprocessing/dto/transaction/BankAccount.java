package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import org.apache.commons.lang.builder.ReflectionToStringBuilder;





public class BankAccount extends PaymentMethod {

    private static final int BANK_NAME_MAX_LENGTH = 16;
    public static final String BANKACCOUNT = "bankAccount";
    private static final long serialVersionUID = 1L;
    private String bankName;
    private String routingNumber;
    private String accountNumber;
    private String accountType;
    private boolean abaBlocked = false;
    private String encryptedAccountNumber;

    private TelecheckData telecheckData;

    public BankAccount() {
        setPaymentType(BANKACCOUNT);
        setAccountType("BANK-CHK");
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public boolean isAbaBlocked() {
        return abaBlocked;
    }

    public void setAbaBlocked(boolean abaBlocked) {
        this.abaBlocked = abaBlocked;
    }

    public String getEncryptedAccountNumber() {
        return encryptedAccountNumber;
    }

    public void setEncryptedAccountNumber(String encryptedAccountNumber) {
        this.encryptedAccountNumber = encryptedAccountNumber;
    }

    public String getLast4Digits() {
        return getAccountNumber().substring((getAccountNumber().length() - 4));
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public String getDisplayString() {
        String name = getBankName();
        if (name != null && name.length() > BANK_NAME_MAX_LENGTH) {
            name = name.substring(0, BANK_NAME_MAX_LENGTH);
        }
        return name + ": **** " + getLast4Digits();
    }

    public TelecheckData getTelecheckData() {
        return telecheckData;
    }

    public void setTelecheckData(TelecheckData telecheckData) {
        this.telecheckData = telecheckData;
    }

	@Override
	public String getExpirationMonth() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExpirationYear() {
		// TODO Auto-generated method stub
		return null;
	}
	
    @Override
    public String getDisplayNumber() {
        return "**** " + getLast4Digits();
    }
}
