package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;

import java.io.Serializable;
import java.rmi.Remote;
import java.util.Date;


public class TransactionLog
        implements Remote, Serializable {
    private static final long serialVersionUID = 1L;
    private Date dateInitiated;
    private Date dateApproved;
    private Date dateCompleted;
    private Date dateExpires;
    private Date dateAvailability;

    public void setDateInitiated(Date dateInitiated) {
        this.dateInitiated = dateInitiated;
    }

    public Date getDateInitiated() {
        return dateInitiated;
    }

    public void setDateApproved(Date dateApproved) {
        this.dateApproved = dateApproved;
    }

    public Date getDateApproved() {
        return dateApproved;
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public Date getDateCompleted() {
        return dateCompleted;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    public Date getDateAvailability() {
        return dateAvailability;
    }

    public void setDateAvailability(Date dateAvailability) {
        this.dateAvailability = dateAvailability;
    }

}
