package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;
import java.util.Calendar;


public class Biller
        implements Remote, Serializable {
    private static final long serialVersionUID = 1L;
    private boolean utilityBillPayment;
    private boolean expressPayment;
    private boolean onlineBiller = false;
    private String agentId;
    private String billerName;
    private String billerCode;
    private String accountNumber;
    private Messages messages;
    private Amount recvAmountLimit;
    private Address address = new Address();
    private boolean doubleAcctNumberEntry = false;
    private BillerCutOffTime cutOffTime;
    private String billerInfoText;
    private String postingTimeFrameText;
    private String emgMerchantId;
    private String gcMerchantId;

    public boolean isDoubleAcctNumberEntry() {
        return doubleAcctNumberEntry;
    }

    public void setDoubleAcctNumberEntry(boolean doubleAcctNumberEntry) {
        this.doubleAcctNumberEntry = doubleAcctNumberEntry;
    }

    public boolean isOnlineBiller() {
        return onlineBiller;
    }

    public void setOnlineBiller(boolean onlineBiller) {
        this.onlineBiller = onlineBiller;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    public Messages getMessages() {
        return messages;
    }

    public Amount getRecvAmountLimit() {
        return recvAmountLimit;
    }

    public void setRecvAmountLimit(Amount recvAmountLimit) {
        this.recvAmountLimit = recvAmountLimit;
    }

    public boolean isUtilityBillPayment() {
        return utilityBillPayment;
    }

    public void setUtilityBillPayment(boolean utilityBillPayment) {
        this.utilityBillPayment = utilityBillPayment;
    }

    public boolean isExpressPayment() {
        return expressPayment;
    }

    public void setExpressPayment(boolean expressPayment) {
        this.expressPayment = expressPayment;
    }

    public BillerCutOffTime getCutOffTime() {
        return cutOffTime;
    }

    public void setCutOffTime(BillerCutOffTime cutOffTime) {
        this.cutOffTime = cutOffTime;
    }

    public String getBillerInfoText() {
        return billerInfoText;
    }

    public void setBillerInfoText(String billerInfoText) {
        this.billerInfoText = billerInfoText;
    }

    public String getPostingTimeFrameText() {
        return postingTimeFrameText;
    }

    public void setPostingTimeFrameText(String postingTimeFrameText) {
        this.postingTimeFrameText = postingTimeFrameText;
    }

    public String getEmgMerchantId() {
        return emgMerchantId;
    }

    public void setEmgMerchantId(String merchantId) {
        emgMerchantId = merchantId;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Biller[ agentId=").append(getAgentId());
        buffer.append(" billerName=").append(getBillerName());
        buffer.append(" billerCode=").append(getBillerCode());
        if (getRecvAmountLimit() == null || getRecvAmountLimit().getValue() == null)
            buffer.append(" recvAmountLimit=null");
        else
            buffer.append(" recvAmountLimit=").append(getRecvAmountLimit().getValue().toString());
        if (getAddress() == null || getAddress().getCity() == null)
            buffer.append(" city=null");
        else
            buffer.append(" city=").append(getAddress().getCity());
        if (getAddress() == null || getAddress().getState() == null)
            buffer.append(" state=null");
        else
            buffer.append(" state=").append(getAddress().getState());
        buffer.append(" utilityBillPayment=").append(isUtilityBillPayment());
        buffer.append(" expressPayment=").append(isExpressPayment());
        buffer.append(" onlineBiller=").append(isOnlineBiller());
        if (getCutOffTime() == null || getCutOffTime().getEndOfDayString(Calendar.getInstance()) == null) {
            buffer.append(" endOfDayString=null");
        } else {
            buffer.append(" endOfDayString=").append(getCutOffTime().getEndOfDayString(Calendar.getInstance()));
        }
        buffer.append(" billerInfoText=").append(getBillerInfoText());
        buffer.append(" postingTimeFrameText=").append(getPostingTimeFrameText());
        buffer.append(" doubleAcctNumberEntry=").append(isDoubleAcctNumberEntry());

        buffer.append(" ]");
        return buffer.toString();
    }

    public void setGcMerchantId(String gcMerchantId) {
        this.gcMerchantId = gcMerchantId;
    }

    public String getGcMerchantId() {
        return gcMerchantId;
    }
}
