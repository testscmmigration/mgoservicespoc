package com.moneygram.mgo.service.asynctransactionprocessing.cache;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator.AgentLocatorProxyImpl;
import com.moneygram.mgo.service.asynctransactionprocessing.util.CountryComparator;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.SpringApplicationContext;


public class LocatorCache  implements CacheElementFactory {

    public static final String COUNTRIES_CACHE_NAME = "LocatorCache";
    public static final String COUNTRIES_CACHE_ELEMENT_NAME = "Countries";
    public static final String COUNTRIES_KEY_NAME = "Countries";
    public static final String COUNTRY_SERVICES_KEY_NAME = "CountryServices";
    public static final String STATES_KEY_NAME = "States";

    protected List<Country> countryCache;
//    protected HashMap<String, Services> countryServicesCache;
    protected HashMap<String, HashMap<String, State>> statesCache;

    protected MGOResourceConfig mgoResourceConfig = SpringApplicationContext.getBean("mgoResourceConfig");
    protected AgentLocatorProxyImpl locatorProxy = SpringApplicationContext.getBean("locatorServiceProxy");

    private void initLocatorCache() throws Exception {
        try {
            countryCache = new ArrayList<Country>();
//            countryServicesCache = new HashMap<String, Services>();
            statesCache = new HashMap<String, HashMap<String, State>>();

            List<Country> countries = locatorProxy.getCountries();
            Collections.sort(countries, new CountryComparator());

            countries = Collections.unmodifiableList(countries);
            countryCache.addAll(countries);

            for (Country country : countries) {
//                countryServicesCache.put(country.getCode(), locatorProxy.getCountryServices(country.getCode()));
                HashMap<String, State> map = (HashMap<String, State>) locatorProxy.getStates(country.getCode());
                if ("US".equals(country.getCode())) {
                    map.remove("PR");
                }
                statesCache.put(country.getCode(), map);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public CacheElement createElement(Serializable key) throws Exception {
        try {
            CacheElement cacheElement = null;
            HashMap<String, Serializable> cacheValuesMap = new HashMap<String, Serializable>();
            initLocatorCache();
            cacheValuesMap.put(COUNTRIES_KEY_NAME, (Serializable) countryCache);
//            cacheValuesMap.put(COUNTRY_SERVICES_KEY_NAME, countryServicesCache);
            cacheValuesMap.put(STATES_KEY_NAME, statesCache);
            cacheElement = new CacheElement(key, cacheValuesMap);
            return cacheElement;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
