package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOStatusCodes;


public abstract class PaymentMethod
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private String accountCode;
    private String accountStatus;
    private String paymentType;
    private Comment comment = new Comment();
    private String accountSubStatus;
    private int effortId;
    private boolean blocked = false;

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    /**
     * Returns true if payment method status is active.
     * 
     * @return
     */
    public boolean isActive() {
        return MGOStatusCodes.ACTIVE.equals(getAccountStatus());
    }

    /**
     * Returns true if payment method status is active and it's not expired.
     * 
     * @return
     */
    public boolean isActiveNotExpired() {
        return isActive() && (!isExpired());
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Comment getComment() {
        return comment;
    }

    public abstract String getDisplayString();

    public String getAccountSubStatus() {
        return accountSubStatus;
    }

    public void setAccountSubStatus(String accountSubStatus) {
        this.accountSubStatus = accountSubStatus;
    }

    public int getEffortId() {
        return effortId;
    }

    public void setEffortId(int effortId) {
        this.effortId = effortId;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    /**
     * Returns true if account has expired.
     * 
     * @return
     */
    public boolean isExpired() {
        return false;
    }

    public abstract String getExpirationMonth();

    public abstract String getExpirationYear();
    
    /**
     * Returns the masked credit card number (**** **** **** 4158)
     */
    public abstract String getDisplayNumber();

}
