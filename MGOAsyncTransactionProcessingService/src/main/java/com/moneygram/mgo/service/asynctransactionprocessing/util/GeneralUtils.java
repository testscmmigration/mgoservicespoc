package com.moneygram.mgo.service.asynctransactionprocessing.util;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class GeneralUtils {


    private static final Logger log = Logger.getLogger(GeneralUtils.class);

    public static TimeZone getTimeZoneByLocale(Locale locale) throws Exception {
        Calendar cal = Calendar.getInstance(locale);
        TimeZone clientTimeZone = cal.getTimeZone();
        if(clientTimeZone != null){
        	log.info("current timezone: "+clientTimeZone);
        }
        return clientTimeZone;
    }
}
