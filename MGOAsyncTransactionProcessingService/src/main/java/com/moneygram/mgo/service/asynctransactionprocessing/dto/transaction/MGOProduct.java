package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.log4j.Logger;




public class MGOProduct
        implements Remote, Serializable {

    private static final Logger logger = Logger.getLogger(MGOProduct.class);
    private static final long serialVersionUID = 1L;
    public static final String BILL_PAY = "EPSEND";
    public static final String SAME_DAY = "MGSEND";
    public static final String ECONOMY = "ESSEND";
    public static final String DELAYED_SEND = "DSSEND";
    public static final String BILL_PAY_CASH = "EPCASH";
    public static final String SAME_DAY_CASH = "MGCASH";
    public static final String INLANE = "INLANESEND";
    public static final String SAME_DAY_UK = "MGSEND";

    public static final String CARD = "CARD";

 
}
