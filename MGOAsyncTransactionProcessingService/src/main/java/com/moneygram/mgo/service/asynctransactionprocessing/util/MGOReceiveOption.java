package com.moneygram.mgo.service.asynctransactionprocessing.util;


public enum MGOReceiveOption {
    WILL_CALL("0", "WILL_CALL", ""), BANCOMER("4", "BANCOMER", ""), CAMBIO_PLUS("5", "CAMBIO_PLUS", ""), HDS_USD("6",
            "HDS_USD", ""), HDS_LOCAL("7", "HDS_LOCAL", ""), HOME_DELIVERY("8", "HOME_DELIVERY", ""), CARD_DEPOSIT("9",
            "CARD_DEPOSIT", ""), BANK_DEPOSIT("10", "BANK_DEPOSIT", ""), RECEIVE_AT("11", "RECEIVE_AT", ""), LTD_WILLCALL(
            "12", "LTD_WILLCALL", ""), ONLY_AT("15", "ONLY_AT", "Any Walmart store"), _48_HOUR ("17", "48_HOUR", "");

    private String id;
    private String code;
    private String name;

    private MGOReceiveOption(String id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static MGOReceiveOption fromCode(String code) throws java.lang.IllegalArgumentException {
        for (MGOReceiveOption e : MGOReceiveOption.values()) {
            if (e.getCode().equals(code)) {
                return e;
            }
        }
        throw new java.lang.IllegalArgumentException();
    }

    public static MGOReceiveOption fromId(String id) throws java.lang.IllegalArgumentException {
        for (MGOReceiveOption ro : MGOReceiveOption.values()) {
            if (ro.getId().equals(id)) {
                return ro;
            }
        }
        throw new java.lang.IllegalArgumentException();
    }

}
