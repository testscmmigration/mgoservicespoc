package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;



public class Receiver
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private Name name = new Name();
    private String receiveOption;
    private String RRNNumber;
    private String accountNumber;
    private String confirmAccountNumber;
    private String testQuestion;
    private String testAnswer;
    private Messages messages = new Messages();
    private Address address = new Address();
    private Delivery Delivery = new Delivery();
    private Phone phone = new Phone();
    private Biller selectedBiller = new Biller();
    /**
     * RRN-specific fields below will only display when Receive Option is "Send to Card-Visa," "Bank Deposit." 
     */
    private String _RRNumber;
    /**
     * Mobile-specific fields below will only display when Receive Option is "Send to Mobile-SMART Money"
     */
    private String _MobileNumber;
    
    
    private String city;
    private String instruction1;
    private String instruction2;
    private String instruction3;
    

    public String getConfirmAccountNumber() {
        return confirmAccountNumber;
    }

    public void setConfirmAccountNumber(String confirmAccountNumber) {
        this.confirmAccountNumber = confirmAccountNumber;
    }

    public Biller getSelectedBiller() {
        return selectedBiller;
    }

    public void setSelectedBiller(Biller selectedBiller) {
        this.selectedBiller = selectedBiller;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Name getName() {
        return name;
    }

    public String getReceiveOption() {
        return receiveOption;
    }

    public void setReceiveOption(String receiveOption) {
        this.receiveOption = receiveOption;
    }

    public String getRRNNumber() {
        return RRNNumber;
    }

    public void setRRNNumber(String rRNNumber) {
        this.RRNNumber = rRNNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getTestQuestion() {
        return testQuestion;
    }

    public void setTestQuestion(String testQuestion) {
        this.testQuestion = testQuestion;
    }

    public String getTestAnswer() {
        return testAnswer;
    }

    public void setTestAnswer(String testAnswer) {
        this.testAnswer = testAnswer;
    }

    public void setDelivery(Delivery delivery) {
        this.Delivery = delivery;
    }

    public Delivery getDelivery() {
        return Delivery;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountNumberLast4() {
        return getAccountNumber() == null ? null : StringHelper.maskLast4(getAccountNumber());
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    public Messages getMessages() {
        return messages;
    }
    
    public void set_RRNumber(String _RRNumber) {
		this._RRNumber = _RRNumber;
	}
    
    public String get_RRNumber() {
		return _RRNumber;
	}
    
    public void set_MobileNumber(String _MobileNumber) {
		this._MobileNumber = _MobileNumber;
	}
    
    public String get_MobileNumber() {
		return _MobileNumber;
	}
    
    

    public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getInstruction1() {
		return instruction1;
	}

	public void setInstruction1(String instruction1) {
		this.instruction1 = instruction1;
	}

	public String getInstruction2() {
		return instruction2;
	}

	public void setInstruction2(String instruction2) {
		this.instruction2 = instruction2;
	}

	public String getInstruction3() {
		return instruction3;
	}

	public void setInstruction3(String instruction3) {
		this.instruction3 = instruction3;
	}

	public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
