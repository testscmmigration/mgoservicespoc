package com.moneygram.mgo.service.asynctransactionprocessing.util;


public class ReceiptFields {
    public static final String TRAN_TYPE = "tranType";
    public static final String STATE = "state";
    public static final String RECEIPT_DATE = "receiptDate";
    public static final String LOCAL_RECEIPT_DATE = "localizedReceiptDate";
    public static final String RECEIPT_TIMESTAMP = "receiptTimeStamp";
    public static final String LANGUAGE = "language";

    public static final String RRN_NUMBER = "rrnNumber";
    public static final String DELIVERY_OPTION = "deliveryOption";
    public static final String SERVICE_OPTION = "serviceOption";
    public static final String MESSAGE1 = "message1";
    public static final String MESSAGE2 = "message2";
    public static final String ACCOUNT_TYPE = "accountType";
    public static final String BANK_ACCOUNT_TYPE = "bankAccountType";
    public static final String BACKUP_CC = "backupCC";
    public static final String REFERENCE_NUMBER = "referenceNumber";
    public static final String TRANSFER_DATE = "transferDate";
    public static final String SENDER_NAME = "senderName";
    public static final String TRANSFER_AMOUNT = "transferAmount";
    public static final String CONSUMER_FEE = "consumerFee";
    public static final String TOTAL = "total";
    public static final String RECEIVER_NAME = "receiverName";
    public static final String RECEIVER_SECOND_LAST_NAME = "receiverSecondLastName";
    public static final String RECEIVE_COUNTRY = "receiveCountry";
    public static final String RECEIVE_AMOUNT = "receiveAmount";
    public static final String RECEIVE_CURRENCY = "receiveCurrency";
    public static final String EXCHANGE_RATE = "exchangeRate";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String SENDER_PHONE = "senderPhone";

    public static final String RECEIVE_STATE = "receiveState";
    public static final String BANCOMER_CONFIRM_NUMBER = "bancomerConfirmNumber";
    public static final String AUTH_NUMBER = "authNumber";
    public static final String REWARDS_DISCOUNT = "rewardsDiscount";
    public static final String ESTIMATED_PICKUP_DATE = "estimatedPickupDate";
    public static final String ACCOUNT_NUMBER = "accountNumber";

    public static final String SEND_CURRENCY = "sendCurrency";

    public static final String BILLER_SERVICE_LEVEL = "serviceLevel";
    public static final String BILLER_END_OF_DAY = "cutOffTime";
    public static final String BILLER_NOTES = "postingMessage";

    // Receipts new fields.
    public static final String SPANISH_VERSION = "spanishVersion";
    public static final String RCV_FEE_AMOUNT = "rcvFeeAmount";
    public static final String RCV_TAX_AMOUNT = "rcvTaxAmount";
    public static final String RCV_TOTAL_TO_RECEIVE_AMOUNT = "rcvTotalToReceiveAmount";
    public static final String RCV_TX_FR_AMOUNT = "rcvTxFrAmount";
    public static final String SENDER_TAX_AMOUNT = "senderTaxAmount";
    public static final String EXCHANGE_RATE_APPLIED = "exchangeRateApplied";
    public static final String AVAILABLE_DATE = "availableDate";
    public static final String DISCLAIMER_TXT_ENG = "disclaimerTxtENG";
    public static final String DISCLAIMER_TXT_SPA = "disclaimerTxtSPA";
    public static final String STATE_REG_NAME_SPA = "stateRegulatorNameSPA";
    public static final String STATE_REG_NAME_ENG = "stateRegulatorNameENG";
    public static final String STATE_REG_PHONE = "stateRegulatorPhone";
    public static final String STATE_REG_URL = "stateRegulatorUrl";
    public static final String SENDER_ADDRESS_STREET = "senderAddress1";
    public static final String SENDER_ADDRESS_CITY = "senderAddress2";
    public static final String SENDER_ADDRESS_COUNTRY = "senderCountry";
    public static final String SENDER_ADDRESS_US = "senderAddress";
    
    // S29a
    public static final String THREE_MINUTE_PHONE_NUMBER = "threeMinutePhoneNumber";
    public static final String THREE_MINUTE_PIN_NUMBER = "threeMinutePinNumber";
    
    public static final String SENDER_FIRST_NAME = "senderFirstName";
    public static final String SENDER_MIDDLE_NAME = "senderMiddleName";
    public static final String SENDER_LAST_NAME = "senderLastName";
    public static final String SENDER_SECOND_LAST_NAME = "senderSecondLastName";
   
    public static final String RECEIVER_FIRST_NAME = "receiverFirstName";
    public static final String RECEIVER_MIDDLE_NAME = "receiverMiddleName";
    public static final String RECEIVER_LAST_NAME = "receiverLastName";

    public static final String SEND_TAXES = "sendTaxes";
    public static final String RECEIVE_TAXES = "receiveTaxes";
    public static final String RECEIVE_FEE = "receiveFee";
    public static final String RECEIVE_FEE_IS_ESTIMATED = "recvFeeEstimated";
    public static final String RECEIVE_TAX_IS_ESTIMATED = "recvTaxEstimated";
    public static final String INDICATIVE_COUNTRY = "isIndicativeRate";
    public static final String RECEIVE_TOTAL_AMOUNT = "totalReceiveAmount";

    public static final String INITIATED_DATE = "dateInitiated";
    public static final String PAYMENT_METHOD_ACC_NUMBER = "pmAccountNumber";
    public static final String CREDIT_CARD_TYPE = "cardType";
    public static final String BANK = "bank";
    public static final String CREDIT_CARD_EXP_DATE = "cardExpireDate";


    
    
}
