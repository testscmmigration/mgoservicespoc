package com.moneygram.mgo.service.asynctransactionprocessing.util;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.apache.commons.lang.StringUtils;


public class StringHelper {
    public StringHelper() {
    }

    public static String formatAsCamelCase(String state) {
        StringBuffer sb = new StringBuffer(state.toLowerCase());
        if (Character.isLetter(sb.charAt(0))) {
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        }
        int i = sb.indexOf(" ");
        while (i != -1) {
            while (!Character.isLetter(sb.charAt(++i))) {
                ;
            }
            sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
            i = sb.indexOf(" ", i + 1);
        }
        return sb.toString();
    }

    public static boolean isNullOrEmpty(String s) {
        return (s == null || s.trim().length() == 0);
    }

    public static int compareToIgnoreCase(String s1, String s2) {
        int comparison = 0;
        if (s1 == null) {
            comparison = (s2 == null ? 0 : 1);
        } else if (s2 == null) {
            comparison = -1;
        } else {
            comparison = s1.compareToIgnoreCase(s2);
        }
        return comparison;
    }

    public static boolean equalIgnoreCase(String s1, String s2) {
        boolean equality;
        if (s1 == null) {
            equality = (s2 == null);
        } else if (s2 == null) {
            equality = false;
        } else {
            equality = s1.equalsIgnoreCase(s2);
        }
        return equality;
    }

    public static String getEmptyIfNull(String s) {
        return (s == null ? "" : s); //$NON-NLS-1$
    }

    public static boolean containsNonDigits(String s) {
        boolean nonDigits = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonDigits == false && i < s.length(); ++i) {
                nonDigits = Character.isDigit(s.charAt(i)) == false;
            }
        }
        return (nonDigits);
    }

    public static boolean containsNonDigits(String s, char[] ignore) {
        boolean nonDigits = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonDigits == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isDigit(c) == false) {
                    nonDigits = true;
                    for (int j = 0; nonDigits == true && j < ignore.length; j++) {
                        nonDigits = (c == ignore[j]) == false;
                    }
                }
            }
        }
        return (nonDigits);
    }

    public static boolean containsAlpha(String s) {
        boolean alpha = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; alpha == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isLetter(c) == true)
                    alpha = true;
            }
        }
        return (alpha);
    }

    public static boolean containsNonAlpha(String s, char[] ignore) {
        boolean nonAlpha = false;
        if (StringHelper.isNullOrEmpty(s) == false) {
            for (int i = 0; nonAlpha == false && i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isLetter(c) == false) {
                    nonAlpha = true;
                    for (int j = 0; nonAlpha == true && j < ignore.length; j++) {
                        nonAlpha = (c == ignore[j]) == false;
                    }
                }
            }
        }
        return (nonAlpha);
    }

    public static String trim(String s) {
        return s == null ? null : s.trim();
    }

    public static String getLastToken(String s, String delimiter) {
        String token = null;
        if (s != null && delimiter != null) {
            token = s.substring(s.lastIndexOf(delimiter) + delimiter.length());
        }
        return (token);
    }

    public static String removeNonDigits(String s) {
        StringBuffer sb = new StringBuffer();
        if (s != null) {
            for (int i = 0; i < s.length(); ++i) {
                char c = s.charAt(i);
                if (Character.isDigit(c)) {
                    sb.append(c);
                }
            }
        }
        return (sb.toString());
    }

    public static String[] split(String stringToSplit, String splitString) {
        // The WSAD Web Environment does not suppport JDK 1.4
        // (does from a Java Project) so I added this String.split method
        // cannot in this project and it can be called from the Web projects.
        return (String[]) stringToSplit.split(splitString);
    }

    public static Date getFileBuildDate(String[] fileName) {
        long timestamp = 0;
        for (int i = 0; i < fileName.length; i++) {
            timestamp = (new File(fileName[i])).lastModified();
            if (timestamp != 0) {
                break;
            }
        }
        return new Date(timestamp);
    }

    public static String getExceptionDumpMsg(Throwable t) {
        StringBuffer msg = new StringBuffer();

        msg.append("Timestamp:\t" + (new Date(System.currentTimeMillis())).toString() + "\n");
        msg.append("Class:\t" + t.getClass().getName() + "\n");
        msg.append("Message:\t" + t.getMessage() + "\n");

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        msg.append(sw.toString());

        try {
            pw.close();
            sw.close();
        } catch (IOException e) {
        }

        return msg.toString();
    }

    public static String prepad(String s, int length, char c) {
        int needed = length - s.length();
        if (needed <= 0) {
            return s;
        }
        StringBuffer sb = new StringBuffer(length);
        for (int i = 0; i < needed; i++) {
            sb.append(c);
        }
        sb.append(s);
        return (sb.toString());
    }

    public static int compareIP(String ip1, String ip2) {
        if (ip1 == null && ip2 == null) {
            return 0;
        }
        if (ip1 == null) {
            return -1;
        }
        if (ip2 == null) {
            return 1;
        }
        String[] ipStr1 = ip1.split("\\.");
        String[] ipStr2 = ip2.split("\\.");
        if (ipStr1.length != 4 || ipStr2.length != 4) {
            return 0;
        }
        for (int i = 0; i < 4; i++) {
            if (containsNonDigits(ipStr1[i]) || containsNonDigits(ipStr2[i])) {
                return 0;
            }
        }
        int[] ipNbr1 = {0, 0, 0, 0};
        int[] ipNbr2 = {0, 0, 0, 0};
        for (int i = 0; i < 4; i++) {
            ipNbr1[i] = Integer.parseInt(ipStr1[i]);
            ipNbr2[i] = Integer.parseInt(ipStr2[i]);
            if (ipNbr1[i] > ipNbr2[i]) {
                return 1;
            } else if (ipNbr1[i] < ipNbr2[i]) {
                return -1;
            }
        }
        return 0;
    }

    public static String truncate(String str, Number length) {
        if (str == null) {
            return str;
        }
        if (str.length() > length.intValue()) {
            return str.substring(0, length.intValue());
        } else {
            return str;
        }
    }

    public static String truncate(String str, int length) {
        if (str == null) {
            return str;
        }
        if (str.length() > length) {
            return str.substring(0, length);
        } else {
            return str;
        }
    }

    public static String extractLastChars(String str, int length) {
        if (str == null || length < 1) {
            return str;
        }
        if (str.length() > length) {
            return str.substring(str.length() - length);
        } else {
            return str;
        }
    }

    public static String replaceAll(String str, String string1, String string2) {
        return str.replaceAll(string1, string2);
    }

    public static String getCcBin(String creditCardNumber) {
        return creditCardNumber.substring(0, 6);
    }

    public static String maskCc(String creditCardNumber) {
        return creditCardNumber.substring(0, 6) + "******" + creditCardNumber.substring(12);
    }

    public static String stripGarbage(String s) {
        String good = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String result = null;
        if (s != null) {
            result = "";
            for (int i = 0; i < s.length(); i++) {
                if (good.indexOf(s.charAt(i)) >= 0) {
                    result += s.charAt(i);
                }
            }
        }
        return result;
    }

    /**
     * Masks the string with last N characters left.
     * 
     * @param value
     * @param n
     * @param mask
     * @return
     */
    public static String maskLastN(String value, int n, char mask) {
        StringBuffer accountNumber = null;
        if (value != null && StringUtils.isNotEmpty(value)) {
            accountNumber = new StringBuffer(value);
            for (int i = 0; i < accountNumber.length() - n; i++) {
                accountNumber.setCharAt(i, mask);
            }
        }

        return (accountNumber == null ? null : accountNumber.toString());
    }

    /**
     * Masks the string with last 4 characters left using '*'.
     * 
     * @param value
     * @return
     */
    public static String maskLast4(String value) {
        return maskLastN(value, 4, '*');
    }

    public static String onlyLetters(String s) {
        String good = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String result = null;
        if (s != null) {
            result = "";
            for (int i = 0; i < s.length(); i++) {
                if (good.indexOf(s.charAt(i)) >= 0) {
                    result += s.charAt(i);
                }
            }
        }
        return result;
    }

    public static String onlyDigitsLetters(String s) {
        String good = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        String result = null;
        if (s != null) {
            result = "";
            for (int i = 0; i < s.length(); i++) {
                if (good.indexOf(s.charAt(i)) >= 0) {
                    result += s.charAt(i);
                }
            }
        }
        return result;
    }

    public static String onlyDigits(String s) {
        String good = "0123456789";
        String result = null;
        if (s != null) {
            result = "";
            for (int i = 0; i < s.length(); i++) {
                if (good.indexOf(s.charAt(i)) >= 0) {
                    result += s.charAt(i);
                }
            }
        }
        return result;
    }

    public static String cleanUp(String s, String sToMatch, boolean isToKeep) {
        final int size = s.length();
        StringBuffer buf = new StringBuffer(size);
        if (!isToKeep) {
            for (int i = 0; i < size; i++) {
                if (sToMatch.indexOf(s.charAt(i)) == -1) {
                    buf.append(s.charAt(i));
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (sToMatch.indexOf(s.charAt(i)) != -1) {
                    buf.append(s.charAt(i));
                }
            }
        }
        return buf.toString();
    }
}
