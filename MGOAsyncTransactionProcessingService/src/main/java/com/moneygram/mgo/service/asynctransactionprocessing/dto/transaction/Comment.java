package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class Comment
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private String commentCode;
    private String commentText;

    public void setCommentCode(String commentCode) {
        this.commentCode = commentCode;
    }

    public String getCommentCode() {
        return commentCode;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCommentText() {
        return commentText;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
