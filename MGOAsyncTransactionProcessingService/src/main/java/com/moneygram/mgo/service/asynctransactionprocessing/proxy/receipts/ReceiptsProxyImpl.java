package com.moneygram.mgo.service.asynctransactionprocessing.proxy.receipts;

/**
 * @author vd51
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.stellent.www.MGOReceipts.EMTAdminReceiptInfoResult;
import com.stellent.www.MGOReceipts.IdcProperty;
import com.stellent.www.MGOReceipts.MGOReceiptsSoap;
import com.stellent.www.MGOReceipts.StatusInfo;

@Component(value = "receiptsServiceProxy")
@DependsOn("mgoResourceConfig")
public class ReceiptsProxyImpl implements ReceiptsProxy {

	// -------------------------------------- Dependency Injection ------------------------------//

	@Autowired
	protected MGOReceiptsSoap receiptsService;
	
	 @Autowired
	 protected MGOResourceConfig mgoResourceConfig;

	// -------------------------------------- Attributes ---------------------------------------------------//

	private static final Logger logger = Logger.getLogger(ReceiptsProxyImpl.class);

	// -------------------------------------- Methods ---------------------------------------------------//

	@Override
	public String getReceiptForTransaction(String language, String state,String tranType, String receiptDate, Properties receiptProps,String partnerSiteId)
			throws Exception {
		logger.debug("[getReceiptForTransaction] Starting Receipt Generation for transaction ");
		String receiptHtml = null; 
		
		try {
			
			List<IdcProperty> receiptParmsList = new ArrayList<IdcProperty>();
			for(String key : receiptProps.stringPropertyNames()) {
				/*condition required for defect 440*/
				if(("WAP".equals(partnerSiteId) || "MGO".equals(partnerSiteId)) && "referenceNumber".equals(key)); else {
					String value = receiptProps.getProperty(key);
					receiptParmsList.add(new IdcProperty(key, receiptProps.getProperty(key)));
				}
			}
						
			List<IdcProperty> extraPropsList = new ArrayList<IdcProperty>();
			
			//convert from arraylist to simple array
			IdcProperty [] receiptParms = receiptParmsList.toArray(new IdcProperty[receiptParmsList.size()]);
			//Base on the request extraProps is no needed but is preferred send a empty Array than a null
			IdcProperty [] extraProps = extraPropsList.toArray(new IdcProperty[extraPropsList.size()]);
			
			String userName = mgoResourceConfig.getAttributeValue(MGOResourceConfig.MGO_RECEIPTS_SERVICE_USERNAME);
			String password = mgoResourceConfig.getAttributeValue(MGOResourceConfig.MGO_RECEIPTS_SERVICE_PASSWORD);
			
			EMTAdminReceiptInfoResult result = receiptsService.EMTAdminReceiptInfo(language, state, tranType,receiptDate,receiptParms, extraProps, userName, password);
			receiptHtml = result.getGeneratedReceiptHtml();
			/*starts defect 440*/
			if("WAP".equals(partnerSiteId) || "MGO".equals(partnerSiteId)){
				receiptHtml = receiptHtml.replaceAll("<td class=\"recLabel\">Reference Number:</td>", "");
				receiptHtml = receiptHtml.replaceAll("\\(Used by MoneyGram for tracking\\)","");
			}
			/*ends defect 440*/
			StatusInfo statusInfo = result.getStatusInfo();
			logger.debug( "[getReceiptForTransaction] StatusInfo Code =" + statusInfo.getStatusCode());
			logger.debug( "[getReceiptForTransaction] StatusInfo Message =" + statusInfo.getStatusMessage());
			
			
		} catch (Exception e) {
			logger.equals("[getReceiptForTransaction] Error when generating the Receipt");
			throw e;
		}

		return receiptHtml;
	}

}
