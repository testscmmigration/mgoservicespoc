package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class Phone implements Remote, Serializable {

    public static final String HOME = "HOME";
    public static final String MOBILE = "MOBILE";
    private static final long serialVersionUID = 1L;
    private String phoneNumber;
    private String phoneType;
    private boolean blocked = false;

    public Phone() {
    }

    public Phone(String phoneNumber, String phoneType) {
        setPhoneNumber(phoneNumber);
        setPhoneType(phoneType);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;

    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneType(String phoneType) {
        if(!"".equals(phoneType)){
        	this.phoneType = phoneType.equalsIgnoreCase(HOME) ? HOME : MOBILE;
        }else{
        	phoneType = "";
        }
    }

    public String getPhoneType() {
        return phoneType;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * @return the blocked
     */
    public boolean isBlocked() {
        return blocked;
    }

    /**
     * @param blocked
     *            the blocked to set
     */
    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
