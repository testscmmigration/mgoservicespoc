package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.StringUtils;



public class TransactionDetails
        implements Remote, Serializable {
    private static final long serialVersionUID = 1L;
    private String bancomerConfirmationNumber;
    private String referenceNumber;
    private String transactionCode;
    private String transactionType;
    private String transactionTypeCode;
    private String receiptTranType;
    private String status;
    private String subStatus;
    private String displayStatus;
    private String service;
    private String receiveOption;
    private String receiveOptionID;
    private String paymentMethod;
    private String backupPaymentMethod;
    private Receiver receiver;
    private String receiveCountry;
    private String receiveCurrency;
    
    private String rewardsNumber = "";
    private TransactionAmount transactionAmount = new TransactionAmount();
    private TransactionLog transactionLog = new TransactionLog();
    private String receiveAgentID;
    private String accountId;
    private boolean showReceipt;
    private String billerServiceLevel;
    private String billerEndOfDayText;
    private String billerNotes;
    // S29a Begin
    private String threeMinuteFreePhoneNumber;
    private String threeMinuteFreePinNumber;
    
    private String mgTransactionSessionId;
   
    // Receipts new feilds.
    private String disclaimerTxtSPA;
    private String disclaimerTxtENG;
    private String pmAccountNumber;
    private String cardType;
    private String cardbank;
    private String cardExpireDate;

    
    /**
     * fiAccountid of the Payment method used for the current transaction
     */
    private Long fiAccountId;
    private Long backupFiAccountId;
    
    private String partnerSiteId;

    /**
     * @param threeMinuteFreePhoneNumber
     *            the threeMinuteFreePhoneNumber to set
     */
    public void setThreeMinuteFreePhoneNumber(String threeMinuteFreePhoneNumber) {
        this.threeMinuteFreePhoneNumber = threeMinuteFreePhoneNumber;
    }

    /**
     * @return the threeMinuteFreePhoneNumber
     */
    public String getThreeMinuteFreePhoneNumber() {
        return threeMinuteFreePhoneNumber;
    }

    /**
     * @param threeMinuteFreePinNumber
     *            the threeMinuteFreePinNumber to set
     */
    public void setThreeMinuteFreePinNumber(String threeMinuteFreePinNumber) {
        this.threeMinuteFreePinNumber = threeMinuteFreePinNumber;
    }

    /**
     * @return the threeMinuteFreePinNumber
     */
    public String getThreeMinuteFreePinNumber() {
        return threeMinuteFreePinNumber;
    }

    // S29a End

    public String getBillerServiceLevel() {
        return billerServiceLevel;
    }

    public void setBillerServiceLevel(String billerServiceLevel) {
        this.billerServiceLevel = billerServiceLevel;
    }

    public String getBillerEndOfDayText() {
        return billerEndOfDayText;
    }

    public void setBillerEndOfDayText(String billerEndOfDayText) {
        this.billerEndOfDayText = billerEndOfDayText;
    }

    public String getBillerNotes() {
        return billerNotes;
    }

    public void setBillerNotes(String billerNotes) {
        this.billerNotes = billerNotes;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    public void setReceiveAgentID(String receiveAgentID) {
        this.receiveAgentID = receiveAgentID;
    }

       public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public String getReceiptTranType() {
        return receiptTranType;
    }

    public void setReceiptTranType(String receiptTranType) {
        this.receiptTranType = receiptTranType;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setReceiveOption(String receiveOption) {
        this.receiveOption = receiveOption;
    }

    public String getReceiveOption() {
        return receiveOption;
    }

    public String getReceiveOptionID() {
        return receiveOptionID;
    }

    public void setReceiveOptionID(String receiveOptionID) {
        this.receiveOptionID = receiveOptionID;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setBackupPaymentMethod(String backupPaymentMethod) {
        this.backupPaymentMethod = backupPaymentMethod;
    }

    public String getBackupPaymentMethod() {
        return backupPaymentMethod;
    }

    public String getRewardsNumber() {
        return rewardsNumber;
    }

    public void setRewardsNumber(String rewardsNumber) {
        this.rewardsNumber = rewardsNumber;
    }

    public void setTransactionAmount(TransactionAmount transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public TransactionAmount getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionLog(TransactionLog transactionLog) {
        this.transactionLog = transactionLog;
    }

    public TransactionLog getTransactionLog() {
        return transactionLog;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public Receiver getReceiver() {
        return receiver;
    }
    
    public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	
    public void setBancomerConfirmationNumber(String bancomerConfirmationNumber) {
        this.bancomerConfirmationNumber = bancomerConfirmationNumber;
    }

    public String getBancomerConfirmationNumber() {
        return bancomerConfirmationNumber;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }

    public boolean isShowReceipt() {
        return showReceipt;
    }

    public void setShowReceipt(boolean showReceipt) {
        this.showReceipt = showReceipt;
    }

    public String getEscapedReceiverFirstName() {
        String name = null;
        if (getReceiver() != null && getReceiver().getName() != null && getReceiver().getName().getFirstName() != null) {
            name = escapeCharacters(getReceiver().getName().getFirstName());
        }
        return name;
    }

    public String getEscapedReceiverMiddleName() {
        String name = null;
        if (getReceiver() != null && getReceiver().getName() != null && getReceiver().getName().getMiddleName() != null) {
            name = escapeCharacters(getReceiver().getName().getMiddleName());
        }
        return name;
    }

    public String getEscapedReceiverLastName() {
        String name = null;
        if (getReceiver() != null && getReceiver().getName() != null && getReceiver().getName().getLastName() != null) {
            name = escapeCharacters(getReceiver().getName().getLastName());
        }
        return name;
    }

    public String getEscapedReceiverSecondLastName() {
        String name = null;
        if (getReceiver() != null && getReceiver().getName() != null
                && getReceiver().getName().getSecondLastName() != null) {
            name = escapeCharacters(getReceiver().getName().getSecondLastName());
        }
        return name;
    }

    public String getEscapedMessage() {
        String name = null;
        if (getReceiver() != null && getReceiver().getName() != null
                && getReceiver().getName().getSecondLastName() != null) {
            name = escapeCharacters(getReceiver().getName().getSecondLastName());
        }
        return name;
    }

    public Long getFiAccountId() {
		return fiAccountId;
	}

	public void setFiAccountId(Long fiAccountId) {
		this.fiAccountId = fiAccountId;
	}

	public Long getBackupFiAccountId() {
		return backupFiAccountId;
	}

	public void setBackupFiAccountId(Long backupFiAccountId) {
		this.backupFiAccountId = backupFiAccountId;
	}

	/**
     * Escapes characters for JS.
     * 
     * @param value
     * @return
     */
    private static String escapeCharacters(String value) {
        String out = null;
        if (value != null) {
            out = StringUtils.replace(value, "'", "\\'");
            out = StringUtils.replace(out, "\"", "\\\"");
        }
        return out;
    }

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}
	
	 public String getMgTransactionSessionId() {
		return mgTransactionSessionId;
	}

	public void setMgTransactionSessionId(String mgTransactionSessionId) {
		this.mgTransactionSessionId = mgTransactionSessionId;
	}

	/**
	 * @return the disclaimerTxtSPA
	 */
	public String getDisclaimerTxtSPA() {
		return disclaimerTxtSPA;
	}

	/**
	 * @param disclaimerTxtSPA the disclaimerTxtSPA to set
	 */
	public void setDisclaimerTxtSPA(String disclaimerTxtSPA) {
		this.disclaimerTxtSPA = disclaimerTxtSPA;
	}

	/**
	 * @return the disclaimerTxtENG
	 */
	public String getDisclaimerTxtENG() {
		return disclaimerTxtENG;
	}

	/**
	 * @param disclaimerTxtENG the disclaimerTxtENG to set
	 */
	public void setDisclaimerTxtENG(String disclaimerTxtENG) {
		this.disclaimerTxtENG = disclaimerTxtENG;
	}

	/**
	 * @return the pmAccountNumber
	 */
	public String getPmAccountNumber() {
		return pmAccountNumber;
	}

	/**
	 * @param pmAccountNumber the pmAccountNumber to set
	 */
	public void setPmAccountNumber(String pmAccountNumber) {
		this.pmAccountNumber = pmAccountNumber;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the cardbank
	 */
	public String getCardbank() {
		return cardbank;
	}

	/**
	 * @param cardbank the cardbank to set
	 */
	public void setCardbank(String cardbank) {
		this.cardbank = cardbank;
	}

	/**
	 * @return the cardExpireDate
	 */
	public String getCardExpireDate() {
		return cardExpireDate;
	}

	/**
	 * @param cardExpireDate the cardExpireDate to set
	 */
	public void setCardExpireDate(String cardExpireDate) {
		this.cardExpireDate = cardExpireDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionDetails [bancomerConfirmationNumber="
				+ bancomerConfirmationNumber + ", referenceNumber="
				+ referenceNumber + ", transactionCode=" + transactionCode
				+ ", transactionType=" + transactionType
				+ ", transactionTypeCode=" + transactionTypeCode
				+ ", receiptTranType=" + receiptTranType + ", status=" + status
				+ ", subStatus=" + subStatus + ", displayStatus="
				+ displayStatus + ", service=" + service + ", receiveOption="
				+ receiveOption + ", receiveOptionID=" + receiveOptionID
				+ ", paymentMethod=" + paymentMethod + ", backupPaymentMethod="
				+ backupPaymentMethod + ", receiver=" + receiver
				+ ", receiveCountry=" + receiveCountry + ", receiveCurrency="
				+ receiveCurrency + ", rewardsNumber=" + rewardsNumber
				+ ", transactionAmount=" + transactionAmount
				+ ", transactionLog=" + transactionLog + ", receiveAgentID="
				+ receiveAgentID + ", accountId=" + accountId
				+ ", showReceipt=" + showReceipt + ", billerServiceLevel="
				+ billerServiceLevel + ", billerEndOfDayText="
				+ billerEndOfDayText + ", billerNotes=" + billerNotes
				+ ", threeMinuteFreePhoneNumber=" + threeMinuteFreePhoneNumber
				+ ", threeMinuteFreePinNumber=" + threeMinuteFreePinNumber
				+ ", mgTransactionSessionId=" + mgTransactionSessionId
				+ ", disclaimerTxtSPA=" + disclaimerTxtSPA
				+ ", disclaimerTxtENG=" + disclaimerTxtENG
				+ ", pmAccountNumber=" + pmAccountNumber + ", cardType="
				+ cardType + ", cardbank=" + cardbank + ", cardExpireDate="
				+ cardExpireDate + ", fiAccountId=" + fiAccountId
				+ ", backupFiAccountId=" + backupFiAccountId
				+ ", partnerSiteId=" + partnerSiteId + "]";
	}

	
}
