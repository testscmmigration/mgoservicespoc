package com.moneygram.mgo.service.asynctransactionprocessing.cache;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.agentconnect1305.wsclient.CodeTableRequest;
import com.moneygram.agentconnect1305.wsclient.CodeTableResponse;
import com.moneygram.agentconnect1305.wsclient.CountryInfo;
import com.moneygram.common.cache.CacheElement;
import com.moneygram.common.cache.CacheElementFactory;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentconnect.AgentConnectProxyInterface;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.SpringApplicationContext;



public class CodeTablesCache implements CacheElementFactory {

    private static final Logger log = LoggerFactory.getLogger(CodeTablesCache.class);
    public static final String CODE_TABLES_CACHE = "MGOAsyncTxProcCodeTablesCache";
    public static String CODE_TABLES_CACHE_ELEMENT = "CODE_TABLES_CACHE_ELEMENT";
    public static String CODE_TABLES_MAP = "CODE_TABLES_MAP";
   
    public static final String COUNTRY_MAP_KEY = "countryMap";
   
    protected HashMap<String, Object> codeTablesCacheDataMap = new HashMap<String, Object>(10);
   
    protected MGOResourceConfig mgoResourceConfig = SpringApplicationContext.getBean("mgoResourceConfig");
    protected AgentConnectProxyInterface agentConnectServiceProxy = SpringApplicationContext.getBean("agentConnectServiceProxy");
    
    
    protected void initCodeTablesCache() throws Exception {
        try {
            codeTablesCacheDataMap.clear();
            Map<String, Country> countryMap = null;

            CodeTableRequest codeTableRequest = new CodeTableRequest();
            agentConnectServiceProxy.populateRequest(codeTableRequest);

            CodeTableResponse codeTableResponse = agentConnectServiceProxy.codeTables(codeTableRequest);
            Country cntry;
            countryMap = new HashMap<String, Country>();
            CountryInfo[] ci = codeTableResponse.getCountryInfo();

            for (int i = 0; i < ci.length; i++) {
                CountryInfo countryInfo = ci[i];
                cntry = new Country();
                cntry.setCode(countryInfo.getCountryCode());
                cntry.setName(countryInfo.getCountryName());
                cntry.setSendActive(countryInfo.isSendActive());
                cntry.setRecvActive(countryInfo.isReceiveActive());
                cntry.setCountryLegacyCode(countryInfo.getCountryLegacyCode());

                countryMap.put(countryInfo.getCountryCode(), cntry);
            }
            codeTablesCacheDataMap.put(COUNTRY_MAP_KEY, countryMap);
           
        } catch (Exception ex) {
            log.error("Call to initCodeTablesCache failed:" + ex.getMessage(), ex);
            throw ex;
        }
    }

    public CacheElement createElement(Serializable key) throws Exception {
        try {
            CacheElement cacheElement = null;
            this.initCodeTablesCache();
            cacheElement = new CacheElement(key, codeTablesCacheDataMap);
            return cacheElement;
        } catch (Exception ex) {
            log.error("Error in  createElement :" + ex.getMessage(), ex);
            throw ex;
        }
    }

}
