package com.moneygram.mgo.service.asynctransactionprocessing.util;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.BankAccount;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.CreditCard;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.PaymentMethod;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;



public class MGOUtil {
	
    private static Logger log = Logger.getLogger(MGOUtil.class);
 
    public static String computeDisplayStatus(String status, Date receiveDate, String receiveOptionID) {
        MGOUtil.log.debug("status: " + status + ", receiveDate: " + receiveDate + ", receiveOptionID: "
                + receiveOptionID);
        String returnVal = MGODisplayStatusCodes.ACTION_REQUIRED;
        if (status != null) {
            MGOUtil.log.info("status is not null");
            if (status.equals(MGOStatusCodes.RECIEVED)) {
                returnVal = MGODisplayStatusCodes.PICKED_UP;
            } else if (status.equals(MGOStatusCodes.AUTOMATEDSCORINGPROCESS)
                    || status.equals(MGOStatusCodes.FORMFREESEND) || status.equals(MGOStatusCodes.APPROVED)) {
                // processing
                MGOUtil.log.debug("return processing");
                returnVal = MGODisplayStatusCodes.PROCESSING;
            } else if (status.equals(MGOStatusCodes.SAVE)) {
                // incomplete
                MGOUtil.log.debug("return incomplete");
                returnVal = MGODisplayStatusCodes.INCOMPLETE;
            } else if (status.equals(MGOStatusCodes.PENDING)) {
                // action required
                MGOUtil.log.debug("return action required");
                returnVal = MGODisplayStatusCodes.ACTION_REQUIRED;
            } else if (status.equals(MGOStatusCodes.SENT)) {
                // sent, check for receive date
                MGOUtil.log.debug("status = sent, checking date");
                if (receiveDate != null) {
                    MGOUtil.log.debug("date is not null, checking receive option");
                    // transaction was received, check receive option
                    if (receiveOptionID.equals(MGOReceiveOption.WILL_CALL.getId())
                            || receiveOptionID.equals(MGOReceiveOption.RECEIVE_AT.getId())
                            || receiveOptionID.equals(MGOReceiveOption.LTD_WILLCALL.getId())
                            || receiveOptionID.equals(MGOReceiveOption.BANCOMER.getId())
                            || receiveOptionID.equals(MGOReceiveOption.ONLY_AT.getId())
                            || receiveOptionID.equals(MGOReceiveOption.CAMBIO_PLUS.getId())) {
                        // pick up at agent
                        MGOUtil.log.debug("return picked up");
                        returnVal = MGODisplayStatusCodes.PICKED_UP;
                    } else if (receiveOptionID.equals(MGOReceiveOption.HDS_LOCAL.getId())
                            || receiveOptionID.equals(MGOReceiveOption.HDS_USD.getId())
                            || receiveOptionID.equals(MGOReceiveOption.HOME_DELIVERY.getId())) {
                        // delivered
                        MGOUtil.log.debug("return delivered");
                        returnVal = MGODisplayStatusCodes.DELIVERED;
                    } else if (receiveOptionID.equals(MGOReceiveOption.CARD_DEPOSIT.getId())
                            || receiveOptionID.equals(MGOReceiveOption.BANK_DEPOSIT.getId())) {
                        // deposited
                        MGOUtil.log.debug("return deposited");
                        returnVal = MGODisplayStatusCodes.DEPOSITED;
                    }
                } else {
                    // transaction not yet received
                    MGOUtil.log.debug("date was null, return available");
                    returnVal = MGODisplayStatusCodes.AVAILABLE;
                }
            } else if (status.equals(MGOStatusCodes.CASH)) {
                // in store payment
                MGOUtil.log.debug("return in store payment");
                returnVal = MGODisplayStatusCodes.IN_STORE_PAYMENT;
            } else if (status.equals(MGOStatusCodes.CANCELED) || status.equals(MGOStatusCodes.DENIED)
                    || status.equals(MGOStatusCodes.ERROR)) {
                // canceled
                MGOUtil.log.debug("return canceled");
                returnVal = MGODisplayStatusCodes.CANCELED;
            } else if (status.equals(MGOStatusCodes.PAYMENTCOMPLETE_BILLPAY)) {
                returnVal = MGODisplayStatusCodes.PAYMENTCOMPLETE_BILLPAY;
            }
        }
        return returnVal;
    }


    public static String getPaymentMethodDisplayString(List<PaymentMethod> financialAccounts, String accountCode) {
        String paymentMethodValue = "";
        for (int i = 0; i < financialAccounts.size(); i++) {
            PaymentMethod paymentMethod = financialAccounts.get(i);
            if (accountCode.equals(paymentMethod.getAccountCode())) {
                paymentMethodValue = paymentMethod.getDisplayString();
                break;
            }
        }
        return paymentMethodValue;
    }

    public static void getPaymentMethodFillTransactionDetails(TransactionDetails transactionDetails,
            List<PaymentMethod> financialAccounts, String accountCode) {
        for (int i = 0; i < financialAccounts.size(); i++) {
            PaymentMethod paymentMethod = financialAccounts.get(i);
            if (accountCode.equals(paymentMethod.getAccountCode())) {
                if (paymentMethod instanceof CreditCard) {
                    transactionDetails.setPmAccountNumber(paymentMethod.getDisplayNumber());
                    transactionDetails.setCardType((String) CreditCard.CC_NAME_MAP.get(((CreditCard) paymentMethod)
                        .getCardType()));
                    transactionDetails.setCardExpireDate(((CreditCard) paymentMethod).getExpirationMonth() + "/"
                            + ((CreditCard) paymentMethod).getExpirationYear());
                } else if (paymentMethod instanceof BankAccount) {
                    transactionDetails.setPmAccountNumber(paymentMethod.getDisplayNumber());
                    transactionDetails.setCardbank(((BankAccount) paymentMethod).getBankName());
                }
                break;
            }
        }
    }

}
