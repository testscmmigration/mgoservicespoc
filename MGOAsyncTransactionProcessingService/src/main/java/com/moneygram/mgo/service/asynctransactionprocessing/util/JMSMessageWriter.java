package com.moneygram.mgo.service.asynctransactionprocessing.util;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;


public class JMSMessageWriter implements JMSWriter {
    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(JMSMessageWriter.class.getName());


    private QueueConnectionFactory connectionFactory;

    private Queue queue;

    public QueueConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(QueueConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public void writeMessage(Message message) throws MessagingException {
        SendingSession ss = null;

        try {
            ss = setupConnection();
            ss.connection.start();

            ss.sender.send(message);
        } catch (JMSException e) {
            throw new MessagingException("Error sending message", e);
        } finally {
            if (ss != null) {
                ss.close();
            }
        }
    }

    @Override
    public void writeMessage(String content) throws MessagingException {
        SendingSession ss = null;

        try {
            ss = setupConnection();
            ss.connection.start();

            TextMessage message = ss.session.createTextMessage();
            message.setText(content);

            ss.sender.send(message);
        } catch (JMSException e) {
            throw new MessagingException("Error sending message", e);
        } finally {
            if (ss != null) {
                ss.close();
            }
        }
    }

    private SendingSession setupConnection() throws MessagingException {
        SendingSession sendSession = new SendingSession();
        try {
            sendSession.connection = connectionFactory.createQueueConnection();
            if (sendSession.connection == null) {
                throw new MessagingException("Could not get connection from connection factory. Object:"
                        + connectionFactory);
            }

            sendSession.session = sendSession.connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            if (sendSession.session == null) {
                throw new MessagingException("Could not get session from connection. Object:" + sendSession.connection);
            }

            sendSession.sender = sendSession.session.createSender(queue);
            if (sendSession.sender == null) {
                throw new MessagingException("Could not get sender from connection. Object:" + sendSession.session);
            }
        } catch (JMSException e) {
            throw new MessagingException("Error sending message", e);
        }

        return sendSession;
    }

    public void write(Message message) throws MessagingException {
        SendingSession ss = null;
        try {
            ss = setupConnection();
            ss.connection.start();
            ss.sender.send(message);

        } catch (JMSException e) {
            throw new MessagingException("Error sending message", e);
        } finally {
            if (ss != null) {
                ss.close();
            }
        }
    }

    protected class SendingSession {
        public QueueConnection connection = null;

        public QueueSession session = null;

        public QueueSender sender = null;

        public void close() {
            if (sender != null) {
                try {
                    sender.close();
                } catch (JMSException e) {
                }
                sender = null;
            }

            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                }
                session = null;
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                }
                connection = null;
            }

        }
    }

}
