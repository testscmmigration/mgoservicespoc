package com.moneygram.mgo.service.asynctransactionprocessing.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

@Component
public class MsgListenerErrorHandler implements ErrorHandler {

	
	private static final Logger logger = LoggerFactory.getLogger(MsgListenerErrorHandler.class);
	
    @Override
    public void handleError(Throwable t) {
        logger.error("[handleError] Error in Messagelistener", t);
    }
}
