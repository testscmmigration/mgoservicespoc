package com.moneygram.mgo.service.asynctransactionprocessing.util;


public class MGODisplayStatusCodes {
    public static final String PROCESSING = "processing";
    public static final String IN_STORE_PAYMENT = "in_store_payment";
    public static final String AVAILABLE = "available";
    public static final String PICKED_UP = "picked_up";
    public static final String DEPOSITED = "deposited";
    public static final String DELIVERED = "delivered";
    public static final String ACTION_REQUIRED = "action_required";
    public static final String CANCELED = "canceled";
    public static final String INCOMPLETE = "incomplete";
    public static final String PAYMENTCOMPLETE_BILLPAY = "Payment complete";

}