package com.moneygram.mgo.service.asynctransactionprocessing.util;


import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;






/**
 * 
 * MGO specific Resource Config. <div>
 * <table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOOnline</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2011</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.19 $ $Date: 2012/02/24 19:18:46 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: uu78 $ </td>
 *         </table>
 *         </div>
 */
public class MGOResourceConfig
        extends ResourceConfig
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8099863906198664457L;
	
	private static final Logger logger = LoggerFactory.getLogger(MGOResourceConfig.class);

    public static final String MGO_TRANSACTION_SERVICE_URL = "MGO_TRANSACTION_SERVICE_URL";
    public static final String AGENT_CONNECT_SERVICE_URL = "AGENT_CONNECT_SERVICE_URL";
	public static final String MGO_CONSUMER_SERVICE_URL = "MGO_CONSUMER_SERVICE_URL";
    public static final String SITE_IDENTIFIER_WAP = "SITE_IDENTIFIER_WAP";
    public static final String SITE_IDENTIFIER_MGODE = "SITE_IDENTIFIER_MGODE";
    public static final String SITE_IDENTIFIER_MGOUK = "SITE_IDENTIFIER_MGOUK";
    public static final String SITE_IDENTIFIER_MGOUS = "SITE_IDENTIFIER_MGOUS";
    public static final String TIME_EXPIRATION_GAP_IN_DAYS = "TIME_EXPIRATION_GAP_IN_DAYS";
    public static final String SENDER_EMAIL_ADDRESS = "SENDER_EMAIL_ADDRESS";
    public static final String WAP_SENDER_EMAIL_ADDRESS = "AFF_SENDER_EMAIL_ADDRESS";
    public static final String MGO_RECEIPTS_SERVICE_URL = "MGO_RECEIPTS_SERVICE_URL";
    public static final String MGO_RECEIPTS_SERVICE_USERNAME = "MGO_RECEIPTS_SERVICE_USERNAME";
    public static final String MGO_RECEIPTS_SERVICE_PASSWORD = "MGO_RECEIPTS_SERVICE_PASSWORD";
    public static final String SHOW_SPANISH_RCPT_DATE = "SHOW_SPANISH_RCPT_DATE";
    public static final String WAP_AGENTLOCATOR_URL = "WAP_AGENTLOCATOR_URL";
    
    
    //Agent Connect Credentials
    public static final String AGENT_CONNECT_USER_ID = "AGENT_CONNECT_USER_ID";
    public static final String AGENT_CONNECT_PASSWORD = "AGENT_CONNECT_PASSWORD";
    
    public static final String AGENT_LOCATOR_SERVICE_URL = "AGENT_LOCATOR_SERVICE_URL";
    
    
	//Properties for connection to the MSG QUEUE for listening incoming request processing messages
    public static final String MSG_QUEUE_NAME="MSG_QUEUE_NAME";
    public static final String MSG_QUEUE_MANAGER="MSG_QUEUE_MANAGER";
    public static final String MSG_QUEUE_HOSTNAME="MSG_QUEUE_HOSTNAME";
    public static final String MSG_QUEUE_PORT="MSG_QUEUE_PORT";
    public static final String MSG_QUEUE_RECEIVE_TIMEOUT_IN_MS = "MSG_QUEUE_RECEIVE_TIMEOUT_IN_MS";

    //These getters are needed for injecting URL values in root-mgoservice-proxies.xml
    
    public String getLocatorServiceUrl() {
		return getAttributeValue(AGENT_LOCATOR_SERVICE_URL);
	}
    
    public String getTransactionServiceUrl() {
		return getAttributeValue(MGO_TRANSACTION_SERVICE_URL);
	}
    
    public String getConsumerServiceUrl() {
		return getAttributeValue(MGO_CONSUMER_SERVICE_URL);
	}
    public String getReceiptsServiceUrl() {
		return getAttributeValue(MGO_RECEIPTS_SERVICE_URL);
	}
    public String getAgentConnectServiceUrl() {
        return getAttributeValue(MGOResourceConfig.AGENT_CONNECT_SERVICE_URL);
    }

    
    //This five getters are needed for injecting queue configuration values in root-context.xml
    public String getMsgQueueName() {
		return getAttributeValue(MSG_QUEUE_NAME);
	}
    
    public String getMsgQueueManager() {
		return getAttributeValue(MSG_QUEUE_MANAGER);
	}
    
    public String getMsgQueueHostName() {
		return getAttributeValue(MSG_QUEUE_HOSTNAME);
	}
    
    public String getMsgQueuePort() {
		return getAttributeValue(MSG_QUEUE_PORT);
	}
    
    public String getMsgQueueReceiveTimeout() {
		return getAttributeValue(MSG_QUEUE_RECEIVE_TIMEOUT_IN_MS);
	}

    
    @PostConstruct
    public void postConstruct() {
        MGOResourceConfig.logger.debug("mgoResourceConfig constructed: " + this.toString());
    }
    
  

}
