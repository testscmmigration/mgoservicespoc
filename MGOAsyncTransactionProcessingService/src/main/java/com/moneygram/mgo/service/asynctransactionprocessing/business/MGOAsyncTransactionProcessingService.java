package com.moneygram.mgo.service.asynctransactionprocessing.business;

import com.moneygram.mgo.service.asynctransactionprocessing.exception.MGOAsyncTransactionProcessingServiceException;

/**
 * This interface define the contract for a MGO Asynchronous Transaction processing business service.
 *
 * @author vl58
 */
public interface MGOAsyncTransactionProcessingService {

	/**
	 * Method that sends a success notification email to the user, after a transaction has been processed correctly
	 * 
	 * 
	 */
	public boolean sendSuccessNotificationEmail(Long transactionId, String userLoginId,
			String transactionLanguageCode, String partnerSiteId) throws MGOAsyncTransactionProcessingServiceException ;
	
}
