package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class State
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
