package com.moneygram.mgo.service.asynctransactionprocessing.proxy.transaction;


import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;



public interface TransactionProxy {

    public TransactionDetails getTransaction(String transactionCode) throws Exception;

	
}
