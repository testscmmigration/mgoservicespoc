package com.moneygram.mgo.service.asynctransactionprocessing.business;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Sender;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.consumer.DataConvert;
import com.moneygram.mgo.service.asynctransactionprocessing.proxy.receipts.ReceiptsProxy;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.ReceiptFields;


/**
 * Implementation of {@link ReceiptGenerator} for the international sites (MGODE, MGOUK, etc) receipt generation logic
 * 
 * @author vl58
 */
@Component("receiptGeneratorInternational")
@DependsOn("mgoResourceConfig")
public class ReceiptGeneratorInternationalImpl extends ReceiptGeneratorBase implements ReceiptGenerator {
	
	public static final String ENGLISH_GB_LANGUAGE_CODE = "en-GB";
	public static final String GERMAN_DE_LANGUAGE_CODE = "de-DE";
	public static final String ENGLISH_DE_LANGUAGE_CODE = "en-DE";
	
	@Autowired
	protected MGOResourceConfig mgoResourceConfig;
	
	@Autowired 
	protected ReceiptsProxy receiptsServiceProxy;
	
	private static final Logger logger = LoggerFactory.getLogger(ReceiptGeneratorInternationalImpl.class);

	
	@Override
	public String generateReceipt(TransactionDetails transactionDetails, Sender sender,
			String transactionLanguageCode, String partnerSiteId) throws Exception {
		String htmlReceipt = null;
		try{
			if(transactionDetails != null){
				htmlReceipt = generateReceiptContent(sender, partnerSiteId, transactionDetails);
			}
		}catch(Exception ex){
			logger.error("Error in generateReceipt: "+ex.getMessage(), ex);
			throw new Exception("Failed to generate receipt",ex);
		}
		return htmlReceipt;
	}
	
	private String generateReceiptContent(Sender sender, String partnerSiteId, TransactionDetails transactionDetails) throws Exception{
		
		logger.debug("[generateReceiptContent] Sender = ("+sender.toString()+") , partnerSiteId = " + partnerSiteId + 
				", transactionDetails = ( " + transactionDetails.toString()+")" );
		
		String htmlReceipt = null;
		try {
			//Setting up the appropriate payment method in the transaction details object
			setPaymentMethodsToTranDetails(sender, transactionDetails);
			
			//Setting up the receiver country and state info in transaction details object
			setCountryAndStateInfo(transactionDetails,sender);
			
			String userSelectedLocale = sender.getPreflanguage();
			Properties props = DataConvert.convertTranToProps(sender.getAddress(), sender.getName(),sender.getPrimaryPhone(), transactionDetails, userSelectedLocale);
			
			String state = sender.getAddress().getState().getCode();
			String tranType = props.getProperty(ReceiptFields.TRAN_TYPE);
			String receiptDate = props.getProperty(ReceiptFields.RECEIPT_DATE);
			
			logger.debug("[generateReceiptContent] userSelectedLocale = "+userSelectedLocale+" , state = " + state + 
					", tranType = " + tranType+" ,receiptDate= " + receiptDate );
			
				htmlReceipt = receiptsServiceProxy.getReceiptForTransaction( userSelectedLocale, state, tranType, receiptDate, props,partnerSiteId);
				logger.debug("[generateReceipt] language selected = "+userSelectedLocale+", receipt content = "+htmlReceipt);
			
		} catch (Exception ex) {
			logger.error("Error in generateReceiptContent: "+ex.getMessage(), ex);
			throw new Exception("Failed to generate receipt",ex);
		}		
		return htmlReceipt;
	}

}
