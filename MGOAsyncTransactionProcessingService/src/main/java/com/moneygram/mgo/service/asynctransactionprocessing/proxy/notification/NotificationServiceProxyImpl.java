/**
 * @author Carlos Rodriguez <crodriguez@moneygram.com>
 * 
 */
package com.moneygram.mgo.service.asynctransactionprocessing.proxy.notification;


// Spring Libraries
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Address;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Name;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.TransactionDetails;
import com.moneygram.mgo.service.asynctransactionprocessing.parametersloader.ParametersLoader;
import com.moneygram.mgo.service.asynctransactionprocessing.util.JMSWriter;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;
import com.moneygram.mgo.service.asynctransactionprocessing.util.StringHelper;



//@Component(value = "notificationServiceProxy")
@DependsOn("mgoResourceConfig")
public class NotificationServiceProxyImpl implements NotificationServiceProxy {

	@Autowired
    protected MGOResourceConfig mgoResourceConfig;
    
    @Autowired
    private JMSWriter jmsMessageWriter;
    
    @Autowired
    protected ParametersLoader paramsLoader;
    
    private static final Logger logger = Logger.getLogger(NotificationServiceProxyImpl.class);
    
    //Valid ISO Country codes
    private static final String DE_ISO_COUNTRY_CODE = "DEU";
    private static final String GB_ISO_COUNTRY_CODE = "GBR";
    private static final String US_ISO_COUNTRY_CODE = "USA";
    
    private static final String PUERTO_RICO = "Puerto Rico";
    
    // MESSAGE TYPES
    private static final String MSG_TYPE_TRNSND = "TRNSND";
	private static final String MSG_TYPE_TRNSNP = "TRNSNP";
	private static final String AFF_MSG_TYPE_TRNSND = "ASNDTR";
	
	private static final String CONSUMER_SITE_URL = "https://www.moneygram.com/moneygramonline";
	private static final String CONSUMER_SITE_URL_UK = "https://www.moneygram.co.uk";
	private static final String HELP_LINE_NUMBER = "1-800-922-7146";
	private static final String HELP_LINE_NUMBER_DE = "0-800-606-6019";
    private static final String CORPORATE_SITE_URL = "http://www.moneygram.com";
    private static final String EMAILHEADER_LOGO_LINK = "http://www.moneygram.com/html/emailHeader.gif";
    private static final String AFFILIATE_NAME = "MoneyGram";
    private static final String AFFILIATE_SITE_NAME = "MoneyGramOnline";
    public static final String NOTIFICATIONCONNECTIONFACTORY = "CONSUMER.NOTIFICATION.MSGING.REQ.QCF";
    public static final String NOTIFICATIONQUEUE = "CONSUMER.NOTIFICATION.MSGING.REQ";

    // MESSAGE PLACE HOLDERS KEYS
    private static final String MSG_KEY_FIRST_NAME = "firstName";
    private static final String MSG_KEY_SENDER_NAME = "senderName";
    private static final String MSG_KEY_RECEIVER_NAME = "receiverName";
    private static final String MSG_KEY_SEND_AMOUNT = "sendAmount";
    private static final String MSG_KEY_SEND_DATE = "sendDate";
    private static final String MSG_KEY_SEND_DATE_UK = "sendDateUK";
    private static final String MSG_KEY_THREE_MINUTE_PHONE_NBR = "threeMinutePhoneNbr";
    private static final String MSG_KEY_THREE_MINUTE_PIN_NBR = "threeMinutePinNbr";
    private static final String MSG_KEY_CONSUMER_SITE_URL = "consumerSiteURL";
    private static final String MSG_KEY_CONTACT_NUMBER = "contactNumber";
    private static final String MSG_KEY_AFFILIATE_LOGO = "affiliateLogo";
    private static final String MSG_KEY_AFFILIATE_NAME = "affiliateName";
    private static final String MSG_KEY_AFFILIATE_SITE_URL = "affiliateSiteUrl";
    private static final String MSG_KEY_AFFILIATE_SITE_NAME = "affiliateSiteName";
    private static final String MSG_KEY_RECEIPT_DATA = "receiptData";
    private static final String MSG_KEY_FUNDING_TYPE = "fundingType";
	private static final String MSG_KEY_WAP_RECEIVE_PROVINCE = "province";
	private static final String MSG_KEY_WAP_RECEIVE_LOCATION = "collectLocation";

    //DELIVERY OPTIONS
    public static final String MGSEND = "MGSEND";
	public static final String DSSEND = "DSSEND";
	public static final String AFF_10_MINUTES ="10-Minute Transfer";
	public static final String AFF_4_HOURS ="Transfer from Checking Account";
	
	//Receiver LOCATION message for WAP
	
	private static final String MSG_WAP_INTNAL_LOCATION=" a MoneyGram Money Transfer <a href={locatorUrl} title=\"Find a Moneygram location\">Agent Location </a> in the expected destination";
	private static final String MSG_WAP_DOMESTIC_LOCATION=" a <a href=\"http://www.walmart.com/cservice/ca_storefinder.gsp\" title=\"Find a Wallmart location\">Walmart store </a>  in {province} ";

    private static final NumberFormat df = new DecimalFormat("#,##0.00");
    
    //Send Reference Number in Transaction sent e-mails.
    private static final String MSG_TYPE_TRNSN2 = "TRNSN2";
    private static final String MSG_TYPE_ASNDT2 = "ASNDT2";
	private static final String MSG_KEY_DEST_COUNTRY = "destCountry";
	private static final String MSG_KEY_RECEIVER_STATE_LABEL = "destStateLabel";
	private static final String MSG_KEY_RECEIVER_STATE_VALUE = "destStateValue";
	private static final String MSG_REFERENCE_NUMBER = "referenceNbr";
    
    // --------------------------------- Methods -----------------------------------------//

    protected String getContentParameter(String key, String value) {
    	logger.debug( "[getContentParameter] Starting -- key = "+ key +" value = " + value );
        StringBuffer contentParameter = new StringBuffer();
        contentParameter.append("<contentParameter>");
        contentParameter.append("<name>");
        contentParameter.append(key);
        contentParameter.append("</name>");
        contentParameter.append("<value>");
        contentParameter.append(value);
        contentParameter.append("</value>");
        contentParameter.append("</contentParameter>");
        
        String contentParameterGenerated = contentParameter.toString();
        logger.debug( "[getMessageType] Ending -- content Parameter Generated = "+ contentParameterGenerated );
        return contentParameterGenerated;
    }

    protected boolean isAffiliateSite(String siteId) {
    	
    	String wap_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_WAP);
        if (!StringHelper.isNullOrEmpty(siteId ) && wap_siteId.equalsIgnoreCase(siteId)) {
        	logger.debug( "[isAffiliateSite] return true, siteId = " + siteId );
        	return true;
        }
        logger.debug( "[isAffiliateSite] return false, siteId = " + siteId );
        return false;
    }

    protected String getMessageType(String siteId, String messageTypeCode, String languageCode) {

    	logger.debug( "[getMessageType] Starting -- message type code = "+ messageTypeCode +" languageCode = " + languageCode );
    	String countryCode = getCountryCodeForSite(siteId);
        
        StringBuffer messageType = new StringBuffer();
        messageType.append("<messageType>");
        messageType.append("<type>");
        messageType.append(isAffiliateSite(siteId)?getAffiliateMessageType(messageTypeCode):messageTypeCode);
        messageType.append("</type>");
        messageType.append("<subtype>1</subtype>");
        messageType.append("<sourceSystem>MGO</sourceSystem>");
        messageType.append("<countryCode>");
        messageType.append(countryCode);
        messageType.append("</countryCode>");
        messageType.append("<languageCode>");
        messageType.append(languageCode);
        messageType.append("</languageCode>");
        messageType.append("</messageType>");
        String messageTypeGenerated = messageType.toString();
        logger.debug( "[getMessageType] Ending -- message type generated = "+ messageTypeGenerated );
        return messageTypeGenerated;
    }
    
    /**
     * Returns the appropriate ISO country code base in the siteId
     * @param siteId
     * @return
     */
    private String getCountryCodeForSite(String siteId) {
    	
    	String WAPSiteId   = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_WAP);
    	String MGOUSSiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUS);
    	String MGOUKSiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUK);
    	
    	if(	WAPSiteId.equalsIgnoreCase(siteId) || MGOUSSiteId.equalsIgnoreCase(siteId) ){
    		logger.debug( "[getCountryCodeForSite] siteId = "+ siteId + ", country code = " + US_ISO_COUNTRY_CODE);
    		return US_ISO_COUNTRY_CODE;
    	}
    	else if(MGOUKSiteId.equalsIgnoreCase(siteId) ){
    		logger.debug( "[getCountryCodeForSite] siteId = "+ siteId + ", country code = " + GB_ISO_COUNTRY_CODE);
    		return GB_ISO_COUNTRY_CODE;
        }
    	else{
    		logger.debug( "[getCountryCodeForSite] siteId = "+ siteId + ", country code = " + DE_ISO_COUNTRY_CODE);
    		return DE_ISO_COUNTRY_CODE;
    	}
		
	}

	protected String getAffiliateMessageType(String msgType) {
		String newMsgType = null;
		if(!msgType.equals(MSG_TYPE_ASNDT2)){
			if (msgType.equals(MSG_TYPE_TRNSND)) {
				newMsgType = AFF_MSG_TYPE_TRNSND;
	    	}
	    	if (msgType.equals(MSG_TYPE_TRNSNP)) {
	    		newMsgType = AFF_MSG_TYPE_TRNSND;
	    	}
		}else{
			newMsgType = msgType;
		}		
    	return newMsgType;
    }

    /**
     * @param siteId
     *            , current siteId
     * @param emailAddress
     *            , sender's emailAddress
     * @param issuerEmail
     *            , email of the corresponding site(e,g mymoneygram aplication email or walmart aplication email )
     * @return the well form tag services that has to be add to the queue message
     */
    protected String getServices(String receiverEmailAddress, String siteId) {
        // create and append the service tag to the XML message
    	logger.debug( "[getServices] Starting -- receiverEmailAddress = "+ receiverEmailAddress +" Site Id = " + siteId );
        String senderEmailKey = null;
        if (isAffiliateSite(siteId)) {
        	senderEmailKey = mgoResourceConfig.getAttributeValue(MGOResourceConfig.WAP_SENDER_EMAIL_ADDRESS);
        } else {
        	senderEmailKey = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SENDER_EMAIL_ADDRESS);
        }
        if(StringHelper.isNullOrEmpty(senderEmailKey)){
        	senderEmailKey ="no-reply.moneygramonline@moneygram.com";
        }

        StringBuffer services = new StringBuffer();
        services.append("<services>");
        services.append("<service type=\"E-MAIL\">");
        services.append("<sender>");
        services.append(senderEmailKey);
        services.append("</sender>");
        services.append("<receiver>");
        services.append(receiverEmailAddress);
        services.append("</receiver>");
        services.append("</service>");
        services.append("</services>");
        
        String servicesGenerated = services.toString();
        logger.debug( "[getMessageType] Ending -- services generated = "+ servicesGenerated );
        return servicesGenerated;
    }

    protected void writeMessageToQueue(String message) throws Exception {
    	logger.debug( "[writeMessageToQueue] Starting" );
        try {
            jmsMessageWriter.writeMessage(message);
            logger.debug( "[writeMessageToQueue] Message wrote in the queue Successfully" );
        } catch (Exception e) {
        	logger.error("[writeMessageToQueue]error writting message to queue: " + e.getMessage());
            throw e;
        }
    }

    /**
     * This method create and write a message to the queue
     * 
     * @param siteId
     *            , current site identifier
     * @param languageCode
     *            , language code for the current locale
     * @param countryCode
     *            , country code for the current locale
     * @param messageType
     *            , Identifier of the message that has to be send to the notification service
     * @param emailAddress
     *            , receiver Email Address
     * @param issuerEmail
     *            , sender Email Address
     * @param contentParameters
     *            , Map<contentParameterName, contentParameterValue> for example <"fistName","Carlos">
     * @param language 
     */
    private void sendMessage(String siteId, String messageType, String emailAddress, HashMap<String, String> contentParameters, String language) {
        try {
            String message = createMessage(siteId, messageType, emailAddress,contentParameters, language);
            writeMessageToQueue(message);
        } catch (Exception e) {
           logger.error("[sendMessage] Exception in SendMessage, e = " + e.getMessage());
        }
    }

     /**
     * 
     * @param siteId
     *            , current site identifier
     * @param messageType
     *            , Identifier of the message that has to be send to the notification service
     * @param emailAddress
     *            , receiver Email Address
     * @param issuerEmail
     *            , sender Email Address
     * @param contentParameters
     *            , Map<contentParameterName, contentParameterValue> for example <"fistName","Carlos">
     * @param language 
     * @return the message that has to be write in the notification service queue
     * @throws Exception 
     */
    protected String createMessage(String siteId, String messageType,
            String emailAddress, HashMap<String, String> contentParameters, String language) throws Exception {
        String message = "";
    	String MGODESiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGODE);
        try {
            StringBuffer messageBuffer = new StringBuffer();
            messageBuffer.append("<MessageInfo>");
            messageBuffer.append(getMessageType(siteId, messageType,language));
            messageBuffer.append(getServices(emailAddress, siteId));
            messageBuffer.append("<contentParameters>");
            for (Entry<String, String> entry : contentParameters.entrySet()) {
                messageBuffer.append(getContentParameter(entry.getKey(), entry.getValue()));
            }
            //Adding Consumer site URL
            String MGOUKSiteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_MGOUK);
    		String siteUrl = CONSUMER_SITE_URL;
    		if (MGOUKSiteId.equals(siteId)) {
    			siteUrl = CONSUMER_SITE_URL_UK;
    		}
            messageBuffer.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, siteUrl));
           // Adding help line number
            String helpLineNumber = HELP_LINE_NUMBER;
    		if(MGODESiteId.equals(siteId)){
    			helpLineNumber = HELP_LINE_NUMBER_DE;
    		}
    		messageBuffer.append(getContentParameter(MSG_KEY_CONTACT_NUMBER, helpLineNumber));
           //Adding Affiliate parameters
    		if (isAffiliateSite(siteId)) {
                messageBuffer.append(getAffiliateParameters());
            }
            messageBuffer.append("</contentParameters>");
            messageBuffer.append("</MessageInfo>");
            message = messageBuffer.toString();
            return message;
        } catch (Exception e) {
        	logger.error("[createMessage]error creating the email message:" + e.getLocalizedMessage());
			throw e;
        }

    }

   
    
    /**
     * this method create and return specific parameters that are need for Walmart emails
     * 
     * @return
     */
    private String getAffiliateParameters() {
        StringBuffer parms = new StringBuffer();
        parms.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_URL, CORPORATE_SITE_URL));
        parms.append(getContentParameter(MSG_KEY_CONTACT_NUMBER, HELP_LINE_NUMBER));
        parms.append(getContentParameter(MSG_KEY_AFFILIATE_LOGO, EMAILHEADER_LOGO_LINK));
        parms.append(getContentParameter(MSG_KEY_AFFILIATE_NAME, AFFILIATE_NAME));
        parms.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_NAME, AFFILIATE_SITE_NAME));
        return parms.toString();
    }
    
    /**
     * 
     * @param firstName
     * @param middleName
     * @param lastName
     * @return
     */
    private String buildFullName(String firstName, String middleName, String lastName) {
		if (!StringHelper.isNullOrEmpty(middleName)) {
			return firstName + " " + middleName + " " + lastName;
		} else {
			return firstName + " " + lastName;
		}
	}
     
    /**
     * 
     * @param mgoProductType
     * @param siteId
     * @return
     */
    private String getFundingType(String mgoProductType, String siteId)
	{
		if (isAffiliateSite(siteId))
		{
			if(mgoProductType.equals(MGSEND))
			{
				return AFF_10_MINUTES;
			}
			else if( mgoProductType.equals(DSSEND))
			{
				return AFF_4_HOURS;
			}
		}
		return mgoProductType;
	}
    
    /**
     * 
     * @param tran
     * @return
     */
    private String getReceiveProvince(TransactionDetails tran) {
    	
		String countryCode = tran.getReceiver().getAddress().getCountry().getCode();
		String countryName =  tran.getReceiver().getAddress().getCountry().getName();
		if (!StringHelper.isNullOrEmpty(countryCode)
				&& countryCode.equals(US_ISO_COUNTRY_CODE)) {
			String stateCode =tran.getReceiver().getAddress().getState().getCode();
			String stateName = tran.getReceiver().getAddress().getState().getName();
			if(!StringHelper.isNullOrEmpty(stateName)){
				return stateName;
			}else{
				return stateCode;
			}
//		} else if (isAffiliateSite(tran.getPartnerSiteId())){
//			return PUERTO_RICO;
		}else if(!StringHelper.isNullOrEmpty(countryName)){
			return countryName;
		}else{
			return countryCode;
		}
	}
    
    /**
	 * 
	 * @param tran
	 * @param siteId
	 * @param name
	 * @param emailAddress
	 * @param language
     * @param receiptData 
	 */
    public void notifySentTransaction(TransactionDetails tran, String siteId, Name name, String emailAddress, String language, String receiptData) {
        logger.debug("[notifySentTransaction] Starting ");
    	HashMap<String, String> contentParameters = new HashMap<String, String>();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d hh:mm:ss a z yyyy");
    	//added by Ankit bhatt for Defect# 415 - Display time in CEST for MGODE
    	if("MGODE".equalsIgnoreCase(tran.getPartnerSiteId())) {
    		dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam")); //CEST Time zone for displaying in CEST format in Email   
    	}
    	
    	if("MGOUK".equalsIgnoreCase(tran.getPartnerSiteId())) {
    		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    	}
    	//ended
    	    	
    	String tranDateFormatted = dateFormat.format( new Date());    	
    	String tranDateFormattedUK = "";
    	    	
    	if(tran.getTransactionLog().getDateApproved() != null ){
    		tranDateFormatted = String.valueOf(dateFormat.format( tran.getTransactionLog().getDateApproved()));
    		tranDateFormattedUK = tranDateFormatted;
		}
		
		String senderName = buildFullName(name.getFirstName(), null, name.getLastName());
		
		Name receivername = tran.getReceiver().getName();
		String receiverName = buildFullName(receivername.getFirstName(),receivername.getMiddleName(), receivername.getLastName());
		
        contentParameters.put(MSG_KEY_FIRST_NAME , name.getFirstName());
        contentParameters.put(MSG_KEY_SENDER_NAME , senderName);
        contentParameters.put(MSG_KEY_RECEIVER_NAME, receiverName);
        contentParameters.put(MSG_KEY_SEND_AMOUNT, df.format(tran.getTransactionAmount().getSendAmount().getValue()));
        contentParameters.put(MSG_KEY_SEND_DATE, tranDateFormatted);
        contentParameters.put(MSG_KEY_SEND_DATE_UK, tranDateFormattedUK);
        contentParameters.put(MSG_KEY_FUNDING_TYPE, getFundingType(tran.getTransactionTypeCode(),siteId));
        contentParameters.put(MSG_KEY_WAP_RECEIVE_PROVINCE, getReceiveProvince(tran));
        Address receiverAddress = tran.getReceiver().getAddress();
        String destStateLabel="";
        String destStateValue="";
        
        contentParameters.put(MSG_KEY_DEST_COUNTRY,paramsLoader.getCountryByCode(receiverAddress.getCountry().getCode()).getName());
        
        if(receiverAddress.getState().getCode() != null && receiverAddress.getState().getName() == null){
        receiverAddress.getState().setName(
                paramsLoader.getStateByCode(receiverAddress.getState().getCode(), receiverAddress.getCountry().getCode()).getName()); 
        }
        
        if(receiverAddress.getState().getCode() != null && receiverAddress.getState().getName() != null){
        	destStateValue = receiverAddress.getState().getName();
        	if (destStateValue.isEmpty());
        	else{
        		destStateLabel = getReceiverStateLabelText(receiverAddress.getCountry().getCode(),language) + ":" ; 
        	}
        }        
        
        contentParameters.put(MSG_KEY_RECEIVER_STATE_LABEL, destStateLabel);
        contentParameters.put(MSG_KEY_RECEIVER_STATE_VALUE, destStateValue);

        //New conditional for international and domestic sends in WAP
        String countryCode = tran.getReceiver().getAddress().getCountry().getCode();
        String receiverLocationMessage = "";
		if (!StringHelper.isNullOrEmpty(countryCode)
				&& countryCode.equals(US_ISO_COUNTRY_CODE)) {
			receiverLocationMessage = MSG_WAP_DOMESTIC_LOCATION;
			receiverLocationMessage = receiverLocationMessage.replace("{province}",  getReceiveProvince(tran));
		}else{
			receiverLocationMessage = MSG_WAP_INTNAL_LOCATION;
			  
			receiverLocationMessage = receiverLocationMessage.replace("{locatorUrl}",  mgoResourceConfig.getAttributeValue(MGOResourceConfig.WAP_AGENTLOCATOR_URL));
		}
		contentParameters.put(MSG_KEY_WAP_RECEIVE_LOCATION,StringEscapeUtils.escapeHtml(receiverLocationMessage));
        
        String messageType = MSG_TYPE_TRNSND;
        if( (tran.getThreeMinuteFreePhoneNumber()!= null && !tran.getThreeMinuteFreePhoneNumber().equals("")) &&
			(tran.getThreeMinuteFreePinNumber()!= null &&!(tran.getThreeMinuteFreePinNumber()).equals("")) ){
        	 	
        		messageType = MSG_TYPE_TRNSNP;
        	 	contentParameters.put(MSG_KEY_THREE_MINUTE_PHONE_NBR, tran.getThreeMinuteFreePhoneNumber());
        	 	contentParameters.put(MSG_KEY_THREE_MINUTE_PIN_NBR, tran.getThreeMinuteFreePinNumber());
        }
        if(receiptData !=null){
        	contentParameters.put(MSG_KEY_RECEIPT_DATA, StringEscapeUtils.escapeXml(receiptData) );
        }
        
        sendMessage( siteId, messageType , emailAddress, contentParameters,language );
        
		messageType = MSG_TYPE_TRNSN2;
		String wap_siteId = mgoResourceConfig.getAttributeValue(MGOResourceConfig.SITE_IDENTIFIER_WAP);
        if (!StringHelper.isNullOrEmpty(siteId) && wap_siteId.equalsIgnoreCase(siteId)) {        	 	
	        messageType = MSG_TYPE_ASNDT2;	        	 	
	    }
        
        contentParameters.put(MSG_REFERENCE_NUMBER,tran.getReferenceNumber());
        
        sendMessage( siteId, messageType , emailAddress, contentParameters,language );       
        
        logger.debug("[notifySentTransaction] Ending");
    }
    private String getReceiverStateLabelText(String isoCountryCode,String language){
    	String receiverStateLabel = "Receiver State/Province";
    	if("de-DE".equalsIgnoreCase(language)){ //modified  - if communication preference is German, label should be in German.
    		receiverStateLabel = "Staat/Provinz des Empf�nger";
    	}
		return receiverStateLabel;    	
    }

   
    
    
}
