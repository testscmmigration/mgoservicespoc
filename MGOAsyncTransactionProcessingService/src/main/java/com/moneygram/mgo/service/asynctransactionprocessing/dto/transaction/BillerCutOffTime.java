package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.rmi.Remote;
import java.util.Calendar;
import java.util.HashMap;


public class BillerCutOffTime
        implements Remote, Serializable {

    private static final long serialVersionUID = 1L;
    public static final int SUN = 1;
    public static final int MON = 2;
    public static final int TUE = 3;
    public static final int WED = 4;
    public static final int THU = 5;
    public static final int FRI = 6;
    public static final int SAT = 7;

    private HashMap<Integer, String> cutOffTimeMap = new HashMap<Integer, String>();

    /**
     * Adds text entry for the given day of week specified.
     * 
     * @param dow
     * @param cutOffTimeText
     * @throws Exception
     */
    public void addCutOffTimeEntry(int dow, String cutOffTimeText) throws Exception {
        if (dow < 1 || dow > 7) {
            throw new Exception("Day of week entry must be 1-7");
        }
        cutOffTimeMap.put(dow, cutOffTimeText);
    }

    /**
     * Will return the text associated with the day of week for Calendar object 'cal'. If the text for this day of week
     * is null, a null will be returned.
     * 
     * @param cal
     * @return
     */
    public String getEndOfDayString(Calendar cal) {
        return getCutOffTimeString(cal.get(Calendar.DAY_OF_WEEK));
    }

    /**
     * Will return the text associated with the day of week.
     * 
     * @return
     */
    public String getEndOfDayString() {
        return getEndOfDayString(Calendar.getInstance());
    }

    public String getCutOffTimeString(int dow) {
        return cutOffTimeMap.get(new Integer(dow));
    }
}
