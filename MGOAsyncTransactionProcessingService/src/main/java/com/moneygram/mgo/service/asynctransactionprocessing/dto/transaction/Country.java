package com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction;


import java.io.Serializable;
import java.math.BigInteger;
import java.rmi.Remote;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


public class Country
        implements Remote, Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private String code;
    private String name;
    private boolean sendActive = false;
    private boolean recvActive = false;
    private Set<Currency> sendCurrencies;
    private List<EstimatorService> recvServices;
    private Set<String> recvServicesKeySet = null;
    private boolean recvOnlineBlocked = false;
    private String countryLegacyCode;
    private boolean rewardsSupported = false;

    public Set<String> getRecvServicesKeySet() {
        return recvServicesKeySet;
    }

    public void setRecvServicesKeySet(Set<String> recvServicesKeySet) {
        this.recvServicesKeySet = recvServicesKeySet;
    }

    public boolean isRewardsSupported() {
        return rewardsSupported;
    }

    public void setRewardsSupported(boolean rewardsSupported) {
        this.rewardsSupported = rewardsSupported;
    }

    public String getCountryLegacyCode() {
        return countryLegacyCode;
    }

    public void setCountryLegacyCode(String countryLegacyCode) {
        this.countryLegacyCode = countryLegacyCode;
    }

    public String getCurrencyCode() {
        return (String) sendCurrencies.toArray()[0];
    }

    public String getCurrenciesJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Currency> it = sendCurrencies.iterator();
        while (it.hasNext()) {
            sb.append(it.next().toJson());

            if (it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSendActive() {
        return sendActive;
    }

    public void setSendActive(boolean sendActive) {
        this.sendActive = sendActive;
    }

    public boolean isRecvActive() {
        return recvActive;
    }

    public void setRecvActive(boolean recvActive) {
        this.recvActive = recvActive;
    }

    public Set<Currency> getSendCurrencies() {
        return sendCurrencies;
    }

    public void setSendCurrencies(Set<Currency> sendCurrencies) {
        this.sendCurrencies = sendCurrencies;
    }

    public void addSendCurrency(Currency currency) {
        if (currency == null) {
            return;
        }
        if (sendCurrencies == null) {
            sendCurrencies = new HashSet<Currency>();
        }
        sendCurrencies.add(currency);
    }

    public Set<String> getSendCurrencyCodeSet() {
        Set<String> set = new HashSet<String>();
        for (Currency c : sendCurrencies) {
            set.add(c.getCode());
        }
        return set;
    }

    public Set<String> getSendCurrencyNameSet() {
        Set<String> set = new HashSet<String>();
        for (Currency c : sendCurrencies) {
            set.add(c.getName());
        }
        return set;
    }

    public List<EstimatorService> getRecvServices() {
        return recvServices;
    }

    public void setRecvServices(List<EstimatorService> recvServices) {
        this.recvServices = recvServices;
    }

    public List<EstimatorService> getOnlineRecvServices() {
        List<EstimatorService> onlineRecvServices = new ArrayList<EstimatorService>();
        if (recvServices != null) {
            for (EstimatorService service : recvServices) {
                if (service.isAvailableForMgo()) {
                    onlineRecvServices.add(service);
                }
            }
        }
        return onlineRecvServices;
    }

    /**
     * Returns the service by the service code.
     * 
     * @param code
     *            service code
     * @return
     */
    public EstimatorService getEstimatorService(String code) {
        EstimatorService selected = null;
        List<EstimatorService> services = getRecvServices();

        if ((services != null) && (code != null)) {
            int index = code.indexOf(":");
            String currency = null;
            if (index > 0) {
                // take the 1st segment of WILL_CALL:MXN:
                String[] codes = code.split(":");
                if (codes.length > 0) {
                    code = codes[0];
                }
                if (codes.length > 1) {
                    currency = codes[1];
                }
            }
            // System.out.println(getClass().getName() + ".getEstimatorService: code="+ code +" currency="+ currency);
            for (EstimatorService service : services) {
                if (code.equals(service.getServiceCode())
                        && ((currency == null) || currency.equals(service.getCurrencyCode()))) {
                    selected = service;
                    break;
                }
            }

        }
        return selected;
    }

    public void addRecvService(String serviceCode, String serviceName, String currencyCode, BigInteger serviceID,
            String receiveAgentID, String mgManaged, String agentManaged, String validationExprs, String checkDigitAlg,
            String serviceLabel, Currency currency, String receiveAgentName, boolean validMgoService) {
        if ((serviceCode == null) || (serviceName == null) || (currencyCode == null)) {
            return;
        }
        if (recvServices == null) {
            recvServices = new ArrayList<EstimatorService>();
            recvServicesKeySet = new HashSet<String>();
        }
        if (recvServicesKeySet.contains(serviceCode + ":" + currencyCode + ":" + receiveAgentID)) {
            return;
        }
        recvServicesKeySet.add(serviceCode + ":" + currencyCode + ":" + receiveAgentID);
        EstimatorService service = new EstimatorService();
        service.setServiceCode(serviceCode);
        service.setServiceName(serviceName);
        service.setCurrencyCode(currencyCode);
        service.setCurrency(currency);
        service.setServiceID(serviceID);
        if (receiveAgentID != null) {
            service.setReceiveAgentID(receiveAgentID);
        }
        service.setReceiveAgentName(receiveAgentName);
        service.setMgManaged(mgManaged);
        service.setAgentManaged(agentManaged);
        service.setValidationExprs(validationExprs);
        service.setCheckDigitAlg(checkDigitAlg);
        service.setServiceLabel(serviceLabel);
        if (receiveAgentID != null) {
            service.setFullyQualifiedDelivaryOption(true);
        } else {
            service.setFullyQualifiedDelivaryOption(false);
        }
        service.setAvailableForMgo(validMgoService);
        recvServices.add(service);
    }

    public void setRecvOnlineBlocked(boolean recvOnlineBlocked) {
        this.recvOnlineBlocked = recvOnlineBlocked;
    }

    public boolean isRecvOnlineBlocked() {
        return recvOnlineBlocked;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * Returns true if country has states.
     * 
     * @param countryCode
     *            3 char country code
     * @return true if country has states.
     */
    public static boolean isStatesAvailable(String countryCode) {
        return "USA".equals(countryCode) || "MEX".equals(countryCode) || "CAN".equals(countryCode);
    }

    public boolean isStatesAvailable() {
    	return isStatesAvailable(code);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        // Country clonedCountry = new Country();
        // clonedCountry.setCode(this.code);
        // clonedCountry.setName(this.name);
        // clonedCountry.setCountryLegacyCode(this.countryLegacyCode);
        // clonedCountry.setRecvActive(this.recvActive);
        // clonedCountry.setRecvOnlineBlocked(this.recvOnlineBlocked);
        // clonedCountry.setRewardsSupported(this.rewardsSupported);
        // clonedCountry.setSendActive(this.sendActive);
        // List<EstimatorService> servicesList = this.recvServices;
        // List<EstimatorService> cloneServicesList = servicesList;
        // if ((servicesList != null) && !servicesList.isEmpty()) {
        // cloneServicesList = new ArrayList<EstimatorService>();
        // for (EstimatorService estimatorService : servicesList) {
        // cloneServicesList.add(estimatorService);
        // }
        // }
        // clonedCountry.setRecvServices(cloneServicesList);
        // Set<Currency> currenciesSet = this.sendCurrencies;
        // Set<Currency> cloneCurrenciesSet = currenciesSet;
        // if ((currenciesSet != null) && !currenciesSet.isEmpty()) {
        // cloneCurrenciesSet = new HashSet<Currency>();
        // for (Currency currency : currenciesSet) {
        // cloneCurrenciesSet.add(currency);
        // }
        // }
        // clonedCountry.setSendCurrencies(cloneCurrenciesSet);
        // clonedCountry.setRecvServicesKeySet(this.recvServicesKeySet);
        return super.clone();
    }

}
