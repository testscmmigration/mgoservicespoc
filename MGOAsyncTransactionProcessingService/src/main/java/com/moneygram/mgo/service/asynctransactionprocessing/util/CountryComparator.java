package com.moneygram.mgo.service.asynctransactionprocessing.util;


import java.util.Comparator;

import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;



public class CountryComparator
        implements Comparator<Object> {
    public int compare(Object object1, Object object2) {
        String countryName1 = ((Country) object1).getName();
        String countryName2 = ((Country) object2).getName();
        return countryName1.compareTo(countryName2);
    }
}
