package com.moneygram.mgo.service.asynctransactionprocessing.proxy.agentlocator;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.moneygram.mgo.service.asynctransactionprocessing.cache.CacheService;
import com.moneygram.mgo.service.asynctransactionprocessing.cache.LocatorCache;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.Country;
import com.moneygram.mgo.service.asynctransactionprocessing.dto.transaction.State;
import com.moneygram.mgo.service.asynctransactionprocessing.util.MGOResourceConfig;




@Component(value = "agentLocatorServiceImpl")
public class AgentLocatorServiceImpl
        implements AgentLocatorService {

    private static final Logger logger = Logger.getLogger(AgentLocatorServiceImpl.class);

    private String locatorCacheName = "MGOAsyncTxProcLocatorCache";
    
    @Autowired
    private MGOResourceConfig mgoResourceConfig;


    @Autowired
    private CacheService<? extends Serializable> cacheService;
    
    @PostConstruct
    private void init(){
    	try{
    	  logger.debug("Locator cache name: "+locatorCacheName);
    	}catch(Exception ex){
    		logger.error("Exception in Agent Locator Service:" + ex.getMessage(),ex);
    	}
    }

    @SuppressWarnings("unchecked")
    public Map<String, State> getStates(String country)throws Exception {
        try {
            Map<String, Map<String, State>> statesMap = (Map<String, Map<String, State>>) cacheService
                .getCacheElementAttribute(locatorCacheName, LocatorCache.COUNTRIES_CACHE_ELEMENT_NAME,
                    LocatorCache.STATES_KEY_NAME);
            Map<String, State> states = statesMap.get(country);

            return states;
        } catch (Exception e) {
            logger.error("Exception in Agent Locator; count not get states for country:" + country);
            throw new Exception("Exception in Agent Locator; count not get States for country:" + country, e);
        }
    }

//    @SuppressWarnings("unchecked")
//    public HashMap<String, Services> getCountryServices() {
//        try {
//            HashMap<String, Services> countryServices = (HashMap<String, Services>) cacheService
//                .getCacheElementAttribute(locatorCacheName, LocatorCache.COUNTRIES_CACHE_ELEMENT_NAME,
//                    LocatorCache.COUNTRY_SERVICES_KEY_NAME);
//
//            return countryServices;
//        } catch (Exception e) {
//            log.error("Exception in Agent Locator; count not get services for all countries");
//            throw new Failure("Exception in Agent Locator; count not get services for all countries", e);
//        }
//    }

    @SuppressWarnings("unchecked")
    public List<Country> getCountries() throws Exception {
    	
        try {
            List<Country> countries = (List<Country>) cacheService.getCacheElementAttribute(locatorCacheName,
                LocatorCache.COUNTRIES_CACHE_ELEMENT_NAME, LocatorCache.COUNTRIES_KEY_NAME);

            return countries;
        } catch (Exception e) {
            logger.error("Exception in Agent Locator; count not get all countries");
            throw new Exception("Exception in Agent Locator; count not get all countries", e);
        }
    }

}