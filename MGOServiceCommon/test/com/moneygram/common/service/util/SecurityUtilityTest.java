/*
 * Created on Aug 20, 2009
 *
 */
package com.moneygram.common.service.util;

import junit.framework.TestCase;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * 
 * Security Utility Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/20 19:15:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SecurityUtilityTest extends TestCase {
    private static final Logger logger = LogFactory.getInstance().getLogger(SecurityUtilityTest.class);
    
    private static final String SECURE_HASH_SEED = "k@#D$%^A*&a";
    private static final String PUBLIC_RSA_KEY = "test/test.crt.der";

    public void testPasswordDigest() throws Exception {
        String encrypted = SecurityUtility.digest("mypswd");
        if (logger.isDebugEnabled()) {
            logger.debug("testPasswordDigest: encrypted="+ encrypted);
        }
        assertNotNull("Expected not null encrypted value", encrypted);
    }

    public void testBankAccountDigest() throws Exception {
        String encrypted = SecurityUtility.digest("1234567890", SECURE_HASH_SEED);
        if (logger.isDebugEnabled()) {
            logger.debug("testBankAccountDigest: encrypted="+ encrypted);
        }
        assertNotNull("Expected not null encrypted value", encrypted);
    }

    public void testBankAccountEncrypt() throws Exception {
        String encrypted = SecurityUtility.encrypt("1234567890", PUBLIC_RSA_KEY);
        if (logger.isDebugEnabled()) {
            logger.debug("testBankAccountEncrypt: encrypted="+ encrypted);
        }
        assertNotNull("Expected not null encrypted value", encrypted);
    }

}
