/*
 * Created on Aug 6, 2009
 *
 */
package com.moneygram.common.service.util;

import java.util.Map;

import com.moneygram.common.service.util.ResourceConfigFactory;
import com.moneygram.ree.lib.Config;


/**
 * 
 * Test Resource Config Factory.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>RSAService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/13 21:29:21 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TestResourceConfigFactory extends ResourceConfigFactory {
    private Map attributes = null;

    public TestResourceConfigFactory(Map attributes) {
        this.attributes = attributes;
    }

    @Override
    public Config createResourceConfiguration(String jndi) {
        return new TestConfig(attributes);
    }
    
}
