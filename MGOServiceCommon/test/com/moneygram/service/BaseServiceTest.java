/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.service;

import junit.framework.TestCase;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.common.service.RequestRouter;

/**
 * Base Service Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/06/24 19:23:54 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseServiceTest extends TestCase {

    /**
     * Creates new instance of BaseServiceTest
     */
    public BaseServiceTest() {
        super();
    }

    /**
     * 
     * Creates new instance of BaseServiceTest
     * @param method
     */
    public BaseServiceTest(String method) {
        super(method);
    }

    protected ApplicationContext getContext() {
        return new ClassPathXmlApplicationContext(getContextResources());
    }

    protected String[] getContextResources() {
        return new String[] { "TestContext.xml" };
    }

    /**
     * Returns configured request router instance. 
     * @return
     */
    protected RequestRouter getRequestRouter() {
        ApplicationContext context = getContext(); 
        RequestRouter requestRouter = (RequestRouter)context.getBean(RequestRouter.REQUEST_ROUTER);
        return requestRouter;
    }

}