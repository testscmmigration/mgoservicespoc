/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.service.command;

import com.moneygram.common.service.BaseOperationRequest;

/**
 * SelectRequest
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/06/18 22:07:12 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SelectRequest extends BaseOperationRequest {

}
