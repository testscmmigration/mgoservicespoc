/*
 * Created on Aug 28, 2009
 *
 */
package com.moneygram.service.command;

import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;

/**
 * 
 * Error Test Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/28 20:17:05 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ErrorTestCommand extends TransactionalCommand {

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof UpdateRequest;
    }

    @Override
    protected OperationResponse process(OperationRequest request) throws CommandException {
        UpdateRequest updateRequest = (UpdateRequest)request;
        
        if (updateRequest.getCode() == null) {
            throw new DataFormatException("Required code is missing");
        }

        if (updateRequest.getCode().length() > 0) {
            throw new BusinessRulesException("Code already exists");
        }
        
        return null;
    }

}
