/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.service.command;

import com.moneygram.common.service.BaseOperationRequest;

/**
 * Update Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/08/28 20:16:36 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class UpdateRequest extends BaseOperationRequest {
    private String code = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    @Override
    public String toString() {
        return super.toString() +" Code="+ getCode();
    }
    
}
