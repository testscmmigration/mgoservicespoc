/*
 * Created on Oct 8, 2009
 *
 */
package com.moneygram.service.command;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;

/**
 * 
 * Test Simulated Mode Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/10/08 21:35:19 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SimulatedModeTestCommand extends TransactionalCommand {

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return true;
    }

    @Override
    protected OperationResponse process(OperationRequest request) throws CommandException {
        return null;
    }

    @Override
    public boolean isInSimulatedMode() throws CommandException {
        return true;
    }
}
