/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.service.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.service.dao.TestDAO;

/**
 * Select Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/08/19 13:55:29 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SelectCommand extends ReadCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(SelectCommand.class);
    
    private TestDAO dao = null;
    
    /**
     * 
     * @see test.spring.BaseCommand#processRequest(test.ServiceRequest.Request)
     */
    public OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("execute: request="+ request);
        }

        SelectRequest selectRequest = (SelectRequest) request;
        
        TestDAO dao = (TestDAO)getDataAccessObject();
        
        List list = dao.testSelect();

        if (logger.isDebugEnabled()) {
            logger.debug("execute: retrieved list="+ list);
        }
        
        return null;
    }

    @Override
    protected boolean isRequestSupported(OperationRequest request) throws CommandException {
        return request instanceof SelectRequest;
    }

}
