/*
 * Created on Oct 8, 2009
 *
 */
package com.moneygram.service;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.service.command.SelectRequest;

/**
 * 
 * Simulated Mode Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/10/08 21:35:19 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SimulatedModeTest extends BaseServiceTest {
    private static final Logger logger = LogFactory.getInstance().getLogger(SimulatedModeTest.class);
    
    public void testSimulatedMode() {
        RequestRouter commandRouter = getRequestRouter();

        //not simulated request sent to command that is not in simulated mode
        ProcessingInstruction processingInstruction = new ProcessingInstruction("select");
        SelectRequest selectRequest = new SelectRequest();
        selectRequest.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        Exception exception;
        
        exception = null;
        try {
            commandRouter.process(selectRequest);
        } catch (CommandException e) {
            exception = e;
            if (logger.isDebugEnabled()) {
                logger.debug("testSimulatedMode: caught exception", e);
            }
        }

        assertNull("Unexpected exception", exception);
        
        //simulated request sent to command that is not in simulated mode
        processingInstruction.setSimulatedModeAction("simulated");
        
        exception = null;
        try {
            commandRouter.process(selectRequest);
        } catch (CommandException e) {
            exception = e;
            if (logger.isDebugEnabled()) {
                logger.debug("testSimulatedMode: caught exception", e);
            }
        }

        assertNotNull("Expected an exception", exception);

        //not simulated request sent to command that is in simulated mode
        processingInstruction.setAction("simulatedModeTest");
        processingInstruction.setSimulatedModeAction(null);
        
        exception = null;
        try {
            commandRouter.process(selectRequest);
        } catch (CommandException e) {
            exception = e;
            if (logger.isDebugEnabled()) {
                logger.debug("testSimulatedMode: caught exception", e);
            }
        }

        assertNotNull("Expected an exception", exception);

    }
}
