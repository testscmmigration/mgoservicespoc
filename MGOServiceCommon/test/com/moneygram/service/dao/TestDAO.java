/*
 * Created on Apr 2, 2008
 *
 */
package com.moneygram.service.dao;

import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.moneygram.common.dao.BaseDAO;
import com.moneygram.common.dao.SingleObjectRowMapper;


/**
 * Test DAO.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2008</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/06/22 22:03:53 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class TestDAO extends BaseDAO {
    
    /**
     * 
     *
     */
    public List testSelect() {
        String sql = null;
        Object[] parms = null;
        
        sql = "select 7 from dual";
        int count = getJdbcTemplate().queryForInt(sql);
        System.out.println(this.getClass().getName()+": count="+ count);

        parms = new Object[]{"SENT", new Integer(10)};
        sql = "select notf_msg_id, RECIPNT_CNTCT_INFO_TEXT ELEC_ADDR, VNDR_MSG_ID VNDR_MSG_ID, NOTF_MSG_STAT_CODE STAT_CODE from notification_msg where NOTF_MSG_STAT_CODE = ? and rownum <= ?";
        List list = getJdbcTemplate().query(sql, parms, new SingleObjectRowMapper(2));
        System.out.println(this.getClass().getName()+": list="+ list);
        
        return list;
    }

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
        TestDAO testDAO = (TestDAO)context.getBean("testDAO");
        System.out.println("main: testDAO.getJdbcTemplate="+ testDAO.getJdbcTemplate());
        System.out.println("main: testDAO.getType="+ testDAO.getType());
        testDAO.testSelect();
    }
    
}
