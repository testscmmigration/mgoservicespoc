/*
 * Created on Aug 18, 2009
 *
 */
package com.moneygram.service;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BaseOperationRequest;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.ClientIntegrationException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.service.command.SelectRequest;
import com.moneygram.service.command.UpdateRequest;

/**
 * 
 * Error Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/08/28 20:17:05 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ErrorTest extends BaseServiceTest {
    private static final Logger logger = LogFactory.getInstance().getLogger(ErrorTest.class);

    public void testCommandException() {
        String errorCode = null;
        CommandException e = null;
        Exception failure = null;

        String category = CommandException.ERROR_CATEGORY.getValue();

        errorCode = String.valueOf(CommandException.MIN_ERROR_CODE);
        e = new CommandException();
        checkException(errorCode, category, e);

        e = new CommandException("Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(CommandException.MIN_ERROR_CODE + 1);
        e = new CommandException(errorCode, "Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(CommandException.MAX_ERROR_CODE + 1);
        try {
            e = new CommandException(errorCode, "Failed message");
        } catch (Exception ex) {
            failure = ex;
            logger.debug("testCommandException: Expected failure", failure);
        }
        assertNotNull("Expected failure", failure);
    }

    public void testBusinessRulesException() {
        String errorCode = null;
        BusinessRulesException e = null;
        Exception failure = null;
        
        String category = BusinessRulesException.ERROR_CATEGORY.getValue();
        
        errorCode = String.valueOf(BusinessRulesException.MIN_ERROR_CODE);
        e = new BusinessRulesException();
        checkException(errorCode, category, e);

        e = new BusinessRulesException("Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(BusinessRulesException.MIN_ERROR_CODE + 1);
        e = new BusinessRulesException(errorCode, "Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(BusinessRulesException.MAX_ERROR_CODE + 1);
        try {
            e = new BusinessRulesException(errorCode, "Failed message");
        } catch (Exception ex) {
            failure = ex;
            logger.debug("testBusinessRulesException: Expected failure", failure);
        }
        assertNotNull("Expected failure", failure);
    }

    public void testDataFormatException() {
        String errorCode = null;
        DataFormatException e = null;
        Exception failure = null;
        
        String category = DataFormatException.ERROR_CATEGORY.getValue();
        
        errorCode = String.valueOf(DataFormatException.MIN_ERROR_CODE);
        e = new DataFormatException();
        checkException(errorCode, category, e);

        e = new DataFormatException("Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(DataFormatException.MIN_ERROR_CODE + 1);
        e = new DataFormatException(errorCode, "Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(DataFormatException.MAX_ERROR_CODE + 1);
        try {
            e = new DataFormatException(errorCode, "Failed message");
        } catch (Exception ex) {
            failure = ex;
            logger.debug("testDataFormatException: Expected failure", failure);
        }
        assertNotNull("Expected failure", failure);
    }

    public void testClientIntegrationException() {
        String errorCode = null;
        ClientIntegrationException e = null;
        Exception failure = null;
        
        String category = ClientIntegrationException.ERROR_CATEGORY.getValue();
        
        errorCode = String.valueOf(ClientIntegrationException.MIN_ERROR_CODE);
        e = new ClientIntegrationException();
        checkException(errorCode, category, e);

        e = new ClientIntegrationException("Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(ClientIntegrationException.MIN_ERROR_CODE + 1);
        e = new ClientIntegrationException(errorCode, "Failed message");
        checkException(errorCode, category, e);

        errorCode = String.valueOf(ClientIntegrationException.MAX_ERROR_CODE + 1);
        try {
            e = new ClientIntegrationException(errorCode, "Failed message");
        } catch (Exception ex) {
            failure = ex;
            logger.debug("testClientIntegrationException: Expected failure", failure);
        }
        assertNotNull("Expected failure", failure);
    }

    public void testServiceErrors() {
        RequestRouter requestRouter = getRequestRouter();
        Exception exception = null;

        //null request test
        UpdateRequest request = null;
        exception = processRequest(requestRouter, request);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected ClientIntegrationException", exception instanceof ClientIntegrationException);
        
        //empty request test
        request = new UpdateRequest();
        exception = processRequest(requestRouter, request);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected ClientIntegrationException", exception instanceof ClientIntegrationException);

        //missing action
        ProcessingInstruction processingInstruction = new ProcessingInstruction();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader(null, null)));
        exception = processRequest(requestRouter, request);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected ClientIntegrationException", exception instanceof ClientIntegrationException);

        //invalid action
        processingInstruction.setAction("invalid123");
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader(null, null)));
        exception = processRequest(requestRouter, request);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected ClientIntegrationException", exception instanceof ClientIntegrationException);

        //mismatch between request and action
        processingInstruction.setAction("errorTest");
        SelectRequest otherRequest = new SelectRequest();
        exception = processRequest(requestRouter, otherRequest);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected ClientIntegrationException", exception instanceof ClientIntegrationException);
        
        //incomplete request test - good for the router, incomplete for the command 
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader(null, null)));
        exception = processRequest(requestRouter, request);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected DataFormatException", exception instanceof DataFormatException);
        
        //bad request test - code already exists 
        UpdateRequest updateRequest = (UpdateRequest)request;
        updateRequest.setCode("test");
        exception = processRequest(requestRouter, updateRequest);
        assertNotNull("Expected exception", exception);
        assertTrue("Expected DataFormatException", exception instanceof BusinessRulesException);
    }

    private Exception processRequest(RequestRouter requestRouter, BaseOperationRequest request) {
        Exception exception = null;
        try {
            requestRouter.process(request);
        } catch (Exception e) {
            exception = e;
            if (logger.isDebugEnabled()) {
                logger.debug("processRequest: exception="+ exception, e);
            }
        }
        return exception;
    }
    

    private void checkException(String errorCode, String category, CommandException e) {
        assertNotNull("Expected not null error code", e.getErrorCode());
        assertEquals("Expected specific error code", errorCode, e.getErrorCode());
        assertEquals("Expected specific error category code", category, e.getErrorCategoryCode().getValue());
    }
}
