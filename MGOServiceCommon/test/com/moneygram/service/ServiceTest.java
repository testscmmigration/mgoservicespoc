/*
 * Created on Nov 15, 2007
 *
 */
package com.moneygram.service;

import org.springframework.context.ApplicationContext;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.ClientHeader;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ProcessingInstruction;
import com.moneygram.common.service.RequestHeader;
import com.moneygram.common.service.RequestRouter;
import com.moneygram.service.command.SelectRequest;
import com.moneygram.service.command.UpdateRequest;
import com.moneygram.service.dao.TestDAO;

/**
 * Spring Test.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>NotificationService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2007</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2009/06/24 19:23:54 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ServiceTest extends BaseServiceTest {
    private static final Logger logger = LogFactory.getInstance().getLogger(ServiceTest.class);

    /**
     * Test jdbc template.
     */
    public void testJdbc() {
        ApplicationContext context = getContext(); 
        TestDAO testDAO = (TestDAO)context.getBean("testDAO");
        
        if (logger.isDebugEnabled()) {
            logger.debug("testJdbc: testDAO.getJdbcTemplate="+ testDAO.getJdbcTemplate());
            logger.debug("testJdbc: testDAO.getType="+ testDAO.getType());
        }
        
        testDAO.testSelect();
    }
    
    /**
     * Test command.
     *
     */
    public void testCommand() throws Exception {
        RequestRouter commandRouter = getRequestRouter();

        //update request
        ProcessingInstruction processingInstruction = new ProcessingInstruction("update");
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        
        commandRouter.process(updateRequest);
        
        //select request
        processingInstruction = new ProcessingInstruction("select");
        SelectRequest selectRequest = new SelectRequest();
        selectRequest.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));

        commandRouter.process(selectRequest);
    }
    
    public void testCommandTransactionContext() throws Exception {
        RequestRouter commandRouter = getRequestRouter();

        //update request with default transaction context
        ProcessingInstruction processingInstruction = new ProcessingInstruction("update");
        UpdateRequest request = new UpdateRequest();
        request.setHeader(new RequestHeader(processingInstruction, new ClientHeader()));
        
        Exception exception = null;
        try {
            OperationResponse response = commandRouter.process(request);
            if (logger.isDebugEnabled()) {
                logger.debug("testCommandTransactionContext: response="+ response);
            }
        } catch (Exception e) {
            exception = e;
            logger.error("Failed to process the request", e);
        }
        assertNull("Did not expect exception", exception);

        //update request with readonly transaction context
        processingInstruction.setReadOnlyFlag(Boolean.TRUE);
        
        exception = null;
        try {
            OperationResponse response = commandRouter.process(request);
            if (logger.isDebugEnabled()) {
                logger.debug("testCommandTransactionContext: response="+ response);
            }
        } catch (Exception e) {
            exception = e;
            logger.error("Failed to process the request", e);
        }
        assertNotNull("Expected exception", exception);

    }
}
