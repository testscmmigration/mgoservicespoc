/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.common.dao;

import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * 
 * Base DAO.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2011/08/03 18:49:46 $ </td><tr><td>
 * @author   </td><td>$Author: ut96 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseDAO extends JdbcDaoSupport implements DataAccessObject {
	
	
    private String type = null;

    /**
     * 
     * @deprecated
     */
    public String getType() {
        return type;
    }
    

    /**
     * 
     * @deprecated
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Executes the jdbc call wrapping the exception inside of DAOException.
     * The error code if any is transferred as well.  
     * @param call
     * @param parameters
     * @return
     * @throws DAOException
     */
    protected Map<String, Object> executeJdbcCall(SimpleJdbcCall call, MapSqlParameterSource parameters, Map errorsMap) throws DAOException {
        Map<String, Object> result = null;
        String message = "Failed to execute the sql="+ call.getProcedureName() +" with parameters="+ parameters.getValues();
        try {
            result = call.execute(parameters);
        } catch (Exception e) {
            throw createDAOException(errorsMap, message, e);
        }
        return result;
    }

    /**
     * Processes Exception creating the DAOException.
     * @param errorsMap
     * @param message
     * @param e
     * @throws DAOException
     */
    protected DAOException createDAOException(Map errorsMap, String message, Exception e) {
        DAOException daoException = null;
        if (e instanceof UncategorizedSQLException) {
            UncategorizedSQLException ue = (UncategorizedSQLException)e;
            SQLException se = ue.getSQLException();
            if (se != null) {
                Class clazz = (Class)errorsMap.get(se.getErrorCode());
                if (clazz != null) {
                    daoException = createDAOExceptionInstance(clazz, message, e);
                }
            }
        }
        if (daoException == null) {
            daoException = new DAOException(message, e);
        }
        return daoException;
    }

    /**
     * Creates new instance of the DAOException class
     * @param message
     * @param e
     * @return
     * @throws Exception
     */
    protected DAOException createDAOExceptionInstance(Class clazz, String message, Exception e) {
        Class[] partypes = new Class[]{String.class, Throwable.class};
        Object[] args = new Object[]{message, e};
        DAOException daoException = null;
        try {
            daoException = (DAOException)createInstance(clazz, partypes, args);
        } catch (Exception e1) {
//            logger.error("Failed to create new instance of "+ clazz, e);
        }
        return daoException;
    }

    /**
     * Creates new instance of the class
     * @param clazz class
     * @param partypes constructor types
     * @param args constructor arguments
     * @return new instance of the class
     * @throws Exception
     */
    protected Object createInstance(Class clazz, Class[] partypes, Object[] args) throws Exception {
        Constructor constructor = clazz.getConstructor(partypes);
        Object object = constructor.newInstance(args);
        return object;    
    }

}
