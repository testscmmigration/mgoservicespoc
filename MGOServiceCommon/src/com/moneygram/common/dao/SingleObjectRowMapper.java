/*
 * Created on Dec 10, 2007
 *
 */
package com.moneygram.common.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Single Object Populator.
 * Populates a single value from the result set based on the column index.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2007</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/07/24 16:59:21 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class SingleObjectRowMapper extends BaseRowMapper {
    private int index = 0;
    
    /**
     * Creates new instance of SingleObjectPopulator.
     * @param index field index
     * 
     */
    public SingleObjectRowMapper(int index) {
        super();
        setIndex(index);
    }

    /**
     * @return Returns the index.
     */
    public int getIndex() {
        return index;
    }
    /**
     * @param index The index to set.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Object value = rs.getObject(index);
        return value;
    }

}
