/*
 * Created on Jul 20, 2009
 *
 */
package com.moneygram.common.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * DAO Utils. <div>
 *<table>
 * <tr>
 * <th>Title:</th>
 * <td>MGOServiceCommon</td>
 * <tr>
 * <th>Copyright:</th>
 * <td>Copyright (c) 2009</td>
 * <tr>
 * <th>Company:</th>
 * <td>MoneyGram</td>
 * <tr>
 * <td>
 * 
 * @version </td><td>$Revision: 1.6 $ $Date: 2009/10/22 22:10:17 $ </td>
 *          <tr>
 *          <td>
 * @author </td><td>$Author: a700 $ </td>
 *         </table>
 *         </div>
 */
public abstract class DAOUtils {
	private static final String BOOLEAN_TRUE = "Y";
	private static final String BOOLEAN_FALSE = "N";

	/**
	 * Converts Date into Calendar.
	 * 
	 * @param date
	 * @return Calendar
	 */
	public static Calendar toCalendar(Date date) {
		Calendar calendar = null;
		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}
		return calendar;
	}

	/**
	 * Converts the db string value into Boolean.
	 * 
	 * @param value
	 * @return
	 */
	public static Boolean toBoolean(String value) {
		return BOOLEAN_TRUE.equals(value) ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Converts the int value into Boolean.
	 * 
	 * @param value
	 * @return
	 */
	public static Boolean toBoolean(int value) {
		return value > 0 ? Boolean.TRUE : Boolean.FALSE;
	}

	/**
	 * Converts Boolean into db string value.
	 * 
	 * @param value
	 * @return
	 */
	public static String toString(Boolean value) {
		return toString(value != null && value.booleanValue());
	}

    /**
     * Converts Boolean into db string value.
     * 
     * @param value
     * @return
     */
	public static String toString(boolean value) {
        return value ? BOOLEAN_TRUE : BOOLEAN_FALSE;
    }

	/**
	 * Converts Boolean into int value.
	 * 
	 * @param value
	 * @return
	 */
	public static int toInt(Boolean value) {
		return value != null && value.booleanValue() ? 1 : 0;
	}

	public static Boolean toBoolean(String value, String trueVal,
			String falseVal) {
		if (trueVal != null)
			return new Boolean(trueVal.equals(value));
		else if (falseVal != null)
			return new Boolean(!falseVal.equals(value));
		return toBoolean(value);
	}

	public static String toString(Boolean value, String trueVal, String falseVal) {
		if (value != null && value.booleanValue())
			return trueVal;
		return falseVal;
	}

	public static String toString(Calendar calendar) {
		if (calendar == null)
			return "null";
		SimpleDateFormat dsf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dateString = dsf.format(calendar.getTime());
		return dateString;
	}
}
