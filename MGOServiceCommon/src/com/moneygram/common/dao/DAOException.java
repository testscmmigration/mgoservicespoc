/*
 * Created on Jun 24, 2009
 *
 */
package com.moneygram.common.dao;

import com.moneygram.common.service.CommandException;

/**
 * 
 * DAO Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/08/11 14:15:39 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class DAOException extends CommandException {
    private static final long serialVersionUID = -9207209200984289965L;

    /**
     * 
     * Creates new instance of DAOException
     */
    public DAOException() {
        super();
        
    }

    /**
     * 
     * Creates new instance of DAOException
     * @param message
     * @param cause
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
        
    }

    /**
     * 
     * Creates new instance of DAOException
     * @param message
     */
    public DAOException(String message) {
        super(message);
        
    }

    /**
     * 
     * Creates new instance of DAOException
     * @param code
     * @param message
     * @param cause
     */
    public DAOException(String code, String message, Throwable cause) {
        super(code, message, cause);
        
    }

    /**
     * 
     * Creates new instance of DAOException
     * @param code
     * @param message
     */
    public DAOException(String code, String message) {
        super(code, message);
        
    }

}
