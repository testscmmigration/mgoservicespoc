/*
 * Created on Jun 5, 2009
 *
 */
package com.moneygram.common.service;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * Read Command that does not require transaction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class ReadCommand extends BaseCommand {
    private static final Logger logger = LogFactory.getInstance().getLogger(ReadCommand.class);
    
    /**
     * Checks the readonly flag and logs the warning if readonly flag is not set.
     * @see com.moneygram.common.service.BaseCommand#validateTransactionContext(com.moneygram.common.service.OperationContext)
     */
    @Override
    protected void validateTransactionContext(RequestHeader context) throws CommandException {
        if (! context.isReadOnly()) {
            if (logger.isInfoEnabled()) {
                logger.info("validateTransactionContext: readonly command is invoked but readonly flag is not set indicating that transaction has been started.");
            }
        }
    }

}
