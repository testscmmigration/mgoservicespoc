/*
 * Created on Jul 1, 2009
 *
 */
package com.moneygram.common.service.util;

import java.util.Arrays;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * 
 * Application Context Factory. Holds ApplicationContext singleton instance.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/28 17:36:03 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class ApplicationContextFactory {
    private static final Logger logger = LogFactory.getInstance().getLogger(ApplicationContextFactory.class);
    
    private static ApplicationContext applicationContext = null;

    private static String[] contextResources = new String[] { "applicationContext.xml" };
    
    public static String[] getContextResources() {
        return contextResources;
    }

    public static void setContextResources(String[] specificContextResources) {
        contextResources = specificContextResources;
    }

    /**
     * Returns a singleton instance of ApplicationContext with 
     * ApplicationContext configured using context resources.
     * @param contextResources context resources  
     * @return singleton instance of ApplicationContextHolder
     */
    public static ApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            synchronized (ApplicationContext.class) {
                if (applicationContext == null) {
                    applicationContext = createContext(getContextResources());
                }                
            }
        }
        return applicationContext;
    }

    protected static ApplicationContext createContext(String[] contextResources) {
        if (logger.isDebugEnabled()) {
            logger.debug("createContext: creating ApplicationContext from contextResources="+ (contextResources == null ? null : Arrays.asList(contextResources)));
        }
        return new ClassPathXmlApplicationContext(contextResources);
    }

}
