package com.moneygram.common.service.util;


import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateFactory;

import javax.crypto.Cipher;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * Security Utility.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2012/01/27 16:51:39 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public abstract class SecurityUtility {

    /**
     * Returns encrypted value.
     * @param textToEncrypt text
     * @param keyfile public key file name including path
     * @return
     * @throws Exception
     */
    public static String encrypt(String textToEncrypt, String keyfile) throws Exception {
        try {
            // Add provider
            Security.addProvider(new BouncyCastleProvider());
        } catch (Exception e) {
            throw new Exception("Failed to add security provider", e);
        }

        PublicKey rsaPublicKey = null;
        InputStream is = null;
        try {
            // Get Key from Public Cert File
            //is = ClassLoader.getSystemClassLoader().getResourceAsStream(keyfile);
            is = new FileInputStream(keyfile);
            System.out.println(SecurityUtility.class.getName() + ".encrypt: is="+is);
            java.security.cert.CertificateFactory cf = CertificateFactory.getInstance("X.509");
            java.security.cert.Certificate cert = cf.generateCertificate(is);
            rsaPublicKey = cert.getPublicKey();
        } catch (Exception e) {
            throw new Exception("Failed to extract the public key from key file="+ keyfile, e);
        } finally {
            if (is != null) {
                is.close();
            }
        }

        Cipher cp = null;
        try {
            // Generate RSA cipher
            cp = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
        } catch (Exception e) {
            throw new Exception("Failed to get an instance of the cipher", e);
        }

        try {
            // Encrypt the plain text using the Public Cert File/Key.
            cp.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
            byte[] encryptedText = cp.doFinal(textToEncrypt.getBytes());
            // Store as Base64 encoded.
            return new String(Base64.encode(encryptedText));
        } catch (Exception e) {
            throw new Exception("Failed to encrypt the value", e);
        }
    }

    /**
     * Encrypts a value, according to the certificate keyfile and
     * without using padding
     * @param textToEncrypt text
     * @param keyfile public key file name including path
     * @return the encrypted value
     * @throws Exception
     */
    public static String encryptWithNoPadding(String textToEncrypt, String keyfile) throws Exception {
        try {
            // Add provider
            Security.addProvider(new BouncyCastleProvider());
        } catch (Exception e) {
            throw new Exception("Failed to add security provider", e);
        }

        PublicKey rsaPublicKey = null;
        InputStream is = null;
        try {
            // Get Key from Public Cert File
            //is = ClassLoader.getSystemClassLoader().getResourceAsStream(keyfile);
            is = new FileInputStream(keyfile);
            System.out.println(SecurityUtility.class.getName() + ".encrypt: is="+is);
            java.security.cert.CertificateFactory cf = CertificateFactory.getInstance("X.509");
            java.security.cert.Certificate cert = cf.generateCertificate(is);
            rsaPublicKey = cert.getPublicKey();
        } catch (Exception e) {
            throw new Exception("Failed to extract the public key from key file="+ keyfile, e);
        } finally {
            if (is != null) {
                is.close();
            }
        }

        Cipher cp = null;
        try {
            // Generate RSA cipher
            cp = Cipher.getInstance("RSA/None/NoPadding", "BC");
        } catch (Exception e) {
            throw new Exception("Failed to get an instance of the cipher", e);
        }

        try {
            // Encrypt the plain text using the Public Cert File/Key.
            cp.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
            byte[] encryptedText = cp.doFinal(textToEncrypt.getBytes());
            // Store as Base64 encoded.
            return new String(Base64.encode(encryptedText));
        } catch (Exception e) {
            throw new Exception("Failed to encrypt the value", e);
        }
    }

    /**
     * Returns SHA digest.
     * @param text
     * @param seed
     * @return
     * @throws Exception
     */
    public static String digest(String text, String seed) throws Exception {
        return digest(text + seed);
    }

    /**
     * Returns SHA digest.
     * @param text
     * @return
     * @throws Exception
     */
    public static String digest(String text) throws Exception {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            return hex(sha.digest(text.getBytes("UTF-8")));
        } catch (Exception e) {
            throw new Exception("Failed to generate SHA digest", e);
        }
    }

    /**
     * Converts byte array into string of hex values.
     * @param array
     * @return
     */
    private static String hex(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }
}