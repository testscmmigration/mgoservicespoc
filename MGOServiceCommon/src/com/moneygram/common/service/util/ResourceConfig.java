/*
 * Created on Nov 27, 2007
 *
 */
package com.moneygram.common.service.util;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.ree.lib.Config;

/**
 * 
 * Resource Config.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>TransactionLoaderEJB</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2008</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/27 19:09:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class ResourceConfig {
    private static final Logger logger = LogFactory.getInstance().getLogger(ResourceConfig.class);

    /**
	 * instance of the resource config.
	 */
	private Config config = null;
	
	private static ResourceConfigFactory resourceConfigFactory = new ResourceConfigFactory();

    /**
     * Initializes the configuration.
     */
    protected void initResourceConfig() {
	    if (getConfig() == null) {
            setConfig(getResourceConfigFactory().createResourceConfiguration(getResourceConfigurationJndiName()));
        }
    }

    /**
     * Returns jndi name of the configuration.
     * @return
     */
    protected abstract String getResourceConfigurationJndiName();

	/**
	 * Returns value of the attribute specified.
	 * @param name name
	 * @return value
	 */
	protected String getAttributeValue(String name) {
	    return (getConfig() == null ? null : (String)getConfig().getAttribute(name));
	}

	/**
	 * Returns integer value of the attribute specified.
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	protected int getIntegerAttributeValue(String attributeName, String defaultValue) {
        // if default value is not passed in, this must be required attribute, so allow the
        //   parseInt to throw an exception out this time
	    int value = getIntegerAttributeValue(attributeName, Integer.parseInt(defaultValue));
		return value;
	}

   protected int getIntegerAttributeValue(String attributeName, int defaultValue) {
       String attributeValue = getAttributeValue(attributeName);
       try {
           return Integer.parseInt(attributeValue);
       } catch (Exception e) {
           logger.warn("Attribute " + attributeName + " is not set. Using default of " + defaultValue);
       }
       return defaultValue;
   }

    /**
     * @return Returns the config.
     */
    public Config getConfig() {
        return config;
    }

    /**
     * @param config The config to set.
     */
    public void setConfig(Config config) {
        this.config = config;
    }

    public static ResourceConfigFactory getResourceConfigFactory() {
        return resourceConfigFactory;
    }

    public static void setResourceConfigFactory(ResourceConfigFactory factory) {
        resourceConfigFactory = factory;
    }

}
