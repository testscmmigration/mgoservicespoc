/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.common.service;

import com.moneygram.common.dao.DataAccessObject;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * Base Command.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.8 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class BaseCommand implements Command {
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseCommand.class);
    
    private DataAccessObject dataAccessObject = null;
    
    public DataAccessObject getDataAccessObject() {
        return dataAccessObject;
    }

    public void setDataAccessObject(DataAccessObject dataAccessObject) {
        this.dataAccessObject = dataAccessObject;
    }

    /**
     * Basic implementation of the request processing.
     * @param request
     * @return response
     * @see com.moneygram.common.service.Command#processRequest(com.moneygram.common.service.ServiceRequest)
     */
    final public OperationResponse processRequest(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("processRequest: request="+ request);
        }

        RequestHeader context = request.getHeader();
        
        //validate Transaction Context
        validateTransactionContext(context);

        //check if request is supported by the command
        if (! isRequestSupported(request)) {
            throw new ClientIntegrationException("Unsupported request type="+ request.getClass().getName() +" by command="+ this.getClass().getName());
        }

        //check simulated mode settings
        if (context.getSimulatedModeAction() != null && (! isInSimulatedMode())) {
            throw new ClientIntegrationException("Command="+ this.getClass().getName() +" is not running in the simlated mode but request contains the simulated mode action");
        }

        if (context.getSimulatedModeAction() == null && isInSimulatedMode()) {
            throw new ClientIntegrationException("Command="+ this.getClass().getName() +" is running in the simlated mode but request does not contain the simulated mode action");
        }

        //process request
        OperationResponse response = process(request); 

        return response;
    }

    /**
     * Processes the request. 
     * @param request
     * @return response
     * @throws CommandException
     */
    protected abstract OperationResponse process(OperationRequest request) throws CommandException;

    /**
     * Validates transaction context.
     * @param context
     * @throws CommandException
     */
    protected abstract void validateTransactionContext(RequestHeader context) throws CommandException;

    protected abstract boolean isRequestSupported(OperationRequest request) throws CommandException;
    
    /**
     * Returns true if command is executing in the simulated mode.
     * @return
     * @throws CommandException
     */
    public boolean isInSimulatedMode() throws CommandException {
        return false;
    }

}
