/*
 * Created on Jun 15, 2009
 *
 */
package com.moneygram.common.service;

import java.io.Serializable;

/**
 * 
 * Operation Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/06/18 22:07:12 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public interface OperationRequest extends Serializable {
    public RequestHeader getHeader();
}
