/*
 * Created on Aug 5, 2009
 *
 */
package com.moneygram.common.service;

/**
 * 
 * Service Processor.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/05 21:00:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public interface ServiceProcessor {
    /**
     * Processes transactional command.
     * @param request
     * @return
     * @throws ServiceException
     */
    public OperationResponse process(OperationRequest request) throws ServiceException;
    
    /**
     * Processes read-only command.
     * @param request
     * @return
     * @throws ServiceException
     */
    public OperationResponse processRead(OperationRequest request) throws ServiceException;
}
