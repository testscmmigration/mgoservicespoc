/*
 * Created on Jun 15, 2009
 *
 */
package com.moneygram.common.service;

/**
 * 
 * Base Operation Request.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/07/08 21:09:17 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class BaseOperationRequest implements OperationRequest {
    private RequestHeader header = null;

    /**
     * 
     * Creates new instance of BaseOperationRequest
     */
    public BaseOperationRequest() {
        super();
        
    }

    /**
     * 
     * Creates new instance of BaseOperationRequest
     * @param header
     */
    public BaseOperationRequest(RequestHeader header) {
        setHeader(header);
    }

    public RequestHeader getHeader() {
        return header;
    }

    public void setHeader(RequestHeader header) {
        this.header = header;
    }
    
    @Override
    public String toString() {
        return super.toString() +" Header="+ getHeader();
    }
}
