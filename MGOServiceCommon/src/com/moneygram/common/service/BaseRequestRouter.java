/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.common.service;

import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * Base Request Router.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.10 $ $Date: 2012/01/22 18:12:38 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class BaseRequestRouter implements RequestRouter {
	// ========================================
	// Fields
	// ========================================
    private static final Logger logger = LogFactory.getInstance().getLogger(BaseRequestRouter.class);
    private static final String NAMESPACE_DELIMITER = ".";
    private static final String DEFAULT_PARTNER_SITE_ID = "MGO";

    private Map<String, Command> commands = null;


	// ========================================
	// Business Methods
	// ========================================
    /**
     * @see com.moneygram.common.service.RequestRouter#process(com.moneygram.common.service.ServiceRequest)
     */
    public OperationResponse process(OperationRequest request) throws CommandException {
        if (logger.isDebugEnabled()) {
            logger.debug("process: request="+ request);
        }

        if (request == null) {
            throw new ClientIntegrationException("Service request must not be null");
        }

        RequestHeader header = request.getHeader();

        if (header == null) {
            throw new ClientIntegrationException("Request header is missing for request="+ request);
        }

        String requestId = null;
        if (header.getClientHeader() != null) {
            requestId = header.getClientHeader().getClientRequestID();
        }

        if (logger.isInfoEnabled()) {
            logger.info("process: request id="+ requestId);
        }

        ProcessingInstruction processingInstruction = header.getProcessingInstruction();
        if (processingInstruction == null) {
            throw new ClientIntegrationException("Processing instruction is missing for request="+ request);
        }

        //retrieve action
        String action = processingInstruction.getAction();

        if (action == null) {
            throw new ClientIntegrationException("Action is missing from the request header="+ header);
        }

        //Retrieve partner site id
        String partnerSiteId = processingInstruction.getPartnerSiteId();

        //instantiate command
        Command command = lookupCommand(action, partnerSiteId);

        if (logger.isDebugEnabled()) {
            logger.debug("process: command="+ command);
        }

        if (command == null) {
        	StringBuilder sb = new StringBuilder();
        	sb.append("Unsupported action \"");
        	sb.append(action);
        	sb.append("\" for partnerSiteId \"");
        	sb.append(partnerSiteId);
        	sb.append("\".");

            throw new ClientIntegrationException(sb.toString());
        }

        //execute command
        OperationResponse operationResponse = command.processRequest(request);

        if (logger.isDebugEnabled()) {
            logger.debug("process: operationResponse="+ operationResponse);
        }

        return operationResponse;
    }


	// ========================================
	// Delegate Methods
	// ========================================
    /**
     * Returns the command mapped to the action.
     * @param action
     * @param partnerSiteId
     * @return command mapped to the action.
     */
    private Command lookupCommand(String action, String partnerSiteId){
    	String commandQName = buildCommandQName(action, partnerSiteId);
    	Command command = (Command) getCommands().get(commandQName);
		return getCommands() == null ? null : ((command != null) ? command : (Command) getCommands().get(action));
    }

    private String buildCommandQName(String action, String partnerSiteId){
    	String commandNamespace = buildCommandNamespace(partnerSiteId);
    	String namespaceDelimiter = commandNamespace.equals("") ? "" :  NAMESPACE_DELIMITER;

    	StringBuilder sb = new StringBuilder();
    	sb.append(commandNamespace);
    	sb.append(namespaceDelimiter);
    	sb.append(action);

    	return sb.toString();

    }
    private String buildCommandNamespace(String partnerSiteId){
    	if(partnerSiteId == null){
    		return "";
    	}

    	partnerSiteId = partnerSiteId.trim();

    	if(partnerSiteId.equals("")){
    		return "";
    	}

    	partnerSiteId = partnerSiteId.toLowerCase();

    	if(partnerSiteId.equals(DEFAULT_PARTNER_SITE_ID.toLowerCase())){
    		return "";
    	}

    	return partnerSiteId;
    }


	// ========================================
	// Getters/Setters
	// ========================================
    /**
     * @see com.moneygram.common.service.RequestRouter#getCommands()
     */
    public Map<String, Command> getCommands() {
        return commands;
    }

    /**
     * @see com.moneygram.common.service.RequestRouter#setCommands(java.util.HashMap)
     */
    public void setCommands(Map<String, Command> commands) {
        this.commands = commands;
    }

}
