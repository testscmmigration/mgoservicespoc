/**
 * ErrorHandlingCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.common.service;

/**
 * 
 * Error Handling Code.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/11 13:59:16 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ErrorHandlingCode implements java.io.Serializable {
    private static final long serialVersionUID = -8258466511050138815L;
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ErrorHandlingCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _RetryNow = "RetryNow";
    public static final java.lang.String _RetryLater = "RetryLater";
    public static final java.lang.String _ReturnError = "ReturnError";
    public static final ErrorHandlingCode RetryNow = new ErrorHandlingCode(_RetryNow);
    public static final ErrorHandlingCode RetryLater = new ErrorHandlingCode(_RetryLater);
    public static final ErrorHandlingCode ReturnError = new ErrorHandlingCode(_ReturnError);
    public java.lang.String getValue() { return _value_;}
    public static ErrorHandlingCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ErrorHandlingCode enumeration = (ErrorHandlingCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ErrorHandlingCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
