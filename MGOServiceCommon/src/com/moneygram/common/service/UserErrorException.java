/*
 * Created on Aug 18, 2009
 *
 */
package com.moneygram.common.service;

/**
 * 
 * User Error Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/18 22:09:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class UserErrorException extends CommandException {
    public static final ErrorCategoryCode ERROR_CATEGORY = ErrorCategoryCode.UserError;

    public UserErrorException(String code, String message, Throwable cause) {
        super(code, message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public UserErrorException(String code, String message) {
        super(code, message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

}
