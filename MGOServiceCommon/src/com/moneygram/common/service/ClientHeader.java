/**
 * HeaderTypeClientHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.common.service;


/**
 * 
 * Client Header.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/27 19:09:31 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ClientHeader  implements java.io.Serializable {
    private static final long serialVersionUID = -5293848272629584499L;

    private java.lang.String clientRequestID;

    private java.lang.String clientSessionID;

    public ClientHeader() {
    }

    public ClientHeader(
           java.lang.String clientRequestID,
           java.lang.String clientSessionID) {
           this.clientRequestID = clientRequestID;
           this.clientSessionID = clientSessionID;
    }


    /**
     * Gets the clientRequestID value for this ClientHeader.
     * 
     * @return clientRequestID
     */
    public java.lang.String getClientRequestID() {
        return clientRequestID;
    }


    /**
     * Sets the clientRequestID value for this ClientHeader.
     * 
     * @param clientRequestID
     */
    public void setClientRequestID(java.lang.String clientRequestID) {
        this.clientRequestID = clientRequestID;
    }


    /**
     * Gets the clientSessionID value for this ClientHeader.
     * 
     * @return clientSessionID
     */
    public java.lang.String getClientSessionID() {
        return clientSessionID;
    }


    /**
     * Sets the clientSessionID value for this ClientHeader.
     * 
     * @param clientSessionID
     */
    public void setClientSessionID(java.lang.String clientSessionID) {
        this.clientSessionID = clientSessionID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientHeader)) return false;
        ClientHeader other = (ClientHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientRequestID==null && other.getClientRequestID()==null) || 
             (this.clientRequestID!=null &&
              this.clientRequestID.equals(other.getClientRequestID()))) &&
            ((this.clientSessionID==null && other.getClientSessionID()==null) || 
             (this.clientSessionID!=null &&
              this.clientSessionID.equals(other.getClientSessionID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientRequestID() != null) {
            _hashCode += getClientRequestID().hashCode();
        }
        if (getClientSessionID() != null) {
            _hashCode += getClientSessionID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" ClientRequestID=").append(getClientRequestID());
        buffer.append(" ClientSessionID=").append(getClientSessionID());
        return buffer.toString();
    }
}
