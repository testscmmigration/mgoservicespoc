/*
 * Created on Jun 15, 2009
 *
 */
package com.moneygram.common.service;

import java.io.Serializable;

/**
 * 
 * Operation Response.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/06/16 15:51:10 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public interface OperationResponse extends Serializable {

}
