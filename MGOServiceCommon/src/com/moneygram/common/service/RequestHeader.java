/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.common.service;

import java.io.Serializable;

/**
 * Request Header.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class RequestHeader implements Serializable {
    private static final long serialVersionUID = -6310372202162332186L;

    private ProcessingInstruction processingInstruction = null;
    private ClientHeader clientHeader = null;
    
    /**
     * Creates new instance of RequestHeader
     */
    public RequestHeader() {
        super();
    }

    /**
     * 
     * Creates new instance of RequestHeader
     * @param processingInstruction
     * @param clientHeader
     */
    public RequestHeader(ProcessingInstruction processingInstruction, ClientHeader clientHeader) {
        super();
        this.processingInstruction = processingInstruction;
        this.clientHeader = clientHeader;
    }

    public ProcessingInstruction getProcessingInstruction() {
        return processingInstruction;
    }

    public void setProcessingInstruction(ProcessingInstruction processingInstruction) {
        this.processingInstruction = processingInstruction;
    }

    public ClientHeader getClientHeader() {
        return clientHeader;
    }

    public void setClientHeader(ClientHeader clientHeader) {
        this.clientHeader = clientHeader;
    }

    /**
     * 
     * @see com.moneygram.common.service.OperationContext#isReadOnly()
     */
    public boolean isReadOnly() {
        return getProcessingInstruction() != null && getProcessingInstruction().getReadOnlyFlag() != null && getProcessingInstruction().getReadOnlyFlag().booleanValue();
    }

    /**
     * 
     * @see com.moneygram.common.service.OperationContext#getSimulatedModeAction()
     */
    public String getSimulatedModeAction() {
        return getProcessingInstruction() != null ? getProcessingInstruction().getSimulatedModeAction() : null;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return super.toString() +" ProcessingInstruction="+ getProcessingInstruction()+" ClientHeader="+ getClientHeader();
    }
}
