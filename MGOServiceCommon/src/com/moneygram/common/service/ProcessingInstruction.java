/**
 * ProcessingInstruction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.common.service;
/**
 *
 * Processing Instruction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2012/01/22 18:12:38 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public class ProcessingInstruction  implements java.io.Serializable {
	private static final long serialVersionUID = 4856104917853999222L;

    private java.lang.String action;

    private java.lang.String partnerSiteId;

    private java.lang.Object[] invocationMethod;

    /* Transaction will not be initiated if true. */
    private java.lang.Boolean readOnlyFlag;

    /* Not currently used. */
    private java.lang.String[] faultHandling;

    private java.lang.String language;

    /* Return Request with Response if true. */
    private java.lang.Boolean echoRequestFlag;

    /* Simulation mode action. If value is specified then the request
     * is executed in the simulation mode. The simulated mode action value
     * is used to determine the type of the response. If service is running
     * in the production mode then the simulation mode request will cause
     * an error. */
    private java.lang.String simulatedModeAction;

    public ProcessingInstruction() {
    }

     public ProcessingInstruction(
            java.lang.String action) {
            this.action = action;
     }

    public ProcessingInstruction(
	           java.lang.String action,
	           java.lang.String partnerSiteId,
	           java.lang.Object[] invocationMethod,
	           java.lang.Boolean readOnlyFlag,
	           java.lang.String[] faultHandling,
	           java.lang.String language,
	           java.lang.Boolean echoRequestFlag,
	           java.lang.String simulatedModeAction) {
           this.action = action;
           this.partnerSiteId = partnerSiteId;
           this.invocationMethod = invocationMethod;
           this.readOnlyFlag = readOnlyFlag;
           this.faultHandling = faultHandling;
           this.language = language;
           this.echoRequestFlag = echoRequestFlag;
           this.simulatedModeAction = simulatedModeAction;
    }

    public ProcessingInstruction(
	           java.lang.String action,
	           java.lang.String partnerSiteId) {
           this.action = action;
           this.partnerSiteId = partnerSiteId;
    }


    /**
     * Gets the action value for this ProcessingInstruction.
     *
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this ProcessingInstruction.
     *
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the partnerSiteId value for this ProcessingInstruction.
     *
     * @return partnerSiteId
     */
    public java.lang.String getPartnerSiteId() {
        return partnerSiteId;
    }


    /**
     * Sets the partnerSiteId value for this ProcessingInstruction.
     *
     * @param partnerSiteId
     */
    public void setPartnerSiteId(java.lang.String partnerSiteId) {
        this.partnerSiteId = partnerSiteId;
    }


    /**
     * Gets the invocationMethod value for this ProcessingInstruction.
     *
     * @return invocationMethod
     */
    public java.lang.Object[] getInvocationMethod() {
        return invocationMethod;
    }


    /**
     * Sets the invocationMethod value for this ProcessingInstruction.
     *
     * @param invocationMethod
     */
    public void setInvocationMethod(java.lang.Object[] invocationMethod) {
        this.invocationMethod = invocationMethod;
    }

    public java.lang.Object getInvocationMethod(int i) {
        return this.invocationMethod[i];
    }

    public void setInvocationMethod(int i, java.lang.Object _value) {
        this.invocationMethod[i] = _value;
    }


    /**
     * Gets the readOnlyFlag value for this ProcessingInstruction.
     *
     * @return readOnlyFlag   * Transaction will not be initiated if true.
     */
    public java.lang.Boolean getReadOnlyFlag() {
        return readOnlyFlag;
    }


    /**
     * Sets the readOnlyFlag value for this ProcessingInstruction.
     *
     * @param readOnlyFlag   * Transaction will not be initiated if true.
     */
    public void setReadOnlyFlag(java.lang.Boolean readOnlyFlag) {
        this.readOnlyFlag = readOnlyFlag;
    }


    /**
     * Gets the faultHandling value for this ProcessingInstruction.
     *
     * @return faultHandling   * Not currently used.
     */
    public java.lang.String[] getFaultHandling() {
        return faultHandling;
    }


    /**
     * Sets the faultHandling value for this ProcessingInstruction.
     *
     * @param faultHandling   * Not currently used.
     */
    public void setFaultHandling(java.lang.String[] faultHandling) {
        this.faultHandling = faultHandling;
    }

    public java.lang.String getFaultHandling(int i) {
        return this.faultHandling[i];
    }

    public void setFaultHandling(int i, java.lang.String _value) {
        this.faultHandling[i] = _value;
    }


    /**
     * Gets the language value for this ProcessingInstruction.
     *
     * @return language
     */
    public java.lang.String getLanguage() {
        return language;
    }


    /**
     * Sets the language value for this ProcessingInstruction.
     *
     * @param language
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }


    /**
     * Gets the echoRequestFlag value for this ProcessingInstruction.
     *
     * @return echoRequestFlag   * Return Request with Response if true.
     */
    public java.lang.Boolean getEchoRequestFlag() {
        return echoRequestFlag;
    }


    /**
     * Sets the echoRequestFlag value for this ProcessingInstruction.
     *
     * @param echoRequestFlag   * Return Request with Response if true.
     */
    public void setEchoRequestFlag(java.lang.Boolean echoRequestFlag) {
        this.echoRequestFlag = echoRequestFlag;
    }


    /**
     * Gets the simulatedModeAction value for this ProcessingInstruction.
     *
     * @return simulatedModeAction   * Simulation mode action. If value is specified then the request
     * is executed in the simulation mode. The simulated mode action value
     * is used to determine the type of the response. If service is running
     * in the production mode then the simulation mode request will cause
     * an error.
     */
    public java.lang.String getSimulatedModeAction() {
        return simulatedModeAction;
    }


    /**
     * Sets the simulatedModeAction value for this ProcessingInstruction.
     *
     * @param simulatedModeAction   * Simulation mode action. If value is specified then the request
     * is executed in the simulation mode. The simulated mode action value
     * is used to determine the type of the response. If service is running
     * in the production mode then the simulation mode request will cause
     * an error.
     */
    public void setSimulatedModeAction(java.lang.String simulatedModeAction) {
        this.simulatedModeAction = simulatedModeAction;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessingInstruction)) return false;
        ProcessingInstruction other = (ProcessingInstruction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.action==null && other.getAction()==null) ||
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.partnerSiteId==null && other.getPartnerSiteId()==null) ||
             (this.partnerSiteId!=null &&
              this.partnerSiteId.equals(other.getPartnerSiteId()))) &&
            ((this.invocationMethod==null && other.getInvocationMethod()==null) ||
             (this.invocationMethod!=null &&
              java.util.Arrays.equals(this.invocationMethod, other.getInvocationMethod()))) &&
            ((this.readOnlyFlag==null && other.getReadOnlyFlag()==null) ||
             (this.readOnlyFlag!=null &&
              this.readOnlyFlag.equals(other.getReadOnlyFlag()))) &&
            ((this.faultHandling==null && other.getFaultHandling()==null) ||
             (this.faultHandling!=null &&
              java.util.Arrays.equals(this.faultHandling, other.getFaultHandling()))) &&
            ((this.language==null && other.getLanguage()==null) ||
             (this.language!=null &&
              this.language.equals(other.getLanguage()))) &&
            ((this.echoRequestFlag==null && other.getEchoRequestFlag()==null) ||
             (this.echoRequestFlag!=null &&
              this.echoRequestFlag.equals(other.getEchoRequestFlag()))) &&
            ((this.simulatedModeAction==null && other.getSimulatedModeAction()==null) ||
             (this.simulatedModeAction!=null &&
              this.simulatedModeAction.equals(other.getSimulatedModeAction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getPartnerSiteId() != null) {
            _hashCode += getPartnerSiteId().hashCode();
        }
        if (getInvocationMethod() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInvocationMethod());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInvocationMethod(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getReadOnlyFlag() != null) {
            _hashCode += getReadOnlyFlag().hashCode();
        }
        if (getFaultHandling() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFaultHandling());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFaultHandling(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLanguage() != null) {
            _hashCode += getLanguage().hashCode();
        }
        if (getEchoRequestFlag() != null) {
            _hashCode += getEchoRequestFlag().hashCode();
        }
        if (getSimulatedModeAction() != null) {
            _hashCode += getSimulatedModeAction().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

	/**
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(" super=").append(super.toString());
        buffer.append(" Action=").append(getAction());
        buffer.append(" InvocationMethod=").append(getInvocationMethod());
        buffer.append(" ReadOnlyFlag=").append(getReadOnlyFlag());
        buffer.append(" FaultHandling=").append(getFaultHandling());
        buffer.append(" Language=").append(getLanguage());
        buffer.append(" EchoRequestFlag=").append(getEchoRequestFlag());
        buffer.append(" SimulatedModeAction=").append(getSimulatedModeAction());
        return buffer.toString();
    }
}
