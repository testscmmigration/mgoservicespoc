/**
 * ServiceException.java
 */

package com.moneygram.common.service;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * Service Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.4 $ $Date: 2009/08/18 22:09:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class ServiceException  extends Exception {
    private static final long serialVersionUID = -449016492649602954L;

    private java.lang.String errorCode;

    /* Service/Application Name */
    private java.lang.String errorSource;

    /* Step in processing that caused the error */
    private java.lang.String errorLocation;

    private ErrorHandlingCode errorHandlingCode;

    /* Indicates client or system error. */
    private ErrorCategoryCode errorCategoryCode;

    /* List of related errors. Example: required field validation. */
    private RelatedError[] relatedErrors;

    /**
     * 
     * Creates new instance of ServiceException
     */
    public ServiceException() {
    }

    /**
     * Creates new instance of ServiceException
     * @param message
     * @deprecated use constructor with error code instead.
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Creates new instance of ServiceException
     * @param message
     * @param cause
     * @deprecated use constructor with error code instead.
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 
     * Creates new instance of ServiceException
     * @param code
     * @param message
     */
    public ServiceException(String code, String message) {
        super(message);
        setErrorCode(code);
    }

    /**
     * 
     * Creates new instance of ServiceException
     * @param code
     * @param message
     * @param cause
     */
    public ServiceException(String code, String message, Throwable cause) {
        super(message, cause);
        setErrorCode(code);
    }

    /**
     * Gets the errorCode value for this ServiceException.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this ServiceException.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        validateErrorCodeRange(errorCode);
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorSource value for this ServiceException.
     * 
     * @return errorSource   * Service/Application Name
     */
    public java.lang.String getErrorSource() {
        return errorSource;
    }


    /**
     * Sets the errorSource value for this ServiceException.
     * 
     * @param errorSource   * Service/Application Name
     */
    public void setErrorSource(java.lang.String errorSource) {
        this.errorSource = errorSource;
    }


    /**
     * Gets the errorLocation value for this ServiceException.
     * 
     * @return errorLocation   * Step in processing that caused the error
     */
    public java.lang.String getErrorLocation() {
        return errorLocation;
    }


    /**
     * Sets the errorLocation value for this ServiceException.
     * 
     * @param errorLocation   * Step in processing that caused the error
     */
    public void setErrorLocation(java.lang.String errorLocation) {
        this.errorLocation = errorLocation;
    }


    /**
     * Gets the errorHandlingCode value for this ServiceException.
     * 
     * @return errorHandlingCode
     */
    public ErrorHandlingCode getErrorHandlingCode() {
        return errorHandlingCode;
    }


    /**
     * Sets the errorHandlingCode value for this ServiceException.
     * 
     * @param errorHandlingCode
     */
    public void setErrorHandlingCode(ErrorHandlingCode errorHandlingCode) {
        this.errorHandlingCode = errorHandlingCode;
    }


    /**
     * Gets the errorCategoryCode value for this ServiceException.
     * 
     * @return errorCategoryCode   * Indicates client or system error.
     */
    public ErrorCategoryCode getErrorCategoryCode() {
        return errorCategoryCode;
    }


    /**
     * Sets the errorCategoryCode value for this ServiceException.
     * 
     * @param errorCategoryCode   * Indicates client or system error.
     */
    public void setErrorCategoryCode(ErrorCategoryCode errorCategoryCode) {
        this.errorCategoryCode = errorCategoryCode;
    }


    /**
     * Gets the relatedErrors value for this ServiceException.
     * 
     * @return relatedErrors   * List of related errors. Example: required field validation.
     */
    public RelatedError[] getRelatedErrors() {
        return relatedErrors;
    }


    /**
     * Sets the relatedErrors value for this ServiceException.
     * 
     * @param relatedErrors   * List of related errors. Example: required field validation.
     */
    public void setRelatedErrors(RelatedError[] relatedErrors) {
        this.relatedErrors = relatedErrors;
    }

    /**
     * Returns the string represenation of the stack trace.
     * @return the string represenation of the stack trace.
     */
    public String getErrorStackTrace() {
        StringWriter writer = new StringWriter();
        printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }

    /**
     * Returns error code range as an array with 1st element indicating the minimum value and 2nd - the maximum.
     * 
     * @return error code range
     */
    protected abstract int[] getErrorCodeRange();

    /**
     * Validates error code specified.
     * @param code
     */
    protected void validateErrorCodeRange(String code) {
        boolean passed = true;
        Exception exception = null;
        try {
            int cd = new Integer(code).intValue();
            int[] range = getErrorCodeRange();
            if (range != null && range.length > 1) {
                passed = (cd >= range[0] && cd <= range[1]);
            }
        } catch (Exception e) {
            exception = e;
        }
        
        if (! passed) {
            throw new IllegalArgumentException("Error code="+ code +" does not belong to the valid error code range", exception);
        }
    }
}
