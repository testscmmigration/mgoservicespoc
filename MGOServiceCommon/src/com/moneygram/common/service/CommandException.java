/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.common.service;

/**
 * Command Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/08/18 22:09:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class CommandException extends ServiceException {
    public static final int MIN_ERROR_CODE = 100;
    public static final int MAX_ERROR_CODE = 199;
    public static final ErrorCategoryCode ERROR_CATEGORY = ErrorCategoryCode.ServiceSystemError;
    
    /**
     * Creates new instance of CommandException
     */
    public CommandException() {
        super(String.valueOf(MIN_ERROR_CODE), "");
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    /**
     * Creates new instance of CommandException
     * @param message
     */
    public CommandException(String message) {
        super(String.valueOf(MIN_ERROR_CODE), message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    /**
     * Creates new instance of CommandException
     * @param message
     * @param cause
     */
    public CommandException(String message, Throwable cause) {
        super(String.valueOf(MIN_ERROR_CODE), message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    /**
     * 
     * Creates new instance of CommandException
     * @param code
     * @param message
     * @param cause
     */
    public CommandException(String code, String message, Throwable cause) {
        super(code, message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    /**
     * 
     * Creates new instance of CommandException
     * @param code
     * @param message
     */
    public CommandException(String code, String message) {
        super(code, message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    @Override
    protected int[] getErrorCodeRange() {
      return new int[]{MIN_ERROR_CODE, MAX_ERROR_CODE};
    }

}
