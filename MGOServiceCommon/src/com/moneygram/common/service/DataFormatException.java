/*
 * Created on Aug 18, 2009
 *
 */
package com.moneygram.common.service;

/**
 * 
 * Data Format Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/18 22:09:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class DataFormatException extends UserErrorException {
    public static final int MIN_ERROR_CODE = 600;
    public static final int MAX_ERROR_CODE = 999;

    public DataFormatException() {
        super(String.valueOf(MIN_ERROR_CODE), "");
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public DataFormatException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public DataFormatException(String code, String message) {
        super(code, message);
    }

    public DataFormatException(String message, Throwable cause) {
        super(String.valueOf(MIN_ERROR_CODE), message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public DataFormatException(String message) {
        super(String.valueOf(MIN_ERROR_CODE), message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    @Override
    protected int[] getErrorCodeRange() {
      return new int[]{MIN_ERROR_CODE, MAX_ERROR_CODE};
    }

}
