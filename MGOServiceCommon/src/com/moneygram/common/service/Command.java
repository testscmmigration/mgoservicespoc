/*
 * Created on Jun 4, 2009
 *
 */
package com.moneygram.common.service;

/**
 * Command Interface.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.3 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public interface Command {
    public OperationResponse processRequest(OperationRequest request) throws CommandException;
}
