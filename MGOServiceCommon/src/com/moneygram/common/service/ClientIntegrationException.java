/*
 * Created on Aug 19, 2009
 *
 */
package com.moneygram.common.service;

/**
 * 
 * Client Integration Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2009/08/19 13:31:35 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ClientIntegrationException extends CommandException {
    public static final int MIN_ERROR_CODE = 200;
    public static final int MAX_ERROR_CODE = 299;
    public static final ErrorCategoryCode ERROR_CATEGORY = ErrorCategoryCode.ClientIntegrationError;

    public ClientIntegrationException() {
        super(String.valueOf(MIN_ERROR_CODE), "");
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public ClientIntegrationException(String code, String message, Throwable cause) {
        super(code, message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public ClientIntegrationException(String code, String message) {
        super(code, message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public ClientIntegrationException(String message, Throwable cause) {
        super(String.valueOf(MIN_ERROR_CODE), message, cause);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    public ClientIntegrationException(String message) {
        super(String.valueOf(MIN_ERROR_CODE), message);
        setErrorCategoryCode(ERROR_CATEGORY);
    }

    @Override
    protected int[] getErrorCodeRange() {
      return new int[]{MIN_ERROR_CODE, MAX_ERROR_CODE};
    }

}
