/*
 * Created on Jun 5, 2009
 *
 */
package com.moneygram.common.service;

/**
 * Transactional Command that requires transaction.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>Test</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.5 $ $Date: 2009/10/09 15:41:33 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public abstract class TransactionalCommand extends BaseCommand {

    /**
     * Validates readonly flag. If readonly flag is set then transaction has not been started.
     * Every command extending this class requires transaction and if transaction has not been started this call will result in failure.
     * @see com.moneygram.common.service.BaseCommand#validateTransactionContext(com.moneygram.common.service.OperationContext)
     */
    @Override
    protected void validateTransactionContext(RequestHeader context) throws CommandException {
        if (context.isReadOnly()) {
            throw new ClientIntegrationException("Command requires transaction but readonly flag is set indicating that transaction has not been started.");
        }
    }

}
