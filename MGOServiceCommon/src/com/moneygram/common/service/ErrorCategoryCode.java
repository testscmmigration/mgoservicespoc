/**
 * ErrorCategoryCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.common.service;

/**
 * 
 * Error Category Code.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2009/08/18 22:09:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class ErrorCategoryCode implements java.io.Serializable {
    private static final long serialVersionUID = 1013537209866898517L;

    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ErrorCategoryCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _ClientIntegrationError = "ClientIntegrationError";
    public static final java.lang.String _UserError = "UserError";
    public static final java.lang.String _ServiceSystemError = "ServiceSystemError";
    public static final ErrorCategoryCode ClientIntegrationError = new ErrorCategoryCode(_ClientIntegrationError);
    public static final ErrorCategoryCode UserError = new ErrorCategoryCode(_UserError);
    public static final ErrorCategoryCode ServiceSystemError = new ErrorCategoryCode(_ServiceSystemError);
    public java.lang.String getValue() { return _value_;}
    public static ErrorCategoryCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ErrorCategoryCode enumeration = (ErrorCategoryCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ErrorCategoryCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
}
