/*
 * Created on Jun 12, 2009
 *
 */
package com.moneygram.common.service;

import java.util.Map;

/**
 * Request Router Interface.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOServiceCommon</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.7 $ $Date: 2012/01/22 18:12:38 $ </td><tr><td>
 * @author   </td><td>$Author: uw25 $ </td>
 *</table>
 *</div>
 */
public interface RequestRouter {
    public static final String REQUEST_ROUTER = "requestRouter";

    /**
     * Processes the request using a command mapped to the action specified in the request header.
     * @param request
     * @return command response
     * @throws CommandException
     */
    public abstract OperationResponse process(OperationRequest request) throws CommandException;

    /**
     * Retuns map of the commands configured.
     * @return map of the commands configured.
     */
    public abstract Map<String, Command> getCommands();

    /**
     * Sets map of the commands configured.
     * @param commands
     */
    public abstract void setCommands(Map<String, Command> commands);

}