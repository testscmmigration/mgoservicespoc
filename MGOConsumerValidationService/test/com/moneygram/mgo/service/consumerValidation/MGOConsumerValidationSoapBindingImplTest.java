package com.moneygram.mgo.service.consumerValidation;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.tempuri.StrucAddressLookupDef_1D;
import org.tempuri.StrucAddressLookupResponseDef_1D;
import org.tempuri.StrucAddressLookupResponseResultsDef_1D;
import org.tempuri.StrucID3CheckDef_1D;
import org.tempuri.StrucID3CheckResponseDef_1D;
import org.tempuri.StrucID3CheckResponseResultsDef_1D;

import com.moneygram.mgo.service.consumerVerification.id3check.proxy.ID3Check_1Proxy;
import com.moneygram.mgo.service.consumerVerification.util.MGOConsumerVerificationServiceResourceConfig;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MGOConsumerValidationSoapBindingImpl.class)
public class MGOConsumerValidationSoapBindingImplTest {

	@InjectMocks
	private MGOConsumerValidationSoapBindingImpl mgoConsumerValidationSoapBindingImpl;
	
	@Mock
	private ID3Check_1Proxy proxy;
	
	Address address = new Address();
	Consumer consumer = new Consumer();
	Phone[] phones = new Phone[1];
	StrucID3CheckResponseResultsDef_1D[] strucID3CheckResponseResults = new StrucID3CheckResponseResultsDef_1D[1];
	LookUpAddressRequest requestLUA = new LookUpAddressRequest();
	ValidateRegistrationInformationRequest requestVRI = new ValidateRegistrationInformationRequest();
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		try {
			MGOConsumerVerificationServiceResourceConfig.getInstance();
		} catch (Exception e) {
			// Exception is expected.
		}
		
		address.setAddressLine1("addressLine1");
		address.setAddressLine2("addressLine2");
		address.setBuildingNumber("buildingNumber");
		address.setCity("city");
		address.setCompany("company");
		address.setDepartament("departament");
		address.setPoBox("poBox");
		address.setPostalCode("postalCode");
		address.setState("state");
		address.setSubBuildingNumber("subBuildingNumber");
		address.setSubCity("subCity");
		address.setTwoCharISOCountry("twoCharISOCountry");
		
		consumer.setConsumerId("consumerId");
		consumer.setCountryOfBirth("countryOfBirth");
		consumer.setDateOfBirth(new Date());
		consumer.setFirstName("firstName");
		consumer.setGender("gender");
		consumer.setLastName("lastName");
		consumer.setMiddleName("middleName");
		consumer.setPlaceOfBirth("placeOfBirth");
		consumer.setTitle("title");
		
		phones[0] = new Phone();
		phones[0].setNumber("number");
		phones[0].setPublished(String.valueOf(true));
		phones[0].setType(PhoneType.home);
		
		strucID3CheckResponseResults[0] = new StrucID3CheckResponseResultsDef_1D();
		strucID3CheckResponseResults[0].setSzResultCode("szResultCode");
		strucID3CheckResponseResults[0].setSzResultDescription("szResultDescription");
		strucID3CheckResponseResults[0].setSzUID("szUID");
		strucID3CheckResponseResults[0].setSzUIDDescription("szUIDDescription");
		
		requestLUA.setAddress(address);
		
		requestVRI.setAddress(address);
		requestVRI.setConsumer(consumer);
		requestVRI.setPhones(phones);
		requestVRI.setMgiProfileIdForIdentityValidationServiceProvider("mgiProfileIdForIdentityValidationServiceProvider");
	}
	
	// Verifying use of lookUpAddress.
	@Test
	public void testLookUpAddress() throws Exception {
		StrucAddressLookupResponseDef_1D response = new StrucAddressLookupResponseDef_1D();
		response.setSzMessage("szMessage");
		response.setSzStatus("szStatus");
		response.setIResultCount(3);
		StrucAddressLookupResponseResultsDef_1D[] results = new StrucAddressLookupResponseResultsDef_1D[1];
		results[0] = new StrucAddressLookupResponseResultsDef_1D();
		results[0].setSzAddressLine1("szAddressLine1");
		results[0].setSzAddressLine2("szAddressLine1");
		results[0].setSzAddressLine3("szAddressLine1");
		results[0].setSzAddressLine4("szAddressLine1");
		results[0].setSzBuilding("szBuilding");
		results[0].setSzCedexMailSort("szCedexMailSort");
		results[0].setSzCity("szCity");
		results[0].setSzFormattedAddress("RATHAUS STR. 2#2523252c 24960 GLUECKSBURG (OSTSEE)#2523252c GERMANY");
		results[0].setSzPOBox("szPOBox");
		results[0].setSzPremise("szPremise");
		results[0].setSzPrincipality("szPrincipality");
		results[0].setSzRegion("szRegion");
		results[0].setSzStateDistrict("szStateDistrict");
		results[0].setSzStreet("szStreet");
		results[0].setSzSubBuilding("szSubBuilding");
		results[0].setSzZipPostcode("szZipPostcode");
		response.setStrucAddressLookupResponseResults(results);
		
		Mockito.when(proxy.addressLookUp((StrucAddressLookupDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString())).thenReturn(response);
		mgoConsumerValidationSoapBindingImpl.lookUpAddress(requestLUA);

		try {
			Mockito.when(proxy.addressLookUp((StrucAddressLookupDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString())).thenThrow(new RemoteException());
			mgoConsumerValidationSoapBindingImpl.lookUpAddress(requestLUA);
			
			Assert.fail("Exception expected.");
		} catch (RemoteException re) {
			// Exception is expected.
		}
		
		Mockito.verify(proxy, Mockito.times(2)).addressLookUp((StrucAddressLookupDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString());
	}
	
	// Verifying use of validateRegistrationInformation.
	@Test
	public void testValidateRegistrationInformation() throws Exception {
		Mockito.when(proxy.validateRegistrationInformation((StrucID3CheckDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString())).thenReturn(new StrucID3CheckResponseDef_1D());
		mgoConsumerValidationSoapBindingImpl.validateRegistrationInformation(requestVRI);

		try {
			Mockito.when(proxy.validateRegistrationInformation((StrucID3CheckDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString())).thenThrow(new RemoteException());
			mgoConsumerValidationSoapBindingImpl.validateRegistrationInformation(requestVRI);
			
			Assert.fail("Exception expected.");
		} catch (RemoteException re) {
			// Exception is expected.
		}
		
		Mockito.verify(proxy, Mockito.times(2)).validateRegistrationInformation((StrucID3CheckDef_1D) Matchers.any(), Matchers.anyString(), Matchers.anyString());
	}
	
	// Verifying fill in methods.
	
	@Test
	public void testFillInValidateRegistrationInformationResponse() {
		StrucID3CheckResponseDef_1D response = new StrucID3CheckResponseDef_1D();
		response.setNScore(2);
		response.setSzAuthenticationID("szAuthenticationID");
		response.setSzBandText("szBandText");
		response.setSzCustomerRef("szCustomerRef");
		response.setSzRemarks("szRemarks");
		response.setSzStatus("szStatus");
		response.setSzTimestamp("szTimestamp");
		response.setStrucID3CheckResponseResults(strucID3CheckResponseResults);
		
		ValidateRegistrationInformationResponse result = mgoConsumerValidationSoapBindingImpl.fillInValidateRegistrationInformationResponse(response);
		
		Assert.assertSame(response.getSzAuthenticationID(), result.getAutenticationId());
		Assert.assertSame(response.getSzBandText(), result.getBandText());
		Assert.assertSame(response.getSzCustomerRef(), result.getConsumerId());
		Assert.assertSame(response.getSzRemarks(), result.getRemarks());
		Assert.assertSame(response.getSzStatus(), result.getStatus());
		Assert.assertSame(response.getNScore(), result.getScore());
	}
	
	@Test
	public void testFillInStrucID3CheckDef_1D() {
		ValidateRegistrationInformationRequest request = new ValidateRegistrationInformationRequest();
		request.setMgiProfileIdForIdentityValidationServiceProvider("mgiProfileIdForIdentityValidationServiceProvider");
		request.setAddress(address);
		request.setConsumer(consumer);
		request.setPhones(phones);
		
		StrucID3CheckDef_1D result = mgoConsumerValidationSoapBindingImpl.fillInStrucID3CheckDef_1D(request);
		
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzAddressLayout());
		Assert.assertSame(address.getBuildingNumber(), result.getStrucAddresses().getStrucAddress1().getSzBuildingNumber());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzCedex());
		Assert.assertSame(address.getCity(), result.getStrucAddresses().getStrucAddress1().getSzCityTown());
		Assert.assertSame(address.getCompany(), result.getStrucAddresses().getStrucAddress1().getSzCompany());
		Assert.assertSame(address.getTwoCharISOCountry(), result.getStrucAddresses().getStrucAddress1().getSzCountry());
		Assert.assertSame(address.getDepartament(), result.getStrucAddresses().getStrucAddress1().getSzDepartment());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzEndDate());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzFFAddress1());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzFFAddress2());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzFFAddress3());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzFFAddress4());
		Assert.assertSame(address.getPoBox(), result.getStrucAddresses().getStrucAddress1().getSzPOBox());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzPremise());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzPrincipality());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzRegion());
		Assert.assertSame(null, result.getStrucAddresses().getStrucAddress1().getSzStartDate());
		Assert.assertSame(address.getState(), result.getStrucAddresses().getStrucAddress1().getSzStateDistrict());
		Assert.assertSame(address.getAddressLine1(), result.getStrucAddresses().getStrucAddress1().getSzStreet());
		Assert.assertSame(address.getSubBuildingNumber(), result.getStrucAddresses().getStrucAddress1().getSzSubBuildingNumber());
		Assert.assertSame(address.getSubCity(), result.getStrucAddresses().getStrucAddress1().getSzSubCity());
		Assert.assertSame(address.getAddressLine2(), result.getStrucAddresses().getStrucAddress1().getSzSubStreet());
		Assert.assertSame(address.getPostalCode(), result.getStrucAddresses().getStrucAddress1().getSzZipPCode());
		
		Assert.assertSame(phones[0].getNumber(), result.getStrucTelephones().getStrucHomeTelephone().getSzNumber());
		Assert.assertSame(Boolean.getBoolean(phones[0].getPublished()), result.getStrucTelephones().getStrucHomeTelephone().isBPublished());
		
		Assert.assertSame(consumer.getConsumerId(), result.getStrucRequestDetails().getSzCustomerRef());
		Assert.assertSame("mgiProfileIdForIdentityValidationServiceProvider", result.getStrucRequestDetails().getSzProfile());
		Assert.assertSame(null, result.getStrucRequestDetails().getSzSuccessCriteria());
		Assert.assertSame(null, result.getStrucRequestDetails().getSzVersion());
		
		Assert.assertSame(consumer.getCountryOfBirth(), result.getStrucPerson().getSzCountryOfBirth());
		Assert.assertSame(consumer.getFirstName(), result.getStrucPerson().getSzFirstName());
		Assert.assertSame(consumer.getGender(), result.getStrucPerson().getSzGender());
		Assert.assertSame(consumer.getLastName(), result.getStrucPerson().getSzSurname());
		Assert.assertSame(consumer.getMiddleName(), result.getStrucPerson().getSzMiddleName());
		Assert.assertSame(consumer.getPlaceOfBirth(), result.getStrucPerson().getSzPlaceOfBirth());
		Assert.assertSame(consumer.getTitle(), result.getStrucPerson().getSzTitle());
	}

	@Test
	public void testFillInStrucAddressLookupDef_1D() {
		LookUpAddressRequest request = new LookUpAddressRequest();
		request.setAddress(address);
		
		StrucAddressLookupDef_1D result = mgoConsumerValidationSoapBindingImpl.fillInStrucAddressLookupDef_1D(request);
		
		Assert.assertSame(null, result.getStrucAddress().getSzAddressLayout());
		Assert.assertSame(address.getBuildingNumber(), result.getStrucAddress().getSzBuildingNumber());
		Assert.assertSame(null, result.getStrucAddress().getSzCedex());
		Assert.assertSame(address.getCity(), result.getStrucAddress().getSzCityTown());
		Assert.assertSame(address.getCompany(), result.getStrucAddress().getSzCompany());
		Assert.assertSame(address.getTwoCharISOCountry(), result.getStrucAddress().getSzCountry());
		Assert.assertSame(address.getDepartament(), result.getStrucAddress().getSzDepartment());
		Assert.assertSame(null, result.getStrucAddress().getSzEndDate());
		Assert.assertSame(null, result.getStrucAddress().getSzFFAddress1());
		Assert.assertSame(null, result.getStrucAddress().getSzFFAddress2());
		Assert.assertSame(null, result.getStrucAddress().getSzFFAddress3());
		Assert.assertSame(null, result.getStrucAddress().getSzFFAddress4());
		Assert.assertSame(address.getPoBox(), result.getStrucAddress().getSzPOBox());
		Assert.assertSame(null, result.getStrucAddress().getSzPremise());
		Assert.assertSame(null, result.getStrucAddress().getSzPrincipality());
		Assert.assertSame(null, result.getStrucAddress().getSzRegion());
		Assert.assertSame(null, result.getStrucAddress().getSzStartDate());
		Assert.assertSame(address.getState(), result.getStrucAddress().getSzStateDistrict());
		Assert.assertSame(address.getAddressLine1(), result.getStrucAddress().getSzStreet());
		Assert.assertSame(address.getSubBuildingNumber(), result.getStrucAddress().getSzSubBuildingNumber());
		Assert.assertSame(address.getSubCity(), result.getStrucAddress().getSzSubCity());
		Assert.assertSame(address.getAddressLine2(), result.getStrucAddress().getSzSubStreet());
		Assert.assertSame(address.getPostalCode(), result.getStrucAddress().getSzZipPCode());
	}
	
	@Test
	public void testFillInLookUpAddressResponse() {
		StrucAddressLookupResponseDef_1D response = new StrucAddressLookupResponseDef_1D();
		response.setSzMessage("szMessage");
		response.setSzStatus("szStatus");
		response.setIResultCount(3);
		StrucAddressLookupResponseResultsDef_1D[] results = new StrucAddressLookupResponseResultsDef_1D[1];
		results[0] = new StrucAddressLookupResponseResultsDef_1D();
		results[0].setSzAddressLine1("szAddressLine1");
		results[0].setSzAddressLine2("szAddressLine1");
		results[0].setSzAddressLine3("szAddressLine1");
		results[0].setSzAddressLine4("szAddressLine1");
		results[0].setSzBuilding("szBuilding");
		results[0].setSzCedexMailSort("szCedexMailSort");
		results[0].setSzCity("szCity");
		results[0].setSzFormattedAddress("IM BECKERFELDE 4-16#2523252c 45475 MUELHEIM AN DER RUHR#2523252c GERMANY");
		results[0].setSzPOBox("szPOBox");
		results[0].setSzPremise("szPremise");
		results[0].setSzPrincipality("szPrincipality");
		results[0].setSzRegion("szRegion");
		results[0].setSzStateDistrict("szStateDistrict");
		results[0].setSzStreet("szStreet");
		results[0].setSzSubBuilding("szSubBuilding");
		results[0].setSzZipPostcode("szZipPostcode");
		
		response.setStrucAddressLookupResponseResults(results);
		
		LookUpAddressResponse result = mgoConsumerValidationSoapBindingImpl.fillInLookUpAddressResponse(response);
		
		Assert.assertSame(response.getSzMessage(), result.getMessage());
		Assert.assertSame(response.getSzStatus(), result.getStatus());
		Assert.assertSame("GLUECKSBURG (OSTSEE) ", result.getAddress()[0].getCity());
		Assert.assertSame("RATHAUS STR. ", result.getAddress()[0].getAddressLine1());
		Assert.assertSame("24960", result.getAddress()[0].getPostalCode());
		Assert.assertSame("2", result.getAddress()[0].getBuildingNumber());
	}
	
}
