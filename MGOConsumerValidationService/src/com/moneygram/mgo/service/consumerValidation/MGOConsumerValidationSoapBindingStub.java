/**
 * MGOConsumerValidationSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class MGOConsumerValidationSoapBindingStub extends org.apache.axis.client.Stub implements com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[2];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("lookUpAddress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressRequest"), com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"),
                      "com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validateRegistrationInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest"), com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"),
                      "com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"), 
                      true
                     ));
        _operations[1] = oper;

    }

    public MGOConsumerValidationSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MGOConsumerValidationSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MGOConsumerValidationSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "address");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.Address.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumer");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.Consumer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "phone");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.Phone.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "phoneType");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.PhoneType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "result");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.Result.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "results");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.Results.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "twoCharISOCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse lookUpAddress(com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest lookUpAddressRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "lookUpAddress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lookUpAddressRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException) {
              throw (com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse validateRegistrationInformation(com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validateRegistrationInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validateRegistrationInformationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException) {
              throw (com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
