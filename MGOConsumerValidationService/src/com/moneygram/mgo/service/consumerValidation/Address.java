/**
 * Address.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class Address  implements java.io.Serializable {
    private java.lang.String addressLine1;

    private java.lang.String addressLine2;

    private java.lang.String company;

    private java.lang.String departament;

    private java.lang.String buildingNumber;

    private java.lang.String subBuildingNumber;

    private java.lang.String city;

    private java.lang.String subCity;

    private java.lang.String state;

    private java.lang.String postalCode;

    private java.lang.String poBox;

    private java.lang.String twoCharISOCountry;

    public Address() {
    }

    public Address(
           java.lang.String addressLine1,
           java.lang.String addressLine2,
           java.lang.String company,
           java.lang.String departament,
           java.lang.String buildingNumber,
           java.lang.String subBuildingNumber,
           java.lang.String city,
           java.lang.String subCity,
           java.lang.String state,
           java.lang.String postalCode,
           java.lang.String poBox,
           java.lang.String twoCharISOCountry) {
           this.addressLine1 = addressLine1;
           this.addressLine2 = addressLine2;
           this.company = company;
           this.departament = departament;
           this.buildingNumber = buildingNumber;
           this.subBuildingNumber = subBuildingNumber;
           this.city = city;
           this.subCity = subCity;
           this.state = state;
           this.postalCode = postalCode;
           this.poBox = poBox;
           this.twoCharISOCountry = twoCharISOCountry;
    }


    /**
     * Gets the addressLine1 value for this Address.
     * 
     * @return addressLine1
     */
    public java.lang.String getAddressLine1() {
        return addressLine1;
    }


    /**
     * Sets the addressLine1 value for this Address.
     * 
     * @param addressLine1
     */
    public void setAddressLine1(java.lang.String addressLine1) {
        this.addressLine1 = addressLine1;
    }


    /**
     * Gets the addressLine2 value for this Address.
     * 
     * @return addressLine2
     */
    public java.lang.String getAddressLine2() {
        return addressLine2;
    }


    /**
     * Sets the addressLine2 value for this Address.
     * 
     * @param addressLine2
     */
    public void setAddressLine2(java.lang.String addressLine2) {
        this.addressLine2 = addressLine2;
    }


    /**
     * Gets the company value for this Address.
     * 
     * @return company
     */
    public java.lang.String getCompany() {
        return company;
    }


    /**
     * Sets the company value for this Address.
     * 
     * @param company
     */
    public void setCompany(java.lang.String company) {
        this.company = company;
    }


    /**
     * Gets the departament value for this Address.
     * 
     * @return departament
     */
    public java.lang.String getDepartament() {
        return departament;
    }


    /**
     * Sets the departament value for this Address.
     * 
     * @param departament
     */
    public void setDepartament(java.lang.String departament) {
        this.departament = departament;
    }


    /**
     * Gets the buildingNumber value for this Address.
     * 
     * @return buildingNumber
     */
    public java.lang.String getBuildingNumber() {
        return buildingNumber;
    }


    /**
     * Sets the buildingNumber value for this Address.
     * 
     * @param buildingNumber
     */
    public void setBuildingNumber(java.lang.String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }


    /**
     * Gets the subBuildingNumber value for this Address.
     * 
     * @return subBuildingNumber
     */
    public java.lang.String getSubBuildingNumber() {
        return subBuildingNumber;
    }


    /**
     * Sets the subBuildingNumber value for this Address.
     * 
     * @param subBuildingNumber
     */
    public void setSubBuildingNumber(java.lang.String subBuildingNumber) {
        this.subBuildingNumber = subBuildingNumber;
    }


    /**
     * Gets the city value for this Address.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this Address.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the subCity value for this Address.
     * 
     * @return subCity
     */
    public java.lang.String getSubCity() {
        return subCity;
    }


    /**
     * Sets the subCity value for this Address.
     * 
     * @param subCity
     */
    public void setSubCity(java.lang.String subCity) {
        this.subCity = subCity;
    }


    /**
     * Gets the state value for this Address.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this Address.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the postalCode value for this Address.
     * 
     * @return postalCode
     */
    public java.lang.String getPostalCode() {
        return postalCode;
    }


    /**
     * Sets the postalCode value for this Address.
     * 
     * @param postalCode
     */
    public void setPostalCode(java.lang.String postalCode) {
        this.postalCode = postalCode;
    }


    /**
     * Gets the poBox value for this Address.
     * 
     * @return poBox
     */
    public java.lang.String getPoBox() {
        return poBox;
    }


    /**
     * Sets the poBox value for this Address.
     * 
     * @param poBox
     */
    public void setPoBox(java.lang.String poBox) {
        this.poBox = poBox;
    }


    /**
     * Gets the twoCharISOCountry value for this Address.
     * 
     * @return twoCharISOCountry
     */
    public java.lang.String getTwoCharISOCountry() {
        return twoCharISOCountry;
    }


    /**
     * Sets the twoCharISOCountry value for this Address.
     * 
     * @param twoCharISOCountry
     */
    public void setTwoCharISOCountry(java.lang.String twoCharISOCountry) {
        this.twoCharISOCountry = twoCharISOCountry;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Address)) return false;
        Address other = (Address) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressLine1==null && other.getAddressLine1()==null) || 
             (this.addressLine1!=null &&
              this.addressLine1.equals(other.getAddressLine1()))) &&
            ((this.addressLine2==null && other.getAddressLine2()==null) || 
             (this.addressLine2!=null &&
              this.addressLine2.equals(other.getAddressLine2()))) &&
            ((this.company==null && other.getCompany()==null) || 
             (this.company!=null &&
              this.company.equals(other.getCompany()))) &&
            ((this.departament==null && other.getDepartament()==null) || 
             (this.departament!=null &&
              this.departament.equals(other.getDepartament()))) &&
            ((this.buildingNumber==null && other.getBuildingNumber()==null) || 
             (this.buildingNumber!=null &&
              this.buildingNumber.equals(other.getBuildingNumber()))) &&
            ((this.subBuildingNumber==null && other.getSubBuildingNumber()==null) || 
             (this.subBuildingNumber!=null &&
              this.subBuildingNumber.equals(other.getSubBuildingNumber()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.subCity==null && other.getSubCity()==null) || 
             (this.subCity!=null &&
              this.subCity.equals(other.getSubCity()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.postalCode==null && other.getPostalCode()==null) || 
             (this.postalCode!=null &&
              this.postalCode.equals(other.getPostalCode()))) &&
            ((this.poBox==null && other.getPoBox()==null) || 
             (this.poBox!=null &&
              this.poBox.equals(other.getPoBox()))) &&
            ((this.twoCharISOCountry==null && other.getTwoCharISOCountry()==null) || 
             (this.twoCharISOCountry!=null &&
              this.twoCharISOCountry.equals(other.getTwoCharISOCountry())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressLine1() != null) {
            _hashCode += getAddressLine1().hashCode();
        }
        if (getAddressLine2() != null) {
            _hashCode += getAddressLine2().hashCode();
        }
        if (getCompany() != null) {
            _hashCode += getCompany().hashCode();
        }
        if (getDepartament() != null) {
            _hashCode += getDepartament().hashCode();
        }
        if (getBuildingNumber() != null) {
            _hashCode += getBuildingNumber().hashCode();
        }
        if (getSubBuildingNumber() != null) {
            _hashCode += getSubBuildingNumber().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getSubCity() != null) {
            _hashCode += getSubCity().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getPostalCode() != null) {
            _hashCode += getPostalCode().hashCode();
        }
        if (getPoBox() != null) {
            _hashCode += getPoBox().hashCode();
        }
        if (getTwoCharISOCountry() != null) {
            _hashCode += getTwoCharISOCountry().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Address.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "address"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressLine1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "addressLine1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressLine2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "addressLine2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("company");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "company"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("departament");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "departament"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("buildingNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "buildingNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subBuildingNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "subBuildingNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "city"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "subCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "postalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("poBox");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "poBox"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("twoCharISOCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "twoCharISOCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
