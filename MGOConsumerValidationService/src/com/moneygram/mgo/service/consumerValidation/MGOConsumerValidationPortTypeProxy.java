package com.moneygram.mgo.service.consumerValidation;

public class MGOConsumerValidationPortTypeProxy implements com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType {
  private String _endpoint = null;
  private com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType mGOConsumerValidationPortType = null;
  
  public MGOConsumerValidationPortTypeProxy() {
    _initMGOConsumerValidationPortTypeProxy();
  }
  
  public MGOConsumerValidationPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initMGOConsumerValidationPortTypeProxy();
  }
  
  private void _initMGOConsumerValidationPortTypeProxy() {
    try {
      mGOConsumerValidationPortType = (new com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationLocator()).getMGOConsumerValidation();
      if (mGOConsumerValidationPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)mGOConsumerValidationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)mGOConsumerValidationPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (mGOConsumerValidationPortType != null)
      ((javax.xml.rpc.Stub)mGOConsumerValidationPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType getMGOConsumerValidationPortType() {
    if (mGOConsumerValidationPortType == null)
      _initMGOConsumerValidationPortTypeProxy();
    return mGOConsumerValidationPortType;
  }
  
  public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse lookUpAddress(com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest lookUpAddressRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException{
    if (mGOConsumerValidationPortType == null)
      _initMGOConsumerValidationPortTypeProxy();
    return mGOConsumerValidationPortType.lookUpAddress(lookUpAddressRequest);
  }
  
  public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse validateRegistrationInformation(com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException{
    if (mGOConsumerValidationPortType == null)
      _initMGOConsumerValidationPortTypeProxy();
    return mGOConsumerValidationPortType.validateRegistrationInformation(validateRegistrationInformationRequest);
  }
  
  
}