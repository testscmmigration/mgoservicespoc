package com.moneygram.mgo.service.consumerValidation.dto;

import java.util.Date;

/**
 * Represents the info to be saved when you do a request to validateregisterinfo
 * @author vx15
 */
public class ValidateRegistrationSaveInfo {

	private Long custId;
	private Long authnId;
	private Date authnDate;
	private String scoreNbr;
	private String deciBandText;
	private String remarksText;
	private String statText;
		
	private String rsltCode;
	private String rsltDesc;
	private String rsltSvrtyText;
	private String chkTypeDesc;
	private String chkTypeCode;
	
	
	public ValidateRegistrationSaveInfo() {

	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public Long getAuthnId() {
		return authnId;
	}

	public void setAuthnId(Long authnId) {
		this.authnId = authnId;
	}

	public Date getAuthnDate() {
		return authnDate;
	}

	public void setAuthnDate(Date authnDate) {
		this.authnDate = authnDate;
	}

	public String getScoreNbr() {
		return scoreNbr;
	}

	public void setScoreNbr(String scoreNbr) {
		this.scoreNbr = scoreNbr;
	}

	public String getDeciBandText() {
		return deciBandText;
	}

	public void setDeciBandText(String deciBandText) {
		this.deciBandText = deciBandText;
	}

	public String getRemarksText() {
		return remarksText;
	}

	public void setRemarksText(String remarksText) {
		this.remarksText = remarksText;
	}

	public String getStatText() {
		return statText;
	}

	public void setStatText(String statText) {
		this.statText = statText;
	}
	
	public String getRsltCode() {
		return rsltCode;
	}

	public void setRsltCode(String rsltCode) {
		this.rsltCode = rsltCode;
	}

	public String getRsltDesc() {
		return rsltDesc;
	}

	public void setRsltDesc(String rsltDesc) {
		this.rsltDesc = rsltDesc;
	}

	public String getRsltSvrtyText() {
		return rsltSvrtyText;
	}

	public void setRsltSvrtyText(String rsltSvrtyText) {
		this.rsltSvrtyText = rsltSvrtyText;
	}

	public String getChkTypeDesc() {
		return chkTypeDesc;
	}

	public void setChkTypeDesc(String chkTypeDesc) {
		this.chkTypeDesc = chkTypeDesc;
	}

	public String getChkTypeCode() {
		return chkTypeCode;
	}

	public void setChkTypeCode(String chkTypeCode) {
		this.chkTypeCode = chkTypeCode;
	}

	
	
}
