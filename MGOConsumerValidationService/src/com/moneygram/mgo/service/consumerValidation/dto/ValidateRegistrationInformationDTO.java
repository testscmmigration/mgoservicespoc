package com.moneygram.mgo.service.consumerValidation.dto;

/**
 * Represents the response  of the call of a SP
 * @see ConsumerValidationDAO.GET_VALIDATE_REGISTRATION_INFORMATION
 * @author vx15
 */
public class ValidateRegistrationInformationDTO {

	private Long custId ;
	private Long authnId;
	private java.util.Date authnDate;
	private String scoreNbr;
	private String deciBandText;
	private String remarksText;
	private String statText;
	private String chkTypeCode;
	private String chkTypeDesc;
	private String rsltCode;
	private String rsltDesc;
	private String rsltSvrtyText;
	
	
	public ValidateRegistrationInformationDTO() {
		
	}
	
	
	public Long getCustId() {
		return custId;
	}
	public void setCustId(Long custId) {
		this.custId = custId;
	}
	public Long getAuthnId() {
		return authnId;
	}
	public void setAuthnId(Long authnId) {
		this.authnId = authnId;
	}
	public java.util.Date getAuthnDate() {
		return authnDate;
	}
	public void setAuthnDate(java.util.Date authnDate) {
		this.authnDate = authnDate;
	}
	public String getScoreNbr() {
		return scoreNbr;
	}
	public void setScoreNbr(String scoreNbr) {
		this.scoreNbr = scoreNbr;
	}
	public String getDeciBandText() {
		return deciBandText;
	}
	public void setDeciBandText(String deciBandText) {
		this.deciBandText = deciBandText;
	}
	public String getRemarksText() {
		return remarksText;
	}
	public void setRemarksText(String remarksText) {
		this.remarksText = remarksText;
	}
	public String getStatText() {
		return statText;
	}
	public void setStatText(String statText) {
		this.statText = statText;
	}
	public String getChkTypeCode() {
		return chkTypeCode;
	}
	public void setChkTypeCode(String chkTypeCode) {
		this.chkTypeCode = chkTypeCode;
	}
	public String getChkTypeDesc() {
		return chkTypeDesc;
	}
	public void setChkTypeDesc(String chkTypeDesc) {
		this.chkTypeDesc = chkTypeDesc;
	}
	public String getRsltCode() {
		return rsltCode;
	}
	public void setRsltCode(String rsltCode) {
		this.rsltCode = rsltCode;
	}
	public String getRsltDesc() {
		return rsltDesc;
	}
	public void setRsltDesc(String rsltDesc) {
		this.rsltDesc = rsltDesc;
	}
	public String getRsltSvrtyText() {
		return rsltSvrtyText;
	}
	public void setRsltSvrtyText(String rsltSvrtyText) {
		this.rsltSvrtyText = rsltSvrtyText;
	}
	
	
	@Override
	public String toString() {
		return "ValidateRegistrationInformationDTO [custId=" + custId
				+ ", authnId=" + authnId + ", authnDate=" + authnDate
				+ ", scoreNbr=" + scoreNbr + ", deciBandText=" + deciBandText
				+ ", remarksText=" + remarksText + ", statText=" + statText
				+ ", chkTypeCode=" + chkTypeCode + ", chkTypeDesc="
				+ chkTypeDesc + ", rsltCode=" + rsltCode + ", rsltDesc="
				+ rsltDesc + ", rsltSvrtyText=" + rsltSvrtyText + "]";
	}
	
	
	
}
