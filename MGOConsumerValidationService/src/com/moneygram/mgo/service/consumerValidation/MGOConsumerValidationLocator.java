/**
 * MGOConsumerValidationLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class MGOConsumerValidationLocator extends org.apache.axis.client.Service implements com.moneygram.mgo.service.consumerValidation.MGOConsumerValidation {

    public MGOConsumerValidationLocator() {
    }


    public MGOConsumerValidationLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MGOConsumerValidationLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MGOConsumerValidation
    private java.lang.String MGOConsumerValidation_address = "http://localhost:8092/MGOService/services/MGOConsumerValidation";

    public java.lang.String getMGOConsumerValidationAddress() {
        return MGOConsumerValidation_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MGOConsumerValidationWSDDServiceName = "MGOConsumerValidation";

    public java.lang.String getMGOConsumerValidationWSDDServiceName() {
        return MGOConsumerValidationWSDDServiceName;
    }

    public void setMGOConsumerValidationWSDDServiceName(java.lang.String name) {
        MGOConsumerValidationWSDDServiceName = name;
    }

    public com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType getMGOConsumerValidation() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MGOConsumerValidation_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMGOConsumerValidation(endpoint);
    }

    public com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType getMGOConsumerValidation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationSoapBindingStub _stub = new com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationSoapBindingStub(portAddress, this);
            _stub.setPortName(getMGOConsumerValidationWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMGOConsumerValidationEndpointAddress(java.lang.String address) {
        MGOConsumerValidation_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationSoapBindingStub _stub = new com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationSoapBindingStub(new java.net.URL(MGOConsumerValidation_address), this);
                _stub.setPortName(getMGOConsumerValidationWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MGOConsumerValidation".equals(inputPortName)) {
            return getMGOConsumerValidation();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "MGOConsumerValidation");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "MGOConsumerValidation"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MGOConsumerValidation".equals(portName)) {
            setMGOConsumerValidationEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
