/**
 * ValidateRegistrationInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class ValidateRegistrationInformationResponse  implements java.io.Serializable {
    private java.lang.String consumerId;

    private java.lang.String status;

    private java.lang.String autenticationId;

    private java.util.Calendar timestamp;

    private java.lang.String bandText;

    private java.lang.String remarks;

    private java.lang.String score;

    private com.moneygram.mgo.service.consumerValidation.Results results;

    public ValidateRegistrationInformationResponse() {
    }

    public ValidateRegistrationInformationResponse(
           java.lang.String consumerId,
           java.lang.String status,
           java.lang.String autenticationId,
           java.util.Calendar timestamp,
           java.lang.String bandText,
           java.lang.String remarks,
           java.lang.String score,
           com.moneygram.mgo.service.consumerValidation.Results results) {
           this.consumerId = consumerId;
           this.status = status;
           this.autenticationId = autenticationId;
           this.timestamp = timestamp;
           this.bandText = bandText;
           this.remarks = remarks;
           this.score = score;
           this.results = results;
    }


    /**
     * Gets the consumerId value for this ValidateRegistrationInformationResponse.
     * 
     * @return consumerId
     */
    public java.lang.String getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this ValidateRegistrationInformationResponse.
     * 
     * @param consumerId
     */
    public void setConsumerId(java.lang.String consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the status value for this ValidateRegistrationInformationResponse.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ValidateRegistrationInformationResponse.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the autenticationId value for this ValidateRegistrationInformationResponse.
     * 
     * @return autenticationId
     */
    public java.lang.String getAutenticationId() {
        return autenticationId;
    }


    /**
     * Sets the autenticationId value for this ValidateRegistrationInformationResponse.
     * 
     * @param autenticationId
     */
    public void setAutenticationId(java.lang.String autenticationId) {
        this.autenticationId = autenticationId;
    }


    /**
     * Gets the timestamp value for this ValidateRegistrationInformationResponse.
     * 
     * @return timestamp
     */
    public java.util.Calendar getTimestamp() {
        return timestamp;
    }


    /**
     * Sets the timestamp value for this ValidateRegistrationInformationResponse.
     * 
     * @param timestamp
     */
    public void setTimestamp(java.util.Calendar timestamp) {
        this.timestamp = timestamp;
    }


    /**
     * Gets the bandText value for this ValidateRegistrationInformationResponse.
     * 
     * @return bandText
     */
    public java.lang.String getBandText() {
        return bandText;
    }


    /**
     * Sets the bandText value for this ValidateRegistrationInformationResponse.
     * 
     * @param bandText
     */
    public void setBandText(java.lang.String bandText) {
        this.bandText = bandText;
    }


    /**
     * Gets the remarks value for this ValidateRegistrationInformationResponse.
     * 
     * @return remarks
     */
    public java.lang.String getRemarks() {
        return remarks;
    }


    /**
     * Sets the remarks value for this ValidateRegistrationInformationResponse.
     * 
     * @param remarks
     */
    public void setRemarks(java.lang.String remarks) {
        this.remarks = remarks;
    }


    /**
     * Gets the score value for this ValidateRegistrationInformationResponse.
     * 
     * @return score
     */
    public java.lang.String getScore() {
        return score;
    }


    /**
     * Sets the score value for this ValidateRegistrationInformationResponse.
     * 
     * @param score
     */
    public void setScore(java.lang.String score) {
        this.score = score;
    }


    /**
     * Gets the results value for this ValidateRegistrationInformationResponse.
     * 
     * @return results
     */
    public com.moneygram.mgo.service.consumerValidation.Results getResults() {
        return results;
    }


    /**
     * Sets the results value for this ValidateRegistrationInformationResponse.
     * 
     * @param results
     */
    public void setResults(com.moneygram.mgo.service.consumerValidation.Results results) {
        this.results = results;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidateRegistrationInformationResponse)) return false;
        ValidateRegistrationInformationResponse other = (ValidateRegistrationInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.autenticationId==null && other.getAutenticationId()==null) || 
             (this.autenticationId!=null &&
              this.autenticationId.equals(other.getAutenticationId()))) &&
            ((this.timestamp==null && other.getTimestamp()==null) || 
             (this.timestamp!=null &&
              this.timestamp.equals(other.getTimestamp()))) &&
            ((this.bandText==null && other.getBandText()==null) || 
             (this.bandText!=null &&
              this.bandText.equals(other.getBandText()))) &&
            ((this.remarks==null && other.getRemarks()==null) || 
             (this.remarks!=null &&
              this.remarks.equals(other.getRemarks()))) &&
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.results==null && other.getResults()==null) || 
             (this.results!=null &&
              this.results.equals(other.getResults())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getAutenticationId() != null) {
            _hashCode += getAutenticationId().hashCode();
        }
        if (getTimestamp() != null) {
            _hashCode += getTimestamp().hashCode();
        }
        if (getBandText() != null) {
            _hashCode += getBandText().hashCode();
        }
        if (getRemarks() != null) {
            _hashCode += getRemarks().hashCode();
        }
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getResults() != null) {
            _hashCode += getResults().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidateRegistrationInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("autenticationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "autenticationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "timestamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bandText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "bandText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remarks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "remarks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("results");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "results"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "results"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
