/**
 * ValidateRegistrationInformationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class ValidateRegistrationInformationRequest  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumerValidation.Consumer consumer;

    private com.moneygram.mgo.service.consumerValidation.Address address;

    private com.moneygram.mgo.service.consumerValidation.Phone[] phones;

    private java.lang.String mgiProfileIdForIdentityValidationServiceProvider;

    public ValidateRegistrationInformationRequest() {
    }

    public ValidateRegistrationInformationRequest(
           com.moneygram.mgo.service.consumerValidation.Consumer consumer,
           com.moneygram.mgo.service.consumerValidation.Address address,
           com.moneygram.mgo.service.consumerValidation.Phone[] phones,
           java.lang.String mgiProfileIdForIdentityValidationServiceProvider) {
           this.consumer = consumer;
           this.address = address;
           this.phones = phones;
           this.mgiProfileIdForIdentityValidationServiceProvider = mgiProfileIdForIdentityValidationServiceProvider;
    }


    /**
     * Gets the consumer value for this ValidateRegistrationInformationRequest.
     * 
     * @return consumer
     */
    public com.moneygram.mgo.service.consumerValidation.Consumer getConsumer() {
        return consumer;
    }


    /**
     * Sets the consumer value for this ValidateRegistrationInformationRequest.
     * 
     * @param consumer
     */
    public void setConsumer(com.moneygram.mgo.service.consumerValidation.Consumer consumer) {
        this.consumer = consumer;
    }


    /**
     * Gets the address value for this ValidateRegistrationInformationRequest.
     * 
     * @return address
     */
    public com.moneygram.mgo.service.consumerValidation.Address getAddress() {
        return address;
    }


    /**
     * Sets the address value for this ValidateRegistrationInformationRequest.
     * 
     * @param address
     */
    public void setAddress(com.moneygram.mgo.service.consumerValidation.Address address) {
        this.address = address;
    }


    /**
     * Gets the phones value for this ValidateRegistrationInformationRequest.
     * 
     * @return phones
     */
    public com.moneygram.mgo.service.consumerValidation.Phone[] getPhones() {
        return phones;
    }


    /**
     * Sets the phones value for this ValidateRegistrationInformationRequest.
     * 
     * @param phones
     */
    public void setPhones(com.moneygram.mgo.service.consumerValidation.Phone[] phones) {
        this.phones = phones;
    }

    public com.moneygram.mgo.service.consumerValidation.Phone getPhones(int i) {
        return this.phones[i];
    }

    public void setPhones(int i, com.moneygram.mgo.service.consumerValidation.Phone _value) {
        this.phones[i] = _value;
    }


    /**
     * Gets the mgiProfileIdForIdentityValidationServiceProvider value for this ValidateRegistrationInformationRequest.
     * 
     * @return mgiProfileIdForIdentityValidationServiceProvider
     */
    public java.lang.String getMgiProfileIdForIdentityValidationServiceProvider() {
        return mgiProfileIdForIdentityValidationServiceProvider;
    }


    /**
     * Sets the mgiProfileIdForIdentityValidationServiceProvider value for this ValidateRegistrationInformationRequest.
     * 
     * @param mgiProfileIdForIdentityValidationServiceProvider
     */
    public void setMgiProfileIdForIdentityValidationServiceProvider(java.lang.String mgiProfileIdForIdentityValidationServiceProvider) {
        this.mgiProfileIdForIdentityValidationServiceProvider = mgiProfileIdForIdentityValidationServiceProvider;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidateRegistrationInformationRequest)) return false;
        ValidateRegistrationInformationRequest other = (ValidateRegistrationInformationRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consumer==null && other.getConsumer()==null) || 
             (this.consumer!=null &&
              this.consumer.equals(other.getConsumer()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.phones==null && other.getPhones()==null) || 
             (this.phones!=null &&
              java.util.Arrays.equals(this.phones, other.getPhones()))) &&
            ((this.mgiProfileIdForIdentityValidationServiceProvider==null && other.getMgiProfileIdForIdentityValidationServiceProvider()==null) || 
             (this.mgiProfileIdForIdentityValidationServiceProvider!=null &&
              this.mgiProfileIdForIdentityValidationServiceProvider.equals(other.getMgiProfileIdForIdentityValidationServiceProvider())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsumer() != null) {
            _hashCode += getConsumer().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getPhones() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPhones());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPhones(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMgiProfileIdForIdentityValidationServiceProvider() != null) {
            _hashCode += getMgiProfileIdForIdentityValidationServiceProvider().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidateRegistrationInformationRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phones");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "phones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "phone"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mgiProfileIdForIdentityValidationServiceProvider");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "mgiProfileIdForIdentityValidationServiceProvider"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
