/**
 * MGOConsumerValidationSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.tempuri.StrucAddressLookupDef_1D;
import org.tempuri.StrucAddressLookupResponseDef_1D;
import org.tempuri.StrucAddressLookupResponseResultsDef_1D;
import org.tempuri.StrucID3CheckAddress_1D;
import org.tempuri.StrucID3CheckAddresses_1D;
import org.tempuri.StrucID3CheckDef_1D;
import org.tempuri.StrucID3CheckEmployment_1D;
import org.tempuri.StrucID3CheckFinancials_1D;
import org.tempuri.StrucID3CheckIdentity_1D;
import org.tempuri.StrucID3CheckPassport_1D;
import org.tempuri.StrucID3CheckPerson_1D;
import org.tempuri.StrucID3CheckRequestDetails_1D;
import org.tempuri.StrucID3CheckResponseDef_1D;
import org.tempuri.StrucID3CheckResponseResultsDef_1D;
import org.tempuri.StrucID3CheckTelephone_1D;
import org.tempuri.StrucID3CheckTelephones_1D;
import org.tempuri.StrucID3CheckUSDrivingLicence_1D;
import org.tempuri.StrucID3CheckUtilities_1D;
import org.tempuri.StrucID3CheckUtility_1D;

import com.moneygram.mgo.service.consumerVerification.id3check.proxy.ID3Check_1Proxy;
import com.moneygram.mgo.service.consumerVerification.util.MGOConsumerVerificationServiceResourceConfig;
import com.moneygram.mgo.service.consumerVerification.util.MapISOCodesToCountries;

public class MGOConsumerValidationSoapBindingImpl
		implements
		com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType {

	private ID3Check_1Proxy proxy = new ID3Check_1Proxy();
	private String username;
	private String password;

	public MGOConsumerValidationSoapBindingImpl() {
		username = MGOConsumerVerificationServiceResourceConfig.getInstance()
				.getGBGroupServiceUsername();
		password = MGOConsumerVerificationServiceResourceConfig.getInstance()
				.getGBGroupServicePassword();

	}

	public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse lookUpAddress(
			com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest lookUpAddressRequest)
			throws java.rmi.RemoteException,
			com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException {
		// Fill in addessLookUp proxy parameters.
		StrucAddressLookupDef_1D addressLookUp = fillInStrucAddressLookupDef_1D(lookUpAddressRequest);

		// Calling proxy.
		StrucAddressLookupResponseDef_1D strucAddressLookUpResponse = null;
		try {
			strucAddressLookUpResponse = proxy.addressLookUp(addressLookUp,
					username, password);

			// Fill in the response.
			return fillInLookUpAddressResponse(strucAddressLookUpResponse);
		} catch (RemoteException e) {
			e.printStackTrace();
//			logger.error("RemoteException calling address look up", e);
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
//			logger.error("Exception calling address look up", e);
			throw new RemoteException("Failed to addressLookUp method.", e);
		}
	}

	public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse validateRegistrationInformation(
			com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest)
			throws java.rmi.RemoteException,
			com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException {
		// Fill in addessLookUp proxy parameters.
		StrucID3CheckDef_1D checkDef = fillInStrucID3CheckDef_1D(validateRegistrationInformationRequest);

		// Calling proxy.
		try {

			StrucID3CheckResponseDef_1D strucCheckResponse = proxy
					.validateRegistrationInformation(checkDef, username, password);

			ValidateRegistrationInformationResponse response = fillInValidateRegistrationInformationResponse(strucCheckResponse);
			
			return response;
			
		} catch (RemoteException e) {
			e.printStackTrace();
//			logger.error("RemoteException calling validateRegistrationInformation", e);
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
//			logger.error("Exception calling validateRegistrationInformation", e);
			throw new RemoteException("Failed to validateRegistrationInformation method.", e);
		}

	}


	public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse fillInLookUpAddressResponse(
			StrucAddressLookupResponseDef_1D strucAddressLookUpResponse) {
		com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse response = new com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse();

		// RATHAUSSTR. 2#2523252c 24960 GLUECKSBURG (OSTSEE)#2523252c GERMANY
		// AGENTUR FUER ARBEIT STUTTGART#2523252c NORDBAHNHOFSTR. 30-34#2523252c 70145 STUTTGART#2523252c GERMANY

		List<Address> addresses = new ArrayList<Address>(
				strucAddressLookUpResponse.getIResultCount());

		StrucAddressLookupResponseResultsDef_1D[] lAddresses = strucAddressLookUpResponse
				.getStrucAddressLookupResponseResults();
		if (lAddresses != null && lAddresses.length > 0) {
			for (StrucAddressLookupResponseResultsDef_1D address_ : lAddresses) {
				Address address = new Address();
				if (address_.getSzBuilding() != null && !address_.getSzBuilding().isEmpty()
						&& address_.getSzStreet() != null && !address_.getSzStreet().isEmpty()
						&& address_.getSzCity() != null && !address_.getSzCity().isEmpty()
						&& address_.getSzZipPostcode() != null && !address_.getSzZipPostcode().isEmpty()) {
					address.setBuildingNumber(address_.getSzBuilding());
					address.setAddressLine1(address_.getSzStreet());
					address.setCity(address_.getSzCity());
					address.setPostalCode(address_.getSzZipPostcode());
					address.setTwoCharISOCountry("DE"); // This is not received
														// from the client.
				} else {
					String decodedAddress;
					try {
						 decodedAddress = java.net.URLDecoder.decode(address_.getSzFormattedAddress(), "UTF-8");
					} catch (UnsupportedEncodingException e) {
						break;
					}
					
					String[] addressSplitted = decodedAddress.split(",");
					String streetNumber = addressSplitted[addressSplitted.length-3];
					String postalCity = addressSplitted[addressSplitted.length-2];
					String country = addressSplitted[addressSplitted.length-1];
					String street = "";
					for (int i = 0; i < streetNumber.split(" ").length - 1; i++) {
						street += streetNumber.split(" ")[i] + " ";
					}
					String city = "";
					for (int i = 2; i < postalCity.split(" ").length; i++) {
						city += postalCity.split(" ")[i] + " ";
					}
					address.setAddressLine1(street);
					address.setBuildingNumber(streetNumber.split(" ")[streetNumber
							.split(" ").length - 1]);
					address.setPostalCode(postalCity.split(" ")[1]);
					address.setCity(city);
					address.setTwoCharISOCountry(MapISOCodesToCountries
							.getCodes(country.split(" ")[country.split(" ").length - 1]));
							
				}
				address.setState(address_.getSzStateDistrict());
				address.setPoBox(address_.getSzPOBox());
				address.setSubBuildingNumber(address_.getSzSubBuilding());
				address.setCompany(""); // This is not received from the client.
				address.setDepartament(""); // This is not received from the
											// client.
				address.setAddressLine2(""); // This is not received from the
												// client.
				address.setSubCity(""); // This is not received from the client.
				addresses.add(address);
			}
		}

		response.setStatus(strucAddressLookUpResponse.getSzStatus());
		response.setResultCount(new BigInteger(String
				.valueOf(strucAddressLookUpResponse.getIResultCount())));
		response.setMessage(strucAddressLookUpResponse.getSzMessage());

		response.setAddress(addresses.toArray(new Address[0]));

		return response;
	}

	public StrucAddressLookupDef_1D fillInStrucAddressLookupDef_1D(
			LookUpAddressRequest lookUpAddressRequest) {
		StrucID3CheckAddress_1D strucAddress = new StrucID3CheckAddress_1D();
		if (lookUpAddressRequest.getAddress() != null) {
			strucAddress.setSzAddressLayout("");
			strucAddress.setSzCedex("");
			strucAddress.setSzBuildingNumber(lookUpAddressRequest.getAddress()
					.getBuildingNumber() == null ? "" : lookUpAddressRequest
					.getAddress().getBuildingNumber());
			strucAddress.setSzEndDate("");
			strucAddress.setSzFFAddress1("");
			strucAddress.setSzFFAddress2("");
			strucAddress.setSzFFAddress3("");
			strucAddress.setSzFFAddress4("");
			strucAddress.setSzPremise("");
			strucAddress.setSzPrincipality("");
			strucAddress.setSzRegion("");
			strucAddress.setSzStartDate("");
			strucAddress.setSzCityTown(lookUpAddressRequest.getAddress()
					.getCity() == null ? "" : lookUpAddressRequest.getAddress()
					.getCity());
			strucAddress.setSzCompany(lookUpAddressRequest.getAddress()
					.getCompany() == null ? "" : lookUpAddressRequest
					.getAddress().getCompany());
			strucAddress.setSzDepartment(lookUpAddressRequest.getAddress()
					.getDepartament() == null ? "" : lookUpAddressRequest
					.getAddress().getDepartament());
			strucAddress.setSzPOBox(lookUpAddressRequest.getAddress()
					.getPoBox() == null ? "" : lookUpAddressRequest
					.getAddress().getPoBox());
			strucAddress.setSzSubBuildingNumber(lookUpAddressRequest
					.getAddress().getSubBuildingNumber() == null ? ""
					: lookUpAddressRequest.getAddress().getSubBuildingNumber());
			strucAddress.setSzSubCity(lookUpAddressRequest.getAddress()
					.getSubCity() == null ? "" : lookUpAddressRequest
					.getAddress().getSubCity());
			strucAddress.setSzStreet(lookUpAddressRequest.getAddress()
					.getAddressLine1() == null ? "" : lookUpAddressRequest
					.getAddress().getAddressLine1());
			strucAddress.setSzSubStreet(lookUpAddressRequest.getAddress()
					.getAddressLine2() == null ? "" : lookUpAddressRequest
					.getAddress().getAddressLine2());
			strucAddress.setSzStateDistrict(lookUpAddressRequest.getAddress()
					.getState() == null ? "" : lookUpAddressRequest
					.getAddress().getState());
			strucAddress.setSzCountry(lookUpAddressRequest.getAddress()
					.getTwoCharISOCountry() == null ? ""
					: MapISOCodesToCountries.getCountries(lookUpAddressRequest
							.getAddress().getTwoCharISOCountry()));
			strucAddress.setSzZipPCode(lookUpAddressRequest.getAddress()
					.getPostalCode() == null ? "" : lookUpAddressRequest
					.getAddress().getPostalCode());
		}
		StrucID3CheckAddress_1D strucDisplayFields = new StrucID3CheckAddress_1D();
		strucDisplayFields.setSzAddressLayout("5");
		strucDisplayFields.setSzCedex("");
		strucDisplayFields.setSzBuildingNumber("");
		strucDisplayFields.setSzEndDate("");
		strucDisplayFields.setSzFFAddress1("");
		strucDisplayFields.setSzFFAddress2("");
		strucDisplayFields.setSzFFAddress3("");
		strucDisplayFields.setSzFFAddress4("");
		strucDisplayFields.setSzPremise("");
		strucDisplayFields.setSzPrincipality("");
		strucDisplayFields.setSzRegion("");
		strucDisplayFields.setSzStartDate("");
		strucDisplayFields.setSzCityTown("");
		strucDisplayFields.setSzCompany("");
		strucDisplayFields.setSzDepartment("");
		strucDisplayFields.setSzPOBox("");
		strucDisplayFields.setSzSubBuildingNumber("");
		strucDisplayFields.setSzSubCity("");
		strucDisplayFields.setSzStreet("");
		strucDisplayFields.setSzSubStreet("");
		strucDisplayFields.setSzStateDistrict("");
		strucDisplayFields.setSzCountry("");
		strucDisplayFields.setSzZipPCode("");

		StrucAddressLookupDef_1D addressLookUp = new StrucAddressLookupDef_1D(
				strucAddress, strucDisplayFields);

		return addressLookUp;
	}

	public StrucID3CheckDef_1D fillInStrucID3CheckDef_1D(
			com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest) {
		// Fill in addessLookUp proxy parameters.
		StrucID3CheckAddresses_1D checkAddresses = new StrucID3CheckAddresses_1D();
		if (validateRegistrationInformationRequest.getAddress() != null) {
			StrucID3CheckAddress_1D checkAddress = new StrucID3CheckAddress_1D();
			checkAddress
					.setSzBuildingNumber(validateRegistrationInformationRequest
							.getAddress().getBuildingNumber() == null ? ""
							: validateRegistrationInformationRequest
									.getAddress().getBuildingNumber());
			checkAddress
					.setSzSubBuildingNumber(validateRegistrationInformationRequest
							.getAddress().getSubBuildingNumber() == null ? ""
							: validateRegistrationInformationRequest
									.getAddress().getSubBuildingNumber());
			checkAddress.setSzCityTown(validateRegistrationInformationRequest
					.getAddress().getCity() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getCity());
			checkAddress.setSzCompany(validateRegistrationInformationRequest
					.getAddress().getCompany() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getCompany());
			checkAddress.setSzDepartment(validateRegistrationInformationRequest
					.getAddress().getDepartament() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getDepartament());
			checkAddress.setSzPOBox(validateRegistrationInformationRequest
					.getAddress().getPoBox() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getPoBox());
			checkAddress
					.setSzSubBuildingNumber(validateRegistrationInformationRequest
							.getAddress().getSubBuildingNumber() == null ? ""
							: validateRegistrationInformationRequest
									.getAddress().getSubBuildingNumber());
			checkAddress.setSzSubCity(validateRegistrationInformationRequest
					.getAddress().getSubCity() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getSubCity());
			checkAddress.setSzStreet(validateRegistrationInformationRequest
					.getAddress().getAddressLine1() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getAddressLine1());
			checkAddress.setSzSubStreet(validateRegistrationInformationRequest
					.getAddress().getAddressLine2() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getAddressLine2());
			checkAddress
					.setSzStateDistrict(validateRegistrationInformationRequest
							.getAddress().getState() == null ? ""
							: validateRegistrationInformationRequest
									.getAddress().getState());
			checkAddress.setSzCountry(validateRegistrationInformationRequest
					.getAddress().getTwoCharISOCountry() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getTwoCharISOCountry());
			checkAddress.setSzZipPCode(validateRegistrationInformationRequest
					.getAddress().getPostalCode() == null ? ""
					: validateRegistrationInformationRequest.getAddress()
							.getPostalCode());
			checkAddresses.setStrucAddress1(checkAddress);
			checkAddresses.setStrucAddress2(new StrucID3CheckAddress_1D());
			checkAddresses.setStrucAddress3(new StrucID3CheckAddress_1D());
		}

		StrucID3CheckPerson_1D checkPerson = new StrucID3CheckPerson_1D();
		if (validateRegistrationInformationRequest.getConsumer() != null) {
			checkPerson.setSzTitle(validateRegistrationInformationRequest
					.getConsumer().getTitle() == null ? ""
					: validateRegistrationInformationRequest.getConsumer()
							.getTitle());
			checkPerson.setSzSurname(validateRegistrationInformationRequest
					.getConsumer().getLastName() == null ? ""
					: validateRegistrationInformationRequest.getConsumer()
							.getLastName());
			checkPerson.setSzFirstName(validateRegistrationInformationRequest
					.getConsumer().getFirstName() == null ? ""
					: validateRegistrationInformationRequest.getConsumer()
							.getFirstName());
			checkPerson.setSzMiddleName(validateRegistrationInformationRequest
					.getConsumer().getMiddleName() == null ? ""
					: validateRegistrationInformationRequest.getConsumer()
							.getMiddleName());
			checkPerson.setSzGender(validateRegistrationInformationRequest
					.getConsumer().getGender() == null ? ""
					: validateRegistrationInformationRequest.getConsumer()
							.getGender());
			checkPerson.setSzDateOfBirth("");
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				checkPerson
						.setSzDateOfBirth(validateRegistrationInformationRequest
								.getConsumer().getDateOfBirth() == null ? ""
								: formatter
										.format(validateRegistrationInformationRequest
												.getConsumer().getDateOfBirth()));
			} catch (Exception e) {
				// If the timestamp is not right, so this will not be on the
				// request.
			}
			checkPerson
					.setSzCountryOfBirth(validateRegistrationInformationRequest
							.getConsumer().getCountryOfBirth() == null ? ""
							: validateRegistrationInformationRequest
									.getConsumer().getCountryOfBirth());
			checkPerson
					.setSzPlaceOfBirth(validateRegistrationInformationRequest
							.getConsumer().getPlaceOfBirth() == null ? ""
							: validateRegistrationInformationRequest
									.getConsumer().getPlaceOfBirth());
		}

		StrucID3CheckRequestDetails_1D checkRequestDetails = new StrucID3CheckRequestDetails_1D();
		checkRequestDetails
				.setSzProfile(validateRegistrationInformationRequest
						.getMgiProfileIdForIdentityValidationServiceProvider() == null ? ""
						: validateRegistrationInformationRequest
								.getMgiProfileIdForIdentityValidationServiceProvider());
		if (validateRegistrationInformationRequest.getConsumer() != null) {
			checkRequestDetails
					.setSzCustomerRef(validateRegistrationInformationRequest
							.getConsumer().getConsumerId() == null ? ""
							: validateRegistrationInformationRequest
									.getConsumer().getConsumerId());
		}

		StrucID3CheckTelephones_1D checkTelephones = new StrucID3CheckTelephones_1D();
		StrucID3CheckTelephone_1D emptyCheckTelephone = new StrucID3CheckTelephone_1D();
		emptyCheckTelephone.setSzNumber("");
		emptyCheckTelephone.setBPublished(false);
		checkTelephones.setStrucHomeTelephone(emptyCheckTelephone);
		checkTelephones.setStrucMobileTelephone(emptyCheckTelephone);
		checkTelephones.setStrucWorkTelephone(emptyCheckTelephone);
		if (validateRegistrationInformationRequest.getPhones() != null) {
			for (Phone phone : validateRegistrationInformationRequest
					.getPhones()) {
				StrucID3CheckTelephone_1D checkTelephone = new StrucID3CheckTelephone_1D();
				checkTelephone.setSzNumber(phone.getNumber());
				checkTelephone.setBPublished(Boolean.getBoolean(phone
						.getPublished()));
				if (phone.getType() == PhoneType.home) {
					checkTelephones.setStrucHomeTelephone(checkTelephone);
				} else if (phone.getType() == PhoneType.mobile) {
					checkTelephones.setStrucMobileTelephone(checkTelephone);
				} else if (phone.getType() == PhoneType.work) {
					checkTelephones.setStrucWorkTelephone(checkTelephone);
				}
			}
		}

		StrucID3CheckDef_1D checkDef = new StrucID3CheckDef_1D();
		checkDef.setBStatus(false);
		checkDef.setStrucAddresses(checkAddresses);
		checkDef.setStrucPerson(checkPerson);
		checkDef.setStrucRequestDetails(checkRequestDetails);
		checkDef.setStrucTelephones(checkTelephones);
		StrucID3CheckEmployment_1D employment = new StrucID3CheckEmployment_1D();
		employment.setSzEndDate("");
		employment.setSzStartDate("");
		employment.setSzStatus("");
		employment.setSzType("");
		checkDef.setStrucEmployment(employment);
		StrucID3CheckFinancials_1D financials = new StrucID3CheckFinancials_1D();
		financials.setSzCardNumber("");
		financials.setSzIssueNumber("");
		financials.setSzMMEndDate("");
		financials.setSzMMStartDate("");
		financials.setSzNameOnCard("");
		financials.setSzSecurityNumber("");
		financials.setSzType("");
		financials.setSzYYEndDate("");
		financials.setSzYYStartDate("");
		checkDef.setStrucFinancials(financials);
		StrucID3CheckIdentity_1D identity = new StrucID3CheckIdentity_1D();
		identity.setSzIDCardNumber("");
		identity.setSzIDCountry("");
		identity.setSzLocation("");
		identity.setSzMedicareNumber("");
		identity.setSzMedicareReferenceNumber("");
		identity.setSzNumber("");
		identity.setSzQuovaCCard("");
		identity.setSzQuovaCountry("");
		identity.setSzQuovaIP("");
		identity.setSzQuovaUSState("");
		identity.setSzType("");
		checkDef.setStrucIdentity(identity);
		StrucID3CheckUSDrivingLicence_1D drivingLicense = new StrucID3CheckUSDrivingLicence_1D();
		drivingLicense.setSzNumber("");
		drivingLicense.setSzState("");
		checkDef.setStrucUSDrivingLicence(drivingLicense);
		StrucID3CheckUtilities_1D utilities = new StrucID3CheckUtilities_1D();
		utilities.setStrucElectricity(new StrucID3CheckUtility_1D());
		checkDef.setStrucUtilities(utilities);
		StrucID3CheckPassport_1D passport = new StrucID3CheckPassport_1D();
		passport.setSzCountryOfOrigin("");
		passport.setSzExpiryDate("");
		passport.setSzNumber("");
		passport.setSzShortPassportNumber("");
		checkDef.setStrucPassport(passport);

		return checkDef;
	}

	public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse fillInValidateRegistrationInformationResponse(
			StrucID3CheckResponseDef_1D strucCheckResponse) {
		com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse response = new com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse();

		response.setConsumerId(strucCheckResponse.getSzCustomerRef() == null ? ""
				: strucCheckResponse.getSzCustomerRef());
		response.setStatus(strucCheckResponse.getSzStatus() == null ? ""
				: strucCheckResponse.getSzStatus());
		response.setAutenticationId(strucCheckResponse.getSzAuthenticationID() == null ? ""
				: strucCheckResponse.getSzAuthenticationID());
		try {
			SimpleDateFormat sdf = new SimpleDateFormat();
			sdf.applyPattern("dd/MM/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(strucCheckResponse.getSzTimestamp()));
			response.setTimestamp(c);
		} catch (Exception e) {
			// If the timestamp is not right, so this will not be on the
			// response.
			response.setTimestamp(Calendar.getInstance());
		}
		response.setBandText(strucCheckResponse.getSzBandText() == null ? ""
				: strucCheckResponse.getSzBandText());
		response.setScore(String.valueOf(strucCheckResponse.getNScore()));
		response.setRemarks(strucCheckResponse.getSzRemarks() == null ? ""
				: strucCheckResponse.getSzRemarks());
		List<Result> results = new ArrayList<Result>();
		if (strucCheckResponse.getStrucID3CheckResponseResults() != null) {
			for (StrucID3CheckResponseResultsDef_1D checkResponseResult : strucCheckResponse
					.getStrucID3CheckResponseResults()) {
				Result result = new Result();
				result.setCheckName(checkResponseResult.getSzUID() == null ? ""
						: checkResponseResult.getSzUID());
				result.setCheckDescription(checkResponseResult
						.getSzUIDDescription() == null ? ""
						: checkResponseResult.getSzUIDDescription());
				result.setCode(checkResponseResult.getSzResultCode() == null ? ""
						: checkResponseResult.getSzResultCode());
				result.setDescription(checkResponseResult
						.getSzResultDescription() == null ? ""
						: checkResponseResult.getSzResultDescription());
				result.setSeverity("");
				
				results.add(result);
			}
		}
		Results results2 = new Results(results.toArray(new Result[0]));
		response.setResults(results2);

		return response;
	}

}
