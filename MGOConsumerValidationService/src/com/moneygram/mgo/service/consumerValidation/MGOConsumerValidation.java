/**
 * MGOConsumerValidation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public interface MGOConsumerValidation extends javax.xml.rpc.Service {
    public java.lang.String getMGOConsumerValidationAddress();

    public com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType getMGOConsumerValidation() throws javax.xml.rpc.ServiceException;

    public com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType getMGOConsumerValidation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
