/**
 * MGOConsumerValidationSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public class MGOConsumerValidationSoapBindingSkeleton implements com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType, org.apache.axis.wsdl.Skeleton {
    private com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressRequest"), com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("lookUpAddress", _params, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "lookUpAddressResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "lookUpAddress"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("lookUpAddress") == null) {
            _myOperations.put("lookUpAddress", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("lookUpAddress")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("consumerValidationServiceException");
        _fault.setQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"));
        _fault.setClassName("com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationRequest"), com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("validateRegistrationInformation", _params, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "validateRegistrationInformationResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "validateRegistrationInformation"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("validateRegistrationInformation") == null) {
            _myOperations.put("validateRegistrationInformation", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("validateRegistrationInformation")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("consumerValidationServiceException");
        _fault.setQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"));
        _fault.setClassName("com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumerValidation", "consumerValidationServiceException"));
        _oper.addFault(_fault);
    }

    public MGOConsumerValidationSoapBindingSkeleton() {
        this.impl = new com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationSoapBindingImpl();
    }

    public MGOConsumerValidationSoapBindingSkeleton(com.moneygram.mgo.service.consumerValidation.MGOConsumerValidationPortType impl) {
        this.impl = impl;
    }
    public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse lookUpAddress(com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest lookUpAddressRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException
    {
        com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse ret = impl.lookUpAddress(lookUpAddressRequest);
        
        return ret;
    }

    public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse validateRegistrationInformation(com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException
    {
        com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse ret = impl.validateRegistrationInformation(validateRegistrationInformationRequest);
        return ret;
    }

}
