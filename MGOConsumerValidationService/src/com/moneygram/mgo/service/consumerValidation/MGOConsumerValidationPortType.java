/**
 * MGOConsumerValidationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumerValidation;

public interface MGOConsumerValidationPortType extends java.rmi.Remote {
    public com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse lookUpAddress(com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest lookUpAddressRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException;
    public com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationResponse validateRegistrationInformation(com.moneygram.mgo.service.consumerValidation.ValidateRegistrationInformationRequest validateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.service.consumerValidation.ConsumerValidationServiceException;
}
