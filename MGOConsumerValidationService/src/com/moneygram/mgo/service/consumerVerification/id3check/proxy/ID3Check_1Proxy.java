package com.moneygram.mgo.service.consumerVerification.id3check.proxy;

import java.rmi.RemoteException;

import org.tempuri.ID3CheckWSSoapProxy;
import org.tempuri.StrucAddressLookupDef_1D;
import org.tempuri.StrucAddressLookupResponseDef_1D;
import org.tempuri.StrucID3CheckDef_1D;
import org.tempuri.StrucID3CheckResponseDef_1D;

import com.moneygram.mgo.service.consumerVerification.util.MGOConsumerVerificationServiceResourceConfig;

public class ID3Check_1Proxy {
	
	
	String endPoint;
	private ID3CheckWSSoapProxy id3CheckWSSoapProxy;
	public ID3Check_1Proxy()
	{
		endPoint=MGOConsumerVerificationServiceResourceConfig.getInstance().getGBGroupServiceURL();
	}
	private ID3CheckWSSoapProxy getID3Check_1Client() {
		if (id3CheckWSSoapProxy != null) {
			return id3CheckWSSoapProxy;
		} else {
			
			id3CheckWSSoapProxy = new ID3CheckWSSoapProxy(endPoint);
			
			return id3CheckWSSoapProxy;
		}
	}
	
	public StrucAddressLookupResponseDef_1D addressLookUp(StrucAddressLookupDef_1D addressLookUp, String userName, String password) throws RemoteException {
		return getID3Check_1Client().addressLookup_1D(addressLookUp, userName, password);
	}
	
	public StrucID3CheckResponseDef_1D validateRegistrationInformation(StrucID3CheckDef_1D checkDef, String userName, String password) throws RemoteException {
		return getID3Check_1Client().ID3Check_1D(checkDef, userName, password);
	}

}
