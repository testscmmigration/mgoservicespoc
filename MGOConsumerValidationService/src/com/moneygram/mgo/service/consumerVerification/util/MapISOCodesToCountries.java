package com.moneygram.mgo.service.consumerVerification.util;

import java.util.HashMap;
import java.util.Map;

public class MapISOCodesToCountries {

	private static final Map<String, String> map;
	private static final Map<String, String> notmap;
	static {
		map = new HashMap<String, String>();
		map.put("DE", "Germany");
		notmap = new HashMap<String, String>();
		notmap.put("GERMANY", "DE");
	}
	
	public static String getCountries(String ISOCode) {
		String result = "";
		if (map.get(ISOCode) != null) {
			result = map.get(ISOCode);
		}
		return result;
	}
	
	public static String getCodes(String countryName) {
		String result = "";
		if (notmap.get(countryName) != null) {
			result = notmap.get(countryName);
		}
		return result;
	}
	
}
