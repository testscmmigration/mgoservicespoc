package com.moneygram.mgo.service.consumerVerification.util;

import com.moneygram.common.service.util.ResourceConfig;

public class MGOConsumerVerificationServiceResourceConfig extends ResourceConfig {
	
	public static final String GBGROUP_SERVICE_URL = "GBGROUP_SERVICE_URL";
	public static final String GBGROUP_SERVICE_USERNAME = "GBGROUP_SERVICE_USERNAME";
	public static final String GBGROUP_SERVICE_PASSWORD = "GBGROUP_SERVICE_PASSWORD";
	public static final String RESOURCE_REFERENCE_JNDI = "java:comp/env/rep/MGOConsumerServiceResourceReference";
	
	
	/**
	 * singleton instance.
	 */
	private static MGOConsumerVerificationServiceResourceConfig instance = null;
	
	/**
	 * 
	 * @return
	 */
	public static MGOConsumerVerificationServiceResourceConfig getInstance() {
		if (instance == null) {
			synchronized (MGOConsumerVerificationServiceResourceConfig.class) {
				
				if (instance == null) {
					
					instance = new MGOConsumerVerificationServiceResourceConfig();
					
					
					try {
						instance.initResourceConfig();
					} catch (Exception e) {
//						logger.error("Exception calling address look up",e);
//						e.printStackTrace();
					}
					
				}
			}
		}
		
		return instance;
	}
	
	public String getGBGroupServiceURL() {
		return getAttributeValue(GBGROUP_SERVICE_URL);
	}
	
	public String getGBGroupServiceUsername() {
		return getAttributeValue(GBGROUP_SERVICE_USERNAME);
	}
	
	public String getGBGroupServicePassword() {
		return getAttributeValue(GBGROUP_SERVICE_PASSWORD);
	}

	@Override
	protected String getResourceConfigurationJndiName() {
		return RESOURCE_REFERENCE_JNDI;
	}

}
