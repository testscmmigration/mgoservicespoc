package com.moneygram.mgo.service.consumerVerification.command;

import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumerValidation.LookUpAddressRequest;
import com.moneygram.mgo.service.consumerValidation.LookUpAddressResponse;

public class LookUpAddressCommand extends ReadCommand {

	
	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		LookUpAddressRequest luaRequest = (LookUpAddressRequest) request;
		
		
		
		
		LookUpAddressResponse response = new LookUpAddressResponse(); 
		
		return null;
	}

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof LookUpAddressRequest;
	}

}
