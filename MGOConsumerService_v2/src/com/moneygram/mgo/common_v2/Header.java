/**
 * Header.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.common_v2;

public class Header  implements java.io.Serializable {
    /* currently not used */
    private com.moneygram.mgo.common_v2.SecurityHeader[] security;

    /* currently not used */
    private com.moneygram.mgo.common_v2.RoutingContextHeader[] routingContext;

    private com.moneygram.mgo.common_v2.ProcessingInstruction processingInstruction;

    private com.moneygram.mgo.common_v2.ClientHeader clientHeader;

    public Header() {
    }

    public Header(
           com.moneygram.mgo.common_v2.SecurityHeader[] security,
           com.moneygram.mgo.common_v2.RoutingContextHeader[] routingContext,
           com.moneygram.mgo.common_v2.ProcessingInstruction processingInstruction,
           com.moneygram.mgo.common_v2.ClientHeader clientHeader) {
           this.security = security;
           this.routingContext = routingContext;
           this.processingInstruction = processingInstruction;
           this.clientHeader = clientHeader;
    }


    /**
     * Gets the security value for this Header.
     * 
     * @return security   * currently not used
     */
    public com.moneygram.mgo.common_v2.SecurityHeader[] getSecurity() {
        return security;
    }


    /**
     * Sets the security value for this Header.
     * 
     * @param security   * currently not used
     */
    public void setSecurity(com.moneygram.mgo.common_v2.SecurityHeader[] security) {
        this.security = security;
    }

    public com.moneygram.mgo.common_v2.SecurityHeader getSecurity(int i) {
        return this.security[i];
    }

    public void setSecurity(int i, com.moneygram.mgo.common_v2.SecurityHeader _value) {
        this.security[i] = _value;
    }


    /**
     * Gets the routingContext value for this Header.
     * 
     * @return routingContext   * currently not used
     */
    public com.moneygram.mgo.common_v2.RoutingContextHeader[] getRoutingContext() {
        return routingContext;
    }


    /**
     * Sets the routingContext value for this Header.
     * 
     * @param routingContext   * currently not used
     */
    public void setRoutingContext(com.moneygram.mgo.common_v2.RoutingContextHeader[] routingContext) {
        this.routingContext = routingContext;
    }

    public com.moneygram.mgo.common_v2.RoutingContextHeader getRoutingContext(int i) {
        return this.routingContext[i];
    }

    public void setRoutingContext(int i, com.moneygram.mgo.common_v2.RoutingContextHeader _value) {
        this.routingContext[i] = _value;
    }


    /**
     * Gets the processingInstruction value for this Header.
     * 
     * @return processingInstruction
     */
    public com.moneygram.mgo.common_v2.ProcessingInstruction getProcessingInstruction() {
        return processingInstruction;
    }


    /**
     * Sets the processingInstruction value for this Header.
     * 
     * @param processingInstruction
     */
    public void setProcessingInstruction(com.moneygram.mgo.common_v2.ProcessingInstruction processingInstruction) {
        this.processingInstruction = processingInstruction;
    }


    /**
     * Gets the clientHeader value for this Header.
     * 
     * @return clientHeader
     */
    public com.moneygram.mgo.common_v2.ClientHeader getClientHeader() {
        return clientHeader;
    }


    /**
     * Sets the clientHeader value for this Header.
     * 
     * @param clientHeader
     */
    public void setClientHeader(com.moneygram.mgo.common_v2.ClientHeader clientHeader) {
        this.clientHeader = clientHeader;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Header)) return false;
        Header other = (Header) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.security==null && other.getSecurity()==null) || 
             (this.security!=null &&
              java.util.Arrays.equals(this.security, other.getSecurity()))) &&
            ((this.routingContext==null && other.getRoutingContext()==null) || 
             (this.routingContext!=null &&
              java.util.Arrays.equals(this.routingContext, other.getRoutingContext()))) &&
            ((this.processingInstruction==null && other.getProcessingInstruction()==null) || 
             (this.processingInstruction!=null &&
              this.processingInstruction.equals(other.getProcessingInstruction()))) &&
            ((this.clientHeader==null && other.getClientHeader()==null) || 
             (this.clientHeader!=null &&
              this.clientHeader.equals(other.getClientHeader())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSecurity() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSecurity());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSecurity(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRoutingContext() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRoutingContext());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRoutingContext(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProcessingInstruction() != null) {
            _hashCode += getProcessingInstruction().hashCode();
        }
        if (getClientHeader() != null) {
            _hashCode += getClientHeader().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Header.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Header"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("security");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "security"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "SecurityHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingContext");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "routingContext"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RoutingContextHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processingInstruction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "processingInstruction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ProcessingInstruction"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "clientHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ClientHeader"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
