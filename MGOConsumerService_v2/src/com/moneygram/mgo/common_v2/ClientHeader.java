/**
 * ClientHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.common_v2;

public class ClientHeader  implements java.io.Serializable {
    private java.lang.String clientRequestID;

    private java.lang.String clientSessionID;

    public ClientHeader() {
    }

    public ClientHeader(
           java.lang.String clientRequestID,
           java.lang.String clientSessionID) {
           this.clientRequestID = clientRequestID;
           this.clientSessionID = clientSessionID;
    }


    /**
     * Gets the clientRequestID value for this ClientHeader.
     * 
     * @return clientRequestID
     */
    public java.lang.String getClientRequestID() {
        return clientRequestID;
    }


    /**
     * Sets the clientRequestID value for this ClientHeader.
     * 
     * @param clientRequestID
     */
    public void setClientRequestID(java.lang.String clientRequestID) {
        this.clientRequestID = clientRequestID;
    }


    /**
     * Gets the clientSessionID value for this ClientHeader.
     * 
     * @return clientSessionID
     */
    public java.lang.String getClientSessionID() {
        return clientSessionID;
    }


    /**
     * Sets the clientSessionID value for this ClientHeader.
     * 
     * @param clientSessionID
     */
    public void setClientSessionID(java.lang.String clientSessionID) {
        this.clientSessionID = clientSessionID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientHeader)) return false;
        ClientHeader other = (ClientHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clientRequestID==null && other.getClientRequestID()==null) || 
             (this.clientRequestID!=null &&
              this.clientRequestID.equals(other.getClientRequestID()))) &&
            ((this.clientSessionID==null && other.getClientSessionID()==null) || 
             (this.clientSessionID!=null &&
              this.clientSessionID.equals(other.getClientSessionID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClientRequestID() != null) {
            _hashCode += getClientRequestID().hashCode();
        }
        if (getClientSessionID() != null) {
            _hashCode += getClientSessionID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ClientHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientRequestID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "clientRequestID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientSessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "clientSessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
