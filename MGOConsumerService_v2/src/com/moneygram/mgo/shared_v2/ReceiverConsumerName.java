/**
 * ReceiverConsumerName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.shared_v2;

public class ReceiverConsumerName  extends com.moneygram.mgo.shared_v2.ConsumerName  implements java.io.Serializable {
    private java.lang.String maternalName;

    public ReceiverConsumerName() {
    }

    public ReceiverConsumerName(
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.String middleName,
           java.lang.String maternalName) {
        super(
            firstName,
            lastName,
            middleName);
        this.maternalName = maternalName;
    }


    /**
     * Gets the maternalName value for this ReceiverConsumerName.
     * 
     * @return maternalName
     */
    public java.lang.String getMaternalName() {
        return maternalName;
    }


    /**
     * Sets the maternalName value for this ReceiverConsumerName.
     * 
     * @param maternalName
     */
    public void setMaternalName(java.lang.String maternalName) {
        this.maternalName = maternalName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReceiverConsumerName)) return false;
        ReceiverConsumerName other = (ReceiverConsumerName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.maternalName==null && other.getMaternalName()==null) || 
             (this.maternalName!=null &&
              this.maternalName.equals(other.getMaternalName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMaternalName() != null) {
            _hashCode += getMaternalName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReceiverConsumerName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maternalName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "maternalName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
