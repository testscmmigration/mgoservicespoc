package com.moneygram.mgo.service.consumer_v2;

import java.util.Date;
import java.util.List;

/**
 * Represents the info to be saved when you do a request to validateregisterinfo
 * @author vx15
 */
public class ValidateRegistrationSaveInfo {

	private Long custId;
	private String authnId;
	private Date authnDate;
	private String scoreNbr;
	private String deciBandText;
	private String remarksText;
	private String statText;
	
	private List<ValidateRegistrationSaveInfoRsls> validateRegistrationSaveInfoRsls;
	
	
	public ValidateRegistrationSaveInfo() {

	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getAuthnId() {
		return authnId;
	}

	public void setAuthnId(String authnId) {
		this.authnId = authnId;
	}

	public Date getAuthnDate() {
		return authnDate;
	}

	public void setAuthnDate(Date authnDate) {
		this.authnDate = authnDate;
	}

	public String getScoreNbr() {
		return scoreNbr;
	}

	public void setScoreNbr(String scoreNbr) {
		this.scoreNbr = scoreNbr;
	}

	public String getDeciBandText() {
		return deciBandText;
	}

	public void setDeciBandText(String deciBandText) {
		this.deciBandText = deciBandText;
	}

	public String getRemarksText() {
		return remarksText;
	}

	public void setRemarksText(String remarksText) {
		this.remarksText = remarksText;
	}

	public String getStatText() {
		return statText;
	}

	public void setStatText(String statText) {
		this.statText = statText;
	}

	public void setValidateRegistrationSaveInfoRsls(
			List<ValidateRegistrationSaveInfoRsls> validateRegistrationSaveInfoRsls) {
		this.validateRegistrationSaveInfoRsls = validateRegistrationSaveInfoRsls;
	}
	
	public List<ValidateRegistrationSaveInfoRsls> getValidateRegistrationSaveInfoRsls() {
		return validateRegistrationSaveInfoRsls;
	}

	@Override
	public String toString() {
		return "ValidateRegistrationSaveInfo [custId=" + custId + ", authnId="
				+ authnId + ", authnDate=" + authnDate + ", scoreNbr="
				+ scoreNbr + ", deciBandText=" + deciBandText
				+ ", remarksText=" + remarksText + ", statText=" + statText
				+ ", validateRegistrationSaveInfoRsls="
				+ validateRegistrationSaveInfoRsls + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authnDate == null) ? 0 : authnDate.hashCode());
		result = prime * result + ((authnId == null) ? 0 : authnId.hashCode());
		result = prime * result + ((custId == null) ? 0 : custId.hashCode());
		result = prime * result
				+ ((deciBandText == null) ? 0 : deciBandText.hashCode());
		result = prime * result
				+ ((remarksText == null) ? 0 : remarksText.hashCode());
		result = prime * result
				+ ((scoreNbr == null) ? 0 : scoreNbr.hashCode());
		result = prime * result
				+ ((statText == null) ? 0 : statText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidateRegistrationSaveInfo other = (ValidateRegistrationSaveInfo) obj;
		if (authnDate == null) {
			if (other.authnDate != null)
				return false;
		} else if (!authnDate.equals(other.authnDate))
			return false;
		if (authnId == null) {
			if (other.authnId != null)
				return false;
		} else if (!authnId.equals(other.authnId))
			return false;
		if (custId == null) {
			if (other.custId != null)
				return false;
		} else if (!custId.equals(other.custId))
			return false;
		if (deciBandText == null) {
			if (other.deciBandText != null)
				return false;
		} else if (!deciBandText.equals(other.deciBandText))
			return false;
		if (remarksText == null) {
			if (other.remarksText != null)
				return false;
		} else if (!remarksText.equals(other.remarksText))
			return false;
		if (scoreNbr == null) {
			if (other.scoreNbr != null)
				return false;
		} else if (!scoreNbr.equals(other.scoreNbr))
			return false;
		if (statText == null) {
			if (other.statText != null)
				return false;
		} else if (!statText.equals(other.statText))
			return false;
		return true;
	}
	
	
	
	
}
