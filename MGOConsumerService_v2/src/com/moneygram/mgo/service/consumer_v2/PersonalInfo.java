/**
 * PersonalInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class PersonalInfo  implements java.io.Serializable {
    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.lang.String middleName;
    
    private java.lang.String secondLastName;

    private java.lang.String ssn;

    private java.util.Calendar dateOfBirth;

    private java.lang.String gender;

    private java.lang.String externalId;

    private java.lang.String externalIdCountryOfIssuance;

    private java.lang.String externalIdType;

    private java.lang.String externalIdStateOfIssuance;

    private java.util.Date externalIdDateOfIssuance;

    private java.util.Date externalIdDateOfExpiration;

    private java.lang.String countryOfBirthCode;

    private java.lang.String passportMRZ;

    public PersonalInfo() {
    }

    public PersonalInfo(
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.String middleName,
           java.lang.String secondLastName,
           java.lang.String ssn,
           java.util.Calendar dateOfBirth,
           java.lang.String gender,
           java.lang.String externalId,
           java.lang.String externalIdCountryOfIssuance,
           java.lang.String externalIdType,
           java.lang.String externalIdStateOfIssuance,
           java.util.Date externalIdDateOfIssuance,
           java.util.Date externalIdDateOfExpiration,
           java.lang.String countryOfBirthCode,
           java.lang.String passportMRZ) {
           this.firstName = firstName;
           this.lastName = lastName;
           this.middleName = middleName;
           this.secondLastName = secondLastName;
           this.ssn = ssn;
           this.dateOfBirth = dateOfBirth;
           this.gender = gender;
           this.externalId = externalId;
           this.externalIdCountryOfIssuance = externalIdCountryOfIssuance;
           this.externalIdType = externalIdType;
           this.externalIdStateOfIssuance = externalIdStateOfIssuance;
           this.externalIdDateOfIssuance = externalIdDateOfIssuance;
           this.externalIdDateOfExpiration = externalIdDateOfExpiration;
           this.countryOfBirthCode = countryOfBirthCode;
           this.passportMRZ = passportMRZ;
    }


    /**
     * Gets the firstName value for this PersonalInfo.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this PersonalInfo.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this PersonalInfo.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this PersonalInfo.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the middleName value for this PersonalInfo.
     * 
     * @return middleName
     */
    public java.lang.String getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this PersonalInfo.
     * 
     * @param middleName
     */
    public void setMiddleName(java.lang.String middleName) {
        this.middleName = middleName;
    }


    /**
     * Gets the ssn value for this PersonalInfo.
     * 
     * @return ssn
     */
    public java.lang.String getSsn() {
        return ssn;
    }


    /**
     * Sets the ssn value for this PersonalInfo.
     * 
     * @param ssn
     */
    public void setSsn(java.lang.String ssn) {
        this.ssn = ssn;
    }


    /**
     * Gets the dateOfBirth value for this PersonalInfo.
     * 
     * @return dateOfBirth
     */
    public java.util.Calendar getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this PersonalInfo.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.util.Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the gender value for this PersonalInfo.
     * 
     * @return gender
     */
    public java.lang.String getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this PersonalInfo.
     * 
     * @param gender
     */
    public void setGender(java.lang.String gender) {
        this.gender = gender;
    }


    /**
     * Gets the externalId value for this PersonalInfo.
     * 
     * @return externalId
     */
    public java.lang.String getExternalId() {
        return externalId;
    }


    /**
     * Sets the externalId value for this PersonalInfo.
     * 
     * @param externalId
     */
    public void setExternalId(java.lang.String externalId) {
        this.externalId = externalId;
    }


    /**
     * Gets the externalIdCountryOfIssuance value for this PersonalInfo.
     * 
     * @return externalIdCountryOfIssuance
     */
    public java.lang.String getExternalIdCountryOfIssuance() {
        return externalIdCountryOfIssuance;
    }


    /**
     * Sets the externalIdCountryOfIssuance value for this PersonalInfo.
     * 
     * @param externalIdCountryOfIssuance
     */
    public void setExternalIdCountryOfIssuance(java.lang.String externalIdCountryOfIssuance) {
        this.externalIdCountryOfIssuance = externalIdCountryOfIssuance;
    }


    /**
     * Gets the externalIdType value for this PersonalInfo.
     * 
     * @return externalIdType
     */
    public java.lang.String getExternalIdType() {
        return externalIdType;
    }


    /**
     * Sets the externalIdType value for this PersonalInfo.
     * 
     * @param externalIdType
     */
    public void setExternalIdType(java.lang.String externalIdType) {
        this.externalIdType = externalIdType;
    }


    /**
     * Gets the externalIdStateOfIssuance value for this PersonalInfo.
     * 
     * @return externalIdStateOfIssuance
     */
    public java.lang.String getExternalIdStateOfIssuance() {
        return externalIdStateOfIssuance;
    }


    /**
     * Sets the externalIdStateOfIssuance value for this PersonalInfo.
     * 
     * @param externalIdStateOfIssuance
     */
    public void setExternalIdStateOfIssuance(java.lang.String externalIdStateOfIssuance) {
        this.externalIdStateOfIssuance = externalIdStateOfIssuance;
    }


    /**
     * Gets the externalIdDateOfIssuance value for this PersonalInfo.
     * 
     * @return externalIdDateOfIssuance
     */
    public java.util.Date getExternalIdDateOfIssuance() {
        return externalIdDateOfIssuance;
    }


    /**
     * Sets the externalIdDateOfIssuance value for this PersonalInfo.
     * 
     * @param externalIdDateOfIssuance
     */
    public void setExternalIdDateOfIssuance(java.util.Date externalIdDateOfIssuance) {
        this.externalIdDateOfIssuance = externalIdDateOfIssuance;
    }


    /**
     * Gets the externalIdDateOfExpiration value for this PersonalInfo.
     * 
     * @return externalIdDateOfExpiration
     */
    public java.util.Date getExternalIdDateOfExpiration() {
        return externalIdDateOfExpiration;
    }


    /**
     * Sets the externalIdDateOfExpiration value for this PersonalInfo.
     * 
     * @param externalIdDateOfExpiration
     */
    public void setExternalIdDateOfExpiration(java.util.Date externalIdDateOfExpiration) {
        this.externalIdDateOfExpiration = externalIdDateOfExpiration;
    }


    /**
     * Gets the countryOfBirthCode value for this PersonalInfo.
     * 
     * @return countryOfBirthCode
     */
    public java.lang.String getCountryOfBirthCode() {
        return countryOfBirthCode;
    }


    /**
     * Sets the countryOfBirthCode value for this PersonalInfo.
     * 
     * @param countryOfBirthCode
     */
    public void setCountryOfBirthCode(java.lang.String countryOfBirthCode) {
        this.countryOfBirthCode = countryOfBirthCode;
    }


    /**
     * Gets the passportMRZ value for this PersonalInfo.
     * 
     * @return passportMRZ
     */
    public java.lang.String getPassportMRZ() {
        return passportMRZ;
    }


    /**
     * Sets the passportMRZ value for this PersonalInfo.
     * 
     * @param passportMRZ
     */
    public void setPassportMRZ(java.lang.String passportMRZ) {
        this.passportMRZ = passportMRZ;
    }
    
    /**
     * Gets the secondLastName value for this PersonalInfo.
     * 
     * @return
     */
    public java.lang.String getSecondLastName() {
		return secondLastName;
	}

    /**
     * Sets the secondLastName value for this PersonalInfo.
     * 
     * @param secondLastName
     */
	public void setSecondLastName(java.lang.String secondLastName) {
		this.secondLastName = secondLastName;
	}

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PersonalInfo)) return false;
        PersonalInfo other = (PersonalInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.middleName==null && other.getMiddleName()==null) || 
             (this.middleName!=null &&
              this.middleName.equals(other.getMiddleName()))) &&
            ((this.secondLastName==null && other.getSecondLastName()==null) ||
             (this.secondLastName!=null &&
              this.secondLastName.equals(other.getSecondLastName()))) &&
            ((this.ssn==null && other.getSsn()==null) || 
             (this.ssn!=null &&
              this.ssn.equals(other.getSsn()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth()))) &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.externalId==null && other.getExternalId()==null) || 
             (this.externalId!=null &&
              this.externalId.equals(other.getExternalId()))) &&
            ((this.externalIdCountryOfIssuance==null && other.getExternalIdCountryOfIssuance()==null) || 
             (this.externalIdCountryOfIssuance!=null &&
              this.externalIdCountryOfIssuance.equals(other.getExternalIdCountryOfIssuance()))) &&
            ((this.externalIdType==null && other.getExternalIdType()==null) || 
             (this.externalIdType!=null &&
              this.externalIdType.equals(other.getExternalIdType()))) &&
            ((this.externalIdStateOfIssuance==null && other.getExternalIdStateOfIssuance()==null) || 
             (this.externalIdStateOfIssuance!=null &&
              this.externalIdStateOfIssuance.equals(other.getExternalIdStateOfIssuance()))) &&
            ((this.externalIdDateOfIssuance==null && other.getExternalIdDateOfIssuance()==null) || 
             (this.externalIdDateOfIssuance!=null &&
              this.externalIdDateOfIssuance.equals(other.getExternalIdDateOfIssuance()))) &&
            ((this.externalIdDateOfExpiration==null && other.getExternalIdDateOfExpiration()==null) || 
             (this.externalIdDateOfExpiration!=null &&
              this.externalIdDateOfExpiration.equals(other.getExternalIdDateOfExpiration()))) &&
            ((this.countryOfBirthCode==null && other.getCountryOfBirthCode()==null) || 
             (this.countryOfBirthCode!=null &&
              this.countryOfBirthCode.equals(other.getCountryOfBirthCode()))) &&
            ((this.passportMRZ==null && other.getPassportMRZ()==null) || 
             (this.passportMRZ!=null &&
              this.passportMRZ.equals(other.getPassportMRZ())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getMiddleName() != null) {
            _hashCode += getMiddleName().hashCode();
        }
        if (getSsn() != null) {
            _hashCode += getSsn().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getExternalId() != null) {
            _hashCode += getExternalId().hashCode();
        }
        if (getExternalIdCountryOfIssuance() != null) {
            _hashCode += getExternalIdCountryOfIssuance().hashCode();
        }
        if (getExternalIdType() != null) {
            _hashCode += getExternalIdType().hashCode();
        }
        if (getExternalIdStateOfIssuance() != null) {
            _hashCode += getExternalIdStateOfIssuance().hashCode();
        }
        if (getExternalIdDateOfIssuance() != null) {
            _hashCode += getExternalIdDateOfIssuance().hashCode();
        }
        if (getExternalIdDateOfExpiration() != null) {
            _hashCode += getExternalIdDateOfExpiration().hashCode();
        }
        if (getCountryOfBirthCode() != null) {
            _hashCode += getCountryOfBirthCode().hashCode();
        }
        if (getPassportMRZ() != null) {
            _hashCode += getPassportMRZ().hashCode();
        }
        if (getSecondLastName() != null) {
            _hashCode += getSecondLastName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PersonalInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PersonalInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "firstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "middleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "secondLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ssn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ssn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "dateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdCountryOfIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalIdCountryOfIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalIdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdStateOfIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalIdStateOfIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdDateOfIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalIdDateOfIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalIdDateOfExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "externalIdDateOfExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryOfBirthCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "countryOfBirthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passportMRZ");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "passportMRZ"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
