/**
 * Access.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class Access  implements java.io.Serializable {
    private java.lang.String passwordHash;

    private java.lang.String ipAddress;

    private java.lang.String webServerName;

    private java.lang.String webServerIpAddress;

    private java.lang.Boolean ipAddressBlocked;

    private java.lang.Boolean logonSuccessful;

    public Access() {
    }

    public Access(
           java.lang.String passwordHash,
           java.lang.String ipAddress,
           java.lang.String webServerName,
           java.lang.String webServerIpAddress,
           java.lang.Boolean ipAddressBlocked,
           java.lang.Boolean logonSuccessful) {
           this.passwordHash = passwordHash;
           this.ipAddress = ipAddress;
           this.webServerName = webServerName;
           this.webServerIpAddress = webServerIpAddress;
           this.ipAddressBlocked = ipAddressBlocked;
           this.logonSuccessful = logonSuccessful;
    }


    /**
     * Gets the passwordHash value for this Access.
     * 
     * @return passwordHash
     */
    public java.lang.String getPasswordHash() {
        return passwordHash;
    }


    /**
     * Sets the passwordHash value for this Access.
     * 
     * @param passwordHash
     */
    public void setPasswordHash(java.lang.String passwordHash) {
        this.passwordHash = passwordHash;
    }


    /**
     * Gets the ipAddress value for this Access.
     * 
     * @return ipAddress
     */
    public java.lang.String getIpAddress() {
        return ipAddress;
    }


    /**
     * Sets the ipAddress value for this Access.
     * 
     * @param ipAddress
     */
    public void setIpAddress(java.lang.String ipAddress) {
        this.ipAddress = ipAddress;
    }


    /**
     * Gets the webServerName value for this Access.
     * 
     * @return webServerName
     */
    public java.lang.String getWebServerName() {
        return webServerName;
    }


    /**
     * Sets the webServerName value for this Access.
     * 
     * @param webServerName
     */
    public void setWebServerName(java.lang.String webServerName) {
        this.webServerName = webServerName;
    }


    /**
     * Gets the webServerIpAddress value for this Access.
     * 
     * @return webServerIpAddress
     */
    public java.lang.String getWebServerIpAddress() {
        return webServerIpAddress;
    }


    /**
     * Sets the webServerIpAddress value for this Access.
     * 
     * @param webServerIpAddress
     */
    public void setWebServerIpAddress(java.lang.String webServerIpAddress) {
        this.webServerIpAddress = webServerIpAddress;
    }


    /**
     * Gets the ipAddressBlocked value for this Access.
     * 
     * @return ipAddressBlocked
     */
    public java.lang.Boolean getIpAddressBlocked() {
        return ipAddressBlocked;
    }


    /**
     * Sets the ipAddressBlocked value for this Access.
     * 
     * @param ipAddressBlocked
     */
    public void setIpAddressBlocked(java.lang.Boolean ipAddressBlocked) {
        this.ipAddressBlocked = ipAddressBlocked;
    }


    /**
     * Gets the logonSuccessful value for this Access.
     * 
     * @return logonSuccessful
     */
    public java.lang.Boolean getLogonSuccessful() {
        return logonSuccessful;
    }


    /**
     * Sets the logonSuccessful value for this Access.
     * 
     * @param logonSuccessful
     */
    public void setLogonSuccessful(java.lang.Boolean logonSuccessful) {
        this.logonSuccessful = logonSuccessful;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Access)) return false;
        Access other = (Access) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.passwordHash==null && other.getPasswordHash()==null) || 
             (this.passwordHash!=null &&
              this.passwordHash.equals(other.getPasswordHash()))) &&
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            ((this.webServerName==null && other.getWebServerName()==null) || 
             (this.webServerName!=null &&
              this.webServerName.equals(other.getWebServerName()))) &&
            ((this.webServerIpAddress==null && other.getWebServerIpAddress()==null) || 
             (this.webServerIpAddress!=null &&
              this.webServerIpAddress.equals(other.getWebServerIpAddress()))) &&
            ((this.ipAddressBlocked==null && other.getIpAddressBlocked()==null) || 
             (this.ipAddressBlocked!=null &&
              this.ipAddressBlocked.equals(other.getIpAddressBlocked()))) &&
            ((this.logonSuccessful==null && other.getLogonSuccessful()==null) || 
             (this.logonSuccessful!=null &&
              this.logonSuccessful.equals(other.getLogonSuccessful())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPasswordHash() != null) {
            _hashCode += getPasswordHash().hashCode();
        }
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        if (getWebServerName() != null) {
            _hashCode += getWebServerName().hashCode();
        }
        if (getWebServerIpAddress() != null) {
            _hashCode += getWebServerIpAddress().hashCode();
        }
        if (getIpAddressBlocked() != null) {
            _hashCode += getIpAddressBlocked().hashCode();
        }
        if (getLogonSuccessful() != null) {
            _hashCode += getLogonSuccessful().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Access.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Access"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordHash");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "passwordHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ipAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webServerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "webServerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webServerIpAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "webServerIpAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipAddressBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ipAddressBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logonSuccessful");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "logonSuccessful"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
