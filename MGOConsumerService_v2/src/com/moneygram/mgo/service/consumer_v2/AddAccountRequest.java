/**
 * AddAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

import com.moneygram.common.service.BaseOperationRequest;

public class AddAccountRequest  extends BaseOperationRequest  implements java.io.Serializable {
    private long consumerId;

    private com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount;

    
    
    public AddAccountRequest() {
    }

    public AddAccountRequest(
           com.moneygram.common.service.RequestHeader header,
           long consumerId,
           com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount) {
        super(
            header);
        this.consumerId = consumerId;
        this.consumerFIAccount = consumerFIAccount;
    }


    /**
     * Gets the consumerId value for this AddAccountRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this AddAccountRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the consumerFIAccount value for this AddAccountRequest.
     * 
     * @return consumerFIAccount
     */
    public com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount getConsumerFIAccount() {
        return consumerFIAccount;
    }


    /**
     * Sets the consumerFIAccount value for this AddAccountRequest.
     * 
     * @param consumerFIAccount
     */
    public void setConsumerFIAccount(com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount) {
        this.consumerFIAccount = consumerFIAccount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddAccountRequest)) return false;
        AddAccountRequest other = (AddAccountRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId() &&
            ((this.consumerFIAccount==null && other.getConsumerFIAccount()==null) || 
             (this.consumerFIAccount!=null &&
              this.consumerFIAccount.equals(other.getConsumerFIAccount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        if (getConsumerFIAccount() != null) {
            _hashCode += getConsumerFIAccount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddAccountRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddAccountRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerFIAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerFIAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
