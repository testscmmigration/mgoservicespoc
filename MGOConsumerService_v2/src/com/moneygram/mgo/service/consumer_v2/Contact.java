/**
 * Contact.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class Contact  implements java.io.Serializable {
    private java.lang.String primaryPhone;

    private java.lang.String primaryPhoneType;

    private java.lang.String alternatePhone;

    private java.lang.String alternatePhoneType;

    private java.lang.String email;

    private java.lang.Boolean promoEmail;

    private com.moneygram.mgo.service.consumer_v2.Language prefCommLanguage;

    public Contact() {
    }

    public Contact(
           java.lang.String primaryPhone,
           java.lang.String primaryPhoneType,
           java.lang.String alternatePhone,
           java.lang.String alternatePhoneType,
           java.lang.String email,
           java.lang.Boolean promoEmail,
           com.moneygram.mgo.service.consumer_v2.Language prefCommLanguage) {
           this.primaryPhone = primaryPhone;
           this.primaryPhoneType = primaryPhoneType;
           this.alternatePhone = alternatePhone;
           this.alternatePhoneType = alternatePhoneType;
           this.email = email;
           this.promoEmail = promoEmail;
           this.prefCommLanguage = prefCommLanguage;
    }


    /**
     * Gets the primaryPhone value for this Contact.
     * 
     * @return primaryPhone
     */
    public java.lang.String getPrimaryPhone() {
        return primaryPhone;
    }


    /**
     * Sets the primaryPhone value for this Contact.
     * 
     * @param primaryPhone
     */
    public void setPrimaryPhone(java.lang.String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }


    /**
     * Gets the primaryPhoneType value for this Contact.
     * 
     * @return primaryPhoneType
     */
    public java.lang.String getPrimaryPhoneType() {
        return primaryPhoneType;
    }


    /**
     * Sets the primaryPhoneType value for this Contact.
     * 
     * @param primaryPhoneType
     */
    public void setPrimaryPhoneType(java.lang.String primaryPhoneType) {
        this.primaryPhoneType = primaryPhoneType;
    }


    /**
     * Gets the alternatePhone value for this Contact.
     * 
     * @return alternatePhone
     */
    public java.lang.String getAlternatePhone() {
        return alternatePhone;
    }


    /**
     * Sets the alternatePhone value for this Contact.
     * 
     * @param alternatePhone
     */
    public void setAlternatePhone(java.lang.String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }


    /**
     * Gets the alternatePhoneType value for this Contact.
     * 
     * @return alternatePhoneType
     */
    public java.lang.String getAlternatePhoneType() {
        return alternatePhoneType;
    }


    /**
     * Sets the alternatePhoneType value for this Contact.
     * 
     * @param alternatePhoneType
     */
    public void setAlternatePhoneType(java.lang.String alternatePhoneType) {
        this.alternatePhoneType = alternatePhoneType;
    }


    /**
     * Gets the email value for this Contact.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Contact.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the promoEmail value for this Contact.
     * 
     * @return promoEmail
     */
    public java.lang.Boolean getPromoEmail() {
        return promoEmail;
    }


    /**
     * Sets the promoEmail value for this Contact.
     * 
     * @param promoEmail
     */
    public void setPromoEmail(java.lang.Boolean promoEmail) {
        this.promoEmail = promoEmail;
    }


    /**
     * Gets the prefCommLanguage value for this Contact.
     * 
     * @return prefCommLanguage
     */
    public com.moneygram.mgo.service.consumer_v2.Language getPrefCommLanguage() {
        return prefCommLanguage;
    }


    /**
     * Sets the prefCommLanguage value for this Contact.
     * 
     * @param prefCommLanguage
     */
    public void setPrefCommLanguage(com.moneygram.mgo.service.consumer_v2.Language prefCommLanguage) {
        this.prefCommLanguage = prefCommLanguage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Contact)) return false;
        Contact other = (Contact) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.primaryPhone==null && other.getPrimaryPhone()==null) || 
             (this.primaryPhone!=null &&
              this.primaryPhone.equals(other.getPrimaryPhone()))) &&
            ((this.primaryPhoneType==null && other.getPrimaryPhoneType()==null) || 
             (this.primaryPhoneType!=null &&
              this.primaryPhoneType.equals(other.getPrimaryPhoneType()))) &&
            ((this.alternatePhone==null && other.getAlternatePhone()==null) || 
             (this.alternatePhone!=null &&
              this.alternatePhone.equals(other.getAlternatePhone()))) &&
            ((this.alternatePhoneType==null && other.getAlternatePhoneType()==null) || 
             (this.alternatePhoneType!=null &&
              this.alternatePhoneType.equals(other.getAlternatePhoneType()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.promoEmail==null && other.getPromoEmail()==null) || 
             (this.promoEmail!=null &&
              this.promoEmail.equals(other.getPromoEmail()))) &&
            ((this.prefCommLanguage==null && other.getPrefCommLanguage()==null) || 
             (this.prefCommLanguage!=null &&
              this.prefCommLanguage.equals(other.getPrefCommLanguage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPrimaryPhone() != null) {
            _hashCode += getPrimaryPhone().hashCode();
        }
        if (getPrimaryPhoneType() != null) {
            _hashCode += getPrimaryPhoneType().hashCode();
        }
        if (getAlternatePhone() != null) {
            _hashCode += getAlternatePhone().hashCode();
        }
        if (getAlternatePhoneType() != null) {
            _hashCode += getAlternatePhoneType().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getPromoEmail() != null) {
            _hashCode += getPromoEmail().hashCode();
        }
        if (getPrefCommLanguage() != null) {
            _hashCode += getPrefCommLanguage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Contact.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Contact"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "primaryPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryPhoneType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "primaryPhoneType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternatePhone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "alternatePhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternatePhoneType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "alternatePhoneType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "promoEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prefCommLanguage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "prefCommLanguage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Language"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
