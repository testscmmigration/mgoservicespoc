/**
 * GetGlobalConsumerProfileIdResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class GetGlobalConsumerProfileIdResponse  extends com.moneygram.common.service.BaseOperationResponse  implements java.io.Serializable {
    private long cnsmrProfileId;

    public GetGlobalConsumerProfileIdResponse() {
    }

    public GetGlobalConsumerProfileIdResponse(
    		com.moneygram.mgo.common_v2.Header header,
           long cnsmrProfileId) {
        this.cnsmrProfileId = cnsmrProfileId;
    }


    /**
     * Gets the cnsmrProfileId value for this GetGlobalConsumerProfileIdResponse.
     * 
     * @return cnsmrProfileId
     */
    public long getCnsmrProfileId() {
        return cnsmrProfileId;
    }


    /**
     * Sets the cnsmrProfileId value for this GetGlobalConsumerProfileIdResponse.
     * 
     * @param cnsmrProfileId
     */
    public void setCnsmrProfileId(long cnsmrProfileId) {
        this.cnsmrProfileId = cnsmrProfileId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetGlobalConsumerProfileIdResponse)) return false;
        GetGlobalConsumerProfileIdResponse other = (GetGlobalConsumerProfileIdResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.cnsmrProfileId == other.getCnsmrProfileId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCnsmrProfileId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetGlobalConsumerProfileIdResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetGlobalConsumerProfileIdResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnsmrProfileId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "cnsmrProfileId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
