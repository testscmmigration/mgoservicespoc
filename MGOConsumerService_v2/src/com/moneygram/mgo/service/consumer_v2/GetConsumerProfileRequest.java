/**
 * GetConsumerProfileRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class GetConsumerProfileRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private java.lang.Long consumerId;

    private java.lang.String consumerLoginId;

    /* Optional address id. If specified then this particular address
     * is returned. Otherwise, if address is requested via the response filter
     * then an active address is returned. */
    private java.lang.Long addressId;

    /* If the filter is not provided the Profile
     * 								Part will be returned. */
    private com.moneygram.mgo.service.consumer_v2.ProfilePart[] responseFilter;

    public GetConsumerProfileRequest() {
    }

    public GetConsumerProfileRequest(
           com.moneygram.common.service.RequestHeader header,
           java.lang.Long consumerId,
           java.lang.String consumerLoginId,
           java.lang.Long addressId,
           com.moneygram.mgo.service.consumer_v2.ProfilePart[] responseFilter) {
        super(
            header);
        this.consumerId = consumerId;
        this.consumerLoginId = consumerLoginId;
        this.addressId = addressId;
        this.responseFilter = responseFilter;
    }


    /**
     * Gets the consumerId value for this GetConsumerProfileRequest.
     * 
     * @return consumerId
     */
    public java.lang.Long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this GetConsumerProfileRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(java.lang.Long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the consumerLoginId value for this GetConsumerProfileRequest.
     * 
     * @return consumerLoginId
     */
    public java.lang.String getConsumerLoginId() {
        return consumerLoginId;
    }


    /**
     * Sets the consumerLoginId value for this GetConsumerProfileRequest.
     * 
     * @param consumerLoginId
     */
    public void setConsumerLoginId(java.lang.String consumerLoginId) {
        this.consumerLoginId = consumerLoginId;
    }


    /**
     * Gets the addressId value for this GetConsumerProfileRequest.
     * 
     * @return addressId   * Optional address id. If specified then this particular address
     * is returned. Otherwise, if address is requested via the response filter
     * then an active address is returned.
     */
    public java.lang.Long getAddressId() {
        return addressId;
    }


    /**
     * Sets the addressId value for this GetConsumerProfileRequest.
     * 
     * @param addressId   * Optional address id. If specified then this particular address
     * is returned. Otherwise, if address is requested via the response filter
     * then an active address is returned.
     */
    public void setAddressId(java.lang.Long addressId) {
        this.addressId = addressId;
    }


    /**
     * Gets the responseFilter value for this GetConsumerProfileRequest.
     * 
     * @return responseFilter   * If the filter is not provided the Profile
     * 								Part will be returned.
     */
    public com.moneygram.mgo.service.consumer_v2.ProfilePart[] getResponseFilter() {
        return responseFilter;
    }


    /**
     * Sets the responseFilter value for this GetConsumerProfileRequest.
     * 
     * @param responseFilter   * If the filter is not provided the Profile
     * 								Part will be returned.
     */
    public void setResponseFilter(com.moneygram.mgo.service.consumer_v2.ProfilePart[] responseFilter) {
        this.responseFilter = responseFilter;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsumerProfileRequest)) return false;
        GetConsumerProfileRequest other = (GetConsumerProfileRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.consumerLoginId==null && other.getConsumerLoginId()==null) || 
             (this.consumerLoginId!=null &&
              this.consumerLoginId.equals(other.getConsumerLoginId()))) &&
            ((this.addressId==null && other.getAddressId()==null) || 
             (this.addressId!=null &&
              this.addressId.equals(other.getAddressId()))) &&
            ((this.responseFilter==null && other.getResponseFilter()==null) || 
             (this.responseFilter!=null &&
              java.util.Arrays.equals(this.responseFilter, other.getResponseFilter())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getConsumerLoginId() != null) {
            _hashCode += getConsumerLoginId().hashCode();
        }
        if (getAddressId() != null) {
            _hashCode += getAddressId().hashCode();
        }
        if (getResponseFilter() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponseFilter());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponseFilter(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetConsumerProfileRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerProfileRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerLoginId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerLoginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addressId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "responseFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfilePart"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "profilePart"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
