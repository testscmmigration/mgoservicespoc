/**
 * UpdateConsumerProfileRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class UpdateConsumerProfileRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumer_v2.Consumer consumer;

    private com.moneygram.mgo.service.consumer_v2.Comment comment;

    private com.moneygram.mgo.service.consumer_v2.ProfileEvent profileEvent;

    private com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask[] updateTasks;

    public UpdateConsumerProfileRequest() {
    }

    public UpdateConsumerProfileRequest(
           com.moneygram.common.service.RequestHeader header,
           com.moneygram.mgo.service.consumer_v2.Consumer consumer,
           com.moneygram.mgo.service.consumer_v2.Comment comment,
           com.moneygram.mgo.service.consumer_v2.ProfileEvent profileEvent,
           com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask[] updateTasks) {
        super(
            header);
        this.consumer = consumer;
        this.comment = comment;
        this.profileEvent = profileEvent;
        this.updateTasks = updateTasks;
    }


    /**
     * Gets the consumer value for this UpdateConsumerProfileRequest.
     * 
     * @return consumer
     */
    public com.moneygram.mgo.service.consumer_v2.Consumer getConsumer() {
        return consumer;
    }


    /**
     * Sets the consumer value for this UpdateConsumerProfileRequest.
     * 
     * @param consumer
     */
    public void setConsumer(com.moneygram.mgo.service.consumer_v2.Consumer consumer) {
        this.consumer = consumer;
    }


    /**
     * Gets the comment value for this UpdateConsumerProfileRequest.
     * 
     * @return comment
     */
    public com.moneygram.mgo.service.consumer_v2.Comment getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this UpdateConsumerProfileRequest.
     * 
     * @param comment
     */
    public void setComment(com.moneygram.mgo.service.consumer_v2.Comment comment) {
        this.comment = comment;
    }


    /**
     * Gets the profileEvent value for this UpdateConsumerProfileRequest.
     * 
     * @return profileEvent
     */
    public com.moneygram.mgo.service.consumer_v2.ProfileEvent getProfileEvent() {
        return profileEvent;
    }


    /**
     * Sets the profileEvent value for this UpdateConsumerProfileRequest.
     * 
     * @param profileEvent
     */
    public void setProfileEvent(com.moneygram.mgo.service.consumer_v2.ProfileEvent profileEvent) {
        this.profileEvent = profileEvent;
    }


    /**
     * Gets the updateTasks value for this UpdateConsumerProfileRequest.
     * 
     * @return updateTasks
     */
    public com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask[] getUpdateTasks() {
        return updateTasks;
    }


    /**
     * Sets the updateTasks value for this UpdateConsumerProfileRequest.
     * 
     * @param updateTasks
     */
    public void setUpdateTasks(com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask[] updateTasks) {
        this.updateTasks = updateTasks;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateConsumerProfileRequest)) return false;
        UpdateConsumerProfileRequest other = (UpdateConsumerProfileRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumer==null && other.getConsumer()==null) || 
             (this.consumer!=null &&
              this.consumer.equals(other.getConsumer()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.profileEvent==null && other.getProfileEvent()==null) || 
             (this.profileEvent!=null &&
              this.profileEvent.equals(other.getProfileEvent()))) &&
            ((this.updateTasks==null && other.getUpdateTasks()==null) || 
             (this.updateTasks!=null &&
              java.util.Arrays.equals(this.updateTasks, other.getUpdateTasks())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumer() != null) {
            _hashCode += getConsumer().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getProfileEvent() != null) {
            _hashCode += getProfileEvent().hashCode();
        }
        if (getUpdateTasks() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpdateTasks());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpdateTasks(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateConsumerProfileRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileEvent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "profileEvent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfileEvent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateTasks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateTasks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileTask"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerProfileTask"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
