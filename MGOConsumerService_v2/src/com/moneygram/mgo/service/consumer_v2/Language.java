/**
 * Language.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class Language  implements java.io.Serializable {
    private java.lang.Long languageTagId;

    private java.lang.String languageTagText;

    private java.lang.String languageTagName;

    public Language() {
    }

    public Language(
           java.lang.Long languageTagId,
           java.lang.String languageTagText,
           java.lang.String languageTagName) {
           this.languageTagId = languageTagId;
           this.languageTagText = languageTagText;
           this.languageTagName = languageTagName;
    }


    /**
     * Gets the languageTagId value for this Language.
     * 
     * @return languageTagId
     */
    public java.lang.Long getLanguageTagId() {
        return languageTagId;
    }


    /**
     * Sets the languageTagId value for this Language.
     * 
     * @param languageTagId
     */
    public void setLanguageTagId(java.lang.Long languageTagId) {
        this.languageTagId = languageTagId;
    }


    /**
     * Gets the languageTagText value for this Language.
     * 
     * @return languageTagText
     */
    public java.lang.String getLanguageTagText() {
        return languageTagText;
    }


    /**
     * Sets the languageTagText value for this Language.
     * 
     * @param languageTagText
     */
    public void setLanguageTagText(java.lang.String languageTagText) {
        this.languageTagText = languageTagText;
    }


    /**
     * Gets the languageTagName value for this Language.
     * 
     * @return languageTagName
     */
    public java.lang.String getLanguageTagName() {
        return languageTagName;
    }


    /**
     * Sets the languageTagName value for this Language.
     * 
     * @param languageTagName
     */
    public void setLanguageTagName(java.lang.String languageTagName) {
        this.languageTagName = languageTagName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Language)) return false;
        Language other = (Language) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.languageTagId==null && other.getLanguageTagId()==null) || 
             (this.languageTagId!=null &&
              this.languageTagId.equals(other.getLanguageTagId()))) &&
            ((this.languageTagText==null && other.getLanguageTagText()==null) || 
             (this.languageTagText!=null &&
              this.languageTagText.equals(other.getLanguageTagText()))) &&
            ((this.languageTagName==null && other.getLanguageTagName()==null) || 
             (this.languageTagName!=null &&
              this.languageTagName.equals(other.getLanguageTagName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLanguageTagId() != null) {
            _hashCode += getLanguageTagId().hashCode();
        }
        if (getLanguageTagText() != null) {
            _hashCode += getLanguageTagText().hashCode();
        }
        if (getLanguageTagName() != null) {
            _hashCode += getLanguageTagName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Language.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Language"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageTagId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "languageTagId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageTagText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "languageTagText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageTagName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "languageTagName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
