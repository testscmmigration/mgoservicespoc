/**
 * BankAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class BankAccount  extends com.moneygram.mgo.service.consumer_v2.FIAccount  implements java.io.Serializable {
    private java.lang.String abaNumber;

    private java.lang.String fiName;

    private java.lang.Boolean abaBlocked;

    public BankAccount() {
    }

    public BankAccount(
           java.lang.Long accountId,
           com.moneygram.mgo.service.consumer_v2.FIAccountType accountType,
           java.lang.String accountNumber,
           java.lang.String accountStatus,
           java.lang.String accountSubStatus,
           com.moneygram.mgo.service.consumer_v2.Comment[] comments,
           java.lang.Boolean blocked,
           java.lang.String encryptedAccountNumber,
           java.lang.String abaNumber,
           java.lang.String fiName,
           java.lang.Boolean abaBlocked) {
        super(
            accountId,
            accountType,
            accountNumber,
            accountStatus,
            accountSubStatus,
            comments,
            blocked,
            encryptedAccountNumber);
        this.abaNumber = abaNumber;
        this.fiName = fiName;
        this.abaBlocked = abaBlocked;
    }


    /**
     * Gets the abaNumber value for this BankAccount.
     * 
     * @return abaNumber
     */
    public java.lang.String getAbaNumber() {
        return abaNumber;
    }


    /**
     * Sets the abaNumber value for this BankAccount.
     * 
     * @param abaNumber
     */
    public void setAbaNumber(java.lang.String abaNumber) {
        this.abaNumber = abaNumber;
    }


    /**
     * Gets the fiName value for this BankAccount.
     * 
     * @return fiName
     */
    public java.lang.String getFiName() {
        return fiName;
    }


    /**
     * Sets the fiName value for this BankAccount.
     * 
     * @param fiName
     */
    public void setFiName(java.lang.String fiName) {
        this.fiName = fiName;
    }


    /**
     * Gets the abaBlocked value for this BankAccount.
     * 
     * @return abaBlocked
     */
    public java.lang.Boolean getAbaBlocked() {
        return abaBlocked;
    }


    /**
     * Sets the abaBlocked value for this BankAccount.
     * 
     * @param abaBlocked
     */
    public void setAbaBlocked(java.lang.Boolean abaBlocked) {
        this.abaBlocked = abaBlocked;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BankAccount)) return false;
        BankAccount other = (BankAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.abaNumber==null && other.getAbaNumber()==null) || 
             (this.abaNumber!=null &&
              this.abaNumber.equals(other.getAbaNumber()))) &&
            ((this.fiName==null && other.getFiName()==null) || 
             (this.fiName!=null &&
              this.fiName.equals(other.getFiName()))) &&
            ((this.abaBlocked==null && other.getAbaBlocked()==null) || 
             (this.abaBlocked!=null &&
              this.abaBlocked.equals(other.getAbaBlocked())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAbaNumber() != null) {
            _hashCode += getAbaNumber().hashCode();
        }
        if (getFiName() != null) {
            _hashCode += getFiName().hashCode();
        }
        if (getAbaBlocked() != null) {
            _hashCode += getAbaBlocked().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BankAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "BankAccount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abaNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "abaNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fiName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "fiName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abaBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "abaBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
