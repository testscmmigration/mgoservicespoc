/**
 * FindConsumersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class FindConsumersResponse  extends com.moneygram.common.service.BaseOperationResponse  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumer_v2.Consumer[] consumers;

    public FindConsumersResponse() {
    }

   

    /**
     * Gets the consumers value for this FindConsumersResponse.
     * 
     * @return consumers
     */
    public com.moneygram.mgo.service.consumer_v2.Consumer[] getConsumers() {
        return consumers;
    }


    /**
     * Sets the consumers value for this FindConsumersResponse.
     * 
     * @param consumers
     */
    public void setConsumers(com.moneygram.mgo.service.consumer_v2.Consumer[] consumers) {
        this.consumers = consumers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindConsumersResponse)) return false;
        FindConsumersResponse other = (FindConsumersResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumers==null && other.getConsumers()==null) || 
             (this.consumers!=null &&
              java.util.Arrays.equals(this.consumers, other.getConsumers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsumers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsumers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindConsumersResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumer"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
