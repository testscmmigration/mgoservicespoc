/**
 * SaveLogonTryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class SaveLogonTryRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private java.lang.String loginId;

    private com.moneygram.mgo.service.consumer_v2.Access access;

    private java.lang.String sourceSite;

    public SaveLogonTryRequest() {
    }

    public SaveLogonTryRequest(
           com.moneygram.common.service.RequestHeader header,
           java.lang.String loginId,
           com.moneygram.mgo.service.consumer_v2.Access access,
           java.lang.String sourceSite) {
        super(
            header);
        this.loginId = loginId;
        this.access = access;
        this.sourceSite = sourceSite;
    }


    /**
     * Gets the loginId value for this SaveLogonTryRequest.
     * 
     * @return loginId
     */
    public java.lang.String getLoginId() {
        return loginId;
    }


    /**
     * Sets the loginId value for this SaveLogonTryRequest.
     * 
     * @param loginId
     */
    public void setLoginId(java.lang.String loginId) {
        this.loginId = loginId;
    }


    /**
     * Gets the access value for this SaveLogonTryRequest.
     * 
     * @return access
     */
    public com.moneygram.mgo.service.consumer_v2.Access getAccess() {
        return access;
    }


    /**
     * Sets the access value for this SaveLogonTryRequest.
     * 
     * @param access
     */
    public void setAccess(com.moneygram.mgo.service.consumer_v2.Access access) {
        this.access = access;
    }


    /**
     * Gets the sourceSite value for this SaveLogonTryRequest.
     * 
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this SaveLogonTryRequest.
     * 
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveLogonTryRequest)) return false;
        SaveLogonTryRequest other = (SaveLogonTryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.loginId==null && other.getLoginId()==null) || 
             (this.loginId!=null &&
              this.loginId.equals(other.getLoginId()))) &&
            ((this.access==null && other.getAccess()==null) || 
             (this.access!=null &&
              this.access.equals(other.getAccess()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) || 
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getLoginId() != null) {
            _hashCode += getLoginId().hashCode();
        }
        if (getAccess() != null) {
            _hashCode += getAccess().hashCode();
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaveLogonTryRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveLogonTryRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "loginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("access");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "access"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Access"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "sourceSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
