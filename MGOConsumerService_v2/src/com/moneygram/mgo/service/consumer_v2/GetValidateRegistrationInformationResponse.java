/**
 * GetValidateRegistrationInformationResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class GetValidateRegistrationInformationResponse  extends com.moneygram.common.service.BaseOperationResponse  implements java.io.Serializable {
    private long custId;

    private long authnId;

    private java.util.Calendar authnDate;

    private java.lang.String scoreNbr;

    private java.lang.String deciBandText;

    private java.lang.String remarksText;

    private java.lang.String chkTypeCode;

    private java.lang.String chkTypeDesc;

    private java.lang.String rsltCode;

    private java.lang.String rsltDesc;

    private java.lang.String rsltSvrtyText;

    public GetValidateRegistrationInformationResponse() {
    }




    /**
     * Gets the custId value for this GetValidateRegistrationInformationResponse.
     * 
     * @return custId
     */
    public long getCustId() {
        return custId;
    }


    /**
     * Sets the custId value for this GetValidateRegistrationInformationResponse.
     * 
     * @param custId
     */
    public void setCustId(long custId) {
        this.custId = custId;
    }


    /**
     * Gets the authnId value for this GetValidateRegistrationInformationResponse.
     * 
     * @return authnId
     */
    public long getAuthnId() {
        return authnId;
    }


    /**
     * Sets the authnId value for this GetValidateRegistrationInformationResponse.
     * 
     * @param authnId
     */
    public void setAuthnId(long authnId) {
        this.authnId = authnId;
    }


    /**
     * Gets the authnDate value for this GetValidateRegistrationInformationResponse.
     * 
     * @return authnDate
     */
    public java.util.Calendar getAuthnDate() {
        return authnDate;
    }


    /**
     * Sets the authnDate value for this GetValidateRegistrationInformationResponse.
     * 
     * @param authnDate
     */
    public void setAuthnDate(java.util.Calendar authnDate) {
        this.authnDate = authnDate;
    }


    /**
     * Gets the scoreNbr value for this GetValidateRegistrationInformationResponse.
     * 
     * @return scoreNbr
     */
    public java.lang.String getScoreNbr() {
        return scoreNbr;
    }


    /**
     * Sets the scoreNbr value for this GetValidateRegistrationInformationResponse.
     * 
     * @param scoreNbr
     */
    public void setScoreNbr(java.lang.String scoreNbr) {
        this.scoreNbr = scoreNbr;
    }


    /**
     * Gets the deciBandText value for this GetValidateRegistrationInformationResponse.
     * 
     * @return deciBandText
     */
    public java.lang.String getDeciBandText() {
        return deciBandText;
    }


    /**
     * Sets the deciBandText value for this GetValidateRegistrationInformationResponse.
     * 
     * @param deciBandText
     */
    public void setDeciBandText(java.lang.String deciBandText) {
        this.deciBandText = deciBandText;
    }


    /**
     * Gets the remarksText value for this GetValidateRegistrationInformationResponse.
     * 
     * @return remarksText
     */
    public java.lang.String getRemarksText() {
        return remarksText;
    }


    /**
     * Sets the remarksText value for this GetValidateRegistrationInformationResponse.
     * 
     * @param remarksText
     */
    public void setRemarksText(java.lang.String remarksText) {
        this.remarksText = remarksText;
    }


    /**
     * Gets the chkTypeCode value for this GetValidateRegistrationInformationResponse.
     * 
     * @return chkTypeCode
     */
    public java.lang.String getChkTypeCode() {
        return chkTypeCode;
    }


    /**
     * Sets the chkTypeCode value for this GetValidateRegistrationInformationResponse.
     * 
     * @param chkTypeCode
     */
    public void setChkTypeCode(java.lang.String chkTypeCode) {
        this.chkTypeCode = chkTypeCode;
    }


    /**
     * Gets the chkTypeDesc value for this GetValidateRegistrationInformationResponse.
     * 
     * @return chkTypeDesc
     */
    public java.lang.String getChkTypeDesc() {
        return chkTypeDesc;
    }


    /**
     * Sets the chkTypeDesc value for this GetValidateRegistrationInformationResponse.
     * 
     * @param chkTypeDesc
     */
    public void setChkTypeDesc(java.lang.String chkTypeDesc) {
        this.chkTypeDesc = chkTypeDesc;
    }


    /**
     * Gets the rsltCode value for this GetValidateRegistrationInformationResponse.
     * 
     * @return rsltCode
     */
    public java.lang.String getRsltCode() {
        return rsltCode;
    }


    /**
     * Sets the rsltCode value for this GetValidateRegistrationInformationResponse.
     * 
     * @param rsltCode
     */
    public void setRsltCode(java.lang.String rsltCode) {
        this.rsltCode = rsltCode;
    }


    /**
     * Gets the rsltDesc value for this GetValidateRegistrationInformationResponse.
     * 
     * @return rsltDesc
     */
    public java.lang.String getRsltDesc() {
        return rsltDesc;
    }


    /**
     * Sets the rsltDesc value for this GetValidateRegistrationInformationResponse.
     * 
     * @param rsltDesc
     */
    public void setRsltDesc(java.lang.String rsltDesc) {
        this.rsltDesc = rsltDesc;
    }


    /**
     * Gets the rsltSvrtyText value for this GetValidateRegistrationInformationResponse.
     * 
     * @return rsltSvrtyText
     */
    public java.lang.String getRsltSvrtyText() {
        return rsltSvrtyText;
    }


    /**
     * Sets the rsltSvrtyText value for this GetValidateRegistrationInformationResponse.
     * 
     * @param rsltSvrtyText
     */
    public void setRsltSvrtyText(java.lang.String rsltSvrtyText) {
        this.rsltSvrtyText = rsltSvrtyText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetValidateRegistrationInformationResponse)) return false;
        GetValidateRegistrationInformationResponse other = (GetValidateRegistrationInformationResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.custId == other.getCustId() &&
            this.authnId == other.getAuthnId() &&
            ((this.authnDate==null && other.getAuthnDate()==null) || 
             (this.authnDate!=null &&
              this.authnDate.equals(other.getAuthnDate()))) &&
            ((this.scoreNbr==null && other.getScoreNbr()==null) || 
             (this.scoreNbr!=null &&
              this.scoreNbr.equals(other.getScoreNbr()))) &&
            ((this.deciBandText==null && other.getDeciBandText()==null) || 
             (this.deciBandText!=null &&
              this.deciBandText.equals(other.getDeciBandText()))) &&
            ((this.remarksText==null && other.getRemarksText()==null) || 
             (this.remarksText!=null &&
              this.remarksText.equals(other.getRemarksText()))) &&
            ((this.chkTypeCode==null && other.getChkTypeCode()==null) || 
             (this.chkTypeCode!=null &&
              this.chkTypeCode.equals(other.getChkTypeCode()))) &&
            ((this.chkTypeDesc==null && other.getChkTypeDesc()==null) || 
             (this.chkTypeDesc!=null &&
              this.chkTypeDesc.equals(other.getChkTypeDesc()))) &&
            ((this.rsltCode==null && other.getRsltCode()==null) || 
             (this.rsltCode!=null &&
              this.rsltCode.equals(other.getRsltCode()))) &&
            ((this.rsltDesc==null && other.getRsltDesc()==null) || 
             (this.rsltDesc!=null &&
              this.rsltDesc.equals(other.getRsltDesc()))) &&
            ((this.rsltSvrtyText==null && other.getRsltSvrtyText()==null) || 
             (this.rsltSvrtyText!=null &&
              this.rsltSvrtyText.equals(other.getRsltSvrtyText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCustId()).hashCode();
        _hashCode += new Long(getAuthnId()).hashCode();
        if (getAuthnDate() != null) {
            _hashCode += getAuthnDate().hashCode();
        }
        if (getScoreNbr() != null) {
            _hashCode += getScoreNbr().hashCode();
        }
        if (getDeciBandText() != null) {
            _hashCode += getDeciBandText().hashCode();
        }
        if (getRemarksText() != null) {
            _hashCode += getRemarksText().hashCode();
        }
        if (getChkTypeCode() != null) {
            _hashCode += getChkTypeCode().hashCode();
        }
        if (getChkTypeDesc() != null) {
            _hashCode += getChkTypeDesc().hashCode();
        }
        if (getRsltCode() != null) {
            _hashCode += getRsltCode().hashCode();
        }
        if (getRsltDesc() != null) {
            _hashCode += getRsltDesc().hashCode();
        }
        if (getRsltSvrtyText() != null) {
            _hashCode += getRsltSvrtyText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetValidateRegistrationInformationResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetValidateRegistrationInformationResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "custId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "authnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authnDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "authnDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "scoreNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deciBandText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "deciBandText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remarksText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "remarksText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chkTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "chkTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chkTypeDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "chkTypeDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "rsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rsltDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "rsltDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rsltSvrtyText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "rsltSvrtyText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	@Override
	public String toString() {
		return "GetValidateRegistrationInformationResponse [custId=" + custId
				+ ", authnId=" + authnId + ", authnDate=" + authnDate
				+ ", scoreNbr=" + scoreNbr + ", deciBandText=" + deciBandText
				+ ", remarksText=" + remarksText + ", chkTypeCode="
				+ chkTypeCode + ", chkTypeDesc=" + chkTypeDesc + ", rsltCode="
				+ rsltCode + ", rsltDesc=" + rsltDesc + ", rsltSvrtyText="
				+ rsltSvrtyText + "]";
	}

}
