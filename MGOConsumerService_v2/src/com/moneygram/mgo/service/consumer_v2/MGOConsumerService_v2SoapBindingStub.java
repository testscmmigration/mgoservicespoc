/**
 * MGOConsumerService_v2SoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class MGOConsumerService_v2SoapBindingStub extends org.apache.axis.client.Stub implements com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2 {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[22];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerProfileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerProfileRequest"), com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerProfileResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("update");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerProfileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileRequest"), com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("create");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "createConsumerProfileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreateConsumerProfileRequest"), com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreateConsumerProfileResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "createConsumerProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateAccountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountRequest"), com.moneygram.mgo.service.consumer_v2.UpdateAccountRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addAccountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddAccountRequest"), com.moneygram.mgo.service.consumer_v2.AddAccountRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddAccountResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.AddAccountResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addAccountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBlockedStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerBlockedStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerBlockedStatusRequest"), com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerBlockedStatusResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerBlockedStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("findConsumers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "findConsumersRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersRequest"), com.moneygram.mgo.service.consumer_v2.FindConsumersRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.FindConsumersResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "findConsumersResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncompleteProfiles");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getIncompleteProfilesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteProfilesRequest"), com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteProfilesResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getIncompleteProfilesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getProfileEvents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getProfileEventsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetProfileEventsRequest"), com.moneygram.mgo.service.consumer_v2.GetProfileEventsRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetProfileEventsResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getProfileEventsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("saveProfileEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveProfileEventRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveProfileEventRequest"), com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveProfileEventResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveProfileEventResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("saveLogonTry");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveLogonTryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveLogonTryRequest"), com.moneygram.mgo.service.consumer_v2.SaveLogonTryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveLogonTryResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveLogonTryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOccupationsList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getOccupationsListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetOccupationsListRequest"), com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetOccupationsListResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getOccupationsListResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addImage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addImageRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddImageRequest"), com.moneygram.mgo.service.consumer_v2.AddImageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddImageResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.AddImageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addImageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getConsumerIDImage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerIDImageRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDImageRequest"), com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDImageResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerIDImageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateConsumerIDImage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerIDImageRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageRequest"), com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerIDImageResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getConsumerIDApprovalStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerIDApprovalStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDApprovalStatusRequest"), com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDApprovalStatusResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getConsumerIDApprovalStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateConsumerIDImageStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerIDImageStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageStatusRequest"), com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageStatusResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerIDImageStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getValidateRegistrationInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getValidateRegistrationInformationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetValidateRegistrationInformationRequest"), com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetValidateRegistrationInformationResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getValidateRegistrationInformationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("saveValidateRegistrationInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveValidateRegistrationInformationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveValidateRegistrationInformationRequest"), com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveValidateRegistrationInformationResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "saveValidateRegistrationInformationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncompleteFirstTxProfiles");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getIncompleteFirstTxProfilesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteFirstTxProfilesRequest"), com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteFirstTxProfilesResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getIncompleteFirstTxProfilesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCCLowAuthSequencerValue");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getCCLowAuthSequencerValueRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetCCLowAuthSequencerValueRequest"), com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetCCLowAuthSequencerValueResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getCCLowAuthSequencerValueResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getGlobalConsumerProfileId");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getGlobalConsumerProfileIdRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetGlobalConsumerProfileIdRequest"), com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetGlobalConsumerProfileIdResponse"));
        oper.setReturnClass(com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "getGlobalConsumerProfileIdResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerServiceException"),
                      "com.moneygram.mgo.common_v1.ServiceException",
                      new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException"), 
                      true
                     ));
        _operations[21] = oper;

    }

    public MGOConsumerService_v2SoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public MGOConsumerService_v2SoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public MGOConsumerService_v2SoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.BaseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "BaseResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.BaseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ClientHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ClientHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorCategoryCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ErrorCategoryCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ErrorHandlingCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ErrorHandlingCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Errors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceException[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "Header");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.Header.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "InvocationMethodCode");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.InvocationMethodCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ProcessingInstruction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ProcessingInstruction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RelatedError.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedErrors");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RelatedError[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RelatedError");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "error");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "RoutingContextHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.RoutingContextHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "SecurityHeader");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.SecurityHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceClient");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceClient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/common_v1", "ServiceException");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.common_v2.ServiceException.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ABANumber");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Access");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Access.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddAccountRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.AddAccountRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddAccountResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.AddAccountResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddImageRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.AddImageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddImageResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.AddImageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "AddressLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "BankAccount");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.BankAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "City");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comment");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Comment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comments");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Comment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comment");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CommentText");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumer");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Consumer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccounts");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerId");
            cachedSerQNames.add(qName);
            cls = long.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerProfileResponseFilter");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ProfilePart[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfilePart");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "profilePart");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumers");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Consumer[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumer");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Contact");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Contact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Country");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreateConsumerProfileRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreateConsumerProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreditCard");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.CreditCard.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "EmailAddress");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FIAccount");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.FIAccount.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FIAccountType");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.FIAccountType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FIName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.FindConsumersRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.FindConsumersResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FirstName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetCCLowAuthSequencerValueRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetCCLowAuthSequencerValueResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerBlockedStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerBlockedStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDApprovalStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDApprovalStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDImageRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerIDImageResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerProfileRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetConsumerProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetGlobalConsumerProfileIdRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetGlobalConsumerProfileIdResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteFirstTxProfilesRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteFirstTxProfilesResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteProfilesRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetIncompleteProfilesResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetOccupationsListRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetOccupationsListResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetProfileEventsRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetProfileEventsRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetProfileEventsResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetValidateRegistrationInformationRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetValidateRegistrationInformationResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Language");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Language.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "LastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "LoginId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "LoyaltyInfo");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.LoyaltyInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "LoyaltyMemberId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "MGOAttributes");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.MGOAttributes.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "MiddleInitial");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "MiddleName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Occupation");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Occupation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Occupations");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.Occupation[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Occupation");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PAN");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PersonalInfo");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.PersonalInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Phone");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PhoneType");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PremierTypeCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfileEvent");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ProfileEvent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfilePart");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ProfilePart.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ReasonCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SavedAgent");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SavedAgent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SavedAgents");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SavedAgent[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SavedAgent");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveLogonTryRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveLogonTryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveLogonTryResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveProfileEventRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveProfileEventResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveValidateRegistrationInformationRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveValidateRegistrationInformationResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SecondLastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ServiceAction");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ServiceAction.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SourceSite");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SSN");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "State");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "StatusCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "TransactionPreferences");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.TransactionPreferences.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateAccountRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountTask");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateAccountTask.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountTasks");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateAccountTask[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountTask");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateAccountTask");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileRequest");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileTask");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileTasks");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileTask");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateConsumerProfileTask");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ValidateRegistrationSaveInfoRsls");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ValidateRsls");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ValidateRegistrationSaveInfoRsls");
            qName2 = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Zip");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "AddressLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "BuildingName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "City");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Country");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "CountryName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "County");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "DeliveryInstructionLine");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "FirstName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LastName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "LoyaltyMemberId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleInitial");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "MiddleName");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerAddress");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ReceiverConsumerAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ReceiverConsumerName");
            cachedSerQNames.add(qName);
            cls = com.moneygram.mgo.shared_v2.ReceiverConsumerName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "State");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "StateCode");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "UserId");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

            qName = new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "Zip");
            cachedSerQNames.add(qName);
            cls = java.lang.String.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(org.apache.axis.encoding.ser.BaseSerializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleSerializerFactory.class, cls, qName));
            cachedDeserFactories.add(org.apache.axis.encoding.ser.BaseDeserializerFactory.createFactory(org.apache.axis.encoding.ser.SimpleDeserializerFactory.class, cls, qName));

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse get(com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest getConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsumerProfileRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse update(com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest updateConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "update"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateConsumerProfileRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse create(com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest createConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "create"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createConsumerProfileRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse updateAccount(com.moneygram.mgo.service.consumer_v2.UpdateAccountRequest updateAccountRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.AddAccountResponse addAccount(com.moneygram.mgo.service.consumer_v2.AddAccountRequest addAccountRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {addAccountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.AddAccountResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.AddAccountResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.AddAccountResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse getBlockedStatus(com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusRequest getConsumerBlockedStatusRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getBlockedStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsumerBlockedStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.FindConsumersResponse findConsumers(com.moneygram.mgo.service.consumer_v2.FindConsumersRequest findConsumersRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "findConsumers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {findConsumersRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.FindConsumersResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.FindConsumersResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.FindConsumersResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse getIncompleteProfiles(com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest getIncompleteProfilesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getIncompleteProfiles"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getIncompleteProfilesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse getProfileEvents(com.moneygram.mgo.service.consumer_v2.GetProfileEventsRequest getProfileEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getProfileEvents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getProfileEventsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse saveProfileEvent(com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest saveProfileEventRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "saveProfileEvent"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {saveProfileEventRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse saveLogonTry(com.moneygram.mgo.service.consumer_v2.SaveLogonTryRequest saveLogonTryRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "saveLogonTry"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {saveLogonTryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse getOccupationsList(com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest getOccupationsListRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getOccupationsList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getOccupationsListRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.AddImageResponse addImage(com.moneygram.mgo.service.consumer_v2.AddImageRequest addImageRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addImage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {addImageRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.AddImageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.AddImageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.AddImageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse getConsumerIDImage(com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest getConsumerIDImageRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getConsumerIDImage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsumerIDImageRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse updateConsumerIDImage(com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageRequest updateConsumerIDImageRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateConsumerIDImage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateConsumerIDImageRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatus(com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest getConsumerIDApprovalStatusRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getConsumerIDApprovalStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsumerIDApprovalStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse updateConsumerIDImageStatus(com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusRequest updateConsumerIDImageStatusRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateConsumerIDImageStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateConsumerIDImageStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse getValidateRegistrationInformation(com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationRequest getValidateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getValidateRegistrationInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getValidateRegistrationInformationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse saveValidateRegistrationInformation(com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationRequest saveValidateRegistrationInformationRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "saveValidateRegistrationInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {saveValidateRegistrationInformationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse getIncompleteFirstTxProfiles(com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesRequest getIncompleteFirstTxProfilesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getIncompleteFirstTxProfiles"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getIncompleteFirstTxProfilesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetIncompleteFirstTxProfilesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse getCCLowAuthSequencerValue(com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueRequest getCCLowAuthSequencerValueRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getCCLowAuthSequencerValue"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getCCLowAuthSequencerValueRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse getGlobalConsumerProfileId(com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdRequest getGlobalConsumerProfileIdRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getGlobalConsumerProfileId"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getGlobalConsumerProfileIdRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.moneygram.mgo.common_v2.ServiceException) {
              throw (com.moneygram.mgo.common_v2.ServiceException) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
