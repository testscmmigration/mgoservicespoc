/**
 * MGOConsumerService_v2SoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class MGOConsumerService_v2SoapBindingImpl implements com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2{
    public com.moneygram.mgo.service.consumer_v2.GetConsumerProfileResponse get(com.moneygram.mgo.service.consumer_v2.GetConsumerProfileRequest getConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileResponse update(com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest updateConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse create(com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest createConsumerProfileRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.UpdateAccountResponse updateAccount(com.moneygram.mgo.service.consumer_v2.UpdateAccountRequest updateAccountRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.AddAccountResponse addAccount(com.moneygram.mgo.service.consumer_v2.AddAccountRequest addAccountRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusResponse getBlockedStatus(com.moneygram.mgo.service.consumer_v2.GetConsumerBlockedStatusRequest getConsumerBlockedStatusRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.FindConsumersResponse findConsumers(com.moneygram.mgo.service.consumer_v2.FindConsumersRequest findConsumersRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse getIncompleteProfiles(com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest getIncompleteProfilesRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.GetProfileEventsResponse getProfileEvents(com.moneygram.mgo.service.consumer_v2.GetProfileEventsRequest getProfileEventsRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse saveProfileEvent(com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest saveProfileEventRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.SaveLogonTryResponse saveLogonTry(com.moneygram.mgo.service.consumer_v2.SaveLogonTryRequest saveLogonTryRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

    public com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse getOccupationsList(com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest getOccupationsListRequest) throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException {
        return null;
    }

	public GetGlobalConsumerProfileIdResponse getGlobalConsumerProfileId(GetGlobalConsumerProfileIdRequest getGlobalConsumerProfileIdRequest)throws java.rmi.RemoteException, com.moneygram.mgo.common_v2.ServiceException  {
		return null;
	}
}
