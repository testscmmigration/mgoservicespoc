package com.moneygram.mgo.service.consumer_v2.util;

import java.util.HashMap;
import java.util.Map;

public abstract class PartnerSiteIdentifier {
	public static final String MGO = "MGO";
	public static final String WAP = "WAP";
	public static final String MGOUK = "MGOUK";
	public static final String MGODE = "MGODE";

	public static final class Country {
		public static final String US = "USA";
		public static final String GB = "GBR";
		public static final String DE = "DEU";
	}

	public static final Map<String, String> COUNTRIES_BY_PARTNER_SITE = new HashMap<String, String>(2);

	static {
		COUNTRIES_BY_PARTNER_SITE.put(PartnerSiteIdentifier.MGO, Country.US);
		COUNTRIES_BY_PARTNER_SITE.put(PartnerSiteIdentifier.WAP, Country.US);
		COUNTRIES_BY_PARTNER_SITE.put(PartnerSiteIdentifier.MGOUK, Country.GB);
		COUNTRIES_BY_PARTNER_SITE.put(PartnerSiteIdentifier.MGODE, Country.DE);
	}
}
