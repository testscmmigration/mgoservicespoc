/**
 * ConsumerFIAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class ConsumerFIAccount  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumer_v2.CreditCard creditCard;

    private com.moneygram.mgo.service.consumer_v2.BankAccount bankAccount;

    public ConsumerFIAccount() {
    }

    public ConsumerFIAccount(
           com.moneygram.mgo.service.consumer_v2.CreditCard creditCard,
           com.moneygram.mgo.service.consumer_v2.BankAccount bankAccount) {
           this.creditCard = creditCard;
           this.bankAccount = bankAccount;
    }


    /**
     * Gets the creditCard value for this ConsumerFIAccount.
     * 
     * @return creditCard
     */
    public com.moneygram.mgo.service.consumer_v2.CreditCard getCreditCard() {
        return creditCard;
    }


    /**
     * Sets the creditCard value for this ConsumerFIAccount.
     * 
     * @param creditCard
     */
    public void setCreditCard(com.moneygram.mgo.service.consumer_v2.CreditCard creditCard) {
        this.creditCard = creditCard;
    }


    /**
     * Gets the bankAccount value for this ConsumerFIAccount.
     * 
     * @return bankAccount
     */
    public com.moneygram.mgo.service.consumer_v2.BankAccount getBankAccount() {
        return bankAccount;
    }


    /**
     * Sets the bankAccount value for this ConsumerFIAccount.
     * 
     * @param bankAccount
     */
    public void setBankAccount(com.moneygram.mgo.service.consumer_v2.BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsumerFIAccount)) return false;
        ConsumerFIAccount other = (ConsumerFIAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.creditCard==null && other.getCreditCard()==null) || 
             (this.creditCard!=null &&
              this.creditCard.equals(other.getCreditCard()))) &&
            ((this.bankAccount==null && other.getBankAccount()==null) || 
             (this.bankAccount!=null &&
              this.bankAccount.equals(other.getBankAccount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreditCard() != null) {
            _hashCode += getCreditCard().hashCode();
        }
        if (getBankAccount() != null) {
            _hashCode += getBankAccount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsumerFIAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "creditCard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreditCard"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "bankAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "BankAccount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
