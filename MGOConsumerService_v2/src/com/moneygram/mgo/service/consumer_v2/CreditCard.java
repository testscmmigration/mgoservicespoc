/**
 * CreditCard.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class CreditCard  extends com.moneygram.mgo.service.consumer_v2.FIAccount  implements java.io.Serializable {
    private java.lang.Integer expMonth;

    private java.lang.Integer expYear;

    private java.lang.Boolean binBlocked;

    private java.lang.String cardType;

    public CreditCard() {
    }

    public CreditCard(
           java.lang.Long accountId,
           com.moneygram.mgo.service.consumer_v2.FIAccountType accountType,
           java.lang.String accountNumber,
           java.lang.String accountStatus,
           java.lang.String accountSubStatus,
           com.moneygram.mgo.service.consumer_v2.Comment[] comments,
           java.lang.Boolean blocked,
           java.lang.String encryptedAccountNumber,
           java.lang.Integer expMonth,
           java.lang.Integer expYear,
           java.lang.Boolean binBlocked,
           java.lang.String cardType) {
        super(
            accountId,
            accountType,
            accountNumber,
            accountStatus,
            accountSubStatus,
            comments,
            blocked,
            encryptedAccountNumber);
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.binBlocked = binBlocked;
        this.cardType = cardType;
    }


    /**
     * Gets the expMonth value for this CreditCard.
     * 
     * @return expMonth
     */
    public java.lang.Integer getExpMonth() {
        return expMonth;
    }


    /**
     * Sets the expMonth value for this CreditCard.
     * 
     * @param expMonth
     */
    public void setExpMonth(java.lang.Integer expMonth) {
        this.expMonth = expMonth;
    }


    /**
     * Gets the expYear value for this CreditCard.
     * 
     * @return expYear
     */
    public java.lang.Integer getExpYear() {
        return expYear;
    }


    /**
     * Sets the expYear value for this CreditCard.
     * 
     * @param expYear
     */
    public void setExpYear(java.lang.Integer expYear) {
        this.expYear = expYear;
    }


    /**
     * Gets the binBlocked value for this CreditCard.
     * 
     * @return binBlocked
     */
    public java.lang.Boolean getBinBlocked() {
        return binBlocked;
    }


    /**
     * Sets the binBlocked value for this CreditCard.
     * 
     * @param binBlocked
     */
    public void setBinBlocked(java.lang.Boolean binBlocked) {
        this.binBlocked = binBlocked;
    }


    /**
     * Gets the cardType value for this CreditCard.
     * 
     * @return cardType
     */
    public java.lang.String getCardType() {
        return cardType;
    }


    /**
     * Sets the cardType value for this CreditCard.
     * 
     * @param cardType
     */
    public void setCardType(java.lang.String cardType) {
        this.cardType = cardType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditCard)) return false;
        CreditCard other = (CreditCard) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.expMonth==null && other.getExpMonth()==null) || 
             (this.expMonth!=null &&
              this.expMonth.equals(other.getExpMonth()))) &&
            ((this.expYear==null && other.getExpYear()==null) || 
             (this.expYear!=null &&
              this.expYear.equals(other.getExpYear()))) &&
            ((this.binBlocked==null && other.getBinBlocked()==null) || 
             (this.binBlocked!=null &&
              this.binBlocked.equals(other.getBinBlocked()))) &&
            ((this.cardType==null && other.getCardType()==null) || 
             (this.cardType!=null &&
              this.cardType.equals(other.getCardType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getExpMonth() != null) {
            _hashCode += getExpMonth().hashCode();
        }
        if (getExpYear() != null) {
            _hashCode += getExpYear().hashCode();
        }
        if (getBinBlocked() != null) {
            _hashCode += getBinBlocked().hashCode();
        }
        if (getCardType() != null) {
            _hashCode += getCardType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditCard.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "CreditCard"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expMonth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "expMonth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expYear");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "expYear"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "binBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "cardType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
