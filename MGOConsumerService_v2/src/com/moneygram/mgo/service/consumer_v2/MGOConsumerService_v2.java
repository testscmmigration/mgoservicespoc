/**
 * MGOConsumerService_v2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public interface MGOConsumerService_v2 extends javax.xml.rpc.Service {
    public java.lang.String getMGOConsumerService_v2Address();

    public com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2 getMGOConsumerService_v2() throws javax.xml.rpc.ServiceException;

    public com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2 getMGOConsumerService_v2(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
