/**
 * Consumer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class Consumer  implements java.io.Serializable {
    private java.lang.Long consumerId;

    private java.lang.String loginId;

    private com.moneygram.mgo.service.consumer_v2.PersonalInfo personal;

    private com.moneygram.mgo.service.consumer_v2.Contact contact;

    private com.moneygram.mgo.service.consumer_v2.LoyaltyInfo loyalty;

    private com.moneygram.mgo.service.consumer_v2.TransactionPreferences transactionPreferences;

    private com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount[] accounts;

    private com.moneygram.mgo.service.consumer_v2.MGOAttributes internal;

    private com.moneygram.mgo.service.consumer_v2.Comment[] comments;

    private com.moneygram.mgo.service.consumer_v2.Access access;

    private com.moneygram.mgo.shared.ConsumerAddress address;

    private com.moneygram.mgo.service.consumer_v2.ProfileEvent[] profileEvents;

    private java.lang.String sourceSite;

    private com.moneygram.mgo.service.consumer_v2.Occupation occupation;

    private java.lang.String test;
    
    //vl58 -- Release 36, new field returned for consumer "cnsmr_prfl_id"
    private java.lang.Long cnsmrProfileId;

    public Consumer() {
    }

    public Consumer(
           java.lang.Long consumerId,
           java.lang.String loginId,
           com.moneygram.mgo.service.consumer_v2.PersonalInfo personal,
           com.moneygram.mgo.service.consumer_v2.Contact contact,
           com.moneygram.mgo.service.consumer_v2.LoyaltyInfo loyalty,
           com.moneygram.mgo.service.consumer_v2.TransactionPreferences transactionPreferences,
           com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount[] accounts,
           com.moneygram.mgo.service.consumer_v2.MGOAttributes internal,
           com.moneygram.mgo.service.consumer_v2.Comment[] comments,
           com.moneygram.mgo.service.consumer_v2.Access access,
           com.moneygram.mgo.shared.ConsumerAddress address,
           com.moneygram.mgo.service.consumer_v2.ProfileEvent[] profileEvents,
           java.lang.String sourceSite,
           com.moneygram.mgo.service.consumer_v2.Occupation occupation,
           java.lang.String test,
           java.lang.Long cnsmrProfileId) {
           this.consumerId = consumerId;
           this.loginId = loginId;
           this.personal = personal;
           this.contact = contact;
           this.loyalty = loyalty;
           this.transactionPreferences = transactionPreferences;
           this.accounts = accounts;
           this.internal = internal;
           this.comments = comments;
           this.access = access;
           this.address = address;
           this.profileEvents = profileEvents;
           this.sourceSite = sourceSite;
           this.occupation = occupation;
           this.test = test;
           this.cnsmrProfileId = cnsmrProfileId;
    }


    /**
     * Gets the consumerId value for this Consumer.
     * 
     * @return consumerId
     */
    public java.lang.Long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this Consumer.
     * 
     * @param consumerId
     */
    public void setConsumerId(java.lang.Long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the loginId value for this Consumer.
     * 
     * @return loginId
     */
    public java.lang.String getLoginId() {
        return loginId;
    }


    /**
     * Sets the loginId value for this Consumer.
     * 
     * @param loginId
     */
    public void setLoginId(java.lang.String loginId) {
        this.loginId = loginId;
    }


    /**
     * Gets the personal value for this Consumer.
     * 
     * @return personal
     */
    public com.moneygram.mgo.service.consumer_v2.PersonalInfo getPersonal() {
        return personal;
    }


    /**
     * Sets the personal value for this Consumer.
     * 
     * @param personal
     */
    public void setPersonal(com.moneygram.mgo.service.consumer_v2.PersonalInfo personal) {
        this.personal = personal;
    }


    /**
     * Gets the contact value for this Consumer.
     * 
     * @return contact
     */
    public com.moneygram.mgo.service.consumer_v2.Contact getContact() {
        return contact;
    }


    /**
     * Sets the contact value for this Consumer.
     * 
     * @param contact
     */
    public void setContact(com.moneygram.mgo.service.consumer_v2.Contact contact) {
        this.contact = contact;
    }


    /**
     * Gets the loyalty value for this Consumer.
     * 
     * @return loyalty
     */
    public com.moneygram.mgo.service.consumer_v2.LoyaltyInfo getLoyalty() {
        return loyalty;
    }


    /**
     * Sets the loyalty value for this Consumer.
     * 
     * @param loyalty
     */
    public void setLoyalty(com.moneygram.mgo.service.consumer_v2.LoyaltyInfo loyalty) {
        this.loyalty = loyalty;
    }


    /**
     * Gets the transactionPreferences value for this Consumer.
     * 
     * @return transactionPreferences
     */
    public com.moneygram.mgo.service.consumer_v2.TransactionPreferences getTransactionPreferences() {
        return transactionPreferences;
    }


    /**
     * Sets the transactionPreferences value for this Consumer.
     * 
     * @param transactionPreferences
     */
    public void setTransactionPreferences(com.moneygram.mgo.service.consumer_v2.TransactionPreferences transactionPreferences) {
        this.transactionPreferences = transactionPreferences;
    }


    /**
     * Gets the accounts value for this Consumer.
     * 
     * @return accounts
     */
    public com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount[] getAccounts() {
        return accounts;
    }


    /**
     * Sets the accounts value for this Consumer.
     * 
     * @param accounts
     */
    public void setAccounts(com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount[] accounts) {
        this.accounts = accounts;
    }


    /**
     * Gets the internal value for this Consumer.
     * 
     * @return internal
     */
    public com.moneygram.mgo.service.consumer_v2.MGOAttributes getInternal() {
        return internal;
    }


    /**
     * Sets the internal value for this Consumer.
     * 
     * @param internal
     */
    public void setInternal(com.moneygram.mgo.service.consumer_v2.MGOAttributes internal) {
        this.internal = internal;
    }


    /**
     * Gets the comments value for this Consumer.
     * 
     * @return comments
     */
    public com.moneygram.mgo.service.consumer_v2.Comment[] getComments() {
        return comments;
    }


    /**
     * Sets the comments value for this Consumer.
     * 
     * @param comments
     */
    public void setComments(com.moneygram.mgo.service.consumer_v2.Comment[] comments) {
        this.comments = comments;
    }


    /**
     * Gets the access value for this Consumer.
     * 
     * @return access
     */
    public com.moneygram.mgo.service.consumer_v2.Access getAccess() {
        return access;
    }


    /**
     * Sets the access value for this Consumer.
     * 
     * @param access
     */
    public void setAccess(com.moneygram.mgo.service.consumer_v2.Access access) {
        this.access = access;
    }


    /**
     * Gets the address value for this Consumer.
     * 
     * @return address
     */
    public com.moneygram.mgo.shared.ConsumerAddress getAddress() {
        return address;
    }


    /**
     * Sets the address value for this Consumer.
     * 
     * @param address
     */
    public void setAddress(com.moneygram.mgo.shared.ConsumerAddress address) {
        this.address = address;
    }


    /**
     * Gets the profileEvents value for this Consumer.
     * 
     * @return profileEvents
     */
    public com.moneygram.mgo.service.consumer_v2.ProfileEvent[] getProfileEvents() {
        return profileEvents;
    }


    /**
     * Sets the profileEvents value for this Consumer.
     * 
     * @param profileEvents
     */
    public void setProfileEvents(com.moneygram.mgo.service.consumer_v2.ProfileEvent[] profileEvents) {
        this.profileEvents = profileEvents;
    }

    public com.moneygram.mgo.service.consumer_v2.ProfileEvent getProfileEvents(int i) {
        return this.profileEvents[i];
    }

    public void setProfileEvents(int i, com.moneygram.mgo.service.consumer_v2.ProfileEvent _value) {
        this.profileEvents[i] = _value;
    }


    /**
     * Gets the sourceSite value for this Consumer.
     * 
     * @return sourceSite
     */
    public java.lang.String getSourceSite() {
        return sourceSite;
    }


    /**
     * Sets the sourceSite value for this Consumer.
     * 
     * @param sourceSite
     */
    public void setSourceSite(java.lang.String sourceSite) {
        this.sourceSite = sourceSite;
    }


    /**
     * Gets the occupation value for this Consumer.
     * 
     * @return occupation
     */
    public com.moneygram.mgo.service.consumer_v2.Occupation getOccupation() {
        return occupation;
    }


    /**
     * Sets the occupation value for this Consumer.
     * 
     * @param occupation
     */
    public void setOccupation(com.moneygram.mgo.service.consumer_v2.Occupation occupation) {
        this.occupation = occupation;
    }


    /**
     * Gets the test value for this Consumer.
     * 
     * @return test
     */
    public java.lang.String getTest() {
        return test;
    }


    /**
     * Sets the test value for this Consumer.
     * 
     * @param test
     */
    public void setTest(java.lang.String test) {
        this.test = test;
    }

    /**
	 * @return the cnsmrProfileId
	 */
	public java.lang.Long getCnsmrProfileId() {
		return cnsmrProfileId;
	}

	/**
	 * @param cnsmrProfileId the cnsmrProfileId to set
	 */
	public void setCnsmrProfileId(java.lang.Long cnsmrProfileId) {
		this.cnsmrProfileId = cnsmrProfileId;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Consumer)) return false;
        Consumer other = (Consumer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consumerId==null && other.getConsumerId()==null) || 
             (this.consumerId!=null &&
              this.consumerId.equals(other.getConsumerId()))) &&
            ((this.loginId==null && other.getLoginId()==null) || 
             (this.loginId!=null &&
              this.loginId.equals(other.getLoginId()))) &&
            ((this.personal==null && other.getPersonal()==null) || 
             (this.personal!=null &&
              this.personal.equals(other.getPersonal()))) &&
            ((this.contact==null && other.getContact()==null) || 
             (this.contact!=null &&
              this.contact.equals(other.getContact()))) &&
            ((this.loyalty==null && other.getLoyalty()==null) || 
             (this.loyalty!=null &&
              this.loyalty.equals(other.getLoyalty()))) &&
            ((this.transactionPreferences==null && other.getTransactionPreferences()==null) || 
             (this.transactionPreferences!=null &&
              this.transactionPreferences.equals(other.getTransactionPreferences()))) &&
            ((this.accounts==null && other.getAccounts()==null) || 
             (this.accounts!=null &&
              java.util.Arrays.equals(this.accounts, other.getAccounts()))) &&
            ((this.internal==null && other.getInternal()==null) || 
             (this.internal!=null &&
              this.internal.equals(other.getInternal()))) &&
            ((this.comments==null && other.getComments()==null) || 
             (this.comments!=null &&
              java.util.Arrays.equals(this.comments, other.getComments()))) &&
            ((this.access==null && other.getAccess()==null) || 
             (this.access!=null &&
              this.access.equals(other.getAccess()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.profileEvents==null && other.getProfileEvents()==null) || 
             (this.profileEvents!=null &&
              java.util.Arrays.equals(this.profileEvents, other.getProfileEvents()))) &&
            ((this.sourceSite==null && other.getSourceSite()==null) || 
             (this.sourceSite!=null &&
              this.sourceSite.equals(other.getSourceSite()))) &&
            ((this.occupation==null && other.getOccupation()==null) || 
             (this.occupation!=null &&
              this.occupation.equals(other.getOccupation()))) &&
            ((this.test==null && other.getTest()==null) || 
             (this.test!=null &&
              this.test.equals(other.getTest()))) &&
            ((this.cnsmrProfileId==null && other.getCnsmrProfileId()==null) || 
             (this.cnsmrProfileId!=null &&
              this.cnsmrProfileId.equals(other.getCnsmrProfileId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsumerId() != null) {
            _hashCode += getConsumerId().hashCode();
        }
        if (getLoginId() != null) {
            _hashCode += getLoginId().hashCode();
        }
        if (getPersonal() != null) {
            _hashCode += getPersonal().hashCode();
        }
        if (getContact() != null) {
            _hashCode += getContact().hashCode();
        }
        if (getLoyalty() != null) {
            _hashCode += getLoyalty().hashCode();
        }
        if (getTransactionPreferences() != null) {
            _hashCode += getTransactionPreferences().hashCode();
        }
        if (getAccounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInternal() != null) {
            _hashCode += getInternal().hashCode();
        }
        if (getComments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getComments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getComments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAccess() != null) {
            _hashCode += getAccess().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getProfileEvents() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProfileEvents());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProfileEvents(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSourceSite() != null) {
            _hashCode += getSourceSite().hashCode();
        }
        if (getOccupation() != null) {
            _hashCode += getOccupation().hashCode();
        }
        if (getTest() != null) {
            _hashCode += getTest().hashCode();
        }
        if (getCnsmrProfileId() != null) {
            _hashCode += getCnsmrProfileId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Consumer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Consumer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "loginId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "personal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "PersonalInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "contact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Contact"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "loyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "LoyaltyInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionPreferences");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "transactionPreferences"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "TransactionPreferences"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accounts");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "accounts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("internal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "internal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "MGOAttributes"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comments");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "comments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("access");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "access"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Access"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/shared_v1", "ConsumerAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileEvents");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "profileEvents"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfileEvent"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceSite");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "sourceSite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("occupation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "occupation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Occupation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("test");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "test"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnsmrProfileId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "cnsmrProfileId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
