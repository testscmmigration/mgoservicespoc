/**
 * Occupation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class Occupation  implements java.io.Serializable {
    private java.lang.Long ocupnCode;

    private java.lang.String ocupnDescription;

    public Occupation() {
    }

    public Occupation(
           java.lang.Long ocupnCode,
           java.lang.String ocupnDescription) {
           this.ocupnCode = ocupnCode;
           this.ocupnDescription = ocupnDescription;
    }


    /**
     * Gets the ocupnCode value for this Occupation.
     * 
     * @return ocupnCode
     */
    public java.lang.Long getOcupnCode() {
        return ocupnCode;
    }


    /**
     * Sets the ocupnCode value for this Occupation.
     * 
     * @param ocupnCode
     */
    public void setOcupnCode(java.lang.Long ocupnCode) {
        this.ocupnCode = ocupnCode;
    }


    /**
     * Gets the ocupnDescription value for this Occupation.
     * 
     * @return ocupnDescription
     */
    public java.lang.String getOcupnDescription() {
        return ocupnDescription;
    }


    /**
     * Sets the ocupnDescription value for this Occupation.
     * 
     * @param ocupnDescription
     */
    public void setOcupnDescription(java.lang.String ocupnDescription) {
        this.ocupnDescription = ocupnDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Occupation)) return false;
        Occupation other = (Occupation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ocupnCode==null && other.getOcupnCode()==null) || 
             (this.ocupnCode!=null &&
              this.ocupnCode.equals(other.getOcupnCode()))) &&
            ((this.ocupnDescription==null && other.getOcupnDescription()==null) || 
             (this.ocupnDescription!=null &&
              this.ocupnDescription.equals(other.getOcupnDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOcupnCode() != null) {
            _hashCode += getOcupnCode().hashCode();
        }
        if (getOcupnDescription() != null) {
            _hashCode += getOcupnDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Occupation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Occupation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocupnCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ocupnCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ocupnDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ocupnDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
