/**
 * FindConsumersRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class FindConsumersRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private java.lang.String ssnMask;

    private java.util.Calendar dateOfBirth;

    private java.lang.String lastName;

    private java.lang.String addressLine1StartsWith;
    
    private java.lang.String addressPostalCode;
    
    private java.lang.String addressBldgName;
    
    public FindConsumersRequest() {
    }

    public FindConsumersRequest(
           com.moneygram.common.service.RequestHeader header,
           java.lang.String ssnMask,
           java.util.Calendar dateOfBirth,
           java.lang.String lastName,
           java.lang.String addressLine1StartsWith,
           java.lang.String addressPostalCode,
           java.lang.String addressBldgName) {
        super(
            header);
        this.ssnMask = ssnMask;
        this.dateOfBirth = dateOfBirth;
        this.lastName = lastName;
        this.addressLine1StartsWith = addressLine1StartsWith;
        this.addressPostalCode = addressPostalCode;
        this.addressBldgName = addressBldgName;
    }


    /**
     * Gets the ssnMask value for this FindConsumersRequest.
     * 
     * @return ssnMask
     */
    public java.lang.String getSsnMask() {
        return ssnMask;
    }


    /**
     * Sets the ssnMask value for this FindConsumersRequest.
     * 
     * @param ssnMask
     */
    public void setSsnMask(java.lang.String ssnMask) {
        this.ssnMask = ssnMask;
    }


    /**
     * Gets the dateOfBirth value for this FindConsumersRequest.
     * 
     * @return dateOfBirth
     */
    public java.util.Calendar getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this FindConsumersRequest.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.util.Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the lastName value for this FindConsumersRequest.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this FindConsumersRequest.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the addressLine1StartsWith value for this FindConsumersRequest.
     * 
     * @return addressLine1StartsWith
     */
    public java.lang.String getAddressLine1StartsWith() {
        return addressLine1StartsWith;
    }


    /**
     * Sets the addressLine1StartsWith value for this FindConsumersRequest.
     * 
     * @param addressLine1StartsWith
     */
    public void setAddressLine1StartsWith(java.lang.String addressLine1StartsWith) {
        this.addressLine1StartsWith = addressLine1StartsWith;
    }

    /**
	 * @param addressPostalCode the addressPostalCode to set
	 */
	public void setAddressPostalCode(java.lang.String addressPostalCode) {
		this.addressPostalCode = addressPostalCode;
	}

	/**
	 * @return the addressPostalCode
	 */
	public java.lang.String getAddressPostalCode() {
		return addressPostalCode;
	}

	/**
	 * @param addressBldgName the addressBldgName to set
	 */
	public void setAddressBldgName(java.lang.String addressBldgName) {
		this.addressBldgName = addressBldgName;
	}

	/**
	 * @return the addressBldgName
	 */
	public java.lang.String getAddressBldgName() {
		return addressBldgName;
	}

	private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FindConsumersRequest)) return false;
        FindConsumersRequest other = (FindConsumersRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.ssnMask==null && other.getSsnMask()==null) || 
             (this.ssnMask!=null &&
              this.ssnMask.equals(other.getSsnMask()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.addressLine1StartsWith==null && other.getAddressLine1StartsWith()==null) || 
             (this.addressLine1StartsWith!=null &&
              this.addressLine1StartsWith.equals(other.getAddressLine1StartsWith()))) &&
            ((this.addressPostalCode==null && other.getAddressPostalCode()==null) || 
             (this.addressPostalCode!=null &&
              this.addressPostalCode.equals(other.getAddressPostalCode()))) &&
            ((this.addressBldgName==null && other.getAddressBldgName()==null) || 
             (this.addressBldgName!=null &&
              this.addressBldgName.equals(other.getAddressBldgName())));
       
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSsnMask() != null) {
            _hashCode += getSsnMask().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getAddressLine1StartsWith() != null) {
            _hashCode += getAddressLine1StartsWith().hashCode();
        }
        if (getAddressPostalCode() != null) {
            _hashCode += getAddressPostalCode().hashCode();
        }
        if (getAddressBldgName() != null) {
            _hashCode += getAddressBldgName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FindConsumersRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "FindConsumersRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ssnMask");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ssnMask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "dateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressLine1StartsWith");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addressLine1StartsWith"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressPostalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addressPostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressBldgName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "addressBldgName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);

    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
