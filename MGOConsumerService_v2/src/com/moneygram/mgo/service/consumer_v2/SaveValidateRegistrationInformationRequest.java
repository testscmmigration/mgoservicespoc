/**
 * SaveValidateRegistrationInformationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class SaveValidateRegistrationInformationRequest  extends com.moneygram.common.service.BaseOperationRequest   implements java.io.Serializable {
    private long custId;

    private java.lang.String authnId;

    private java.util.Calendar authnDate;

    private java.lang.String scoreNbr;

    private java.lang.String deciBandText;

    private java.lang.String remarksText;

    private java.lang.String statText;

    private com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls[] validateRsls;

    public SaveValidateRegistrationInformationRequest() {
    }

    public SaveValidateRegistrationInformationRequest(
    		com.moneygram.common.service.RequestHeader header,
           long custId,
           java.lang.String authnId,
           java.util.Calendar authnDate,
           java.lang.String scoreNbr,
           java.lang.String deciBandText,
           java.lang.String remarksText,
           java.lang.String statText,
           com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls[] validateRsls) {
        super(
            header);
        this.custId = custId;
        this.authnId = authnId;
        this.authnDate = authnDate;
        this.scoreNbr = scoreNbr;
        this.deciBandText = deciBandText;
        this.remarksText = remarksText;
        this.statText = statText;
        this.validateRsls = validateRsls;
    }


    /**
     * Gets the custId value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return custId
     */
    public long getCustId() {
        return custId;
    }


    /**
     * Sets the custId value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param custId
     */
    public void setCustId(long custId) {
        this.custId = custId;
    }


    /**
     * Gets the authnId value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return authnId
     */
    public java.lang.String getAuthnId() {
        return authnId;
    }


    /**
     * Sets the authnId value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param authnId
     */
    public void setAuthnId(java.lang.String authnId) {
        this.authnId = authnId;
    }


    /**
     * Gets the authnDate value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return authnDate
     */
    public java.util.Calendar getAuthnDate() {
        return authnDate;
    }


    /**
     * Sets the authnDate value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param authnDate
     */
    public void setAuthnDate(java.util.Calendar authnDate) {
        this.authnDate = authnDate;
    }


    /**
     * Gets the scoreNbr value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return scoreNbr
     */
    public java.lang.String getScoreNbr() {
        return scoreNbr;
    }


    /**
     * Sets the scoreNbr value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param scoreNbr
     */
    public void setScoreNbr(java.lang.String scoreNbr) {
        this.scoreNbr = scoreNbr;
    }


    /**
     * Gets the deciBandText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return deciBandText
     */
    public java.lang.String getDeciBandText() {
        return deciBandText;
    }


    /**
     * Sets the deciBandText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param deciBandText
     */
    public void setDeciBandText(java.lang.String deciBandText) {
        this.deciBandText = deciBandText;
    }


    /**
     * Gets the remarksText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return remarksText
     */
    public java.lang.String getRemarksText() {
        return remarksText;
    }


    /**
     * Sets the remarksText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param remarksText
     */
    public void setRemarksText(java.lang.String remarksText) {
        this.remarksText = remarksText;
    }


    /**
     * Gets the statText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return statText
     */
    public java.lang.String getStatText() {
        return statText;
    }


    /**
     * Sets the statText value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param statText
     */
    public void setStatText(java.lang.String statText) {
        this.statText = statText;
    }


    /**
     * Gets the validateRsls value for this SaveValidateRegistrationInformationRequest.
     * 
     * @return validateRsls
     */
    public com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls[] getValidateRsls() {
        return validateRsls;
    }


    /**
     * Sets the validateRsls value for this SaveValidateRegistrationInformationRequest.
     * 
     * @param validateRsls
     */
    public void setValidateRsls(com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls[] validateRsls) {
        this.validateRsls = validateRsls;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveValidateRegistrationInformationRequest)) return false;
        SaveValidateRegistrationInformationRequest other = (SaveValidateRegistrationInformationRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.custId == other.getCustId() &&
            ((this.authnId==null && other.getAuthnId()==null) || 
             (this.authnId!=null &&
              this.authnId.equals(other.getAuthnId()))) &&
            ((this.authnDate==null && other.getAuthnDate()==null) || 
             (this.authnDate!=null &&
              this.authnDate.equals(other.getAuthnDate()))) &&
            ((this.scoreNbr==null && other.getScoreNbr()==null) || 
             (this.scoreNbr!=null &&
              this.scoreNbr.equals(other.getScoreNbr()))) &&
            ((this.deciBandText==null && other.getDeciBandText()==null) || 
             (this.deciBandText!=null &&
              this.deciBandText.equals(other.getDeciBandText()))) &&
            ((this.remarksText==null && other.getRemarksText()==null) || 
             (this.remarksText!=null &&
              this.remarksText.equals(other.getRemarksText()))) &&
            ((this.statText==null && other.getStatText()==null) || 
             (this.statText!=null &&
              this.statText.equals(other.getStatText()))) &&
            ((this.validateRsls==null && other.getValidateRsls()==null) || 
             (this.validateRsls!=null &&
              java.util.Arrays.equals(this.validateRsls, other.getValidateRsls())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getCustId()).hashCode();
        if (getAuthnId() != null) {
            _hashCode += getAuthnId().hashCode();
        }
        if (getAuthnDate() != null) {
            _hashCode += getAuthnDate().hashCode();
        }
        if (getScoreNbr() != null) {
            _hashCode += getScoreNbr().hashCode();
        }
        if (getDeciBandText() != null) {
            _hashCode += getDeciBandText().hashCode();
        }
        if (getRemarksText() != null) {
            _hashCode += getRemarksText().hashCode();
        }
        if (getStatText() != null) {
            _hashCode += getStatText().hashCode();
        }
        if (getValidateRsls() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getValidateRsls());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getValidateRsls(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaveValidateRegistrationInformationRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveValidateRegistrationInformationRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "custId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authnId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "authnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authnDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "authnDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scoreNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "scoreNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deciBandText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "deciBandText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remarksText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "remarksText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "statText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validateRsls");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "validateRsls"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ValidateRegistrationSaveInfoRsls"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
