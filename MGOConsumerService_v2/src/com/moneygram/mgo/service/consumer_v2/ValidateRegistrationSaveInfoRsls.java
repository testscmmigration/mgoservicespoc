/**
 * ValidateRegistrationSaveInfoRsls.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class ValidateRegistrationSaveInfoRsls  implements java.io.Serializable {
    private java.lang.String chkTypeDesc;

    private java.lang.String chkTypeCode;

    private java.lang.String rsltCode;

    private java.lang.String rsltDesc;

    public ValidateRegistrationSaveInfoRsls() {
    }

    public ValidateRegistrationSaveInfoRsls(
           java.lang.String chkTypeDesc,
           java.lang.String chkTypeCode,
           java.lang.String rsltCode,
           java.lang.String rsltDesc) {
           this.chkTypeDesc = chkTypeDesc;
           this.chkTypeCode = chkTypeCode;
           this.rsltCode = rsltCode;
           this.rsltDesc = rsltDesc;
    }


    /**
     * Gets the chkTypeDesc value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @return chkTypeDesc
     */
    public java.lang.String getChkTypeDesc() {
        return chkTypeDesc;
    }


    /**
     * Sets the chkTypeDesc value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @param chkTypeDesc
     */
    public void setChkTypeDesc(java.lang.String chkTypeDesc) {
        this.chkTypeDesc = chkTypeDesc;
    }


    /**
     * Gets the chkTypeCode value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @return chkTypeCode
     */
    public java.lang.String getChkTypeCode() {
        return chkTypeCode;
    }


    /**
     * Sets the chkTypeCode value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @param chkTypeCode
     */
    public void setChkTypeCode(java.lang.String chkTypeCode) {
        this.chkTypeCode = chkTypeCode;
    }


    /**
     * Gets the rsltCode value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @return rsltCode
     */
    public java.lang.String getRsltCode() {
        return rsltCode;
    }


    /**
     * Sets the rsltCode value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @param rsltCode
     */
    public void setRsltCode(java.lang.String rsltCode) {
        this.rsltCode = rsltCode;
    }


    /**
     * Gets the rsltDesc value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @return rsltDesc
     */
    public java.lang.String getRsltDesc() {
        return rsltDesc;
    }


    /**
     * Sets the rsltDesc value for this ValidateRegistrationSaveInfoRsls.
     * 
     * @param rsltDesc
     */
    public void setRsltDesc(java.lang.String rsltDesc) {
        this.rsltDesc = rsltDesc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidateRegistrationSaveInfoRsls)) return false;
        ValidateRegistrationSaveInfoRsls other = (ValidateRegistrationSaveInfoRsls) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.chkTypeDesc==null && other.getChkTypeDesc()==null) || 
             (this.chkTypeDesc!=null &&
              this.chkTypeDesc.equals(other.getChkTypeDesc()))) &&
            ((this.chkTypeCode==null && other.getChkTypeCode()==null) || 
             (this.chkTypeCode!=null &&
              this.chkTypeCode.equals(other.getChkTypeCode()))) &&
            ((this.rsltCode==null && other.getRsltCode()==null) || 
             (this.rsltCode!=null &&
              this.rsltCode.equals(other.getRsltCode()))) &&
            ((this.rsltDesc==null && other.getRsltDesc()==null) || 
             (this.rsltDesc!=null &&
              this.rsltDesc.equals(other.getRsltDesc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getChkTypeDesc() != null) {
            _hashCode += getChkTypeDesc().hashCode();
        }
        if (getChkTypeCode() != null) {
            _hashCode += getChkTypeCode().hashCode();
        }
        if (getRsltCode() != null) {
            _hashCode += getRsltCode().hashCode();
        }
        if (getRsltDesc() != null) {
            _hashCode += getRsltDesc().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidateRegistrationSaveInfoRsls.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ValidateRegistrationSaveInfoRsls"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chkTypeDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "chkTypeDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("chkTypeCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "chkTypeCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rsltCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "rsltCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rsltDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "rsltDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
