/**
 * GetOccupationsListRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class GetOccupationsListRequest  extends com.moneygram.common.service.BaseOperationRequest   implements java.io.Serializable {
    private java.lang.String isoCountryCode;

    private java.lang.String languageTagText;

    public GetOccupationsListRequest() {
    }

    public GetOccupationsListRequest(
    		com.moneygram.common.service.RequestHeader header,
           java.lang.String isoCountryCode,
           java.lang.String languageTagText) {
        super(
            header);
        this.isoCountryCode = isoCountryCode;
        this.languageTagText = languageTagText;
    }


    /**
     * Gets the isoCountryCode value for this GetOccupationsListRequest.
     * 
     * @return isoCountryCode
     */
    public java.lang.String getIsoCountryCode() {
        return isoCountryCode;
    }


    /**
     * Sets the isoCountryCode value for this GetOccupationsListRequest.
     * 
     * @param isoCountryCode
     */
    public void setIsoCountryCode(java.lang.String isoCountryCode) {
        this.isoCountryCode = isoCountryCode;
    }


    /**
     * Gets the languageTagText value for this GetOccupationsListRequest.
     * 
     * @return languageTagText
     */
    public java.lang.String getLanguageTagText() {
        return languageTagText;
    }


    /**
     * Sets the languageTagText value for this GetOccupationsListRequest.
     * 
     * @param languageTagText
     */
    public void setLanguageTagText(java.lang.String languageTagText) {
        this.languageTagText = languageTagText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOccupationsListRequest)) return false;
        GetOccupationsListRequest other = (GetOccupationsListRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.isoCountryCode==null && other.getIsoCountryCode()==null) || 
             (this.isoCountryCode!=null &&
              this.isoCountryCode.equals(other.getIsoCountryCode()))) &&
            ((this.languageTagText==null && other.getLanguageTagText()==null) || 
             (this.languageTagText!=null &&
              this.languageTagText.equals(other.getLanguageTagText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getIsoCountryCode() != null) {
            _hashCode += getIsoCountryCode().hashCode();
        }
        if (getLanguageTagText() != null) {
            _hashCode += getLanguageTagText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOccupationsListRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "GetOccupationsListRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "isoCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("languageTagText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "languageTagText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
