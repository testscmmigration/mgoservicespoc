package com.moneygram.mgo.service.consumer_v2;

/**
 * @author vx15
 */
public class CustomerExternalIdentificationInformation implements java.io.Serializable {
	
	private static final long serialVersionUID = -4830373328670106021L;
	
	private Long custExtnlIdentId;
	private String encryptExternalId;
	private String maskExternalId;
	private Long customerId;
	private String identDocBsnsCode;
	private String issuIsoCntryCode;
	private String isoSubdivCode;
	private java.util.Date issuDate;
	private java.util.Date expDate;
	private String encryptPsprtMrzLn2Text;
	private String bsnsCode;
	
	
	public Long getCustExtnlIdentId() {
		return custExtnlIdentId;
	}
	public void setCustExtnlIdentId(Long custExtnlIdentId) {
		this.custExtnlIdentId = custExtnlIdentId;
	}
	public String getEncryptExternalId() {
		return encryptExternalId;
	}
	public void setEncryptExternalId(String encryptExternalId) {
		this.encryptExternalId = encryptExternalId;
	}
	public String getMaskExternalId() {
		return maskExternalId;
	}
	public void setMaskExternalId(String maskExternalId) {
		this.maskExternalId = maskExternalId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getIdentDocBsnsCode() {
		return identDocBsnsCode;
	}
	public void setIdentDocBsnsCode(String identDocBsnsCode) {
		this.identDocBsnsCode = identDocBsnsCode;
	}
	public String getIssuIsoCntryCode() {
		return issuIsoCntryCode;
	}
	public void setIssuIsoCntryCode(String issuIsoCntryCode) {
		this.issuIsoCntryCode = issuIsoCntryCode;
	}
	public String getIsoSubdivCode() {
		return isoSubdivCode;
	}
	public void setIsoSubdivCode(String isoSubdivCode) {
		this.isoSubdivCode = isoSubdivCode;
	}
	public java.util.Date getIssuDate() {
		return issuDate;
	}
	public void setIssuDate(java.util.Date issuDate) {
		this.issuDate = issuDate;
	}
	public java.util.Date getExpDate() {
		return expDate;
	}
	public void setExpDate(java.util.Date expDate) {
		this.expDate = expDate;
	}
	public String getEncryptPsprtMrzLn2Text() {
		return encryptPsprtMrzLn2Text;
	}
	public void setEncryptPsprtMrzLn2Text(String encryptPsprtMrzLn2Text) {
		this.encryptPsprtMrzLn2Text = encryptPsprtMrzLn2Text;
	}
	public String getBsnsCode() {
		return bsnsCode;
	}
	public void setBsnsCode(String bsnsCode) {
		this.bsnsCode = bsnsCode;
	}
	
	
	
}
