/**
 * ServiceAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class ServiceAction implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ServiceAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _getConsumerProfile_v2 = "getConsumerProfile_v2";
    public static final java.lang.String _updateConsumerProfile_v2 = "updateConsumerProfile_v2";
    public static final java.lang.String _createConsumerProfile_v2 = "createConsumerProfile_v2";
    public static final java.lang.String _updateConsumerAccount_v2 = "updateConsumerAccount_v2";
    public static final java.lang.String _addConsumerAccount_v2 = "addConsumerAccount_v2";
    public static final java.lang.String _getConsumerBlockedStatus_v2 = "getConsumerBlockedStatus_v2";
    public static final java.lang.String _findConsumers_v2 = "findConsumers_v2";
    public static final java.lang.String _getIncompleteProfiles_v2 = "getIncompleteProfiles_v2";
    public static final java.lang.String _getProfileEvents_v2 = "getProfileEvents_v2";
    public static final java.lang.String _saveProfileEvent_v2 = "saveProfileEvent_v2";
    public static final java.lang.String _saveLogonTry_v2 = "saveLogonTry_v2";
    public static final java.lang.String _getOccupationsList_v2 = "getOccupationsList_v2";
    public static final java.lang.String _addImage_v2 = "addImage_v2";
    public static final java.lang.String _getConsumerIDImage_v2 = "getConsumerIDImage_v2";
    public static final java.lang.String _updateConsumerIDImage_v2 = "updateConsumerIDImage_v2";
    public static final java.lang.String _getConsumerIDApprovalStatus_v2 = "getConsumerIDApprovalStatus_v2";
    public static final java.lang.String _updateConsumerIDImageStatus_v2 = "updateConsumerIDImageStatus_v2";
    public static final java.lang.String _getValidateRegistrationInformation_v2 = "getValidateRegistrationInformation_v2";
    public static final java.lang.String _saveValidateRegistrationInformation_v2 = "saveValidateRegistrationInformation_v2";
    public static final java.lang.String _getIncompleteFirstTxProfiles_v2 = "getIncompleteFirstTxProfiles_v2";
    public static final java.lang.String _getCCLowAuthSequencerValue_v2 = "getCCLowAuthSequencerValue_v2";
	public static final java.lang.String _getGlobalConsumerProfileId_v2 = "getGlobalConsumerProfileId_v2";
    public static final ServiceAction getConsumerProfile_v2 = new ServiceAction(_getConsumerProfile_v2);
    public static final ServiceAction updateConsumerProfile_v2 = new ServiceAction(_updateConsumerProfile_v2);
    public static final ServiceAction createConsumerProfile_v2 = new ServiceAction(_createConsumerProfile_v2);
    public static final ServiceAction updateConsumerAccount_v2 = new ServiceAction(_updateConsumerAccount_v2);
    public static final ServiceAction addConsumerAccount_v2 = new ServiceAction(_addConsumerAccount_v2);
    public static final ServiceAction getConsumerBlockedStatus_v2 = new ServiceAction(_getConsumerBlockedStatus_v2);
    public static final ServiceAction findConsumers_v2 = new ServiceAction(_findConsumers_v2);
    public static final ServiceAction getIncompleteProfiles_v2 = new ServiceAction(_getIncompleteProfiles_v2);
    public static final ServiceAction getProfileEvents_v2 = new ServiceAction(_getProfileEvents_v2);
    public static final ServiceAction saveProfileEvent_v2 = new ServiceAction(_saveProfileEvent_v2);
    public static final ServiceAction saveLogonTry_v2 = new ServiceAction(_saveLogonTry_v2);
    public static final ServiceAction getOccupationsList_v2 = new ServiceAction(_getOccupationsList_v2);
    public static final ServiceAction addImage_v2 = new ServiceAction(_addImage_v2);
    public static final ServiceAction getConsumerIDImage_v2 = new ServiceAction(_getConsumerIDImage_v2);
    public static final ServiceAction updateConsumerIDImage_v2 = new ServiceAction(_updateConsumerIDImage_v2);
    public static final ServiceAction getConsumerIDApprovalStatus_v2 = new ServiceAction(_getConsumerIDApprovalStatus_v2);
    public static final ServiceAction updateConsumerIDImageStatus_v2 = new ServiceAction(_updateConsumerIDImageStatus_v2);
    public static final ServiceAction getValidateRegistrationInformation_v2 = new ServiceAction(_getValidateRegistrationInformation_v2);
    public static final ServiceAction saveValidateRegistrationInformation_v2 = new ServiceAction(_saveValidateRegistrationInformation_v2);
    public static final ServiceAction getIncompleteFirstTxProfiles_v2 = new ServiceAction(_getIncompleteFirstTxProfiles_v2);
    public static final ServiceAction getCCLowAuthSequencerValue_v2 = new ServiceAction(_getCCLowAuthSequencerValue_v2);
    public static final ServiceAction getGlobalConsumerProfileId_v2 = new ServiceAction(_getGlobalConsumerProfileId_v2);
    
    public java.lang.String getValue() { return _value_;}
    public static ServiceAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ServiceAction enumeration = (ServiceAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ServiceAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceAction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ServiceAction"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
