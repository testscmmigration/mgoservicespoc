package com.moneygram.mgo.service.consumer_v2;

/**
 * @author vx15
 */
public class CreditCardReponse {

	private CreditCard creditCard;
	private String blockedStatCode;
	
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	public CreditCard getCreditCard() {
		return creditCard;
	}
	
    public void setBlockedStatCode(String blockedStatCode) {
		this.blockedStatCode = blockedStatCode;
	}
    
    public String getBlockedStatCode() {
		return blockedStatCode;
	}
	
}
