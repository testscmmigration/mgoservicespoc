/**
 * UpdateAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class UpdateAccountRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount;

    private com.moneygram.mgo.service.consumer_v2.Comment comment;

    private com.moneygram.mgo.service.consumer_v2.UpdateAccountTask[] updateTasks;

    public UpdateAccountRequest() {
    }

    public UpdateAccountRequest(
           com.moneygram.common.service.RequestHeader header,
           com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount,
           com.moneygram.mgo.service.consumer_v2.Comment comment,
           com.moneygram.mgo.service.consumer_v2.UpdateAccountTask[] updateTasks) {
        super(
            header);
        this.consumerFIAccount = consumerFIAccount;
        this.comment = comment;
        this.updateTasks = updateTasks;
    }


    /**
     * Gets the consumerFIAccount value for this UpdateAccountRequest.
     * 
     * @return consumerFIAccount
     */
    public com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount getConsumerFIAccount() {
        return consumerFIAccount;
    }


    /**
     * Sets the consumerFIAccount value for this UpdateAccountRequest.
     * 
     * @param consumerFIAccount
     */
    public void setConsumerFIAccount(com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount consumerFIAccount) {
        this.consumerFIAccount = consumerFIAccount;
    }


    /**
     * Gets the comment value for this UpdateAccountRequest.
     * 
     * @return comment
     */
    public com.moneygram.mgo.service.consumer_v2.Comment getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this UpdateAccountRequest.
     * 
     * @param comment
     */
    public void setComment(com.moneygram.mgo.service.consumer_v2.Comment comment) {
        this.comment = comment;
    }


    /**
     * Gets the updateTasks value for this UpdateAccountRequest.
     * 
     * @return updateTasks
     */
    public com.moneygram.mgo.service.consumer_v2.UpdateAccountTask[] getUpdateTasks() {
        return updateTasks;
    }


    /**
     * Sets the updateTasks value for this UpdateAccountRequest.
     * 
     * @param updateTasks
     */
    public void setUpdateTasks(com.moneygram.mgo.service.consumer_v2.UpdateAccountTask[] updateTasks) {
        this.updateTasks = updateTasks;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateAccountRequest)) return false;
        UpdateAccountRequest other = (UpdateAccountRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.consumerFIAccount==null && other.getConsumerFIAccount()==null) || 
             (this.consumerFIAccount!=null &&
              this.consumerFIAccount.equals(other.getConsumerFIAccount()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.updateTasks==null && other.getUpdateTasks()==null) || 
             (this.updateTasks!=null &&
              java.util.Arrays.equals(this.updateTasks, other.getUpdateTasks())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConsumerFIAccount() != null) {
            _hashCode += getConsumerFIAccount().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getUpdateTasks() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpdateTasks());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpdateTasks(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateAccountRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerFIAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerFIAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ConsumerFIAccount"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "Comment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateTasks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateTasks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateAccountTask"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "updateAccountTask"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
