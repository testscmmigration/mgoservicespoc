/**
 * SaveProfileEventRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class SaveProfileEventRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    private long consumerId;

    private com.moneygram.mgo.service.consumer_v2.ProfileEvent event;

    public SaveProfileEventRequest() {
    }

    public SaveProfileEventRequest(
           com.moneygram.common.service.RequestHeader header,
           long consumerId,
           com.moneygram.mgo.service.consumer_v2.ProfileEvent event) {
        super(
            header);
        this.consumerId = consumerId;
        this.event = event;
    }


    /**
     * Gets the consumerId value for this SaveProfileEventRequest.
     * 
     * @return consumerId
     */
    public long getConsumerId() {
        return consumerId;
    }


    /**
     * Sets the consumerId value for this SaveProfileEventRequest.
     * 
     * @param consumerId
     */
    public void setConsumerId(long consumerId) {
        this.consumerId = consumerId;
    }


    /**
     * Gets the event value for this SaveProfileEventRequest.
     * 
     * @return event
     */
    public com.moneygram.mgo.service.consumer_v2.ProfileEvent getEvent() {
        return event;
    }


    /**
     * Sets the event value for this SaveProfileEventRequest.
     * 
     * @param event
     */
    public void setEvent(com.moneygram.mgo.service.consumer_v2.ProfileEvent event) {
        this.event = event;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SaveProfileEventRequest)) return false;
        SaveProfileEventRequest other = (SaveProfileEventRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.consumerId == other.getConsumerId() &&
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getConsumerId()).hashCode();
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SaveProfileEventRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "SaveProfileEventRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("event");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "event"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfileEvent"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
