/**
 * ProfileEvent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class ProfileEvent  implements java.io.Serializable {
    /* Event business code that is defined in ACTY_LOG_BSNS_CODE field
     * of ACTY_LOG table. */
    private java.lang.String event;

    private java.util.Calendar eventDateTime;

    public ProfileEvent() {
    }

    public ProfileEvent(
           java.lang.String event,
           java.util.Calendar eventDateTime) {
           this.event = event;
           this.eventDateTime = eventDateTime;
    }


    /**
     * Gets the event value for this ProfileEvent.
     * 
     * @return event   * Event business code that is defined in ACTY_LOG_BSNS_CODE field
     * of ACTY_LOG table.
     */
    public java.lang.String getEvent() {
        return event;
    }


    /**
     * Sets the event value for this ProfileEvent.
     * 
     * @param event   * Event business code that is defined in ACTY_LOG_BSNS_CODE field
     * of ACTY_LOG table.
     */
    public void setEvent(java.lang.String event) {
        this.event = event;
    }


    /**
     * Gets the eventDateTime value for this ProfileEvent.
     * 
     * @return eventDateTime
     */
    public java.util.Calendar getEventDateTime() {
        return eventDateTime;
    }


    /**
     * Sets the eventDateTime value for this ProfileEvent.
     * 
     * @param eventDateTime
     */
    public void setEventDateTime(java.util.Calendar eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProfileEvent)) return false;
        ProfileEvent other = (ProfileEvent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent()))) &&
            ((this.eventDateTime==null && other.getEventDateTime()==null) || 
             (this.eventDateTime!=null &&
              this.eventDateTime.equals(other.getEventDateTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        if (getEventDateTime() != null) {
            _hashCode += getEventDateTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProfileEvent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "ProfileEvent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("event");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "event"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "eventDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
