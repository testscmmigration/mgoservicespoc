/**
 * UpdateConsumerProfileTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class UpdateConsumerProfileTask implements java.io.Serializable {

	private static final long serialVersionUID = -7503208376056667971L;
	
	private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected UpdateConsumerProfileTask(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _UpdateStatus = "UpdateStatus";
    public static final java.lang.String _SaveAgents = "SaveAgents";
    public static final java.lang.String _AddComment = "AddComment";
    public static final java.lang.String _UpdatePersonal = "UpdatePersonal";
    public static final java.lang.String _UpdateContact = "UpdateContact";
    public static final java.lang.String _ChangePassword = "ChangePassword";
    public static final java.lang.String _UpdateBlocked = "UpdateBlocked";
    public static final java.lang.String _LogAccess = "LogAccess";
    public static final java.lang.String _UpdateLoyalty = "UpdateLoyalty";
    public static final java.lang.String _UpdateSecurity = "UpdateSecurity";
    public static final java.lang.String _AddProfileEvent = "AddProfileEvent";
    public static final java.lang.String _UpdateName = "UpdateName";
    
    public static final UpdateConsumerProfileTask UpdateStatus = new UpdateConsumerProfileTask(_UpdateStatus);
    public static final UpdateConsumerProfileTask SaveAgents = new UpdateConsumerProfileTask(_SaveAgents);
    public static final UpdateConsumerProfileTask AddComment = new UpdateConsumerProfileTask(_AddComment);
    public static final UpdateConsumerProfileTask UpdatePersonal = new UpdateConsumerProfileTask(_UpdatePersonal);
    public static final UpdateConsumerProfileTask UpdateContact = new UpdateConsumerProfileTask(_UpdateContact);
    public static final UpdateConsumerProfileTask ChangePassword = new UpdateConsumerProfileTask(_ChangePassword);
    public static final UpdateConsumerProfileTask UpdateBlocked = new UpdateConsumerProfileTask(_UpdateBlocked);
    public static final UpdateConsumerProfileTask LogAccess = new UpdateConsumerProfileTask(_LogAccess);
    public static final UpdateConsumerProfileTask UpdateLoyalty = new UpdateConsumerProfileTask(_UpdateLoyalty);
    public static final UpdateConsumerProfileTask UpdateSecurity = new UpdateConsumerProfileTask(_UpdateSecurity);
    public static final UpdateConsumerProfileTask AddProfileEvent = new UpdateConsumerProfileTask(_AddProfileEvent);
    public static final UpdateConsumerProfileTask UpdateName = new UpdateConsumerProfileTask(_UpdateName);
    
    
    
    public java.lang.String getValue() { 
    	return _value_;
    }
    
    public static UpdateConsumerProfileTask fromValue(java.lang.String value) throws java.lang.IllegalArgumentException {
        UpdateConsumerProfileTask enumeration = (UpdateConsumerProfileTask)_table_.get(value);
        if (enumeration==null) 
        	throw new java.lang.IllegalArgumentException("invalid value = " + value);
        return enumeration;
    }
    public static UpdateConsumerProfileTask fromString(java.lang.String value) throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateConsumerProfileTask.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerProfileTask"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
