/**
 * MGOAttributes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class MGOAttributes  implements java.io.Serializable {
    private java.lang.String premierStatus;

    private java.lang.String consumerStatus;

    private java.lang.String consumerSubStatus;

    private java.lang.Boolean consumerBlocked;

    private java.lang.Boolean primaryPhoneBlocked;

    private java.lang.Boolean alternatePhoneBlocked;

    private java.lang.Boolean emailBlocked;

    private java.lang.Boolean emailDomainBlocked;

    private java.util.Calendar securityQuestionsCollectionDate;

    private java.lang.String securityQuestion;

    private java.lang.String securityAnswer;

    private java.lang.String globalUniqueId;

    private java.util.Calendar createDate;

    public MGOAttributes() {
    }

    public MGOAttributes(
           java.lang.String premierStatus,
           java.lang.String consumerStatus,
           java.lang.String consumerSubStatus,
           java.lang.Boolean consumerBlocked,
           java.lang.Boolean primaryPhoneBlocked,
           java.lang.Boolean alternatePhoneBlocked,
           java.lang.Boolean emailBlocked,
           java.lang.Boolean emailDomainBlocked,
           java.util.Calendar securityQuestionsCollectionDate,
           java.lang.String securityQuestion,
           java.lang.String securityAnswer,
           java.lang.String globalUniqueId,
           java.util.Calendar createDate) {
           this.premierStatus = premierStatus;
           this.consumerStatus = consumerStatus;
           this.consumerSubStatus = consumerSubStatus;
           this.consumerBlocked = consumerBlocked;
           this.primaryPhoneBlocked = primaryPhoneBlocked;
           this.alternatePhoneBlocked = alternatePhoneBlocked;
           this.emailBlocked = emailBlocked;
           this.emailDomainBlocked = emailDomainBlocked;
           this.securityQuestionsCollectionDate = securityQuestionsCollectionDate;
           this.securityQuestion = securityQuestion;
           this.securityAnswer = securityAnswer;
           this.globalUniqueId = globalUniqueId;
           this.createDate = createDate;
    }


    /**
     * Gets the premierStatus value for this MGOAttributes.
     * 
     * @return premierStatus
     */
    public java.lang.String getPremierStatus() {
        return premierStatus;
    }


    /**
     * Sets the premierStatus value for this MGOAttributes.
     * 
     * @param premierStatus
     */
    public void setPremierStatus(java.lang.String premierStatus) {
        this.premierStatus = premierStatus;
    }


    /**
     * Gets the consumerStatus value for this MGOAttributes.
     * 
     * @return consumerStatus
     */
    public java.lang.String getConsumerStatus() {
        return consumerStatus;
    }


    /**
     * Sets the consumerStatus value for this MGOAttributes.
     * 
     * @param consumerStatus
     */
    public void setConsumerStatus(java.lang.String consumerStatus) {
        this.consumerStatus = consumerStatus;
    }


    /**
     * Gets the consumerSubStatus value for this MGOAttributes.
     * 
     * @return consumerSubStatus
     */
    public java.lang.String getConsumerSubStatus() {
        return consumerSubStatus;
    }


    /**
     * Sets the consumerSubStatus value for this MGOAttributes.
     * 
     * @param consumerSubStatus
     */
    public void setConsumerSubStatus(java.lang.String consumerSubStatus) {
        this.consumerSubStatus = consumerSubStatus;
    }


    /**
     * Gets the consumerBlocked value for this MGOAttributes.
     * 
     * @return consumerBlocked
     */
    public java.lang.Boolean getConsumerBlocked() {
        return consumerBlocked;
    }


    /**
     * Sets the consumerBlocked value for this MGOAttributes.
     * 
     * @param consumerBlocked
     */
    public void setConsumerBlocked(java.lang.Boolean consumerBlocked) {
        this.consumerBlocked = consumerBlocked;
    }


    /**
     * Gets the primaryPhoneBlocked value for this MGOAttributes.
     * 
     * @return primaryPhoneBlocked
     */
    public java.lang.Boolean getPrimaryPhoneBlocked() {
        return primaryPhoneBlocked;
    }


    /**
     * Sets the primaryPhoneBlocked value for this MGOAttributes.
     * 
     * @param primaryPhoneBlocked
     */
    public void setPrimaryPhoneBlocked(java.lang.Boolean primaryPhoneBlocked) {
        this.primaryPhoneBlocked = primaryPhoneBlocked;
    }


    /**
     * Gets the alternatePhoneBlocked value for this MGOAttributes.
     * 
     * @return alternatePhoneBlocked
     */
    public java.lang.Boolean getAlternatePhoneBlocked() {
        return alternatePhoneBlocked;
    }


    /**
     * Sets the alternatePhoneBlocked value for this MGOAttributes.
     * 
     * @param alternatePhoneBlocked
     */
    public void setAlternatePhoneBlocked(java.lang.Boolean alternatePhoneBlocked) {
        this.alternatePhoneBlocked = alternatePhoneBlocked;
    }


    /**
     * Gets the emailBlocked value for this MGOAttributes.
     * 
     * @return emailBlocked
     */
    public java.lang.Boolean getEmailBlocked() {
        return emailBlocked;
    }


    /**
     * Sets the emailBlocked value for this MGOAttributes.
     * 
     * @param emailBlocked
     */
    public void setEmailBlocked(java.lang.Boolean emailBlocked) {
        this.emailBlocked = emailBlocked;
    }


    /**
     * Gets the emailDomainBlocked value for this MGOAttributes.
     * 
     * @return emailDomainBlocked
     */
    public java.lang.Boolean getEmailDomainBlocked() {
        return emailDomainBlocked;
    }


    /**
     * Sets the emailDomainBlocked value for this MGOAttributes.
     * 
     * @param emailDomainBlocked
     */
    public void setEmailDomainBlocked(java.lang.Boolean emailDomainBlocked) {
        this.emailDomainBlocked = emailDomainBlocked;
    }


    /**
     * Gets the securityQuestionsCollectionDate value for this MGOAttributes.
     * 
     * @return securityQuestionsCollectionDate
     */
    public java.util.Calendar getSecurityQuestionsCollectionDate() {
        return securityQuestionsCollectionDate;
    }


    /**
     * Sets the securityQuestionsCollectionDate value for this MGOAttributes.
     * 
     * @param securityQuestionsCollectionDate
     */
    public void setSecurityQuestionsCollectionDate(java.util.Calendar securityQuestionsCollectionDate) {
        this.securityQuestionsCollectionDate = securityQuestionsCollectionDate;
    }


    /**
     * Gets the securityQuestion value for this MGOAttributes.
     * 
     * @return securityQuestion
     */
    public java.lang.String getSecurityQuestion() {
        return securityQuestion;
    }


    /**
     * Sets the securityQuestion value for this MGOAttributes.
     * 
     * @param securityQuestion
     */
    public void setSecurityQuestion(java.lang.String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }


    /**
     * Gets the securityAnswer value for this MGOAttributes.
     * 
     * @return securityAnswer
     */
    public java.lang.String getSecurityAnswer() {
        return securityAnswer;
    }


    /**
     * Sets the securityAnswer value for this MGOAttributes.
     * 
     * @param securityAnswer
     */
    public void setSecurityAnswer(java.lang.String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }


    /**
     * Gets the globalUniqueId value for this MGOAttributes.
     * 
     * @return globalUniqueId
     */
    public java.lang.String getGlobalUniqueId() {
        return globalUniqueId;
    }


    /**
     * Sets the globalUniqueId value for this MGOAttributes.
     * 
     * @param globalUniqueId
     */
    public void setGlobalUniqueId(java.lang.String globalUniqueId) {
        this.globalUniqueId = globalUniqueId;
    }


    /**
     * Gets the createDate value for this MGOAttributes.
     * 
     * @return createDate
     */
    public java.util.Calendar getCreateDate() {
        return createDate;
    }


    /**
     * Sets the createDate value for this MGOAttributes.
     * 
     * @param createDate
     */
    public void setCreateDate(java.util.Calendar createDate) {
        this.createDate = createDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MGOAttributes)) return false;
        MGOAttributes other = (MGOAttributes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.premierStatus==null && other.getPremierStatus()==null) || 
             (this.premierStatus!=null &&
              this.premierStatus.equals(other.getPremierStatus()))) &&
            ((this.consumerStatus==null && other.getConsumerStatus()==null) || 
             (this.consumerStatus!=null &&
              this.consumerStatus.equals(other.getConsumerStatus()))) &&
            ((this.consumerSubStatus==null && other.getConsumerSubStatus()==null) || 
             (this.consumerSubStatus!=null &&
              this.consumerSubStatus.equals(other.getConsumerSubStatus()))) &&
            ((this.consumerBlocked==null && other.getConsumerBlocked()==null) || 
             (this.consumerBlocked!=null &&
              this.consumerBlocked.equals(other.getConsumerBlocked()))) &&
            ((this.primaryPhoneBlocked==null && other.getPrimaryPhoneBlocked()==null) || 
             (this.primaryPhoneBlocked!=null &&
              this.primaryPhoneBlocked.equals(other.getPrimaryPhoneBlocked()))) &&
            ((this.alternatePhoneBlocked==null && other.getAlternatePhoneBlocked()==null) || 
             (this.alternatePhoneBlocked!=null &&
              this.alternatePhoneBlocked.equals(other.getAlternatePhoneBlocked()))) &&
            ((this.emailBlocked==null && other.getEmailBlocked()==null) || 
             (this.emailBlocked!=null &&
              this.emailBlocked.equals(other.getEmailBlocked()))) &&
            ((this.emailDomainBlocked==null && other.getEmailDomainBlocked()==null) || 
             (this.emailDomainBlocked!=null &&
              this.emailDomainBlocked.equals(other.getEmailDomainBlocked()))) &&
            ((this.securityQuestionsCollectionDate==null && other.getSecurityQuestionsCollectionDate()==null) || 
             (this.securityQuestionsCollectionDate!=null &&
              this.securityQuestionsCollectionDate.equals(other.getSecurityQuestionsCollectionDate()))) &&
            ((this.securityQuestion==null && other.getSecurityQuestion()==null) || 
             (this.securityQuestion!=null &&
              this.securityQuestion.equals(other.getSecurityQuestion()))) &&
            ((this.securityAnswer==null && other.getSecurityAnswer()==null) || 
             (this.securityAnswer!=null &&
              this.securityAnswer.equals(other.getSecurityAnswer()))) &&
            ((this.globalUniqueId==null && other.getGlobalUniqueId()==null) || 
             (this.globalUniqueId!=null &&
              this.globalUniqueId.equals(other.getGlobalUniqueId()))) &&
            ((this.createDate==null && other.getCreateDate()==null) || 
             (this.createDate!=null &&
              this.createDate.equals(other.getCreateDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPremierStatus() != null) {
            _hashCode += getPremierStatus().hashCode();
        }
        if (getConsumerStatus() != null) {
            _hashCode += getConsumerStatus().hashCode();
        }
        if (getConsumerSubStatus() != null) {
            _hashCode += getConsumerSubStatus().hashCode();
        }
        if (getConsumerBlocked() != null) {
            _hashCode += getConsumerBlocked().hashCode();
        }
        if (getPrimaryPhoneBlocked() != null) {
            _hashCode += getPrimaryPhoneBlocked().hashCode();
        }
        if (getAlternatePhoneBlocked() != null) {
            _hashCode += getAlternatePhoneBlocked().hashCode();
        }
        if (getEmailBlocked() != null) {
            _hashCode += getEmailBlocked().hashCode();
        }
        if (getEmailDomainBlocked() != null) {
            _hashCode += getEmailDomainBlocked().hashCode();
        }
        if (getSecurityQuestionsCollectionDate() != null) {
            _hashCode += getSecurityQuestionsCollectionDate().hashCode();
        }
        if (getSecurityQuestion() != null) {
            _hashCode += getSecurityQuestion().hashCode();
        }
        if (getSecurityAnswer() != null) {
            _hashCode += getSecurityAnswer().hashCode();
        }
        if (getGlobalUniqueId() != null) {
            _hashCode += getGlobalUniqueId().hashCode();
        }
        if (getCreateDate() != null) {
            _hashCode += getCreateDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MGOAttributes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "MGOAttributes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("premierStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "premierStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerSubStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerSubStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "consumerBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryPhoneBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "primaryPhoneBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternatePhoneBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "alternatePhoneBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "emailBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailDomainBlocked");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "emailDomainBlocked"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("securityQuestionsCollectionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "securityQuestionsCollectionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("securityQuestion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "securityQuestion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("securityAnswer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "securityAnswer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalUniqueId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "globalUniqueId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "createDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
