package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;

public class BlockedPhoneNumberRowMapper extends BaseRowMapper {
	/**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		return rs.getString("BLKD_PH_NBR");
	}
}
