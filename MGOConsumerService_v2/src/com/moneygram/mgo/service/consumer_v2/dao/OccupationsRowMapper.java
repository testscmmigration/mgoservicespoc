package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer_v2.Occupation;

public class OccupationsRowMapper extends BaseRowMapper {

	private static final Logger logger = LogFactory.getInstance().getLogger(BaseRowMapper.class);			
	@Override
	public Object mapRow(ResultSet rs, int rownum) throws SQLException {
		Occupation occupation = new Occupation();
		occupation.setOcupnCode(rs.getLong("OCUPN_CODE"));
		occupation.setOcupnDescription(rs.getString("LOCALE_TEXT"));
		logger.info("Occupation obtained: "+occupation.getOcupnCode() + " -> "+ occupation.getOcupnDescription());
		return occupation;
	}

}
