/*
 * Created on Oct 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegInfo;

/**
 *
 * GetValidateRegInfoRowMapper Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GetValidateRegInfoRowMapper</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/10/22 02:46:33 $ </td><tr><td>
 * @author   </td><td>$Author: vx15 $ </td>
 *</table>
 *</div>
 */
public class GetValidateRegInfoRowMapper extends BaseRowMapper {
	private static final Logger logger = LogFactory.getInstance().getLogger(BaseRowMapper.class);		
	
	private GetValidateRegInfo result;

	@Override
	public Object mapRow(ResultSet rs, int rownum) throws SQLException {

		result = new GetValidateRegInfo();
		
		Long custId = rs.getLong("cust_id");
		Long authnId = rs.getLong("authn_id");
		Date authnDate = rs.getDate("authn_date");
		String scoreNbr = rs.getString("score_nbr");
		String deciBandText = rs.getString("deci_band_text");
		String remarksText = rs.getString("remarks_text");
		String statText = rs.getString("stat_text");
		String chkTypeCode = rs.getString("chk_type_code");
		String chkTypeDesc = rs.getString("chk_type_desc");
		String rsltCode = rs.getString("rslt_code");
		String rsltDesc = rs.getString("rslt_desc");
		String rsltSvrtyText = rs.getString("rslt_svrty_text");
		
		logger.info("GetValidateRegInfoRowMapper.mapRow. custId="+custId);
		
		result.setCustId(custId);
		result.setAuthnId(authnId);
		result.setAuthnDate(authnDate);
		result.setScoreNbr(scoreNbr);
		result.setDeciBandText(deciBandText);
		result.setRemarksText(remarksText);
		result.setStatText(statText);
		result.setChkTypeCode(chkTypeCode);
		result.setChkTypeDesc(chkTypeDesc);
		result.setRsltCode(rsltCode);
		result.setRsltDesc(rsltDesc);
		result.setRsltSvrtyText(rsltSvrtyText);
		
		
		return result;
	}
	
	public GetValidateRegInfo getResult() {
		return result;
	}


}
