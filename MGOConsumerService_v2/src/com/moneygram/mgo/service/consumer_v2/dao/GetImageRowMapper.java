/*
 * Created on Oct 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 *
 * GetImage Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GetImageRowMapper</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/10/22 02:46:33 $ </td><tr><td>
 * @author   </td><td>$Author: vx15 $ </td>
 *</table>
 *</div>
 */
public class GetImageRowMapper extends BaseRowMapper {
	private static final Logger logger = LogFactory.getInstance().getLogger(BaseRowMapper.class);			

	private Object[] result;
	
	
	@Override
	public Object mapRow(ResultSet rs, int rownum) throws SQLException {
		
		Long custExtnlIdentId = rs.getLong("cust_extnl_ident_id");
		Blob encryptIdentDocImgBlob = rs.getBlob("encrypt_ident_doc_img_blob");
		String identDocImgFileName =  rs.getString("ident_doc_img_file_name");
		
		logger.info("custExtnlIdentId= " + custExtnlIdentId+ ", identDocImgFileName= " +identDocImgFileName);
		
		result = new Object[]{custExtnlIdentId, encryptIdentDocImgBlob, identDocImgFileName };
		
		return result;
	}
	
	public Object[] getResult() {
		return result;
	}


}
