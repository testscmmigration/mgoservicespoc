/*
 * Created on Oct 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 *
 * GetImageApprovalStatus Row Mapper.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>GetImageApprovalStatusRowMapper</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2009</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.2 $ $Date: 2011/10/22 02:46:33 $ </td><tr><td>
 * @author   </td><td>$Author: vx15 $ </td>
 *</table>
 *</div>
 */
public class GetImageApprovalStatusRowMapper extends BaseRowMapper {
	private static final Logger logger = LogFactory.getInstance().getLogger(BaseRowMapper.class);		
	
	private Object[] result;

	@Override
	public Object mapRow(ResultSet rs, int rownum) throws SQLException {

		Long custExtnlIdentId = rs.getLong("cust_extnl_ident_id");
		String encrypt_extnl_id = rs.getString("encrypt_extnl_id");
		String mask_extnl_id = rs.getString("mask_extnl_id");
		Long cust_id = rs.getLong("cust_id");
		String ident_doc_bsns_code =  rs.getString("ident_doc_bsns_code");
		String issu_iso_cntry_code =  rs.getString("issu_iso_cntry_code");
		String iso_subdiv_code =  rs.getString("iso_subdiv_code");
		java.util.Date issu_date =  rs.getDate("issu_date");
		java.util.Date exp_date =  rs.getDate("exp_date");
		String encrypt_psprt_mrz_ln2_text =  rs.getString("encrypt_psprt_mrz_ln2_text");
		String bsns_code =  rs.getString("bsns_code");
		

		
		logger.info("encrypt_extnl_id= " + encrypt_extnl_id+ ", cust_id= " +cust_id);
		
		result = new Object[]{custExtnlIdentId,encrypt_extnl_id,mask_extnl_id,cust_id,ident_doc_bsns_code,
				issu_iso_cntry_code,iso_subdiv_code,issu_date,exp_date,encrypt_psprt_mrz_ln2_text,bsns_code };
		
		//TODO: put into a dto
		return result;
	}
	
	public Object[] getResult() {
		return result;
	}


}
