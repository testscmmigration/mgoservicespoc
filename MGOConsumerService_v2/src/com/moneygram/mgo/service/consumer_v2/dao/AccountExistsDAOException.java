/*
 * Created on May 12, 2010
 *
 */
package com.moneygram.mgo.service.consumer_v2.dao;

import com.moneygram.common.dao.DAOException;

/**
 * 
 * Account Exists DAO Exception.
 *<div>
 *<table>
 * <tr><th>Title:</th>    <td>MGOConsumerService</td>
 * <tr><th>Copyright:</th><td>Copyright (c) 2010</td>
 * <tr><th>Company:</th>  <td>MoneyGram</td><tr><td>
 * @version  </td><td>$Revision: 1.1 $ $Date: 2010/05/13 17:30:56 $ </td><tr><td>
 * @author   </td><td>$Author: a700 $ </td>
 *</table>
 *</div>
 */
public class AccountExistsDAOException extends DAOException {
    private static final long serialVersionUID = 2179262573283193196L;

    public AccountExistsDAOException() {
        super();
        
    }

    public AccountExistsDAOException(String code, String message, Throwable cause) {
        super(code, message, cause);
        
    }

    public AccountExistsDAOException(String code, String message) {
        super(code, message);
        
    }

    public AccountExistsDAOException(String message, Throwable cause) {
        super(message, cause);
        
    }

    public AccountExistsDAOException(String message) {
        super(message);
        
    }
    
}
