package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.mgo.shared.ConsumerAddress;

public class ConsumerAddressRowMapper extends BaseRowMapper {
    /**
     *
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
     */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ConsumerAddress ca = new ConsumerAddress();
		ca.setAddressId(new Long(rs.getLong("addr_id")));
		ca.setLine1(rs.getString("ADDR_LINE1_TEXT"));
		ca.setLine2(rs.getString("ADDR_LINE2_TEXT"));
		ca.setLine3(rs.getString("ADDR_LINE3_TEXT"));
		//BEGIN: VL58 - S12 - new fields for Germany site
		ca.setBuildingName(rs.getString("ADDR_BLDG_NAME"));
		//END: VL58 - S12 - new fields for Germany site
		ca.setCounty(rs.getString("ADDR_CNTY_NAME"));
		ca.setCity(rs.getString("ADDR_CITY_NAME"));
		ca.setState(rs.getString("ADDR_STATE_NAME"));
		ca.setZipCode(rs.getString("ADDR_POSTAL_CODE"));
		ca.setCountry(rs.getString("ADDR_CNTRY_ID"));
		return ca;
	}
}
