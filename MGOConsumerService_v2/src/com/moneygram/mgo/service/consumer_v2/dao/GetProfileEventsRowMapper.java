package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.common.dao.DAOUtils;
import com.moneygram.mgo.service.consumer_v2.ProfileEvent;

public class GetProfileEventsRowMapper extends BaseRowMapper {
    /**
	 * 
	 * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProfileEvent event = new ProfileEvent();
		event.setEventDateTime(DAOUtils.toCalendar(rs.getTimestamp("ACTY_DATE")));
        event.setEvent(rs.getString("ACTY_LOG_BSNS_CODE"));
		return event;
	}

}
