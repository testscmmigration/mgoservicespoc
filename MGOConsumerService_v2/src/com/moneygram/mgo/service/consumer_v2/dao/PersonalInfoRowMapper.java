package com.moneygram.mgo.service.consumer_v2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.moneygram.common.dao.BaseRowMapper;
import com.moneygram.mgo.service.consumer_v2.PersonalInfo;

public class PersonalInfoRowMapper extends BaseRowMapper {
	
	private PersonalInfo personalInfo;
	
	public PersonalInfoRowMapper(PersonalInfo current){
		personalInfo = current;
	}
	/**
     *
     * @see com.moneygram.common.dao.BaseRowMapper#mapRow(java.sql.rs, int)
     */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		if (personalInfo == null){
			personalInfo = new PersonalInfo();
		}
		personalInfo.setExternalIdType(rs.getString("ident_doc_bsns_code"));
		personalInfo.setExternalId(rs.getString("encrypt_extnl_id"));
		personalInfo.setExternalIdCountryOfIssuance(rs.getString("ISSU_ISO_CNTRY_CODE"));
		personalInfo.setExternalIdDateOfIssuance(rs.getDate("ISSU_DATE"));
		personalInfo.setExternalIdDateOfExpiration(rs.getDate("EXP_DATE"));
		personalInfo.setExternalIdStateOfIssuance(rs.getString("ISO_SUBDIV_CODE"));
		
		return personalInfo;
	}
}
