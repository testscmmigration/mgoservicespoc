package com.moneygram.mgo.service.consumer_v2.gcpservice.proxy;

import com.moneygram.gcp.service_v1.CreateConsumerProfileRequest;
import com.moneygram.gcp.service_v1.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.ProxyException;



public interface GCPServiceProxy {

    public long getConsumerProfileID() throws ProxyException;

	public long createGCPProfile(CreateConsumerProfileRequest request)throws ProxyException;

	public long updateGCPProfile(UpdateConsumerProfileRequest request)throws ProxyException;
}
