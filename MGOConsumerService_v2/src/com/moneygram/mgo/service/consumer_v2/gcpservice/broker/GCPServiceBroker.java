package com.moneygram.mgo.service.consumer_v2.gcpservice.broker;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.gcp.service_v1.CreateConsumerProfileRequest;
import com.moneygram.gcp.service_v1.DynamicField;
import com.moneygram.gcp.service_v1.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.GCPServiceException;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.ProxyException;
import com.moneygram.mgo.service.consumer_v2.gcpservice.proxy.GCPServiceProxy;
import com.moneygram.mgo.service.consumer_v2.gcpservice.proxy.GCPServiceProxyImpl;
import com.moneygram.mgo.service.consumer_v2.util.MGOConsumerServiceResourceConfig;

public class GCPServiceBroker {
	
	public static MGOConsumerServiceResourceConfig getResourceConfig() {
		return MGOConsumerServiceResourceConfig.getInstance();
	}
	private static final Logger log = LogFactory.getInstance().getLogger(GCPServiceBroker.class);
	private GCPServiceProxy gcpServiceProxy=new GCPServiceProxyImpl();
	
	private int gcpRetryTimes = getResourceConfig().getGCPServiceCallRetryTimes();
    private int gcpRetryWaitTime = getResourceConfig().getGCPServiceCallRetryWaitPeriod();
	
	
	
	public long getGCPID() throws Exception{
		// Call GCP service to retrieve GCP consumer ID, get number of retry times for connection in case the
        // service is down
        long gcpId = -1;
        boolean gcpServiceOK = false;
        try {
        	int counter=0;
            while ((counter <= gcpRetryTimes) && !gcpServiceOK) {
                try {
                	
                	log.debug("Entry inside GCPServiceBroker getGCPID " );
                    gcpId = gcpServiceProxy.getConsumerProfileID();
                    gcpServiceOK = true;
                } catch (ProxyException p) {
                    log.error("Error calling GCP service: " + p.getMessage(), p);
                    log.debug("Calling GCP Service, retry number: " + counter);
                    // Wait "gcpRetryWaitTime" seconds and retry again the calling
                    counter++;
                    if(gcpRetryTimes > 0){
                    	Thread.sleep(gcpRetryWaitTime *1000);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in getGCPID: " + e.getMessage(), e);
        }

        if (!gcpServiceOK) {
            // if GCP service didn�t return the id, throw an exception
        	log.error("GCP Service down, could not proceed with consumer profile creation");
        	throw new GCPServiceException("GCP Service down, could not proceed with consumer profile creation");
        } 
        return gcpId;
		
	}

	public static  DynamicField[] generateGCPDynamicFields(String email)
 {
		int size = 2;
		DynamicField[] dynamicFields = new DynamicField[size];
		DynamicField dynamicField;
		dynamicField = new DynamicField();
		dynamicField.setName("tranNoification.eMail");
		dynamicField.setValue(email);
		dynamicFields[0] = dynamicField;

		dynamicField = new DynamicField();
		dynamicField.setName("profile.channel.type");
		dynamicField.setValue("MGO");
		dynamicFields[1] = dynamicField;
		
		return dynamicFields;
	
	}
	
  
	
	public long createGCPProfile(CreateConsumerProfileRequest request) throws GCPServiceException
	{
		long gcpId = -1;
        boolean gcpServiceOK = false;
        try {
        	int counter=0;
        	
        	log.debug("Entry inside GCPServiceBroker createGCPProfile " );
            while ((counter <= gcpRetryTimes) && !gcpServiceOK) {
                try {
                    gcpId = gcpServiceProxy.createGCPProfile(request);
                    gcpServiceOK = true;
                    log.debug("gcpId"+ gcpId);
                } catch (ProxyException p) {
                    log.error("Error calling GCP service: " + p.getMessage(), p);
                    log.debug("Calling GCP Service, retry number: " + counter);
                    // Wait "gcpRetryWaitTime" seconds and retry again the calling
                    counter++;
                    if(gcpRetryTimes > 0){
                    	Thread.sleep(gcpRetryWaitTime *1000);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in getGCPID: " + e.getMessage(), e);
        }

        if (!gcpServiceOK) {
            // if GCP service didn�t return the id, throw an exception
        	log.error("GCP Service down, could not proceed with consumer profile creation");
        	throw new GCPServiceException("GCP Service down, could not proceed with consumer profile creation");
        } 
        return gcpId;
	}
	public long updateGCPProfile(UpdateConsumerProfileRequest  request) throws GCPServiceException
	{
		long gcpId = -1;
        boolean gcpServiceOK = false;
        try {
        	int counter=0;
            while ((counter <= gcpRetryTimes) && !gcpServiceOK) {
                try {
                    log.debug("Entry inside GCPServiceBroker updateGCPProfile");
                    gcpId = gcpServiceProxy.updateGCPProfile(request);
                    gcpServiceOK = true;
                    log.debug("gcpId"+ gcpId);
                } catch (ProxyException p) {
                    log.error("Error calling GCP service: " + p.getMessage(), p);
                    log.debug("Calling GCP Service, retry number: " + counter);
                    // Wait "gcpRetryWaitTime" seconds and retry again the calling
                    counter++;
                    if(gcpRetryTimes > 0){
                    	Thread.sleep(gcpRetryWaitTime *1000);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in getGCPID: " + e.getMessage(), e);
        }

        if (!gcpServiceOK) {
            // if GCP service didn�t return the id, throw an exception
        	log.error("GCP Service down, could not proceed with consumer profile creation");
        	throw new GCPServiceException("GCP Service down, could not proceed with consumer profile creation");
        } 
        return gcpId;
	}
}
