package com.moneygram.mgo.service.consumer_v2.gcpservice.proxy;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.gcp.service_v1.CreateConsumerProfileRequest;
import com.moneygram.gcp.service_v1.CreateConsumerProfileResponse;
import com.moneygram.gcp.service_v1.GCPProxy;
import com.moneygram.gcp.service_v1.GetConsumerProfileIDRequest;
import com.moneygram.gcp.service_v1.GetConsumerProfileIDResponse;
import com.moneygram.gcp.service_v1.ServiceAction;
import com.moneygram.gcp.service_v1.UpdateConsumerProfileRequest;
import com.moneygram.gcp.service_v1.UpdateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.ProxyException;
import com.moneygram.mgo.service.consumer_v2.util.MGOConsumerServiceResourceConfig;



public class GCPServiceProxyImpl
        implements GCPServiceProxy {

	private static final Logger log = LogFactory.getInstance().getLogger(GCPServiceProxyImpl.class);
	
	public static MGOConsumerServiceResourceConfig getResourceConfig() {
		return MGOConsumerServiceResourceConfig.getInstance();
	}
	
    private GCPProxy gcpServiceClient;
    
    public GCPServiceProxyImpl(){
    	try{  
    		String endpoint=getResourceConfig().getGCPServiceUrl();
    		String timeout=getResourceConfig().getGCPServiceTimeOut();
    		gcpServiceClient=new GCPProxy(endpoint);
    		log.debug("GCP Service url: "+endpoint);
    	} catch (Exception ex) {
            log.error("Error in GCPServiceProxyImpl: " + ex.getMessage(), ex);
        }
    }
    
 
    
    
    public long getConsumerProfileID() throws ProxyException {
        long gcpId = -1;
        try {
            GetConsumerProfileIDRequest request = new GetConsumerProfileIDRequest();
            Header header = new Header();
            ProcessingInstruction processingInstr = new ProcessingInstruction();
            header.setProcessingInstruction(processingInstr);
            request.setHeader(header);
            request.setHeader(getHeader("GetConsumerProfileID"));
            GetConsumerProfileIDResponse response = gcpServiceClient.getConsumerProfileID(request);
            gcpId = response.getProfileId();
            log.debug("GCP consumer profile id: " + gcpId);
        } catch (Exception ex) {
            log.error("Error in getConsumerProfileID: " + ex.getMessage(), ex);
            throw new ProxyException("Failed to obtain GCP consumer profile id", ex);
        }
        return gcpId;
    }
    
	private Header getHeader(String action) throws ProxyException {
		Header header = new Header();
		ClientHeader cheader = new ClientHeader();
		ProcessingInstruction processingInstr = new ProcessingInstruction();
		cheader.setClientName("MGO");
		header.setClientHeader(cheader);
		processingInstr.setAction(action);
		header.setProcessingInstruction(processingInstr);
		return header;
	}
    public long createGCPProfile(CreateConsumerProfileRequest request) throws ProxyException
    {
    	 long gcpId = -1;
         try {
             request.setHeader(getHeader("CreateConsumerProfile"));
//           request.setHeader(getHeader(ServiceAction._createConsumerProfile));
             CreateConsumerProfileResponse response = gcpServiceClient.createConsumerProfile(request);
             gcpId=response.getConsumerProfile().getConsumerId();
             log.debug("GCP consumer profile id: " + gcpId);
         } catch (Exception ex) {
             log.error("Error in getConsumerProfileID: " + ex.getMessage(), ex);
             throw new ProxyException("Failed to obtain GCP consumer profile id", ex);
         }
         return gcpId;		
    	
    }
    
    public long updateGCPProfile (UpdateConsumerProfileRequest request) throws ProxyException
    {
    	 long gcpId = -1;
         try {            
//             request.setHeader(getHeader(ServiceAction._updateConsumerProfile));
             request.setHeader(getHeader("UpdateConsumerProfile"));
             UpdateConsumerProfileResponse response = gcpServiceClient.updateConsumerProfile(request);
             gcpId=response.getConsumerProfile().getConsumerId();
             log.debug("GCP consumer profile id: " + gcpId);
         } catch (Exception ex) {
             log.error("Error in getConsumerProfileID: " + ex.getMessage(), ex);
             throw new ProxyException("Failed to obtain GCP consumer profile id", ex);
         }
         return gcpId;		
    	
    }
 
}
