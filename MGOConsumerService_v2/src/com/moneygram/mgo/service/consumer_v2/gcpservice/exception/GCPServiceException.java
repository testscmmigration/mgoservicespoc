
 
package com.moneygram.mgo.service.consumer_v2.gcpservice.exception;

public class GCPServiceException
        extends Exception {
    private static final long serialVersionUID = -6515022454705095520L;

    public GCPServiceException() {
        super();

    }

    public GCPServiceException(String message, Throwable cause) {
        super(message, cause);

    }

    public GCPServiceException(String message) {
        super(message);

    }

    public GCPServiceException(Throwable cause) {
        super(cause);

    }

}
