/**
 * UpdateConsumerIDImageStatusRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.moneygram.mgo.service.consumer_v2;

public class UpdateConsumerIDImageStatusRequest  extends com.moneygram.common.service.BaseOperationRequest  implements java.io.Serializable {
    
	private Long custExtnlIdentId;
	private Long custId;
	private String identDocStatBsnsCode;

    public UpdateConsumerIDImageStatusRequest() {
    }

    public UpdateConsumerIDImageStatusRequest(
    		com.moneygram.common.service.RequestHeader header,
           long custExtnlIdentId, long custId, String identDocStatBsnsCode) {
        super(header);
        this.custExtnlIdentId = custExtnlIdentId;
        this.custId = custId;
        this.identDocStatBsnsCode = identDocStatBsnsCode;
    }


    

    public Long getCustExtnlIdentId() {
		return custExtnlIdentId;
	}
    
    @Deprecated
	public void setCustExtnlIdentId(Long custExtnlIdentId) {
		this.custExtnlIdentId = custExtnlIdentId;
	}

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getIdentDocStatBsnsCode() {
		return identDocStatBsnsCode;
	}

	public void setIdentDocStatBsnsCode(String identDocStatBsnsCode) {
		this.identDocStatBsnsCode = identDocStatBsnsCode;
	}

	


    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((custExtnlIdentId == null) ? 0 : custExtnlIdentId.hashCode());
		result = prime * result + ((custId == null) ? 0 : custId.hashCode());
		result = prime
				* result
				+ ((identDocStatBsnsCode == null) ? 0 : identDocStatBsnsCode
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateConsumerIDImageStatusRequest other = (UpdateConsumerIDImageStatusRequest) obj;
		if (custExtnlIdentId == null) {
			if (other.custExtnlIdentId != null)
				return false;
		} else if (!custExtnlIdentId.equals(other.custExtnlIdentId))
			return false;
		if (custId == null) {
			if (other.custId != null)
				return false;
		} else if (!custId.equals(other.custId))
			return false;
		if (identDocStatBsnsCode == null) {
			if (other.identDocStatBsnsCode != null)
				return false;
		} else if (!identDocStatBsnsCode.equals(other.identDocStatBsnsCode))
			return false;
		return true;
	}




	// Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateConsumerIDImageStatusRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "UpdateConsumerIDImageStatusRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custExtnlIdentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "custExtnlIdentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "custId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identDocStatBsnsCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://moneygram.com/mgo/service/consumer_v2", "identDocStatBsnsCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
