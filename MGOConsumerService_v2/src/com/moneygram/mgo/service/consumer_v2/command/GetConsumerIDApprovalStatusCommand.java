/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.CustomerExternalIdentificationInformation;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class GetConsumerIDApprovalStatusCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetConsumerIDApprovalStatusCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetConsumerIDApprovalStatusRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		GetConsumerIDApprovalStatusRequest concreteRequest = (GetConsumerIDApprovalStatusRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + GetConsumerIDApprovalStatusCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("GetConsumerIDApprovalStatusCommand is missing");
        if (concreteRequest.getCustId() <=0 )
            throw new DataFormatException("GetConsumerIDApprovalStatusCommand CustId is missing");

        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        
        long custId = concreteRequest.getCustId();
        String encryptExtnlId = concreteRequest.getEncryptExtnlId();
        String maskExtnlId = concreteRequest.getMaskExtnlId();
        
        
        CustomerExternalIdentificationInformation ceii = dao.getCustomerExternalIdentificationInformation(custId, encryptExtnlId , maskExtnlId);
        
        GetConsumerIDApprovalStatusResponse response = new GetConsumerIDApprovalStatusResponse();
        if(ceii != null && ceii.getBsnsCode() != null) {
        	response.setStatus(ceii.getBsnsCode());	//current code
        } else {
        	response.setStatus("null");
        }
        
        
		return response;
	}
	
	

}
