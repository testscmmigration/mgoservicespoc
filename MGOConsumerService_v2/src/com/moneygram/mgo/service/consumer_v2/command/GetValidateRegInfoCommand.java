package com.moneygram.mgo.service.consumer_v2.command;

import java.util.Calendar;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegInfo;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationRequest;
import com.moneygram.mgo.service.consumer_v2.GetValidateRegistrationInformationResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class GetValidateRegInfoCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetValidateRegInfoCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetValidateRegistrationInformationRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		GetValidateRegistrationInformationRequest concreteRequest = (GetValidateRegistrationInformationRequest) request;
		

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + GetValidateRegInfoCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("GetValidateRegInfoRequest is missing");
        if (concreteRequest.getCustId() <=0 )
            throw new DataFormatException("GetValidateRegInfoRequest custId is missing");

        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        GetValidateRegInfo data = dao.getValidateRegistrationInformation(concreteRequest.getCustId());
        
        GetValidateRegistrationInformationResponse response = new GetValidateRegistrationInformationResponse();
        
        Calendar c = Calendar.getInstance();
        if(data == null) {
        	logger.debug("No data found");
        	response.setCustId(-1L);
        	response.setAuthnDate(c);
        	return response;
        }
        if(data.getAuthnDate()!= null)
        	c.setTime(data.getAuthnDate());
        response.setAuthnDate(c);
        response.setAuthnId(data.getAuthnId());
        response.setChkTypeCode(data.getChkTypeCode());
        response.setChkTypeDesc(data.getChkTypeDesc());
        response.setCustId(data.getCustId());
        response.setDeciBandText(data.getDeciBandText());
        response.setRemarksText(data.getRemarksText());
        response.setRsltCode(data.getRsltCode());
        response.setRsltDesc(data.getRsltDesc());
        response.setRsltSvrtyText(data.getRsltSvrtyText());
        response.setScoreNbr(data.getScoreNbr());
        
        
        return response;
	}
	
	

}
