/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.AddImageRequest;
import com.moneygram.mgo.service.consumer_v2.AddImageResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class AddIDImageToConsumerCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			AddIDImageToConsumerCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof AddImageRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		AddImageRequest concreteRequest = (AddImageRequest) request;
		

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + AddImageRequest.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("AddImageRequest is missing");
        if (concreteRequest.getCustomerExternalId() <=0 )
            throw new DataFormatException("AddImageRequest customerExternalId is missing");
        if (concreteRequest.getFileBytes() == null )
            throw new DataFormatException("AddImageRequest FileBytes is missing");
        if (concreteRequest.getImageName() == null )
            throw new DataFormatException("AddImageRequest imageName is missing");
        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        boolean txOk = dao.addImage(concreteRequest.getImageName(), concreteRequest.getFileBytes(), concreteRequest.getCustomerExternalId());
        
        AddImageResponse response = new AddImageResponse();
        response.setImageOK(txOk);
        
		return response;
	}
	
	

}
