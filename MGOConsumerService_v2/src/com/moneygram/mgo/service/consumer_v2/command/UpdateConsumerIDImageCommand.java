/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class UpdateConsumerIDImageCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			UpdateConsumerIDImageCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof UpdateConsumerIDImageRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		UpdateConsumerIDImageRequest concreteRequest = (UpdateConsumerIDImageRequest) request;
		

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + UpdateConsumerIDImageCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("UpdateConsumerIDImageRequest is missing");
        if (concreteRequest.getConsumerId() <=0 )
            throw new DataFormatException("UpdateConsumerIDImageRequest consumer ID is missing");
        if (concreteRequest.getFileBytes() == null )
            throw new DataFormatException("UpdateConsumerIDImageRequest FileBytes is missing");
        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        boolean txOk = dao.addImage(concreteRequest.getImageName(), concreteRequest.getFileBytes(), concreteRequest.getConsumerId());
        
        UpdateConsumerIDImageResponse response = new UpdateConsumerIDImageResponse();
        response.setImageOK(txOk);
        
		return response;
	}
	
	

}
