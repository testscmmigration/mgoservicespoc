/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import java.math.BigDecimal;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueRequest;
import com.moneygram.mgo.service.consumer_v2.GetCCLowAuthSequencerValueResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class GetCCLowAuthSequencerValueCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetCCLowAuthSequencerValueCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetCCLowAuthSequencerValueRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		GetCCLowAuthSequencerValueRequest concreteRequest = (GetCCLowAuthSequencerValueRequest) request;
		

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + GetCCLowAuthSequencerValueCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("GetCCLowAuthSequencerValueRequest is missing");
        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        BigDecimal nsv = dao.getCCNextSeqValue();

        
        if (nsv == null)
            throw new DataFormatException("CC Next Seq Value is null");
        GetCCLowAuthSequencerValueResponse response = new GetCCLowAuthSequencerValueResponse();
        response.setSeqValue(nsv.toString());
		return response;
	}
	
	

}
