/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerIDImageStatusResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class UpdateConsumerIDImageStatusCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			UpdateConsumerIDImageStatusCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof UpdateConsumerIDImageStatusRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		UpdateConsumerIDImageStatusRequest concreteRequest = (UpdateConsumerIDImageStatusRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + UpdateConsumerIDImageStatusCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("UpdateConsumerIDImageStatusCommand is missing");
        if (concreteRequest.getCustId() <=0 )
            throw new DataFormatException("UpdateConsumerIDImageStatusCommand CustId is missing");
        if (concreteRequest.getIdentDocStatBsnsCode() == null )
            throw new DataFormatException("UpdateConsumerIDImageStatusCommand IdentDocStatBsnsCode is missing");
        if (concreteRequest.getCustExtnlIdentId()!= null && concreteRequest.getCustExtnlIdentId() > 0 )
            throw new DataFormatException("UpdateConsumerIDImageStatusCommand CustExtnlIdentId is deprecated, value must be null");
        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        boolean txOK = dao.updateConsumerIDImageStatus(concreteRequest.getCustId(), concreteRequest.getIdentDocStatBsnsCode());
        
        UpdateConsumerIDImageStatusResponse response = new UpdateConsumerIDImageStatusResponse();
        response.setStatusOK(txOK);
        
		return response;
	}
	
	

}
