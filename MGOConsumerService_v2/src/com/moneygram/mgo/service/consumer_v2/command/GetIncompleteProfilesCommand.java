/*
 * Created on Aug 28, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import java.util.ArrayList;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesRequest;
import com.moneygram.mgo.service.consumer_v2.GetIncompleteProfilesResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;

public class GetIncompleteProfilesCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetIncompleteProfilesCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetIncompleteProfilesRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetIncompleteProfilesRequest profileRequest = (GetIncompleteProfilesRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: GetIncompleteProfilesRequest: " + profileRequest.toString());
		}
		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		List<Long> list = null;
	
		try {
			list = dao.getIncompleteProfiles(profileRequest.getStartDateTime(), profileRequest
					.getEndDateTime(), null, profileRequest.getMaxResults());
		} catch (Exception e) {
			logger.warn("process: failed to get incomplete profiles", e);
			throw new CommandException("Failed to get incomplete profiles", e);
		}
		GetIncompleteProfilesResponse response = new GetIncompleteProfilesResponse();
		response.setConsumerId(convertListToPrimitiveLongArray(list));

		return response;
}

	private long[] convertListToPrimitiveLongArray(List<Long> list) {
		if (list == null || list.size() == 0) {
			return new long[0];
		}
		long[] longArray = new long[list.size()];

		int i = 0;
		for (Long l : list) {
			longArray[i] = l.longValue();
			i++;
		}
		return longArray;
	}
}
