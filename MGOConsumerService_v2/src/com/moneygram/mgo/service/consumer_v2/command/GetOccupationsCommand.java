package com.moneygram.mgo.service.consumer_v2.command;

import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.FindConsumersRequest;
import com.moneygram.mgo.service.consumer_v2.GetOccupationsListRequest;
import com.moneygram.mgo.service.consumer_v2.GetOccupationsListResponse;
import com.moneygram.mgo.service.consumer_v2.Occupation;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;

public class GetOccupationsCommand extends ReadCommand {
	
	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetOccupationsCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
		throws CommandException {
		return request instanceof GetOccupationsListRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		GetOccupationsListRequest occupationsRequest = (GetOccupationsListRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: getOccupationsList isoCountryCode="
					+ occupationsRequest.getIsoCountryCode()
					+ ", languageTagText=" + occupationsRequest.getLanguageTagText()) ;
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		List<Occupation> list = null;
		try {
			String isoCountryCode=occupationsRequest.getIsoCountryCode();
			String languageTagText=occupationsRequest.getLanguageTagText();
			list = dao.getOccupationsList(isoCountryCode, languageTagText);
		} catch (Exception e) {
			logger.warn("process: failed to find occupations", e);
			throw new CommandException("Failed to find occupations", e);
		}

		GetOccupationsListResponse response = new GetOccupationsListResponse();
		if (list == null)
			response.setOccupations(new Occupation[0]);
		else
			response.setOccupations((Occupation[]) list.toArray(new Occupation[list
					.size()]));
		return response;
	}

}
