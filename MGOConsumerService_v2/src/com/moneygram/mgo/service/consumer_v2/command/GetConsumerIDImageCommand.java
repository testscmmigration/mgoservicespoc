/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class GetConsumerIDImageCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetConsumerIDImageCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof GetConsumerIDImageRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request) throws CommandException {
		GetConsumerIDImageRequest concreteRequest = (GetConsumerIDImageRequest) request;
		

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + GetConsumerIDImageCommand.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("GetConsumerIDImageRequest is missing");
        if (concreteRequest.getCustomerExternalId() <=0 )
            throw new DataFormatException("GetConsumerIDImageRequest customerId is missing");

        
        ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
        Object[] data = dao.getImage(concreteRequest.getCustomerExternalId());
        GetConsumerIDImageResponse response = new GetConsumerIDImageResponse();
        
        if(data != null) {
        	if(data[0]!= null) {
        		response.setIdentId((Long)data[0]);	
        	}
        	if(data[1] != null) {
        		response.setImageBytes((byte[])data[1]);
        	}
            if(data[2] != null ) {
            	response.setImageName((String)data[2]);
            }
        } else {
        	throw new CommandException("No data returned");
        }
                
		return response;
	}
	
	

}
