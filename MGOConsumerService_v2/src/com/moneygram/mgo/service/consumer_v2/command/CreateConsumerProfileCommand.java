/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.common.util.StringUtility;
import com.moneygram.gcp.service_v1.ConsumerProfile;
import com.moneygram.gcp.service_v1.ConsumerProfileResponseFilter;
import com.moneygram.gcp.service_v1.PersonalInfo;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer_v2.gcpservice.broker.GCPServiceBroker;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.GCPServiceException;
import com.moneygram.mgo.service.consumer_v2.util.ConsumerUtil;
import com.moneygram.mgo.service.consumer_v2.util.PartnerSiteIdentifier;
import com.moneygram.shared.entity_v1.ConsumerAddress;
import com.moneygram.shared.entity_v1.ConsumerName;

public class CreateConsumerProfileCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			CreateConsumerProfileCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof CreateConsumerProfileRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		CreateConsumerProfileRequest profileRequest = (CreateConsumerProfileRequest) request;
		Consumer consumer = profileRequest.getConsumer();

		if (logger.isDebugEnabled()) {
			logger.debug("process: create consumer profile consumer="
					+ consumer);
		}

		if (consumer == null)
            throw new DataFormatException("Consumer element is missing");
        if (consumer.getPersonal() == null)
            throw new DataFormatException("Consumer personal element is missing");
        if (consumer.getAddress() == null)
            throw new DataFormatException("Consumer personal address element is missing");
        if (consumer.getContact() == null)
            throw new DataFormatException("Consumer contact element is missing");
        if (consumer.getInternal() == null)
            throw new DataFormatException("Consumer internal element is missing");

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		Long consumerId = null;
		try {
			//Begin : vl58 - S8 (New Architecture) Call GCP service for retrieving GCP id
			Long gcpId=null;
		
			gcpId=consumer.getCnsmrProfileId();
//			gcpId = getGCPId();
			 gcpId = createGCPProfile(gcpId, consumer);
			//End : vl58 - S8 (New Architecture) Call GCP service for retrieving GCP id
			consumerId = dao.addConsumerProfile(consumer,gcpId);
		} catch (Exception e) {
			throw new CommandException("Failed to add consumer profile", e);
		}
		consumer.setConsumerId(consumerId);

		Long addressId = null;
		try {
			addressId = ConsumerUtil.getAddressId(consumerId, dao);
		} catch (Exception e) {
			throw new CommandException("Failed to get address id", e);
		}
		consumer.getAddress().setAddressId(addressId);

		if (consumer.getAccounts() != null && consumer.getAccounts().length > 0) {
			String consumerName = null;
			try {
				consumerName = ConsumerUtil.getConsumerName(consumerId, dao);
			} catch (Exception e) {
				throw new CommandException("Failed to get consumer name", e);
			}

			for (int i = 0; i < consumer.getAccounts().length; i++) {
				ConsumerUtil.checkNewAccountValidity(consumer.getAccounts()[i]);
				try {
					ConsumerUtil.addAccount(consumerId, addressId, consumer
							.getAccounts()[i], dao, consumerName);
				} catch (Exception e) {
					throw new CommandException("Failed to add account", e);
				}
			}
		}

		if (consumer.getAccess() != null
				&& consumer.getAccess().getPasswordHash() != null) {
			try {
				dao.addConsumerPasswordHash(consumer.getConsumerId(), consumer
						.getAccess().getPasswordHash());
			} catch (Exception e) {
				throw new CommandException("Failed to add password hash", e);
			}
		}

		if (consumer.getLoyalty() != null
				&& consumer.getLoyalty().getAutoEnroll() != null
				&& consumer.getLoyalty().getMemberId() != null) {
			try {
				dao.updateConsumerLoyalty(consumer.getConsumerId(), consumer
						.getLoyalty().getAutoEnroll(), consumer.getLoyalty()
						.getMemberId());
			} catch (Exception e) {
				throw new CommandException("Failed to update loyalty info", e);
			}
		}

		if (consumer.getInternal() != null
				&& consumer.getInternal().getSecurityQuestionsCollectionDate() != null) {
			try {
				dao.updateConsumerSecurityQuestionsCollectionDate(consumer.getConsumerId(),
						consumer.getInternal().getSecurityQuestionsCollectionDate().getTime());
			} catch (Exception e) {
				throw new CommandException("Failed to update consumer security info", e);
			}
		}

		CreateConsumerProfileResponse response = new CreateConsumerProfileResponse();
		response.setConsumer(consumer);
		return response;
	}
	
	private long getGCPId() throws GCPServiceException{
		long gcpId=-1;
		try{
			GCPServiceBroker gcpServiceBroker=new GCPServiceBroker();
			gcpId=gcpServiceBroker.getGCPID();
		}catch(GCPServiceException ex){
			logger.error("Error in getGCPId: "+ex.getMessage(),ex);
			throw ex;
		}catch(Exception ex){
			logger.error("Error in getGCPId: "+ex.getMessage(),ex);
		}
		return gcpId;
	}

	private long createGCPProfile(Long gcpId, Consumer consumer) {

		GCPServiceBroker gcpServiceBroker=new GCPServiceBroker();
		com.moneygram.gcp.service_v1.CreateConsumerProfileRequest request = new com.moneygram.gcp.service_v1.CreateConsumerProfileRequest();
		ConsumerName consumerName= new ConsumerName();
		ConsumerProfile consumerProfile = new ConsumerProfile();
		PersonalInfo personalInfo = new PersonalInfo();
		ConsumerAddress address = new ConsumerAddress();
	    ConsumerProfileResponseFilter consumerProfileResponseFilter = new ConsumerProfileResponseFilter();
	    String language = null;

		if (consumer.getSourceSite()
				.equalsIgnoreCase(PartnerSiteIdentifier.MGO)
				|| consumer.getSourceSite().equalsIgnoreCase(
						PartnerSiteIdentifier.WAP)) { // = MGO US or WAP
			language = "en-US";
		} else if (consumer.getSourceSite().equalsIgnoreCase(
				PartnerSiteIdentifier.MGOUK)) {
			language = "en-GB";
		} else {
			
		if(consumer.getContact() != null && consumer.getContact().getPrefCommLanguage()!= null && consumer.getContact().getPrefCommLanguage().getLanguageTagText()!= null){				
			language = consumer.getContact().getPrefCommLanguage().getLanguageTagText();
		     }			
			else
			{
			language = "en-US";
			}
			
		}
		personalInfo.setLanguage(language);		
		consumerName.setFirstName(consumer.getPersonal().getFirstName());
		consumerName.setLastName(consumer.getPersonal().getLastName());
		personalInfo.setName(consumerName);
		personalInfo.setEmailAddress(consumer.getLoginId());
		address.setCountry(consumer.getAddress().getCountry());
		consumerProfile.setAddress(address);		
		consumerProfile.setPersonalInfo(personalInfo);
		request.setConsumerProfile(consumerProfile);
		request.getConsumerProfile().setConsumerId(gcpId); //gcpId would be null for MGODE
		request.setResponseFilter(consumerProfileResponseFilter);
	    request.getConsumerProfile().setDynamicFields(GCPServiceBroker.generateGCPDynamicFields(consumer.getLoginId()));

	try {
		gcpId= gcpServiceBroker.createGCPProfile(request);
	} catch (GCPServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}  //returns the gcpProfileId
	return gcpId;

		}
}
