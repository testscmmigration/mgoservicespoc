/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.ProfileEvent;
import com.moneygram.mgo.service.consumer_v2.SaveProfileEventRequest;
import com.moneygram.mgo.service.consumer_v2.SaveProfileEventResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;

public class SaveProfileEventCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			SaveProfileEventCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof SaveProfileEventRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		SaveProfileEventRequest savePERequest = (SaveProfileEventRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: save profile event - SaveProfileEventRequest="
					+ savePERequest);
		}

		if (savePERequest == null)
            throw new DataFormatException("SaveProfileEventRequest is null");
		if (savePERequest.getConsumerId() <= 0)
            throw new DataFormatException("consumer Id invalid value");		
        if (savePERequest.getEvent() == null)
            throw new DataFormatException("Profile Event is missing");

        ProfileEvent event = savePERequest.getEvent();
		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();
		try {
			dao.saveProfileEvent(savePERequest.getConsumerId(), event);
		} catch (Exception e) {
			throw new CommandException("Failed to save profile event", e);
		}		

		SaveProfileEventResponse response = new SaveProfileEventResponse();		
		return response;
	}

}
