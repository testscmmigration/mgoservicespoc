/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.BusinessRulesException;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.AddAccountRequest;
import com.moneygram.mgo.service.consumer_v2.AddAccountResponse;
import com.moneygram.mgo.service.consumer_v2.ConsumerFIAccount;
import com.moneygram.mgo.service.consumer_v2.dao.AccountExistsDAOException;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer_v2.util.ConsumerUtil;
import com.moneygram.mgo.shared.ErrorCodes;

public class AddAccountCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			AddAccountCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof AddAccountRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		AddAccountRequest profileRequest = (AddAccountRequest) request;
		Long consumerId = new Long(profileRequest.getConsumerId());
		ConsumerFIAccount account = profileRequest.getConsumerFIAccount();

		if (logger.isDebugEnabled()) {
			logger.debug("process: add account consumerId=" + consumerId
					+ " account=" + account);
		}

		if (account == null) {
			logger.debug("process: invalid input account");
			throw new DataFormatException("Invalid input account");
		}
		if (consumerId == null) {
			logger.debug("process: invalid input consumer id");
			throw new DataFormatException("Invalid input consumer id");
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		Long addressId = null;
		try {
			addressId = ConsumerUtil.getAddressId(consumerId, dao);
		} catch (Exception e) {
			logger.warn("process: failed to get address id", e);
			throw new CommandException("Failed to get address id", e);
		}

		String consumerName = null;
		try {
			consumerName = ConsumerUtil.getConsumerName(consumerId, dao);
		} catch (Exception e) {
			logger.warn("process: failed to get consumer name", e);
			throw new CommandException("Failed to get consumer name", e);
		}

		try {
			ConsumerUtil.addAccount(consumerId, addressId, account, dao,
					consumerName);
        } catch (AccountExistsDAOException e) {
            throw new BusinessRulesException(
                    ErrorCodes.ACCOUNT_ALREADY_EXISTS, 
                    ErrorCodes.getMessage(ErrorCodes.ACCOUNT_ALREADY_EXISTS), 
                    e);
		} catch (Exception e) {
			logger.info("process: failed to add account", e);
			throw new CommandException("Failed to add account", e);
		}

		AddAccountResponse response = new AddAccountResponse();
		response.setConsumerFIAccount(account);
		return response;
	}

}
