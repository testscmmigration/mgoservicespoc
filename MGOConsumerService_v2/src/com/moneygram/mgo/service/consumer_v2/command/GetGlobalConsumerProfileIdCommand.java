package com.moneygram.mgo.service.consumer_v2.command;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.CreateConsumerProfileResponse;
import com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdRequest;
import com.moneygram.mgo.service.consumer_v2.GetGlobalConsumerProfileIdResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
import com.moneygram.mgo.service.consumer_v2.gcpservice.broker.GCPServiceBroker;
import com.moneygram.mgo.service.consumer_v2.gcpservice.exception.GCPServiceException;
import com.moneygram.mgo.service.consumer_v2.util.ConsumerUtil;

public class GetGlobalConsumerProfileIdCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			GetGlobalConsumerProfileIdCommand.class);
	
	protected OperationResponse process(OperationRequest request)
	throws CommandException {

		Long gcpId = null;
		try {
			// Begin : vl58 - S8 (New Architecture) Call GCP service for
			// retrieving GCP id

			try {
				logger.debug("Entry inside GetGlobalConsumerProfileIdCommand");
				gcpId = getGCPId();
				logger.debug("gcpId" + gcpId);
			} catch (GCPServiceException ex) {
				throw new CommandException(ex.getMessage(), ex);
			}
			// End : vl58 - S8 (New Architecture) Call GCP service for
			// retrieving GCP id
		} catch (Exception e) {
			throw new CommandException("Failed to add consumer profile", e);
		}

		GetGlobalConsumerProfileIdResponse response = new GetGlobalConsumerProfileIdResponse();
		response.setCnsmrProfileId(gcpId);
		return response;
	}

	private long getGCPId() throws GCPServiceException {
		long gcpId = -1;
		try {
			GCPServiceBroker gcpServiceBroker = new GCPServiceBroker();
			gcpId = gcpServiceBroker.getGCPID();
		} catch (GCPServiceException ex) {
			logger.error("Error in getGCPId: " + ex.getMessage(), ex);
			throw ex;
		} catch (Exception ex) {
			logger.error("Error in getGCPId: " + ex.getMessage(), ex);
		}
		return gcpId;
	}

@Override
protected boolean isRequestSupported(OperationRequest request)
		throws CommandException {
	// TODO Auto-generated method stub
	return request instanceof GetGlobalConsumerProfileIdRequest;
}
}
