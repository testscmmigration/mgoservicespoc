/*
 * Created on Jun 22, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.DataFormatException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.TransactionalCommand;
import com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationRequest;
import com.moneygram.mgo.service.consumer_v2.SaveValidateRegistrationInformationResponse;
import com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfo;
import com.moneygram.mgo.service.consumer_v2.ValidateRegistrationSaveInfoRsls;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;
/**
 * @author vx15
 */
public class SaveValidateRegInfoCommand extends TransactionalCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			SaveValidateRegInfoCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof SaveValidateRegistrationInformationRequest;
	}

	@Override
	@Transactional
	protected OperationResponse process(OperationRequest request) throws CommandException {
		SaveValidateRegistrationInformationRequest concreteRequest = (SaveValidateRegistrationInformationRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("Operation: =" + SaveValidateRegistrationInformationRequest.class.getSimpleName()+" = "+ concreteRequest);
		}

		if (concreteRequest == null)
            throw new DataFormatException("SaveValidateRegistrationInformationRequest is missing");
        if (concreteRequest.getCustId() <=0 )
            throw new DataFormatException("SaveValidateRegistrationInformationRequest CustId is missing");

        ConsumerDAO dao = (ConsumerDAO)getDataAccessObject();
        
        ValidateRegistrationSaveInfo info = new ValidateRegistrationSaveInfo();
        
        info.setAuthnDate(concreteRequest.getAuthnDate()!=null?concreteRequest.getAuthnDate().getTime():null);
        info.setAuthnId(concreteRequest.getAuthnId());
        info.setCustId(concreteRequest.getCustId());
        info.setDeciBandText(concreteRequest.getDeciBandText());
        info.setRemarksText(concreteRequest.getRemarksText());
        
        List<ValidateRegistrationSaveInfoRsls> rsls = new ArrayList<ValidateRegistrationSaveInfoRsls>();
        
        if(concreteRequest.getValidateRsls() != null)
        for (ValidateRegistrationSaveInfoRsls  currentRsl : concreteRequest.getValidateRsls()){
        	ValidateRegistrationSaveInfoRsls rsl = new ValidateRegistrationSaveInfoRsls();
            rsl.setRsltCode(currentRsl.getRsltCode());
            rsl.setRsltDesc(currentRsl.getRsltDesc());
            rsl.setChkTypeCode(currentRsl.getChkTypeCode());
            rsl.setChkTypeDesc(currentRsl.getChkTypeDesc());
            rsls.add(rsl);
        }
        
        info.setValidateRegistrationSaveInfoRsls(rsls);
        

        info.setScoreNbr(concreteRequest.getScoreNbr());
        info.setStatText(concreteRequest.getStatText());
        

        //TODO: check transactionality, if one fails all should fail
        boolean saveOK1 = dao.saveValidateRegistrationInformationResp(info);
        boolean saveOK2 = dao.saveValidateRegistrationInformationTyp(info);
        boolean saveOK3 = dao.saveValidateRegistrationInformationRsl(info);
        
        SaveValidateRegistrationInformationResponse response = new SaveValidateRegistrationInformationResponse();
        response.setSaveOK(saveOK1 && saveOK2 && saveOK3);
        
		return response;
	}
	
	

}
