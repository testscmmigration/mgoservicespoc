/*
 * Created on Aug 28, 2009
 *
 */
package com.moneygram.mgo.service.consumer_v2.command;

import java.util.Date;
import java.util.List;

import com.moneygram.common.dao.DAOUtils;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.service.CommandException;
import com.moneygram.common.service.OperationRequest;
import com.moneygram.common.service.OperationResponse;
import com.moneygram.common.service.ReadCommand;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.FindConsumersRequest;
import com.moneygram.mgo.service.consumer_v2.FindConsumersResponse;
import com.moneygram.mgo.service.consumer_v2.dao.ConsumerDAO;

public class FindConsumersCommand extends ReadCommand {

	public static final Logger logger = LogFactory.getInstance().getLogger(
			FindConsumersCommand.class);

	@Override
	protected boolean isRequestSupported(OperationRequest request)
			throws CommandException {
		return request instanceof FindConsumersRequest;
	}

	@Override
	protected OperationResponse process(OperationRequest request)
			throws CommandException {
		FindConsumersRequest profileRequest = (FindConsumersRequest) request;

		if (logger.isDebugEnabled()) {
			logger.debug("process: find consumers dateOfBirth="
					+ DAOUtils.toString(profileRequest.getDateOfBirth())
					+ " ssnMask=" + profileRequest.getSsnMask() + " lastName="
					+ profileRequest.getLastName() + " addressLine1StartsWith="
					+ profileRequest.getAddressLine1StartsWith() + "addressPostalCode= "
					+ profileRequest.getAddressPostalCode() + "addressBldgName= "
					+ profileRequest.getAddressBldgName());
		}

		ConsumerDAO dao = (ConsumerDAO) getDataAccessObject();

		List<Consumer> list = null;
		try {
			Date birthDate = null;
			if (profileRequest.getDateOfBirth() != null)
				birthDate = profileRequest.getDateOfBirth().getTime();

			list = dao.getActiveConsumers(birthDate, profileRequest
					.getSsnMask(), profileRequest.getLastName(), profileRequest
					.getAddressLine1StartsWith(), profileRequest.getAddressPostalCode(), 
					profileRequest.getAddressBldgName());
		} catch (Exception e) {
			logger.warn("process: failed to find consumers", e);
			throw new CommandException("Failed to find consumers", e);
		}

		FindConsumersResponse response = new FindConsumersResponse();
		if (list == null)
			response.setConsumers(new Consumer[0]);
		else
			response.setConsumers((Consumer[]) list.toArray(new Consumer[list
					.size()]));
		return response;
	}
}
